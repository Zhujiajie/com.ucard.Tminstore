//
//  mainInfoCell.swift
//  UCardStore_Swift
//
//  Created by 朱佳杰 on 2017/10/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MainInfoCell: UITableViewCell {
    
    fileprivate var userImage: UIImageView?
    
    fileprivate var infoLabel: UILabel?
    
    fileprivate var timeLabel: UILabel?
    
    fileprivate var priceLabel: UILabel?
    
    fileprivate var contentImage: UIImageView?
    
    fileprivate var iconImage: UIImageView?
    
    fileprivate var countLabel: UILabel?
    
    fileprivate var makeButton: BaseButton?
    
    fileprivate var lineView: UIView?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = UITableViewCellSelectionStyle.none
        userImage = UIImageView.init()
        userImage?.image = #imageLiteral(resourceName: "shareMore")
        contentView.addSubview(userImage!)
        userImage?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(contentView).offset(10)
            make.top.equalTo(contentView).offset(10)
            make.width.height.equalTo(40/baseWidth)
        })
        infoLabel = UILabel.init()
        infoLabel?.textColor = UIColor.black
        infoLabel?.font = UIFont.systemFont(ofSize: 15.0)
        infoLabel?.text = "sadahdkjash"
        contentView.addSubview(infoLabel!)
        infoLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(self.userImage!.snp.right).offset(10)
            make.top.equalTo(self.userImage!)
        })
        timeLabel = UILabel.init()
        timeLabel?.textColor = UIColor(hexString: "989898")
        timeLabel?.font = UIFont.systemFont(ofSize: 14.0)
        timeLabel?.text = "6分钟前"
        contentView.addSubview(timeLabel!)
        timeLabel?.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.equalTo(self.infoLabel!)
            make.bottom.equalTo(self.userImage!.snp.bottom)
        }
        priceLabel = UILabel.init()
        priceLabel?.textColor = UIColor(hexString: "FFB66A")
        priceLabel?.font = UIFont.systemFont(ofSize: 17.0)
        priceLabel?.textAlignment = NSTextAlignment.right
        priceLabel?.text = "¥12"
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(contentView).offset(-10)
            make.top.equalTo(self.userImage!)
        })
        contentImage = UIImageView.init()
        contentImage?.image = #imageLiteral(resourceName: "shareArrowIcon")
        contentView.addSubview(contentImage!)
        contentImage?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self.userImage!.snp.bottom).offset(10)
            make.left.right.equalTo(contentView)
            make.height.equalTo(200/baseHeight)
        })
        iconImage = UIImageView.init()
        iconImage?.image = #imageLiteral(resourceName: "cell_icon")
        contentView.addSubview(iconImage!)
        iconImage?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self.contentImage!.snp.bottom).offset(10)
            make.left.equalTo(self.userImage!)
            make.width.height.equalTo(20/baseWidth)
        })
        countLabel = UILabel.init()
        countLabel?.text = "12次购买"
        countLabel?.textColor = UIColor(hexString: "9A9A9A")
        countLabel?.font = UIFont.systemFont(ofSize: 13.0)
        contentView.addSubview(countLabel!)
        countLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(self.iconImage!.snp.right).offset(10)
            make.centerY.equalTo(self.iconImage!)
        })
        makeButton = BaseButton.leftImageAndRightTitleButton(iconName: "cell_dimond", andFont: UIFont.systemFont(ofSize: 15.0), andTextColorHexString: "9A9A9A", andTitle: "定制")
        contentView.addSubview(makeButton!)
        makeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(self.priceLabel!)
            make.centerY.equalTo(self.iconImage!)
            make.height.equalTo(30)
            make.width.equalTo(70)
        })
        lineView = UIView.init()
        contentView.addSubview(lineView!)
        lineView?.backgroundColor = UIColor(hexString: "F2F2F2")
        lineView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.bottom.equalTo(contentView)
            make.height.equalTo(5)
        })
    }
    
    func assignData(data: MainInfoModel) {
        priceLabel?.text = "¥" + "12"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
