//
//  Comment+CoreDataProperties.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import Foundation
import CoreData


extension Comment {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Comment> {
        return NSFetchRequest<Comment>(entityName: "Comment");
    }

    @NSManaged public var commentId: String?
    @NSManaged public var content: String?
    @NSManaged public var createDate: NSDate?
    @NSManaged public var memberCode: String?
    @NSManaged public var nickName: String?
    @NSManaged public var originalId: String?
    @NSManaged public var replyTotal: Int32
    @NSManaged public var status: String?
    @NSManaged public var updateDate: NSDate?
    @NSManaged public var userLogoStr: String?
    @NSManaged public var postcardInCommunity: PostcardInCommunity?

}
