//
//  PostcardInfo+CoreDataProperties.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/28.
//  Copyright © 2016年 ucard. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PostcardInfo {

    @NSManaged var id: NSNumber?
    @NSManaged var nickName: String?
    @NSManaged var originalCountryCode: String?
    @NSManaged var destinationCountryCode: String?
    @NSManaged var serialNumber: String?
    @NSManaged var userId: String?
    @NSManaged var frontPicture: String?
    @NSManaged var receiverId: NSNumber?
    @NSManaged var backPicture: String?
    @NSManaged var caption: String?
    @NSManaged var message: String?
    @NSManaged var signPicture: String?
    @NSManaged var sendingAddress: String?
    @NSManaged var userAddressId: NSNumber?
    @NSManaged var likeCount: NSNumber?
    @NSManaged var commentsTotal: NSNumber?
    @NSManaged var stored: NSNumber?
    @NSManaged var isHorizon: NSNumber?
    @NSManaged var userLiked: NSNumber?
    @NSManaged var sendingState: NSNumber?
    @NSManaged var paiedAt: Date?
    @NSManaged var sendAt: Date?
    @NSManaged var receivedAt: Date?
    @NSManaged var createdAt: Date?
    @NSManaged var createdAtString: String?
    @NSManaged var city_CN: String?
    @NSManaged var city_EN: String?
    @NSManaged var userPhoto: String?
    @NSManaged var paymentType: NSNumber?
    @NSManaged var totalPriceRMB: NSNumber?
    @NSManaged var totalPriceEUR: NSNumber?
    @NSManaged var commentList: NSOrderedSet?
    @NSManaged var videoID: String?
}
