//
//  PostcardInCommunity+CoreDataProperties.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import Foundation
import CoreData


extension PostcardInCommunity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PostcardInCommunity> {
        return NSFetchRequest<PostcardInCommunity>(entityName: "PostcardInCommunity");
    }

    @NSManaged public var backImageURLStr: String?
    @NSManaged public var city: String?
    @NSManaged public var commentTotal: Int32
    @NSManaged public var country: String?
    @NSManaged public var createDate: NSDate?
    @NSManaged public var customerType: String?
    @NSManaged public var detailedAddress: String?
    @NSManaged public var district: String?
    @NSManaged public var frontImageURLStr: String?
    @NSManaged public var isAR: NSNumber?
    @NSManaged public var isOnMarket: NSNumber?
    @NSManaged public var isPurchased: NSNumber?
    @NSManaged public var isSale: NSNumber?
    @NSManaged public var memberCode: String?
    @NSManaged public var nickName: String?
    @NSManaged public var orderTotal: Int32
    @NSManaged public var originalId: String?
    @NSManaged public var photoLocation: NSObject?
    @NSManaged public var photoNumber: String?
    @NSManaged public var pointPrice: Int32
    @NSManaged public var province: String?
    @NSManaged public var remark: String?
    @NSManaged public var reviewStatus: String?
    @NSManaged public var targetId: String?
    @NSManaged public var updateDate: NSDate?
    @NSManaged public var userLogoURLStr: String?
    @NSManaged public var videoId: String?
    @NSManaged public var commentList: NSSet?
    @NSManaged public var isLiked: NSNumber?
    @NSManaged public var likeCount: Int32

}

// MARK: Generated accessors for commentList
extension PostcardInCommunity {

    @objc(addCommentListObject:)
    @NSManaged public func addToCommentList(_ value: Comment)

    @objc(removeCommentListObject:)
    @NSManaged public func removeFromCommentList(_ value: Comment)

    @objc(addCommentList:)
    @NSManaged public func addToCommentList(_ values: NSSet)

    @objc(removeCommentList:)
    @NSManaged public func removeFromCommentList(_ values: NSSet)

}
