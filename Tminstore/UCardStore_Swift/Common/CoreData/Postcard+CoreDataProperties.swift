//
//  Postcard+CoreDataProperties.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/6.
//  Copyright © 2017年 ucard. All rights reserved.
//

import Foundation
import CoreData


extension Postcard {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Postcard> {
        return NSFetchRequest<Postcard>(entityName: "Postcard");
    }

    @NSManaged public var addressImageUUID: String?
    @NSManaged public var backImageRemark: String?
    @NSManaged public var backImageUUID: String?
    @NSManaged public var backPreviewImage: NSData?
    @NSManaged public var frontImageRemark: String?
    @NSManaged public var frontImageUUID: String?
    @NSManaged public var frontPreviewImage: NSData?
    @NSManaged public var memberCode: String?
    @NSManaged public var photoLocation: NSObject?
    @NSManaged public var photoLocationText: String?
    @NSManaged public var userID: String?
    @NSManaged public var videoID: String?
    @NSManaged public var videoPath: String?
    @NSManaged public var videoRemark: String?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var backView: BackView?
    @NSManaged public var frontView: FrontView?
    @NSManaged public var isPrivate: NSNumber?
}
