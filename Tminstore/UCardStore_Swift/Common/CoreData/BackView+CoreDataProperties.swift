//
//  BackView+CoreDataProperties.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/9/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import Foundation
import CoreData

extension BackView {

    @NSManaged public var signatureImage: Data?
    @NSManaged public var templateIndex: NSNumber?
    @NSManaged public var text0: String?
    @NSManaged public var text0FontName: String?
    @NSManaged public var text0FontSize: NSNumber?
    @NSManaged public var text1: String?
    @NSManaged public var text1FontName: String?
    @NSManaged public var text1FontSize: NSNumber?
    @NSManaged public var yOffset: NSNumber?
    @NSManaged public var postcard: Postcard?

}
