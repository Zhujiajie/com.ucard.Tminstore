//
//  FrontView+CoreDataProperties.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/5.
//  Copyright © 2017年 ucard. All rights reserved.
//

import Foundation
import CoreData


extension FrontView {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FrontView> {
        return NSFetchRequest<FrontView>(entityName: "FrontView");
    }

    @NSManaged public var filter0Number: NSNumber?
    @NSManaged public var filter1Number: NSNumber?
    @NSManaged public var filter2Number: NSNumber?
    @NSManaged public var filter3Number: NSNumber?
    @NSManaged public var image0Data: NSData?
    @NSManaged public var image1Data: NSData?
    @NSManaged public var image2Data: NSData?
    @NSManaged public var image3Data: NSData?
    @NSManaged public var isHorizon: NSNumber?
    @NSManaged public var templateIndex: NSNumber?
    @NSManaged public var postcard: Postcard?

}
