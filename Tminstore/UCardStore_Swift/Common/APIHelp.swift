//
//  APIHelp.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/23.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:------------接口相关数据-------------------

import Foundation

/// 主接口
//let javaBaseAPI: String = "http://106.14.60.213:8012/ucardstore/"
//let javaBaseAPI: String = "http://106.14.60.213:8013/"
/// 游客模式的域名
//let javaBaseAPI: String = "http://106.14.60.213:8016/"
//let javaBaseAPI: String = "http://106.14.60.236:8017/"
let javaBaseAPI: String = "http://106.14.60.213:8013/"
let javaBaseAPI02: String = "http://106.14.60.213:8013/"
/// 又拍云主域名
let upyunImageBase: String = "http://ucardstorevideo.b0.upaiyun.com/iOSImages/"

/// 存放接口的结构体
struct APIHelp {
    
    /// 又拍云域名
    let upyunBase: String = "http://ucardstorevideo.b0.upaiyun.com"
    
    /// 时光首页的接口
    let timeDataAPI: String = javaBaseAPI + "storePictures/classification"
    
    /// 邮箱登录接口
    let emailLoginAPI: String = javaBaseAPI + "login/email"
    
    /// 手机号码登录接口
    let phoneNumberLoginAPI: String = javaBaseAPI + "login/phone"
    
    /// 请求手机验证码的接口
    let requestPhoneVerificationCodeAPI: String = javaBaseAPI + "register/phoneRegister"
    
    /// 验证手机验证码的接口
    let verifyVerificationCodeAPI: String = javaBaseAPI + "register/phoneCode"
    
    /// 邮箱注册接口
    let emailRegistAPI: String = javaBaseAPI + "register/emailRegister"
    
    /// 手机号码注册设置密码的接口
    let phoneRegistAPI: String = javaBaseAPI + "register/registByPhone"
    
    /// 微信登录的接口
    let weChatLoginAPI: String = javaBaseAPI + "thirdParty/weChat"
    
    /// 微博登录的接口
    let weiBoLoginAPI: String = javaBaseAPI + "thirdParty/MicroBlog"
    
    /// 获取用户地址列表的接口
    let getUserAddressListAPI: String = javaBaseAPI + "address/queryAddress/"
    
    /// 删除用户地址信息的接口
    let deleteUserAddressAPI: String = javaBaseAPI + "address/delAddress"
    
    /// 新增用户地址信息的接口
    let addUserAddressAPI: String = javaBaseAPI + "address/addAddress"
    
    /// 更新用户地址信息的接口
    let updateAddressAPI: String = javaBaseAPI + "address/updateAddress"
    
    /// 获取用户优惠券列表的接口
    let getUserCouponListAPI: String = javaBaseAPI + "coupon/queryCoupon/"
    
    /// 获取优惠券的接口
    let addCouponAPI: String = javaBaseAPI + "coupon/receiveCoupon/"
    
    /// 使用优惠券的接口
    let useCouponAPI: String = javaBaseAPI + "coupon/useCoupon"
    
    /// 上传邮寄订单的接口
    let uploadMainOrderAPI: String = javaBaseAPI + "individual/postcard/make"
    
    /// 请求支付数据的接口
    let mailOrderPayAPI: String = javaBaseAPI + "purchase/payment/postcard/mail/one"
    
    /// 请求二次支付的接口
    let payAgainAPI: String = javaBaseAPI + "purchase/secondPay/one"
    
    /// 将已付款的AR邮寄订单分享到时空圈的接口
    let shareARMailToCommunityAPI: String = javaBaseAPI + "community/mailed/release"
    
    /// 重新发布到时空圈的接口
    let shareToCommunityAgainAPI: String = javaBaseAPI + "community/release/anew"
    
    /// 上传头像的接口
    let uploadPortraitAPI: String = javaBaseAPI + "userLogo"
    
    /// 发布到社区的接口
    let shareToCommunityAPI: String = javaBaseAPI + "community/release/one"
    
    /// 发布为私人的接口
    let releaseToPrivateAPI: String = javaBaseAPI + "community/releaseToIndividual"
    
    /// 取消分享到时空圈
    let cancelReleaseAPI: String = javaBaseAPI + "community/originalId/cancel"
    
    /// 通知后台支付结果的接口
    let notifyPaymentResultAPI: String = javaBaseAPI + "purchase/paySuccess"
    
    /// 获取社区主页数据的接口
    // 该接口将要弃用
    //let getCommunityDataAPI: String = javaBaseAPI + "community/showShare"
    let getCommunityDataAPI: String = javaBaseAPI + "community/showNewShare"
    
    /// 举报的接口
    let reportAPI: String = javaBaseAPI + "report"
    
    /// 获取用户信息的接口
    let requestUserInfo: String = javaBaseAPI + "userInfo"
    
    /// 查看评论的接口
    let checkCommentAPI: String = javaBaseAPI + "community/showComment"
    
    /// 查看回复的接口
    let checkReplyAPI: String = javaBaseAPI + "community/showReply"
    
    /// 查看用户所有发布到时空圈的图片
    let releaseCardAPI: String = javaBaseAPI + "community/postcard"
    
    /// 查看用户所有邮寄订单的接口
    //let checkOrderAPI: String = javaBaseAPI + "individual/exhibition/mailed"
    let checkOrderAPI: String = javaBaseAPI + "orders/"
    
    /// 完善用户信息的接口
    let putUserInfoAPI: String = javaBaseAPI + "userInfo/supplement"
    
    /// 发表评论
    let postCommentApi: String = javaBaseAPI + "community/writeComment"
    
    /// 发表回复的接口
    let postReplyAPI: String = javaBaseAPI + "community/writeReply"
    
    /// 删除评论的接口
    let deleteCommentAPI: String = javaBaseAPI + "community/deleteComment"
    
    /// 删除回复的接口
    let deleteReplyAPI: String = javaBaseAPI + "community/deleteReply"
    
    /// 举报评论的接口
    let reportCommentAPI: String = javaBaseAPI + "community/comment/commentId/"
    
    /// 举报回复的接口
    let reportReplyAPI: String = javaBaseAPI + "community/reply/replyId/"
    
    /// 发送点赞请求的接口
    let sendLikeAPI: String = javaBaseAPI + "community/addLike"
    
    /// 发送取消点赞请求的接口
    let sendCancelLikeAPI: String = javaBaseAPI + "community/cancelLike"
    
    /// 上传极光的推送ID
    let postJPushIDAPI: String = javaBaseAPI + "userPushId"
    
    /// 退出登录的API
    let LogoutAPI: String = javaBaseAPI + "logout"
    
    /// 绑定邮箱-请求验证码
    let bingingEmailRequestCodeAPI: String = javaBaseAPI + "binding/bindingByEmail"
    
    /// 绑定邮箱-验证验证码
    let bingingEmailVerificationAPI: String = javaBaseAPI + "binding/verificationByEmail"
    
    /// 绑定手机-请求验证码
    let bingdingPhoneRequestCodeAPI: String = javaBaseAPI + "binding/bindingByPhone"
    
    /// 绑定手机-验证验证码
    let bingdingPhoneVerificationAPI: String = javaBaseAPI + "binding/verificationByPhone"
    
    /// 删除发布的接口
    let deleteReleaseAPI: String = javaBaseAPI + "community/identity/originalId/"
    
    /// 未支付的订单删除
    let deleteOrderAPI: String = javaBaseAPI + "purchase/payment/identity/"
    
    /// 买图限制条件检查的接口
    let purchaseAvaliableCheckAPI: String = javaBaseAPI + "purchase/payment/postcards/image/check"
    
    /// 买图提交订单接口
    let purchaseConfirmOrderAPI: String = javaBaseAPI + "purchase/payment/postcards/image/purchase"
    
    /// 买图成功之后，更改买图次数
    let changePurchaseCountAPI: String = javaBaseAPI + "community/postcards/image/originalId"
    
    /// 显示消息列表
    let getMessageListAPI: String = javaBaseAPI + "message/showNotice/"
    
    /// 将某一条消息状态改为已读
    let readMessageAPI: String = javaBaseAPI + "message/readNotice"
    
    /// 删除某一条消息
    let deleteMessageAPI: String = javaBaseAPI + "message/delNotice"
    
    /// 显示系统消息列表
    let getSystemMessageListAPI: String = javaBaseAPI + "message/showMessage/"
    
    /// 判断是否有未读推送消息
    let checkNoticeAPI: String = javaBaseAPI + "message/isNoticeRead"
    
    /// 判断是否有未读系统消息
    let checkSystemNoticeAPI: String = javaBaseAPI + "message/isMessageRead"
    
    /// 所有推送消息标为已读
    let readAllMessageAPI: String = javaBaseAPI + "message/readAllNotice"
    
    /// 所有系统消息标为已读
    let readAllSystemMessageAPI: String = javaBaseAPI + "message/readAllMessage"
    
    /// 删除所有推送消息
    let deleteAllMessageAPI: String = javaBaseAPI + "message/delAllNotice"
    
    /// 读取系统消息
    let readSystemMessageAPI: String = javaBaseAPI + "message/readMessage"
    
    /// 删除系统消息
    let deleteSystemMessageAPI: String = javaBaseAPI + "message/delMessage"
    
    /// 查看被买的一张图
    let checkPurchasedCardAPI: String = javaBaseAPI + "community/postcards/postcard/originalId/"
    
    /// 查看附近1000米的图片信息
    let mapDataAPI: String = javaBaseAPI + "spaceTimeCircle/location/range"
    
    /// 向后台索取AR是被图的originalId的接口
    let requestARImageOriginalIdAPI: String = javaBaseAPI + "codeCard/code/"
    
    /// 通过originalId向后端索取AR识别图下载地址和视频地址的信息
    let requestARImageInfoAPI: String = javaBaseAPI + "AR/video/image/originalId/"
    
    /// 通过mailOrderNo向后端索取单个订单的信息
    let requestOrderInfoAPI: String = javaBaseAPI + "order/mailOrderNo/"
    
    /// 发布地图的接口
    let releaseToMapAPI: String = javaBaseAPI + "spaceTimeCircle/create"
    
    /// 查看某图片对应的所有AR视频
    let checkARVideosAPI: String = javaBaseAPI + "arPalt/showArList"
    
    /// 查看某图片对应的视频记忆
    let showARVideoMemoryAPI: String = javaBaseAPI + "videos/originalId/"
    
    
    /// 查看用户发布到地图的数据
    let checkMapDataAPI: String = javaBaseAPI + "spaceTimeCircle/"
    
    /// 取消发布到地图
    let canelMapDataAPI: String = javaBaseAPI + "spaceTimeCircle/cancel"
    
    /// 重新发布到地图
    let reReleaseToMapAPI: String = javaBaseAPI + "spaceTimeCircle/create/anew"
    
    /// 删除发布到地图的数据
    let deletaMapDataAPI: String = javaBaseAPI + "spaceTimeCircle/delete"
    
    /// 在时光轴添加视频的接口
    let spaceTimeAddVideoAPI: String = javaBaseAPI + "spaceTimeCircle/video/add"
    
    /// 查看时光轴的发表评论
    let showSpaceTimeCommentAPI: String = javaBaseAPI + "spaceTimeCircle/communion/showComment"
    
    /// 发表视频评论的接口
    let uploadVideoCommentAPI: String = javaBaseAPI + "spaceTimeCircle/communion/writeComment"
    
    /// 发表视频回复的接口
    let uploadVideoReplyAPI: String = javaBaseAPI + "spaceTimeCircle/communion/writeReply"
    
    /// 显示视频回复
    let showVideoReplyAPI: String = javaBaseAPI + "spaceTimeCircle/communion/showReply"
    
    /// 上传录屏信息
    let uploadVideoShareAPI: String = javaBaseAPI + "screencap/add/screencap"
    
    /// 时光轴视频点赞的接口
    let videoLikeAPI: String = javaBaseAPI + "spaceTimeCircle/communion/video/addLike"
    
    /// 时光轴视频取消点赞的接口
    let videoCancelLikeAPI: String = javaBaseAPI + "spaceTimeCircle/communion/video/cancelLike"
    
    /// 查看用户的礼品优惠券
    let getGiftCouponAPI: String = javaBaseAPI + "coupon/getGift/"
    
    /// 线上活动，提交信息
    let activityUploadInfoAPI: String = javaBaseAPI + "coupon/updateGift"
    
    /// 判断是否弹出推广信息
    let alertCheckAPI: String = javaBaseAPI + "if/popup"
    
    /// 获取商品信息的接口
    let goodsListAPI: String = javaBaseAPI + "store/goods/list/"
    
    /// 获取创意图片的接口
    let requestIdealListAPI: String = javaBaseAPI + "store/pictures/listAll/"
    /// 计算价格接口
    let computePriceAPI: String = javaBaseAPI + "store/pictures/computePrice"
    /// 上传订单的信息接口
    let uploadGoodsOrderAPI: String = javaBaseAPI + "store/pictures/mail/buy"
    
    /// 查看用户发布到时空圈的图片的详情
    let checkSpaceTimeVideoCommentAPI: String = javaBaseAPI + "spaceTimeCircle/video/"
    
    /// 查看用户发布到时空圈的图片的详情
    let checkSpaceTimeVideoMemoryAPI: String = javaBaseAPI + "videos/originalId/"
    
    /// 查看时空圈点赞详情
    let checkSpaceTimeLikeDetailAPI: String = javaBaseAPI + "spaceTimeCircle/communion/likes/videoId/"
    
    /// 获取时光相册的API
    let arAlbumAPI: String = javaBaseAPI + "timeAlbum/code/"
    
    /// 查看用户AR识别图的接口
    let checkUserARImageAPI: String = javaBaseAPI + "images/"
    
    /// 查看单张AR识别图对应的视频接口
    let checkSingleImageVideoAPI: String = javaBaseAPI + "AR/videos/originalId/"
    
    /// 查看用户录屏信息的接口
    let checkUserRecordInfoAPI: String = javaBaseAPI + "screencap/show/screencap/"
    
    /// 统计AR时光相册图片的扫描次数
    let arAlbumScanTimesAPI: String = javaBaseAPI + "timeAlbum/scannedTimes/originalId/"
    
    /// 删除录屏数据的接口
    let deleteRecordInfoAPI: String = javaBaseAPI + "screencap/del/screencap"
    
    /// 查看用户个人图片的接口
    let checkUserPhotoAPI: String = javaBaseAPI + "store/pictures/listCode/"
    
    /// 查看用户图片对应的视频
    let checkUserPhotoDetailInfoAPI: String = javaBaseAPI + "store/pictures/originalId/"
    
    /// 根据主条目获取所属子条目
    let checkSubCatalogAPI: String = javaBaseAPI + "cloud/subcatalogs/catalog/"
    
    /// 根据获取子条目下图片信息
    let subCatalogImageAPI: String = javaBaseAPI + "clouds/subcatalog/"
    
    /// 统计云识别扫描次数的API
    let countCloudAPI: String = javaBaseAPI + "cloud/scanTime"
    
    /// 判断云识别是否已点赞
    let checkCloudIsLikedAPI: String = javaBaseAPI + "like/cloud/video/targetId/"
    
    /// 对云识别进行点赞或者取消点赞。POST方法点赞，PUT方法取消点赞
    let setCloudLikeAPI: String = javaBaseAPI + "like/cloud/video/targetId/"
    
    /// 查看其他用户数据
    let checkOtherUserInfoAPI: String = javaBaseAPI + "userFollow/findUserInfo"
    
    /// 查看其他用户时光圈数据
    let checkOtherUserSpaceTimeDataAPI: String = javaBaseAPI + "userFollow/findUserSpaceInfo/"
    
    /// 查看我的好友
    let checkMyFriendsAPI: String = javaBaseAPI + "userFollow/user/friends/"
    
    /// 查看我的关注
    let checkMyFollowsAPI: String = javaBaseAPI + "userFollow/showFollow/"
    
    /// 查看我的粉丝
    let checkMyFansAPI: String = javaBaseAPI + "userFollow/showFans/"
    
    /// 关注某人
    let followUserAPI: String = javaBaseAPI + "userFollow/followUser"
    
    /// 取消关注某人
    let cancelFollowUserAPI: String = javaBaseAPI + "userFollow/cancelFollow"
    
    /// 获取其他用户的关注列表
    let requestOtherUserFollowsAPI: String = javaBaseAPI + "userFollow/user/showFollow/"
    
    /// 获取其他用户的粉丝数据
    let requestOtherUserFansAPI: String = javaBaseAPI + "userFollow/user/showFans/"
    
    /// 获取其他用户录屏信息
    let requestOtherUserRecordAPI: String = javaBaseAPI + "userFollow/findUserScreenInfo/"
    
    /// 查看当前用户的好友数据
    let requestMyFriendsDataAPI: String = javaBaseAPI + "userFollow/user/friends/"
    
    /// 查看当前用户的关注数据
    let requestMyFollowsDataAPI: String = javaBaseAPI + "userFollow/showFollow/"
    
    /// 查看当前用户的粉丝数据
    let requestMyFansDataAPI: String = javaBaseAPI + "userFollow/showFans/"
    
    /// 上传创意图片的接口
    let uploadIdealAPI: String = javaBaseAPI + "store/pictures/insert"
    
    /// 查看钱包余额
    let checkBalanceAPI: String = javaBaseAPI + "user/wallet/lookupWallet"
    
    /// 查看钱包交易明细
    let checkWalletDetailAPI: String = javaBaseAPI + "user/wallet/walletOrder/"
    
    /// 查看我的创意图片的接口
    let checkMyIdealAPI: String = javaBaseAPI + "store/pictures/listCode/"
    
    /// 查看我上传的图片接口
    let checkMyUploadAPI: String = javaBaseAPI + "storePictures/uploaded/"
    
    /// 支付密码相关, 获取验证码
    let paymentPasswordGetCode: String = javaBaseAPI02 + "user/pay/password/judgePhone"
    
    /// 支付密码相关, 验证验证码
    let paymentPasswordCheck: String = javaBaseAPI02 + "user/pay/password/phoneCode"
    
    /// 设置支付密码的接口
    let setPaymentPasswordAPI: String = javaBaseAPI02 + "user/pay/password/insertPayPwd"
    
    /// 获取新用户优惠券的接口
    let newUserCouponAPI: String = javaBaseAPI + "coupon/firstCoupon"
    
    /// 判断用户是否设置过支付密码
    let checkPaymentPasswordAPI: String = javaBaseAPI + "user/pay/password/judge/is/payPwd"
    
    /// 修改或重置支付密码的接口
    let resetPaymentPasswordAPI: String = javaBaseAPI02 + "user/pay/password/updatePayPwd"
    
    /// 判断旧支付密码是否有效
    let checkOldPaymentPasswordAPI: String = javaBaseAPI02 + "user/pay/password/judgePayPwd"
    
    /// 提交订单信息的接口
    let uploadOrderInfoAPI: String = javaBaseAPI + "store/pictures/mail/buyNew/one"
    
    /// 请求支付信息的接口
    let requestChargeInfoAPI: String = javaBaseAPI + "store/pictures/mail/payByChannel"
    
    /// 选择支付方式进行支付
    let choosePayChannelAndPayAPI: String = javaBaseAPI + "store/pictures/mail/payByChannel"
    
    /// 验证支付密码
    let verifyPaymentPasswordAPI: String = javaBaseAPI + "store/walletPay/pwd"
    
    //MARK: -------------谷歌相关--------------
    let googleAPIKey: String = "AIzaSyBO6L8uX9u_VIpoiobZKWVH8NDTRnyS8MU"
    
    //MARK:---------微信相关----------
    /// 微信AppKey
    let WeiChatAppKey: String = "wxb0e52176f9035f39"
    /// 微信AppSecret
    let WeiChatAppSecret: String = "be6df9bd9576e139d9200ed9fe8e4834"
    
    //MARK: ------------微博相关---------------
    /// 获取微信accessToken
    let GetWeiChatAccessAPI: String = "https://api.weixin.qq.com/sns/oauth2/access_token"
    /// 微博AppKey
    let WeiboAppKey: String = "1097336707"
    /// 微博AppSecret
    let WeiboAppSecret:String = "d399ee94d24c081898af39de79d1ea84"
    /// 微博回调页
    let WeiboRedirectURL: String = "http://ucardapi.southeastasia.cloudapp.azure.com/signin-sinaWeibo"
    
    //MARK:--------------paypal相关----------------
    /// PayPal ClientID
    let PayPalClientID: String = "AbbWH-RkvrDvhN5RdzIqxnfNvAiy95Ln6D4J-79VApekfSlmOX1TTNEFm1zWZl9xk4LMXMFTKsNWx3ie"
    /// PayPal 沙盒 ClientID
    let PayPalSandboxClientID: String = "AcozcWnuEwRCFG8egdQLSNFH0aRZ-JTxbMWQOwOFtOr9kJSb_E21zZhG_0epKD2JC1BcidypDIEWhTij"
    let PayPalPublickKey = "10181022285fe2cfbedee37a7a07789d"
    
    //MARK:-----------------友盟相关---------------
    /// 友盟APPKey
    let UMengAPPKey: String = "57859776e0f55aac5c000a91"
    
    //MARK:------------极光推送相关-----------------
    
    /// 极光appKey
    let JPushAppKey: String = "77f2c9b5371012ea52ef5e2d"
    /// 推送渠道
    let JPushChannel: String = "App Store"
    /// false表示开发环境，true表示生产环境
    let isProduction: Bool = true
    
    //MARK: ------------从iTunes获取新版本-------------------
    func GetITunesAPI()->String{
        if languageCode() == "CN"{
            return "https://itunes.apple.com/cn/lookup?id=966372400"
        }else{
            return "https://itunes.apple.com/lookup?id=966372400"
        }
    }
    
    //MARK: ping++相关
    let kAppURLScheme: String = "Tminstore"
    
    //MARK: ------------从远程下载图片-------------
    
    /// 事例图片
    func sampleImageURL()->URL{
        if screenWidth > 375 {
            return URL(string: upyunImageBase + "sampleImage@3x.png")!
        }else{
            return URL(string: upyunImageBase + "sampleImage@2x.png")!
        }
    }
    
    /// 启动页的背景图片
    func launchBackgroundImageURL()->URL{
        return URL(string: upyunImageBase + "launchBackground.png")!
    }
    
    /// 明信片背面TIMEORY的logo
    func timeoryLogoURL()->URL{
        return URL(string: upyunImageBase + "ucardLogo.png")!
    }
    
    /// 明信片背面邮票
    func stampURL()->URL{
        return URL(string: upyunImageBase + "stamp.png")!
    }
    
    /// 明信片背面底部的caption
    func captionURL()->URL{
        return URL(string: upyunImageBase + "timeoryCaption@3x.png")!
    }
    
    /// 发布到地图成功之后的展示的图片
    func releaseSuccessImage()->URL?{
        if languageCode() == "CN" {
            return URL(string: upyunImageBase + "releaseSuccessCN.png!/format/webp")
        }else{
            return URL(string: upyunImageBase + "releaseSuccessEN.png!/format/webp")
        }
    }
    
    /// 发布界面的介绍视图
    func releaseInfoImage()->URL?{
        if languageCode() == "CN" {
            return URL(string: upyunImageBase + "releaseInfoCN.png!/format/webp")
        }else{
            return URL(string: upyunImageBase + "releaseInfoEN.png!/format/webp")
        }
    }
    
    /// 时光轴录屏的介绍事例图片
    func timeLineRecordIntroImage()->URL?{
        if languageCode() == "CN" {
            return URL(string: upyunImageBase + "timeLineRecordIntroCN.png!/format/webp")
        }else{
            return URL(string: upyunImageBase + "timeLineRecordIntroEN.png!/format/webp")
        }
    }
    
    /// 时光轴录屏的介绍事例图片
    func timeLineIntroImage()->URL?{
        if languageCode() == "CN" {
            return URL(string: upyunImageBase + "timeLineIntroCN.png!/format/webp")
        }else{
            return URL(string: upyunImageBase + "timeLineIntroEN.png!/format/webp")
        }
    }
    
    /// 用户个人界面，底部的背景图片
    func userInfoBackground()->URL?{
        return URL(string: upyunImageBase + "userInfoBackground.png!/format/webp")
    }
    
    /// 更新弹窗的背景图片
    func updateAlertBackgroundImage()->URL?{
        if languageCode() == "CN" {
            return URL(string: upyunImageBase + "updateAlertCN.png!/format/webp")
        }else{
            return URL(string: upyunImageBase + "updateAlertEN.png!/format/webp")
        }
    }
    
    /// 推广活动的弹窗
    func activityAlertBackgroundImage()->URL?{
        if languageCode() == "CN" {
            return URL(string: upyunImageBase + "activityAlertCN.png!/format/webp")
        }else{
            return URL(string: upyunImageBase + "activityAlertEN.png!/format/webp")
        }
    }
    
    /// 优惠券图片
    let couponImageURL: URL? = URL(string: upyunImageBase + "couponImage01.png!/format/webp")
    /// 水印图片的下载地址
    let watermarkImageURL: URL? = URL(string: upyunImageBase + "watermark01.png")
    
    /// 第一张引导页
    let introImage01URL: URL? = URL(string: "http://ucardstorevideo.b0.upaiyun.com/iOSImages/TminstoreIntro01.png!/format/webp")
    /// 第二张引导页
    let introImage02URL: URL? = URL(string: "http://ucardstorevideo.b0.upaiyun.com/iOSImages/TminstoreIntro02.png!/format/webp")
    /// 第三张引导页
    let introImage03URL: URL? = URL(string: "http://ucardstorevideo.b0.upaiyun.com/iOSImages/TminstoreIntro03.png!/format/webp")
    
    /// 线索视图的背景图片
    let clueBackgroundImageURL: URL? = URL(string: upyunImageBase + "clueBackgroundImage.png!/format/webp")
    
    /// 记忆店铺浏览上传作品的入口
    let StorePreviewImageURL: URL? = URL(string: upyunImageBase + "StorePreviewImage04.png!/format/webp")
    
    /// 抱枕定制的URL
    let pillowCustomizationURL: URL? = URL(string: upyunImageBase + "storeImage/pillow02.png!/format/webp")
    
    /// 明信片定制URL
    let postcardCustomizationURL: URL? = URL(string: upyunImageBase + "storeImage/postcard.png!/format/webp")
    
    /// T恤定制URL
    let tshirtCustomizationURL: URL? = URL(string: upyunImageBase + "storeImage/TShirt.png!/format/webp")
    
    /// 马克杯定制URL
    let cupCustomizationURL: URL? = URL(string: upyunImageBase + "storeImage/cup.png!/format/webp")
    
    /// 地图介绍图片的URL
    let mapIntroImageURL: URL? = URL(string: upyunImageBase + "mapIntro02.png!/format/webp")
    
    /// 订单详情界面，展示明信片文字的图片
    let postcardOrderBackImageURL: URL? = URL(string: upyunImageBase + "PostcardOrderBackImage.png!/format/webp")
    
    /// 新版AR扫描界面，云识别介绍的图片
    let newARIntroImageURL: URL? = URL(string: upyunImageBase + "newARIntro.png!/format/webp")
    
    /// AR名片视频介绍图片
    let ARCardVideoIntroURL: URL? = URL(string: upyunImageBase + "ARCardVideoIntro.png!/format/webp")
    
    /// AR名片PPT介绍图片
    let ARCardImageIntroURL: URL? = URL(string: upyunImageBase + "ARCardPPTIntro.png!/format/webp")
    
    /// 隐私条款的网址
    ///
    /// - returns: NSURL
    func privateURL()-> URL{
        if languageCode() == "CN" {
            return URL(string: "http://ucardstorevideo.b0.upaiyun.com/AndroidFiles/private_CHN.html")!
        }else{
            return URL(string: "http://ucardstorevideo.b0.upaiyun.com/AndroidFiles/private_ENG.html")!
        }
    }
    
    
    //MARK: 优惠券说明链接
    /**
     优惠券说明链接
     
     - returns: NSURL
     */
    func couponSupportURL()->URL{
        if languageCode() == "CN" {
            return URL(string: "http://www.timeory.com/coupon/coupon_intro_CN.html")!
        }else{
            return URL(string: "http://www.timeory.com/coupon/coupon_intro_EN.html")!
        }
    }
    
    //MARK: 存放字体的地址
    /// 存放字体的地址
    ///
    /// - returns: NSURL
    func fontPath()->URL{
        let docPath: URL = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask).first!
        let filePath: URL = docPath.appendingPathComponent("FZYBXSJW--GB1-0.ttf")
        return filePath
    }
}
