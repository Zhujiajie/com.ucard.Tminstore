//
//  PopAnimation.swift
//  ZTWWiki
//
//  Created by Wenslow on 16/2/17.
//  Copyright © 2016年 Wenslow. All rights reserved.
//

import UIKit

class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration: Double = 0.5

    var originFrame = CGRect.zero
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        //where my animation take place
        let containerView: UIView = transitionContext.containerView
        
        //fetch the new view, and store it to the view
        let toView: UIView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
        
        let rootView: UIView = toView
        let initialFrame: CGRect = originFrame
        let finalFrame: CGRect = rootView.frame
        
        let xScaleFactor: CGFloat = initialFrame.width / finalFrame.width
        let yScaleFactor: CGFloat = initialFrame.height / finalFrame.height
        
        let scaleTransform: CGAffineTransform = CGAffineTransform(scaleX: xScaleFactor, y: yScaleFactor)
        
        
        rootView.transform = scaleTransform
        rootView.center = CGPoint(x: initialFrame.midX, y: initialFrame.midY)
        rootView.clipsToBounds = true
        
        containerView.addSubview(toView)
        containerView.bringSubview(toFront: rootView)
        
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.0, options: [], animations: {
            rootView.transform = CGAffineTransform.identity
            rootView.center = CGPoint(x: finalFrame.midX, y: finalFrame.midY)
            }, completion: { _ in
                transitionContext.completeTransition(true)
        })
    }
}
