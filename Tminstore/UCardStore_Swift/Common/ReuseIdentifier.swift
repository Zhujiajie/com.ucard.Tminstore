//
//  ReuseIdentifier.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import Foundation

/// 存放cell复用标签的结构体
struct ReuseIdentifier {
    
    /// 首页的cell
    let mainInfoCellIdentify: String = "MainInfoCellIdentify"
    
    /// 社区主页collectionViewCell
    let MainCollectionViewCellReuseIdentifier: String = "MainCollectionViewCellReuseIdentifier"
    
    /// 社区主页展示
    let MainImageCollectionViewCellReuseIdentifier: String = "MainImageCollectionViewCellReuseIdentifier"
    
    /// 社区主页tableView
    let MainTableViewCellReuseIdentifier: String = "MainTableViewCellReuseIdentifier"
    
    /// 社区详情界面的tableView
    let MDTableViewCellReuseIdentifier: String = "MDTableViewCellReuseIdentifier"

    /// 社区回复界面的tableView
    let MainReplyTableViewCellReuseIdentifier: String = "MainReplyTableViewCellReuseIdentifier"
    
    /// 引导页的cell
    let GuideCollectionViewCellReuseIdentifier: String = "GuideCollectionViewCellReuseIdentifier"
    
    /// 地图Annotation弹出的collectionView
    let MapAnnotationCollectionViewCellReuseIdentifier: String = "MapAnnotationCollectionViewCellReuseIdentifier"
    
    /// 地图上的collectionView
    let MapCollectionViewCellReuseIdentifier: String = "MapCollectionViewCellReuseIdentifier"
    
    /// 地图上的annotation
    let annotationReuseIndetifier: String = "annotationReuseIndetifier"
    
    /// AR本地识别的collectionViewCell
    let LocalARTimeLineCollectionViewCellReuseIdentifier: String = "LocalARTimeLineCollectionViewCell"
    
    /// 消息列表的tableView
    let MessageTableViewCellReuseIdentifier: String = "MessageTableViewCellReuseIdentifier"
    
    /// 预览视图
    let MUPreviewVCCellReuseIdentifier: String = "MUPreviewVCCellReuseIdentifier"
    
    /// 地址视图
    let MUAddressListVCCellReuseIdentifier: String = "MUAddressListVCCellReuseIdentifier"

    /// 编辑地址
    let MUEditAddressTableViewCellReuseIdentifier: String = "MUEditAddressTableViewCellReuseIdentifier"
    
    /// 展示图片的collectionView
    let MUPhotoCollectionViewCellReuseIdentifier: String = "MUPhotoCollectionViewCellReuseIdentifier"
    
    /// 优惠券活动
    let MUCouponActivityTableViewCellReuseIdentifier: String = "MUCouponActivityTableViewCellReuseIdentifier"
    
    /// 优惠券
    let MUCouponTableViewCellReuseIdentifier: String = "MUCouponTableViewCellReuseIdentifier"
    
    /// 兑换优惠券
    let MUGetCouponTableViewCellReuseIdentifier: String = "MUGetCouponTableViewCellReuseIdentifier"
    
    /// 字体collectionView
    let FontCollectionViewCellReuseIdentifier: String = "FontCollectionViewCellReuseIdentifier"
    
    /// 新发布界面内容
    let MUNewPublishTableViewContentCellReuseIdentifier: String = "MUNewPublishTableViewContentCellReuseIdentifier"
    
    /// 新发布界面位置
    let MUNewPublishTableViewLocationCellReuseIdentifier: String = "MUNewPublishTableViewLocationCellReuseIdentifier"
    
    /// 新发布界面是否发布为私人
    let MUNewPublishTableViewPrivateCellReuseIdentifier: String = "MUNewPublishTableViewPrivateCellReuseIdentifier"
    
    /// 新发布界面信息
    let MUNewPulishCollectionViewInfoCellappReuseIdentifier: String = "MUNewPulishCollectionViewInfoCellappReuseIdentifier"
    
    /// 新发布界面价格
    let MUNewPublishCollectionViewPriceCellReuseIdentifier: String = "MUNewPublishCollectionViewPriceCellReuseIdentifier"
    
    /// 草稿箱图片
    let PDraftImageCollectionViewCellappReuseIdentifier: String = "PDraftImageCollectionViewCellappReuseIdentifier"
    
    /// 草稿箱视频
    let PDraftVideoCollectionViewCellappReuseIdentifier: String = "PDraftVideoCollectionViewCellappReuseIdentifier"
    
    /// 支付界面
    let MUPayTableViewCellReuseIdentifier: String = "MUPayTableViewCellReuseIdentifier"
    
    /// 制作模块预览
    let MUPreviewTableViewCellReuseIdentifier: String = "MUPreviewTableViewCellReuseIdentifier"
    
    /// 旧发布界面
    let MUPublishCollectionViewCellReuseIdentifier: String = "MUPublishCollectionViewCellReuseIdentifier"
    
    /// 相册分类
    let AlbumTableViewCellReuseIdentifier: String = "AlbumTableViewCellReuseIdentifier"
    
    /// 旧草稿
    let PMainDraftCellReuseIdentifier: String = "PMainDraftCellReuseIdentifier"
    
    /// 展示性别
    let PGenderCellReuseidentifier: String = "PGenderCellReuseidentifier"
    
    /// 注册界面
    let PLoginCollectionViewCellReuseIdentifier: String = "PLoginCollectionViewCellReuseIdentifier"
    
    /// 个人地图
    let PMainMapCollectionViewCellappReuseIdentifier: String = "PMainMapCollectionViewCellappReuseIdentifier"
    
    /// 个人发布
    let PMainReleaseCellReuseIdentifier: String = "PMainReleaseCellReuseIdentifier"
    
    /// 个人订单
    let PMainOrderCellReuseIdentifier: String = "PMainOrderCellReuseIdentifier"
    
    /// 个人支付
    let PMainPurchaseCellReuseIdentifier: String = "PMainPurchaseCellReuseIdentifier"
    
    /// 个人地图详细，内容
    let PNewMapContentCellReuseIdentifier: String = "PNewMapContentCellReuseIdentifier"
    
    /// 个人地图详细
    let PMapCollectionViewCellReuseIdentifier: String = "PMapCollectionViewCellReuseIdentifier"
    
    /// 新个人主页
    let PNewMainTableViewCellReuseIdentifier: String = "PNewMainTableViewCellReuseIdentifier"
    
    /// 订单
    let POrderTableViewCellReuseIdentifier: String = "POrderTableViewCellReuseIdentifier"
    
    /// 订单视频
    let POrderDetailCollectionViewVideoCellReuseIdentifier: String = "POrderDetailCollectionViewVideoCellReuseIdentifier"
    
    /// 订单图片
    let POrderDetailCollectionViewImageCellReuseIdentifier: String = "POrderDetailCollectionViewImageCellReuseIdentifier"
    
    /// 订单文字
    let POrderDetailCollectionViewTextCellReuseIdentifier: String = "POrderDetailCollectionViewTextCellReuseIdentifier"
    
    /// 买图详情
    let PPurchaseTableViewCellReuseIdentifier: String = "PPurchaseTableViewCellReuseIdentifier"
    
    /// 设置个人
    let ProfileVCReuseIdentifier: String = "ProfileVCReuseIdentifier"
    
    /// 设置
    let PSettingTableCellReuseIdentifier: String = "PSettingTableCellReuseIdentifier"
    
    /// 商铺创意
    let StoreIdealCollectionViewCellReuseIdentifier: String = "StoreIdealCollectionViewCellReuseIdentifier"
    
    /// 商铺主页
    let StoreMianCollectionViewCellReuseIdentifier: String = "StoreMianCollectionViewCellReuseIdentifier"
    
    /// 商铺制作
    let StoreMakeTableViewCellReuseIdentifier: String = "StoreMakeTableViewCellReuseIdentifier"
    
    /// 系统消息
    let SystemMessageTableViewCellReuseIdentifier: String = "SystemMessageTableViewCellReuseIdentifier"
    
    /// 系统回复
    let VideoReplyTableViewCellReuseIdentifier: String = "VideoReplyTableViewCellReuseIdentifier"
    
    /// annotationView复用
    let ZTAnnotationTableViewCellReuseIdentifier: String = "ZTAnnotationTableViewCellReuseIdentifier"
    
    /// 地图详情，sectionHeader
    let PMapTableViewHeaderViewReuseIdentifer: String = "PMapTableViewHeaderViewReuseIdentifer"
    
    /// 我的时空详情界面的评论cell
    let PNewMapTableViewCellReuseIdentifer: String = "PNewMapTableViewCellReuseIdentifer"
    
    ///评论
    let PMapCommentTableViewCellappReuseIdentifier: String = "PMapCommentTableViewCellappReuseIdentifier"
    ///图片
    let PMapImageTableViewCellappReuseIdentifier: String = "PMapImageTableViewCellappReuseIdentifier"
    
    /// 用户信息界面, collectionViewCell
    let PUserSpaceTimeCollectionCellReuseIdentifier: String = "PUserSpaceTimeCollectionCellReuseIdentifier"
    
    /// 记忆店铺商品cell
    let storeGoods: String = "storeGoods"
    
    /// 记忆店铺创意图片的cell
    let storeIdeals: String = "storeIdeals"
    
    /// 记忆店铺选择创意图片或者上传图片的cell
    let myPhotoChooseCell: String = "myPhotoChooseCell"
}
