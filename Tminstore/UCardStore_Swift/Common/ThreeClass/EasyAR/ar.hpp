/**
* Copyright (c) 2015-2016 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

#ifndef __EASYAR_SAMPLE_UTILITY_AR_H__
#define __EASYAR_SAMPLE_UTILITY_AR_H__

#include "easyar/camera.hpp"
#include "easyar/imagetracker.hpp"
#include "easyar/augmenter.hpp"
#include "easyar/imagetarget.hpp"
#include "easyar/frame.hpp"
#include "easyar/player.hpp"
#include "easyar/utility.hpp"
#include "easyar/cloud.hpp"
#include "easyar/barcode.hpp"
#include <string>
#include <vector>

namespace EasyAR{
namespace samples{

class AR
{
public:
    AR();
    virtual ~AR();
    virtual bool initCamera();
    virtual void initCloud(const std::string& url, const std::string& key, const std::string& secret);
    virtual void loadFromImage(const std::string& path);
    virtual void loadFromJsonFile(const std::string& path, const std::string& targetname);
    virtual void loadAllFromJsonFile(const std::string& path);
    virtual bool start();
    virtual bool stop();
    virtual bool clear();

    //云识别改动
    virtual bool cloudStop();
    virtual bool cloudStart();
    
    virtual void initGL();
    virtual void resizeGL(int width, int height);
    virtual void render();
    void setPortrait(bool portrait);
    
protected:
    CameraDevice camera_;
    ImageTracker tracker_;
    Augmenter augmenter_;
    BarCodeScanner barcode_;
    bool portrait_;
    Vec4I viewport_;
protected:
    class CloudRecognizerInitCallBack : public EasyAR::CloudRecognizerInitCallBack
    {
    public:
        CloudRecognizerInitCallBack(ImageTracker tracker) : tracker_(tracker) {}
        virtual void operator() (CloudRecognizer::InitStatus status);
        virtual ~CloudRecognizerInitCallBack() {}
    private:
        ImageTracker tracker_;
    };

    class CloudRecognizerCallBack : public EasyAR::CloudRecognizerCallBack
    {
    public:
        CloudRecognizerCallBack(ImageTracker tracker) : tracker_(tracker) {}
        virtual void operator() (CloudRecognizer::Status status, TargetList trackables);
        virtual ~CloudRecognizerCallBack() {}
    private:
        ImageTracker tracker_;
        std::vector<std::string> uids;
    };
protected:
    CloudRecognizer cloud_;
    CloudRecognizerInitCallBack* cloud_init_callback_;
    CloudRecognizerCallBack* cloud_callback_;
};

    class ARVideo {
    public:
        ARVideo();
        ~ARVideo();
        
        void openVideoFile(const std::string& path, int texid);
        void openTransparentVideoFile(const std::string& path, int texid);
        void openStreamingVideo(const std::string& url, int texid);
        
        void setVideoStatus(VideoPlayer::Status status);
        void onFound();
        void onLost();
        void update();
        void close();
        
        class CallBack : public VideoPlayerCallBack
        {
        public:
            CallBack(ARVideo* video);
            void operator() (VideoPlayer::Status status);
        private:
            ARVideo* video_;
        };
        
        VideoPlayer player_;
        
    private:
//        VideoPlayer player_;
        bool prepared_;
        bool found_;
        CallBack* callback_;
        std::string path_;
    };
    
}
}
#endif
