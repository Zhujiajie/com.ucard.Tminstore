/**
 * Copyright (c) 2015-2016 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
 * EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
 * and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
 */

#import "LocalView.h"
//#import "AppDelegate.h"

#include <iostream>
#include "ar.hpp"
#include "renderer.hpp"

/*
 * Steps to create the key for this sample:
 *  1. login www.easyar.com
 *  2. create app with
 *      Name: HelloARLocalCloud
 *      Bundle ID: cn.easyar.samples.HelloARLocalcloud
 *  3. find the created item in the list and show key
 *  4. set key string bellow
 */

//定义函数指针
typedef void (*FoundTarget)(void *data, bool found, int index);//成功找到AR目标

namespace EasyAR{
    namespace samples{
        
        class HelloARLocal : public AR
        {
        public:
            HelloARLocal();
            ~HelloARLocal();
            
            virtual void initGL();
            virtual void resizeGL(int width, int height);
            virtual void render();
            virtual bool clear();
            
            //MARK: 加载多图片
//            int imageCount;//识别图的数量
            Vec2I view_size;
            Renderer* renderer[3];
            int tracked_target;
            int active_target;
            int texid[3];
            ARVideo* video;
            Renderer* video_renderer;
            
            void registCallBackFunction(void *, FoundTarget);//注册回调函数
            
            private :
                void *data;//实现回调的对象
                FoundTarget foundTargetHandler;//函数声明
          
        };
        
        //注册回调函数的实现
        void HelloARLocal::registCallBackFunction(void *data, FoundTarget handler)
        {
            this->data = data;
            this->foundTargetHandler = handler;
        }
        
        
        HelloARLocal::HelloARLocal()
        {
            view_size[0] = -1;
            tracked_target = 0;
            active_target = 0;
//            texid[0] = 0;
//            renderer[0] = new Renderer;
            for(int i = 0; i < 3; ++i) {
                texid[i] = 0;
                renderer[i] = new Renderer;
            }
            video = NULL;
            video_renderer = NULL;
        }
        
        HelloARLocal::~HelloARLocal()
        {
//            delete renderer[0];
            for(int i = 0; i < 3; ++i) {
                delete renderer[i];
            }
        }
        
        //MARK: 加载多图片
        void HelloARLocal::initGL()
        {
            augmenter_ = Augmenter();
            renderer[0]->init();
            texid[0] = renderer[0]->texId();
//            for(int i = 0; i < imageCount; ++i) {
//                renderer[i]->init();
//                texid[i] = renderer[i]->texId();
//            }
        }
        
        void HelloARLocal::resizeGL(int width, int height)
        {
            view_size = Vec2I(width, height);
        }
        
        void HelloARLocal::render()
        {
            glClearColor(0.f, 0.f, 0.f, 1.f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            
            Frame frame = augmenter_.newFrame();
            if(view_size[0] > 0){
                int width = view_size[0];
                int height = view_size[1];
                Vec2I size = Vec2I(1, 1);
                if (camera_ && camera_.isOpened())
                    size = camera_.size();
                if(portrait_)
                    std::swap(size[0], size[1]);
                float scaleRatio = std::max((float)width / (float)size[0], (float)height / (float)size[1]);
                Vec2I viewport_size = Vec2I((int)(size[0] * scaleRatio), (int)(size[1] * scaleRatio));
                if(portrait_)
                    viewport_ = Vec4I(0, height - viewport_size[1], viewport_size[0], viewport_size[1]);
                else
                    viewport_ = Vec4I(0, width - height, viewport_size[0], viewport_size[1]);
                if(camera_ && camera_.isOpened())
                    view_size[0] = -1;
            }
            augmenter_.setViewPort(viewport_);
            augmenter_.drawVideoBackground();
            glViewport(viewport_[0], viewport_[1], viewport_[2], viewport_[3]);
            
            //MARK: 设置多目标识别的代码
            AugmentedTarget::Status status = frame.targets()[0].status();
            if(status == AugmentedTarget::kTargetStatusTracked){
                int id = frame.targets()[0].target().id();
                if(active_target && active_target != id) {
                    video->onLost();
                    delete video;
                    video = NULL;
                    tracked_target = 0;
                    active_target = 0;
                }
                if (!tracked_target) {
                    
                    if (foundTargetHandler != NULL) {
                        int index = id - tracker_.targets()[tracker_.targets().size() - 1].id();
                        (*foundTargetHandler)(data, true, index);//找到目标
                    }//callBack
                    
                    if (video) {
                        video->onFound();
                        tracked_target = id;
                        active_target = id;
                    }
                }
                Matrix44F projectionMatrix = getProjectionGL(camera_.cameraCalibration(), 0.2f, 500.f);
                Matrix44F cameraview = getPoseGL(frame.targets()[0].pose());
                ImageTarget target = frame.targets()[0].target().cast_dynamic<ImageTarget>();
                if(tracked_target) {
                    video->update();
                    //MARK: 使AR目标尺寸适应视频尺寸
                    Vec2F newTargetSize = target.size();
                    float targeScale = newTargetSize[0]/newTargetSize[1];
                    Vec2I videoSize = video->player_.size();
                    float videoScale = float(videoSize[0])/float(videoSize[1]);
                    if (targeScale < videoScale){
                        newTargetSize[0] = float(videoSize[0])/float(videoSize[1]) * newTargetSize[1];
                    }else{
                        newTargetSize[1] = float(videoSize[1])/float(videoSize[0]) * newTargetSize[0];
                    }
                    video_renderer->render(projectionMatrix, cameraview, newTargetSize);
                }
            } else {
                if (tracked_target) {
                    video->onLost();
                    tracked_target = 0;
                }
                if (foundTargetHandler != NULL) {
                    (*foundTargetHandler)(data, false, 0);//失去目标
                }//callBack
            }
        }
        
        //MARK: 清除所有项目
        bool HelloARLocal::clear()
        {
            AR::clear();
            if(video){
                delete video;
                video = NULL;
                tracked_target = 0;
                active_target = 0;
            }
            return true;
        }
    }
}

@interface LocalView ()
{
    EasyAR::samples::HelloARLocal ar;
}

@property(nonatomic, strong) CADisplayLink * displayLink;

- (void)displayLinkCallback:(CADisplayLink*)displayLink;

@end

@implementation LocalView

//NSString* key = @"Ljq326kluYVZ1YokAUBVzhO8my72Uoo8sRxh03CBHNdiyy4eosKtD2oXUspKfthEpsk9GV6j8W6iQFlt7Pu8dtcdHwiauZ3y9a6Na442fa1bb5baa63d7e86ec99030d7667XImlss9HGR4YN7HbXHWQybRKuALfVuQoutV8IlHYDjLLR4ZRVfNPS7tVGJxmLhB17bUM";

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

+ (LocalView *)shareInstance{
    static dispatch_once_t onceToken;
    static LocalView *instance;
    dispatch_once(&onceToken,^{ instance=[[LocalView alloc] initWithFrame:[UIScreen mainScreen].bounds];});
    return instance;
}

- (id)initWithFrame:(CGRect)frame
{
//    frame.size.width = frame.size.height = MAX(frame.size.width, frame.size.height);
    self = [super initWithFrame:frame];
    if(self){
        [self setupGL];

        EasyAR::initialize([@"Ljq326kluYVZ1YokAUBVzhO8my72Uoo8sRxh03CBHNdiyy4eosKtD2oXUspKfthEpsk9GV6j8W6iQFlt7Pu8dtcdHwiauZ3y9a6Na442fa1bb5baa63d7e86ec99030d7667XImlss9HGR4YN7HbXHWQybRKuALfVuQoutV8IlHYDjLLR4ZRVfNPS7tVGJxmLhB17bUM" UTF8String]);
        ar.initGL();
        ar.registCallBackFunction((__bridge void *)self, callBack);
    }

    return self;
}

- (void)dealloc
{
    ar.clear();
}

- (void)setupGL
{
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;

    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
    _context = [[EAGLContext alloc] initWithAPI:api];
    if (!_context)
        NSLog(@"Failed to initialize OpenGLES 2.0 context");
    if (![EAGLContext setCurrentContext:_context])
        NSLog(@"Failed to set current OpenGL context");

    GLuint frameBuffer;
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);

    int width, height;
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);

    GLuint depthRenderBuffer;
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
}

//MARK: 开始识别
- (void)start{
    self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkCallback:)];
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

//MARK: 清除AR识别
- (void)clear{
    self.active = NO;
    ar.clear();
    [self.displayLink invalidate];
}

- (void)stop
{
    self.active = NO;
    ar.stop();
    [self.displayLink invalidate];
}

- (void)displayLinkCallback:(CADisplayLink*)displayLink
{
    ar.render();
    (void)displayLink;
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)resize:(CGRect)frame orientation:(UIInterfaceOrientation)orientation
{
    BOOL isPortrait = FALSE;
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            isPortrait = TRUE;
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            isPortrait = FALSE;
            break;
        default:
            break;
    }
    ar.setPortrait(isPortrait);
    ar.resizeGL(frame.size.width, frame.size.height);
}

- (void)setOrientation:(UIInterfaceOrientation)orientation
{
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
            EasyAR::setRotationIOS(270);
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            EasyAR::setRotationIOS(90);
            break;
        case UIInterfaceOrientationLandscapeLeft:
            EasyAR::setRotationIOS(180);
            break;
        case UIInterfaceOrientationLandscapeRight:
            EasyAR::setRotationIOS(0);
            break;
        default:
            break;
    }
}

- (void)setActive:(bool)active{
    _active = active;
    if (self.active) {
        EasyAR::onResume();
    }else{
        EasyAR::onPause();
    }
}

////MARK: 设置识别图的数量
//- (void)setImageCount:(int)imageCount{
//    _imageCount = imageCount;
//    ar.imageCount = _imageCount;
//}

//MARK: 切换视频的播放地址
- (void)setVideoURL: (NSString *)urlString{
    
    if (ar.video) {
        ar.video->onLost();
        delete ar.video;
        ar.video = NULL;
    }
    
    ar.tracked_target = 0;
    ar.active_target = 0;
    char *videoCURL = (char *)[urlString UTF8String];
    ar.video = new EasyAR::samples::ARVideo;
    ar.video->openStreamingVideo(videoCURL, ar.texid[0]);
    ar.video_renderer = ar.renderer[0];
}

//MARK: 实现C++回调
void callBack(void *data, bool found, int index){
    LocalView *glView = (__bridge LocalView *)data;
    glView.foundTarget(found, index);
}

//MARK: 加载本地的视频
- (void)loadVideo: (NSString *)videoPath{
    
//    int videoPositon = 0;
    
    if (ar.video) {
//        videoPositon =ar.video->player_.currentPosition();//获取当前视频的播放位置
//        ar.video->onLost();
//        delete ar.video;
//        ar.video = NULL;
        return; 
    }
    
    ar.tracked_target = 0;
    ar.active_target = 0;
    char *videoCURL = (char *)[videoPath cStringUsingEncoding:NSUTF8StringEncoding];
    ar.video = new EasyAR::samples::ARVideo;
    ar.video->openVideoFile(videoCURL, ar.texid[0]);
    ar.video_renderer = ar.renderer[0];
}


/**
 加载AR识别图

 @param imagePath 图片地址
 */
- (void)loadImage: (NSString *)imagePath{
    ar.loadFromImage([imagePath cStringUsingEncoding:NSUTF8StringEncoding]);
}

//MARK: 开始AR识别
/**
 加载我识别图片之后，开始AR
 */
- (void)startAR{
    self.active = true;
    ar.initCamera();
    ar.start();
    [self start];
}

@end
