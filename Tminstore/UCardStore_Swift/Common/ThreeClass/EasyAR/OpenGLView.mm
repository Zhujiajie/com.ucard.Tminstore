/**
 * Copyright (c) 2015-2016 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
 * EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
 * and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
 */

#import "OpenGLView.h"
//#import "AppDelegate.h"

#include <iostream>
#include "ar.hpp"
#include "renderer.hpp"

/*
 * Steps to create the key for this sample:
 *  1. login www.easyar.com
 *  2. create app with
 *      Name: HelloARCloud
 *      Bundle ID: cn.easyar.samples.helloarcloud
 *  3. find the created item in the list and show key
 *  4. set key string bellow
 */
NSString* key = @"Ljq326kluYVZ1YokAUBVzhO8my72Uoo8sRxh03CBHNdiyy4eosKtD2oXUspKfthEpsk9GV6j8W6iQFlt7Pu8dtcdHwiauZ3y9a6Na442fa1bb5baa63d7e86ec99030d7667XImlss9HGR4YN7HbXHWQybRKuALfVuQoutV8IlHYDjLLR4ZRVfNPS7tVGJxmLhB17bUM";
NSString* cloud_url = @"oi43.easyar.com:8080";
NSString* cloud_key = @"4e056078f43dbfe73a150bc5e50548ac98d035330f10d6ec164be8fcd0c70c36";
NSString* cloud_secret = @"0e8e9cc8f67675db077c9740557b4abcfc4ae5913b62a2585dc072d1637574c8189f6f519003dc675de1f5d81def542eb1b8d003fb9432ad61feac23d68c23e0";

/* 又拍云url */
NSString *upurl = @"http://ucardstorevideo.b0.upaiyun.com/video/";

//定义函数指针
typedef void (*FoundTarget)(void *data, const char *videoID, const char *videoURLString);//成功找到AR目标

namespace EasyAR{
    namespace samples{
        
        class HelloAR : public AR
        {
        public:
            HelloAR();
            ~HelloAR();
            virtual void initGL();
            virtual void resizeGL(int width, int height);
            virtual void render();
            virtual bool clear();
            
            Vec2I view_size;
            Renderer* renderer[3];
            int tracked_target;
            int active_target;
            int texid[3];
            ARVideo* video;
            Renderer* video_renderer;
            
            bool cloudStopped;
            
            void registCallBackFunction(void *, FoundTarget);//注册回调函数
            
            private :
                void *data;//实现回调的对象
                FoundTarget foundTargetHandler;//函数声明
        };
        
        //注册回调函数的实现
        void HelloAR::registCallBackFunction(void *data, FoundTarget handler)
        {
            this->data = data;
            this->foundTargetHandler = handler;
        }
        
        
        HelloAR::HelloAR()
        {
            view_size[0] = -1;
            tracked_target = 0;
            active_target = 0;
            for(int i = 0; i < 3; ++i) {
                texid[i] = 0;
                renderer[i] = new Renderer;
            }
            video = NULL;
            video_renderer = NULL;
        }
        
        HelloAR::~HelloAR()
        {
            for(int i = 0; i < 3; ++i) {
                delete renderer[i];
            }
        }
        
        
        void HelloAR::initGL()
        {
            augmenter_ = Augmenter();
            for(int i = 0; i < 3; ++i) {
                renderer[i]->init();
                texid[i] = renderer[i]->texId();
            }
        }
        
        void HelloAR::resizeGL(int width, int height)
        {
            view_size = Vec2I(width, height);
        }
        
        void HelloAR::render()
        {
            glClearColor(0.f, 0.f, 0.f, 1.f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            
            Frame frame = augmenter_.newFrame();
            if(view_size[0] > 0){
                int width = view_size[0];
                int height = view_size[1];
                Vec2I size = Vec2I(1, 1);
                if (camera_ && camera_.isOpened())
                    size = camera_.size();
                if(portrait_)
                    std::swap(size[0], size[1]);
                float scaleRatio = std::max((float)width / (float)size[0], (float)height / (float)size[1]);
                Vec2I viewport_size = Vec2I((int)(size[0] * scaleRatio), (int)(size[1] * scaleRatio));
                if(portrait_)
                    viewport_ = Vec4I(0, height - viewport_size[1], viewport_size[0], viewport_size[1]);
                else
                    viewport_ = Vec4I(0, width - height, viewport_size[0], viewport_size[1]);
                if(camera_ && camera_.isOpened())
                    view_size[0] = -1;
            }
            augmenter_.setViewPort(viewport_);
            augmenter_.drawVideoBackground();
            glViewport(viewport_[0], viewport_[1], viewport_[2], viewport_[3]);
            
            AugmentedTarget::Status status = frame.targets()[0].status();
            if(status == AugmentedTarget::kTargetStatusTracked){
                int id = frame.targets()[0].target().id();
                if(active_target && active_target != id) {
                    video->onLost();
                    delete video;
                    video = NULL;
                    tracked_target = 0;
                    active_target = 0;
                }
                if (!tracked_target) {
                    
                    if (cloudStopped == false && video == NULL) {
                        
                        char *videoID;//视频的ID
                        char *videoURLString;//视频的路径
                        //拼接视频下载地址
                        NSString *base64Meta = [NSString stringWithCString:frame.targets()[0].target().metaData() encoding:NSUTF8StringEncoding];
                        if (![base64Meta isEqualToString:@""]) {
                            NSData *metaData = [[NSData alloc]initWithBase64EncodedString:base64Meta options:NSDataBase64DecodingIgnoreUnknownCharacters];
                            //解析base64Meta是否失败
                            if (metaData != nil) {
                                NSString *videoURLStr = [[NSString alloc]initWithData:metaData encoding:NSUTF8StringEncoding];
                                videoURLString = (char *)[videoURLStr UTF8String];
                                NSString *videoName = [videoURLStr stringByReplacingOccurrencesOfString:upurl withString:@""];
                                videoID = (char *)[[videoName stringByReplacingOccurrencesOfString:@".mp4" withString:@""] UTF8String];
                            }else{
                                
                                NSString *videoUID = [NSString stringWithCString:frame.targets()[0].target().name() encoding:NSUTF8StringEncoding];
                                videoID = (char *)[videoUID UTF8String];
                            }
                            
                        }else{
                            NSString *videoUID = [NSString stringWithCString:frame.targets()[0].target().name() encoding:NSUTF8StringEncoding];
                            videoID = (char *)[videoUID UTF8String];
                        }
                        
                        if (foundTargetHandler != NULL) {
                            (*foundTargetHandler)(data, videoID, videoURLString);//传递视频ID
                        }//callBack
                        cloudStopped = true;
                        AR::cloudStop();//成功加载视频，停止云识别
                    }
                    if (video) {
                        video->onFound();
                        tracked_target = id;
                        active_target = id;
                        AR::cloudStop();//成功加载视频，停止云识别
                    }
                }
                Matrix44F projectionMatrix = getProjectionGL(camera_.cameraCalibration(), 0.2f, 500.f);
                Matrix44F cameraview = getPoseGL(frame.targets()[0].pose());
                ImageTarget target = frame.targets()[0].target().cast_dynamic<ImageTarget>();
                if(tracked_target) {
                    video->update();
                    //MARK: 使AR目标尺寸适应视频尺寸
                    Vec2F newTargetSize = target.size();
                    float targeScale = newTargetSize[0]/newTargetSize[1];
                    Vec2I videoSize = video->player_.size();
                    float videoScale = float(videoSize[0])/float(videoSize[1]);
                    if (targeScale < videoScale){
                        newTargetSize[0] = float(videoSize[0])/float(videoSize[1]) * newTargetSize[1];
                    }else{
                        newTargetSize[1] = float(videoSize[1])/float(videoSize[0]) * newTargetSize[0];
                    }
                    video_renderer->render(projectionMatrix, cameraview, newTargetSize);
                }
            } else {
                if (tracked_target) {
                    video->onLost();
                    tracked_target = 0;
                    AR::cloudStart();//失去目标，开始云识别
                }
                if (video != NULL) {
                    cloudStopped = false;
                }
            }
            
            //MARK: 二维码功能
//            static int barcode_index = 0;
//            if (frame.index() != barcode_index) {
//                barcode_index = frame.index();
//                std::string text = frame.text();
//                if (!text.empty()) {
//                    NSString *codeString = [NSString stringWithCString:text.c_str() encoding:NSUTF8StringEncoding];
//                    NSLog(@"text->>>%@", codeString);
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"getQRCode" object:nil userInfo:@{@"code": codeString}];
//                }
//            }
            
//            for (int i = 0; i < frame.targets().size(); ++i) {
//                AugmentedTarget::Status status = frame.targets()[i].status();
//                if(status == AugmentedTarget::kTargetStatusTracked){
//                    int id = frame.targets()[0].target().id();
//                    if(active_target && active_target != id) {
//                        video->onLost();
//                        delete video;
//                        video = NULL;
//                        tracked_target = 0;
//                        active_target = 0;
//                    }
//                    if (!tracked_target) {
//                        if (video == NULL) {
//                            //拼接视频下载地址
//                            NSString *videoUID = [NSString stringWithCString:frame.targets()[0].target().name() encoding:NSUTF8StringEncoding];
//                            NSString *videoURL = [NSString stringWithFormat:@"%@%@.mp4", upurl, videoUID];
//                            char *videoCURL = (char *)[videoURL UTF8String];
//                            video = new ARVideo;
//                            //                    video->openVideoFile("video.mp4", texid[0]);
//                            video->openStreamingVideo(videoCURL, texid[0]);
//                            video_renderer = renderer[0];
//                        }
//                        if (video) {
//                            video->onFound();
//                            tracked_target = id;
//                            active_target = id;
//                        }
//                    }
//                    Matrix44F projectionMatrix = getProjectionGL(camera_.cameraCalibration(), 0.2f, 500.f);
//                    Matrix44F cameraview = getPoseGL(frame.targets()[0].pose());
//                    ImageTarget target = frame.targets()[0].target().cast_dynamic<ImageTarget>();
//                    if(tracked_target) {
//                        video->update();
//                        video_renderer->render(projectionMatrix, cameraview, target.size());
//                    }
//                } else {
//                    if (tracked_target) {
//                        video->onLost();
//                        tracked_target = 0;
//                    }
//                }
            
//            }
        }
        
        //MARK: 清除所有项目
        bool HelloAR::clear()
        {
            augmenter_.newFrame().targets().~AugmentedTargetList();
            AR::clear();
            if(video){
                delete video;
                video = NULL;
                tracked_target = 0;
                active_target = 0;
            }
            return true;
        }
    }
}

@interface OpenGLView ()
{
    EasyAR::samples::HelloAR ar;
}

@property(nonatomic, strong) CADisplayLink * displayLink;

- (void)displayLinkCallback:(CADisplayLink*)displayLink;

@end

@implementation OpenGLView

+ (Class)layerClass
{
    return [CAEAGLLayer class];
}

+ (OpenGLView *)shareInstance{
    static dispatch_once_t onceToken;
    static OpenGLView *instance;
    dispatch_once(&onceToken,^{ instance=[[OpenGLView alloc] initWithFrame:[UIScreen mainScreen].bounds];});
    return instance;
}

- (id)initWithFrame:(CGRect)frame
{
//    frame.size.width = frame.size.height = MAX(frame.size.width, frame.size.height);
    self = [super initWithFrame:frame];
    if(self){
        [self setupGL];

        EasyAR::initialize([key UTF8String]);
        ar.initGL();
        ar.registCallBackFunction((__bridge void *)self, callBack);
    }

    return self;
}

- (void)dealloc
{
    ar.clear();
}

- (void)setupGL
{
    _eaglLayer = (CAEAGLLayer*) self.layer;
    _eaglLayer.opaque = YES;

    EAGLRenderingAPI api = kEAGLRenderingAPIOpenGLES2;
    _context = [[EAGLContext alloc] initWithAPI:api];
    if (!_context)
        NSLog(@"Failed to initialize OpenGLES 2.0 context");
    if (![EAGLContext setCurrentContext:_context])
        NSLog(@"Failed to set current OpenGL context");

    GLuint frameBuffer;
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

    glGenRenderbuffers(1, &_colorRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context renderbufferStorage:GL_RENDERBUFFER fromDrawable:_eaglLayer];
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, _colorRenderBuffer);

    int width, height;
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);

    GLuint depthRenderBuffer;
    glGenRenderbuffers(1, &depthRenderBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);
}

//MARK: 开始云识别
- (void)start{
    self.active = YES;
    ar.initCamera();
    //MARK: 从云端加载
    ar.initCloud([cloud_url UTF8String], [cloud_key UTF8String], [cloud_secret UTF8String]);
    ar.start();
    ar.cloudStopped = false;//解决扫描出错的问题
    self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(displayLinkCallback:)];
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}

//MARK: 清除AR识别
- (void)clear{
    self.active = NO;
    ar.clear();
    [self.displayLink invalidate];
}

- (void)stop
{
    self.active = NO;
    ar.stop();
    [self.displayLink invalidate];
}

- (void)displayLinkCallback:(CADisplayLink*)displayLink
{
    ar.render();
    (void)displayLink;
    glBindRenderbuffer(GL_RENDERBUFFER, _colorRenderBuffer);
    [_context presentRenderbuffer:GL_RENDERBUFFER];
}

- (void)resize:(CGRect)frame orientation:(UIInterfaceOrientation)orientation
{
    BOOL isPortrait = FALSE;
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
            isPortrait = TRUE;
            break;
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight:
            isPortrait = FALSE;
            break;
        default:
            break;
    }
    ar.setPortrait(isPortrait);
    ar.resizeGL(frame.size.width, frame.size.height);
}

- (void)setOrientation:(UIInterfaceOrientation)orientation
{
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
            EasyAR::setRotationIOS(270);
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            EasyAR::setRotationIOS(90);
            break;
        case UIInterfaceOrientationLandscapeLeft:
            EasyAR::setRotationIOS(180);
            break;
        case UIInterfaceOrientationLandscapeRight:
            EasyAR::setRotationIOS(0);
            break;
        default:
            break;
    }
}

- (void)setActive:(bool)active{
    _active = active;
    if (self.active) {
        EasyAR::onResume();
    }else{
        EasyAR::onPause();
    }
}

- (void)startCloud{
    ar.cloudStart();
}

- (void)stopCloud{
    ar.cloudStop();
}

//MARK: 切换视频的播放地址
- (void)setVideoURL: (NSString *)urlString{
    
    if (ar.video) {
        ar.video->onLost();
        delete ar.video;
        ar.video = NULL;
    }
    
    ar.tracked_target = 0;
    ar.active_target = 0;
    char *videoCURL = (char *)[urlString UTF8String];
    ar.video = new EasyAR::samples::ARVideo;
    ar.video->openStreamingVideo(videoCURL, ar.texid[0]);
    ar.video_renderer = ar.renderer[0];
    ar.cloudStop();
}

//MARK: 实现C++回调
void callBack(void *data, const char *videoID, const char *videoURLString){
    NSString *videoIDStr = [NSString stringWithCString:videoID encoding:NSUTF8StringEncoding];
    NSString *newVideoURLString = [NSString stringWithCString:videoURLString encoding:NSUTF8StringEncoding];
    OpenGLView *glView = (__bridge OpenGLView *)data;
    glView.foundTarget(videoIDStr, newVideoURLString);
//    [OpenGLView shareInstance].foundTarget(videoIDStr);//闭包传值
}

//MARK: 加载本地的视频
- (void)loadVideo: (NSString *)videoPath{
    
//    int videoPositon = 0;
    
    if (ar.video) {
//        videoPositon =ar.video->player_.currentPosition();//获取当前视频的播放位置
//        ar.video->onLost();
//        delete ar.video;
//        ar.video = NULL;
        return; 
    }
    
    ar.tracked_target = 0;
    ar.active_target = 0;
    char *videoCURL = (char *)[videoPath cStringUsingEncoding:NSUTF8StringEncoding];
    ar.video = new EasyAR::samples::ARVideo;
    ar.video->openVideoFile(videoCURL, ar.texid[0]);
    ar.video_renderer = ar.renderer[0];
//    if (videoPositon != 0){
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            ar.video->player_.seek(videoPositon);//载入指定视频的时间
//        });
//    }
}

@end
