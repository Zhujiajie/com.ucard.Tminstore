/**
* Copyright (c) 2015-2016 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>



@interface OpenGLView : UIView

@property(nonatomic, strong) CAEAGLLayer * eaglLayer;
@property(nonatomic, strong) EAGLContext *context;
@property(nonatomic) GLuint colorRenderBuffer;
/* app是否处于活动状态 */
@property(nonatomic) bool active;
@property(nonatomic) bool cloudInitSuccess;
// 识别到目标，传递视频ID
@property (nonatomic, strong) void(^foundTarget) (NSString *videoID, NSString *videoURLString);

+ (OpenGLView *)shareInstance;

- (void)start;
- (void)stop;
- (void)clear;
- (void)resize:(CGRect)frame orientation:(UIInterfaceOrientation)orientation;
- (void)setOrientation:(UIInterfaceOrientation)orientation;

- (void)startCloud;
- (void)stopCloud;

/**
 切换视频播放地址

 @param urlString 视频地址
 */
- (void)setVideoURL: (NSString *)urlString;

/**
 加载本地的视频

 @param videoPath 视频路径
 */
- (void)loadVideo: (NSString *)videoPath;
@end
