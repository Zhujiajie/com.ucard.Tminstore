/**
* Copyright (c) 2015-2016 VisionStar Information Technology (Shanghai) Co., Ltd. All Rights Reserved.
* EasyAR is the registered trademark or trademark of VisionStar Information Technology (Shanghai) Co., Ltd in China
* and other countries for the augmented reality technology developed by VisionStar Information Technology (Shanghai) Co., Ltd.
*/

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>



@interface LocalView : UIView

@property(nonatomic, strong) CAEAGLLayer * eaglLayer;
@property(nonatomic, strong) EAGLContext *context;
@property(nonatomic) GLuint colorRenderBuffer;
/* app是否处于活动状态 */
@property(nonatomic) bool active;
// 识别到目标，传递视频ID
@property (nonatomic, strong) void(^foundTarget) (bool found, int index);
/// 识别图的数量
//@property (nonatomic, assign) int imageCount;

//- (void)start;
- (void)stop;
- (void)clear;
- (void)resize:(CGRect)frame orientation:(UIInterfaceOrientation)orientation;
- (void)setOrientation:(UIInterfaceOrientation)orientation;
/**
 加载我识别图片之后，开始AR
 */
- (void)startAR;
/**
 切换视频播放地址

 @param urlString 视频地址
 */
- (void)setVideoURL: (NSString *)urlString;

/**
 加载本地的视频

 @param videoPath 视频路径
 */
- (void)loadVideo: (NSString *)videoPath;


/**
 加载本地识别图片

 @param imagePath 图片的路径
 */
- (void)loadImage: (NSString *)imagePath;
@end
