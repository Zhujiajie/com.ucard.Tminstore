//
//  UIImage+fixOrientation.h
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/24.
//  Copyright © 2016年 ucard. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

- (UIImage *)imageAtRect:(CGRect)rect;
- (UIImage *)imageByScalingProportionallyToMinimumSize:(CGSize)targetSize;
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
- (UIImage *)imageByScalingToSize:(CGSize)targetSize;
- (UIImage *)imageRotatedByRadians:(CGFloat)radians;
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;

/**
 添加水印

 @param logoImage 水印图片
 @param watemarkText 水印文字
 @return 加上水印的图片
 */
- (UIImage *)addWatemarkTextAfteriOS7_WithLogoImage:(UIImage *)logoImage watemarkText:(NSString *)watemarkText;

/**
 添加半透明水印
 
 @param logoImage 需要加水印的原图
 @param translucentWatemarkImage 半透明的水印图片
 @param translucentWatemarkImageRect 添加水印图片的位置
 @return 添加完水印的图片
 */
+ (UIImage *)addWatemarkImageWithLogoImage:(UIImage *)logoImage
                  translucentWatemarkImage:(UIImage *)translucentWatemarkImage
              translucentWatemarkImageRect:(CGRect)translucentWatemarkImageRect;

/**
 按固定的宽度来压缩图片

 @param sourceImage 原图片
 @param i_width 设定的宽度
 @return 生成的新图片
 */
+ (UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width;

@end
