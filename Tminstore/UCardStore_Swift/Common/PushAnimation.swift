//
//  PushAnimation.swift
//  NameReaction
//
//  Created by Wenslow on 16/4/27.
//  Copyright © 2016年 TingZhu. All rights reserved.
//

//MARK:-------------重写presentViewController的转场动画----------------

import UIKit

class PushAnimation: NSObject, UIViewControllerAnimatedTransitioning {

    var duration = 0.3
    var presenting = true
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView
        
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)
        
        if !presenting {
            //添加阴影
            fromView?.layer.shadowColor = UIColor.black.cgColor
            fromView?.layer.shadowRadius = 10
            fromView?.layer.shadowOpacity = 0.8
        }
        
        containerView.addSubview(toView!)
        
        presenting ? containerView.bringSubview(toFront: toView!) : containerView.sendSubview(toBack: toView!)
        
        toView?.frame = CGRect(x: (presenting ? screenWidth : (-160)), y: 0, width: screenWidth, height: screenHeight)
        
        UIView.animate(withDuration: duration, delay: 0.0, options: [.curveEaseOut], animations: {
            
            fromView?.frame.origin.x = self.presenting ? -160 : screenWidth
            toView?.frame.origin.x = 0
            
            }) { (_) in
                
                if transitionContext.transitionWasCancelled{
                    toView?.frame.origin.x = 0
                    fromView?.frame.origin.x = 0
                }else{
                    
                    fromView?.removeFromSuperview()
                    fromView?.frame.origin.x = self.presenting ? -160 : screenWidth
                    toView?.frame.origin.x = 0
                }
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
