//
//  ZTLoadingAnimation.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/10/25.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class ZTLoadingAnimation: UIView {

    var logoView: UIImageView?
    var ring0View: UIImageView?
    var ring1View: UIImageView?
    var ring2View: UIImageView?
    var ring3View: UIImageView?
    
    var visualView: UIVisualEffectView?
    
    static let sharedInstance = ZTLoadingAnimation()
    
    fileprivate override init(frame: CGRect){
        super.init(frame: frame)
    }
    
    /// 加载动画
    ///
    /// - parameter animatorType: 动画类型
    /// - parameter interactable: 是否支持交互
    class func show(animatorType: AnimatorType, isInteractable interactable: Bool){
        
        OperationQueue.main.addOperation {
            sharedInstance.backgroundColor = UIColor.clear
            sharedInstance.setUI(animatorType: animatorType, isInteractable: interactable)
            app.window?.addSubview(sharedInstance)
            sharedInstance.beginAnimation()
        }
    }
    
    /// 停止动画
    class func dismiss(){
        
        if sharedInstance.logoView != nil {
            sharedInstance.logoView?.layer.removeAllAnimations()
            sharedInstance.logoView?.removeFromSuperview()
        }
        if sharedInstance.visualView != nil {
            sharedInstance.visualView?.removeFromSuperview()
        }
        sharedInstance.removeFromSuperview()
    }
    
    fileprivate func setUI(animatorType: AnimatorType, isInteractable interactable: Bool){
        
        if interactable == true {
            self.frame = CGRect(x: 0, y: 0, width: 102, height: 102)
            self.center = app.window!.center
        }else{
            self.frame = UIScreen.main.bounds
        }
        
        setImageView()//设置logo
        
        if animatorType == .none {
            self.addSubview(logoView!)
            logoView?.center = self.center
        }else{
            setVisualView()
            visualView?.contentView.addSubview(logoView!)
            logoView?.center = visualView!.center
            self.addSubview(visualView!)
            visualView?.center = self.center
        }
    }
    
    /// 设置logo
    fileprivate func setImageView() {
        logoView = UIImageView(image: UIImage(named: "appLogo"))
        logoView?.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
    }
    
    /// 设置背景高斯模糊
    fileprivate func setVisualView(){
        let blur = UIBlurEffect(style: .dark)
        visualView = UIVisualEffectView(effect: blur)
        visualView?.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        visualView?.layer.cornerRadius = 10
        visualView?.layer.masksToBounds = true
    }
    
    /// logo的动画
    fileprivate func beginAnimation(){
        
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = -Double.pi * 2
        rotateAnimation.duration = 3.0
        rotateAnimation.repeatCount = MAXFLOAT
        
        logoView?.layer.add(rotateAnimation, forKey: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
