//
//  Interactive.swift
//  NameReaction
//
//  Created by Wenslow on 16/4/27.
//  Copyright © 2016年 TingZhu. All rights reserved.
//

//MARK:-------------使presentViewController的转场动画支持手势交互----------------

import UIKit

class Interactive: UIPercentDrivenInteractiveTransition {

    var isInteracting = false
    var shouldCompleteTransition = false
    
    var viewController: UIViewController?
    var pan: UIScreenEdgePanGestureRecognizer?
    
    //为presentedView增加手势交互
    func addGesture(_ viewController: UIViewController) {
        self.viewController = viewController
        
        pan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
        pan?.edges = .left
        
        viewController.view.addGestureRecognizer(pan!)
    }
    
    func handleGesture(_ pan: UIScreenEdgePanGestureRecognizer) {
        
        let width = UIScreen.main.bounds.width
        let height = UIScreen.main.bounds.height
        
        if width > height{
            return
        }

        let translation = pan.translation(in: pan.view?.superview!)
        
        switch pan.state {
        case .began:
            
            isInteracting = true
            viewController?.dismiss(animated: true, completion: nil)
            
        case .changed:
            
            if isInteracting {
                var fraction = abs(translation.x/(450/baseWidth))
                fraction = min(max(fraction, 0.01), 0.99)
                shouldCompleteTransition = fraction > 0.5
                update(fraction)
            }
        default:
            
            if isInteracting {
                
                isInteracting = false
                if !shouldCompleteTransition || pan.state == .cancelled {
                    cancel()
                }else{
                    finish()
                }
            }
        }
    }
    
    deinit{
        pan?.view?.removeGestureRecognizer(pan!)
    }
}
