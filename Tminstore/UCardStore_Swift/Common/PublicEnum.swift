//
//  publicEnum.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import Foundation

/// 支付结果标题的类型
///
/// - number: 订单编号
/// - method: 支付方式
/// - orderTime: 支付时间
enum PayTitleStyle: String {
    case number
    case method
    case orderTime
}

/// 预览界面cell的类型
///
/// - address: 展示地址的
/// - originPrice: 展示原价的
/// - coupon: 展示优惠券折扣
/// - price: 展示需要支付的价格
enum PreviewCellStyle: String {
    case address = "address"
    case originPrice = "originPrice"
    case coupon = "coupon"
    case price = "price"
}


/// 图片Cell类型
///
/// - allPhotos: 所有图片
/// - smartAlbums: 智能相册
/// - userCollections: 用户相册
enum CellIdentifier: String {
    case allPhotos = "allPhotos"
    case smartAlbums = "smartAlbums"
    case userCollections = "userCollections"
}

/// 订单的状态
///
/// - unPaied: 未付款
/// - paid: 已付款
/// - send: 已发货
/// - received: 已签收
enum OrderStaus: String {
    /// 未付款
    case unPaied = "00"
    /// 已付款
    case paid = "01"
    /// 已发货
    case send = "02"
    /// 已签收
    case received = "03"
}

/// 支付方式
/// 支付方式
///
/// - payPal: payPal
/// - weChat: 微信支付
/// - aliPay: 支付宝支付
/// - coupon: 优惠券全免支付
enum PayStyle: String {
    case balance = "05"
    case unknown = "04"
    case payPal  = "03"
    case weChat  = "02"
    case aliPay  = "01"
    case coupon  = "00"
}

/// 订单详情界面的cell的复用标签
///
/// - arCode:"arCode": 展示地址和AR密码的cell
/// - price:"price": 展示价格的cell
/// - number:"number": 展示订单号的cell
enum PDetailCellReuseIdentifier: String {
    case arCode = "arCode"
    case address = "address"
    case number = "number"
}

/// 个人界面按钮的类型
enum ProfileButtonType: Int {
    case draft = 0
    case record = 1
    case wallet = 2
    case purchase = 3
    case community = 4
    case map = 5
    case order = 6
}

/// 在个人界面显示更多的视图的按钮的tag
///
/// - coupon: 优惠券
/// - point: 积分
/// - purchase: 买图
enum PMainMoreViewButtonTag: Int {
    case coupon = 0
    case point = 1
    case purchase = 2
}

/// 明信片审核状态
///
/// - waitingForReview: 等待审核
/// - reviewed: 审核通过
/// - reviewFailed: 审核失败
enum ReviewStatus: String {
    case waitingForReview = "00"
    case reviewed         = "01"
    case reviewFailed     = "02"
}

/// 客户类型
///
/// - Undefined: 为定义
/// - Bussines: 商户
/// - Customer: 个人客户
enum CustomerType: String {
    case undefined = "A"
    case bussines = "B"
    case customer = "C"
}

/// 账户类型
///
/// - nomal: 普通用户
/// - business: 企业用户
enum UserType: String{
    case nomal = "02"
    case business = "03"
}

/// 时光圈内容的状态
///
/// - delete: 已删除
/// - normal: 正常
/// - cancel: 已被取消
enum SpaceTimeStatus: String {
    case delete = "00"
    case normal = "01"
    case cancel = "02"
}

/// 用户性别
///
/// - male: 男性 ->>> 01
/// - female: 女性 ->>> 02
/// - undefine: 未设置 ->>> 03
enum UserGender: String{
    case male     = "01"
    case female   = "02"
    case undefine = "03"
}

/// 用户状态
///
/// - unacticed: 未激活
/// - actived: 已激活
enum UserStatus: String{
    case unacticed = "00"
    case actived   = "01"
}

/// 分享按钮的类型
///
/// - weChatSpace: 微信朋友圈
/// - weChatFriend: 微信好友
/// - WeiBo: 微博
enum ShareButtonType: Int {
    case weChatSpace = 0
    case weChatFriend = 1
    case weiBo = 2
    case more = 3
}

/// 是否显示高斯模糊背景
///
/// - None:   不显示
/// - Visual: 显示
enum AnimatorType {
    case none
    case visual
}

/// 需要展示的数据类型
///
/// - comment: 评论数据
/// - memory: 记忆数据
/// - like: 点赞
enum DetailMapDataType{
    case comment
    case memory
    case like
}


/// 用户信息界面的按钮类型
///
/// - spaceTime: 时光圈
/// - record: 录屏
/// - follow: 关注
/// - fans: 粉丝
enum UserInfoButtonType {
    case spaceTime
    case record
    case follow
    case fans
}

//MARK: 用户好友, 关注和粉丝
/// 用户好友, 关注和粉丝
///
/// - friends: 好友
/// - follows: 关注
/// - fans: 粉丝
enum UserOtherInfoType {
    case friends
    case follows
    case fans
}


/// 交易类型
///
/// - imageIncome: 图片收入
enum BusinessType: String {
    case imageIncome = "00"
}
