//
//  UserInfoModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/17.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: -----------------用户个人信息的模型------------------
import UIKit

/// 用户信息模型
class UserInfoModel: NSObject {

    /// 用户id
    var memberCode: String = ""
    /// 注册名
    var loginName: String = ""
    /// 用户昵称
    var nickName: String = ""
    /// 用户手机号码
    var phoneNumber: String = ""
    /// 性别
    var gender: UserGender = UserGender.undefine
    /// 用户的邮箱地址m    
    var email: String = ""
    /// 真实姓名
    var firstName: String = ""
    /// 真实姓名
    var lastName: String = ""
    /// 生日
    var birthDate: Date?
    /// 在第三方的id
    var thirdId: String = ""
    /// 极光ID
    var jpushId: String = ""
    /// 用户标记
    var remark: String = ""
    /// 用户状态
    var status: UserStatus = UserStatus.unacticed
    /// 用户头像
    var userLogoURL: URL?
    /// 是否是内测用户
    var isTester: Bool = false
    
    /// 是否已关注
    var isFollowed: Bool = false
    /// 是否是粉丝
    var isFans: Bool = false
    
    /// 好友数量
    var friendNumber: Int = 0
    /// 粉丝数量
    var fansNumber: Int = 0
    /// 关注数量
    var followNumber: Int = 0
    
    /// 陌生人时空圈数据
    var spaceTimeData: [SpaceTimeDataModel] = [SpaceTimeDataModel]()
    
    //MARK: 从json得到用户信息
    /// 从json得到用户信息
    ///
    /// - Parameter dic: json字典
    class func initFromDic(dic: [String: Any])->UserInfoModel{
        
        let userInfo: UserInfoModel = UserInfoModel()
        
        if let memberCode: String = dic["memberCode"] as? String{
            userInfo.memberCode = memberCode
        }
        if let loginName: String = dic["loginName"] as? String{
            userInfo.loginName = loginName
        }
        if let nickName: String = dic["nickName"] as? String{
            userInfo.nickName = nickName
            userNickName = nickName
        }else{
            userNickName = ""
        }
        if let phoneNumber: String = dic["phone"] as? String{
            userInfo.phoneNumber = phoneNumber
        }
        if let gender: String = dic["gender"] as? String{
            switch gender {
            case "01":
                userInfo.gender = UserGender.male
            case "02":
                userInfo.gender = UserGender.female
            default:
                userInfo.gender = UserGender.undefine
            }
        }
        if let email: String = dic["email"] as? String{
            userInfo.email = email
        }
        if let firstName: String = dic["firstName"] as? String{
            userInfo.firstName = firstName
        }
        if let lastName: String = dic["lastName"] as? String{
            userInfo.lastName = lastName
        }
        if let birthDateInt: Double = dic["birthDate"] as? Double{
            let birthDate: Date = Date(timeIntervalSince1970: (birthDateInt / 1000))
            userInfo.birthDate = birthDate
        }
        if let thirdId: String = dic["thirdId"] as? String{
            userInfo.thirdId = thirdId
        }
        if let jpushId: String = dic["jpushId"] as? String{
            userInfo.jpushId = jpushId
        }
        if let remark: String = dic["remark"] as? String{
            userInfo.remark = remark
        }
        if let status: String = dic["status"] as? String{
            switch status {
            case "00":
                userInfo.status = UserStatus.unacticed
            default:
                userInfo.status = UserStatus.actived
            }
        }
        if let userLogoString: String = dic["userLogo"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: userLogoString + "!/format/webp")
            userInfo.userLogoURL = url
            currentUserLogoURL = userInfo.userLogoURL
        }else{
            currentUserLogoURL = nil
        }
        
        if let token: String = dic["tminstoreToken"] as? String{
            saveUserToken(token: token)//每次请求用户的个人信息时，刷新token
            UserDefaults.standard.synchronize()
        }
        if let isTesterString: String = dic["isTester"] as? String, isTesterString == "01"{
            userInfo.isTester = true
        }else{
            userInfo.isTester = false
        }
        setIsTestUser(result: userInfo.isTester)
        if let fansTotal: Int = dic["fansTotal"] as? Int{
            userInfo.fansNumber = fansTotal
        }
        if let followTotal: Int = dic["followTotal"] as? Int{
            userInfo.followNumber = followTotal
        }
        return userInfo
    }
    
    //MARK: 其他用户的个人信息
    /// 其他用户的个人信息
    ///
    /// - Parameter dic: JSON
    /// - Returns: 用户数据模型
    class func otherUserInfoInitFromDic(dic: [String: Any])->UserInfoModel{
        let model: UserInfoModel = UserInfoModel()
        
        if let isFollow: Bool = dic["isFollow"] as? Bool{
            model.isFollowed = isFollow
        }
        if let isFans: Bool = dic["flag"] as? Bool{
            model.isFans = isFans
        }
        
        if let user: [String: Any] = dic["user"] as? [String: Any] {
            if let nickName: String = user["nickName"] as? String{
                model.nickName = nickName
            }
            if let userLogo: String = user["userLogo"] as? String{
                model.userLogoURL = URL(string: userLogo + "!/format/webp")
            }
            if let fansTotal: Int = user["fansTotal"] as? Int{
                model.fansNumber = fansTotal
            }
            if let followTotal: Int = user["followTotal"] as? Int{
                model.followNumber = followTotal
            }
            if let memberCode: String = user["memberCode"] as? String{
                model.memberCode = memberCode
            }
        }
        if let spaceTimeCircleList: [[String: Any]] = dic["spaceTimeCircleList"] as? [[String: Any]]{
            for json: [String: Any] in spaceTimeCircleList{
                let postcardModel: SpaceTimeDataModel = SpaceTimeDataModel.initFromDic(dic: json)
                model.spaceTimeData.append(postcardModel)
            }
        }
        return model
    }
    
    //MARK: 其他用户关注数据
    /// 其他用户关注数据
    ///
    /// - Parameter dic: JSON数据
    class func initWithOtherUserDic(dic: [String: Any])->UserInfoModel{
        let model: UserInfoModel = UserInfoModel()
        if let followMemberCode: String = dic["followMemberCode"] as? String{
            model.memberCode = followMemberCode
        }
        if let nickName: String = dic["nickName"] as? String{
            model.nickName = nickName
        }
        if let userLogo: String = dic["userLogo"] as? String{
            model.userLogoURL = URL(string: userLogo + "!/format/webp")
        }
        if let isFollow: Bool = dic["isFollow"] as? Bool{
            model.isFollowed = isFollow
        }
        if let isFans: Bool = dic["isFan"] as? Bool{
            model.isFans = isFans
        }
        return model
    }
    
    //MARK: 其他用户的粉丝数据
    /// 其他用户的粉丝数据
    ///
    /// - Parameter dic: JSON
    class func initWithOtherFansDic(dic: [String: Any])->UserInfoModel{
        let model: UserInfoModel = UserInfoModel()
        if let followMemberCode: String = dic["memberCode"] as? String{
            model.memberCode = followMemberCode
        }
        if let nickName: String = dic["nickName"] as? String{
            model.nickName = nickName
        }
        if let userLogo: String = dic["userLogo"] as? String{
            model.userLogoURL = URL(string: userLogo + "!/format/webp")
        }
        if let isFollow: Bool = dic["isFollow"] as? Bool{
            model.isFollowed = isFollow
        }
        if let isFans: Bool = dic["isFan"] as? Bool{
            model.isFans = isFans
        }
        return model
    }
}
