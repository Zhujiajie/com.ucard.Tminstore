//
//  GoodModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: ----------------商品的模型--------------
import UIKit

/// 定制类型
///
/// - pillow: 抱枕
/// - postcard: 明信片
/// - tShirt: T恤
/// - cup: 马克杯
enum CustomizationType: String {
    case pillow = "02"
    case postcard = "01"
    case tShirt = "03"
    case cup = "04"
}

class GoodsModel: NSObject {
    
    /// 商品的名称
    var goodsName: String = ""
    /// 商品ID
    var goodsId: String = ""
    /// 商品图片URL
    var goodsImageURL: URL?
    /// 商品价格
    var goodsPrice: Double = 0.0
    /// 商品类型
    var goodsType: CustomizationType = CustomizationType.postcard
    
    //MARK: 从JSON字典生成模型
    /// 从JSON字典生成模型
    ///
    /// - Parameter dic: JSON字典
    class func initFromJson(dic: [String: Any])->GoodsModel{
        let goods: GoodsModel = GoodsModel()
        
        if let goodsId: String = dic["goodsId"] as? String{
            goods.goodsId = goodsId
        }
        if let goodsName: String = dic["goodsName"] as? String{
            goods.goodsName = goodsName
        }
        if let goodsPicture: String = dic["goodsPicture"] as? String{
            goods.goodsImageURL = URL(string: goodsPicture + "!/format/webp")
        }
        if let goodsPrice: Double = dic["goodsPrice"] as? Double{
            goods.goodsPrice = goodsPrice
        }
        if let typeString: String = dic["goodsType"] as? String{
            switch typeString{
            case "01":
                goods.goodsType = CustomizationType.postcard
            case "02":
                goods.goodsType = CustomizationType.pillow
            case "03":
                goods.goodsType = CustomizationType.tShirt
            case "04":
                goods.goodsType = CustomizationType.cup
            default:
                break
            }
        }
        return goods
    }
}
