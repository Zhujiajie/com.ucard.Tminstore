//
//  OrderPostcardModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/11.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class OrderPostcardModel: NSObject {

    /// 订单编号
    var mailOrderNo: String = ""
    /// 业务订单号
    var businessOrderNo: String = ""
    /// 订单号
    var paymentOrderNo: String = ""
    /// 用户ID
    var memberCode: String = ""
    /// 图片编号
    var originalId: String = ""
    /// 背面ID
    var backId: String = ""
    /// 支付方式
    var payChannel: PayStyle = PayStyle.unknown
    /// 订单金额
    var orderAmount: Double = 0.0
    /// 邮寄地址ID
    var postAddressId: String = ""
    /// 使用的优惠券的ID
    var couponId: String = ""
    /// 商品预览图片
    var storePictures: URL?
    /// 商品预览图片的缩略图
    var storePicturesThumbnail: URL?
    /// 购买的数量
    var buyTotal: Int = 0
    /// 视频的完成URL
    var videoURL: URL?
    /// 商品实物ID
    var goodsId: String = ""
    /// 背面文字
    var backContent: String = ""
    /// 积分金额
    var points: Int = 0
    /// 订单状态
    var status: OrderStaus = OrderStaus.unPaied
    /// 下单时间
    var createDate: Date?
    /// 更新时间
    var updateDate: Date?
    /// 图片是否被占用
    var imageIsOccupied: Bool = false
    /// 是否无效的订单
    var isInValid: Bool = false
    /// 正面图片的路径
    var frontImageURL: URL?
    /// 正面图片的占位图
    var frontPlaceImageURL: URL?
    /// 背面图片的路径
    var backImageURL: URL?
    /// 视频的id
    var videoId: String = ""
    /// 是否被分享到时空圈
    var isShared: Bool = false
    /// 图片价格
    var imagePrice: Double = 0.0
    /// 是否使用快递
    var useExpress: Bool = false
    /// AR密码
    var arCode: String?
    
    /// 地址模型
    var addressModel: AddressModel = AddressModel()
    /// 商品实物模型
    var goodsModel: GoodsModel = GoodsModel()
    
    /// 交易类型
    var businessType: BusinessType = BusinessType.imageIncome
    /// 钱包余额
    var balance: Double = 0.0
    /// 余额变动
    var balanceChange: Double = 0.0
    
    //MARK: 从字典生成对象
    /// 从字典生成对象
    ///
    /// - Parameter dic: 服务器返回的数据
    class func initFromDic(dic: [String: Any])->OrderPostcardModel{
        
        let order: OrderPostcardModel = OrderPostcardModel()
        
        if let mailOrderNo: String = dic["mailOrderNo"] as? String{
            order.mailOrderNo = mailOrderNo
        }
        if let businessOrderNo: String = dic["businessOrderNo"] as? String{
            order.businessOrderNo = businessOrderNo
        }
        if let paymentOrderNo: String = dic["paymentOrderNo"] as? String{
            order.paymentOrderNo = paymentOrderNo
        }
        if let memberCode: String = dic["memberCode"] as? String{
            order.memberCode = memberCode
        }
        if let originalId: String = dic["originalId"] as? String{
            order.originalId = originalId
        }
        if let backId: String = dic["backId"] as? String{
            order.backId = backId
        }
        if let payChannelStr: String = dic["payChannel"] as? String{
            switch payChannelStr {
            case "00":
                order.payChannel = PayStyle.coupon
            case "01":
                order.payChannel = PayStyle.aliPay
            case "02":
                order.payChannel = PayStyle.weChat
            default:
                order.payChannel = PayStyle.unknown
            }
        }
        if let orderAmount: Double = dic["orderAmount"] as? Double{
            order.orderAmount = orderAmount
        }
        if let addressId: String = dic["addressId"] as? String{
            order.addressModel.addressId = addressId
        }
        if let postAddressId: String = dic["postAddressId"] as? String{
            order.postAddressId = postAddressId
        }
        if let couponId: String = dic["couponId"] as? String{
            order.couponId = couponId
        }
        if let storePicturesStr: String = dic["storePictures"] as? String{
            order.storePictures = URL(string: storePicturesStr + "!/format/webp")
            order.storePicturesThumbnail = URL(string: storePicturesStr + "!placeHolder")
        }
        if let buyTotal: Int = dic["buyTotal"] as? Int{
            order.buyTotal = buyTotal
        }
        if let videoURLStr: String = dic["videoUrl"] as? String{
            order.videoURL = URL(string: videoURLStr)
        }
        if let goodsId: String = dic["goodsId"] as? String{
            order.goodsId = goodsId
        }
        if let backContent: String = dic["backContent"] as? String{
            order.backContent = backContent
        }
        if let points: Int = dic["points"] as? Int{
            order.points = points
        }
        if let statusStr: String = dic["status"] as? String{
            switch statusStr {
            case "00":
                order.status = OrderStaus.unPaied
            case "01":
                order.status = OrderStaus.paid
            case "02":
                order.status = OrderStaus.send
            default:
                order.status = OrderStaus.received
            }
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            order.createDate = Date(timeIntervalSince1970: createDateValue / 1000)
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            order.updateDate = Date(timeIntervalSince1970: updateDateValue / 1000)
        }
        if let imageIdOccupiedStr: String = dic["imageIsOccupied"] as? String{
            if imageIdOccupiedStr == "00"{
                order.imageIsOccupied = false
            }else{
                order.imageIsOccupied = true
            }
        }
        if let isInvalidStr: String = dic["isInvalid"] as? String{
            if isInvalidStr == "00"{
                order.isInValid = false
            }else{
                order.isInValid = true
            }
        }
        if let frontImageURLStr: String = dic["frontImageUrl"] as? String{
            let url: URL? = URL(string: frontImageURLStr + "!/format/webp")
            order.frontImageURL = url
            let placeHolderURL: URL? = URL(string: frontImageURLStr + "!placeHolder")
            order.frontPlaceImageURL = placeHolderURL
        }
        if let backImageURLStr: String = dic["backImageUrl"] as? String{
            let url: URL? = URL(string: backImageURLStr + "!/format/webp")
            order.backImageURL = url
        }
        if let videoId: String = dic["videoId"] as? String{
            order.videoId = videoId
        }
        if let detailedAddress: String = dic["detailedAddress"] as? String{
            order.addressModel.detailedAddress = detailedAddress
        }
        if let country: String = dic["country"] as? String{
            order.addressModel.country = country
        }
        if let province: String = dic["province"] as? String{
            order.addressModel.province = province
        }
        if let city: String = dic["city"] as? String, city != "?"{
            order.addressModel.city = city
        }
        if let district: String = dic["dictrict"] as? String, district != "?"{
            order.addressModel.district = district
        }
        if let postName: String = dic["postName"] as? String{
            order.addressModel.mailName = postName
        }else{
            order.addressModel.mailName = NSLocalizedString("未知", comment: "")
        }
        if let phoneNumber: String = dic["phone"] as? String{
            order.addressModel.phoneNumber = phoneNumber
        }
        if let postcode: String = dic["postCode"] as? String{
            order.addressModel.postcode = postcode
        }
        if let isShared: String = dic["isShared"] as? String{
            if isShared == "01"{
                order.isShared = true
            }
        }
        if let arCode: String = dic["code"] as? String{
            order.arCode = arCode
        }
        return order
    }
    
    //MARK: 从钱包数据生成模型
    /// 从钱包数据生成模型
    ///
    /// - Parameter dic: 钱包交易明细接口返回的JSON
    class func initFromWalletDic(dic: [String: Any])->OrderPostcardModel{
        
        let model: OrderPostcardModel = OrderPostcardModel()
        
        if let businessType: String = dic["businessType"] as? String{
            if let type: BusinessType = BusinessType(rawValue: businessType){
                model.businessType = type
            }
        }
        if let orderAmount: Double = dic["orderAmount"] as? Double{
            model.orderAmount = orderAmount
        }
        if let oldWallet: Double = dic["oldWallet"] as? Double{
            if model.businessType == BusinessType.imageIncome {
                model.balance += oldWallet
            }else{
                model.balance -= oldWallet
            }
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            model.createDate = Date(timeIntervalSince1970: createDateValue / 1000)
        }
        return model
    }
}
