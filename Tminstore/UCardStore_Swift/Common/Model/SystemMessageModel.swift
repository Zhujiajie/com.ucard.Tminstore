//
//  SystemMessageModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/8.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class SystemMessageModel: NSObject {
    
    /// 消息ID
    var messageId: String = ""
    /// 消息的主图
    var subject: String = ""
    /// 消息的内容
    var content: String = ""
    /// 消息的已读状态
    var readStatus: Bool = false
    /// 消息的发送状态
    var sendStatus: Bool = false
    /// 消息的发送时间
    var createDate: Date?
    /// 消息的读取时间
    var readDate: Date?
    
    //MARK: 从JSON字典，创建模型
    /// 从JSON字典，创建模型
    ///
    /// - Parameter dic: [String: Any]
    class func initFromDic(dic: [String: Any])->SystemMessageModel{
        let systemMessage: SystemMessageModel = SystemMessageModel()
        if let messageId: String = dic["messageId"] as? String{
            systemMessage.messageId = messageId
        }
        if let subject: String = dic["subject"] as? String{
            systemMessage.subject = subject
        }
        if let content: String = dic["content"] as? String{
            systemMessage.content = content
        }
        if let readStatusString: String = dic["readStatus"] as? String, readStatusString == "01"{
            systemMessage.readStatus = true
        }
        if let sendStatusString: String = dic["sendStatus"] as? String, sendStatusString == "01"{
            systemMessage.sendStatus = true
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            let date: Date = Date(timeIntervalSince1970: createDateValue / 1000)
            systemMessage.createDate = date
        }
        return systemMessage
    }
}
