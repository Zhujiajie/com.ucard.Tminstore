//
//  ReocrdModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 保存录屏信息的模型
import UIKit

class RecordModel: NSObject {
    
    /// 录屏的ID
    var screencapId: String = ""
    /// 视频的路径
    var videoURL: URL?
    /// 缩略图路径
    var thumbnailURL: URL?
    /// 缩略占位图
    var thumbnailPlaceHolderURL: URL?
    /// 生成时间
    var createDate: Date?
    /// 用户的备注
    var remark: String = ""
    /// 用户头像
    var userLogoURL: URL?
    
    //MARK: 从JSON生成对象
    /// 从JSON生成对象
    ///
    /// - Parameter dic: JSON
    class func initFromDic(dic: [String: Any])->RecordModel{
        let model: RecordModel = RecordModel()
        if let screencapUrl: String = dic["screencapUrl"] as? String{
            model.videoURL = URL(string: screencapUrl)
        }
        if let thumbnailView: String = dic["thumbnailView"] as? String{
            model.thumbnailURL = URL(string: thumbnailView + "!/format/webp")
            model.thumbnailPlaceHolderURL = URL(string: thumbnailView + "!placeHolder")
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            let creatDate: Date = Date(timeIntervalSince1970: createDateValue/1000)
            model.createDate = creatDate
        }
        if let screencapId: String = dic["screencapId"] as? String{
            model.screencapId = screencapId
        }
        if let remark: String = dic["remark"] as? String{
            model.remark = remark
        }
        if let userLogo: String = dic["userLogo"] as? String{
            model.userLogoURL = URL(string: userLogo + "!/format/webp")
        }
        return model
    }
}
