//
//  CouponModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class CouponModel: NSObject {

    /// 优惠券code
    var couponCode: String = ""
    /// 优惠券名字
    var couponName: String = ""
    /// 优惠券内容
    var couponContent: String = NSLocalizedString("全免优惠券", comment: "")
    /// 优惠券抵扣金额
    var couponPromotion: Double = 0.0
//    /// 优惠券类型
//    var couponType: String = ""
    /// 优惠开始时间
    var couponStartTime: Date?
    /// 优惠结束时间
    var couponEndTime: Date?
//    /// 优惠券总数
//    var totalNum: Int = 0
//    /// 已发送的优惠券数量
//    var sendNum: Int = 0
//    /// 剩余优惠券数量
//    var remainNum: Int = 0
    /// 优惠券是否可用
    var couponStatus: Bool = false
    /// 需要支付的费用
    var priceNeedToPay: Double = 0.0
    /// 是否是活动礼券
    var isActivityCoupon: Bool = false
    /// 领取时间
    var createDate: Date?
    
    //MARK: 从json生成优惠券
    /// 从json生成优惠券
    ///
    /// - Parameter dic: [String: Any]
    class func initWithDic(dic: [String: Any])->CouponModel{
        
        let coupon: CouponModel = CouponModel()
        
        if let couponCode: String = dic["couponCode"] as? String{
            coupon.couponCode = couponCode
        }
        if let couponName: String = dic["couponName"] as? String{
            coupon.couponName = couponName
        }
        if let couponContent: String = dic["couponContent"] as? String{
            coupon.couponContent = couponContent
        }
        if let couponPromotionString: String = dic["couponPromotionCode"] as? String{
            coupon.couponPromotion = Double(couponPromotionString)!
        }
        if let couponStatus: String = dic["status"] as? String, couponStatus == "00"{
            coupon.couponStatus = true
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            coupon.createDate = Date(timeIntervalSince1970: createDateValue/1000)
        }
        if let couponStartTime: Double = dic["couponStartTime"] as? Double{
            coupon.createDate = Date(timeIntervalSince1970: couponStartTime/1000)
        }
        if let couponEndTime: Double = dic["couponEndTime"] as? Double{
            coupon.createDate = Date(timeIntervalSince1970: couponEndTime/1000)
        }
        
        return coupon
    }
}
