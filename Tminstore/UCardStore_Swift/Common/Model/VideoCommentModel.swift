//
//  VideoCommentModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

/// 视频评论的模型

import UIKit

class VideoCommentModel: NSObject {

    /// id
    var id: String = ""
    var cardOrderNo: String = ""
    var originalId: String = ""
    var memberCode: String = ""
    var nickName: String = ""
    var commentTotal: Int = 0
    var userLogoURL: URL?
    var videoURL: URL?
    var status: String = ""
    var remark: String = ""
    var likeTotal: Int = 0
    var isLike: Bool = false
    var createDate: Date?
    var updateDate: Date?
    var thumbnailURL: URL?
    
    //MARK: 生成视频评论
    class func initFromVideoCommentDic(dic: [String: Any])->VideoCommentModel{
        let model: VideoCommentModel = VideoCommentModel()
        
        if let id: String = dic["commentId"] as? String{
            model.id = id
        }
        if let originalId: String = dic["originalId"] as? String{
            model.originalId = originalId
        }
        if let memberCode: String = dic["memberCode"] as? String{
            model.memberCode = memberCode
        }
        if let nickName: String = dic["nickName"] as? String{
            model.nickName = nickName
        }
        if let commentTotal: Int = dic["commentTotal"] as? Int{
            model.commentTotal = commentTotal
        }
        if let userLogo: String = dic["userLogo"] as? String{
            model.userLogoURL = URL(string: userLogo + "!/format/webp")
        }
        if let videoUrl: String = dic["contentUrl"] as? String{
            model.videoURL = URL(string: videoUrl)
        }
        if let status: String = dic["status"] as? String{
            model.status = status
        }
        if let remark: String = dic["remark"] as? String{
            model.remark = remark
        }
        if let likeTotal: Int = dic["likeTotal"] as? Int{
            model.likeTotal = likeTotal
        }
        if let isLike: Bool = dic["isLike"] as? Bool{
            model.isLike = isLike
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            model.createDate = Date(timeIntervalSince1970: createDateValue/1000)
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            model.updateDate = Date(timeIntervalSince1970: updateDateValue/1000)
        }
        if let thumbnail: String = dic["thumbnailView"] as? String{
            model.thumbnailURL = URL(string: thumbnail + "!/format/webp")
        }
        return model
    }
    
    //MARK: 从JSON字典生成视频评论模型
    /// 从JSON字典生成视频评论模型
    ///
    /// - Parameter dic: JSON
    class func initFromDic(dic: [String: Any])->VideoCommentModel{
        let model: VideoCommentModel = VideoCommentModel()
        
        if let id: String = dic["id"] as? String{
            model.id = id
        }
        if let cardOrderNo: String = dic["cardOrderNo"] as? String{
            model.cardOrderNo = cardOrderNo
        }
        if let originalId: String = dic["originalId"] as? String{
            model.originalId = originalId
        }
        if let memberCode: String = dic["memberCode"] as? String{
            model.memberCode = memberCode
        }
        if let nickName: String = dic["nickName"] as? String{
            model.nickName = nickName
        }
        if let commentTotal: Int = dic["commentTotal"] as? Int{
            model.commentTotal = commentTotal
        }
        if let userLogo: String = dic["userLogo"] as? String{
            model.userLogoURL = URL(string: userLogo + "!/format/webp")
        }
        if let videoUrl: String = dic["videoUrl"] as? String{
            model.videoURL = URL(string: videoUrl)
        }
        if let status: String = dic["status"] as? String{
            model.status = status
        }
        if let remark: String = dic["remark"] as? String{
            model.remark = remark
        }
        if let likeTotal: Int = dic["likeTotal"] as? Int{
            model.likeTotal = likeTotal
        }
        if let isLike: Bool = dic["isLike"] as? Bool{
            model.isLike = isLike
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            model.createDate = Date(timeIntervalSince1970: createDateValue/1000)
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            model.updateDate = Date(timeIntervalSince1970: updateDateValue/1000)
        }
        if let thumbnail: String = dic["thumbnail"] as? String{
            model.thumbnailURL = URL(string: thumbnail + "!/format/webp")
        }
        return model
    }
}
