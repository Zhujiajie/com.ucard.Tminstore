//
//  MUFinalPostCardModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: -----------------正在制作的明信片的模型-----------------
import UIKit
import CoreLocation

class MUFinalPostcardModel: NSObject {
    
    /// 是否是从买图流程来的
    var isComeFromPurchase: Bool = false
    /// 是否是私人的
    var isPrivate: Bool = false
    /// 原创ID
    var originalId: String = ""
    
    /// 正面图片
    var frontImage: UIImage?
    /// 正面预览图片
    var previewImage: UIImage?
    /// 反面图片
    var backImage: UIImage?
    /// 地址图片
    var addressImage: UIImage?
    
    /// 正面图片的UUID
    var frontImageUUID: String = ""
    /// 背面图片的UUID
    var backImageUUID: String = ""
    /// 地址图片的UUID
    var addressImageUUID: String = ""
    /// 视频的UUID
    var videoID: String = ""
    /// 商品预览图片的UUID
    var previewImageUUID = ""
    /// 视频的存储路径
    var videoPath: URL?

    /// 正面图片在又拍云的路径
    var frontImageURLPath: String{
        get{
            return appAPIHelp.upyunBase + frontImageUpun + frontImageUUID + ".png"
        }
    }
    /// 背面图片在又拍云的路径
    var backImageURLPath: String{
        get{
            return appAPIHelp.upyunBase + backImageUpun + backImageUUID + ".png"
        }
    }
    /// 地址图片在又拍云的路径
    var addressImageURLPath: String{
        get{
            return appAPIHelp.upyunBase + addressImageUpun + addressImageUUID + ".png"
        }
    }
    /// 视频在又拍云的路径
    var videoURLPath: String{
        get{
            return appAPIHelp.upyunBase + videoUpun + videoID + ".mp4"
        }
    }
    /// 预览图片在又拍云的路径
    var previewImageURLPath: String{
        get{
            return appAPIHelp.upyunBase + frontImageUpun + previewImageUUID + ".png"
        }
    }
    /// 分享到社区正面图片在又拍云的路径
    var communityFrontImageURLPath: String{
        get{
            return appAPIHelp.upyunBase + communityFrontImageUpun + frontImageUUID + ".png"
        }
    }
    /// 分享到社区背面图片在又拍云的路径
    var communityBackImageURLPath: String{
        get{
            return appAPIHelp.upyunBase + communityBackImageUpun + backImageUUID + ".png"
        }
    }
    /// 分享到社区视频在又拍云的路径
    var communityVideoURLPath: String{
        get{
            return appAPIHelp.upyunBase + communityVideoUpun + videoID + ".mp4"
        }
    }
    
    
    /// 正面图片的描述信息
    var frontImageRemark: String = ""
    /// 背面图片的描述信息
    var backImageRemark: String = ""
    /// 视频的描述信息
    var videoRemark: String = ""
    
    /// 卡片背面的信息
    var message: String = ""
    /// 背面文字的字体
    var messageFontName: String = ""
    /// 背面文字字体大小
    var messageFontSize: Int = defualTextSize
    
    /// 照片所在的地理位置
    var photoLocation: CLLocation?
    /// 照片所在地理位置的文字
    var photoLocationText: String?
    /// 国家的中文名
    var countryInCn: String = ""
    /// 国家的英文名
    var countryInEn: String = ""
    /// 省份的中文名
    var stateInCN: String = ""
    /// 省份的英文名
    var stateInEn: String = ""
    /// 城市的中文名
    var cityInCN: String = ""
    /// 城市的英文名
    var cityInEN: String = ""
    
    /// 用户设定的卡片积分价格
    var pointPrice: Int = 0
    
    /// 是否需要连接云端检测AR图片，默认必须
    var shouldCheckARImage: Bool = true
    
    /// 签名图片
    var signatureImage: UIImage?
    
    /// 是否正面朝上，草稿箱需要用到这个属性
    var isFront = true
    
    /// 买图流程中，明信片正面的地址
    var buyFrontImageURL: URL?
    
    /// 创建的时间
    var createDate: Date?
    
    
    /// 支付模型
    let payModel: PayModel = PayModel()
    /// 地址模型
    var addressModel: AddressModel?
    /// 商品模型
    var goods: GoodsModel?{
        didSet{
            if goods != nil {
                payModel.goodsPrice = goods!.goodsPrice
                payModel.goodsName = goods!.goodsName
            }
            if goods?.goodsType == CustomizationType.postcard{
                payModel.isMailAvailable = true
            }
        }
    }
    /**
     从Core Data数据，生成PostCard
     
     - parameter postcardRecord: Postcard
     
     - returns: MUFinalPostCardModel
     */
    class func initFromCoreData(postcardData: Postcard)->MUFinalPostcardModel{
        
        let postcard: MUFinalPostcardModel = MUFinalPostcardModel()
        
        if let imageData: Data = postcardData.frontPreviewImage as Data? {
            postcard.frontImage = UIImage(data: imageData)
        }
        if let imageData: Data = postcardData.backPreviewImage as Data? {
            postcard.backImage = UIImage(data: imageData)
        }
//        if let frontImageUUID: String = postcardData.frontImageUUID{
//            postcard.frontImageUUID = frontImageUUID
//        }
//        if let backImageUUID: String = postcardData.backImageUUID{
//            postcard.backImageUUID = backImageUUID
//        }
//        if let addressImageUUID: String = postcardData.addressImageUUID{
//            postcard.addressImageUUID = addressImageUUID
//        }
//        if let videoID: String = postcardData.videoID{
//            postcard.videoID = videoID
//        }
        if let videoURLStr = postcardData.videoPath{
            let url: URL? = URL(fileURLWithPath: videoURLStr)
            postcard.videoPath = url
        }
        if let frontImageRemark: String = postcardData.frontImageRemark{
            postcard.frontImageRemark = frontImageRemark
        }
        if let backImageRemark: String = postcardData.backImageRemark{
            postcard.backImageRemark = backImageRemark
        }
        if let videoRemark: String = postcardData.videoRemark{
            postcard.videoRemark = videoRemark
        }
        if let message: String = postcardData.backView?.text0{
            postcard.message = message
        }
        if let messageFontName: String = postcardData.backView?.text0FontName{
            postcard.messageFontName = messageFontName
        }
        if let messageFontSize: Int = postcardData.backView?.text0FontSize?.intValue{
            postcard.messageFontSize = messageFontSize
        }
        if let photoLocation: CLLocation = postcardData.photoLocation as? CLLocation{
            postcard.photoLocation = photoLocation
        }
        if let photoLocationText: String = postcardData.photoLocationText{
            postcard.photoLocationText = photoLocationText
        }
        if let signatureImageData: Data = postcardData.backView?.signatureImage{
            let image: UIImage? = UIImage(data: signatureImageData)
            postcard.signatureImage = image
        }
        if let creatDate: Date = postcardData.createdDate as Date?{
            postcard.createDate = creatDate
        }
        return postcard
    }
}
