//
//  MessageModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: -------------------推送消息的模型---------------
import UIKit

/// 推送消息的模型
class MessageModel: NSObject {

    /// 消息ID
    var noticeId: String = ""
    /// 消息来源的memberCode
    var memberCode: String = ""
    /// 消息的标题
    var noticeTitle: String = ""
    /// 消息的内容
    var noticeContent: String = ""
    /// 消息是否已读
    var status: Bool = false
    /// 消息的创建时间
    var createDate: Date?
    /// 用户的头像
    var userLogoURL: URL?
    /// videoID
    var videoId: String = ""
    
    //MARK: 从JSON字典，创建模型
    /// 从JSON字典，创建模型
    ///
    /// - Parameter dic: [String: Any]
    class func initFromDic(dic: [String: Any])->MessageModel{
        let message: MessageModel = MessageModel()
        
        if let noticeId: String = dic["noticeId"] as? String{
            message.noticeId = noticeId
        }
        if let memberCode: String = dic["memberCode"] as? String{
            message.memberCode = memberCode
        }
        if let noticeTitle: String = dic["noticeTitle"] as? String{
            message.noticeTitle = noticeTitle
        }
        if let noticeContent: String = dic["noticeContent"] as? String{
            message.noticeContent = noticeContent
        }
        if let statusString: String = dic["status"] as? String, statusString == "01"{
            message.status = true
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            let date: Date = Date(timeIntervalSince1970: createDateValue / 1000)
            message.createDate = date
        }
        if let userLogoURLString: String = dic["userLogo"] as? String{
            message.userLogoURL = URL(string: userLogoURLString + "!/format/webp")
        }
        if let noticeParameter: String = dic["noticeParameter"] as? String{
            let paras: [String] = noticeParameter.components(separatedBy: ",")
            if paras.isEmpty == false{
                message.videoId = paras[0]
            }
        }
        return message
    }
}
