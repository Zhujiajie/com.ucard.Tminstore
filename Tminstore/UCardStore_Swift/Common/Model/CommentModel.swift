//
//  CommentModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/14.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: 评论

import UIKit

class CommentModel: NSObject {
    
    /// 评论ID
    var commentId: String = ""
    /// 被评论的卡片的ID
    var originalId: String = ""
    /// 评论的用户的ID
    var memberCode: String = ""
    /// 用户头像
    var userLogo: URL?
    /// 昵称
    var nickName: String = ""
    /// 评论内容
    var content: String = " "
    /// 状态
    var status: String = ""
    /// 评论时间
    var createDate: Date?
    /// 评论更新时间
    var updateDate: Date?
    /// 被回复数
    var replyTotal: Int = 0
    /// 回复数组
    var replyArray: [ReplyModel] = [ReplyModel]()
    /// 视频路径
    var contentURL: URL?
    /// 视频的缩略图
    var videoThumbnail: UIImage?
    /// 视频缩略图的路径
    var videoThumbnailURL: URL?
    /**
     从服务器返回的字典中，生成CommentModel
     
     - parameter dic: [String: AnyObject]
     
     - returns: CommentModel
     */
    class func initFromDic(dic: [String: Any])-> CommentModel{
        
        let comment = CommentModel()
        
        if let commentId: String = dic["commentId"] as? String{
            comment.commentId = commentId
        }
        if let originalId: String = dic["originalId"] as? String{
            comment.originalId = originalId
        }
        if let memberCode: String = dic["memberCode"] as? String{
            comment.memberCode = memberCode
        }
        if let userLogoStr: String = dic["userLogo"] as? String{
            let url: URL? = URL(string: userLogoStr + "!/format/webp")
            comment.userLogo = url
        }
        if let nickName: String = dic["nickName"] as? String{
            comment.nickName = nickName
        }
        if let content: String = dic["content"] as? String{
            comment.content = content
        }
        if let status: String = dic["status"] as? String{
            comment.status = status
        }
        if let creatDateValue: Double = dic["createDate"] as? Double{
            let date: Date = Date(timeIntervalSince1970: creatDateValue / 1000)
            comment.createDate = date
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            let date: Date = Date(timeIntervalSince1970: updateDateValue / 1000)
            comment.updateDate = date
        }
        if let replyTotal: Int = dic["replyTotal"] as? Int{
            comment.replyTotal = replyTotal
        }
        if let contentURLString: String = dic["contentUrl"] as? String{
            comment.contentURL = URL(string: contentURLString)
        }
        if let replyList: [[String: Any]] = dic["replyList"] as? [[String: Any]]{
            if replyList.isEmpty == false{
                for i: Int in 0..<replyList.count{
                    if i > 4 {break}
                    let reply: ReplyModel = ReplyModel.initFromDic(dic: replyList[i])
                    comment.replyArray.append(reply)
                }
            }
        }
        if let videoThumbnailURLString: String = dic["thumbnailView"] as? String{
            let url: URL? = URL(string: videoThumbnailURLString + "!/format/webp")
            comment.videoThumbnailURL = url
        }
        return comment
    }
    
    //MARK: 从PostCardModel生产CommentModel
    /// 从PostCardModel生产CommentModel
    ///
    /// - Parameter postCard: PostcardModel
    class func initFromPostCardModel(postCard: PostcardModel)->CommentModel{
        let comment: CommentModel = CommentModel()
        
        comment.userLogo = postCard.userLogoURL
        comment.nickName = postCard.nickName
        comment.commentId = ""
        comment.memberCode = postCard.memberCode
        comment.originalId = postCard.originalId
        comment.contentURL = postCard.videoURL
        comment.createDate = postCard.createDate
        comment.videoThumbnailURL = postCard.videoThumbnailURL
        
        return comment
    }
    
    //MARK: 从时光圈模型转换来
    /// 从时光圈模型转换来
    ///
    /// - Parameter mapModel: SpaceTimeDataModel
    class func initFromSpaceTimeModel(mapModel: SpaceTimeDataModel)->CommentModel{
        let comment: CommentModel = CommentModel()
        
        comment.userLogo = mapModel.userLogoURL
        comment.nickName = mapModel.nickName
        comment.commentId = ""
        comment.memberCode = mapModel.memberCode
        comment.originalId = mapModel.originalId
        comment.contentURL = mapModel.videoURL
        comment.createDate = mapModel.createDate
        comment.videoThumbnailURL = mapModel.videoThumbnailURL
        
        return comment
    }
    
    /**
     从core data 中生成CommentModel
     
     - parameter commentObject: Comment
     
     - returns: CommentModel
     */
    class func initFromCoreData(commentObject: Comment)->CommentModel{
        
        let comment = CommentModel()
        
        if let commentId: String = commentObject.commentId{
            comment.commentId = commentId
        }
        if let memberCode: String = commentObject.memberCode{
            comment.memberCode = memberCode
        }
        if let originalId: String = commentObject.originalId{
            comment.originalId = originalId
        }
        if let userLogoStr: String = commentObject.userLogoStr{
            let url: URL? = URL(string: userLogoStr + "!thumbernail")
            comment.userLogo = url
        }
        if let nickName: String = commentObject.nickName{
            comment.nickName = nickName
        }
        if let content: String = commentObject.content{
            comment.content = content
        }
        if let status: String = commentObject.status{
            comment.status = status
        }
        if let createDate: NSDate = commentObject.createDate{
            comment.createDate = createDate as Date?
        }
        if let updateDate: NSDate = commentObject.updateDate{
            comment.updateDate = updateDate as Date?
        }
        comment.replyTotal = Int(commentObject.replyTotal)
        
        return comment
    }
}
