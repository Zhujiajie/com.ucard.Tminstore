//
//  MapDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class SpaceTimeDataModel: NSObject {

    /// 明信片ID
    var originalId: String = ""
    ///用来追中AR视频的id，对应又拍云的文件路径
    var videoId: String?
    /// 视频地址
    var videoURL: URL?
    /// 视频的remark
    var videoRemark: String = ""
    /// 用户ID
    var memberCode: String = ""
    /// 用户昵称
    var nickName: String = ""
    /// 明信片主人的头像
    var userLogoURL: URL?
    /// 个人备注
    var personalRemark: String = ""
    /// 正面图片的评论
    var frontImageRemark: String = ""
    /// 审核状态
    var reviewStatus: ReviewStatus = ReviewStatus.waitingForReview
    /// 评论总数
    var commentTotal: Int = 0
    /// 制作日期
    var createDate: Date?
    /// 更新日期
    var updateDate: Date?
    /// 正面图片地址
    var frontImageURL: URL?
    /// 正面图片的占位图
    var frontPlaceImageURL: URL?
    /// AR识别图
    var arImageURL: URL?
    /// 图片地理位置信息
    var photoLocation: CLLocation?
    /// 国家
    var country: String = ""
    /// 省份
    var province: String = ""
    /// 市
    var city: String = ""
    /// 区
    var district: String = ""
    /// 详细地址
    var detailedAddress: String = ""
    /// 状态
    var status: SpaceTimeStatus = SpaceTimeStatus.normal
    /// 视频缩略图的URL
    var videoThumbnailURL: URL?
    
    //MARK: 从字典生成模型
    /// 从字典生成模型
    ///
    /// - Parameter dic: [String: Any]
    class func initFromDic(dic: [String: Any])-> SpaceTimeDataModel{
        
        let model: SpaceTimeDataModel = SpaceTimeDataModel()
        
        if let videoId: String = dic["videoId"] as? String{
            model.videoId = videoId
        }
        if let videoUrlString: String = dic["videoUrl"] as? String{
            model.videoURL = URL(string: videoUrlString)
            if model.videoId == nil {
                model.videoId = model.videoURL?.deletingPathExtension().lastPathComponent
            }
        }
        if let videoRemark: String = dic["videoRemark"] as? String{
            model.videoRemark = videoRemark
        }
        if let originalId: String = dic["originalId"] as? String{
            model.originalId = originalId
        }
        if let memberCode: String = dic["memberCode"] as? String{
            model.memberCode = memberCode
        }
        if let nickName: String = dic["nickName"] as? String{
            model.nickName = nickName
        }
        if let userLogoURLString: String = dic["userLogo"] as? String{
            model.userLogoURL = URL(string: userLogoURLString)
        }
        if let personalRemark: String = dic["personalRemark"] as? String{
            model.personalRemark = personalRemark
        }
        if let frontImageRemark: String = dic["frontImageRemark"] as? String{
            model.frontImageRemark = frontImageRemark
        }
        if let reviewsStatusString: String = dic["reviewStatus"] as? String{
            switch reviewsStatusString {
            case "00":
                model.reviewStatus = ReviewStatus.waitingForReview
            case "01":
                model.reviewStatus = ReviewStatus.reviewed
            default:
                model.reviewStatus = ReviewStatus.reviewFailed
            }
        }
        if let commentTotal: Int = dic["commentTotal"] as? Int{
            model.commentTotal = commentTotal
        }
        if let creatDateValue: Double = dic["createDate"] as? Double{
            let date: Date = Date(timeIntervalSince1970: creatDateValue / 1000)
            model.createDate = date
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            let date: Date = Date(timeIntervalSince1970: updateDateValue / 1000)
            model.updateDate = date
        }
        if let frontImageURLStr: String = dic["frontImageUrl"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: frontImageURLStr + "!/format/webp")
            model.frontImageURL = url
            let placeHolderURL: URL? = URL(string: frontImageURLStr + "!placeHolder")
            model.frontPlaceImageURL = placeHolderURL
            let arImageURL: URL? = URL(string: frontImageURLStr + "!thumbnail")
            model.arImageURL = arImageURL
        }
        if let longitude: Double = dic["longitude"] as? Double{
            if let latitude: Double = dic["latitude"] as? Double{
                let photoLocation = CLLocation(latitude: latitude, longitude: longitude)
                model.photoLocation = photoLocation
            }
        }
        if let country: String = dic["country"] as? String{
            model.country = country
        }
        if let province: String = dic["province"] as? String{
            model.province = province
        }
        if let city: String = dic["city"] as? String, city != "?"{
            model.city = city
        }
        if let district: String = dic["district"] as? String, district != "?"{
            model.district = district
        }
        if let detailedAddress: String = dic["detailedAddress"] as? String{
            model.detailedAddress = detailedAddress
        }
        if let statusString: String = dic["status"] as? String{
            switch statusString {
            case "00":
                model.status = SpaceTimeStatus.delete
            case "01":
                model.status = SpaceTimeStatus.normal
            default:
                model.status = SpaceTimeStatus.cancel
            }
        }
//        if let infoDicList: [[String: Any]] = dic["videoList"] as? [[String: Any]]{
//            if let inDic: [String: Any] = infoDicList.first{
//                if let nickName: String = inDic["nickName"] as? String{
//                    model.nickName = nickName
//                }
//                if let userLogoURLString: String = inDic["userLogo"] as? String{
//                    model.userLogoURL = URL(string: userLogoURLString)
//                }
//                if let videoId: String = inDic["id"] as? String{
//                    model.videoId = videoId
//                }
//                if let videoUrlString: String = inDic["videoUrl"] as? String{
//                    model.videoURL = URL(string: videoUrlString)
//                }
//                if let thumbnailString: String = inDic["thumbnail"] as? String{
//                    model.videoThumbnailURL = URL(string: thumbnailString)
//                }
//            }
//        }
        
        return model
    }
}
