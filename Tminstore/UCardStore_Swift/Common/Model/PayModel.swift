//
//  PayModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class PayModel: NSObject {

    /// 支付类型， 默认微信支付
    var payStyle: PayStyle = PayStyle.weChat
    
//    /// 优惠券id
//    var couponId: String?
    
    /// chargeModel， ping++需要用到的支付信息
    var charge: [String: Any]?
    
//    /// 订单序列号
//    var serialNumber: String = ""
    
    /// ping++订单ID
    var chargeId: String = ""
    
    /// 优惠券信息
    var couponModel: CouponModel?
    
    /// 图片的ID
    var originalId: String = ""
    
    /// 积分价格
    var pointPrice: Int = 0
    
    /// 订单编号
    var orderNo: String = ""
    /// 邮寄订单编号
    var mailOrderNo: String = ""
    /// 商品的名字
    var goodsName: String = ""
    /// 订单数量
    var orderNumber: Int = 0
    /// 图片的价格
    var imagePrice: Double = 0.0
    /// 商品实物的价格
    var goodsPrice: Double = 0.0
    /// 商品实物价格总价
    var goodsTotalPrice: Double{
        get{
            return goodsPrice * Double(orderNumber)
        }
    }
    /// 订单总价--使用代金券之后的金额，不包括运费
    var orderTotalAmount: Double{
        get{
            if couponModel != nil{
                return imagePrice + goodsTotalPrice - couponModel!.couponPromotion
            }else{
                return imagePrice + goodsTotalPrice
            }
        }
    }
    /// 需要支付的金额--包括了图片价格和运费，以及减去优惠券金额的价格
    var priceNeedToPay: Double{
        get{
            if couponModel != nil{
                return imagePrice + goodsTotalPrice + freightCharge - couponModel!.couponPromotion
            }else{
                return imagePrice + goodsTotalPrice + freightCharge
            }
        }
    }
    /// 运费
    dynamic var freightCharge: Double = 0.0
    /// 是否支持平邮，只有明信片支持平邮
    var isMailAvailable: Bool = false
//    /// 最终减去优惠金额的价格
//    var finalPrice: Double = 0.0
    
    /// 最终传给Ping++的金额, 从后端获取
    var pingxxAmount: Double = 0
    
}
