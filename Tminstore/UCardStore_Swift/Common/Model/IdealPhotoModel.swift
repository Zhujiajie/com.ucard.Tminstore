//
//  IdealPhotoModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/4.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class IdealPhotoModel: NSObject {
    
    /// 图片ID
    var originalId: String = ""
    /// 图片价格
    var moneyPrice: Double = 0
    /// 图片的URL
    var imgUrl: URL?
    /// 占位图
    var placeHolderURL: URL?
    /// 图片的大小
    var imageSize: CGSize = CGSize(width: 184/baseWidth, height: 150/baseWidth)
    /// 原图是否已被下载
    var isOriginalImageDownloaded: Bool = false
    /// 购买次数
    var orderTotal: Int = 0
    /// 作者昵称
    var nickName: String = ""
    /// 作者头像
    var portraitURL: URL?
    /// 原始图片
    var originalImage: UIImage?
    /// 裁剪过的图片
    var croppedImage: UIImage?
    
    /// 视频数组
    var videoList: [URL] = [URL]()
    /// 实物预览图数组
    var storePictures: [URL] = [URL]()
    /// 视图预览图占位图数组
    var storePicturesPlaceHolder: [URL] = [URL]()
    
    /// 生成的事件
    var createDate: Date?
    
    //MARK: 从JSON字典生成模型
    /// 从JSON字典生成模型
    ///
    /// - Parameter dic: JSON字典
    class func initFromDic(dic: [String: Any])->IdealPhotoModel{
        let model: IdealPhotoModel = IdealPhotoModel()
        if let originalId: String = dic["originalId"] as? String{
            model.originalId = originalId
        }
        if let moneyPrice: Double = dic["moneyPrice"] as? Double{
            model.moneyPrice = moneyPrice
        }
        if let imgUrlString: String = dic["imgUrl"] as? String{
            model.imgUrl = URL(string: imgUrlString + "!/format/webp")
            model.placeHolderURL = URL(string: imgUrlString + "!placeHolder")
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            model.createDate = Date(timeIntervalSince1970: createDateValue/1000)
        }
        if let orderTotla: Int = dic["orderTotal"] as? Int{
            model.orderTotal = orderTotla
        }
        if let nickName: String = dic["nickName"] as? String{
            model.nickName = nickName
        }
        if let userLogo: String = dic["userLogo"] as? String{
            model.portraitURL = URL(string: userLogo + "!/format/webp")
        }
        return model
    }
    
    //MARK: 获取视频和图片数据
    /// 获取视频和图片数据
    ///
    /// - Parameter results: <#results description#>
    func addVideoAndStoreImageData(results: [[String: Any]]){
        for dic: [String: Any] in results{
            if let videoUrlList: [String] = dic["videoUrlList"] as? [String], videoUrlList.isEmpty == false {
                if let storePictures: String = dic["storePictures"] as? String{
                    self.videoList.append(URL(string: videoUrlList.first!)!)
                    self.storePictures.append(URL(string: storePictures + "!/format/webp")!)
                    self.storePicturesPlaceHolder.append(URL(string: storePictures + "!placeHolder")!)
                }
            }
        }
    }
}
