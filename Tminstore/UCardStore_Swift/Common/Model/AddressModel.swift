//
//  AddressModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

//MARK: ----------地址模型------------

class AddressModel: NSObject, NSCopying, NSMutableCopying {
    
    /// 是否是新增地址
    var isEdting: Bool = true
    /// 地址ID
    var addressId: String = ""
    /// 用户ID
    var memberCode: String = ""
    /// 收件人姓名
    var mailName: String = ""
    /// 手机号码
    var phoneNumber: String = ""
    /// 邮编
    var postcode: String = ""
    /// 是否是默认地址
    var isDefault: Bool = false
    /// 国家
    var country: String = NSLocalizedString("中国", comment: "")
    /// 省份
    var province: String = ""
    /// 城市
    var city: String = ""
    /// 区
    var district: String = ""
    /// 详细地址
    var detailedAddress: String = ""
    /// 生成时间
    var createDate: Date?
    /// 更新时间
    var updateDate: Date?
    
    /**
     从字典设置地址模型
     
     - parameter dic: 服务器返回的字典
     
     - returns: AddressModel
     */
    class func initFromeAddressDic(dic: [String: Any])->AddressModel{
        
        let addressModel = AddressModel()
        
        if let addressId: String = dic["addressId"] as? String {
            addressModel.addressId = addressId
        }
        if let memberCode: String = dic["memberCode"] as? String{
            addressModel.memberCode = memberCode
        }
        if let mailName: String = dic["mailName"] as? String {
            addressModel.mailName = mailName
        }
        if let phoneNumber: String = dic["phone"] as? String{
            addressModel.phoneNumber = phoneNumber
        }
        if let postcode: String = dic["postCode"] as? String{
            addressModel.postcode = postcode
        }
        if let isDefault: String = dic["isDefault"] as? String{
            if isDefault == "00"{
                addressModel.isDefault = false
            }else{
                addressModel.isDefault = true
            }
        }
        if let country: String = dic["country"] as? String{
            addressModel.country = country
        }
        if let province: String = dic["province"] as? String{
            addressModel.province = province
        }
        if let city: String = dic["city"] as? String{
            addressModel.city = city
        }
        if let district: String = dic["district"] as? String{
            addressModel.district = district
        }
        if let detailedAddress: String = dic["detailedAddress"] as? String{
            addressModel.detailedAddress = detailedAddress
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            let creatDate: Date = Date(timeIntervalSince1970: createDateValue/1000)
            addressModel.createDate = creatDate
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            let updateDate: Date = Date(timeIntervalSince1970: updateDateValue/1000)
            addressModel.updateDate = updateDate
        }
        return addressModel
    }
    
    //MARK: 从订单接口解析生成地址模型
    /// 从订单接口解析生成地址模型
    ///
    /// - Parameter dic: JSON数据
    class func initFromOrderJSON(dic: [String: Any])->AddressModel{
        let addressModel: AddressModel = AddressModel()
//        if let postAddressId: String = dic["postAddressId"] as? String{
//            addressModel.addressId = postAddressId
//        }
        if let postName: String = dic["postName"] as? String{
            addressModel.mailName = postName
        }
        if let phone: String = dic["phone"] as? String{
            addressModel.phoneNumber = phone
        }
        if let postcode: String = dic["postCode"] as? String{
            addressModel.postcode = postcode
        }
        if let country: String = dic["country"] as? String{
            addressModel.country = country
        }
        if let province: String = dic["province"] as? String{
            addressModel.province = province
        }
        if let city: String = dic["city"] as? String{
            addressModel.city = city
        }
        if let district: String = dic["district"] as? String{
            addressModel.district = district
        }
        if let detailedAddress: String = dic["detailedAddress"] as? String{
            addressModel.detailedAddress = detailedAddress
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            let creatDate: Date = Date(timeIntervalSince1970: createDateValue/1000)
            addressModel.createDate = creatDate
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            let updateDate: Date = Date(timeIntervalSince1970: updateDateValue/1000)
            addressModel.updateDate = updateDate
        }
        return addressModel
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let newAddress: AddressModel = AddressModel()
        newAddress.isEdting = self.isEdting
        newAddress.addressId = self.addressId
        newAddress.memberCode = self.memberCode
        newAddress.mailName = self.mailName
        newAddress.phoneNumber = self.phoneNumber
        newAddress.postcode = self.postcode
        newAddress.isDefault = self.isDefault
        newAddress.country = self.country
        newAddress.province = self.province
        newAddress.city = self.city
        newAddress.district = self.district
        newAddress.detailedAddress = self.detailedAddress
        newAddress.createDate = self.createDate
        newAddress.updateDate = self.updateDate
        return newAddress
    }
    
    func mutableCopy(with zone: NSZone? = nil) -> Any{
        let newAddress: AddressModel = AddressModel()
        newAddress.isEdting = self.isEdting
        newAddress.addressId = self.addressId
        newAddress.memberCode = self.memberCode
        newAddress.mailName = self.mailName
        newAddress.phoneNumber = self.phoneNumber
        newAddress.postcode = self.postcode
        newAddress.isDefault = self.isDefault
        newAddress.country = self.country
        newAddress.province = self.province
        newAddress.city = self.city
        newAddress.district = self.district
        newAddress.detailedAddress = self.detailedAddress
        newAddress.createDate = self.createDate
        newAddress.updateDate = self.updateDate
        return newAddress
    }
}
