//
//  PostCardModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/8.
//  Copyright © 2016年 ucard. All rights reserved.
//


import UIKit
import CoreLocation

/// 展示在社区的明信片的模型
class PostcardModel: NSObject {
    
    /// 明信片ID
    var originalId: String = ""
    ///用来追中AR视频的id，对应又拍云的文件路径
    var videoId: String?
    /// 视频地址
    var videoURL: URL?
    /// 用户ID
    var memberCode: String = ""
    /// 用户昵称
    var nickName: String = ""
    /// 备注
    var remark: String = ""
    /// 审核状态
    var reviewStatus: ReviewStatus = ReviewStatus.waitingForReview
    /// 是否被分享
    var isOnMarket: Bool = true
    /// 是否已支付
    var isPurchased: Bool = true
    /// 积分价格
    var pointPrice: Int = 0
    /// 评论总数
    var commentTotal: Int = 0
    /// 被买图总数
    var orderTotal: Int = 0
    /// 制作日期
    var createDate: Date?
    /// 更新日期
    var updateDate: Date?
    /// 是否被买卡
    var isSale: Bool = true
    /// 正面图片地址
    var frontImageURL: URL?
    /// 正面图片的占位图
    var frontPlaceImageURL: URL?
    /// 背面图片地址
    var backImageURL: URL?
    /// 图片在视+的id
    var targetId: String = ""
    /// 图片地理位置信息
    var photoLocation: CLLocation?
    /// 国家
    var country: String = ""
    /// 省份
    var province: String = ""
    /// 市
    var city: String = ""
    /// 区
    var district: String = ""
    /// 详细地址
    var detailedAddress: String = ""
    /// 是否是AR
    var isAR: Bool = false
    /// 客户类型，B 商户， C 个人
    var customerType: CustomerType = CustomerType.undefined
    /// 评论列表
    var commentList: [CommentModel] = [CommentModel]()
    /// 明信片主人的头像
    var userLogoURL: URL?
    /// 是否点过赞
    var isLiked: Bool = false
    /// 点赞数
    var likeCount: Int = 0
    /// 明信片是否正面朝上
    var isFront: Bool = true
    /// AR识别图的下载地址
    var arImageURL: URL?
    /// AR缩略图
    var arThumbnailURL: URL?
    /// AR识别图在本地的路径
    var arImagePath: String = ""
    /// 视频的缩略图
    var videoThumbnailURL: URL?
    /// 用户类型
    var userType: UserType?
    /// 地图上icon的路径
    var annotationIconURL: URL?
    /// AR密码
    var arCode: String?
    /// 图片大小，用户缓存图片大小
    var imageSize: CGSize = CGSize(width: 184/baseWidth, height: 150/baseWidth)
    /// 是否已经下载了大图
    var isOriginalImageDownloaded: Bool = false
    /// 商城价格
    var moneyPoint: Double = 0.00
    /// AR的UUID
    var arUUID: UUID = UUID()
    /// 距离
    var distance: CLLocationDistance?
    /// 是否被选中，UI展示中用到该数据
    var isSelected: Bool = false
    /// 扫描次数
    var scannedTimes: Int = 0
    
    /**
     从服务器返回的字典，初始化明信片
     
     - parameter dic: [String: AnyObject]
     
     - returns: PostCardModel
     */
    class func initFromDic(dic: [String: Any])-> PostcardModel{
        
        let postcard: PostcardModel = PostcardModel()
        
        if let originalId: String = dic["originalId"] as? String{
            postcard.originalId = originalId
        }
        if let videoId = dic["videoId"] as? String{
            postcard.videoId = videoId
            postcard.videoURL = URL(string: "http://ucardstorevideo.b0.upaiyun.com" + videoUpun + videoId + ".mp4")
        }
        if let memberCode: String = dic["memberCode"] as? String{
            postcard.memberCode = memberCode
        }
        if let nickName: String = dic["nickName"] as? String{
            postcard.nickName = nickName
        }
        if let remark: String = dic["remark"] as? String{
            postcard.remark = remark
        }
        if let frontImageRemark: String = dic["frontImageRemark"] as? String{
            postcard.remark = frontImageRemark
        }
        if let reviewStatus: String = dic["reviewStatus"] as? String{
            switch reviewStatus {
            case "00":
                postcard.reviewStatus = ReviewStatus.waitingForReview
            case "01":
                postcard.reviewStatus = ReviewStatus.reviewed
            default:
                postcard.reviewStatus = ReviewStatus.reviewFailed
            }
        }
        if let isOnMarket: String = dic["isOnMarket"] as? String{
            if isOnMarket == "00"{
                postcard.isOnMarket = false
            }else{
                postcard.isOnMarket = true
            }
        }
        if let isPurchased: String = dic["isPurchased"] as? String{
            if isPurchased == "00"{
                postcard.isPurchased = false
            }else{
                postcard.isPurchased = true
            }
        }
        if let pointPrice: Int = dic["pointPrice"] as? Int{
            postcard.pointPrice = pointPrice
        }
        if let commentTotal: Int = dic["commentTotal"] as? Int{
            postcard.commentTotal = commentTotal
        }
        if let orderTotla: Int = dic["orderTotal"] as? Int{
            postcard.orderTotal = orderTotla
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            let creatDate: Date = Date(timeIntervalSince1970: createDateValue/1000)
            postcard.createDate = creatDate
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            let updateDate: Date = Date(timeIntervalSince1970: updateDateValue/1000)
            postcard.updateDate = updateDate
        }
        if let isSale: String = dic["isSale"] as? String{
            if isSale == "00"{
                postcard.isSale = false
            }else{
                postcard.isSale = true
            }
        }
        if let frontImageURLStr: String = dic["frontImageUrl"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: frontImageURLStr + "!/format/webp")
            postcard.frontImageURL = url
            let placeHolderURL: URL? = URL(string: frontImageURLStr + "!placeHolder")
            postcard.frontPlaceImageURL = placeHolderURL
            let arImageURL: URL? = URL(string: frontImageURLStr + "!thumbnail")
            postcard.arImageURL = arImageURL
        }
        if let backImageURLStr: String = dic["backImageUrl"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: backImageURLStr + "!/format/webp")
            postcard.backImageURL = url
        }
        if let targetId: String = dic["targetId"] as? String{
            postcard.targetId = targetId
        }
        if let longitude: Double = dic["longitude"] as? Double{
            if let latitude: Double = dic["latitude"] as? Double{
                let photoLocation = CLLocation(latitude: latitude, longitude: longitude)
                postcard.photoLocation = photoLocation
            }
        }
        if let country: String = dic["country"] as? String{
            postcard.country = country
        }
        if let province: String = dic["province"] as? String{
            postcard.province = province
        }
        if let city: String = dic["city"] as? String, city != "?"{
            postcard.city = city
        }
        if let district: String = dic["district"] as? String, district != "?"{
            postcard.district = district
        }
        if let detailedAddress: String = dic["detailedAddress"] as? String{
            postcard.detailedAddress = detailedAddress
        }
        if let isARStr: String = dic["isAr"] as? String{
            if isARStr == "00"{
                postcard.isAR = false
            }else{
                postcard.isAR = true
            }
        }
        if let customerTypeStr: String = dic["customerType"] as? String{
            switch customerTypeStr {
            case "B":
                postcard.customerType = CustomerType.bussines
            case "C":
                postcard.customerType = CustomerType.customer
            default:
                postcard.customerType = CustomerType.undefined
            }
        }
        if let userLogoStr: String = dic["userLogo"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: userLogoStr + "!/format/webp")
            postcard.userLogoURL = url
        }
        if let commentDics: [[String: Any]] = dic["commentList"] as? [[String: Any]]{
            for commentDic: [String: Any] in commentDics{
                let comment: CommentModel = CommentModel.initFromDic(dic: commentDic)
                postcard.commentList.append(comment)
            }
        }
        if let isLikedString: String = dic["isLike"] as? String, isLikedString == "01"{
            postcard.isLiked = true
        }
        if let likeCount: Int = dic["likeTotal"] as? Int{
            postcard.likeCount = likeCount
        }
        if let videoThumbnailURLString: String = dic["videoThumbnail"] as? String{
            let url: URL? = URL(string: videoThumbnailURLString + "!/format/webp")
            postcard.videoThumbnailURL = url
        }
        if let code: String = dic["code"] as? String{
            postcard.arCode = code
        }
        return postcard
    }
    
    //MARK: 从时空圈数据生成模型
    /// 从时空圈数据生成模型
    ///
    /// - Parameter dic: JSON
    class func initFromSpaceTimeCircle(dic: [String: Any])->PostcardModel{
        let postcard: PostcardModel = PostcardModel()
        
        if let memberCode: String = dic["memberCode"] as? String{
            postcard.memberCode = memberCode
        }
        if let originalId: String = dic["originalId"] as? String{
            postcard.originalId = originalId
        }
        if let videoURLString = dic["videoUrl"] as? String{
            postcard.videoURL = URL(string: videoURLString)
        }
        if let nickName: String = dic["nickName"] as? String{
            postcard.nickName = nickName
        }
        if let remark: String = dic["frontImageRemark"] as? String{
            postcard.remark = remark
        }
        if let reviewStatus: String = dic["reviewStatus"] as? String{
            switch reviewStatus {
            case "00":
                postcard.reviewStatus = ReviewStatus.waitingForReview
            case "01":
                postcard.reviewStatus = ReviewStatus.reviewed
            default:
                postcard.reviewStatus = ReviewStatus.reviewFailed
            }
        }
        if let commentTotal: Int = dic["commentTotal"] as? Int{
            postcard.commentTotal = commentTotal
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            let creatDate: Date = Date(timeIntervalSince1970: createDateValue/1000)
            postcard.createDate = creatDate
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            let updateDate: Date = Date(timeIntervalSince1970: updateDateValue/1000)
            postcard.updateDate = updateDate
        }
        if let frontImageURLStr: String = dic["frontImageUrl"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: frontImageURLStr + "!/format/webp")
            postcard.frontImageURL = url
            let placeHolderURL: URL? = URL(string: frontImageURLStr + "!placeHolder")
            postcard.frontPlaceImageURL = placeHolderURL
            let arImageURL: URL? = URL(string: frontImageURLStr + "!thumbnail")
            postcard.arImageURL = arImageURL
            let arThumbnailURL: URL? = URL(string: frontImageURLStr + "!unity")
            postcard.arThumbnailURL = arThumbnailURL
        }else if let frontImageURLStr: String = dic["imageUrl"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: frontImageURLStr + "!/format/webp")
            postcard.frontImageURL = url
            let placeHolderURL: URL? = URL(string: frontImageURLStr + "!placeHolder")
            postcard.frontPlaceImageURL = placeHolderURL
            let arImageURL: URL? = URL(string: frontImageURLStr + "!thumbnail")
            postcard.arImageURL = arImageURL
            let arThumbnailURL: URL? = URL(string: frontImageURLStr + "!unity")
            postcard.arThumbnailURL = arThumbnailURL
        }
        if let longitude: Double = dic["longitude"] as? Double{
            if let latitude: Double = dic["latitude"] as? Double{
                let photoLocation = CLLocation(latitude: latitude, longitude: longitude)
                postcard.photoLocation = photoLocation
            }
        }
        if let country: String = dic["country"] as? String{
            postcard.country = country
        }
        if let province: String = dic["province"] as? String{
            postcard.province = province
        }
        if let city: String = dic["city"] as? String, city != "?"{
            postcard.city = city
        }
        if let district: String = dic["district"] as? String, district != "?"{
            postcard.district = district
        }
        if let detailedAddress: String = dic["detailedAddress"] as? String{
            postcard.detailedAddress = detailedAddress
        }
        if let userLogoStr: String = dic["userLogo"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: userLogoStr + "!/format/webp")
            postcard.userLogoURL = url
        }
        if let videoThumbnailURLString: String = dic["thumbnail"] as? String{
            let url: URL? = URL(string: videoThumbnailURLString + "!/format/webp")
            postcard.videoThumbnailURL = url
        }
        if let commentTotal: Int = dic["commentTotal"] as? Int{
            postcard.commentTotal = commentTotal
        }
        if let videoId: String = dic["id"] as? String{
            postcard.videoId = videoId
        }
        if let userTypeString: String = dic["userType"] as? String{
            switch userTypeString{
            case "02":
                postcard.userType = UserType.nomal
            case "03":
                postcard.userType = UserType.business
            default:
                break
            }
        }
        if let iconURLString: String = dic["icon"] as? String{
            postcard.annotationIconURL = URL(string: iconURLString)
        }
        if let isLikedString: String = dic["isLike"] as? String, isLikedString == "01"{
            postcard.isLiked = true
        }
        return postcard
    }
    
    //MARK: 商城的数据
    /// 商城的数据
    ///
    /// - Parameter dic: JSON数据
    class func initFromMarket(dic: [String: Any])->PostcardModel{
        let postcard: PostcardModel = PostcardModel()
        
        if let originalId: String = dic["originalId"] as? String{
            postcard.originalId = originalId
        }
        if let remark: String = dic["remark"] as? String{
            postcard.remark = remark
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            let creatDate: Date = Date(timeIntervalSince1970: createDateValue/1000)
            postcard.createDate = creatDate
        }
        if let updateDateValue: Double = dic["updateDate"] as? Double{
            let updateDate: Date = Date(timeIntervalSince1970: updateDateValue/1000)
            postcard.updateDate = updateDate
        }
        if let frontImageURLStr: String = dic["imgUrl"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: frontImageURLStr + "!/format/webp")
            postcard.frontImageURL = url
            let placeHolderURL: URL? = URL(string: frontImageURLStr + "!placeHolder")
            postcard.frontPlaceImageURL = placeHolderURL
            let arImageURL: URL? = URL(string: frontImageURLStr + "!thumbnail")
            postcard.arImageURL = arImageURL
        }
        if let moneyPoint: Double = dic["moneyPrice"] as? Double{
            postcard.moneyPoint = moneyPoint
        }
        if let orderTotla: Int = dic["orderTotal"] as? Int{
            postcard.orderTotal = orderTotla
        }
        return postcard
    }
    
    //MARK: 从时光相册生成模型
    /// 从时光相册生成模型
    ///
    /// - Parameter dic: PostcardModel
    class func initFromARAlbum(dic: [String: Any])->PostcardModel{
        
        let postcard: PostcardModel = PostcardModel()
        
        if let originalId: String = dic["originalId"] as? String{
            postcard.originalId = originalId
        }
        if let frontImageURLStr: String = dic["frontImageUrl"] as? String{
            // 向又拍云请求压缩过的图片
            let url: URL? = URL(string: frontImageURLStr + "!/format/webp")
            postcard.frontImageURL = url
            let placeHolderURL: URL? = URL(string: frontImageURLStr + "!placeHolder")
            postcard.frontPlaceImageURL = placeHolderURL
            let arImageURL: URL? = URL(string: frontImageURLStr + "!thumbnail")
            postcard.arImageURL = arImageURL
            let arThumbnailURL: URL? = URL(string: frontImageURLStr + "!unity")
            postcard.arThumbnailURL = arThumbnailURL
        }
        if let code: String = dic["code"] as? String{
            postcard.arCode = code
        }
        if let timeVideoList: [[String: Any]] = dic["timeVideoList"] as? [[String: Any]]{
            if let videoDic: [String: Any] = timeVideoList.first{
                if let videoUrl: String = videoDic["videoUrl"] as? String{
                    postcard.videoURL = URL(string: videoUrl)
                }
            }
        }
        if let scannedTimes: Int = dic["scannedTimes"] as? Int{
            postcard.scannedTimes = scannedTimes
        }
        return postcard
    }
    
    /**
     从Core Data数据，生成PostCardModel
     
     - parameter postcardData: PostcardInCommunity
     
     - returns: PostcardModel
     */
    class func initFromCoreData(postcardData: PostcardInCommunity)->PostcardModel{
        
        let postcard: PostcardModel = PostcardModel()
        
        if let originalId: String = postcardData.originalId{
            postcard.originalId = originalId
        }
        if let videoId: String = postcardData.videoId{
            postcard.videoId = videoId
            postcard.videoURL = URL(string: "http://ucardstorevideo.b0.upaiyun.com" + videoUpun + videoId + ".mp4")
        }
        if let memberCode: String = postcardData.memberCode{
            postcard.memberCode = memberCode
        }
        if let nickName: String = postcardData.nickName{
            postcard.nickName = nickName
        }
        if let remark: String = postcardData.remark{
            postcard.remark = remark
        }
        if let reviewStatusStr: String = postcardData.reviewStatus {
            switch reviewStatusStr{
            case "00":
                postcard.reviewStatus = ReviewStatus.waitingForReview
            case "01":
                postcard.reviewStatus = ReviewStatus.reviewed
            default:
                postcard.reviewStatus = ReviewStatus.reviewFailed
            }
        }
        if let isOnMarket: Bool = postcardData.isOnMarket?.boolValue{
            postcard.isOnMarket = isOnMarket
        }
        if let isPurchase: Bool = postcardData.isPurchased?.boolValue{
            postcard.isPurchased = isPurchase
        }
        postcard.pointPrice = Int(postcardData.pointPrice)
        postcard.commentTotal = Int(postcardData.commentTotal)
        postcard.orderTotal = Int(postcardData.orderTotal)
        if let createDate: Date = postcardData.createDate as Date?{
            postcard.createDate = createDate
        }
        if let updateDate: Date = postcardData.updateDate as Date?{
            postcard.updateDate = updateDate
        }
        if let isSale: Bool = postcardData.isSale?.boolValue{
            postcard.isSale = isSale
        }
        if let frontImageURLStr: String = postcardData.frontImageURLStr{
            let url: URL? = URL(string: frontImageURLStr)
            postcard.frontImageURL = url
        }
        if let backImageURLStr: String = postcardData.backImageURLStr{
            let url: URL? = URL(string: backImageURLStr)
            postcard.backImageURL = url
        }
        if let targetId: String = postcardData.targetId{
            postcard.targetId = targetId
        }
        if let photoLocation: CLLocation = postcardData.photoLocation as? CLLocation{
            postcard.photoLocation = photoLocation
        }
        if let country: String = postcardData.country{
            postcard.country = country
        }
        if let province: String = postcardData.province{
            postcard.province = province
        }
        if let city: String = postcardData.city{
            postcard.city = city
        }
        if let district: String = postcardData.district{
            postcard.district = district
        }
        if let detailedAddress: String = postcardData.detailedAddress{
            postcard.detailedAddress = detailedAddress
        }
        if let isAR: Bool = postcardData.isAR?.boolValue{
            postcard.isAR = isAR
        }
        if let customerTypeStr: String = postcardData.customerType{
            switch customerTypeStr {
            case "B":
                postcard.customerType = CustomerType.bussines
            case "C":
                postcard.customerType = CustomerType.customer
            default:
                postcard.customerType = CustomerType.undefined
            }
        }
        if let userLogoURLStr: String = postcardData.userLogoURLStr{
            let url: URL? = URL(string: userLogoURLStr)
            postcard.userLogoURL = url
        }
        if (postcardData.commentList != nil) {
            for commentData: NSFastEnumerationIterator.Element in postcardData.commentList!{
                let comment: CommentModel = CommentModel.initFromCoreData(commentObject: commentData as! Comment)
                postcard.commentList.append(comment)
            }
        }
        if let isLiked: Bool = postcardData.isLiked?.boolValue{
            postcard.isLiked = isLiked
        }
        postcard.likeCount = Int(postcardData.likeCount)
        return postcard
    }
}
