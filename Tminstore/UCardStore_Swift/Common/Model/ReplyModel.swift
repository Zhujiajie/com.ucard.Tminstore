//
//  ReplyModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/19.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 对回复评论的模型

import UIKit

class ReplyModel: NSObject {

    /// 回复ID
    var id: String = ""
    /// 评论ID
    var commentId: String = ""
    /// 回复者的id
    var replyMemberCode: String = ""
    /// 回复者的头像
    var userLogoURL: URL?
    /// 回复者的昵称
    var replyName: String = ""
    /// 被回复的人的id
    var toReplyMemberCode: String = ""
    /// 被回复者的昵称
    var toReplyName: String = ""
    /// 回复内容
    var content: String = ""
    /// 回复的状态
    var status: String = ""
    /// 回复日期
    var createDate: Date?
    /// 更新日期
    var updateDate: Date?
    /// 视频路径
    var contentUrl: URL?
    /// 缩略图的路径
    var thumbnailUrl: URL?
    /// 回复的ID
    var replyId: String = ""
    
    /// 从json字典生成回复
    ///
    /// - Parameter dic: 字典
    class func initFromDic(dic: [String: Any])->ReplyModel{
        
        let reply: ReplyModel = ReplyModel()
        
        if let id: String = dic["id"] as? String{
            reply.id = id
        }
        if let commentId: String = dic["commentId"] as? String{
            reply.commentId = commentId
        }
        if let replyMemberCode: String = dic["replyMemberCode"] as? String{
            reply.replyMemberCode = replyMemberCode
        }
        if let userLogoStr: String = dic["userLogo"] as? String{
            reply.userLogoURL = URL(string: userLogoStr + "!/format/webp")
        }
        if let replyName: String = dic["replyName"] as? String{
            reply.replyName = replyName
        }
        if let toReplyMemberCode: String = dic["toReplyMemberCode"] as? String{
            reply.toReplyMemberCode = toReplyMemberCode
        }
        if let toReplyName: String = dic["toReplyName"] as? String{
            reply.toReplyName = toReplyName
        }
        if let content: String = dic["content"] as? String{
            reply.content = content
        }
        if let status: String = dic["status"] as? String{
            reply.status = status
        }
        if let createDateValue: Double = dic["createDate"] as? Double{
            reply.createDate = Date(timeIntervalSince1970: createDateValue/1000)
        }
        if let updateDateValue: Double = dic["updateDateValue"] as? Double{
            reply.updateDate = Date(timeIntervalSince1970: updateDateValue/1000)
        }
        if let replyId: String = dic["replyId"] as? String{
            reply.replyId = replyId
        }
        if let contentURLString: String = dic["contentUrl"] as? String{
            reply.contentUrl = URL(string: contentURLString)
        }
        if let thumbnaiURLString: String = dic["thumbnailView"] as? String{
            reply.thumbnailUrl = URL(string: thumbnaiURLString)
        }
        return reply
    }
    
    
}
