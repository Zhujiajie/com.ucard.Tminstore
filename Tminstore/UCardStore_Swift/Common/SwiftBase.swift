//
//  SwiftBase.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/2.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreLocation

let app: AppDelegate = UIApplication.shared.delegate as! AppDelegate

/// cell复用的标签
let appReuseIdentifier: ReuseIdentifier = ReuseIdentifier()

/// 所有接口
let appAPIHelp:APIHelp = APIHelp()

///用户头像路径
var currentUserLogoURL: URL?

/// 用户昵称 
var userNickName: String = ""

/// 占位图片
let placeImage: UIImage = UIImage(named: "placeHolder")!

/// 是否是内测用户
///
/// - Returns: Bool
func isTestUser()-> Bool{
    return UserDefaults.standard.bool(forKey: "isTestUser")
}

/// 设置是否是测试用户
///
/// - Parameter result: Bool
func setIsTestUser(result: Bool){
    UserDefaults.standard.set(result, forKey: "isTestUser")
}

/// 获取最顶端的controller
///
/// - Returns: UIViewController
func topViewController()->UIViewController{
    var topViewController: UIViewController = (app.window?.rootViewController)!
    while topViewController.presentedViewController != nil {
        topViewController = topViewController.presentedViewController!
    }
    return topViewController
}

/// RSA加密用弓腰
let RSAPublicKey: String = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCEGENnf3rdiO20isoLQqezw12FoWXII9FBw8nR1MWQ3X0CVzOsqY1hOmxD/YI9OB7WVIaVax5tj1l+wk6A0v85Z4OpGWqz4B5L3fCUlBwf/M6DXHlSN1OZttvQF3OeWvc6gvJHihR7pp18zc4KfCJx0Ry6IrGH/2SNOVE1AIgvRQIDAQAB"

/// 开启APP之后，是否需要展示介绍图
var shouldShowIntroView = true

/// 当前用户的地理位置
var currentUserLocation: CLLocation?

/// 屏幕高度
let screenWidth: CGFloat  = UIScreen.main.bounds.size.width
/// 屏幕宽度
let screenHeight: CGFloat = UIScreen.main.bounds.size.height

/// 导航栏高度
let navBarHeight: CGFloat     = CGFloat(44)
/// 状态栏高度
let statusBarHeight: CGFloat  = CGFloat(20)
/// 导航栏加状态栏高度
let nav_statusHeight: CGFloat = CGFloat(64)
/// tabbar 高度
let tabbarHeight: CGFloat     = CGFloat(44)

/// autoLayout的宽度计算比例
let baseWidth: CGFloat  = 375/UIScreen.main.bounds.size.width
/// autoLayout的高度计算比例
let baseHeight: CGFloat = 667/UIScreen.main.bounds.size.height

/// photoCollectionView Cell的大小
let photoCollectionViewCellSize: CGSize = CGSize(width: 91/baseWidth, height: 91/baseWidth)
/// 第一个cell的大小
let firstCellSize: CGSize = CGSize(width: photoCollectionViewCellSize.width * 2 + 3/baseWidth, height: photoCollectionViewCellSize.height * 2 + 3)

/**
 获取view的宽度
 
 - parameter view: view
 
 - returns: CGFlote
 */
func WIDTH(_ view: UIView) -> CGFloat {
    return view.frame.width
}

/**
 获取view的高度
 
 - parameter view: view
 
 - returns: view的高度
 */
func HEIGHT(_ view: UIView) -> CGFloat {
    return view.frame.height
}
/**
 获取view的origin.x
 
 - parameter view: view
 
 - returns: CGPoint
 */
func X(_ view: UIView) -> CGFloat {
    return view.frame.origin.x
}
/**
 获取view的origin.y
 
 - parameter view: view
 
 - returns: CGPoint
 */
func Y(_ view: UIView) -> CGFloat {
    return view.frame.origin.y
}

/**
 快速获取
 
 - parameter R: red
 - parameter G: green
 - parameter B: blue
 - parameter A: alpha
 
 - returns: UIColor
 */
func RGBAColor(R: CGFloat, G: CGFloat, B: CGFloat, A: CGFloat) -> UIColor {
    return UIColor(red: R/255, green: G/255, blue: B/255, alpha: A)
}

/// 字体LobsterTwo-Bold，导航栏标题字体
let LobsterTwo_Bold: String = "LobsterTwo-BoldItalic"

/// 字体FZYBXSJW--GB1-0，中文手写体
let FZYBXSJW_GB1_0: String = "FZYBXSJW--GB1-0"

/// 字体FZYBXSJW--GB1-0的下载地址
let FZYBXSJW_GB1_0_URL: String = "http://ucardstorevideo.b0.upaiyun.com/font/FZYBXSJW--GB1-0.ttf"

//MARK: 是否已经成功加载字体FZYBXSJW_GB1_0
/**
 是否已成功加载字体FZYBXSJW_GB1_0
 
 - returns: Bool
 */
func loadFZYBXSJW_GB1_0_State()->Bool{
    if let result = UserDefaults.standard.object(forKey: FZYBXSJW_GB1_0) as? Bool{
        return result
    }else{
        return false
    }
}

///设置已经加载字体FZYBXSJW_GB1_0的状态
var fontHasLoaded: Bool = false

/// present动画
let animator: PushAnimation = PushAnimation()
//// 动画手势
let interactive: Interactive = Interactive()


//MARK: 获取系统语言
/**
 获取当前系统语言
 
 - returns: String?
 */
func getPreferredLanguage()-> String?{
    
    let pre: String? = Bundle.main.preferredLocalizations.first
    return pre
}

//MARK: 获得languageCode
/**
 获得languageCode, 中文环境返回“CN”，其它语言环境返回“EN”
 
 - returns: 中文环境返回“CN”，其它语言环境返回“EN”
 */
func languageCode()->String{
    let pre: String? = getPreferredLanguage()
    
    if pre == nil {
        return "EN"
    }else if pre!.contains("zh") == true{
        return "CN"
    }else{
        return "EN"
    }
}

//MARK: 存储极光pushID
/**
 存储极光pushID
 
 - parameter registrationID: 极光的registrationID
 */
func saveJPushRegistrationID(registrationID: String?){
    if let id: String = registrationID {
        UserDefaults.standard.set(id, forKey: "registrationID")
    }
}

//MARK: 取出极光pushID
/**
 取出极光pushID
 
 - returns: 若没有id，则返回“”
 */
func getJPushRegistrationID() -> String{
    if let id: String = UserDefaults.standard.object(forKey: "registrationID") as? String{
        return id
    }
    return ""
}

//MARK: -----------------设置用户是否已经登录---------------------
/**
 设置用户是否已经登录
 
 - parameter loginStatus: 用户是否已经登录
 */
func setLoginStatus(loginStatus: Bool){
    UserDefaults.standard.set(loginStatus, forKey: "javaLoginStatus")
}

//MARK: 获取用户登录情况
/**
 获取用户登录情况
 
 - returns: bool
 */
func getUserLoginStatus()->Bool{
    let status: Bool = UserDefaults.standard.bool(forKey: "javaLoginStatus")
    return status
}

//MARK: 设置游客模式
/**
 设置是否为游客模式
 
 - parameter isGesture: 是否为游客模式
 */
func setGestureModel(isGesture: Bool){
    UserDefaults.standard.set(isGesture, forKey: "isGesture")
}

//MARK: 存储用户token
/**
 存储用户token
 
 - parameter token: token
 */
func saveUserToken(token: String) {
    UserDefaults.standard.set(token, forKey: "userToken")
}

//MARK: 从NSUserDefaults获取用户token
/**
 从NSUserDefaults获取用户token，之后直接使用userToken
 */
func getUserToken() -> String{
    if let token: String = UserDefaults.standard.object(forKey: "userToken") as? String{
        return token
    }
    return ""
}

//MARK: 保存用户ID
/**
 保存用户id，游客则保存id为gesture
 
 - parameter userId: String
 */
func saveUserID(userId: String){
    if userId != "" {
        UserDefaults.standard.set(userId, forKey: "userID")
    }else{
        UserDefaults.standard.set("gesture", forKey: "userID")
    }
}
//MARK: 获取用户ID
/**
 获取用户的ID
 
 - returns: String
 */
func getUserID()->String{
    if let userID: String = UserDefaults.standard.object(forKey: "userID") as? String{
        return userID
    }
    return ""
}
//MARK: 保存用户名
/**
 保存用户名
 
 - parameter name: String
 */
func saveUserName(name: String?){
    UserDefaults.standard.set(name, forKey: "userName")
}
//MARK: 取得用户名
/**
 取得用户名
 
 - returns: String
 */
func getUserName()->String{
    if let name: String = UserDefaults.standard.object(forKey: "userName") as? String{
        return name
    }else{
        return NSLocalizedString("游客", comment: "")
    }
}
//MARK: 保存用户头像地址
/**
 保存用户头像地址
 
 - parameter urlStr: String
 */
func saveUserPortrit(urtStr urlStr: String?){
    UserDefaults.standard.set(urlStr, forKey: "portritURL")
}
//MARK: 取得用户头像的url
/**
 取得用户头像的url
 
 - returns: NSURL?
 */
func getUserPortritURL()->URL?{
    if let urlStr: String = UserDefaults.standard.object(forKey: "portritURL") as? String, urlStr != ""{
        return URL(string: urlStr)
    }else{
        return nil
    }
}
//MARK: 是否处于游客模式
/**
 判断用户是否在游客模式
 
 - returns: 用户是否在游客模式 Bool
 */
func getGestureModel()->Bool{
    return UserDefaults.standard.bool(forKey: "isGesture")
}

//MARK: 判断用户是否是第一次载入
/**
 判断用户是否是第一次载入
 
 - returns: Bool
 */
func hasUserLoadAPP()->Bool{
    
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    
    if let result: Bool = UserDefaults.standard.object(forKey: buildVersion) as? Bool {
        return result
    }else{
        return false
    }
}

//MARK: 设置用户是否已经在如果该版本的app
/**
 设置用户是否已经在如果该版本的app
 */
func setUserHasLoadAPP(_ value: Bool = true){
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    UserDefaults.standard.set(value, forKey: buildVersion)
}

//MARK: 用户是否已经看过引导页
/// 用户是否已经看过引导页
///
/// - Returns: Bool
func hasIntroViewShowed()->Bool{
    
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    
    if let result: Bool = UserDefaults.standard.object(forKey: buildVersion + "introView") as? Bool {
        return result
    }else{
        return false
    }
}

//MARK: 设置用户已经看过引导页
/// 设置用户已经看过引导页
///
/// - Parameter value: Bool
func setIntroViewShowed(_ value: Bool = true){
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    UserDefaults.standard.set(value, forKey: buildVersion + "introView")
}


//MARK: 用户忽略获取App更新信息
/**
 用户忽略获取App更新信息
 */
func ignoreAppUpdata(){
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "update"
    UserDefaults.standard.set(true, forKey: key)
}

//MARK:检查用户是否已忽略获取app更新
/**
 检查用户是否已忽略获取app更新
 
 - returns: Bool
 */
func checkAppUpdateOrNot()->Bool{
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "update"
    if let result: Bool = UserDefaults.standard.object(forKey: key) as? Bool {
        return result
    }else{
        return false
    }
}

//MARK: 保存地理位置信息的字符串
/**
 保存地理位置信息的字符串
 
 - parameter str: String
 */
func saveLocationString(str: String){
    UserDefaults.standard.set(str, forKey: "location")
}

//MARK: 获取已保存的地理位置字符串
/**
 获取已保存的地理位置字符串
 
 - returns: String
 */
func getLocationString()->String{
    if let locationStr: String = UserDefaults.standard.object(forKey: "location") as? String{
        return locationStr
    }else{
        return ""
    }
}
//MARK: 是否看过教程
/**
 是否看过教程
 
 - returns: Bool
 */
func getTutorialStatus()->Bool{
    if let result: Bool = UserDefaults.standard.object(forKey: "Tutorial") as? Bool {
        return result
    }else{
        return false
    }
}
//MARK: 设置用户已经看过教程
/**
 设置用户已经看过教程
 */
func setTutorialStatus(){
    UserDefaults.standard.set(true, forKey: "Tutorial")
}

//MARK: 是否看过引导页
/// 是否看过引导页
///
/// - Returns: Bool
func getGuideVCStatus()->Bool{
    if let result: Bool = UserDefaults.standard.object(forKey: "Guide4.2.3") as? Bool {
        return result
    }else{
        return false
    }
//    return false
}
//MARK: 设置用户已经看过引导页
/// 设置用户已经看过引导页
func setGuideVCStatus(result: Bool = true){
    UserDefaults.standard.set(result, forKey: "Guide4.2.3")
}

//MARK: 判断用户是否在使用TminstoreToken
/// 判断用户是否在使用TminstoreToken
///
/// - Returns: Bool
func isUsingTminstoreToken()->Bool{
    return UserDefaults.standard.bool(forKey: "tminstoreToken")
}

//MARK: 设置用户使用过TminstoreToken
/// 设置用户使用过TminstoreToken
func setUsingTminstoreToken(){
    UserDefaults.standard.set(true, forKey: "tminstoreToken")
}

//MARK: 保存邀请码
/**
 保存邀请码
 
 - parameter code: 邀请码
 */
func saveInviterCode(code: String){
    UserDefaults.standard.set(code, forKey: "InviterCode")
}
//MARK: 获取邀请码
/**
 获取邀请码
 
 - returns: String?
 */
func getInviterCodeString()->String{
    if let code: String = UserDefaults.standard.object(forKey: "InviterCode") as? String {
        return code
    }
    return ""
}

//MARK: 在后台做的事
/**
 多线程封装
 
 - parameter inBackground: 在后台做的任务
 - parameter mainQueue:    返回到主线程的任务
 */
func doInBackground(inBackground: (()->())?, backToMainQueue mainQueue: (()->())? = nil){
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: {
        inBackground?()
        DispatchQueue.main.async(execute: {
            mainQueue?()
        })
    })
}

//MARK: 延迟执行某任务
/// 延迟执行某任务
///
/// - Parameters:
///   - seconds: 需要计时的事件
///   - completion: 计时时间到了之后执行的闭包，主线程
///   - completionBackground: 计时时间到了之后执行的闭包，在后台线程
func delay(seconds: Double, completion: (()->())? = nil, completionBackground: (()->())? = nil){
    
    let popTime: DispatchTime = DispatchTime.now() + Double(Int64(Double(NSEC_PER_SEC) * seconds)) / Double(NSEC_PER_SEC)
    
    DispatchQueue.main.asyncAfter(deadline: popTime) { () -> Void in
        completion?()
    }
    
    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).asyncAfter(deadline: popTime) { () -> Void in
        completionBackground?()
    }
}

//MARK: 保存当前社区主页的分页数
/**
 保存当前社区主页的分页数
 
 - parameter num: Int
 */
func savePageNum(num: Int){
    UserDefaults.standard.set(num, forKey: "pageNum")
}

//MARK: 取出当前社区分页数
/**
 取出当前社区分页数
 
 - returns: Int
 */
func getPageNum()->Int{
    return UserDefaults.standard.integer(forKey: "pageNum")
}

//MARK: 保存社区主页最大页数
/**
 保存社区主页最大页数
 
 - parameter maxPageNum: Int
 */
func saveMaxPageNum(maxPageNum: Int){
    UserDefaults.standard.set(maxPageNum, forKey: "maxPageNum")
}

//MARK: 取出社区主页最大页数
/**
 取出社区主页最大页数
 
 - returns: Int
 */
func getMaxPageNum()->Int{
    return UserDefaults.standard.integer(forKey: "maxPageNum")
}

//MARK: app的主色调
/// app的主色调
///
/// - Returns: UIColor
func appThemeColor()->UIColor{
    return UIColor(hexString: "4CDFED")
}

//MARK: 导航栏的底色
/// 导航栏的底色
///
/// - Returns: UIColor
func mainNavigationBarTintColor()->UIColor{
    return UIColor(hexString: "FEFEFE")
}

//MARK: 保存手机验证码
/// 保存手机验证码
///
/// - Parameter code: 验证码
func savePhoneVerificationCode(code: String){
    UserDefaults.standard.set(code, forKey: "verificationCode")
}

//MARK: 取出手机验证码
/// 取出手机验证码
///
/// - Returns: String?
func getPhoneVerificationCode()->String?{
    let code: String? = UserDefaults.standard.object(forKey: "verificationCode") as? String
    return code
}

//MARK: 保存手机号码
/// 保存手机号码
///
/// - Parameter number: 手机号码
func savePhoneNumber(number: String){
    UserDefaults.standard.set(number, forKey: "phoneNumber")
}

//MARK: 获取手机号码
/// 获取已存储的手机号码
///
/// - Returns: String?
func getPhoneNumber()->String?{
    let number: String? = UserDefaults.standard.object(forKey: "phoneNumber") as? String
    return number
}

//MARK: 获取动态启动页的图片
/// 获取动态启动页的网址
///
/// - Returns: URL?
func getLaunchScreenImageURL()->URL?{
    return UserDefaults.standard.url(forKey: "launchScreenImageURL")
}

//MARK: 保存动态启动页的图片
/// 保存动态启动页的图片
///
/// - Parameter imageURLString: 图片地址
func saveLaunchScreenImageURL(imageURLString: String?){
    var url: URL?
    if let str: String = imageURLString{
        url = URL(string: str)
    }else{
        url = nil
    }
    UserDefaults.standard.set(url, forKey: "launchScreenImageURL")
}

//MARK: 获取点击启动页跳转的网页地址
/// 获取点击启动页跳转的网页地址
///
/// - Returns: URL?
func getLaunchScreenWebView()->URL?{
    return UserDefaults.standard.url(forKey: "launchScreenWebViewURL")
}

//MARK: 保存点击启动页跳转的网页
/// 保存点击启动页跳转的网页
///
/// - Parameter urlString: 网页的地址
func saveLaunchScreenWebView(urlString: String?){
    var url: URL?
    if let str: String = urlString{
        url = URL(string: str)
    }else{
        url = nil
    }
    UserDefaults.standard.set(url, forKey: "launchScreenWebViewURL")
}

//MARK: 保存动态启动页开始显示的时间
/// 保存动态启动页开始显示的时间
///
/// - Parameter beginTime: TimeInterval
func saveLaunchScreenBeginTime(beginTime: TimeInterval){
    let beginDate: Date = Date(timeIntervalSince1970: beginTime)
    UserDefaults.standard.set(beginDate, forKey: "launchScreenBeginTime")
}

//MARK: 保存动态启动页结束显示的时间
/// 保存动态启动页结束显示的时间
///
/// - Parameter endTime: TimeInterval
func saveLaunchScreenEndTime(endTime: TimeInterval){
    let beginDate: Date = Date(timeIntervalSince1970: endTime)
    UserDefaults.standard.set(beginDate, forKey: "launchScreenEndTime")
}

//MARK: 保存启动页显示时间
/// 保存启动页显示时间
///
/// - Parameter count: Int
func saveLaucnScreenCountTime(count: TimeInterval){
    UserDefaults.standard.set(count, forKey: "launchScreenCountTime")
}

//MARK: 得到启动页显示时间
/// 得到启动页显示时间
///
/// - Returns: Int?
func getLaunchScreenCountTime()->TimeInterval?{
    return UserDefaults.standard.double(forKey: "launchScreenCountTime")
}

//MARK: 保存最近的定位信息
/// 保存最近的定位信息
///
/// - Parameter location: CLLocation
func saveLastLocation(location: CLLocation){
    //解析出经纬度
    let latlng: String = "\(location.coordinate.latitude),\(location.coordinate.longitude)"
    saveLocationString(str: latlng)//将定位信息保存
//    UserDefaults.standard.set(location, forKey: "lastLoation")
}

//MARK: 取出最近的定位信息
/// 取出最近的定位信息
///
/// - Returns: CLLocation?
func getLastLocation()->String{
    return getLocationString()
}

//MARK: 是否加载过地图介绍页面
/// 是否加载过地图介绍页面
///
/// - Returns: Bool
func hasLoadMapIntroView()->Bool{
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "MapIntroView"
    return UserDefaults.standard.bool(forKey: key)
}

//MARK: 设置已经加载过地图介绍页面
/// 设置已经加载过地图介绍页面
func setHasLoadMapIntroView(){
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "MapIntroView"
    UserDefaults.standard.set(true, forKey: key)
}

//MARK: 是否记载过发布到地图的介绍页面
/// 是否记载过发布到地图的介绍页面
///
/// - Returns: Bool
func hasLoadReleaseToMapInfoImageView()->Bool{
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "releaseToMapInfo"
    return UserDefaults.standard.bool(forKey: key)
}

//MARK: 是否在本地识别界面加载过介绍视图
/// 是否在本地识别界面加载过介绍视图
///
/// - Returns: Bool
func hasLoadLocalNewARIntroView()->Bool{
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "LocalNewARIntroView"
    return UserDefaults.standard.bool(forKey: key)
}

//MARK: 设置已经加载过本地识别界面加载过介绍视图
/// 设置已经加载过本地识别界面加载过介绍视图
func setHasLoadLocalNewARIntroView(){
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "LocalNewARIntroView"
    UserDefaults.standard.set(true, forKey: key)
}

//MARK: 设置已经加载过发布到地图的介绍页面
/// 设置已经加载过发布到地图的介绍页面
func setLoadReleaseToMapInfoImageView(){
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "releaseToMapInfo"
    UserDefaults.standard.set(true, forKey: key)
}

//MARK: 是否已经加载过时光轴介绍图片URL
/// 是否已经加载过时光轴介绍图片URL
///
/// - Returns: Bool
func hasLoadTimeLineRecordImageView()->Bool{
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "timeLineRecord"
    return UserDefaults.standard.bool(forKey: key)
}

//MARK: 设置已经加载过时光轴的介绍页面
/// 设置已经加载过时光轴的介绍页面
func setLoadTimeLineRecordImageView(){
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "timeLineRecord"
    UserDefaults.standard.set(true, forKey: key)
}

//MARK: 是否已经加载过时光轴介绍图片URL
/// 是否已经加载过时光轴介绍图片URL
///
/// - Returns: Bool
func hasLoadTimeLineIntroImageView()->Bool{
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "timeLineIntro"
    return UserDefaults.standard.bool(forKey: key)
}

//MARK: 设置已经加载过时光轴的介绍页面
/// 设置已经加载过时光轴的介绍页面
func setLoadTimeLineIntroImageView(){
    let buildVersion: String = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
    let key: String = buildVersion + "timeLineIntro"
    UserDefaults.standard.set(true, forKey: key)
}

//MARK: 是否看过发布为私人的提示信息
/// 是否看过发布为私人的提示信息
///
/// - Returns: Bool
func hasReleaseToPrivateInfoShowed()->Bool{
    let result: Bool = UserDefaults.standard.bool(forKey: "releaseToPrivate")
    UserDefaults.standard.set(true, forKey: "releaseToPrivate")
    return result
}

//MARK: 是否已经进入过地图
/// 是否已经进入过地图
///
/// - Returns: Bool
func hasLoadMap()->Bool{
    return UserDefaults.standard.bool(forKey: "map")
}

//MARK: 设置用户已经进入过地图
/// 设置用户已经进入过地图
func setHasLoadMap(){
    UserDefaults.standard.set(true, forKey: "map")
}

//MARK: 判读是否在显示动态启动页的区间
/// 判读是否在显示动态启动页的区间
///
/// - Returns: (是否要显示动态启动页， 图片URL， weiView的URL(可能为空)，启动页的显示时间)
func shouldShowLaunchScreen()->(shouldShowLaunchScreen: Bool, imageURL: URL, webViewURL: URL?, timeCount: TimeInterval){
    
    guard let beginDate: Date = UserDefaults.standard.object(forKey: "launchScreenBeginTime") as? Date, let endDate: Date = UserDefaults.standard.object(forKey: "launchScreenEndTime") as? Date, let imageURL: URL = getLaunchScreenImageURL(), let timeCount: TimeInterval = getLaunchScreenCountTime() else { return (false, URL(string: "http://www.timeory.com")!, nil, 0.0) }
    
    let now: Date = Date()
    let webViewURL: URL? = getLaunchScreenWebView()
    
    if now > beginDate && now < endDate{
        return (true, imageURL, webViewURL, timeCount)
    }else{
        return (false, URL(string: "http://www.timeory.com")!, nil, 0)
    }
}

//MARK: 自动约束的动画
/// 自动约束的动画
///
/// - Parameters:
///   - view: 发生动画的View
///   - time: 动画时长，默认0.5秒
///   - delay: 动画延迟， 默认0.0秒
///   - completion: 动画完成的闭包
func autoLayoutAnimation(view: UIView, andAnimationTime time: TimeInterval = 0.5, withDelay delay: TimeInterval = 0.0, completionClosure completion: (()->())? = nil) {
    
    view.setNeedsUpdateConstraints()//约束需要更新
    view.updateConstraintsIfNeeded()//检测需要更新的约束
    
    UIView.animate(withDuration: time, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.curveEaseOut, animations: {
        view.layoutIfNeeded()//强制更新约束
    }, completion: {(_) in
        completion?()
    })
}

//MARK: 设置时间
/// 设置时间
///
/// - Parameters:
///   - date: Date
///   - timeStyle: 时间格式， 默认为none
///   - dateStyle: 日期格式， 默认为medium
/// - Returns: String
func getDateText(date: Date, andTimeStyle timeStyle: DateFormatter.Style = DateFormatter.Style.none, andDateStyle dateStyle: DateFormatter.Style = DateFormatter.Style.medium) ->String{
    
    var timeText: String
    let todayDate: Date = Date()//今天的日期
    var timeInterval: TimeInterval = todayDate.timeIntervalSince(date)//评论的时间间隔
    if timeInterval < 0 {
        timeInterval = 0
    }
    switch timeInterval {
    case 0..<60://一分钟内
        timeText = "\(Int(timeInterval))" + NSLocalizedString("秒", comment: "")
    case 60..<3600://一小时之内
        timeText = "\(Int(timeInterval/60))" + NSLocalizedString("分钟", comment: "")
    case 3600..<86400://二十四小时之内
        timeText = "\(Int(timeInterval/3600))" + NSLocalizedString("小时", comment: "")
    case 86400..<604800://七天之内
        timeText = "\(Int(timeInterval/86400))" + NSLocalizedString("天", comment: "")
    default://超过七天
        let timeFomatter: DateFormatter = DateFormatter()
        timeFomatter.timeStyle = timeStyle
        timeFomatter.dateStyle = dateStyle
        timeText = timeFomatter.string(from: date)
    }
    return timeText
}

///// 正面图片在又拍云的文件夹
//let frontImageUpun: String = "/frontImage/"
///// 背面图片又拍云的文件夹
//let backImageUpun: String = "/backImage/"
///// 地址图片又拍云的文件夹
//let addressImageUpun: String = "/addressImage/"
///// 视频又拍云的文件夹
//let videoUpun: String = "/video/"
///// 头像在又拍云的文件夹
//let portraitUpun: String = "/userLogo/"
//// 动态启动页的网址
//let launchScreenURLStr: String = "http://ucardstorevideo.b0.upaiyun.com/iOSImages/launchScreen.json"
///// 社区正面图片在又拍云的文件夹
//let communityFrontImageUpun: String = "/frontImage/"
///// 社区背面图片在又拍云的文件夹
//let communityBackImageUpun: String = "/backImage/"
///// 社区视频在又拍云的文件夹
//let communityVideoUpun: String = "/video/"
///// 地图图片在有拍云的文件夹
//let mapFrontImageUpun: String = "/map/frontImage/"
///// 地图视频在有拍云的文件夹
//let mapVideoUpun: String = "/map/video/"
///// 地图视频缩略图在有拍云的文件夹
//let mapVideoThumbnailUpun: String = "/map/video/thumbnail/"
///// 地图评论的视频文件夹
//let mapCommentVideoUpun: String = "/map/comment/video/"
///// 地图评论视频的缩略图的文件夹
//let mapCommentThumbnailUpun: String = "/map/comment/thumbnail/"
///// 地图回复的视频文件夹
//let mapReplyVideoUpun: String = "/map/reply/video/"
///// 地图回复的缩略图文件夹
//let mapReplyThumbnailUpun: String = "/map/reply/thumbnail/"
///// 分享的视频存放的文件夹
//let mapShareVideoUpun: String = "/map/share/video/"
///// 分享的视频的缩略图存放的文件夹
//let mapShareVideoThumbnailUpun: String = "/map/share/thumbnail/"
///// 地图截屏图片的存放文件夹
//let mapSnapShotUpun: String = "/map/share/snapShot/"
///// 存放创意图片的文件夹
//let idealUyun: String = "/ideal/"
/// 正面图片在又拍云的文件夹
let frontImageUpun: String = "/test/"
/// 背面图片又拍云的文件夹
let backImageUpun: String = "/test/"
/// 地址图片又拍云的文件夹
let addressImageUpun: String = "/test/"
/// 视频又拍云的文件夹
let videoUpun: String = "/test/"
/// 头像在又拍云的文件夹
let portraitUpun: String = "/test/"
// 动态启动页的网址
let launchScreenURLStr: String = "http://ucardstorevideo.b0.upaiyun.com/test/launchScreenTest.json"
/// 社区正面图片在又拍云的文件夹
let communityFrontImageUpun: String = "/test/"
/// 社区背面图片在又拍云的文件夹
let communityBackImageUpun: String = "/test/"
/// 社区视频在又拍云的文件夹
let communityVideoUpun: String = "/test/"
/// 地图图片在有拍云的文件夹
let mapFrontImageUpun: String = "/test/"
/// 地图视频在有拍云的文件夹
let mapVideoUpun: String = "/test/"
/// 地图视频缩略图在有拍云的文件夹
let mapVideoThumbnailUpun: String = "/test/"
/// 地图评论的视频文件夹
let mapCommentVideoUpun: String = "/test/"
/// 地图评论视频的缩略图的文件夹
let mapCommentThumbnailUpun: String = "/test/"
/// 地图回复的视频文件夹
let mapReplyVideoUpun: String = "/test/"
/// 地图回复的缩略图文件夹
let mapReplyThumbnailUpun: String = "/test/"
/// 分享的视频存放的文件夹
let mapShareVideoUpun: String = "/test/"
/// 分享的视频的缩略图存放的文件夹
let mapShareVideoThumbnailUpun: String = "/test/"
/// 地图截屏图片的存放文件夹
let mapSnapShotUpun: String = "/test/"
/// 存放创意图片的文件夹
let idealUyun: String = "/test/"
///AR卡片的定价
let postcardPrice: Double = 5.00
