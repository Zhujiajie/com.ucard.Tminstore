//
//  StorePurchaseChannelView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/2.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class StorePurchaseChannelView: UIView {

    var postcardModel: MUFinalPostcardModel?
    
    ///微信ICON
    fileprivate var weChatIcon: BaseImageView?
    /// 微信名称
    fileprivate var weChatTitleLabel: BaseLabel?
    /// 微信确认ICON
    fileprivate var weChatConfirmIcon: BaseImageView?
    /// 微信确认的按钮
    fileprivate var weChatConfirmButton: BaseButton?
    
    ///支付宝ICON
    fileprivate var aliPayIcon: BaseImageView?
    /// 支付宝名称
    fileprivate var aliPayTitleLabel: BaseLabel?
    /// 支付宝确认ICON
    fileprivate var aliPayConfirmIcon: BaseImageView?
    /// 支付宝确认的按钮
    fileprivate var aliPayConfirmButton: BaseButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        weChatIcon = BaseImageView.normalImageView(andImageName: "weChatPay")
        addSubview(weChatIcon!)
        weChatIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self)
            make.left.equalTo(self).offset(24/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 35/baseWidth))
        })
        
        weChatTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "ABABAB", andText: NSLocalizedString("微信支付", comment: ""))
        addSubview(weChatTitleLabel!)
        weChatTitleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(weChatIcon!)
            make.left.equalTo(weChatIcon!.snp.right).offset(36/baseWidth)
        })
        
        weChatConfirmIcon = BaseImageView.normalImageView(andImageName: "paySelected")
        addSubview(weChatConfirmIcon!)
        weChatConfirmIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(weChatIcon!)
            make.right.equalTo(self).offset(-22/baseWidth)
            make.size.equalTo(CGSize(width: 25/baseWidth, height: 25/baseWidth))
        })
        
        weChatConfirmButton = BaseButton.normalIconButton()
        addSubview(weChatConfirmButton!)
        weChatConfirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.equalTo(weChatIcon!)
            make.rightMargin.equalTo(weChatConfirmIcon!)
            make.height.equalTo(49/baseWidth)
        })
        //MARK: 点击切换到微信支付
        weChatConfirmButton?.touchButtonClosure = {[weak self] _ in
            self?.postcardModel?.payModel.payStyle = PayStyle.weChat
            self?.weChatConfirmIcon?.image = UIImage(named: "paySelected")
            self?.aliPayConfirmIcon?.image = UIImage(named: "payDeselected")
        }
        
        aliPayIcon = BaseImageView.normalImageView(andImageName: "aliPay")
        addSubview(aliPayIcon!)
        aliPayIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(weChatIcon!)
            make.bottom.equalTo(self)
            make.top.equalTo(weChatIcon!.snp.bottom).offset(29.5/baseWidth)
            make.size.equalTo(weChatIcon!)
        })
        
        aliPayTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "ABABAB", andText: NSLocalizedString("支付宝支付", comment: ""))
        addSubview(aliPayTitleLabel!)
        aliPayTitleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(aliPayIcon!)
            make.leftMargin.equalTo(weChatTitleLabel!)
        })
        
        aliPayConfirmIcon = BaseImageView.normalImageView(andImageName: "payDeselected")
        addSubview(aliPayConfirmIcon!)
        aliPayConfirmIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(aliPayIcon!)
            make.rightMargin.equalTo(weChatConfirmIcon!)
            make.size.equalTo(weChatConfirmIcon!)
        })
        
        aliPayConfirmButton = BaseButton.normalIconButton()
        addSubview(aliPayConfirmButton!)
        aliPayConfirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(weChatConfirmButton!.snp.bottom)
            make.left.bottom.equalTo(aliPayIcon!)
            make.right.equalTo(aliPayConfirmIcon!)
        })
        //MARK: 点击切换到支付宝支付
        aliPayConfirmButton?.touchButtonClosure = {[weak self] _ in
            self?.postcardModel?.payModel.payStyle = PayStyle.aliPay
            self?.weChatConfirmIcon?.image = UIImage(named: "payDeselected")
            self?.aliPayConfirmIcon?.image = UIImage(named: "paySelected")
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
