//
//  StorePurchaseView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/2.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class StorePurchaseView: UIView {

    /// 数据源
    var payModel: PayModel?{
        didSet{
            setPrice()
            if payModel != oldValue{
                payModel?.addObserver(self, forKeyPath: "freightCharge", options: NSKeyValueObservingOptions.new, context: nil)//观察运费的变化
            }
        }
    }
    
    /// 点击去支付按钮的事件
    var touchConfirmButtonClosure: (()->())?
    
    /// 价格的label
    fileprivate var priceLabel: BaseLabel?
    /// 确认支付的button
    fileprivate var confirmButton: BaseButton?
    /// 运费lable
    fileprivate var shippingChargeLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 2)
        layer.shadowRadius = 4
        
        confirmButton = BaseButton.normalTitleButton(title: NSLocalizedString("去支付", comment: ""), andTextFont: UIFont.systemFont(ofSize: 13), andTextColorHexString: "FFFFFF", andBackgroundColorHexString: "14D9DA")
        addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.bottom.equalTo(self)
            make.width.equalTo(127/baseWidth)
        })
        confirmButton?.touchButtonClosure = {[weak self] _ in
            self?.touchConfirmButtonClosure?()
        }
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "E0B347")
        addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(self)
            make.left.equalTo(self).offset(24/baseWidth)
        })
        
        shippingChargeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "CBCBCB", andText: NSLocalizedString("(含邮费)", comment: ""))
        addSubview(shippingChargeLabel!)
        shippingChargeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(priceLabel!)
            make.left.equalTo(priceLabel!.snp.right).offset(5/baseWidth)
        })
        shippingChargeLabel?.isHidden = true
        
    }
    
    //MARK: 设置价格
    fileprivate func setPrice(){
        priceLabel?.text = NSLocalizedString("实付", comment: "") + "¥ \(payModel!.priceNeedToPay)"
        if payModel?.isMailAvailable == true{
            if payModel?.freightCharge != 0.0{
                shippingChargeLabel?.isHidden = false
            }else{
                shippingChargeLabel?.isHidden = true
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "freightCharge"{
            setPrice()
        }
    }
    
    deinit {
        payModel?.removeObserver(self, forKeyPath: "freightCharge")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
