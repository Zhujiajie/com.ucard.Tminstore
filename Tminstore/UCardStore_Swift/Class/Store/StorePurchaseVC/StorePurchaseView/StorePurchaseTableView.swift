//
//  StorePurchaseTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/2.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StorePurchaseTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        register(StorePurchaseAddressCell.self, forCellReuseIdentifier: "0")
        register(StorePurchasePriceCell.self, forCellReuseIdentifier: "1")
        register(StorePurchasePriceCell.self, forCellReuseIdentifier: "2")
        register(StorePurchaseCouponCell.self, forCellReuseIdentifier: "3")
        register(StorePurchasePriceCell.self, forCellReuseIdentifier: "4")
        register(StorePurchaseFreightCell.self, forCellReuseIdentifier: "5")
        register(StorePurchasePayChannelCell.self, forCellReuseIdentifier: "6")
        register(StorePurchasePreviewCell.self, forCellReuseIdentifier: "7")
        tableFooterView = UIView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
