//
//  StorePurchaseFreightView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/5.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 确认运送方式的视图
import UIKit
import SnapKit

class StorePurchaseFreightView: UIView {
    
    var isFirstTimeSet: Bool = true
    
    var postcardModel: MUFinalPostcardModel?{
        didSet{
            if postcardModel?.payModel.isMailAvailable == true{
                if isFirstTimeSet {
                    expressConfirmButton?.sendActions(for: UIControlEvents.touchUpInside)
                    isFirstTimeSet = false
                    return
                }
                if postcardModel?.payModel.freightCharge == 0{
                    mailConfirmButton?.sendActions(for: UIControlEvents.touchUpInside)
                }else{
                    expressConfirmButton?.sendActions(for: UIControlEvents.touchUpInside)
                }
            }
        }
    }
    
    /// 快递label
    fileprivate var expressLabel: BaseLabel?
    fileprivate var expressPriceLabel: BaseLabel?
    fileprivate var expressConfirmIcon: BaseImageView?
    fileprivate var expressConfirmButton: BaseButton?
    
    /// 平邮
    fileprivate var mailLabel: BaseLabel?
    fileprivate var mailPriceLabel: BaseLabel?
    fileprivate var mailConfirmIcon: BaseImageView?
    fileprivate var mailConfirmButton: BaseButton?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        expressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "ABABAB", andText: NSLocalizedString("快递", comment: ""))
        addSubview(expressLabel!)
        expressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(21)
            make.left.equalTo(self).offset(24/baseWidth)
            make.width.equalTo(64/baseWidth)
        })
        
        expressPriceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "E0B347", andText: "¥ 15")
        addSubview(expressPriceLabel!)
        expressPriceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(expressLabel!)
            make.left.equalTo(expressLabel!.snp.right)
        })
        
        expressConfirmIcon = BaseImageView.normalImageView(andImageName: "paySelected")
        addSubview(expressConfirmIcon!)
        expressConfirmIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(expressLabel!)
            make.right.equalTo(self).offset(-22/baseWidth)
            make.size.equalTo(CGSize(width: 25/baseWidth, height: 25/baseWidth))
        })
        
        expressConfirmButton = BaseButton.normalIconButton()
        addSubview(expressConfirmButton!)
        expressConfirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self)
            make.left.equalTo(expressLabel!)
            make.right.equalTo(expressConfirmIcon!)
            make.bottom.equalTo(expressLabel!).offset(11.5)
        })
        //MARK: 点击快递按钮
        expressConfirmButton?.touchButtonClosure = {[weak self] _ in
            self?.expressConfirmIcon?.image = UIImage(named: "paySelected")
            self?.mailConfirmIcon?.image = UIImage(named: "payDeselected")
            self?.postcardModel?.payModel.freightCharge = 15.0
        }
        
        mailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "ABABAB", andText: NSLocalizedString("平邮", comment: ""))
        addSubview(mailLabel!)
        mailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self).offset(-23)
            make.leftMargin.equalTo(expressLabel!)
            make.width.equalTo(expressLabel!)
        })
        
        mailPriceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "E0B347", andText: "免费")
        addSubview(mailPriceLabel!)
        mailPriceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(mailLabel!)
            make.left.equalTo(mailLabel!.snp.right)
        })
        
        mailConfirmIcon = BaseImageView.normalImageView(andImageName: "payDeselected")
        addSubview(mailConfirmIcon!)
        mailConfirmIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(mailLabel!)
            make.rightMargin.equalTo(expressConfirmIcon!)
            make.size.equalTo(expressConfirmIcon!)
        })
        
        mailConfirmButton = BaseButton.normalIconButton()
        addSubview(mailConfirmButton!)
        mailConfirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self)
            make.left.equalTo(mailLabel!)
            make.right.equalTo(mailConfirmIcon!)
            make.top.equalTo(expressConfirmButton!.snp.bottom)
        })
        //MARK: 点击平邮事件
        mailConfirmButton?.touchButtonClosure = {[weak self] _ in
            self?.expressConfirmIcon?.image = UIImage(named: "payDeselected")
            self?.mailConfirmIcon?.image = UIImage(named: "paySelected")
            self?.postcardModel?.payModel.freightCharge = 0.0
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
