//
//  StorePurchaseTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/2.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class StorePurchaseAddressCell: UITableViewCell {

    /// 地址数据
    var addressModel: AddressModel?{
        didSet{
            if addressModel != nil{
                noAddressLabel?.isHidden = true
                nameLabel?.text = addressModel?.mailName
                phoneNumberLabel?.text = addressModel?.phoneNumber
                detailAddressLabel?.text = addressModel?.detailedAddress
            }
        }
    }
    
    /// 没有选择地址时展示的label
    fileprivate var noAddressLabel: BaseLabel?
    /// 收件人label
    fileprivate var nameLabel: BaseLabel?
    /// 电话号码的label
    fileprivate var phoneNumberLabel: BaseLabel?
    /// 详细地址的label
    fileprivate var detailAddressLabel: BaseLabel?

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let grayRightArrow: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "grayRightArrow")
        contentView.addSubview(grayRightArrow)
        grayRightArrow.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-18/baseWidth)
            make.size.equalTo(CGSize(width: 10/baseWidth, height: 15/baseWidth))
        }
        
        noAddressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "525252", andText: NSLocalizedString("请添加收货地址", comment: ""))
        contentView.addSubview(noAddressLabel!)
        noAddressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(24/baseWidth)
        })
        
        phoneNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "0B0B0B", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(phoneNumberLabel!)
        phoneNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(grayRightArrow.snp.left).offset(-25/baseWidth)
            make.top.equalTo(contentView).offset(8/baseWidth)
            make.height.equalTo(15)
        })
        
        nameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "0B0B0B")
        contentView.addSubview(nameLabel!)
        nameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(phoneNumberLabel!)
            make.left.equalTo(contentView).offset(24/baseWidth)
            make.right.equalTo(phoneNumberLabel!.snp.left).offset(-5/baseWidth)
            make.height.equalTo(phoneNumberLabel!)
        })
        
        detailAddressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "9D9D9D", andNumberOfLines: 2)
        contentView.addSubview(detailAddressLabel!)
        detailAddressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nameLabel!.snp.bottom).offset(7/baseWidth)
            make.leftMargin.equalTo(nameLabel!)
            make.rightMargin.equalTo(phoneNumberLabel!)
            make.bottom.equalTo(contentView).offset(-10/baseWidth)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示商品信息的cell
class StorePurchasePriceCell: UITableViewCell {
    
    var postcardModel: MUFinalPostcardModel?
    var indexPath: IndexPath?{
        didSet{
            if indexPath == nil{return}
            switch indexPath!.row {
            case 0://图片价格
                titleLabel?.font = UIFont.systemFont(ofSize: 14)
                titleLabel?.textColor = UIColor(hexString: "ABABAB")
                priceLabel?.font = UIFont.systemFont(ofSize: 14)
                priceLabel?.textColor = UIColor(hexString: "E0B347")
                titleLabel?.text = NSLocalizedString("买图", comment: "")
                priceLabel?.text = "¥ \(postcardModel!.payModel.imagePrice)"
            case 1://实物价格
                titleLabel?.font = UIFont.systemFont(ofSize: 14)
                titleLabel?.textColor = UIColor(hexString: "ABABAB")
                priceLabel?.font = UIFont.systemFont(ofSize: 14)
                priceLabel?.textColor = UIColor(hexString: "E0B347")
                switch postcardModel!.goods!.goodsType {
                case CustomizationType.pillow:
                    titleLabel?.text = NSLocalizedString("AR抱枕价格", comment: "")
                case CustomizationType.postcard:
                    titleLabel?.text = NSLocalizedString("AR明信片价格", comment: "")
                case CustomizationType.tShirt:
                    titleLabel?.text = NSLocalizedString("AR T恤价格", comment: "")
                default:
                    titleLabel?.text = NSLocalizedString("AR马克杯价格", comment: "")
                }
                priceLabel?.text = "¥ \(postcardModel!.payModel.goodsPrice * Double(postcardModel!.payModel.orderNumber))"
            default://商品总价
                titleLabel?.text = NSLocalizedString("商品合计", comment: "")
                priceLabel?.text = "¥ \(postcardModel!.payModel.orderTotalAmount)"
            }
        }
    }
    
    /// 商品价格
    fileprivate var titleLabel: BaseLabel?
    /// 价格label
    fileprivate var priceLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.layer.masksToBounds = true
        selectionStyle = UITableViewCellSelectionStyle.none
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "FF7337", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-21/baseWidth)
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "525252")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(24/baseWidth)
            make.right.equalTo(priceLabel!.snp.left).offset(-5/baseWidth)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 选择代金券的cell
class StorePurchaseCouponCell: UITableViewCell{
    
    var couponCount: Int = 0{
        didSet{
            if couponModel != nil {return}
            priceLabel?.text = "\(couponCount) " + NSLocalizedString("张优惠券可用", comment: "")
        }
    }
    
    var couponModel: CouponModel?{
        didSet{
            if couponModel == nil {return}
            priceLabel?.text = "-¥ \(couponModel!.couponPromotion)"
        }
    }
    
    /// 商品价格
    fileprivate var titleLabel: BaseLabel?
    /// 价格label
    fileprivate var priceLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.layer.masksToBounds = true
        
        let rightArrow: BaseImageView = BaseImageView.normalImageView(andImageName: "grayRightArrow")
        contentView.addSubview(rightArrow)
        rightArrow.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-22/baseWidth)
            make.size.equalTo(CGSize(width: 7, height: 11))
        }
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "E0B347", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(rightArrow.snp.left).offset(-19/baseWidth)
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "ABABAB", andText: NSLocalizedString("优惠券", comment: ""))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(24/baseWidth)
            make.right.equalTo(priceLabel!.snp.left).offset(-5/baseWidth)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 选择运送方式的cell
class StorePurchaseFreightCell: UITableViewCell{
    
    var postcardModel: MUFinalPostcardModel?{
        didSet{
            freightView?.postcardModel = postcardModel
        }
    }
    
    fileprivate var titleLabel: BaseLabel?
    fileprivate var freightView: StorePurchaseFreightView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.layer.masksToBounds = true
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "525252", andText: NSLocalizedString("邮寄方式", comment: ""))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(21.5/baseWidth)
            make.left.equalTo(contentView).offset(24/baseWidth)
        })
        
        freightView = StorePurchaseFreightView()
        contentView.addSubview(freightView!)
        freightView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom)
            make.left.right.equalTo(contentView)
            make.size.equalTo(CGSize(width: 333/baseWidth, height: 101))
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 选择支付渠道的cell
class StorePurchasePayChannelCell: UITableViewCell{
    
    var postcardModel: MUFinalPostcardModel?{
        didSet{
            payChannelView?.postcardModel = postcardModel
        }
    }
    
    fileprivate var titleLabel: BaseLabel?
    fileprivate var payChannelView: StorePurchaseChannelView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = UITableViewCellSelectionStyle.none
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "525252", andText: NSLocalizedString("支付方式", comment: ""))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(21.5/baseWidth)
            make.left.equalTo(contentView).offset(24/baseWidth)
        })
        
        payChannelView = StorePurchaseChannelView()
        contentView.addSubview(payChannelView!)
        payChannelView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(29/baseWidth)
            make.left.right.equalTo(contentView)
            make.size.equalTo(CGSize(width: 333/baseWidth, height: 98))
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 预览商品图片的cell
class StorePurchasePreviewCell: UITableViewCell{
    
    var postcardModel: MUFinalPostcardModel?{
        didSet{
            previewImageView?.image = postcardModel?.previewImage
            titleLabel?.text = postcardModel!.payModel.goodsName
            numberLabel01?.text = "x \(postcardModel!.payModel.orderNumber)"
            priceNeedToPayLabel01?.text = "¥\(postcardModel!.payModel.goodsTotalPrice)"
            if postcardModel?.goods?.goodsType == CustomizationType.postcard{//设置阴影
            }else{
                shadowView?.isHidden = true
            }
        }
    }
    /// 阴影视图
    fileprivate var shadowView: BaseView?
    /// 商品预览图片
    fileprivate var previewImageView: BaseImageView?
    /// 商品标题
    fileprivate var titleLabel: BaseLabel?
    /// 商品数量
    fileprivate var numberLabel: BaseLabel?
    /// 商品数量的值
    fileprivate var numberLabel01: BaseLabel?
    /// 价格
    fileprivate var priceNeedToPayLabel: BaseLabel?
    /// 价格的值
    fileprivate var priceNeedToPayLabel01: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = UITableViewCellSelectionStyle.none
        
        shadowView = BaseView.shadowView()
        contentView.addSubview(shadowView!)
        shadowView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(26.5/baseWidth)
            make.left.equalTo(contentView).offset(21/baseWidth)
            make.size.equalTo(CGSize(width: 110, height: 80))
        })
        
        previewImageView = BaseImageView.normalImageView()
        contentView.addSubview(previewImageView!)
        previewImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(26.5/baseWidth)
            make.left.equalTo(contentView).offset(21/baseWidth)
            make.size.equalTo(CGSize(width: 110, height: 80))
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "545454")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(previewImageView!)
            make.left.equalTo(previewImageView!.snp.right).offset(12/baseWidth)
        })
        
        numberLabel01 = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "707070", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(numberLabel01!)
        numberLabel01?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(9/baseWidth)
            make.right.equalTo(contentView).offset(-18/baseWidth)
        })
        numberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "707070", andText: NSLocalizedString("数量", comment: ""))
        contentView.addSubview(numberLabel!)
        numberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(numberLabel01!)
            make.leftMargin.equalTo(titleLabel!)
            make.right.equalTo(numberLabel01!.snp.left)
        })
        
        priceNeedToPayLabel01 = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "707070", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(priceNeedToPayLabel01!)
        priceNeedToPayLabel01?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(numberLabel!.snp.bottom).offset(2/baseWidth)
            make.rightMargin.equalTo(numberLabel01!)
        })
        priceNeedToPayLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "707070", andText: NSLocalizedString("合计", comment: ""))
        contentView.addSubview(priceNeedToPayLabel!)
        priceNeedToPayLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(priceNeedToPayLabel01!)
            make.leftMargin.equalTo(titleLabel!)
            make.right.equalTo(priceNeedToPayLabel01!.snp.left)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
