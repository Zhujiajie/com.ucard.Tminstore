//
//  StoreConfirmViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 确认订单的界面

import UIKit
import SnapKit

class StoreConfirmViewController: BaseViewController {

    fileprivate let viewModel: StoreNewPurchaseViewModel = StoreNewPurchaseViewModel()
    fileprivate let couponViewModel: MUCouponViewModel = MUCouponViewModel()
    
    var postcardModel: MUFinalPostcardModel?
    
    fileprivate var tableViewController: StoreConfirmTableViewController?
    /// 确认支付的视图
    fileprivate var confirmView: StoreNewPurchaseView?
    /// 是否已经获取过代金券数量
    fileprivate var hasRequestCouponCount: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableViewController?.tableView.reloadData()
        confirmView?.payModel = postcardModel!.payModel
        if hasRequestCouponCount == false{
            hasRequestCouponCount = true
            couponViewModel.getCouponList(index: 1)//获取优惠券数量
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        confirmView = StoreNewPurchaseView()
        view.addSubview(confirmView!)
        confirmView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(49/baseWidth)
        })
        
        tableViewController = StoreConfirmTableViewController(style: UITableViewStyle.plain)
        tableViewController?.postcardModel = postcardModel
        view.insertSubview(tableViewController!.view, belowSubview: confirmView!)
        tableViewController?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(confirmView!.snp.top)
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closure(){
        
        //MARK: 点击cell的事件
        tableViewController?.selectCell = {[weak self] (indexPath: IndexPath) in
            switch indexPath.section {
            case 0:
                self?.enterAddressListVC()//选择地址
            case 1:
                switch indexPath.row {
                case 2:
                    self?.enterCouponVC()
                default:
                    break
                }
            default:
                break
            }
        }
        
        //MARK: 获得优惠券数量
        couponViewModel.totalCoupon = {[weak self] (count: Int, _) in
            self?.tableViewController?.setCouponCount(couponCount: count)
        }
        
        //MARK: 获取优惠券信息失败
        couponViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击提交订单按钮的事件
        confirmView?.touchConfirmButtonClosure = {[weak self] _ in
            MobClick.event("makePayment")
            if self?.postcardModel?.addressModel == nil{
                self?.alert(alertTitle: NSLocalizedString("请先选择地址", comment: ""))
            }else if self?.postcardModel?.goods?.goodsType == CustomizationType.postcard && self?.postcardModel?.addressModel?.postcode == "" && self?.postcardModel?.payModel.freightCharge == 0{
                self?.alert(alertTitle: NSLocalizedString("请先填写邮编", comment: ""), leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("好的", comment: ""),  rightClosure: {
                    self?.enterEditAddressVC()
                })
            }else{
                self?.viewModel.uploadOrderInfo(postcardModel: (self?.postcardModel)!)
            }
        }
        
        //MARK: 上传订单成功
        viewModel.uploadOrderSuccess = {[weak self] _ in
            let vc: StoreChoosePaymentViewController = StoreChoosePaymentViewController()
            vc.postcardModel = self?.postcardModel
            vc.umengViewName = "收银台"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 进入地址编辑界面
    fileprivate func enterAddressListVC(){
        let vc: MUAddressListController = MUAddressListController()
        vc.umengViewName = "地址列表"
        vc.postcard = postcardModel
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 进入优惠券界面
    fileprivate func enterCouponVC(){
        let couponVC: MUCouponVC = MUCouponVC()
        _ = navigationController?.pushViewController(couponVC, animated: true)
        //MARK: 使用优惠券成功，返回刷新数据
        couponVC.useCouponSuccess = {[weak self] (coupon: CouponModel) in
            self?.postcardModel?.payModel.couponModel = coupon
            self?.tableViewController?.tableView.reloadData()
            self?.tableViewController?.setDiscount(couponModel: coupon)
        }
    }
    
    //MARK: 进入地址编辑界面
    fileprivate func enterEditAddressVC(){
        let eaVC: MUEditAddressController = MUEditAddressController()
        eaVC.umengViewName = "编辑地址"
        eaVC.addressModel = postcardModel?.addressModel?.mutableCopy() as? AddressModel
        eaVC.addOrEditAddressSuccess = {[weak self] _ in
            self?.postcardModel?.addressModel = eaVC.addressModel
            self?.tableViewController?.tableView.reloadData()
        }
        navigationController?.pushViewController(eaVC, animated: true)
    }
}
