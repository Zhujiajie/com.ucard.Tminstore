//
//  StoreChoosePaymentTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreChoosePaymentTableViewController: UITableViewController {

    weak var postcardModel: MUFinalPostcardModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(StoreChoosePaymentTableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        self.tableView.tableFooterView = UIView()
    }

    //MARK: 选择默认的支付渠道
    func setDefaultPayChannel(){
        var indexPath: IndexPath
        switch postcardModel!.payModel.payStyle {
        case PayStyle.weChat:
            indexPath = IndexPath.init(row: 0, section: 0)
        default:
            indexPath = IndexPath.init(row: 1, section: 0)

//        case PayStyle.aliPay:
//            indexPath = IndexPath.init(row: 1, section: 0)
//        default:
//            indexPath = IndexPath.init(row: 2, section: 0)
        }
        if let cell: StoreChoosePaymentTableViewCell = self.tableView.cellForRow(at: indexPath) as? StoreChoosePaymentTableViewCell{
            cell.selectOrDeselectCell(isSelect: true)
        }
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: StoreChoosePaymentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! StoreChoosePaymentTableViewCell

        cell.indexPath = indexPath

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //切换cell的选中状态
        for cell: UITableViewCell in self.tableView.visibleCells {
            if let cell: StoreChoosePaymentTableViewCell = cell as? StoreChoosePaymentTableViewCell{
                cell.selectOrDeselectCell(isSelect: cell.indexPath == indexPath)
            }
        }
        switch indexPath.row {
        case 0:
            postcardModel?.payModel.payStyle = PayStyle.weChat
        default:
            postcardModel?.payModel.payStyle = PayStyle.aliPay

//        case 1:
//            postcardModel?.payModel.payStyle = PayStyle.aliPay
//        default:
//            postcardModel?.payModel.payStyle = PayStyle.balance
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 72.5/baseWidth
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4
    }
}

//MARK: 支付渠道的Cell
class StoreChoosePaymentTableViewCell: UITableViewCell{
    
    var indexPath: IndexPath?{
        didSet{
            setICONAndTitle()
        }
    }
    
    /// 支付渠道的ICON
    fileprivate var imageICON: BaseImageView?
    /// 支付渠道的label
    fileprivate var channelLabel: BaseLabel?
    /// 是否被选中的icon
    fileprivate var selectImageView: BaseImageView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imageICON = BaseImageView.normalImageView()
        contentView.addSubview(imageICON!)
        imageICON?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(22/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        })
        
        channelLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "585858")
        contentView.addSubview(channelLabel!)
        channelLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(imageICON!)
            make.left.equalTo(imageICON!.snp.right).offset(36/baseWidth)
        })
        
        selectImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "paymentDeselct")
        contentView.addSubview(selectImageView!)
        selectImageView?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-27/baseWidth)
            make.size.equalTo(CGSize(width: 20/baseWidth, height: 20/baseWidth))
        })
    }
    
    //MARK: 选择或取消选择cell
    /// 选择或取消选择cell
    ///
    /// - Parameter isSelect: 是否是选中
    func selectOrDeselectCell(isSelect: Bool) {
        var imageName: String
        if isSelect {
            imageName = "paymentSelect"
        }else{
            imageName = "paymentDeselct"
        }
        selectImageView?.image = UIImage(named: imageName)
    }
    
    //MARK: 设置ICON和标题
    fileprivate func setICONAndTitle(){
        var imageName: String
        var titleText: String
        switch indexPath!.row {
        case 0:
            imageName = "weChatPay"
            titleText = NSLocalizedString("微信支付", comment: "")
        default:
            imageName = "aliPay"
            titleText = NSLocalizedString("支付宝支付", comment: "")

            
            
            
//        case 1:
//            imageName = "aliPay"
//            titleText = NSLocalizedString("支付宝支付", comment: "")
//        default:
//            imageName = "balancePay"
//            titleText = NSLocalizedString("余额支付", comment: "")
        }
        imageICON?.image = UIImage(named: imageName)
        channelLabel?.text = titleText
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
