//
//  StoreChoosePaymentViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 选择支付渠道的控制器

import UIKit
import SnapKit

class StoreChoosePaymentViewController: BaseViewController {

    fileprivate let viewModel: StoreNewPurchaseViewModel = StoreNewPurchaseViewModel()
    fileprivate let paymentPasswordViewModel: MUPaymentManagementViewModel = MUPaymentManagementViewModel()
    
    var postcardModel: MUFinalPostcardModel?
    
    /// 确认支付的视图
    fileprivate var confirmView: StoreNewPurchaseView?
    /// 展示支付渠道的tableViewControl
    fileprivate var tableViewController: StoreChoosePaymentTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        confirmView?.payModel = postcardModel!.payModel
        postcardModel?.payModel.payStyle = PayStyle.weChat
        tableViewController?.setDefaultPayChannel()//设定默认的支付渠道
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        setNavigationTitle(NSLocalizedString("收银台", comment: ""))
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        confirmView = StoreNewPurchaseView()
        view.addSubview(confirmView!)
        confirmView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(49/baseWidth)
        })
        confirmView?.changeConfirmButtomTitle()
        
        tableViewController = StoreChoosePaymentTableViewController()
        tableViewController?.postcardModel = postcardModel
        view.insertSubview(tableViewController!.view, belowSubview: confirmView!)
        tableViewController?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(confirmView!.snp.top)
        })
    }

    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击支付按钮
        confirmView?.touchConfirmButtonClosure = {[weak self] _ in
            //对于余额支付, 先判断是否设置过支付密码
            switch (self?.postcardModel?.payModel.payStyle)! {
            case PayStyle.balance:
                self?.paymentPasswordViewModel.checkPaymentPasswordStatus()
            default:
                self?.viewModel.requestPaymentInfo(payModel: (self?.postcardModel?.payModel)!)
            }
        }
        
        //MARK: 支付成功
        viewModel.uploadOrderSuccess = {[weak self] _ in
            let vc: StorePaySuccessViewController = StorePaySuccessViewController()
            vc.postcardModel = self?.postcardModel
            vc.umengViewName = "支付成功"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 网络出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 判断支付密码状态成功
        paymentPasswordViewModel.checkPaymentPassword = {[weak self] (result: Bool) in
            if result {
                self?.enterPaymentPasswordAlert()//弹出输入支付密码的弹窗
            }else{
                self?.shouldSetPaymentPassword()//需要设置支付密码
            }
        }
        
        //MARK: 出错的闭包
        paymentPasswordViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 提醒用户去设置支付密码
    fileprivate func shouldSetPaymentPassword(){
        alert(alertTitle: NSLocalizedString("您还未设置支付密码\n请先设置支付密码再操作", comment: ""), messageString: "", leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("立即设置", comment: ""), leftClosure: nil, rightClosure: { [weak self] _ in
            let vc: MUPaymentManagementViewController = MUPaymentManagementViewController()
            vc.umengViewName = "支付管理"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }, presentComplition: nil)
    }
    
    //MARK: 用户输入支付密码的弹窗
    fileprivate func enterPaymentPasswordAlert(){
        let ac: UIAlertController = UIAlertController(title: NSLocalizedString("请输入支付密码", comment: ""), message: NSLocalizedString("需要付款", comment: "") + " ¥ \(postcardModel!.payModel.pingxxAmount/100)", preferredStyle: UIAlertControllerStyle.alert)
        
        let cancel: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        ac.addAction(cancel)
        
        //添加一个输入框
        ac.addTextField { (textField: UITextField) in
            textField.isSecureTextEntry = true
        }
        
        //确认支付的按钮
        let confirmAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("立即支付", comment: ""), style: UIAlertActionStyle.default) { [weak self] (_) in
            if let textField: UITextField = ac.textFields?.first, textField.text != nil {
                self?.viewModel.requestPaymentInfo(payModel: (self?.postcardModel?.payModel)!, andPaymentPassword: textField.text!)
            }
        }
        ac.addAction(confirmAction)
        
        present(ac, animated: true, completion: nil)
    }
}
