//
//  StoreConfirmTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

fileprivate let addressCell: String = "addressCell"
fileprivate let priceCell: String = "priceCell"
fileprivate let freightCell: String = "freightCell"
fileprivate let previewCell: String = "previewCell"

class StoreConfirmTableViewController: UITableViewController {

    /// 选择了cell
    var selectCell: ((_ indexPath: IndexPath)->())?
    
    /// 数据源
    weak var postcardModel: MUFinalPostcardModel?
    
//    override init(style: UITableViewStyle) {
//        super.init(style: style)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    //MARK: 设置展示代金券数量
    /// 设置展示代金券数量
    ///
    /// - Parameter couponCount: 代金券数量
    func setCouponCount(couponCount: Int){
        if let cell: StoreConfirmTableViewPriceDetailCell = self.tableView.cellForRow(at: IndexPath.init(row: 2, section: 1)) as? StoreConfirmTableViewPriceDetailCell{
            cell.setCouponNumber(couponCount: couponCount)
        }
    }
    
    //MARK: 设置代金券优惠金额
    /// 设置代金券优惠金额
    ///
    /// - Parameter couponModel: CouponModel
    func setDiscount(couponModel: CouponModel){
        if let cell: StoreConfirmTableViewPriceDetailCell = self.tableView.cellForRow(at: IndexPath.init(row: 2, section: 1)) as? StoreConfirmTableViewPriceDetailCell{
            cell.setDiscount(couponModel: couponModel)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(StoreConfirmTableViewAddressCell.self, forCellReuseIdentifier: addressCell)
        self.tableView.register(StoreConfirmTableViewPriceDetailCell.self, forCellReuseIdentifier: priceCell)
        self.tableView.register(StoreConfirmTableViewFreightCell.self, forCellReuseIdentifier: freightCell)
        self.tableView.register(StoreConfirmTableViewPreviewCell.self, forCellReuseIdentifier: previewCell)
        
        self.tableView.tableFooterView = UIView()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return 4
        default:
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0://地址
            let cell: StoreConfirmTableViewAddressCell = tableView.dequeueReusableCell(withIdentifier: addressCell, for: indexPath) as! StoreConfirmTableViewAddressCell
            
            cell.addressModel = postcardModel?.addressModel
            
            return cell
            
        case 1://价格详情
            let cell: StoreConfirmTableViewPriceDetailCell = tableView.dequeueReusableCell(withIdentifier: priceCell, for: indexPath) as! StoreConfirmTableViewPriceDetailCell
            
            cell.indexPath = indexPath
            cell.postcardModel = postcardModel
            return cell
            
        case 2://邮寄方式
            let cell: StoreConfirmTableViewFreightCell = tableView.dequeueReusableCell(withIdentifier: freightCell, for: indexPath) as! StoreConfirmTableViewFreightCell
            
            cell.postcardModel = postcardModel
            return cell
            
        default://预览
            let cell: StoreConfirmTableViewPreviewCell = tableView.dequeueReusableCell(withIdentifier: previewCell, for: indexPath) as! StoreConfirmTableViewPreviewCell
            
            cell.postcardModel = postcardModel
            return cell
        }
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectCell?(indexPath)
    }
    //MARK: 设置Cell的高度
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 123/baseWidth
        case 1:
            return 50/baseWidth
        case 2:
            if postcardModel?.payModel.isMailAvailable == true {
                return 150/baseWidth
            }
            return 0
        default:
            return 135/baseWidth
        }
        
    }
    //MARK: 设置sectionHeader的高度
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 4
        }
        return 7
    }
}

//MARK: 展示地址的Cell
class StoreConfirmTableViewAddressCell: UITableViewCell{
    
    /// 地址数据
    var addressModel: AddressModel?{
        didSet{
            if addressModel != nil{
                noAddressLabel?.isHidden = true
                nameLabel?.text = addressModel?.mailName
                phoneNumberLabel?.text = addressModel?.phoneNumber
                detailAddressLabel?.setParagrapText(text: addressModel?.detailedAddress, andLineSpace: 7)
                isDefaultLabel?.isHidden = !addressModel!.isDefault
            }
        }
    }
    
    /// 没有选择地址时展示的label
    fileprivate var noAddressLabel: BaseLabel?
    /// 收件人label
    fileprivate var nameLabel: BaseLabel?
    /// 电话号码的label
    fileprivate var phoneNumberLabel: BaseLabel?
    /// 是否是默认的label
    fileprivate var isDefaultLabel: BaseLabel?
    /// 详细地址的label
    fileprivate var detailAddressLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let grayRightArrow: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "grayRightArrow")
        contentView.addSubview(grayRightArrow)
        grayRightArrow.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-21/baseWidth)
            make.size.equalTo(CGSize(width: 7/baseWidth, height: 11/baseWidth))
        }
        
        noAddressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "525252", andText: NSLocalizedString("请添加收货地址", comment: ""))
        contentView.addSubview(noAddressLabel!)
        noAddressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(24/baseWidth)
        })
        
        nameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 16), andTextColorHex: "474747")
        contentView.addSubview(nameLabel!)
        nameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(24/baseWidth)
            make.left.equalTo(contentView).offset(23/baseWidth)
        })
        
        phoneNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "363636", andTextAlignment: NSTextAlignment.left)
        contentView.addSubview(phoneNumberLabel!)
        phoneNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(nameLabel!)
            make.left.equalTo(nameLabel!.snp.right).offset(40/baseWidth)
        })
        
        isDefaultLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "FFFFFF", andTextAlignment: NSTextAlignment.center, andText: NSLocalizedString("默认", comment: ""))
        contentView.addSubview(isDefaultLabel!)
        isDefaultLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(nameLabel!.snp.bottom).offset(25/baseWidth)
            make.leftMargin.equalTo(nameLabel!)
            make.size.equalTo(CGSize(width: 42/baseWidth, height: 20/baseWidth))
        })
        //添加背景色和圆角, 并隐藏
        isDefaultLabel?.backgroundColor = UIColor(hexString: "6D6D6D")
        isDefaultLabel?.layer.cornerRadius = 2
        isDefaultLabel?.layer.masksToBounds = true
        isDefaultLabel?.isHidden = true
        
        detailAddressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "787878", andNumberOfLines: 2)
        contentView.addSubview(detailAddressLabel!)
        detailAddressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(phoneNumberLabel!.snp.bottom).offset(7.5/baseWidth)
            make.left.equalTo(isDefaultLabel!.snp.right).offset(36/baseWidth)
            make.right.equalTo(grayRightArrow.snp.left).offset(-10/baseWidth)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示价格详情的Cell
class StoreConfirmTableViewPriceDetailCell: UITableViewCell{
    
    var postcardModel: MUFinalPostcardModel?{
        didSet{
            if postcardModel != nil {
                switch indexPath!.row {
                case 0://图片价格
                    priceLabel?.text = "¥ \(postcardModel!.payModel.imagePrice)"
                case 1://实物价格
                    switch postcardModel!.goods!.goodsType {
                    case CustomizationType.pillow:
                        titleLabel?.text = NSLocalizedString("AR抱枕价格", comment: "")
                    case CustomizationType.postcard:
                        titleLabel?.text = NSLocalizedString("AR明信片价格", comment: "")
                    case CustomizationType.tShirt:
                        titleLabel?.text = NSLocalizedString("AR T恤价格", comment: "")
                    default:
                        titleLabel?.text = NSLocalizedString("AR马克杯价格", comment: "")
                    }
                    priceLabel?.text = "¥ \(postcardModel!.payModel.goodsPrice * Double(postcardModel!.payModel.orderNumber))"
                case 3://商品总价
                    priceLabel?.text = "¥ \(postcardModel!.payModel.orderTotalAmount)"
                default:
                    break
                }
            }
        }
    }
    
    var indexPath: IndexPath?{
        didSet{
            if indexPath == nil || titleLabel != nil{return}
            setUI()
        }
    }
    
    /// 商品价格
    fileprivate var titleLabel: BaseLabel?
    /// 价格label
    fileprivate var priceLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "414141")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(24/baseWidth)
        })
        
        switch indexPath!.row {
        case 2://展示代金券的Cell
            // 代金券Cell可以点击
            selectionStyle = UITableViewCellSelectionStyle.default
            
            let grayRightArrow: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "grayRightArrow")
            contentView.addSubview(grayRightArrow)
            grayRightArrow.snp.makeConstraints { (make: ConstraintMaker) in
                make.centerY.equalTo(contentView)
                make.right.equalTo(contentView).offset(-21/baseWidth)
                make.size.equalTo(CGSize(width: 7/baseWidth, height: 11/baseWidth))
            }
            
            priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "FF7337", andTextAlignment: NSTextAlignment.right)
            contentView.addSubview(priceLabel!)
            priceLabel?.snp.makeConstraints({ (make) in
                make.centerY.equalTo(contentView)
                make.right.equalTo(grayRightArrow.snp.left).offset(-22/baseWidth)
            })
        default://其他Cell
            priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "E0B347", andTextAlignment: NSTextAlignment.right)
            contentView.addSubview(priceLabel!)
            priceLabel?.snp.makeConstraints({ (make) in
                make.centerY.equalTo(contentView)
                make.right.equalTo(contentView).offset(-21/baseWidth)
            })
        }
        
        //设置标题文字
        
        var titleText: String
        
        switch indexPath!.row {
        case 0:
            titleText = NSLocalizedString("买图", comment: "")
        case 1:
            titleText = NSLocalizedString("", comment: "")
        case 2:
            titleText = NSLocalizedString("代金券", comment: "")
        default:
            titleText = NSLocalizedString("合计", comment: "")
        }
        titleLabel?.text = titleText
    }
    
    //MARK: 设置有几张代金券可用
    /// 设置有几张代金券可用
    ///
    /// - Parameter couponNumber: 代金券数量
    func setCouponNumber(couponCount: Int){
        priceLabel?.text = "\(couponCount) " + NSLocalizedString("张优惠券可用", comment: "")
    }
    
    //MARK: 设置代金券优惠的金额
    /// 设置代金券优惠的金额
    ///
    /// - Parameter couponModel: 代金券模型
    func setDiscount(couponModel: CouponModel){
        priceLabel?.text = "-¥ \(couponModel.couponPromotion)"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示运输方式的Cell
class StoreConfirmTableViewFreightCell: UITableViewCell{
    
    var postcardModel: MUFinalPostcardModel?{
        didSet{
            freightView?.postcardModel = postcardModel
        }
    }
    
    fileprivate var titleLabel: BaseLabel?
    fileprivate var freightView: StorePurchaseFreightView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.layer.masksToBounds = true
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "525252", andText: NSLocalizedString("邮寄方式", comment: ""))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(21.5/baseWidth)
            make.left.equalTo(contentView).offset(24/baseWidth)
        })
        
        freightView = StorePurchaseFreightView()
        contentView.addSubview(freightView!)
        freightView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom)
            make.left.right.equalTo(contentView)
            make.size.equalTo(CGSize(width: 333/baseWidth, height: 101))
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 预览商品图片的cell
class StoreConfirmTableViewPreviewCell: UITableViewCell{
    
    var postcardModel: MUFinalPostcardModel?{
        didSet{
            previewImageView?.image = postcardModel?.previewImage
            titleLabel?.text = postcardModel!.payModel.goodsName
            numberLabel01?.text = "x \(postcardModel!.payModel.orderNumber)"
            priceNeedToPayLabel01?.text = "¥\(postcardModel!.payModel.goodsTotalPrice)"
            if postcardModel?.goods?.goodsType == CustomizationType.postcard{//设置阴影
            }else{
                shadowView?.isHidden = true
            }
        }
    }
    /// 阴影视图
    fileprivate var shadowView: BaseView?
    /// 商品预览图片
    fileprivate var previewImageView: BaseImageView?
    /// 商品标题
    fileprivate var titleLabel: BaseLabel?
    /// 商品数量
    fileprivate var numberLabel: BaseLabel?
    /// 商品数量的值
    fileprivate var numberLabel01: BaseLabel?
    /// 价格
    fileprivate var priceNeedToPayLabel: BaseLabel?
    /// 价格的值
    fileprivate var priceNeedToPayLabel01: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = UITableViewCellSelectionStyle.none
        
        shadowView = BaseView.shadowView()
        contentView.addSubview(shadowView!)
        shadowView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(26.5/baseWidth)
            make.left.equalTo(contentView).offset(21/baseWidth)
            make.size.equalTo(CGSize(width: 110, height: 80))
        })
        
        previewImageView = BaseImageView.normalImageView()
        contentView.addSubview(previewImageView!)
        previewImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(26.5/baseWidth)
            make.left.equalTo(contentView).offset(21/baseWidth)
            make.size.equalTo(CGSize(width: 110, height: 80))
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "545454")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(previewImageView!)
            make.left.equalTo(previewImageView!.snp.right).offset(12/baseWidth)
        })
        
        numberLabel01 = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "707070", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(numberLabel01!)
        numberLabel01?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(9/baseWidth)
            make.right.equalTo(contentView).offset(-18/baseWidth)
        })
        numberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "707070", andText: NSLocalizedString("数量", comment: ""))
        contentView.addSubview(numberLabel!)
        numberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(numberLabel01!)
            make.leftMargin.equalTo(titleLabel!)
            make.right.equalTo(numberLabel01!.snp.left)
        })
        
        priceNeedToPayLabel01 = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "707070", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(priceNeedToPayLabel01!)
        priceNeedToPayLabel01?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(numberLabel!.snp.bottom).offset(2/baseWidth)
            make.rightMargin.equalTo(numberLabel01!)
        })
        priceNeedToPayLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "707070", andText: NSLocalizedString("合计", comment: ""))
        contentView.addSubview(priceNeedToPayLabel!)
        priceNeedToPayLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(priceNeedToPayLabel01!)
            make.leftMargin.equalTo(titleLabel!)
            make.right.equalTo(priceNeedToPayLabel01!.snp.left)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
