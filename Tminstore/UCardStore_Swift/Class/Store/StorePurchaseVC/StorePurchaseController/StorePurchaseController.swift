//
//  StorePurchaseController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/2.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 确认订单的界面
import UIKit
import SnapKit

class StorePurchaseController: BaseViewController {
    
    fileprivate let viewModel: StorePurchaseViewModel = StorePurchaseViewModel()
    fileprivate let couponViewModel: MUCouponViewModel = MUCouponViewModel()
    
    var postcardModel: MUFinalPostcardModel?
    
    fileprivate var tableView: StorePurchaseTableView?
    /// 确认支付的视图
    fileprivate var confirmView: StorePurchaseView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView?.reloadData()
        confirmView?.payModel = postcardModel!.payModel
        couponViewModel.getCouponList(index: 1)//获取优惠券数量
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        confirmView = StorePurchaseView()
        view.addSubview(confirmView!)
        confirmView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(49/baseWidth)
        })
        
        tableView = StorePurchaseTableView()
        view.insertSubview(tableView!, belowSubview: confirmView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(confirmView!.snp.top)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 点击去支付按钮的事件
        confirmView?.touchConfirmButtonClosure = {[weak self] _ in
            MobClick.event("makePayment")
            if self?.postcardModel?.addressModel == nil{
                self?.alert(alertTitle: NSLocalizedString("请先选择地址", comment: ""))
            }else if self?.postcardModel?.goods?.goodsType == CustomizationType.postcard && self?.postcardModel?.addressModel?.postcode == "" && self?.postcardModel?.payModel.freightCharge == 0{
                self?.alert(alertTitle: NSLocalizedString("请先填写邮编", comment: ""), leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("好的", comment: ""),  rightClosure: { 
                    self?.enterEditAddressVC()
                })
            }else{
                self?.viewModel.computePrice(postcardModel: (self?.postcardModel)!)
            }
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 支付成功
        viewModel.uploadOrderSuccess = {[weak self] _ in
            self?.dismissSVProgress()
            let vc: MUPaySuccessController = MUPaySuccessController()
            vc.payModel = self?.postcardModel?.payModel
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 获得优惠券数量
        couponViewModel.totalCoupon = {[weak self] (count: Int, _) in
            if let cell: StorePurchaseCouponCell = self?.tableView?.cellForRow(at: IndexPath(row: 2, section: 1)) as? StorePurchaseCouponCell{
                cell.couponCount = count
            }
        }
    }
    
    //MARK: 进入地址编辑界面
    fileprivate func enterEditAddressVC(){
        let eaVC: MUEditAddressController = MUEditAddressController()
        eaVC.umengViewName = "编辑地址"
        eaVC.addressModel = postcardModel?.addressModel?.mutableCopy() as? AddressModel
        eaVC.addOrEditAddressSuccess = {[weak self] _ in
            self?.postcardModel?.addressModel  = eaVC.addressModel
            self?.tableView?.reloadData()
        }
        navigationController?.pushViewController(eaVC, animated: true)
    }
}

extension StorePurchaseController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return 7
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell: StorePurchaseAddressCell = tableView.dequeueReusableCell(withIdentifier: "0", for: indexPath) as! StorePurchaseAddressCell
            cell.addressModel = postcardModel?.addressModel
            return cell
        }else{
            switch indexPath.row {
            case 0:
                let cell: StorePurchasePriceCell = tableView.dequeueReusableCell(withIdentifier: "1", for: indexPath) as! StorePurchasePriceCell
                cell.postcardModel = postcardModel
                cell.indexPath = indexPath
                return cell
            case 1:
                let cell: StorePurchasePriceCell = tableView.dequeueReusableCell(withIdentifier: "2", for: indexPath) as! StorePurchasePriceCell
                cell.postcardModel = postcardModel
                cell.indexPath = indexPath
                return cell
            case 2:
                let cell: StorePurchaseCouponCell = tableView.dequeueReusableCell(withIdentifier: "3", for: indexPath) as! StorePurchaseCouponCell
                cell.couponModel = postcardModel?.payModel.couponModel
                return cell
            case 3:
                let cell: StorePurchasePriceCell = tableView.dequeueReusableCell(withIdentifier: "4", for: indexPath) as! StorePurchasePriceCell
                cell.postcardModel = postcardModel
                cell.indexPath = indexPath
                return cell
            case 4:
                let cell: StorePurchaseFreightCell = tableView.dequeueReusableCell(withIdentifier: "5", for: indexPath) as! StorePurchaseFreightCell
                cell.postcardModel = postcardModel
                return cell
            case 5:
                let cell: StorePurchasePayChannelCell = tableView.dequeueReusableCell(withIdentifier: "6", for: indexPath) as! StorePurchasePayChannelCell
                cell.postcardModel = postcardModel
                return cell
            default:
                let cell: StorePurchasePreviewCell = tableView.dequeueReusableCell(withIdentifier: "7", for: indexPath) as! StorePurchasePreviewCell
                cell.postcardModel = postcardModel
                return cell
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 80
        }else{
            switch indexPath.row {
            case 0:
                if postcardModel?.payModel.imagePrice == 0.0{
                    return 0
                }else{
                    return 46
                }
            case 1, 2, 3:
                return 46
            case 4:
                if postcardModel?.payModel.isMailAvailable == true{
                    return 138.5
                }else{
                    return 0
                }
            case 5:
                return 183.5
            default:
                return 134
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0{
            let vc: MUAddressListController = MUAddressListController()
            vc.postcard = postcardModel
            _ = navigationController?.pushViewController(vc, animated: true)
        }else if indexPath.row == 2{
            let couponVC: MUCouponVC = MUCouponVC()
            _ = navigationController?.pushViewController(couponVC, animated: true)
            //MARK: 使用优惠券成功，返回刷新数据
            couponVC.useCouponSuccess = {[weak self] (coupon: CouponModel) in
                self?.postcardModel?.payModel.couponModel = coupon
                self?.tableView?.reloadData()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view: UIView = UIView()
        view.backgroundColor = UIColor(hexString: "#F7F7F7")
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 4
    }
}
