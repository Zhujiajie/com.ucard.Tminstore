//
//  StorePaySuccessViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StorePaySuccessViewController: BaseViewController {

    var postcardModel: MUFinalPostcardModel?
    
    fileprivate var grayLine01: BaseView?
    fileprivate var grayLine02: BaseView?
    fileprivate var grayLine03: BaseView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //禁止滑动返回
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        view.backgroundColor = UIColor.white
        setNavigationBar()
        setUpperUI()
        setDownUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        navigationItem.hidesBackButton = true//隐藏返回按钮
        setNavigationTitle(NSLocalizedString("支付成功", comment: ""))
    }
    
    //MARK: 设置上部分UI
    fileprivate func setUpperUI(){
        
        grayLine01 = BaseView.colorLine(colorString: "#F3F6F8")
        view.addSubview(grayLine01!)
        grayLine01?.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(view)
            make.height.equalTo(9)
        }
        
        let successImageView: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "storePaySuccess")
        view.addSubview(successImageView)
        successImageView.snp.makeConstraints { (make) in
            make.top.equalTo(grayLine01!.snp.bottom).offset(27/baseWidth)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 58/baseWidth, height: 58/baseWidth))
        }
        
        let titleLabel: BaseLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 24), andTextColorHex: "565656", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("支付成功", comment: ""))
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.top.equalTo(successImageView.snp.bottom).offset(35/baseWidth)
        }
        
        //返回按钮
        let backButton: BaseButton = BaseButton.normalTitleButton(title: NSLocalizedString("回到店铺", comment: ""), andTextFont: UIFont.systemFont(ofSize: 18), andTextColorHexString: "FFFFFF", andBackgroundColorHexString: "545454", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: 4)
        view.addSubview(backButton)
        backButton.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(22/baseWidth)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 142/baseWidth, height: 47/baseWidth))
        }
        
        //MARK: 点击返回按钮的事件
        backButton.touchButtonClosure = {[weak self] _ in
            self?.navigationController?.popToRootViewController(animated: true)
        }
        
        grayLine02 = BaseView.colorLine(colorString: "#F3F6F8")
        view.addSubview(grayLine02!)
        grayLine02?.snp.makeConstraints({ (make) in
            make.top.equalTo(backButton.snp.bottom).offset(28/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(3)
        })
    }
    
    //MARK: 设置下方的UI
    fileprivate func setDownUI(){
        
        var titleFont: UIFont
        if #available(iOS 8.2, *) {
            titleFont = UIFont.systemFont(ofSize: 15, weight: UIFontWeightMedium)
        } else {
            titleFont = UIFont.systemFont(ofSize: 15)
        }
        
        //订单编号
        let titleLabel01: BaseLabel = BaseLabel.normalLabel(font: titleFont, andTextColorHex: "696969", andTextAlignment: NSTextAlignment.right, andNumberOfLines: 1, andText: NSLocalizedString("订单编号", comment: ""))
        view.addSubview(titleLabel01)
        titleLabel01.snp.makeConstraints { (make) in
            make.top.equalTo(grayLine02!.snp.bottom).offset(22/baseWidth)
            make.right.equalTo(view).offset(-230/baseWidth)
        }
        
        let subTitleLabel01: BaseLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 13), andTextColorHex: "BEBEBE", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: nil)
        view.addSubview(subTitleLabel01)
        subTitleLabel01.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleLabel01)
            make.left.equalTo(titleLabel01.snp.right).offset(17/baseWidth)
        }
        subTitleLabel01.text = postcardModel!.payModel.orderNo
        
        //交易方式
        let titleLabel02: BaseLabel = BaseLabel.normalLabel(font: titleFont, andTextColorHex: "696969", andTextAlignment: NSTextAlignment.right, andNumberOfLines: 1, andText: NSLocalizedString("交易方式", comment: ""))
        view.addSubview(titleLabel02)
        titleLabel02.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel01.snp.bottom).offset(22/baseWidth)
            make.rightMargin.equalTo(titleLabel01)
        }
        
        let subTitleLabel02: BaseLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 13), andTextColorHex: "BEBEBE", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: nil)
        view.addSubview(subTitleLabel02)
        subTitleLabel02.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleLabel02)
            make.leftMargin.equalTo(subTitleLabel01)
        }
        
        var payChannelText: String
        switch postcardModel!.payModel.payStyle {
        case PayStyle.weChat:
            payChannelText = NSLocalizedString("微信支付", comment: "")
        case PayStyle.aliPay:
            payChannelText = NSLocalizedString("支付宝支付", comment: "")
        default:
            payChannelText = NSLocalizedString("余额支付", comment: "")
        }
        subTitleLabel02.text = payChannelText
        
        //下单时间
        let titleLabel03: BaseLabel = BaseLabel.normalLabel(font: titleFont, andTextColorHex: "696969", andTextAlignment: NSTextAlignment.right, andNumberOfLines: 1, andText: NSLocalizedString("下单时间", comment: ""))
        view.addSubview(titleLabel03)
        titleLabel03.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel02.snp.bottom).offset(22/baseWidth)
            make.rightMargin.equalTo(titleLabel01)
        }
        
        let subTitleLabel03: BaseLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 13), andTextColorHex: "BEBEBE", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: nil)
        view.addSubview(subTitleLabel03)
        subTitleLabel03.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleLabel03)
            make.leftMargin.equalTo(subTitleLabel01)
        }
        
        let timeFomatter: DateFormatter = DateFormatter()
        timeFomatter.timeStyle = DateFormatter.Style.short
        timeFomatter.dateStyle = DateFormatter.Style.short
        
        subTitleLabel03.text = timeFomatter.string(from: Date())
        
        grayLine03 = BaseView.colorLine(colorString: "#F3F6F8")
        view.addSubview(grayLine03!)
        grayLine03?.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel03.snp.bottom).offset(21/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(3)
        })
    }
}
