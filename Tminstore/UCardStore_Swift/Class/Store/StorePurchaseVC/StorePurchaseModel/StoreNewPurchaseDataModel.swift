//
//  StoreNewPurchaseDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreNewPurchaseDataModel: BaseDataModel {
    
    /// 上传订单成功，去支付
    var uploadOrderSuccess: ((_ result: [String: Any])->())?
    
    fileprivate var postcardModel: MUFinalPostcardModel?
    
    /// 正面图片在又拍云的存放key
    var frontImageKey: String?
    /// 预览图片在又拍云的存放key
    var previewImageKey: String?
    /// 视频在又拍云的存放key
    var videoKey: String?
    
    //MARK: 开始上传订单数据
    func uploadOrderData(postcardModel: MUFinalPostcardModel){
        if self.postcardModel == nil{
            self.postcardModel = postcardModel
            self.postcardModel?.frontImageUUID = UUID().uuidString
            self.postcardModel?.videoID = UUID().uuidString
            self.postcardModel?.previewImageUUID = UUID().uuidString
            
            frontImageKey = frontImageUpun + self.postcardModel!.frontImageUUID + ".png"
            previewImageKey = frontImageUpun + self.postcardModel!.previewImageUUID + ".png"
            videoKey = videoUpun + self.postcardModel!.videoID + ".mp4"
        }
        uploadFrontImage()
    }
    
    //MARK: 上传正面图片
    fileprivate func uploadFrontImage(){
        
        //压缩一下图片
        let newImage: UIImage = UIImage(image: postcardModel!.frontImage!, scaledToWidth: 1000)
        
        guard let imageData: Data = UIImagePNGRepresentation(newImage) else {
            errorClosure?(nil)
            return }
        
        uploadToUpyun(data: imageData, andSaveKey: frontImageKey!, andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.uploadPreviewImage()
            }, failClosure: { [weak self] (error: Error?, _, _) in
                self?.errorClosure?(error)
            }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                self?.uploadImageProgress(percent: completeCount/totalCount/2)
        })
    }
    
    //MARK: 上传预览图片
    fileprivate func uploadPreviewImage(){
        
        //压缩一下图片
        let newImage: UIImage = UIImage(image: postcardModel!.previewImage!, scaledToWidth: 1000)
        
        guard let imageData: Data = UIImagePNGRepresentation(newImage) else {
            errorClosure?(nil)
            return }
        
        uploadToUpyun(data: imageData, andSaveKey: previewImageKey!, andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.uploadVideo()
            }, failClosure: { [weak self] (error: Error?, _, _) in
                self?.errorClosure?(error)
            }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                self?.uploadImageProgress(percent: 0.5 + completeCount/totalCount/2)
        })
    }
    
    
    //MARK: 上传视频到又拍云
    func uploadVideo(){
        
        do{
            let videoData: Data = try Data(contentsOf: postcardModel!.videoPath!, options: [])
            
            let parameters: [String: Any] = [
                "name": "naga",
                "type": "video",
                "avopts": "/wmImg/L2lPU0ltYWdlcy93YXRlcm1hcmswMS5wbmc=/wmGravity/northwest/wmDx/20/wmDy/15",
                "save_as": postcardModel!.videoURLPath
            ]
            
            uploadToUpyun(data: videoData, andSaveKey: videoKey!, andParameters: ["apps": [parameters]], successClosure: { [weak self] (_, _) in
                self?.uploadOrderInfo()
                }, failClosure: { [weak self] (error: Error?, _, _) in
                    self?.errorClosure?(error)
                }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                    self?.uploadVideoProgress(percent: completeCount/totalCount)
            })
            
        }catch{errorClosure?(error)}
    }
    
    //MARK: 上传订单信息
    fileprivate func uploadOrderInfo(){
        
        var couponId: String
        if postcardModel?.payModel.couponModel?.couponCode != nil{
            couponId = postcardModel!.payModel.couponModel!.couponCode
        }else{
            couponId = ""
        }
        var isMail: String
        if postcardModel?.goods?.goodsType == CustomizationType.postcard{
            if postcardModel?.payModel.freightCharge != 0.0{
                isMail = "00"
            }else{
                isMail = "01"
            }
        }else{
            isMail = ""
        }
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "originalId": postcardModel!.originalId,
            "goodsId": postcardModel!.goods!.goodsId,
            "couponId": couponId,
            "buyTotal": postcardModel!.payModel.orderNumber,
            "addressId": postcardModel!.addressModel!.addressId,
            "orderAmount": postcardModel!.payModel.priceNeedToPay,
            "imgUrl": postcardModel!.frontImageURLPath,
            "videoUrl": postcardModel!.videoURLPath,
            "storePictures": postcardModel!.previewImageURLPath,
            "isMail": isMail,
            "backContent": postcardModel!.message
        ]
        
        print(parameters)
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.uploadOrderInfoAPI, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String: Any]) in
            self?.uploadOrderSuccess?(result)
            }, failureClourse: {[weak self] (error: Error?) in
                self?.errorClosure?(error)
        })
    }
    
    //MARK: 请求支付信息的接口
    /// 请求支付信息的接口
    ///
    /// - Parameters:
    ///   - payModel: 支付模型
    ///   - success: 成功的闭包
    func requestChargeInfo(payModel: PayModel, andSuccessClosure success: ((_ result: [String: Any])->())?){
        
        let para: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "channel": payModel.payStyle.rawValue,
            "amount": payModel.pingxxAmount,
            "originalId": payModel.originalId,
            "mailOrderNo": payModel.mailOrderNo,
            "orderNO": payModel.orderNo,
            "currency": "cny"
        ]
        
        print(para)
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.requestChargeInfoAPI, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 支付完成之后，通知后台支付成功
    /// 支付完成之后，通知后台支付成功
    func notifyPaymentResult(orderNo: String, andChargeId chargeId: String){
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "orderNo": orderNo,
            "chargeId": chargeId
        ]
        print(parameters)
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.notifyPaymentResultAPI, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            print(result)
        }, failureClourse: nil, completionClosure: nil)
    }
    
    //MARK: 验证支付密码
    /// 验证支付密码
    ///
    /// - Parameters:
    ///   - password: 支付密码
    ///   - success: 成功的闭包
    func verifyPaymentPassword(password: String, andOrderNo orderNo: String, andSuccessClosure success: ((_ result: [String: Any])->())?){
        
        let encrypted: String = RSA.encryptString(password, publicKey: RSAPublicKey)
        
        let para: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "payPwd": encrypted,
            "orderNo": orderNo
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.verifyPaymentPasswordAPI, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
