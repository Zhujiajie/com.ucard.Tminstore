//
//  StorePurchaseViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StorePurchaseViewModel: BaseViewModel {
    
    fileprivate let dataModel: StorePurchaseDataModel = StorePurchaseDataModel()
    fileprivate var postcardModel: MUFinalPostcardModel?
    /// 上传成功
    var uploadOrderSuccess: (()->())?
    /// ping++订单编号
    fileprivate var chargeId: String = ""
    /// 订单编号
    fileprivate var orderNo: String = ""
    
    override init() {
        super.init()
        
        //MARK: 出错
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.dismissSVProgress()
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 上传订单成功的闭包，开始去支付
        dataModel.uploadOrderSuccess = {[weak self] (result: [String: Any]) in
            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let orderNo: String = data["orderNo"] as? String{
                        self?.postcardModel?.payModel.orderNo = orderNo
                        self?.orderNo = orderNo
                        if let charge: String = data["charge"] as? String{
                            if let id: String = data["id"] as? String{
                                self?.chargeId = id
                                self?.pingppPay(charge: charge)
                            }
                            return
                        }
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 检查价格的接口
    func computePrice(postcardModel: MUFinalPostcardModel){
        self.postcardModel = postcardModel
        showSVProgress(title: nil)
        dataModel.countTotalAmount(postcardModel: postcardModel, successClosure: {[weak self] (result: [String: Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any] {
                    if let orderAmt: Double = data["orderAmt"] as? Double{
                        //不含运费的价格对比
                        if orderAmt == postcardModel.payModel.orderTotalAmount{
                            self?.dataModel.uploadOrderData(postcardModel: postcardModel)
                        }else{
                            let message: [String: Any] = [
                                "message": "金额计算出错，请重新下单"
                            ]
                            self?.dismissSVProgress()
                            self?.errorClosure?(nil, message)

                        }
                        return
                    }
                }
            }
            self?.dismissSVProgress()
            self?.errorClosure?(nil, result)
        })
    }
    
    //MARK: 上传订单成功，去支付
    fileprivate func pingppPay(charge: String){
        let chargeModel: NSObject = charge as NSObject
        
        print(charge)
        
        Pingpp.createPayment(chargeModel, appURLScheme: appAPIHelp.kAppURLScheme, withCompletion: { [weak self](result: String?, pingError: PingppError?) in
            if result != nil{
//                print(result!)
//                print(pingError!.code)
                switch result!{
                case "success"://成功
                    self?.uploadOrderSuccess?()
                    self?.dataModel.notifyPaymentResult(orderNo: (self?.orderNo)!, andChargeId: (self?.chargeId)!)
                case "cancel":// 取消
                    let message: [String: Any] = ["message": NSLocalizedString("支付被取消", comment: "")]
                    self?.errorClosure?(nil, message)
                default:// 失败
                    let message: [String: Any] = ["message": NSLocalizedString("支付失败", comment: "")]
                    self?.errorClosure?(nil, message)
                }
            }else{
                var string: String
                if pingError != nil{
                    string = pingError!.getMsg()
                }else{
                    string = NSLocalizedString("支付失败", comment: "")
                }
                let message: [String: Any] = ["message": string]
                self?.errorClosure?(nil, message)
            }
        })
    }
    
    //MARK: 二次支付
    /// 二次支付
    ///
    /// - Parameter payModel: PayModel
    func payAgain(payStyle: PayStyle, andOrderNO orderNo: String, andMailOrderNo mailOrderNo: String){
        self.orderNo = orderNo
        dataModel.payAgain(payStyle: payStyle, andOrderNO: orderNo, andMailOrderNo: mailOrderNo, successClosure: {[weak self] (result: [String: Any]) in
            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let charge: String = data["charge"] as? String{
                        // 获取ping++订单号
                        if let chargeId: String = data["id"] as? String{
                            self?.chargeId = chargeId
                        }
                        self?.pingppPay(charge: charge)
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        })
    }
}
