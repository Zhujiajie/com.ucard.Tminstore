//
//  StoreNewPurchaseViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreNewPurchaseViewModel: BaseViewModel {

    fileprivate let dataModel: StoreNewPurchaseDataModel = StoreNewPurchaseDataModel()
    fileprivate var postcardModel: MUFinalPostcardModel?
    /// 上传成功
    var uploadOrderSuccess: (()->())?
    
    override init() {
        super.init()
        
        //MARK: 出错
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.dismissSVProgress()
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 上传订单成功的闭包，开始去支付
        dataModel.uploadOrderSuccess = {[weak self] (result: [String: Any]) in
            self?.dismissSVProgress()
            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let paymentBean: [String: Any] = data["paymentBean"] as? [String: Any]{
                        if let amount: String = paymentBean["amount"] as? String{
                            self?.postcardModel?.payModel.pingxxAmount = Double(amount)!
                            if let originalId: String = paymentBean["originalId"] as? String{
                                self?.postcardModel?.payModel.originalId = originalId
                                if let mailOrderNo: String = paymentBean["mailOrderNo"] as? String{
                                    self?.postcardModel?.payModel.mailOrderNo = mailOrderNo
                                    if let orderNo: String = paymentBean["orderNO"] as? String{
                                        self?.postcardModel?.payModel.orderNo = orderNo
                                        self?.uploadOrderSuccess?()//上传订单成功
                                        return
                                    }
                                }
                            }
                        }
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
        
        //MARK: 请求完成
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 上传订单信息的接口
    func uploadOrderInfo(postcardModel: MUFinalPostcardModel){
        self.postcardModel = postcardModel
        showSVProgress(title: nil)
        dataModel.uploadOrderData(postcardModel: postcardModel)
    }
    
    //MARK: 请求支付信息的接口
    /// 请求支付信息的接口
    ///
    /// - Parameter payModel: 支付模型
    func requestPaymentInfo(payModel: PayModel, andPaymentPassword password: String? = nil){
        showSVProgress(title: nil)
        dataModel.requestChargeInfo(payModel: payModel) { [weak self] (result: [String : Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if payModel.payStyle == PayStyle.balance {
                    self?.verifyPassword(password: password!, andPayModel: payModel)
                    return
                }else{
                    if let data: [String: Any] = result["data"] as? [String: Any]{
                        if let id: String = data["id"] as? String{
                            payModel.chargeId = id
                            if let charge: String = data["charge"] as? String{
                                self?.pingppPay(charge: charge, andPayModel: payModel)
                                return
                            }
                        }
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 上传订单成功，去支付
    fileprivate func pingppPay(charge: String, andPayModel payModel: PayModel){
        let chargeModel: NSObject = charge as NSObject
        
//        Pingpp.setDebugMode(true)
        
        Pingpp.createPayment(chargeModel, appURLScheme: appAPIHelp.kAppURLScheme, withCompletion: { [weak self](result: String?, pingError: PingppError?) in
            if result != nil{
                //                print(result!)
                //                print(pingError!.code)
                switch result!{
                case "success"://成功
                    self?.uploadOrderSuccess?()
                    self?.dataModel.notifyPaymentResult(orderNo: payModel.orderNo, andChargeId: payModel.chargeId)
                case "cancel":// 取消
                    let message: [String: Any] = ["message": NSLocalizedString("支付被取消", comment: "")]
                    self?.errorClosure?(nil, message)
                default:// 失败
                    let message: [String: Any] = ["message": NSLocalizedString("支付失败", comment: "")]
                    self?.errorClosure?(nil, message)
                }
            }else{
                var string: String
                if pingError != nil{
                    string = pingError!.getMsg()
                }else{
                    string = NSLocalizedString("支付失败", comment: "")
                }
                let message: [String: Any] = ["message": string]
                self?.errorClosure?(nil, message)
            }
        })
    }
    
    //MARK: 验证支付密码
    /// 验证支付密码
    ///
    /// - Parameter password: 支付密码
    func verifyPassword(password: String, andPayModel payModel: PayModel){
        showSVProgress(title: nil)
        
        dataModel.verifyPaymentPassword(password: password, andOrderNo: payModel.orderNo) { [weak self] (result: [String : Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.uploadOrderSuccess?()
                return
            }
            self?.errorClosure?(nil, result)
        }
    }
}
