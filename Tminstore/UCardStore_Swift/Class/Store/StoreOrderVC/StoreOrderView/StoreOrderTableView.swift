//
//  StoreOrderTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/1.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreOrderTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        backgroundColor = UIColor.white
        
        register(StoreOrderPreviewTableViewCell.self, forCellReuseIdentifier: "0")
        register(StoreOrderPreviewTableViewCell.self, forCellReuseIdentifier: "1")
        
        estimatedRowHeight = 463/baseWidth
        
        tableFooterView = UIView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
