//
//  StoreOrderPreviewTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/1.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class StoreOrderPreviewTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    var postcardModel: MUFinalPostcardModel?
    
    var indexPath: IndexPath?{
        didSet{
            if indexPath?.item == 0{
                setContentUI()
            }else{
                setIntroUI()
            }
        }
    }
    
    /// 承载图片和视频的collectionView
    fileprivate var collectionView: PDraftCollectionView?
    /// 展示页数的指示器
    fileprivate var pageControl: UIPageControl?
    
    fileprivate var titleLabel: BaseLabel?
    fileprivate var priceLabel: BaseLabel?
    fileprivate var numberLabel: BaseLabel?
    fileprivate var orderNumberChangeView: StoreOrderNumberView?
    fileprivate var introLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    //MARK: 设置内容UI
    fileprivate func setContentUI(){
        collectionView = PDraftCollectionView()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(23/baseWidth)
            make.left.right.equalTo(contentView)
            make.height.equalTo(275/baseWidth)
        })
        
        // 页数指示器
        pageControl = UIPageControl()
        pageControl?.numberOfPages = 2
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "60DBE8")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D6D6D6")
        contentView.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom)
            make.centerX.equalTo(contentView)
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "363636")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(pageControl!.snp.bottom)
            make.left.equalTo(contentView).offset(38/baseWidth)
        })
        switch postcardModel!.goods!.goodsType {
        case CustomizationType.pillow:
            titleLabel?.text = NSLocalizedString("AR抱枕定制", comment: "")
        case CustomizationType.postcard:
            titleLabel?.text = NSLocalizedString("AR明信片定制" , comment: "")
        case CustomizationType.tShirt:
            titleLabel?.text = NSLocalizedString("AR T恤定制", comment: "")
        default:
            titleLabel?.text = NSLocalizedString("AR马克杯定制", comment: "")
        }
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "E0B347")
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(12/baseWidth)
            make.leftMargin.equalTo(titleLabel!)
        })
        priceLabel?.text = "¥ \(postcardModel!.payModel.goodsPrice)"
        
        numberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "363636", andText: NSLocalizedString("数量", comment: ""))
        contentView.addSubview(numberLabel!)
        numberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(priceLabel!.snp.bottom).offset(31/baseWidth)
            make.leftMargin.equalTo(titleLabel!)
        })
        
        orderNumberChangeView = StoreOrderNumberView()
        contentView.addSubview(orderNumberChangeView!)
        orderNumberChangeView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(numberLabel!.snp.bottom).offset(9/baseWidth)
            make.leftMargin.equalTo(titleLabel!)
            make.size.equalTo(CGSize(width: 183/baseWidth, height: 38/baseWidth))
            make.bottom.equalTo(contentView).offset(-21/baseWidth)
        })
        //购买数量发生变化
        orderNumberChangeView?.orderNumberChanged = {[weak self] (num: Int) in
            self?.postcardModel?.payModel.orderNumber = num
            self?.priceLabel?.text = "¥ \((self?.postcardModel?.payModel.goodsTotalPrice)!)"
        }
        orderNumberChangeView?.orderNumber = 1//默认一个
    }
    
    //MARK: 设置介绍UI
    fileprivate func setIntroUI(){
        if titleLabel != nil {return}
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "363636", andText: NSLocalizedString("操作说明", comment: ""))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(16/baseWidth)
            make.left.equalTo(contentView).offset(38/baseWidth)
        })
        
        introLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "8A8A8A", andNumberOfLines: 2, andText: NSLocalizedString("打开APP点击右上角按钮对准明信片扫描", comment: ""))
        contentView.addSubview(introLabel!)
        introLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(8.5/baseWidth)
            make.leftMargin.equalTo(titleLabel!)
            make.right.equalTo(contentView).offset(-38/baseWidth)
            make.bottom.equalTo(contentView).offset(-15/baseWidth)
        })
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    //MARK: 设置cell的内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0{
            let cell: PDraftImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftImageCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftImageCollectionViewCell
            cell.image = postcardModel?.previewImage
            cell.imageView?.contentMode = UIViewContentMode.scaleAspectFit
            return cell
        }else{
            let cell: PDraftVideoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftVideoCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftVideoCollectionViewCell
            cell.videoPath = postcardModel?.videoPath
            return cell
        }
    }
    
    //MARK: 不显示Cell时。需要暂停播放视频
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell: PDraftVideoCollectionViewCell = cell as? PDraftVideoCollectionViewCell{
            cell.pausePlayer(hidePlayButton: true)
        }
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == screenWidth{
            if let cell: PDraftVideoCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? PDraftVideoCollectionViewCell{
                cell.playVideo()
            }
        }
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
