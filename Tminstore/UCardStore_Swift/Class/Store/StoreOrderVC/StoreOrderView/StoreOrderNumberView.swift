//
//  StoreOrderNumberView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/1.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class StoreOrderNumberView: UIView {

    /// 购买的数量
    var orderNumber: Int = 1{
        didSet{
            if orderNumber == 1{
                reduceButton?.isEnabled = false
                reduceButton?.alpha = 0.8
            }else if orderNumber == 99{
                plusButton?.alpha = 0.8
                plusButton?.isEnabled = false
            }else {
                reduceButton?.isEnabled = true
                reduceButton?.alpha = 1.0
                plusButton?.alpha = 1.0
                plusButton?.isEnabled = true
            }
            numberLabel?.text = "\(orderNumber)"
            orderNumberChanged?(orderNumber)//购买数量发生变化
        }
    }
    
    /// 购买数量发生变化
    var orderNumberChanged: ((_ orderNumber: Int)->())?
    
    /// 减号按钮
    fileprivate var reduceButton: BaseButton?
    /// 加号按钮
    fileprivate var plusButton: BaseButton?
    /// 购买数量的label
    fileprivate var numberLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.borderColor = UIColor(hexString: "A9A9A9").cgColor
        layer.borderWidth = 0.5
        
        reduceButton = BaseButton.normalIconButton(iconName: "reduceButton")
        addSubview(reduceButton!)
        reduceButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.bottom.equalTo(self)
            make.width.equalTo(50/baseWidth)
        })
        reduceButton?.layer.borderWidth = 0.5
        reduceButton?.layer.borderColor = UIColor(hexString: "A9A9A9").cgColor
        //MARK: 点击减号的事件
        reduceButton?.touchButtonClosure = {[weak self] _ in
            self?.orderNumber -= 1
        }
        
        plusButton = BaseButton.normalIconButton(iconName: "plusButton")
        addSubview(plusButton!)
        plusButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.bottom.equalTo(self)
            make.width.equalTo(reduceButton!)
        })
        plusButton?.layer.borderWidth = 0.5
        plusButton?.layer.borderColor = UIColor(hexString: "A9A9A9").cgColor
        //MARK: 点击加号的事件
        plusButton?.touchButtonClosure = {[weak self] _ in
            self?.orderNumber += 1
        }
        
        numberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "363636", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: "1")
        addSubview(numberLabel!)
        numberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.bottom.equalTo(self)
            make.left.equalTo(reduceButton!.snp.right)
            make.right.equalTo(plusButton!.snp.left)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
