//
//  StoreOrderPreviewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/1.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import ChameleonFramework

class StoreOrderPreviewController: BaseViewController {

    fileprivate let paymentPasswordViewModel: MUPaymentManagementViewModel = MUPaymentManagementViewModel()
    
    var postcardModel: MUFinalPostcardModel?
    fileprivate var tableView: StoreOrderTableView?
    fileprivate var purchaseButton: BaseButton?
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        
        purchaseButton = BaseButton.normalTitleButton(title: NSLocalizedString("立即购买", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "FFFFFF", andBackgroundColorHexString: "505050", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: nil)
        view.addSubview(purchaseButton!)
        purchaseButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(54/baseWidth)
        })
        
        
        tableView = StoreOrderTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(purchaseButton!.snp.top)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    //MARK: 闭包回调
    fileprivate func closure(){
        //MARK: 点击立即购买按钮，值支付界面
        purchaseButton?.touchButtonClosure = {[weak self] _ in
            if getUserLoginStatus() {
                MobClick.event("makeOrder")
                //            //判断是否设置过支付密码
                //            self?.paymentPasswordViewModel.checkPaymentPasswordStatus()
                self?.enterConfirmOrderVC()
            }else{
                self?.enterLoginView()
            }
        }
        
        //MARK: 判断支付密码状态成功
        paymentPasswordViewModel.checkPaymentPassword = {[weak self] (result: Bool) in
            if result {
                self?.enterConfirmOrderVC()
            }else{
                self?.beginSetPaymentPassword()
            }
        }
        
        //MARK: 出错的闭包
        paymentPasswordViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 进入确认订单界面
    fileprivate func enterConfirmOrderVC(){
//        let vc: StorePurchaseController = StorePurchaseController()
        let vc: StoreConfirmViewController = StoreConfirmViewController()
        vc.postcardModel = postcardModel
        vc.umengViewName = "确认订单"
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 进入设置支付密码的界面
    fileprivate func beginSetPaymentPassword(){
        alert(alertTitle: NSLocalizedString("请先设置支付密码", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("好的", comment: ""), rightButtonTitle: nil, leftClosure: { [weak self] _ in
            let vc: MUPaymentManagementViewController = MUPaymentManagementViewController()
            vc.umengViewName = "支付管理"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
            }, rightClosure: nil, presentComplition: nil)
    }
}

extension StoreOrderPreviewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: StoreOrderPreviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: "0", for: indexPath) as! StoreOrderPreviewTableViewCell
        cell.postcardModel = postcardModel
        cell.indexPath = indexPath
        return cell
    }
}
