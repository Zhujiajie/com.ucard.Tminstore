//
//  StoreIdealContainerCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

class StoreIdealContainerCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: screenWidth, height: screenHeight - nav_statusHeight - 48/baseWidth)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        
        super.init(frame: frame, collectionViewLayout: layout)
        isPagingEnabled = true
        backgroundColor = UIColor.white
        register(UICollectionViewCell.self, forCellWithReuseIdentifier: "1")
        register(UICollectionViewCell.self, forCellWithReuseIdentifier: "2")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
