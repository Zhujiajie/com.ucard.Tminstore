//
//  StoreIdealHeaderView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 用于在全部和个人之间切换的视图
import UIKit
import SnapKit

class StoreIdealHeaderView: UIView {

    /// 点击全部按钮的事件
    var touchAllButtonClosure: (()->())?
    /// 点击个人按钮的事件
    var touchPersonButtonClosure: (()->())?
    
    /// 切换到全部的按钮
    fileprivate var allButton: BaseButton?
    /// 切换到个人的按钮
    fileprivate var personButton: BaseButton?
    /// 指示用的绿色线条
    fileprivate var greenLine: UIView?
    /// 是否正在显示全部
    var isAll: Bool = true{
        didSet{
            if isAll == oldValue{return}
            switchBetweenAllAndPerson(isAll: isAll)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        allButton = BaseButton.normalTitleButton(title: NSLocalizedString("创意", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "454545")
        addSubview(allButton!)
        allButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.bottom.left.equalTo(self)
            make.width.equalTo(screenWidth/2)
        })
        allButton?.touchButtonClosure = {[weak self] _ in
            self?.isAll = true
        }
        
        personButton = BaseButton.normalTitleButton(title: NSLocalizedString("个人", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "454545")
        addSubview(personButton!)
        personButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.bottom.equalTo(self)
            make.left.equalTo(allButton!.snp.right)
        })
        personButton?.touchButtonClosure = {[weak self] _ in
            self?.isAll = false
        }
        
        greenLine = UIView()
        greenLine?.backgroundColor = UIColor(hexString: "59FFF4")
        addSubview(greenLine!)
        greenLine?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self)
            make.size.equalTo(CGSize(width: 129/baseWidth, height: 3))
            make.centerX.equalTo(allButton!)
        })
    }
    
    //MARK: 在全部和个人之间切换
    fileprivate func switchBetweenAllAndPerson(isAll: Bool){
        greenLine?.snp.remakeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self)
            make.size.equalTo(CGSize(width: 141/baseWidth, height: 2))
            if isAll{
                make.centerX.equalTo(allButton!)
                touchAllButtonClosure?()
            }else{
                make.centerX.equalTo(personButton!)
                touchPersonButtonClosure?()
            }
        })
        autoLayoutAnimation(view: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
