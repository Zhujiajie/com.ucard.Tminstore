//
//  StoreIdealCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage
import MJRefresh
import CHTCollectionViewWaterfallLayout

class StoreIdealCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let layout: CHTCollectionViewWaterfallLayout = CHTCollectionViewWaterfallLayout()
        layout.columnCount = 2
        layout.minimumColumnSpacing = 2
        layout.minimumInteritemSpacing = 2
        layout.itemRenderDirection = CHTCollectionViewWaterfallLayoutItemRenderDirection.leftToRight
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        register(StoreIdealCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.StoreIdealCollectionViewCellReuseIdentifier)
        backgroundColor = UIColor.white
        mj_header = BaseAnimationHeader()
        mj_footer = BaseRefreshFooter()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class StoreIdealCollectionViewCell: UICollectionViewCell {
    
//    var indexPath: IndexPath?
//    
    var postcardModel: PostcardModel?{
        didSet{
            SDWebImageManager.shared().cachedImageExists(for: postcardModel?.frontImageURL, completion: { [weak self] (result: Bool) in
                if result == true{
                    self?.imageView?.sd_setImage(with: self?.postcardModel?.frontImageURL)
                }else{
                    self?.imageView?.sd_setImage(with: self?.postcardModel?.frontPlaceImageURL)
                }
            })
            if postcardModel!.moneyPoint == 0.0{
                priceLabel?.text = NSLocalizedString("免费", comment: "")
            }else{
                priceLabel?.text = "¥ \(postcardModel!.moneyPoint)"
            }
        }
    }
    
    /// 数据源
    var idealModel: IdealPhotoModel?{
        didSet{
            SDWebImageManager.shared().cachedImageExists(for: idealModel?.imgUrl, completion: { [weak self] (result: Bool) in
                if result == true{
                    self?.imageView?.sd_setImage(with: self?.idealModel?.imgUrl)
                }else{
                    self?.imageView?.sd_setImage(with: self?.idealModel?.placeHolderURL)
                }
            })
            if idealModel?.moneyPrice == 0.0{
                priceLabel?.text = NSLocalizedString("免费", comment: "")
            }else{
                priceLabel?.text = "¥ \(idealModel!.moneyPrice)"
            }
        }
    }
    
//
//    /// 下载图片成功
//    var downLoadImageSuccess: ((_ indexPath: IndexPath)->())?
    
    /// 下载的图片
    var image: UIImage?{
        get{
            return imageView?.image
        }
    }
    
    /// imageView
    fileprivate var imageView: BaseImageView?
    /// 价格的Label
    fileprivate var priceLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "FFBC77")
        imageView?.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(imageView!).offset(-6.5/baseWidth)
            make.left.equalTo(imageView!).offset(10/baseWidth)
        })
    }
    
//    //MARK: 在子线程下载图片
//    func downLoadImageInBackGround(indexPath: IndexPath, andPostcardModel postcardModel: PostcardModel){
//        
//        //先判断是否已缓存图片
//        SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
//            
//            if result == true{
//                self?.downLoadImageInBackGround(indexPath: indexPath, andPostcardModel: postcardModel)
//            }else{
//                if let placeHolderURL: URL = postcardModel.frontPlaceImageURL{
//                    self?.imageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
//                        DispatchQueue.main.async {
//                            self?.downLoadImageInBackGround(indexPath: indexPath, andPostcardModel: postcardModel, andPlaceHolderImage: image)
//                        }
//                    })
//                }else{
//                    self?.downLoadImageInBackGround(indexPath: indexPath, andPostcardModel: postcardModel)
//                }
//            }
//        })
//    }
//    
//    //MARK: 直接下载原图
//    fileprivate func downLoadOrigianlImage(indexPath: IndexPath, andPostcardModel postcardModel: PostcardModel, andPlaceHolderImage placeHolderImage: UIImage? = nil){
//        
//        imageView?.sd_setImage(with: postcardModel.frontImageURL, placeholderImage: placeHolderImage, options: {[weak self] (image: UIImage?, _, _, _) in
//            if image != nil{
//                postcardModel.imageSize = image!.size
//                DispatchQueue.main.async {
//                    self?.downLoadImageSuccess?(indexPath)
//                }
//            }
//        })
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
