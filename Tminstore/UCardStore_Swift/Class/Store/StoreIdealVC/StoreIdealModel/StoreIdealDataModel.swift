//
//  StoreIdealDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreIdealDataModel: BaseDataModel {

    //MARK: 请求创意图片的接口
    /// 请求创意图片的接口
    ///
    /// - Parameters:
    ///   - pageNum: 分页数
    ///   - success: 成功的闭包
    func requestIdeal(pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlStr: String = appAPIHelp.requestIdealListAPI + "\(pageNum)"
        
        let parameter: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameter, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
}
