//
//  StoreIdealViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

class StoreIdealViewModel: BaseViewModel {

    fileprivate let dataModel: StoreIdealDataModel = StoreIdealDataModel()
    
    /// 下载图片的进度
    var imageDownLoadProgress: ((_ progress: Float)->())?
    
    /// 网络请求错误
    var netWorkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 向服务器端请求第几页数据
    var pageNum: Int = 1
    
    /// 数据源
    var postcardArray: [PostcardModel] = [PostcardModel]()
    /// 没有更多数据了
    var noMoreData: (()->())?
    /// 可以加载数据了
    var shouldReloadData: (()->())?
    /// 需要展示下载加载器
    var shouldShowIndicator: (()->())?
    
    fileprivate lazy var downLoadManager: SDWebImagePrefetcher = SDWebImagePrefetcher.shared()
    
    override init() {
        
        super.init()
        //MARK: 出错
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.netWorkError?(error, nil)
        }
        //MARK: 网络请求完成
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 显示创意图片的
    func requestIdeal(){
        
        dataModel.requestIdeal(pageNum: pageNum) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if list.isEmpty{
                            self?.noMoreData?()
                        }else{
                            if self?.pageNum == 1{
                                self?.postcardArray.removeAll()
                            }
                            for dic: [String: Any] in list{
                                let postcardModel: PostcardModel = PostcardModel.initFromMarket(dic: dic)
                                if postcardModel.frontImageURL != nil{
                                    self?.postcardArray.append(postcardModel)
                                }
                            }
                            self?.shouldReloadData?()
                            
                            //在后台下载图片
                            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                                self?.downLoadImage()
                            }
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.netWorkError?(nil, result)
        }
    }
    
    //MARK: 批量下载图片
    fileprivate func downLoadImage(){
        
        //展示加载提示器
        DispatchQueue.main.async {[weak self] _ in
            self?.shouldShowIndicator?()
        }
        
        let imageURLs: [URL] = postcardArray.map { (postcardModel) -> URL in
            return postcardModel.frontImageURL!
        }
        
        downLoadManager.prefetchURLs(imageURLs, progress: { [weak self] (count: UInt, totalCount: UInt) in
            DispatchQueue.main.async {
                self?.imageDownLoadProgress?(Float(count)/Float(totalCount))
            }
        }) { [weak self] (count: UInt, skipCount: UInt) in
            if skipCount != 0{
                DispatchQueue.main.async {
                    let message: [String: Any] = [
                        "message": "图片下载失败"
                    ]
                    self?.errorClosure?(nil, message)
                }
            }
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                self?.getImageSize()
            }
        }
    }
    
    //MARK: 获取图片的大小
    fileprivate func getImageSize(){
        let imageCache: SDImageCache = SDImageCache.shared()
        _ = postcardArray.map({ (postcard: PostcardModel) in
            if let image: UIImage = imageCache.imageFromCache(forKey: postcard.frontImageURL!.absoluteString){
                postcard.imageSize = image.size
                postcard.isOriginalImageDownloaded = true
            }
        })
        DispatchQueue.main.async {[weak self] _ in
            self?.shouldReloadData?()
        }
    }
}
