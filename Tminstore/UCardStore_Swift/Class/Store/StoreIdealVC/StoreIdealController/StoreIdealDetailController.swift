//
//  StoreIdealDetailController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import ChameleonFramework

class StoreIdealDetailController: BaseViewController {

    /// 数据源
    var idealModel: IdealPhotoModel?
    
    /// 获得了原图
    var getOriginalImage: ((_ idealModel: IdealPhotoModel?)->())?
    
    /// 图片
    fileprivate var imageView: BaseImageView?
    /// 返回按钮
    fileprivate var backButton: BaseButton?
    /// 价格label
    fileprivate var priceLabel: BaseLabel?
    /// 作者头像
    fileprivate var userPortraitView: BaseImageView?
    /// 昵称label
    fileprivate var nickNameLabel: BaseLabel?
    /// 作品数label
    fileprivate var orderTotalLabel: BaseLabel?
    /// 制作按钮
    fileprivate var makeButton: BaseButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUI()
        closures()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill, andImageName: nil)
        view.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make) in
            make.left.top.right.equalTo(view)
            make.height.equalTo(380/baseWidth)
        })
        imageView?.sd_setImage(with: idealModel?.imgUrl)
        
        backButton = BaseButton.normalIconButton(iconName: "idealBack")
        view.addSubview(backButton!)
        backButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(view).offset(15/baseWidth)
            make.left.equalTo(view).offset(11/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        })
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "FFCC4B", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: "¥  \(idealModel!.moneyPrice)")
        imageView!.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make) in
            make.left.equalTo(imageView!).offset(18/baseWidth)
            make.bottom.equalTo(imageView!).offset(-7/baseWidth)
        })
        
        userPortraitView = BaseImageView.userPortraitView(cornerRadius: 20/baseWidth)
        view.addSubview(userPortraitView!)
        userPortraitView?.snp.makeConstraints({ (make) in
            make.top.equalTo(imageView!.snp.bottom).offset(25/baseWidth)
            make.left.equalTo(view).offset(18/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        })
        userPortraitView?.sd_setImage(with: idealModel?.portraitURL)
        
        nickNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "666666", andText: idealModel!.nickName)
        view.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(userPortraitView!)
            make.left.equalTo(userPortraitView!.snp.right).offset(10/baseWidth)
        })
        
        orderTotalLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "919191", andText: NSLocalizedString("购买次数: ", comment: "") + "\(idealModel!.orderTotal)")
        view.addSubview(orderTotalLabel!)
        orderTotalLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(8/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
        })
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F8F8F8")
        view.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.top.equalTo(userPortraitView!.snp.bottom).offset(25/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(3)
        }
        
        makeButton = BaseButton.normalTitleButton(title: NSLocalizedString("制作实物", comment: ""), andTextFont: UIFont.systemFont(ofSize: 12), andTextColorHexString: "FFFFFF")
        makeButton?.backgroundColor = UIColor(gradientStyle: UIGradientStyle.leftToRight, withFrame: CGRect(x: 0, y: 0, width: 318/baseWidth, height: 49/baseWidth), andColors: [UIColor(hexString: "#00F5FE"), UIColor(hexString: "#18FFF2")])
        makeButton?.layer.shadowColor = UIColor(hexString: "00FDF6").cgColor
        makeButton?.layer.shadowOffset = CGSize(width: 0, height: 9)
        makeButton?.layer.shadowRadius = 18
        makeButton?.layer.shadowOpacity = 0.4
        makeButton?.layer.masksToBounds = false
        view.addSubview(makeButton!)
        makeButton?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(view).offset(-45/baseWidth)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 318/baseWidth, height: 49/baseWidth))
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击返回按钮
        backButton?.touchButtonClosure = {[weak self] _ in
            self?.dismissSelf()
        }
        
        //MARK: 点击制作按钮
        makeButton?.touchButtonClosure = {[weak self] _ in
            if let image: UIImage = self?.imageView?.image{
                self?.dismissSelf(animated: true, completion: {
                    self?.idealModel?.originalImage = image
                    self?.getOriginalImage?(self?.idealModel)
                })
            }
        }
    }
}
