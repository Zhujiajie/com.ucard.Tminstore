//
//  StoreIdealController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import Photos
import CHTCollectionViewWaterfallLayout

class StoreIdealController: BaseViewController {

    fileprivate let viewModel: StoreIdealViewModel = StoreIdealViewModel()
    
    /// 数据模型
    var postcardModel: MUFinalPostcardModel?
    
    /// 顶部用于在全部和个人之间切换的视图
    fileprivate var switchView: StoreIdealHeaderView?
    /// 承载网络图片和相册图片的collectionView
    fileprivate var containerCollectionView: StoreIdealContainerCollectionView?
    /// 展示创意的collectionView
    fileprivate var idealCollectionView: StoreIdealCollectionView?
    /// 展示相册的collectionView
    fileprivate var albumCollectionView: MUPhotoVC?
    /// 展示图片下载进度的进度条
    fileprivate var progressView: UIProgressView?
    /// 裁切图片的控制器
    fileprivate var imageCropVC: StoreIdealCropController?
    /// 下载的加载提示
    fileprivate var indicator: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(hexString: "#F8F8F8")
        setNavigationBar()
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if viewModel.postcardArray.isEmpty {
            idealCollectionView?.mj_header.beginRefreshing()//获取数据
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        indicator?.stopAnimating()//停止接在动画
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("挑选识别图片", comment: ""), attributeDic: attDic)
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        
        switchView = StoreIdealHeaderView()
        view.addSubview(switchView!)
        switchView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.height.equalTo(38/baseWidth)
        })
        
        containerCollectionView = StoreIdealContainerCollectionView()
        view.addSubview(containerCollectionView!)
        containerCollectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(switchView!.snp.bottom).offset(10/baseWidth)
            make.left.right.bottom.equalTo(view)
        })
        containerCollectionView?.delegate = self
        containerCollectionView?.dataSource = self
        
        albumCollectionView = MUPhotoVC()
        
        idealCollectionView = StoreIdealCollectionView()
        idealCollectionView?.delegate = self
        idealCollectionView?.dataSource = self
        
        progressView = UIProgressView()
        view.addSubview(progressView!)
        progressView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.top.right.equalTo(view)
        })
        progressView?.progressTintColor = UIColor(hexString: "59FFF4")
        progressView?.trackTintColor = UIColor.clear
    }
    
    //MARK: 进入照片裁剪界面
    fileprivate func enterPhotoCropVC(image: UIImage){
        imageCropVC = StoreIdealCropController(croppingStyle: TOCropViewCroppingStyle.default, image: image)
        //设置裁切的比例
        switch postcardModel!.goods!.goodsType {
        case CustomizationType.pillow:
            imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.preset4x3
        case CustomizationType.postcard:
            imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.preset7x5
        case CustomizationType.tShirt:
            imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.presetCustom
            imageCropVC?.customAspectRatio = CGSize(width: 5, height: 6)
        default:
            imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.presetCustom
            imageCropVC?.customAspectRatio = CGSize(width: 4, height: 5)
        }
        imageCropVC?.delegate = self
        present(imageCropVC!, animated: true, completion: nil)
    }
    
    //MARK: 下载图片时,展示下载提示器
    fileprivate func showIndicator(){
        if indicator == nil{
            indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            navigationController?.view.addSubview(indicator!)
            indicator?.snp.makeConstraints({ (make) in
                make.center.equalTo(navigationController!.view)
            })
            indicator?.hidesWhenStopped = true
        }
        indicator?.startAnimating()
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 切换collectionView
        switchView?.touchAllButtonClosure = {[weak self] _ in
            self?.containerCollectionView?.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
        switchView?.touchPersonButtonClosure = {[weak self] _ in
            self?.containerCollectionView?.scrollToItem(at: IndexPath.init(item: 1, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
        
        //MARK: 从系统相册获取图片
        albumCollectionView?.getImageClosure = {[weak self] (image: UIImage, _) in
            self?.enterPhotoCropVC(image: image)
        }
        
        //MARK: 下拉刷新
        idealCollectionView?.mj_header.refreshingBlock = {[weak self] _ in
            self?.idealCollectionView?.mj_footer.resetNoMoreData()
            self?.viewModel.pageNum = 1
            self?.viewModel.requestIdeal()
        }
        
        //MARK: 上拉刷新
        idealCollectionView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.requestIdeal()
        }
        
        //MARK: 需要刷新
        viewModel.shouldReloadData = {[weak self] _ in
            self?.indicator?.stopAnimating()
            self?.idealCollectionView?.mj_header.endRefreshing()
            self?.idealCollectionView?.mj_footer.endRefreshing()
            self?.idealCollectionView?.reloadData()
        }
        
        //MARK: 没有更多数据了
        viewModel.noMoreData = {[weak self] _ in
            self?.idealCollectionView?.mj_header.endRefreshing()
            self?.idealCollectionView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 网络出错
        viewModel.netWorkError = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.idealCollectionView?.mj_header.endRefreshing()
            self?.idealCollectionView?.mj_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 下载图片的进度
        viewModel.imageDownLoadProgress = {[weak self] (progress: Float) in
            self?.progressView?.isHidden = progress == 1
            self?.progressView?.setProgress(progress, animated: true)
        }
        
        //MARK: 需要展示下载提示器
        viewModel.shouldShowIndicator = {[weak self] _ in
            self?.showIndicator()
        }
    }
}

extension StoreIdealController: UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout, TOCropViewControllerDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.isKind(of: StoreIdealContainerCollectionView.self){
            return 2
        }else{
            return viewModel.postcardArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.isKind(of: StoreIdealContainerCollectionView.self){
            if indexPath.item == 0{
                let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "1", for: indexPath)
                cell.contentView.addSubview(idealCollectionView!)
                idealCollectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
                    make.edges.equalTo(cell.contentView)
                })
                return cell
            }else{
                let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "2", for: indexPath)
                cell.contentView.addSubview(albumCollectionView!.view)
                albumCollectionView?.view.snp.makeConstraints({ (make: ConstraintMaker) in
                    make.edges.equalTo(cell.contentView)
                })
                return cell
            }
        }else{
            let cell: StoreIdealCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.StoreIdealCollectionViewCellReuseIdentifier, for: indexPath) as! StoreIdealCollectionViewCell
            cell.postcardModel = viewModel.postcardArray[indexPath.item]
            return cell
        }
    }
    
    //MARK: 实现瀑布流
    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, sizeForItemAt indexPath: IndexPath!) -> CGSize {
        if collectionView.isKind(of: StoreIdealContainerCollectionView.self){
            if let layout: UICollectionViewFlowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
                return layout.itemSize
            }else{
                return CGSize.zero
            }
        }else{
            return viewModel.postcardArray[indexPath.item].imageSize
        }
    }
    
    //MARK: 点击cell，选择图片
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isKind(of: StoreIdealCollectionView.self) && viewModel.postcardArray[indexPath.item].isOriginalImageDownloaded == true{
            if let cell: StoreIdealCollectionViewCell = collectionView.cellForItem(at: indexPath) as? StoreIdealCollectionViewCell{
                if cell.image != nil{
                    postcardModel?.payModel.imagePrice = viewModel.postcardArray[indexPath.item].moneyPoint//获取价格
                    postcardModel?.originalId = viewModel.postcardArray[indexPath.item].originalId//获取图片ID
                    enterPhotoCropVC(image: cell.image!)
                }
            }
        }
    }
    
    //MARK: 滚动cell
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.isKind(of: StoreIdealContainerCollectionView.self){
            let isAll: Bool = Int(scrollView.contentOffset.x / screenWidth) == 0
            switchView?.isAll = isAll
        }
    }
    
    //MARK: 取消裁切照片
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        dismiss(animated: true, completion: nil)
    }
    //MARK: 成功获得照片
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        postcardModel?.frontImage = image
//        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)//保存裁切的照片
        dismiss(animated: true, completion: {[weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        })
    }
}
