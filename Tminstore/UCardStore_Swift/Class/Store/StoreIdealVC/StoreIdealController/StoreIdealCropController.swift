//
//  StoreIdealCropController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreIdealCropController: TOCropViewController {

    var ratio: TOCropViewControllerAspectRatioPreset?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset7x5
        aspectRatioPreset = ratio!
        aspectRatioLockEnabled = true
        resetAspectRatioEnabled = false
    }

}
