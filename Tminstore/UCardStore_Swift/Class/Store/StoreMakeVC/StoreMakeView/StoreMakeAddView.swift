//
//  StoreMakeAddView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 添加图片或视频的视图

import UIKit
import SnapKit

class StoreMakeAddView: UIView {

    ///点击添加图片按钮的事件
    var touchAddImageButtonClosure: (()->())?
    ///点击添加视频按钮的事件
    var touchAddVideoButtonClosure: (()->())?
    
    /// 添加图片的按钮
    fileprivate var addImageImageView: BaseImageView?
    fileprivate var addImageButton: BaseButton?
    /// 添加图片的label
    fileprivate var addImageLabel: BaseLabel?
    /// 添加视频的按钮
    fileprivate var addVideoImageView: BaseImageView?
    fileprivate var addVideoButton: BaseButton?
    /// 添加视频的label
    fileprivate var addVideoLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        //添加图片
        addImageImageView = BaseImageView.normalImageView(andImageName: "storeAddImage")
        addSubview(addImageImageView!)
        addImageImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self).offset(-57/baseWidth)
            make.top.equalTo(self).offset(15/baseWidth)
            make.size.equalTo(CGSize(width: 28/baseWidth, height: 28/baseWidth))
        })
        
        addImageLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "737373", andText: NSLocalizedString("选择图片", comment: ""))
        addSubview(addImageLabel!)
        addImageLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(addImageImageView!.snp.bottom).offset(4/baseWidth)
            make.centerX.equalTo(addImageImageView!)
        })
        
        addImageButton = BaseButton.normalIconButton()
        addSubview(addImageButton!)
        addImageButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(addImageImageView!)
            make.centerX.equalTo(addImageImageView!)
            make.bottom.equalTo(addImageLabel!)
            make.size.equalTo(CGSize(width: 48/baseWidth, height: 43/baseWidth))
        })
        addImageButton?.touchButtonClosure = {[weak self] _ in
            self?.touchAddImageButtonClosure?()
        }
        
        //添加视频
        addVideoImageView = BaseImageView.normalImageView(andImageName: "storeAddVideo")
        addSubview(addVideoImageView!)
        addVideoImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self).offset(57/baseWidth)
            make.topMargin.equalTo(addImageImageView!)
            make.size.equalTo(addImageImageView!)
        })
        
        addVideoLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "737373", andText: NSLocalizedString("添加视频", comment: ""))
        addSubview(addVideoLabel!)
        addVideoLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(addImageLabel!)
            make.centerX.equalTo(addVideoImageView!)
        })
        
        addVideoButton = BaseButton.normalIconButton()
        addSubview(addVideoButton!)
        addVideoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(addVideoImageView!)
            make.bottom.equalTo(addVideoLabel!)
            make.centerX.equalTo(addVideoImageView!)
            make.size.equalTo(CGSize(width: 48/baseWidth, height: 43/baseWidth))
        })
        addVideoButton?.touchButtonClosure = {[weak self] _ in
            self?.touchAddVideoButtonClosure?()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
