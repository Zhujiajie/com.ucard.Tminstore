//
//  StoreMakeTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreMakeTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        separatorStyle = UITableViewCellSeparatorStyle.none
        
        backgroundColor = UIColor.white
        
        register(StoreMakeTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.StoreMakeTableViewCellReuseIdentifier)
        
        tableFooterView = UIView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
