//
//  StoreMakeCardTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class StoreMakeTableViewCell: UITableViewCell {

//    /// 定制类型
//    var type: CustomizationType?{
//        didSet{
//            if type != nil{
//                setUI()
//            }
//        }
//    }
    
    /// 点击图片，去更换或者添加图片
    var tapImageViewClosure: (()->())?
    
    ///商品预览图片
    var previewImage: UIImage?{
        get{
            if postcardModel?.goods?.goodsType == CustomizationType.postcard{
                return imageContainerView?.getBitmapImage()
            }else{
                return backgroundImageView?.getBitmapImage()
            }
        }
    }
    
    /// 数据源
    var postcardModel: MUFinalPostcardModel?{
        didSet{
            if infoLabel?.text == nil{
                setUI()
            }
            if postcardModel?.frontImage != nil {
                contentImageView?.alpha = 1.0
            }else{
                contentImageView?.alpha = 0.22
            }
            contentImageView?.image = postcardModel?.frontImage
            addImageIcon?.isHidden = contentImageView?.image != nil
            if postcardModel?.goods?.goodsType == CustomizationType.postcard{
                textView?.text = postcardModel?.message
            }
        }
    }
    
    /// 展示介绍的Label
    fileprivate var infoLabel: BaseLabel?
    /// 背面枕头，杯子等imageView
    fileprivate var backgroundImageView: BaseImageView?
    /// 展示图片的imageView
    fileprivate var contentImageView: BaseImageView?
    /// 提示用户添加图片的icon
    fileprivate var addImageIcon: BaseImageView?
    /// 用于添加阴影和边框的承载视图
    fileprivate var imageContainerView: BaseView?
    /// 展示用户填写文字的textView
    fileprivate var textView: BaseTextView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        infoLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "787878")
        contentView.addSubview(infoLabel!)
        infoLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(36/baseWidth)
            make.centerX.equalTo(contentView)
        })
    }
    
    //MARK: 根据类型，设置UI
    fileprivate func setUI(){
        switch postcardModel!.goods!.goodsType{
        case CustomizationType.pillow:
            pillowUI()
        case CustomizationType.postcard:
            cardUI()
        case CustomizationType.tShirt:
            tShirtUI()
        default:
            cupUI()
        }
    }
    
    //MARK: 定制枕头的UI
    fileprivate func pillowUI(){
        
        infoLabel?.text = NSLocalizedString("AR抱枕定制", comment: "")
        
        backgroundImageView = BaseImageView.normalImageView()
        contentView.addSubview(backgroundImageView!)
        backgroundImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(infoLabel!.snp.bottom).offset(50/baseWidth)
            make.left.equalTo(contentView).offset(50/baseWidth)
            make.right.equalTo(contentView).offset(-49/baseWidth)
            make.height.equalTo(253/baseWidth)
        })
        backgroundImageView?.sd_setImage(with: appAPIHelp.pillowCustomizationURL)
        
        contentImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentImageView?.backgroundColor = UIColor(hexString: "#7A7A7A")
        contentImageView?.alpha = 0.22
        backgroundImageView?.addSubview(contentImageView!)
        contentImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(backgroundImageView!).offset(55/baseWidth)
            make.left.equalTo(backgroundImageView!).offset(40/baseWidth)
            make.bottom.equalTo(backgroundImageView!).offset(-55/baseWidth)
            make.right.equalTo(backgroundImageView!).offset(-46/baseWidth)
        })
        contentImageView?.layer.borderColor = UIColor(hexString: "979797").cgColor
        contentImageView?.layer.borderWidth = 1
        setTapImageClosure()
    }
    
    //MARK: 明信片的UI
    fileprivate func cardUI(){
        
        infoLabel?.text = NSLocalizedString("AR明信片定制", comment: "")
        
        imageContainerView = BaseView.shadowView()
        contentView.addSubview(imageContainerView!)
        imageContainerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(74/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 325/baseWidth, height: 234/baseWidth))
        })
        
        contentImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentImageView?.backgroundColor = UIColor(hexString: "D8D8D8")
        imageContainerView?.addSubview(contentImageView!)
        contentImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(imageContainerView!)
            make.size.equalTo(CGSize(width: 313/baseWidth, height: 224/baseWidth))
        })
        
        textView = BaseTextView.normalTextView(font: UIFont.systemFont(ofSize: 12), andPlaceHolder: NSLocalizedString("点击添加背面文字祝福", comment: ""), andCornerRadius: 3, andBorderWidth: 1, andBorderColorHexString: "DFDFDF")
        contentView.addSubview(textView!)
        textView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(imageContainerView!.snp.bottom).offset(16/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 315/baseWidth, height: 146/baseWidth))
        }
        textView?.addTextDidChangeHandler({ [weak self] (textView: FSTextView?) in
            if textView != nil{
                self?.postcardModel?.message = textView!.text
            }
        })
        setTapImageClosure()
    }
    
    //MARK: T恤的UI
    fileprivate func tShirtUI(){
        infoLabel?.text = NSLocalizedString("AR文化衫定制", comment: "")
        
        backgroundImageView = BaseImageView.normalImageView()
        contentView.addSubview(backgroundImageView!)
        backgroundImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(infoLabel!.snp.bottom).offset(54/baseWidth)
            make.left.equalTo(contentView).offset(39/baseWidth)
            make.right.equalTo(contentView).offset(-39/baseWidth)
            make.height.equalTo(285/baseWidth)
        })
        backgroundImageView?.sd_setImage(with: appAPIHelp.tshirtCustomizationURL)
        
        contentImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentImageView?.backgroundColor = UIColor(hexString: "#7A7A7A")
        contentImageView?.alpha = 0.22
        backgroundImageView?.addSubview(contentImageView!)
        contentImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(backgroundImageView!).offset(79/baseWidth)
            make.left.equalTo(backgroundImageView!).offset(86/baseWidth)
            make.bottom.equalTo(backgroundImageView!).offset(-47/baseWidth)
            make.right.equalTo(backgroundImageView!).offset(-79/baseWidth)
        })
        contentImageView?.layer.borderColor = UIColor(hexString: "979797").cgColor
        contentImageView?.layer.borderWidth = 1
        setTapImageClosure()
    }
    
    //MARK: 马克杯的UI
    fileprivate func cupUI(){
        
        infoLabel?.text = NSLocalizedString("AR马克杯定制", comment: "")
        
        backgroundImageView = BaseImageView.normalImageView()
        contentView.addSubview(backgroundImageView!)
        backgroundImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(infoLabel!.snp.bottom).offset(79/baseWidth)
            make.left.equalTo(contentView).offset(77/baseWidth)
            make.right.equalTo(contentView).offset(-20/baseWidth)
            make.height.equalTo(288/baseWidth)
        })
        backgroundImageView?.sd_setImage(with: appAPIHelp.cupCustomizationURL)
        
        contentImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentImageView?.backgroundColor = UIColor(hexString: "#7A7A7A")
        contentImageView?.alpha = 0.22
        backgroundImageView?.addSubview(contentImageView!)
        contentImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(backgroundImageView!).offset(75/baseWidth)
            make.left.equalTo(backgroundImageView!).offset(45/baseWidth)
            make.bottom.equalTo(backgroundImageView!).offset(-58.5/baseWidth)
            make.right.equalTo(backgroundImageView!).offset(-109.7/baseWidth)
        })
        contentImageView?.layer.borderColor = UIColor.white.cgColor
        contentImageView?.layer.borderWidth = 1
        setTapImageClosure()
    }
    
    //MARK: 给图片增加一个点击事件
    fileprivate func setTapImageClosure(){
        addImageIcon = BaseImageView.normalImageView(andImageName: "plusButton")
        contentImageView?.addSubview(addImageIcon!)
        addImageIcon?.snp.makeConstraints({ (make) in
            make.center.equalTo(contentImageView!)
            make.size.equalTo(CGSize(width: 24/baseWidth, height: 24/baseWidth))
        })
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapImageView(tap:)))
        contentImageView?.addGestureRecognizer(tap)
    }
    
    //MARK: 点击图片的事件
    func tapImageView(tap: UITapGestureRecognizer){
        tapImageViewClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
