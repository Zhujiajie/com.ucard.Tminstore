//
//  StoreMakeCardController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: --------------制作AR明信片的视图----------------

import UIKit
import SnapKit
import IQKeyboardManagerSwift
import MobileCoreServices

class StoreMakeController: BaseViewController {

    /// 定制类型
    var goods: GoodsModel?
    
    /// 创意图片模型
    var idealModel: IdealPhotoModel?
    
    /// 数据模型
    fileprivate var postcardModel: MUFinalPostcardModel = MUFinalPostcardModel()
    
    /// 添加图片和添加视频的视图
    fileprivate var addImageAndVideoView: StoreMakeAddView?
    /// 展示具体图片内容的tableView
    fileprivate var contentTableView: StoreMakeTableView?
    
    /// 选择视频的imagePicker
    fileprivate var videoImagePicker: UIImagePickerController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(hexString: "#F8F8F8")
        postcardModel.goods = goods
        
        if idealModel != nil {
            postcardModel.frontImage = idealModel!.croppedImage
            postcardModel.payModel.imagePrice = idealModel!.moneyPrice//获取价格
            postcardModel.originalId = idealModel!.originalId
        }
        
        setNavigationBar()
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cancelTansparentNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
        if postcardModel.frontImage != nil{
            contentTableView?.reloadData()
        }
        checkPreviewButtonCanEnabled()//判断是否可以启用预览按钮
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("定制", comment: ""), attributeDic: attDic)
        setNavigationBarRightButtonWithTitle(NSLocalizedString("预览", comment: ""))
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        addImageAndVideoView = StoreMakeAddView()
        view.addSubview(addImageAndVideoView!)
        addImageAndVideoView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(75/baseWidth)
        })
        
        contentTableView = StoreMakeTableView()
        view.addSubview(contentTableView!)
        contentTableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(7/baseWidth)
            make.bottom.equalTo(addImageAndVideoView!.snp.top).offset(-8/baseWidth)
            make.left.right.equalTo(view)
        })
        contentTableView?.delegate = self
        contentTableView?.dataSource = self
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 点击添加图片按钮
        addImageAndVideoView?.touchAddImageButtonClosure = {[weak self] _ in
            self?.enterIdealVC()
        }
        
        //MARK: 点击添加视频按钮
        addImageAndVideoView?.touchAddVideoButtonClosure = {[weak self] _ in
//            let vc: MUViewRecordVC = MUViewRecordVC()
//            vc.umengViewName = "AR定制添加视频"
//            vc.postcardModel = self?.postcardModel
//            vc.useDismiss = false
//            _ = self?.navigationController?.pushViewController(vc, animated: true)
//            vc.getVideoSuccess = {[weak self] (videoPath: URL) in
//                self?.postcardModel.videoPath = videoPath
//                self?.navigationController?.popViewController(animated: true)
//                self?.checkPreviewButtonCanEnabled()//判断是否可以启用预览按钮
//                if self?.postcardModel.frontImage != nil{
//                    self?.enterPreviewVC()
//                }
//            }
            if self?.videoImagePicker == nil {
                self?.videoImagePicker = UIImagePickerController()
                self?.videoImagePicker?.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self?.videoImagePicker?.mediaTypes = [kUTTypeMovie as String]
                self?.videoImagePicker?.delegate = self
            }
            self?.present((self?.videoImagePicker)!, animated: true, completion: nil)
        }
    }
    
    //MARK: 判断预览按钮能都使用
    /// 判断是否可以启用预览按钮
    fileprivate func checkPreviewButtonCanEnabled(){
        if postcardModel.frontImage != nil && postcardModel.videoPath != nil{
            navigationItem.rightBarButtonItem?.isEnabled = true
        }else{
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    //MARK: 点击预览按钮的事件
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        enterPreviewVC()
    }
    
    //MARK: 进入订单预览界面
    fileprivate func enterPreviewVC(){
        if let cell: StoreMakeTableViewCell = contentTableView?.cellForRow(at: IndexPath(row: 0, section: 0)) as? StoreMakeTableViewCell{
            postcardModel.previewImage = cell.previewImage
            let vc: StoreOrderPreviewController = StoreOrderPreviewController()
            vc.umengViewName = "订单预览"
            vc.postcardModel = postcardModel
            _ = navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: 进入选择图片的界面
    fileprivate func enterIdealVC(){
        let vc: StoreIdealController = StoreIdealController()
        vc.umengViewName = "挑选识别图像"
        vc.postcardModel = postcardModel
        _ = navigationController?.pushViewController(vc, animated: true)
        CFRunLoopWakeUp(CFRunLoopGetCurrent())
    }
}

extension StoreMakeController: UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 502/baseWidth
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: StoreMakeTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.StoreMakeTableViewCellReuseIdentifier, for: indexPath) as! StoreMakeTableViewCell
        cell.postcardModel = postcardModel
        //MARK: 点击图片，进入图片选择控制器
        cell.tapImageViewClosure = {[weak self] _ in
            self?.enterIdealVC()
        }
        return cell
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        dismiss(animated: true, completion: nil)
        
        if let url: URL = info[UIImagePickerControllerMediaURL] as? URL{
            //判断视频是否短于5秒
            let asset: AVAsset = AVAsset(url: url)
            let time: Float64 = CMTimeGetSeconds(asset.duration)
            //若视频短于5秒，重新进入视频拍摄界面
            if time <= 5 {
                dismiss(animated: false, completion: { [weak self] _ in
                    self?.alert(alertTitle: NSLocalizedString("视频不能短于5秒", comment: ""), leftClosure: { [weak self] _ in
                        self?.present((self?.videoImagePicker)!, animated: false, completion: nil)
                    })
                })
                return
            }
            
            //进入视频裁剪界面
            let videoCropVC: MUCropVideoVC = MUCropVideoVC()
            videoCropVC.videoURL = url
            videoCropVC.shouldUseDismiss = true
            videoCropVC.isComeFromMapView = true
            videoCropVC.umengViewName = "视频剪切"
            let nav: UINavigationController = UINavigationController(rootViewController: videoCropVC)
            present(nav, animated: true, completion: nil)
            
            //MARK: 裁剪视频成功
            videoCropVC.getVideoSuccess = {[weak self] (videoPathURL: URL) in
                self?.postcardModel.videoPath = videoPathURL
                self?.dismiss(animated: true, completion: nil)
            }
            
            
        }
    }
}
