//
//  StoreMainViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreMainViewModel: BaseViewModel {
    
    fileprivate let dataModel: StoreMainDataModel = StoreMainDataModel()
    /// 数据源
    var goodsArray: [GoodsModel] = [GoodsModel]()
    
    /// 获取数据成功的闭包
    var getDataSuccess: (()->())?
    /// 获取创意图片成功
    var getIdealImageSuccess: (()->())?
    
    /// 上传创意图片成功
    var uploadIdealSuccess: (()->())?
    
    /// 向服务器端请求第几页数据
    var pageNum: Int = 1
    /// 创意图片数组
    var idealModelArray: [IdealPhotoModel] = [IdealPhotoModel]()
    /// 没有更多数据了
    var noMoreData: (()->())?
    /// 可以加载数据了
    var shouldReloadData: (()->())?
    
    
    override init() {
        super.init()
    
        //MARK: 上传创意图片成功
        dataModel.uploadImageSuccess = {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.getIdealImageSuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
        }
        
        //MARK: 出错
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 网络请求完成
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 请求商品数据
    /// 请求商品数据
    func requestGoodsInfo(){
        showSVProgress(title: nil)
        dataModel.getGoodsInfo(pageIndex: 1, successClosure: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        for dic: [String: Any] in list{
                            let goods: GoodsModel = GoodsModel.initFromJson(dic: dic)
                            self?.goodsArray.append(goods)
                        }
                        self?.getDataSuccess?()
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        })
    }
    
    //MARK: 显示创意图片的
    /// 显示创意图片的
    func requestIdealOneTime(){
        dataModel.requestIdeal { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        self?.idealModelArray.removeAll()
                        for dic: [String: Any] in list{
                            if (self?.idealModelArray.count)! > 3{
                                break
                            }
                            let model: IdealPhotoModel = IdealPhotoModel.initFromDic(dic: dic)
                            if model.imgUrl != nil{
                                self?.idealModelArray.append(model)
                            }
                        }
                        self?.getIdealImageSuccess?()
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 上传创意图片
    /// 上传创意图片
    ///
    /// - Parameters:
    ///   - image: 图片
    ///   - price: 价格
    func uploadIdealImage(image: UIImage, andPrice price: String){
        showSVProgress(title: nil)
        dataModel.uploadImage(image: image, andPrice: price)
    }
    
    //MARK: 显示创意图片的
    /// 显示创意图片的
    func requestIdeal(){
        
        dataModel.requestIdeal(pageNum: pageNum) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if list.isEmpty{
                            self?.noMoreData?()
                        }else{
                            if self?.pageNum == 1{
                                self?.idealModelArray.removeAll()
                            }
                            for dic: [String: Any] in list{
                                let model: IdealPhotoModel = IdealPhotoModel.initFromDic(dic: dic)
                                if model.imgUrl != nil{
                                    self?.idealModelArray.append(model)
                                }
                            }
                            self?.shouldReloadData?()
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
}
