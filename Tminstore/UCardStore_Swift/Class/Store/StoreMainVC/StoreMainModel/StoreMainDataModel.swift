//
//  StoreMainDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreMainDataModel: BaseDataModel {
    
    /// 图片存放在又拍云的路径
    let imageKey: String = idealUyun + UUID().uuidString + ".png"
    
    /// 上传图片成功
    var uploadImageSuccess: ((_ result: [String: Any])->())?
    
    //MARK: 获取商品信息
    /// 获取商品信息
    ///
    /// - Parameters:
    ///   - pageIndex: 页数
    ///   - success: 成功的闭包
    func getGoodsInfo(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?) {
        let urlString: String = appAPIHelp.goodsListAPI + "\(pageIndex)"
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 请求创意图片的接口
    /// 请求创意图片的接口
    ///
    /// - Parameters:
    ///   - pageNum: 分页数
    ///   - success: 成功的闭包
    func requestIdeal(success: ((_ result: [String: Any])->())?){
        
        let urlStr: String = appAPIHelp.requestIdealListAPI + "1"
        
        let parameter: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameter, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 上传图片
    /// 上传图片
    ///
    /// - Parameters:
    ///   - image: 图片
    ///   - price: 价格
    func uploadImage(image: UIImage, andPrice price: String){
        
        //压缩一下图片
        let newImage: UIImage = UIImage(image: image, scaledToWidth: 1000)
        
        guard let imageData: Data = UIImagePNGRepresentation(newImage) else {
            errorClosure?(nil)
            return }
        
        uploadToUpyun(data: imageData, andSaveKey: imageKey, andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.uploadImageInfo(price: price)
            }, failClosure: { [weak self] (error: Error?, _, _) in
                self?.errorClosure?(error)
            }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                self?.uploadImageProgress(percent: completeCount/totalCount)
        })
    }
    
    //MARK: 上传信息
    fileprivate func uploadImageInfo(price: String){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "moneyPrice": Double(price)!,
            "imgUrl": appAPIHelp.upyunBase + imageKey
        ]
//        print(parameters)
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.uploadIdealAPI, ParametersDictionary: parameters, successClosure: { [weak self](result: [String: Any]) in
            self?.uploadImageSuccess?(result)
            }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 请求创意图片的接口
    /// 请求创意图片的接口
    ///
    /// - Parameters:
    ///   - pageNum: 分页数
    ///   - success: 成功的闭包
    func requestIdeal(pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlStr: String = appAPIHelp.requestIdealListAPI + "\(pageNum)"
        
        let parameter: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameter, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
