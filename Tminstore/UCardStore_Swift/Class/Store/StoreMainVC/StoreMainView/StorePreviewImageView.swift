//
//  StorePreviewImageView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 浏览上传的作品的入口图片
import UIKit
import SnapKit

class StorePreviewImageView: BaseImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        sd_setImage(with: appAPIHelp.StorePreviewImageURL)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
