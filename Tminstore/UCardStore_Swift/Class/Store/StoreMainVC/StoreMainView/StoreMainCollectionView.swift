//
//  StoreMainCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreMainCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: screenWidth/2 - 3/baseWidth, height: 205/baseWidth)
        layout.minimumLineSpacing = 5/baseWidth
        layout.minimumInteritemSpacing = 5/baseWidth
        layout.scrollDirection = UICollectionViewScrollDirection.vertical
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        backgroundColor = UIColor(hexString: "#F8F8F8")
        register(StoreMianCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.StoreMianCollectionViewCellReuseIdentifier)
        contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)//向下偏移
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
