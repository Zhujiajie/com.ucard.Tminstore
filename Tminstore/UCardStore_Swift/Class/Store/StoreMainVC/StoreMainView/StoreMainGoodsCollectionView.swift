//
//  StoreMainGoodsCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/29.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class StoreMainGoodsCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 8/baseWidth
        layout.minimumInteritemSpacing = 8/baseWidth
        layout.itemSize = CGSize(width: 129/baseWidth, height: 163/baseWidth)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        
        self.backgroundColor = UIColor.white
        self.contentInset = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 0)
        register(StoreMainGoodsCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.storeGoods)
        register(StoreMainIdealsCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.storeIdeals)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示商品的Cell
class StoreMainGoodsCollectionViewCell: UICollectionViewCell{
    
    var goods: GoodsModel?{
        didSet{
            setData()
        }
    }
    
    /// 展示图片
    fileprivate var imageView: BaseImageView?
    /// 展示价格
    fileprivate var priceLabel: BaseLabel?
    /// 展示标题
    fileprivate var titleLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = UIColor(hexString: "FCFCFC")
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(contentView).offset(-11/baseWidth)
            make.centerX.equalTo(contentView)
        })
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "EEC75C")
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(titleLabel!.snp.top).offset(2/baseWidth)
            make.centerX.equalTo(contentView)
        })
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.equalTo(contentView).offset(12/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
            make.bottom.equalTo(priceLabel!.snp.top).offset(-4/baseWidth)
        })
    }
    
    //MARK: 设置内容
    fileprivate func setData(){
        titleLabel?.text = goods?.goodsName
        priceLabel?.text = "¥\(goods!.goodsPrice)"
        imageView?.sd_setImage(with: goods?.goodsImageURL)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示创意图片的Cell
class StoreMainIdealsCollectionViewCell: UICollectionViewCell{
    
    var ideals: IdealPhotoModel?{
        didSet{
            setData()
        }
    }
    
    /// 展示图片
    fileprivate var imageView: BaseImageView?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: 设置内容
    fileprivate func setData(){
        imageView?.sd_setImage(with: ideals?.imgUrl)
    }
}
