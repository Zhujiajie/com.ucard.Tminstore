//
//  StoreUploadTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/29.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

fileprivate let firstCell: String = "firstCell"
fileprivate let secondCell: String = "secondCell"

class StoreUploadTableViewController: UITableViewController {

    /// 输入了错误的价格
    var errorPrice: (()->())?
    /// 点击添加图片的按钮
    var touchAddImageButtonClosure: (()->())?
    /// 用户编辑了价格
    var userInputPrice: (()->())?
    
    /// 用户设置的价格
    var price: String?{
        get{
            if let cell: StoreUploadTableViewPriceCell = tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? StoreUploadTableViewPriceCell{
                return cell.priceString
            }else{
                return nil
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(StoreUploadTableViewImageCell.self, forCellReuseIdentifier: firstCell)
        self.tableView.register(StoreUploadTableViewPriceCell.self, forCellReuseIdentifier: secondCell)
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.tableFooterView = UIView()
        
        self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.onDrag
        self.tableView.backgroundColor = UIColor(hexString: "#F8F8F8")
        
        self.tableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)
    }
    
    //MARK: tableView向上滚动
    func tableViewScrollToUp(height: CGFloat) {
        self.tableView.contentOffset = CGPoint(x: 0, y: height)
    }

    //MARK: 设置图片
    func getFinalImage(image: UIImage?) {
        if let cell: StoreUploadTableViewImageCell = tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? StoreUploadTableViewImageCell{
            cell.contentImage = image
        }
    }
    
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: StoreUploadTableViewImageCell = tableView.dequeueReusableCell(withIdentifier: firstCell, for: indexPath) as! StoreUploadTableViewImageCell
            /// 点击添加图片的按钮
            cell.touchAddImageButtonClosure = {[weak self] _ in
                self?.touchAddImageButtonClosure?()
            }
            return cell
        }else{
            let cell: StoreUploadTableViewPriceCell = tableView.dequeueReusableCell(withIdentifier: secondCell, for: indexPath) as! StoreUploadTableViewPriceCell
            /// 输入了错误的价格
            cell.errorPrice = {[weak self] _ in
                self?.errorPrice?()
            }
            /// 用户编辑了价格
            cell.userInputPrice = {[weak self] _ in
                self?.userInputPrice?()
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 405/baseWidth
        }else{
            return 185/baseWidth
        }
    }
}

//MARK: 展示图片的cell
class StoreUploadTableViewImageCell: UITableViewCell{
    
    /// 图片
    var contentImage: UIImage?{
        didSet{
            contentImageView?.image = contentImage
            addButton?.isHidden = contentImage != nil
        }
    }
    
    /// 点击添加图片的按钮
    var touchAddImageButtonClosure: (()->())?
    
    /// 展示图片
    fileprivate var contentImageView: BaseImageView?
    /// 添加图片的按钮
    fileprivate var addButton: BaseButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        contentImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill, andImageName: nil)
        contentImageView?.backgroundColor = UIColor(hexString: "D8D8D8")
        contentView.addSubview(contentImageView!)
        contentImageView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(contentView).inset(UIEdgeInsets(top: 12/baseWidth, left: 25/baseWidth, bottom: 17/baseWidth, right: 25/baseWidth))
        })
        contentImageView?.addTapGesture()
        //MARK: 点击imageView, 更换图片
        contentImageView?.tapImageView = {[weak self] _ in
            self?.touchAddImageButtonClosure?()
        }
        
        addButton = BaseButton.normalIconButton(iconName: "idealVCAdd")
        contentView.addSubview(addButton!)
        addButton?.snp.makeConstraints({ (make) in
            make.center.equalTo(contentImageView!)
            make.size.equalTo(CGSize(width: 54/baseWidth, height: 54/baseWidth))
        })
        
        //MARK: 点击添加图片的按钮
        addButton?.touchButtonClosure = {[weak self] _ in
            self?.touchAddImageButtonClosure?()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 输入价格的cell
class StoreUploadTableViewPriceCell: UITableViewCell{
    
    /// 用户输入的价格
    var priceString: String? {
        get {
            return textField?.text
        }
    }
    
    /// 用户编辑了价格
    var userInputPrice: (()->())?
    
    /// 输入了错误的价格
    var errorPrice: (()->())?
    
    /// 设置价格label
    fileprivate var titleLabel: BaseLabel?
    /// 输入价格的textField
    fileprivate var textField: BaseTextField?
    /// 说明label
    fileprivate var supportLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F5F5F5")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(contentView)
            make.height.equalTo(4)
        }
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "363636", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("设置价格", comment: ""))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine.snp.bottom).offset(13/baseWidth)
            make.centerX.equalTo(contentView)
        })
        
        textField = BaseTextField.numberTextField(font: UIFont.systemFont(ofSize: 12), andTextColorHexString: "363636", andPlaceHolder: "3-100")
        textField?.borderStyle = UITextBorderStyle.roundedRect
        contentView.addSubview(textField!)
        textField?.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(29/baseWidth)
            make.size.equalTo(CGSize(width: 163/baseWidth, height: 38/baseWidth))
            make.centerX.equalTo(contentView)
        })
        //MARK: 完成输入
        textField?.didEndEditing = {[weak self] (price: String) in
            self?.userInputPrice?()
            if self?.checkPrice(price: price) == false {
                self?.textField?.text = nil
                self?.errorPrice?()
            }
        }
        
        supportLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "919191", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 2, andText: NSLocalizedString("您上传的图像将会被购买并\n制作成AR时光记忆", comment: ""))
        contentView.addSubview(supportLabel!)
        supportLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(textField!.snp.bottom).offset(21/baseWidth)
            make.centerX.equalTo(contentView)
        })
    }
    
    //MARK: 检查输入的价格
    fileprivate func checkPrice(price: String)->Bool{
        if let priceNumber: Int = Int(price){
            switch priceNumber {
            case 3...100:
                return true
            default:
                return false
            }
        }else{
            return false
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
