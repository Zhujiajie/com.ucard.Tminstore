//
//  StoreMainTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/29.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

fileprivate let firstCell: String = "firstCell"
fileprivate let secondCell: String = "secondCell"
fileprivate let thirdCell: String = "thirdCell"

class StoreMainTableViewController: UITableViewController {

    weak var viewModel: StoreMainViewModel?

    /// 点击下载按钮的事件
    var touchDownLoadButtonClosure: (()->())?
    
    /// 点击定制按钮的事件
    var touchMakeButtonClosure: (()->())?
    
    /// 点击作品按钮的事件
    var touchWorkButtonClosure: (()->())?
    
    /// 点击了cell
    var selectGoodsCell: ((_ indexPath: IndexPath)->())?
    
    /// 点击了cell
    var selectIdealCell: ((_ indexPath: IndexPath)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(StoreMainTableViewImageCell.self, forCellReuseIdentifier: firstCell)
        self.tableView.register(StoreMainTableViewGoodsCell.self, forCellReuseIdentifier: secondCell)
        self.tableView.register(StoreMainTableViewIdealsCell.self, forCellReuseIdentifier: thirdCell)
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.tableFooterView = UIView()
        
        closures()
    }

    //MARK: 回调
    fileprivate func closures(){
        
        //MARK: 获取商品信息成功
        viewModel?.getDataSuccess = {[weak self] _ in
            if let cell: StoreMainTableViewGoodsCell = self?.tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? StoreMainTableViewGoodsCell{
                cell.reloadCollectionViewData()
            }
        }
        
        //MARK: 获取创意图片成功
        viewModel?.getIdealImageSuccess = {[weak self] _ in
            if let cell: StoreMainTableViewIdealsCell = self?.tableView.cellForRow(at: IndexPath.init(row: 2, section: 0)) as? StoreMainTableViewIdealsCell{
                cell.reloadCollectionViewData()
            }
        }
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell: StoreMainTableViewImageCell = tableView.dequeueReusableCell(withIdentifier: firstCell, for: indexPath) as! StoreMainTableViewImageCell
            
            //MARK: 点击上传按钮的事件
            cell.touchDownloadButtonClosure = {[weak self] _ in
                self?.touchDownLoadButtonClosure?()
            }
            return cell
        case 1:
            let cell: StoreMainTableViewGoodsCell = tableView.dequeueReusableCell(withIdentifier: secondCell, for: indexPath) as! StoreMainTableViewGoodsCell
            cell.viewModel = viewModel
            
            //MARK: 点击定制按钮
            cell.touchMakeButtonClosure = {[weak self] _ in
                self?.touchMakeButtonClosure?()
            }
            
            //MARK: 点击了实物cell
            cell.selectGoodsCell = {[weak self] (indexPath: IndexPath) in
                self?.selectGoodsCell?(indexPath)
            }
            return cell
        default:
            let cell: StoreMainTableViewIdealsCell = tableView.dequeueReusableCell(withIdentifier: thirdCell, for: indexPath) as! StoreMainTableViewIdealsCell
            cell.viewModel = viewModel
            
            //MARK: 点击作品按钮
            cell.touchWorkButtonClosure = {[weak self] _ in
                self?.touchWorkButtonClosure?()
            }
            
            //MARK: 点击了cell
            cell.selectIdealCell = {[weak self] (indexPath: IndexPath) in
                self?.selectIdealCell?(indexPath)
            }
            
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 163/baseWidth
        case 1:
            return 230/baseWidth
        default:
            return 205/baseWidth
        }
    }
}

//MARK: 展示介绍图片的cell
class StoreMainTableViewImageCell: UITableViewCell{
    
    /// 点击了下载按钮
    var touchDownloadButtonClosure: (()->())?
    
    /// 推广图片
    fileprivate var previewEnterImageView: StorePreviewImageView?
    
    /// 点击下载标题
    fileprivate var titleLabel: BaseLabel?
    /// 右箭头
    fileprivate var rightArrowICON: BaseImageView?
    /// 点击下载的按钮
    fileprivate var downLoadButton: BaseButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        previewEnterImageView = StorePreviewImageView(frame: CGRect.zero)
        contentView.addSubview(previewEnterImageView!)
        previewEnterImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
        
        downLoadButton = BaseButton.normalIconButton()
        downLoadButton?.backgroundColor = UIColor(white: 1.0, alpha: 0.84)
        contentView.addSubview(downLoadButton!)
        downLoadButton?.snp.makeConstraints({ (make) in
            make.left.equalTo(contentView).offset(28/baseWidth)
            make.bottom.equalTo(contentView).offset(-13/baseWidth)
            make.size.equalTo(CGSize(width: 99/baseWidth, height: 30/baseWidth))
        })
        //MARK: 点击下载按钮
        downLoadButton?.touchButtonClosure = {[weak self] _ in
//            let appURL: URL = URL(string: "https://itunes.apple.com/cn/app/timeory/id1265724080?mt=8")!
//            if UIApplication.shared.canOpenURL(appURL) {
//                UIApplication.shared.openURL(appURL)
//            }
            self?.touchDownloadButtonClosure?()
        }
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "797979", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: NSLocalizedString("点击下载", comment: ""))
        downLoadButton?.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(downLoadButton!)
            make.left.equalTo(downLoadButton!).offset(16/baseWidth)
        })
        
        rightArrowICON = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "downLoadRightArrow")
        downLoadButton?.addSubview(rightArrowICON!)
        rightArrowICON?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(downLoadButton!)
            make.right.equalTo(downLoadButton!).offset(-4.5/baseWidth)
            make.size.equalTo(CGSize(width: 15/baseWidth, height: 12/baseWidth))
        })
//
//        uploadButton = BaseButton.normalTitleButton(title: NSLocalizedString("点击上传", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "FFFFFF", andBackgroundColorHexString: nil, andBorderColorHexString: "FFFFFF", andBorderWidth: 1, andCornerRadius: 0)
//        contentView.addSubview(uploadButton!)
//        uploadButton?.snp.makeConstraints({ (make) in
//            make.top.equalTo(titleLabel!.snp.bottom).offset(6.5/baseWidth)
//            make.leftMargin.equalTo(titleLabel!)
//            make.size.equalTo(CGSize(width: 100/baseWidth, height: 32/baseWidth))
//        })
//        
//        //MARK: 点击上传按钮
//        uploadButton?.touchButtonClosure = {[weak self] _ in
//            self?.touchUploadButtonClosure?()
//        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示商品的cell
class StoreMainTableViewGoodsCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource{
    
    weak var viewModel: StoreMainViewModel?
    
    /// 点击定制按钮的事件
    var touchMakeButtonClosure: (()->())?
    /// 点击了cell
    var selectGoodsCell: ((_ indexPath: IndexPath)->())?
    
    /// 透明按钮
    fileprivate var clearButton: BaseButton?
    /// icon1
    fileprivate var icon1: BaseImageView?
    /// 标题
    fileprivate var titleLabel: BaseLabel?
    /// icon2
    fileprivate var icon2: BaseImageView?
    /// 展示商品的CollectionView
    fileprivate var collectionView: StoreMainGoodsCollectionView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        clearButton = BaseButton.normalIconButton(iconName: nil)
        contentView.addSubview(clearButton!)
        clearButton?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(53/baseWidth)
        })
        //MARK: 点击定制按钮的事件
        clearButton?.touchButtonClosure = {[weak self] _ in
            self?.touchMakeButtonClosure?()
        }
        
        icon1 = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "storeMakeICON1")
        clearButton?.addSubview(icon1!)
        icon1?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(clearButton!)
            make.left.equalTo(clearButton!).offset(14/baseWidth)
            make.size.equalTo(CGSize(width: 5/baseWidth, height: 22/baseWidth))
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "000000", andText: NSLocalizedString("定制", comment: ""))
        clearButton?.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(icon1!)
            make.left.equalTo(icon1!.snp.right).offset(15/baseWidth)
        })
        
        icon2 = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "storeMakeICON2")
        clearButton?.addSubview(icon2!)
        icon2?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(icon1!)
            make.right.equalTo(clearButton!).offset(-20/baseWidth)
            make.size.equalTo(CGSize(width: 9/baseWidth, height: 15/baseWidth))
        })
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F8F8F8")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(4)
        }
        
        collectionView = StoreMainGoodsCollectionView()
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(contentView)
            make.top.equalTo(clearButton!.snp.bottom)
            make.bottom.equalTo(grayLine.snp.top)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    //MARK: 重载数据
    /// 重载数据
    func reloadCollectionViewData(){
        collectionView?.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        return viewModel!.goodsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: StoreMainGoodsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.storeGoods, for: indexPath) as! StoreMainGoodsCollectionViewCell
        cell.goods = viewModel!.goodsArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectGoodsCell?(indexPath)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示创意图片的cell
class StoreMainTableViewIdealsCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource{
    
    weak var viewModel: StoreMainViewModel?
    
    /// 点击作品按钮的事件
    var touchWorkButtonClosure: (()->())?
    /// 点击了cell
    var selectIdealCell: ((_ indexPath: IndexPath)->())?
    
    /// 透明按钮
    fileprivate var clearButton: BaseButton?
    /// icon1
    fileprivate var icon1: BaseImageView?
    /// 标题
    fileprivate var titleLabel: BaseLabel?
    /// icon2
    fileprivate var icon2: BaseImageView?
    
    /// 展示创意图片的CollectionView
    fileprivate var collectionView: StoreMainGoodsCollectionView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        clearButton = BaseButton.normalIconButton(iconName: nil)
        contentView.addSubview(clearButton!)
        clearButton?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(53/baseWidth)
        })
        //MARK: 点击作品按钮的事件
        clearButton?.touchButtonClosure = {[weak self] _ in
            self?.touchWorkButtonClosure?()
        }
        
        icon1 = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "storeMakeICON1")
        clearButton?.addSubview(icon1!)
        icon1?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(clearButton!)
            make.left.equalTo(clearButton!).offset(14/baseWidth)
            make.size.equalTo(CGSize(width: 5/baseWidth, height: 22/baseWidth))
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "000000", andText: NSLocalizedString("作品", comment: ""))
        clearButton?.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(icon1!)
            make.left.equalTo(icon1!.snp.right).offset(15/baseWidth)
        })
        
        icon2 = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "storeMakeICON2")
        clearButton?.addSubview(icon2!)
        icon2?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(icon1!)
            make.right.equalTo(clearButton!).offset(-20/baseWidth)
            make.size.equalTo(CGSize(width: 9/baseWidth, height: 15/baseWidth))
        })
        
        collectionView = StoreMainGoodsCollectionView()
        if let layout: UICollectionViewFlowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            layout.itemSize = CGSize(width: 129/baseWidth, height: 142/baseWidth)
        }
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(contentView)
            make.top.equalTo(clearButton!.snp.bottom)
            make.bottom.equalTo(contentView)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    //MARK: 重载数据
    /// 重载数据
    func reloadCollectionViewData(){
        collectionView?.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        return viewModel!.idealModelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: StoreMainIdealsCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.storeIdeals, for: indexPath) as! StoreMainIdealsCollectionViewCell
        cell.ideals = viewModel!.idealModelArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectIdealCell?(indexPath)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
