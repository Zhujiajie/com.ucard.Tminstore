//
//  StoreMainIdealController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreMainIdealController: BaseViewController {

    fileprivate let paymentPasswordViewModel: MUPaymentManagementViewModel = MUPaymentManagementViewModel()
    
    fileprivate let viewModel: StoreMainViewModel = StoreMainViewModel()
    
    /// 展示创意图片的collectionViewController
    fileprivate var collectionViewController: StoreMainIdealCollectionViewController?
    
    /// 上传创意图片的按钮
    fileprivate var uploadButton: BaseButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionViewController?.collectionView?.mj_header.beginRefreshing()//获取商品信息
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("作品", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        collectionViewController = StoreMainIdealCollectionViewController()
        collectionViewController?.viewModel = viewModel
        view.addSubview(collectionViewController!.view)
        collectionViewController?.view.snp.makeConstraints({ (make) in
            make.edges.equalTo(view)
        })
        
        uploadButton = BaseButton.normalIconButton(iconName: "uploadIdeal")
        view.addSubview(uploadButton!)
        uploadButton?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(view).offset(-15/baseWidth)
            make.right.equalTo(view).offset(-15/baseWidth)
            make.size.equalTo(CGSize(width: 70/baseWidth, height: 70/baseWidth))
        })
    }

    //MARK: 回调
    fileprivate func closure(){
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.collectionViewController?.collectionView?.mj_header.endRefreshing()
            self?.collectionViewController?.collectionView?.mj_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击了Cell
        collectionViewController?.selectCell = {[weak self] (indexPath: IndexPath) in
            self?.enterIdelaDetailVC(indexPath: indexPath)
        }
        
        //MARK: 判断上传按钮是否需要隐藏
        collectionViewController?.shouldShowOrHideUploadButton = {[weak self] (shouldShow: Bool) in
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { 
                if shouldShow == true{
                    self?.uploadButton?.alpha = 1
                }else{
                    self?.uploadButton?.alpha = 0
                }
            }, completion: nil)
        }
        
        //MARK: 点击上传按钮
        uploadButton?.touchButtonClosure = {[weak self] _ in
            if getUserLoginStatus() {
                //判断是否设置过支付密码
                self?.paymentPasswordViewModel.checkPaymentPasswordStatus()
//                self?.enterUploadImageVC()
            }else{
               self?.enterLoginView()
            }
        }

        //MARK: 判断支付密码状态成功
        paymentPasswordViewModel.checkPaymentPassword = {[weak self] (result: Bool) in
//            if result {
//                self?.enterUploadImageVC()
//            }else{
//              self?.beginSetPaymentPassword()
//
//            }
            self?.enterUploadImageVC()

            
            
        }
        
        //MARK: 出错的闭包
        paymentPasswordViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 进入创意图片详情界面
    fileprivate func enterIdelaDetailVC(indexPath: IndexPath){
        let vc: StoreIdealDetailController = StoreIdealDetailController()
        vc.umengViewName = "创意图片详情"
        vc.idealModel = viewModel.idealModelArray[indexPath.item]
        present(vc, animated: true, completion: nil)
        
        //MARK: 获得了创意图片原图
        vc.getOriginalImage = {[weak self] (idealModel: IdealPhotoModel?) in
            self?.enterGoosListVC(idealModel: idealModel)
        }
    }
    
    //MARK: 进入视图列表
    fileprivate func enterGoosListVC(idealModel: IdealPhotoModel?){
        let vc: StoreGoodsController = StoreGoodsController()
        vc.umengViewName = "定制列表"
        vc.idealModel = idealModel
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 进入上传图片的界面
    fileprivate func enterUploadImageVC(){
        let vc: StoreUploadController = StoreUploadController()
        vc.umengViewName = "上传作品"
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 进入设置支付密码的界面
    fileprivate func beginSetPaymentPassword(){
        alert(alertTitle: NSLocalizedString("请先设置支付密码", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("好的", comment: ""), rightButtonTitle: nil, leftClosure: { [weak self] _ in
            let vc: MUPaymentManagementViewController = MUPaymentManagementViewController()
            vc.umengViewName = "支付管理"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
            }, rightClosure: nil, presentComplition: nil)
    }
}
