//
//  StoreUploadController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/29.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class StoreUploadController: BaseViewController {

    fileprivate let viewModel: StoreMainViewModel = StoreMainViewModel()
    
    /// tableView
    fileprivate var tableViewController: StoreUploadTableViewController?
    
    /// imagePicker
    fileprivate var imagePicker: UIImagePickerController?
    /// 裁切图片的控制器
    fileprivate var imageCropVC: StoreIdealCropController?
    /// 成功裁剪的图片
    fileprivate var finalImage: UIImage?{
        didSet{
            tableViewController?.getFinalImage(image: finalImage)
            checkUploadButton()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dismissKeyboard), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        setNavigationBar()
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        setNavigationBarRightButtonWithTitle(NSLocalizedString("上传", comment: ""))
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("上传作品", comment: ""), attributeDic: attDic)
        
        // 暂时不能使用这个按钮
        navigationItem.rightBarButtonItem?.isEnabled = false
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        tableViewController = StoreUploadTableViewController()
        view.addSubview(tableViewController!.view)
        tableViewController?.view.snp.makeConstraints({ (make) in
            make.edges.equalTo(view)
        })
    }
    
    //MARK: 回调
    fileprivate func closures(){
        
        //MARK: 输入了错误的价格
        tableViewController?.errorPrice = {[weak self] _ in
            self?.navigationItem.rightBarButtonItem?.isEnabled = false
            self?.alert(alertTitle: NSLocalizedString("价格需要在3到100之间", comment: ""))
        }
        
        //MARK: 点击了添加图片的按钮
        tableViewController?.touchAddImageButtonClosure = {[weak self] _ in
            self?.beginChoosePhoto()
        }
        
        //MARK: 用户编辑了价格
        tableViewController?.userInputPrice = {[weak self] _ in
            self?.checkUploadButton()
        }
        
        //MARK: 上传成功
        viewModel.getIdealImageSuccess = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("上传成功", comment: ""), messageString: nil, leftClosure: { 
                self?.navigationController?.popViewController(animated: true)
            }, rightClosure: nil, presentComplition: nil)
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 监测键盘高度
    func keyboardWillChangeFrame(notification: Notification){
        guard let userInfo: [String: Any] = notification.userInfo as? [String: Any] else {return}
        if let keyboardFrame: CGRect = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect{
            tableViewController?.tableViewScrollToUp(height: keyboardFrame.height)
        }
    }
    
    //MAKR: 检测键盘消失
    func dismissKeyboard() {
        tableViewController?.tableViewScrollToUp(height: 0)
    }
    
    //MARK: 检测上传按钮是否可用
    fileprivate func checkUploadButton(){
        if finalImage != nil && tableViewController?.price != nil && tableViewController?.price != ""{
            navigationItem.rightBarButtonItem?.isEnabled = true
        }else{
            navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    //MARK: 进入相册
    /**
     进入相册
     */
    fileprivate func beginChoosePhoto() {
        //判断相册是否可用
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            if imagePicker == nil {
                imagePicker = UIImagePickerController()
                imagePicker?.sourceType = UIImagePickerControllerSourceType.photoLibrary
                imagePicker?.delegate = self
            }
            present(imagePicker!, animated: true, completion: nil)
        }else{
            alert(alertTitle: nil, messageString: NSLocalizedString("相册不可用", comment: ""), leftButtonTitle: NSLocalizedString("好的", comment: ""), rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//提醒用户相册不可用
        }
    }
    
    //MARK: 进入照片裁剪界面
    fileprivate func enterPhotoCropVC(image: UIImage){
        imageCropVC = StoreIdealCropController(croppingStyle: TOCropViewCroppingStyle.default, image: image)
        imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.presetCustom
        imageCropVC?.customAspectRatio = CGSize(width: 6, height: 7)
        imageCropVC?.delegate = self
        present(imageCropVC!, animated: true, completion: nil)
    }
    
    //MARK: 点击导航栏右按钮, 上传图片
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        viewModel.uploadIdealImage(image: finalImage!, andPrice: tableViewController!.price!)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

extension StoreUploadController: UIImagePickerControllerDelegate, UINavigationControllerDelegate, TOCropViewControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        dismiss(animated: true, completion: nil)
        if let image: UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            enterPhotoCropVC(image: image)
        }
    }
    
    //MARK: 取消裁切照片
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        dismiss(animated: true, completion: nil)
    }
    //MARK: 成功获得照片
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        dismiss(animated: true, completion: nil)
        finalImage = image
    }
}
