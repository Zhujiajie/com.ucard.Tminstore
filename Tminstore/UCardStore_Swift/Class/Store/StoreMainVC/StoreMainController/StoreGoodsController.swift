//
//  StoreGoodsController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class StoreGoodsController: BaseViewController {

    /// 从创意图片界面获得原图
    var idealModel: IdealPhotoModel?
    
    fileprivate let viewModel: StoreMainViewModel = StoreMainViewModel()
    
    /// 展示定制内容的collectionView
    fileprivate var contentCollectionView: StoreMainCollectionView?
    
    /// 裁剪图片的控制器
    fileprivate var imageCropVC: StoreIdealCropController?
    
    /// 选择的indexPath
    fileprivate var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if viewModel.goodsArray.isEmpty{
            viewModel.requestGoodsInfo()//获取商品信息
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("定制", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        contentCollectionView = StoreMainCollectionView()
        view.addSubview(contentCollectionView!)
        contentCollectionView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(view)
        })
        contentCollectionView?.delegate = self
        contentCollectionView?.dataSource = self
    }

    //MARK: 回调
    fileprivate func closure(){
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 成功获得商品信息
        viewModel.getDataSuccess = {[weak self] _ in
            self?.contentCollectionView?.reloadData()
        }
    }
    
    //MARK: 进入图片裁切界面
    fileprivate func enterImageCropVC(indexPath: IndexPath){
        imageCropVC = StoreIdealCropController(croppingStyle: TOCropViewCroppingStyle.default, image: idealModel!.originalImage!)
        //设置裁切的比例
        switch viewModel.goodsArray[indexPath.item].goodsType {
        case CustomizationType.pillow:
            imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.preset4x3
        case CustomizationType.postcard:
            imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.preset7x5
        case CustomizationType.tShirt:
            imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.presetCustom
            imageCropVC?.customAspectRatio = CGSize(width: 5, height: 6)
        default:
            imageCropVC?.ratio = TOCropViewControllerAspectRatioPreset.presetCustom
            imageCropVC?.customAspectRatio = CGSize(width: 4, height: 5)
        }
        imageCropVC?.delegate = self
        present(imageCropVC!, animated: true, completion: nil)
    }
    
    //MARK: 进入制作界面
    fileprivate func enterMakeVC(idealModel: IdealPhotoModel?){
        let vc: StoreMakeController = StoreMakeController()
        vc.goods = viewModel.goodsArray[selectedIndexPath!.item]
        vc.idealModel = idealModel
        switch viewModel.goodsArray[selectedIndexPath!.item].goodsType {
        case CustomizationType.postcard:
            vc.umengViewName = "AR明信片定制"
        case CustomizationType.pillow:
            vc.umengViewName = "AR抱枕定制"
        case CustomizationType.tShirt:
            vc.umengViewName = "AR文化衫定制"
        case CustomizationType.cup:
            vc.umengViewName = "AR马克杯定制"
        }
        _ = navigationController?.pushViewController(vc, animated: true)
    }
}

extension StoreGoodsController: UICollectionViewDelegate, UICollectionViewDataSource, TOCropViewControllerDelegate{

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.goodsArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: StoreMianCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.StoreMianCollectionViewCellReuseIdentifier, for: indexPath) as! StoreMianCollectionViewCell
        cell.goods = viewModel.goodsArray[indexPath.item]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        //判断是否有已选择的创意图片
        if idealModel == nil {
            enterMakeVC(idealModel: nil)
        }else{
            enterImageCropVC(indexPath: indexPath)
        }
    }
    
    //MARK: 取消裁切照片
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: 成功获得照片
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        idealModel?.croppedImage = image//获取裁切过的图片
        dismiss(animated: true, completion: {[weak self] _ in
            self?.enterMakeVC(idealModel: self?.idealModel)
        })
    }
}
