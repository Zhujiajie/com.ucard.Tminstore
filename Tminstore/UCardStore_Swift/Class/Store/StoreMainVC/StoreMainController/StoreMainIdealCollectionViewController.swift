//
//  StoreMainIdealCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "Cell"

class StoreMainIdealCollectionViewController: UICollectionViewController {

    weak var viewModel: StoreMainViewModel?
    
    weak var photoViewModel: PPhotoViewModel?
    
    /// 需要隐藏上传按钮
    var shouldShowOrHideUploadButton: ((_ shouldShow: Bool)->())?
    
    /// 点击cell的事件
    var selectCell: ((_ indexPath: IndexPath)->())?
    
    /// 上一次的滚动距离
    fileprivate var lastYContentOffset: CGFloat = 0.0
    
    init() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 26/baseWidth
        layout.minimumInteritemSpacing = 10/baseWidth
        layout.itemSize = CGSize(width: 169/baseWidth, height: 255/baseWidth)
        layout.scrollDirection = UICollectionViewScrollDirection.vertical
        
        super.init(collectionViewLayout: layout)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Register cell classes
        self.collectionView!.register(StoreMainIdealCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.backgroundColor = UIColor.white
        
        self.collectionView?.contentInset = UIEdgeInsets(top: 0, left: 10/baseWidth, bottom: 20/baseWidth, right: 10/baseWidth)
        
        self.collectionView?.mj_header = BaseAnimationHeader()
        self.collectionView?.mj_footer = BaseRefreshFooter()
        
        closures()
    }

    //MARK: 回调
    fileprivate func closures(){
        //MARK: 获得数据成功
        viewModel?.shouldReloadData = {[weak self] _ in
            self?.collectionView?.mj_header.endRefreshing()
            self?.collectionView?.mj_footer.endRefreshing()
            self?.collectionView?.reloadData()
        }
        
        //MARK: 没有更多数据了
        viewModel?.noMoreData = {[weak self] _ in
            self?.collectionView?.mj_header.endRefreshing()
            self?.collectionView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 成功获得用户创意图片
        photoViewModel?.getIdealData = {[weak self] _ in
            self?.collectionView?.mj_header.endRefreshing()
            self?.collectionView?.mj_footer.endRefreshing()
            self?.collectionView?.reloadData()
        }
        
        //MARK: 没有更多创意图片
        photoViewModel?.noMoreIdealData = {[weak self] _ in
            self?.collectionView?.mj_header.endRefreshing()
            self?.collectionView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 下拉刷新
        collectionView?.mj_header.refreshingBlock = {[weak self] _ in
            if self?.viewModel != nil {
                self?.viewModel?.pageNum = 1
                self?.viewModel?.requestIdeal()
            }else{
                self?.photoViewModel?.idealPageNum = 1
                self?.photoViewModel?.checkIdeal()
            }
        }
        
        //MARK: 上拉刷新
        collectionView?.mj_footer.refreshingBlock = {[weak self] _ in
            if self?.viewModel != nil {
                self?.viewModel?.requestIdeal()
            }else{
                self?.photoViewModel?.checkIdeal()
            }
        }
    }
    
    
    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel != nil {
            return viewModel!.idealModelArray.count
        }else if photoViewModel != nil {
            return photoViewModel!.userIdealDataArray.count
        }else{
            return 0
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: StoreMainIdealCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! StoreMainIdealCollectionViewCell
        if viewModel != nil {
            cell.model = viewModel!.idealModelArray[indexPath.row]
        }else {
            cell.model = photoViewModel!.userIdealDataArray[indexPath.row]
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectCell?(indexPath)
    }
    
    //MARK: 检测scrollView的滚动方向
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if lastYContentOffset > scrollView.contentOffset.y {
            shouldShowOrHideUploadButton?(true)
        }else{
            shouldShowOrHideUploadButton?(false)
        }
        lastYContentOffset = scrollView.contentOffset.y
    }
}

//MARK: 展示创意图片的Cell
class StoreMainIdealCollectionViewCell: UICollectionViewCell{
    
    var model: IdealPhotoModel?{
        didSet{
            setData()
        }
    }
    
    /// 购买次数的label
    fileprivate var purchaseTimeLabel: BaseLabel?
    /// 作者的Label
    fileprivate var authorLabel: BaseLabel?
    /// 价格的Label
    fileprivate var priceLabel: BaseLabel?
    /// 图片
    fileprivate var imageView: BaseImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        purchaseTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "A8A8A8", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: nil)
        contentView.addSubview(purchaseTimeLabel!)
        purchaseTimeLabel?.snp.makeConstraints({ (make) in
            make.left.bottom.equalTo(contentView)
        })
        
        authorLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "A8A8A8", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: nil)
        contentView.addSubview(authorLabel!)
        authorLabel?.snp.makeConstraints({ (make) in
            make.leftMargin.equalTo(purchaseTimeLabel!)
            make.bottom.equalTo(purchaseTimeLabel!.snp.top).offset(-2/baseWidth)
        })
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill, andImageName: nil)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make) in
            make.left.top.right.equalTo(contentView)
            make.height.equalTo(207/baseWidth)
        })
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "FFCC4B", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: nil)
        imageView?.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make) in
            make.left.equalTo(imageView!).offset(12.5/baseWidth)
            make.bottom.equalTo(imageView!).offset(-7/baseWidth)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        purchaseTimeLabel?.text = NSLocalizedString("购买次数: ", comment: "") + "\(model!.orderTotal)"
        authorLabel?.text = NSLocalizedString("作者: ", comment: "") + "\(model!.nickName)"
        
        if let url: URL = model?.imgUrl{
            //先判断是否已缓存图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                if result == true{
                    self?.imageView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.model?.placeHolderURL{
                        self?.imageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.imageView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.imageView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.imageView?.sd_setImage(with: url)
                    }
                }
            })
        }

        if model?.moneyPrice == 0.0 {
            priceLabel?.text = NSLocalizedString("免费", comment: "")
        }else{
            priceLabel?.text = "¥  \(model!.moneyPrice)"
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
