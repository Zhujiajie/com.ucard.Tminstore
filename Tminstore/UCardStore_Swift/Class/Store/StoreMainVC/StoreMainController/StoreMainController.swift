//
//  StoreMainController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 记忆店铺的主页面
import UIKit
import SnapKit
import StoreKit

class StoreMainController: BaseViewController, SKStoreProductViewControllerDelegate {

    fileprivate let viewModel: StoreMainViewModel = StoreMainViewModel()
    
    /// 导航栏左按钮
    var leftBarButtonItem: BaseBarButtonItem?
    
    /// 点击导航栏左按钮的事件
    var touchLeftBarButtonItemClosure: (()->())?
    
    /// 点击视频按钮的事件
    var touchVideoButtonClosure: (()->())?
    /// 点击图片按钮的事件
    var touchImageButtonClosure: (()->())?
    
    /// tableView
    fileprivate var tableViewController: StoreMainTableViewController?
    
    /// 进入AR名片视频播放界面
    fileprivate var videoButton: BaseButton?
    /// 进入AR名片PPT展示界面
    fileprivate var imageButton: BaseButton?
    /// 开启摄像头按钮
    fileprivate var openCamera: BaseButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexString: "#F2F2F2")
        setNavigationBar()
        setUI()
        //setARButtons()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.init(name: LobsterTwo_Bold, size: 20)!] as [String: Any]
        setNavigationAttributeTitle("Tminstore", attributeDic: attDic)
        
        if viewModel.goodsArray.isEmpty{
            viewModel.requestGoodsInfo()//获取商品信息
        }
        viewModel.requestIdealOneTime()//获取创意图片
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置是否有未读消息
    func setMessageStatus(hasNewMessage: Bool){
        var imageName: String
        if hasNewMessage {
            imageName = "NewPerson"
        }else{
            imageName = "Person"
        }
        leftBarButtonItem?.image = UIImage(named: imageName)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        
        // 自定义导航栏按钮
        leftBarButtonItem = BaseBarButtonItem.showPersonItem()
        navigationItem.leftBarButtonItem = leftBarButtonItem
        
        setNavigationBarRightButtonWithImageName("enterAR")
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        tableViewController = StoreMainTableViewController()
        tableViewController?.viewModel = viewModel
        view.addSubview(tableViewController!.view)
        tableViewController?.view.snp.makeConstraints({ (make) in
            make.edges.equalTo(view)
        })
    }
    
    //MARK: 设置AR名片入口button
    fileprivate func setARButtons(){
        
        videoButton = BaseButton.normalTitleButton(title: "视频", andTextFont: UIFont.boldSystemFont(ofSize: 20), andTextColorHexString: "#2D2D2D", andBackgroundColorHexString: "#FFFFFF", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: nil)
        view.addSubview(videoButton!)
        videoButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(view).offset(50)
            make.left.equalTo(view).offset(0)
            make.size.equalTo(CGSize(width: 100, height: 100))
        })
        
        imageButton = BaseButton.normalTitleButton(title: "PPT", andTextFont: UIFont.boldSystemFont(ofSize: 20), andTextColorHexString: "#2D2D2D", andBackgroundColorHexString: "#FFFFFF", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: nil)
        view.addSubview(imageButton!)
        imageButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(view).offset(50)
            make.left.equalTo(view).offset(100)
            make.size.equalTo(CGSize(width: 100, height: 100))
        })
        
        openCamera = BaseButton.normalTitleButton(title: "摄像头", andTextFont: UIFont.boldSystemFont(ofSize: 20), andTextColorHexString: "#2D2D2D", andBackgroundColorHexString: "#FFFFFF", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: nil)
        view.addSubview(openCamera!)
        openCamera?.snp.makeConstraints({ (make) in
            make.top.equalTo(view).offset(50)
            make.left.equalTo(view).offset(200)
            make.size.equalTo(CGSize(width: 100, height: 100))
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击个人按钮的事件
        leftBarButtonItem?.touchItemClosure = { [weak self] _ in
            MobClick.event("enterPerson")
            self?.touchLeftBarButtonItemClosure?()
        }
        
//        //MARK: 点击上传按钮的事件
//        tableViewController?.touchUploadButtonClosure = {[weak self] _ in
//            let vc: StoreUploadController = StoreUploadController()
//            vc.umengViewName = "上传作品"
//            _ = self?.navigationController?.pushViewController(vc, animated: true)
//        }
        
        //MARK: 点击下载按钮的事件
        tableViewController?.touchDownLoadButtonClosure = {[weak self] _ in
            self?.openTimeoryView()
        }
        
        //MARK: 点击定制按钮
        tableViewController?.touchMakeButtonClosure = {[weak self] _ in
            self?.enterGoosListVC(idealModel: nil)
        }
        
        //MARK: 点击了实物Cell
        tableViewController?.selectGoodsCell = {[weak self] (indexPath: IndexPath) in
            self?.enterMakeVC(indexPath: indexPath)
        }
        
        //MARK: 点击了作品按钮
        tableViewController?.touchWorkButtonClosure = {[weak self] _ in
            let vc: StoreMainIdealController = StoreMainIdealController()
            vc.umengViewName = "作品列表"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 点击了创意图片Cell
        tableViewController?.selectIdealCell = { [weak self] (indexPath: IndexPath) in
            self?.enterIdelaDetailVC(indexPath: indexPath)
        }
        
        //MARK: 点击视频按钮
        videoButton?.touchButtonClosure = {[weak self] _ in
            self?.touchVideoButtonClosure?()
        }
        
        //MARK: 点击图片按钮
        imageButton?.touchButtonClosure = {[weak self] _ in
            self?.touchImageButtonClosure?()
        }
        
        //MARK: 点击开启摄像头按钮
        openCamera?.touchButtonClosure = {[weak self] _ in
            let vc: VideoCommentController = VideoCommentController()
            let nav: UINavigationController = UINavigationController(rootViewController: vc)
            self?.present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 点击导航栏右按钮事件
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        MobClick.event("enter_ar")
        enterARVC()
    }
    
    //MARK: 进入AR界面
    fileprivate func enterARVC(){
        // 请求摄像头权限
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { [weak self](result: Bool) in
            DispatchQueue.main.async {
                if result == true {
                    let arVC: LocalNewARController = LocalNewARController()
                    arVC.umengViewName = "AR"
                    let nav: UINavigationController = UINavigationController(rootViewController: arVC)
                    self?.present(nav, animated: true, completion: nil)
                    //                    let arVC: EasyARVC = EasyARVC()
                    //                    arVC.umengViewName = "AR"
                    //                    self?.present(arVC, animated: true, completion: nil)
                }else{
                    //引导用户去开启摄像头权限
                    self?.alert(alertTitle: NSLocalizedString("AR功能取得摄像头权限才能正常工作", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("就不给你权限", comment: ""), rightButtonTitle: NSLocalizedString("这就去设置", comment: ""), leftClosure: nil, rightClosure: {
                        let settingURL: URL = URL(string: UIApplicationOpenSettingsURLString)!
                        if UIApplication.shared.canOpenURL(settingURL){
                            UIApplication.shared.openURL(settingURL)
                        }
                    }, presentComplition: nil)
                }
            }
        }
    }
    
    //MARK: 进入实物制作界面
    fileprivate func enterMakeVC(indexPath: IndexPath){
        let vc: StoreMakeController = StoreMakeController()
        vc.goods = viewModel.goodsArray[indexPath.item]
        
        switch viewModel.goodsArray[indexPath.item].goodsType {
        case CustomizationType.postcard:
            vc.umengViewName = "AR明信片定制"
        case CustomizationType.pillow:
            vc.umengViewName = "AR抱枕定制"
        case CustomizationType.tShirt:
            vc.umengViewName = "AR文化衫定制"
        case CustomizationType.cup:
            vc.umengViewName = "AR马克杯定制"
        }
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 进入创意图片详情界面
    fileprivate func enterIdelaDetailVC(indexPath: IndexPath){
        let vc: StoreIdealDetailController = StoreIdealDetailController()
        vc.umengViewName = "创意图片详情"
        vc.idealModel = viewModel.idealModelArray[indexPath.item]
        present(vc, animated: true, completion: nil)
        
        //MARK: 获得了创意图片原图
        vc.getOriginalImage = {[weak self] (idealModel: IdealPhotoModel?) in
            self?.enterGoosListVC(idealModel: idealModel)
        }
    }
    
    //MARK: 进入视图列表
    fileprivate func enterGoosListVC(idealModel: IdealPhotoModel?){
        let vc: StoreGoodsController = StoreGoodsController()
        vc.umengViewName = "定制列表"
        vc.idealModel = idealModel
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 使用StoreKit打开Timeory页面
    fileprivate func openTimeoryView(){
//        if #available(iOS 10.3, *) {
//            SKStoreReviewController.requestReview()
//        }
        showSVProgress(title: nil)
        let vc: SKStoreProductViewController = SKStoreProductViewController()
        vc.delegate = self
        vc.loadProduct(withParameters: [SKStoreProductParameterITunesItemIdentifier: "1265724080"]) { [weak self] (result: Bool, error: Error?) in
            self?.dismissSVProgress()
            if error == nil {
                self?.present(vc, animated: true, completion: nil)
            }else{
                self?.handelNetWorkError(error, withResult: nil, loginSuccessClosure: nil)
            }
        }
    }
    
    func productViewControllerDidFinish(_ viewController: SKStoreProductViewController) {
        dismiss(animated: true, completion: nil)
    }
}
