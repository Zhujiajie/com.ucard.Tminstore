//
//  ActivityCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class ActivityCollectionViewCell: UICollectionViewCell {
    
    /// 点击退出按钮的事件
    var touchDismissButton: (()->())?
    /// 点击了确认按钮的事件
    var touchConfirmButton: ((_ addressModel: AddressModel?)->())?
    /// 地址模型
    fileprivate let addressModel: AddressModel = AddressModel()
    
    var indexPath: IndexPath?{
        didSet{
            if indexPath?.item == 0{
                setFirstCell()
            }else if indexPath?.item == 1{
                setSecondCell()
            }
        }
    }
    
    /// 确认的按钮
    fileprivate var confirmButton: UIButton?
    /// 退出的按钮
    fileprivate var dismissButton: UIButton?
    /// 按钮的文字格式
    fileprivate let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
    
    /// 输入姓名的textField
    fileprivate var nameTextField: ActivityTextField?
    /// 输入电话的textField
    fileprivate var phoneTextField: ActivityTextField?
    /// 输入详细地址的textView
    fileprivate var addressTextView: ActivityTextView?
    /// 输入邮编的textField
    fileprivate var mailCodeTextField: ActivityTextField?
    
    ///第一张图片的URL
    fileprivate let activityImage01URL: URL? = URL(string: upyunImageBase + "activityImage01.png!/format/webp")
    /// 第二张图片的URL
    fileprivate let activityImage02URL :URL? = URL(string: upyunImageBase + "activityImage02.png!/format/webp")
    /// 确认按钮的URL
    fileprivate let activityButton01: URL? = URL(string: upyunImageBase + "activityButton01.png!/format/webp")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        confirmButton = UIButton()
        confirmButton?.addTarget(self, action: #selector(touchConfirmButton(button:)), for: UIControlEvents.touchUpInside)
        confirmButton?.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        dismissButton = UIButton()
        dismissButton?.setImage(UIImage(named: "purchaseDismiss"), for: UIControlState.normal)
        dismissButton?.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        dismissButton?.addTarget(self, action: #selector(touchDismissButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 第一个Cell的布局
    fileprivate func setFirstCell(){
        
        let imageView: UIImageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(imageView)
        imageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(76/baseWidth)
            make.left.right.equalTo(contentView)
            make.height.equalTo(385/baseWidth)
        }
        imageView.sd_setImage(with: activityImage01URL)
        
        confirmButton?.sd_setBackgroundImage(with: activityButton01, for: UIControlState.normal)
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("点击领取", comment: ""), attributes: attDic)
        confirmButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        contentView.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(imageView.snp.bottom).offset(35/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 271/baseWidth, height: 47/baseWidth))
        })
        
        contentView.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(confirmButton!.snp.bottom).offset(48/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        })
    }
    
    //MARK: 第二个Cell的布局
    fileprivate func setSecondCell(){
        let containerView: UIView = UIView()
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 7
        containerView.layer.masksToBounds = true
        contentView.addSubview(containerView)
        containerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(103/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 327/baseWidth, height: 433/baseWidth))
        }
        
        nameTextField = ActivityTextField()
        nameTextField?.setPlaceHolder(placeHolder: NSLocalizedString("收件人姓名", comment: ""))
        containerView.addSubview(nameTextField!)
        nameTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(containerView).offset(64/baseWidth)
            make.centerX.equalTo(containerView)
            make.size.equalTo(CGSize(width: 267/baseWidth, height: 46/baseWidth))
        })
        
        phoneTextField = ActivityTextField()
        phoneTextField?.setPlaceHolder(placeHolder: NSLocalizedString("收件人电话", comment: ""))
        containerView.addSubview(phoneTextField!)
        phoneTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nameTextField!.snp.bottom).offset(14/baseWidth)
            make.centerX.equalTo(nameTextField!)
            make.size.equalTo(nameTextField!)
        })
        
        addressTextView = ActivityTextView()
        containerView.addSubview(addressTextView!)
        addressTextView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(phoneTextField!.snp.bottom).offset(14/baseWidth)
            make.centerX.equalTo(nameTextField!)
            make.size.equalTo(CGSize(width: 267/baseWidth, height: 86/baseWidth))
        })
        
        mailCodeTextField = ActivityTextField()
        mailCodeTextField?.keyboardType = UIKeyboardType.decimalPad
        mailCodeTextField?.setPlaceHolder(placeHolder: NSLocalizedString("邮编", comment: ""))
        containerView.addSubview(mailCodeTextField!)
        mailCodeTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(addressTextView!.snp.bottom).offset(14/baseWidth)
            make.centerX.equalTo(nameTextField!)
            make.size.equalTo(nameTextField!)
        })
        
        let attString: NSAttributedString = NSAttributedString(string: NSLocalizedString("提交", comment: ""), attributes: attDic)
        confirmButton?.setAttributedTitle(attString, for: UIControlState.normal)
        containerView.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(mailCodeTextField!.snp.bottom).offset(31/baseWidth)
            make.centerX.equalTo(nameTextField!)
            make.size.equalTo(CGSize(width: 281/baseWidth, height: 52/baseWidth))
        })
        
        enableOrDisenableConfirmButton(isEnanble: false)
        
        nameTextField?.getTextClosure = {[weak self] (text: String?) in
            self?.addressModel.mailName = text ?? ""
            self?.checkAddressModel()
        }
        
        phoneTextField?.getTextClosure = {[weak self] (text: String?) in
            self?.addressModel.phoneNumber = text ?? ""
            self?.checkAddressModel()
        }
        
        addressTextView?.getTextClosure = {[weak self] (text: String?) in
            self?.addressModel.detailedAddress = text ?? ""
            self?.checkAddressModel()
        }
        
        mailCodeTextField?.getTextClosure = {[weak self] (text: String?) in
            self?.addressModel.postcode = text ?? ""
            self?.checkAddressModel()
        }
        
        contentView.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(confirmButton!.snp.bottom).offset(59/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        })

        let imageView: UIImageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(imageView)
        imageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(31/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 171/baseWidth, height: 122/baseWidth))
        }
        imageView.sd_setImage(with: activityImage02URL)
    }
    
    //MARK: 点击确认按钮的事件
    func touchConfirmButton(button: UIButton){
        if indexPath?.item == 0{
            touchConfirmButton?(nil)
        }else{
            touchConfirmButton?(addressModel)
        }
    }
    
    //MARK: 点击退出按钮的事件
    func touchDismissButton(button: UIButton){
        touchDismissButton?()
    }
    
    //MARK: 启动或者停用confirmButton
    fileprivate func enableOrDisenableConfirmButton(isEnanble: Bool){
        if isEnanble{
            confirmButton?.backgroundColor = UIColor(hexString: "4CDFED")
            confirmButton?.titleLabel?.alpha = 1.0
        }else{
            confirmButton?.backgroundColor = UIColor(hexString: "#A6EFF6")
            confirmButton?.titleLabel?.alpha = 0.5
        }
        confirmButton?.isEnabled = isEnanble
    }
    
    //MARK: 检车地址信息是否填写完成
    fileprivate func checkAddressModel(){
        if addressModel.mailName == "" || addressModel.phoneNumber == "" || addressModel.detailedAddress == ""{
            enableOrDisenableConfirmButton(isEnanble: false)
        }else{
            enableOrDisenableConfirmButton(isEnanble: true)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
