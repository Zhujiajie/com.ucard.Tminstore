//
//  ActivityCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ActivityCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = UIScreen.main.bounds.size
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        
        backgroundColor = UIColor.clear
        register(ActivityCollectionViewCell.self, forCellWithReuseIdentifier: "0")
        register(ActivityCollectionViewCell.self, forCellWithReuseIdentifier: "1")
        isScrollEnabled = false
        showsVerticalScrollIndicator = false
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.57)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
