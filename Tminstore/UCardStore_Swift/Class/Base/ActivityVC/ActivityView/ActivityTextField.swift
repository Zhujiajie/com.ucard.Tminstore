//
//  ActivityTextField.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ActivityTextField: UITextField, UITextFieldDelegate {

    /// 成功获得文字的闭包
    var getTextClosure: ((_ text: String?)->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layer.borderColor = UIColor(hexString: "C2C2C2").cgColor
        layer.borderWidth = 0.5
        layer.cornerRadius = 3
        layer.masksToBounds = true
        
        font = UIFont.systemFont(ofSize: 12)
        
        delegate = self
        
        let leftView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 10))
        leftView.backgroundColor = UIColor.clear
        self.leftView = leftView
        leftViewMode = UITextFieldViewMode.always
    }
    
    //MARK: 设置占位符
    /// 设置占位符
    ///
    /// - Parameter placeHolder: 占位符
    func setPlaceHolder(placeHolder: String){
        self.placeholder = placeHolder
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        getTextClosure?(textField.text)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
