//
//  ActivityTextView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ActivityTextView: FSTextView, UITextViewDelegate {

    /// 成功获得文字的闭包
    var getTextClosure: ((_ text: String?)->())?
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        layer.borderColor = UIColor(hexString: "C2C2C2").cgColor
        layer.borderWidth = 0.5
        layer.cornerRadius = 3
        layer.masksToBounds = true
        
        font = UIFont.systemFont(ofSize: 12)
        
        delegate = self
        
        placeholder = NSLocalizedString("详细地址", comment: "")
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        getTextClosure?(textView.text)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
