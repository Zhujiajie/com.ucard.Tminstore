//
//  ActivityVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ActivityVC: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    fileprivate let viewModel: ActivityViewModel = ActivityViewModel()
    
    /// 退出的闭包
    var dismissClosure: (()->())?
    
    override var umengViewName: String{
        set{
            self.umengViewName = newValue
        }
        get{
            return "周庄线上活动"
        }
    }
    
    var backImage: UIImage?
    
    fileprivate var collectionView: ActivityCollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let backImageView: UIImageView = UIImageView()
        view.addSubview(backImageView)
        backImageView.contentMode = UIViewContentMode.scaleToFill
        backImageView.image = backImage
        backImageView.frame = view.bounds
        
        collectionView = ActivityCollectionView()
        view.addSubview(collectionView!)
        collectionView?.frame = view.bounds
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        viewModel.getGiftID()
        
        closures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 获取礼品ID出错
        viewModel.getGiftIdFail = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
            self?.dismissSelf(animated: true, completion: {
                self?.dismissClosure?()
            })
        }
        
        //MARK: 提交信息成功
        viewModel.uploadInfoSuccess = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("提交成功", comment: ""), leftClosure: { 
                self?.dismissSelf(animated: true, completion: {
                    self?.dismissClosure?()
                })
            })
        }
        
        //MARK: 提交信息失败
        viewModel.uploadFailed = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0{
            let cell: ActivityCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "0", for: indexPath) as! ActivityCollectionViewCell
            cell.indexPath = indexPath
            //点击 点击领取 按钮，滚动到第二页
            cell.touchConfirmButton = { (_) in
                collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            }
            //点击退出按钮
            cell.touchDismissButton = {[weak self] _ in
                self?.dismissSelf(animated: true, completion: {
                    self?.dismissClosure?()
                })
            }
            return cell
        }else{
            let cell: ActivityCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "1", for: indexPath) as! ActivityCollectionViewCell
            cell.indexPath = indexPath
            //点击提交按钮，提交信息
            cell.touchConfirmButton = { [weak self] (addressModel: AddressModel?) in
                self?.viewModel.uploadInfo(addressModel: addressModel)
            }
            //点击退出按钮
            cell.touchDismissButton = {[weak self] _ in
                self?.dismissSelf(animated: true, completion: {
                    self?.dismissClosure?()
                })
            }
            return cell
        }
    }
}
