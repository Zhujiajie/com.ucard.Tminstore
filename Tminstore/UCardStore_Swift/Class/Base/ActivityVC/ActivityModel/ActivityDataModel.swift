//
//  ActivityDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ActivityDataModel: BaseDataModel {
    
    //MARK: 获取礼品优惠券的接口
    /// 获取礼品优惠券的接口
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    func getActivityGiftCoupon(success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?, andCompletion complete: (()->())? = nil){
        
        let urlString: String = appAPIHelp.getGiftCouponAPI + "1"
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            complete?()
        })
    }
    
    //MARK: 线上活动，提交信息
    func uploadInfo(addressModel: AddressModel, andGiftId giftId: String, andSuccess success: ((_ result: [String: Any])->())?, andFail fail: ((_ error: Error?)->())?, andCompletion complete: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "giftId": giftId,
            "userName": addressModel.mailName,
            "phone": addressModel.phoneNumber,
            "address": addressModel.detailedAddress,
            "postCode": addressModel.postcode
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.activityUploadInfoAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            fail?(error)
        }, completionClosure: {
            complete?()
        })
    }
}
