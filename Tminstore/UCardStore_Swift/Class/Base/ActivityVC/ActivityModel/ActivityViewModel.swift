//
//  ActivityViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ActivityViewModel: BaseViewModel {

    fileprivate let dataModel: ActivityDataModel = ActivityDataModel()
    
    /// 成功获得礼品券ID
    var getActivityCouponSuccess: (()->())?
    /// 获取礼品券ID失败
    var getGiftIdFail: ((_ error: Error?, _ result: [String: Any]?)->())?
    /// 提交信息成功
    var uploadInfoSuccess: (()->())?
    /// 提交信息失败
    var uploadFailed: ((_ error: Error?, _ result: [String: Any]?)->())?
    /// 礼品券ID
    fileprivate var giftId: String?
    
    //MARK: 获取giftID
    /// 获取giftID
    func getGiftID(){
        showSVProgress(title: nil)
        
        dataModel.getActivityGiftCoupon(success: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if let dic: [String: Any] = list.first{
                            if let giftId: String = dic["giftId"] as? String{
                                self?.giftId = giftId
                                return
                            }
                        }
                    }
                }
            }
            self?.getGiftIdFail?(nil, result)
        }, failureClosure: { [weak self] (error: Error?) in
            self?.getGiftIdFail?(error, nil)
            }, andCompletion: {[weak self] _ in
              self?.dismissSVProgress()
        })
    }
    
    //MARK: 上传用户填的活动信息
    /// 上传用户填的活动信息
    ///
    /// - Parameters:
    ///   - addressModel: 地址信息
    func uploadInfo(addressModel: AddressModel?){
        if giftId == nil || addressModel == nil{
            uploadFailed?(nil, nil)
            return
        }
        showSVProgress(title: nil)
        dataModel.uploadInfo(addressModel: addressModel!, andGiftId: giftId!, andSuccess: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.uploadInfoSuccess?()
                return
            }
            self?.uploadFailed?(nil, result)
            }, andFail: { [weak self] (error: Error?) in
            self?.uploadFailed?(error, nil)
            }, andCompletion: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
}
