//
//  BaseAnimationModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/30.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class BaseAnimationModel: NSObject {
    
    //MARK: autoLayoutAnimaton
    /**
     自动约束的动画，默认设置了弹簧效果
     
     - parameter view: view
     */
    func autoLayoutAnimation(view: UIView, withDelay delay: Double) {
        
        view.setNeedsUpdateConstraints()//约束需要更新
        view.updateConstraintsIfNeeded()//检测需要更新的约束
        
        UIView.animate(withDuration: 0.5, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: .curveEaseOut, animations: {
            view.layoutIfNeeded()//强制更新约束
            }, completion: nil)
    }
}
