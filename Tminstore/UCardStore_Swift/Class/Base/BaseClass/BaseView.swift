//
//  BaseView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseView: UIView {
    
    ///点击事件
    var tapClosure: (()->())?
    
    //MARK: 设置点击事件
    /// 设置点击事件
    func setTapGesture() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapView(tap:)))
        self.addGestureRecognizer(tap)
    }
    
    //MARK: 带颜色的线条
    /// 带颜色的线条
    ///
    /// - Parameter colorString: 颜色的HEX值
    /// - Returns: BaseView
    class func colorLine(colorString: String)->BaseView{
        let view: BaseView = BaseView()
        view.backgroundColor = UIColor(hexString: colorString)
        return view
    }
    
    //MARK: 带阴影的视图
    /// 带阴影的视图，白色背景
    ///
    /// - Returns: BaseView
    class func shadowView()->BaseView{
        let view: BaseView = BaseView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 4
        return view
    }
    
    //MARK: 半透明的遮罩视图
    /// 半透明的遮罩视图
    ///
    /// - Parameter alpha: 透明度
    /// - Returns: BaseView
    class func maskView(alpha: CGFloat)->BaseView{
        let view: BaseView = BaseView()
        view.backgroundColor = UIColor(white: 0.0, alpha: alpha)
        return view
    }
    
    //MARK: 获取当前页面的截图
    /// 获取当前页面的截图
    ///
    /// - Returns: UIImage?
    func getBitmapImage()->UIImage?{
        //绘制背景
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0);
        //背景
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    //MARK: 点击事件
    func tapView(tap: UITapGestureRecognizer){
        tapClosure?()
    }
}
