//
//  BaseVisualEffectView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseVisualEffectView: UIVisualEffectView {

    /// 点击事件
    var tapClosure: (()->())?
    
    override init(effect: UIVisualEffect?) {
        super.init(effect: effect)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapVisualView(tap:)))
        addGestureRecognizer(tap)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: 没有任何效果的高斯模糊视图，初始化之后，用动画来添加模糊效果
    /// 没有任何效果的高斯模糊视图，初始化之后，用动画来添加模糊效果
    ///
    /// - Returns: BaseVisualEffectView
    class func noEffectVisualView()->BaseVisualEffectView{
        let visualView: BaseVisualEffectView = BaseVisualEffectView(effect: nil)
        return visualView
    }
    
    //MARK: 白色的高斯模糊
    /// 白色的高斯模糊
    ///
    /// - Returns: BaseVisualEffectView
    class func lightVisualEffectView()->BaseVisualEffectView{
        let effect: UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        let visualView: BaseVisualEffectView = BaseVisualEffectView(effect: effect)
        return visualView
    }

    //MARK: 出现白色模糊的动画
    /// 出现白色模糊的动画
    func appearLightEffectAnimation() {
        if #available(iOS 10.0, *) {
            let animator: UIViewPropertyAnimator = UIViewPropertyAnimator(duration: 0.5, curve: UIViewAnimationCurve.linear, animations: {[weak self] _ in
                self?.effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            })
            animator.startAnimation()
        } else {
            self.effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            self.alpha = 0
            UIView.animate(withDuration: 0.5, animations: { [weak self] _ in
                self?.alpha = 1
            })
        }
    }
    
    //MARK: 消失的动画
    fileprivate func dismissAnimation(){
        if #available(iOS 10.0, *) {
            let animator: UIViewPropertyAnimator = UIViewPropertyAnimator(duration: 0.5, curve: UIViewAnimationCurve.linear, animations: {[weak self] _ in
                self?.effect = nil
            })
            animator.startAnimation()
            animator.addCompletion({ [weak self] (position: UIViewAnimatingPosition) in
                if position == UIViewAnimatingPosition.end{
                    self?.removeFromSuperview()
                    self = nil
                }
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: { [weak self] _ in
                self?.alpha = 0
            }, completion: { [weak self] (_) in
                self?.removeFromSuperview()
                self = nil
            })
        }
    }
    
    //MARK: 点击事件
    func tapVisualView(tap: UITapGestureRecognizer){
        tap.isEnabled = false//防止重复点击
        dismissAnimation()
        tapClosure?()
    }
}
