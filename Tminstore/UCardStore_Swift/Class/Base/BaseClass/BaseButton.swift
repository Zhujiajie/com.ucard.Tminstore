//
//  BaseButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseButton: UIButton {

    /// 点击按钮的事件
    var touchButtonClosure: (()->())?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 普通带icon的按钮
    /// 普通带icon的按钮
    ///
    /// - Parameter iconName: icon名字
    /// - Returns: BaseButton
    class func normalIconButton(iconName: String? = nil)->BaseButton{
        let button: BaseButton = BaseButton()
        if iconName != nil{
            button.setImage(UIImage(named: iconName!), for: UIControlState.normal)
        }
        return button
    }
    
    //MARK: 普通文字按钮
    /// 普通文字按钮
    ///
    /// - Parameters:
    ///   - title: 标题
    ///   - font: 字体
    ///   - textColorHexString: 文字颜色
    ///   - backgroundColorHexString: 背景颜色，默认为空
    ///   - borderColorHexString: 边框颜色，默认为空
    ///   - borderWidth: 边框宽度，默认为空
    ///   - cornerRadius: 圆角，默认为空
    /// - Returns: BaseButton
    class func normalTitleButton(title: String, andTextFont font: UIFont, andTextColorHexString textColorHexString: String, andBackgroundColorHexString backgroundColorHexString: String? = nil, andBorderColorHexString borderColorHexString: String? = nil, andBorderWidth borderWidth: CGFloat? = nil, andCornerRadius cornerRadius: CGFloat? = nil)->BaseButton{
        
        let button: BaseButton = BaseButton()
        
        let attDic: [String: Any] = [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor(hexString: textColorHexString)]
        let attStr: NSAttributedString = NSAttributedString(string: title, attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        
        if backgroundColorHexString != nil{
            button.backgroundColor = UIColor(hexString: backgroundColorHexString!)
        }
        if borderColorHexString != nil{
            button.layer.borderColor = UIColor(hexString: borderColorHexString!).cgColor
        }
        if borderWidth != nil{
            button.layer.borderWidth = borderWidth!
        }
        if cornerRadius != nil{
            button.layer.cornerRadius = cornerRadius!
        }
        button.layer.masksToBounds = true
        
        return button
    }
    
    //MARK: 设置图片居左，文字局右的按钮
    /// 设置图片居左，文字局右的按钮
    ///
    /// - Parameters:
    ///   - iconName: icon 的名字
    ///   - font: 字体
    ///   - colorHexString: 文字颜色
    ///   - title: 标题
    /// - Returns: BaseButton
    class func leftImageAndRightTitleButton(iconName: String? = nil, andFont font: UIFont, andTextColorHexString colorHexString: String, andTitle title: String)->BaseButton{
        let button: BaseButton = BaseButton()
        
        //设置icon
        if iconName != nil{
            button.setImage(UIImage(named: iconName!), for: UIControlState.normal)
        }
        button.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        //设置标题
        button.setTitle(font: font, andTextColorHexString: colorHexString, andTitle: title)
        
        //设置图片居左，文字局右
        button.setImageLeftAndTitleRight()
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        return button
    }
    
    //MARK: 标题在左边，图片在右边的按钮
    /// 标题在左边，图片在右边的按钮
    ///
    /// - Parameters:
    ///   - iconName: icon 的名字
    ///   - font: 字体
    ///   - colorHexString: 文字颜色
    ///   - title: 标题
    /// - Returns: BaseButton
    class func leftTitleRightImageButton(iconName: String? = nil, andFont font: UIFont, andTextColorHexString colorHexString: String, andTitle title: String)->BaseButton{
        
        let button: BaseButton = BaseButton()
        
        //设置icon
        if iconName != nil{
            button.setImage(UIImage(named: iconName!), for: UIControlState.normal)
        }
        button.imageView?.contentMode = UIViewContentMode.scaleAspectFit
        
        //设置标题
        button.setTitle(font: font, andTextColorHexString: colorHexString, andTitle: title)
        
        //设置图片居左，文字局右
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right//内容居右
//        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40)
        return button
    }
    
    //MARK: 设置标题
    /// 设置标题
    ///
    /// - Parameters:
    ///   - font: 字体
    ///   - colorHexString: 文字颜色
    ///   - title: 标题
    func setTitle(font: UIFont, andTextColorHexString colorHexString: String, andTitle title: String){
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: colorHexString), NSFontAttributeName: font]
        let attString: NSAttributedString = NSAttributedString(string: title, attributes: attDic)
        self.setAttributedTitle(attString, for: UIControlState.normal)
    }
    
    //MARK: 设置图片左移，标题右移
    /// 设置图片左移，标题右移
    func setImageLeftAndTitleRight(){
        contentHorizontalAlignment = UIControlContentHorizontalAlignment.left//内容居左
//        imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)//图片向左偏移
//        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)//标题向右偏移
    }
    
    //MARK: 点击按钮
    func touchButton(button: UIButton){
        touchButtonClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
