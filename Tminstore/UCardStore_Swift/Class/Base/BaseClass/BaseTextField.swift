//
//  BaseTextField.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseTextField: UITextField, UITextFieldDelegate {
    
    /// 停止编辑
    var didEndEditing: ((_ text: String)->())?
    
    //MARK: 只能输入数字的textField
    /// 只能输入数字的textField
    ///
    /// - Parameter font: 字体
    /// - Returns: BaseTextField
    class func numberTextField(font: UIFont, andTextColorHexString textColorHexString: String? = nil, andPlaceHolder placeHolder: String? = nil)->BaseTextField{
        
        let textField: BaseTextField = BaseTextField()
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.autocapitalizationType = UITextAutocapitalizationType.none
        textField.spellCheckingType = UITextSpellCheckingType.no
        textField.keyboardAppearance = UIKeyboardAppearance.dark
        textField.keyboardType = UIKeyboardType.numberPad
        textField.font = font
        if textColorHexString != nil {
            textField.textColor = UIColor(hexString: textColorHexString!)
        }
        textField.textAlignment = NSTextAlignment.center
        if placeHolder != nil{
            textField.placeholder = placeHolder
        }else{
            textField.placeholder = NSLocalizedString("请输入密码查看AR视频", comment: "")
        }
        
        textField.delegate = textField
        return textField
    }
    
    //MARK: 输入密码的textField
    /// 输入密码的textField
    ///
    /// - Parameters:
    ///   - font: 字体
    ///   - textColorHexString: 字体颜色
    ///   - placeHolder: 占位符
    /// - Returns: BaseTextField
    class func passwordTextField(font: UIFont, andTextColorHexString textColorHexString: String? = nil, andPlaceHolder placeHolder: String? = nil)->BaseTextField{
        
        let textField: BaseTextField = BaseTextField()
        textField.isSecureTextEntry = true
        textField.font = font
        if textColorHexString != nil {
            textField.textColor = UIColor(hexString: textColorHexString!)
        }
        textField.textAlignment = NSTextAlignment.center
        if placeHolder != nil{
            textField.placeholder = placeHolder
        }
        textField.delegate = textField
        return textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text == nil || textField.text == "" {return}
        didEndEditing?(textField.text!)
    }
}
