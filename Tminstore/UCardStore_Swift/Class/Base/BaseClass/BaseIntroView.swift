//
//  BaseIntroView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseIntroView: UIView {

    var tap: UITapGestureRecognizer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        tap = UITapGestureRecognizer(target: self, action: #selector(tapToDismiss(tap:)))
        addGestureRecognizer(tap!)
    }
    
    func tapToDismiss(tap: UITapGestureRecognizer){
        dismissAnimation()
    }
    
    //MARK: 消失的动画
    func dismissAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 0
            }, completion: {[weak self] (_) in
                self?.removeFromSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
