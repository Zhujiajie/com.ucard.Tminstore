//
//  BaseViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/2.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import ChameleonFramework
import SnapKit

let navigationBarTintColor = UIColor(hexString: "68cfd6")

class BaseViewController: UIViewController {
    
    /// 友盟页面统计
    open var umengViewName: String = "其他"
    /// 动画
    fileprivate let transition: PopAnimator = PopAnimator()

    /// 当前controller的index
    var controllerIndex: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        navigationController?.interactivePopGestureRecognizer?.delegate = nil//开启手势返回
        navigationController?.navigationBar.barTintColor = mainNavigationBarTintColor()
        navigationController?.navigationBar.isTranslucent = false
//        navigationController?.navigationBar.tintColor = appThemeColor()
        navigationController?.navigationBar.tintColor = UIColor(hexString: "4D4D4D")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView(umengViewName)//友盟页面统计
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView(umengViewName)//友盟页面统计
    }
    
//MARK:------------------------------设置导航栏------------------------------------
    
    //MARK: 设置导航栏标题
    /**
     设置导航栏的标题，默认白色
     
     - parameter title: 标题
     */
    func setNavigationTitle(_ title: String) {
        navigationItem.title = title
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: appThemeColor()] as [String: Any]
    }
    
    //MARK:设置导航栏的attributeTitle
    /**
     设置导航栏的attributeTitle
     
     - parameter title:        标题
     - parameter attributeDic: attribute
     */
    func setNavigationAttributeTitle(_ title: String, attributeDic: [String : Any]) {
        navigationItem.title = title
        navigationController?.navigationBar.titleTextAttributes = attributeDic
    }
    
    //MARK:根据图片设置导航栏左按钮
    /**
     根据图片设置导航栏左按钮
     - parameter imageName: 图片名字
     */
    func setNavigationBarLeftButtonWithImageName(_ imageName: String) {
        let button: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: imageName)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: UIBarButtonItemStyle.plain, target: self, action: #selector(navigationBarLeftBarButtonAction(_:)))
        navigationItem.leftBarButtonItem = button
    }
    
    //MARK: 从文字设置导航栏左按钮
    /**
     从文字设置导航栏左按钮
     - parameter title: title
     */
    func setNavigationBarLeftButtonWithTitle(_ title: String) {
        let button: UIBarButtonItem = UIBarButtonItem(title: title, style: UIBarButtonItemStyle.plain, target: self, action: #selector(navigationBarLeftBarButtonAction(_:)))
        navigationItem.leftBarButtonItem = button
    }
    
    //MARK: 导航栏左按钮点击事件
    /**
     导航栏左按钮点击事件，默认返回上一页
     - parameter button:
     */
    func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem){
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK:从图片设置导航栏右按钮
    /**
     根据图片设置导航栏右按钮
     - parameter imageName: 图片名字
     */
    func setNavigationBarRightButtonWithImageName(_ imageName: String) {
        let button: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: imageName)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), style: UIBarButtonItemStyle.plain, target: self, action: #selector(navigationBarRightBarButtonAction(_:)))
        navigationItem.rightBarButtonItem = button
    }
    
    //MARK: 从文字设置导航栏右按钮
    /**
     从文字设置导航栏右按钮
     - parameter title: title
     */
    func setNavigationBarRightButtonWithTitle(_ title: String) {
        let button: UIBarButtonItem = UIBarButtonItem(title: title, style: UIBarButtonItemStyle.plain, target: self, action: #selector(navigationBarRightBarButtonAction(_:)))
        navigationItem.rightBarButtonItem = button
    }
    
    //MARK: 导航栏右按钮点击事件
    /**
     导航栏右按钮点击事件
     - parameter button:
     */
    func navigationBarRightBarButtonAction(_ button: UIBarButtonItem){
        
    }
    
    //MARK: 导航栏透明
    /// 设置导航栏为透明
    func transparentNavigationBar(){
        self.edgesForExtendedLayout = []
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        self.navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    //MARK: 取消导航栏透明
    func cancelTansparentNavigationBar(){
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    //MARK: 获取当前页面的截图
    func getScreenImage()->UIImage?{
        //绘制背景
        let scale = CGFloat(1748.0 / 296.0 * 1.0)
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale);
        //背景
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

    
//MARK:------------------------创建提示框-----------------------------
    //MARK: 创建提示框
    /**
     创建提示框
     
     - parameter title:      标题
     - parameter message:    信息
     - parameter leftTitle:  左按钮标题 默认 字符串 “OK” or “好的”
     - parameter rightTitle: 右按钮标题
     - parameter left:       左按钮点击事件
     - parameter right:      右按钮点击事件
     - parameter complition: 提示框弹出完成之后的闭包
     */
    func alert(alertTitle title: String?, messageString message: String? = nil, leftButtonTitle leftTitle: String = NSLocalizedString("好的", comment: ""), rightButtonTitle rightTitle: String? = nil, leftClosure left: (()->())? = nil, rightClosure right: (()->())? = nil, presentComplition complition: (()->())? = nil){
        
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let leftAc: UIAlertAction = UIAlertAction(title: leftTitle, style: .cancel) { (_) in
            left?()
        }// 左按钮
        ac.addAction(leftAc)
        
        if (rightTitle != nil) {
            let rightAc: UIAlertAction = UIAlertAction(title: rightTitle, style: .default, handler: { (_) in
                right?()
            })//右按钮
            ac.addAction(rightAc)
        }
        
        present(ac, animated: true) {
            complition?()
        }//提示框弹出结束后的事件
    }
    
    //MARK: 创建删除提示框
    /**
     创建删除提示框
     
     - parameter title:      标题
     - parameter message:    信息
     - parameter leftTitle:  左按钮标题
     - parameter rightTitle: 右按钮标题
     - parameter left:       左按钮点击事件
     - parameter right:      右按钮点击事件
     - parameter complition: 提示框弹出完成之后的闭包
     */
    func deleteAlert(alertTitle title: String? = nil, messageString message: String? = nil, leftButtonTitle leftTitle: String = NSLocalizedString("取消", comment: ""), rightButtonTitle rightTitle: String = NSLocalizedString("删除", comment: ""), leftClosure left: (()->())? = nil, rightClosure right: (()->())?, presentCompletion completion: (()->())? = nil){
        
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let leftAc: UIAlertAction = UIAlertAction(title: leftTitle, style: .cancel) { (_) in
            left?()
        }// 左按钮
        ac.addAction(leftAc)
        
        let rightAc: UIAlertAction = UIAlertAction(title: rightTitle, style: .destructive, handler: { (_) in
            right?()
        })//删除按钮
        ac.addAction(rightAc)
        
        present(ac, animated: true) {
            completion?()
        }//提示框弹出结束后的事件
    }
    
//MARK:----------------设置SVProgressHUD--------------------
    
    //MARK: 成功的弹窗
    /// 成功的弹窗
    func successSVProgress(title: String?){
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.showSuccess(withStatus: title)
        delay(seconds: 1.0, completion: { [weak self] _ in
            self?.dismissSVProgress()
        })
    }
    
    //MARK: 展示加载器
    /**
     展示加载器
     
     - parameter title:    加载器上的标题
     - parameter maskType: mask类型，默认为.Clear
     */
    func showSVProgress(title: String?, andMaskType maskType :SVProgressHUDMaskType = .clear){
        if title == nil {
            SVProgressHUD.show()
        }else{
            SVProgressHUD.show(withStatus: NSLocalizedString(title!, comment: ""))
        }
        SVProgressHUD.setDefaultMaskType(maskType)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
    }
    
    //MARK: 进度条
    /// 进度条
    ///
    /// - Parameters:
    ///   - receiveSize: 已接受的数据
    ///   - targetSize: 需要接受的数据
    ///   - title: 标题。默认为空
    func setSVProgress(receiveSize: Int, andTargetSize targetSize: Int, andTitle title: String? = nil){
        SVProgressHUD.showProgress(Float(receiveSize / targetSize), status: title)
    }
    
    //MARK: 隐藏加载指示器
    /**
     隐藏加载指示器
     */
    func dismissSVProgress(){
        SVProgressHUD.dismiss()
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.none)
    }
    
    
    //MARK: 退出
    /// dismissViewController
    ///
    /// - parameter animated:          是否需要动画，默认true
    /// - parameter completionClosure: dismiss完成后的闭包
    func dismissSelf(animated: Bool = true, completion completionClosure: (()->())? = nil){
        let rootVC: UIViewController? = self.presentingViewController
        rootVC?.dismiss(animated: animated, completion: nil)
        completionClosure?()
    }
}

//extension BaseViewController: UIViewControllerTransitioningDelegate{
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        
//        if let vc: MainCommitVC = presented as? MainCommitVC{
//            transition.originFrame = vc.cellFrame//获取cell当前的frame
//            return transition
//        }else{
//            //对要展示的视图增加手势
//            interactive.addGesture(presented)
//            animator.presenting = true
//            return animator
//        }
//    }
//    
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        if dismissed.isKind(of: MainCommitVC.self) == true{
//            return nil
//        }else{
//            animator.presenting = false
//            return animator
//        }
//    }
//    
//    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
//        if animator.isKind(of: PushAnimation.self) == true{
//            return interactive.isInteracting ? interactive : nil
//        }else{
//            return nil
//        }
//    }
//}

////MARK: 动画
//extension BaseViewController{
//    
//    //MARK: autoLayoutAnimaton
//    /**
//     自动约束的动画，默认设置了弹簧效果
//     
//     - parameter view: view
//     */
//    func autoLayoutAnimation(view: UIView, withDelay delay: Double, completionClosure completion: (()->())? = nil, andAnimationTime time: TimeInterval = 0.5) {
//        
//        view.setNeedsUpdateConstraints()//约束需要更新
//        view.updateConstraintsIfNeeded()//检测需要更新的约束
//        
//        UIView.animate(withDuration: time, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.curveEaseOut, animations: {
//            view.layoutIfNeeded()//强制更新约束
//        }, completion: {(_) in
//            completion?()
//        })
//    }
//}

//MARK: 网络相关
extension BaseViewController{
    //MARK:------------处理网络请求失败-----------------
    /**
     处理网络请求失败，token过期则让用户重新登陆。其他错误则弹窗
     
     - parameter error:        alamofire返回的错误
     - parameter loginSuccess: 重新登录成功的闭包
     */
    func handelNetWorkError(_ error: Error? = nil, withResult result: [String: Any]? = nil, loginSuccessClosure loginSuccess: (()->())? = nil){
        
        dismissSVProgress()
        
        //提醒用户出错
        if #available(iOS 10.0, *) {
            let notificationGenerator: UINotificationFeedbackGenerator = UINotificationFeedbackGenerator()
            notificationGenerator.prepare()
            notificationGenerator.notificationOccurred(UINotificationFeedbackType.error)
        }
        
        // token过期，让用户重新登录
        if let code: Int = result?["code"] as? Int, code == 1003{
            enterLoginView()
            return
        }
        
        if let message: String = result?["message"] as? String{// 打印Java后台返回的错误
            alert(alertTitle: message)
            return
        }else{
            //网络错误
            if error != nil{
                alert(alertTitle: error?.localizedDescription)
            }else{
                alert(alertTitle: NSLocalizedString("未知错误", comment: ""))
            }
        }
    }
    
    //MARK: 进入登录注册界面
    /// 进入登录注册界面
    func enterLoginView(loginSuccessClosure: (()->())? = nil){
        
        let loginVC: PLoginViewController = PLoginViewController()
        loginVC.shouldUseDismiss = true
        
        //MARK: 登录成功的闭包
        loginVC.loginSuccessClosure = {
            loginSuccessClosure?()
        }
        
        present(loginVC, animated: true, completion: nil)
        MobClick.profileSignOff()//用户退出登录，清空友盟的账号信息
        saveUserToken(token: "")//清空Token
        saveUserID(userId: "")//清空用户的id
        saveUserName(name: nil)//清空用户昵称
        setLoginStatus(loginStatus: false)//设置app为未登陆状态
        setUserHasLoadAPP(false)//设置用户未登陆过app
    }
}
