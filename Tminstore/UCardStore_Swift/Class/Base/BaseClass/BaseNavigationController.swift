//
//  BaseNavigationController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    //MARK: 获取当前页面的截图
    /// 获取当前页面的截图
    ///
    /// - Returns: UIImage?
    func getScreenImage()->UIImage?{
        //绘制背景
        let scale = CGFloat(1748.0 / 296.0 * 1.0)
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale);
        //背景
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }

}
