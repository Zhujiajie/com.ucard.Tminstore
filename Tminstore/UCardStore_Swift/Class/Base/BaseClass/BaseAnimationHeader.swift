//
//  BaseAnimationHeader.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: ----------自定义的下拉刷新动画------------
import UIKit
import MJRefresh

class BaseAnimationHeader: MJRefreshGifHeader {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        var refreshingImages: [UIImage]         = [UIImage]()
        
        //添加图片
        for i in 0..<40 {
            var refreshingImageName: String = ""
            if i < 10 {
                refreshingImageName = "Rereshing0\(i)"
            }else{
                refreshingImageName = "Rereshing\(i)"
            }
            let tempImage: UIImage = UIImage(named: refreshingImageName)!
            let scaledImage: UIImage = UIImage(cgImage: tempImage.cgImage!, scale: 3.0, orientation: UIImageOrientation.up)//缩放图片
            refreshingImages.append(scaledImage)
        }
        setImages(refreshingImages, for: MJRefreshState.idle)//初始状态
        setImages(refreshingImages, duration: 1.0, for: MJRefreshState.pulling)//下拉过程的动画
        setImages(refreshingImages, duration: 1.0, for: MJRefreshState.refreshing)//刷新时的动画
        lastUpdatedTimeLabel.isHidden = true
        stateLabel.isHidden = true
    }
    
    
    override func beginRefreshing() {
        super.beginRefreshing()
        //下拉时，轻微震动
        if #available(iOS 10.0, *) {
            let feedbackGenerator: UIImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: UIImpactFeedbackStyle.light)
            feedbackGenerator.prepare()
            feedbackGenerator.impactOccurred()
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
