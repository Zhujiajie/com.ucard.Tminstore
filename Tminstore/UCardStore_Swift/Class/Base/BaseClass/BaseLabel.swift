//
//  BaseLabel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {

    //MARK: 通用的UILabel
    /// 通用的UILabel
    ///
    /// - Parameters:
    ///   - font: 字体
    ///   - textColorHex: 字体颜色的16进制文字，默认为空
    ///   - textAlignment: 文字排版，默认左对齐
    ///   - numberOfLines: label限制的行数，默认一行
    ///   - text: label的文字，默认为空
    /// - Returns: BaseLabel
    class func normalLabel(font: UIFont, andTextColorHex textColorHex: String? = nil, andTextAlignment textAlignment: NSTextAlignment = NSTextAlignment.left, andNumberOfLines numberOfLines: Int = 1, andText text: String? = nil)->BaseLabel{
        let label: BaseLabel = BaseLabel()
        label.font = font
        if textColorHex != nil{
            label.textColor = UIColor(hexString: textColorHex!)
        }
        if text != nil{
            label.text = text!
        }
        label.textAlignment = textAlignment
        label.numberOfLines = numberOfLines
        return label
    }

    //MARK: 设置行间距
    /// 设置行间距
    ///
    /// - Parameters:
    ///   - text: 文字
    ///   - lineSpace: 行间距
    func setParagrapText(text: String?, andLineSpace lineSpace: CGFloat?){
        if text == nil || text == "" || lineSpace == nil{
            return
        }
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle.init()
        paragraphStyle.lineSpacing = lineSpace!
        paragraphStyle.lineBreakMode = self.lineBreakMode
        paragraphStyle.alignment = self.textAlignment
        
        let  attributeText: NSMutableAttributedString = NSMutableAttributedString.init(string: text!)
        attributeText.addAttributes([NSParagraphStyleAttributeName: paragraphStyle], range: NSRange.init(location: 0, length: text!.characters.count))
        self.attributedText = attributeText
    }
}
