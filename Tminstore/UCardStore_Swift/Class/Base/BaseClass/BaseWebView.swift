//
//  BaseWebView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import WebKit
import SnapKit

class BaseWebView: BaseViewController {
    
    /// 是否可以使用pop方法来退出
    var canUsePopToDismiss: Bool = false
    /// 网页路径
    var urlString: String?
    /// 退出的闭包
    var dismissClosure: (()->())?
    /// 进度条
    fileprivate var progressView: UIProgressView?
    
    fileprivate var webView: WKWebView?
    ///是否已经有观察器了
    fileprivate var hasObserver: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        setWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("推送网页")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("推送网页")
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationBarLeftButtonWithImageName("backArrow")
        setNavigationBarRightButtonWithImageName("dot")
    }
    
    //MARK: 设置webView
    fileprivate func setWebView(){
        
        webView = WKWebView()
        webView?.evaluateJavaScript("navigator.userAgent", completionHandler: { [weak self] (result: Any?, _) in
            
            if result != nil{//设置UserAgent
                let userAgent: String = result! as! String
                let newUserAgent: String = userAgent + "/timeory"
                let userAgentDefaults: [String: Any] = ["UserAgent": newUserAgent] as [String: Any]
                UserDefaults.standard.register(defaults: userAgentDefaults)
                UserDefaults.standard.synchronize()//立即保存
            }
            
            //回到主线程重新设置webView，并加载网页
            DispatchQueue.main.async {
                
                let config: WKWebViewConfiguration = WKWebViewConfiguration()
                config.userContentController = WKUserContentController()
                config.userContentController.add(self!, name: "ios_native")
                
                self?.webView = WKWebView(frame: CGRect.zero, configuration: config)
                self?.webView?.allowsBackForwardNavigationGestures = true//允许返回手势
                self?.webView?.uiDelegate = self//UI交互代理
                self?.webView?.navigationDelegate = self//导航代理
                self?.view.addSubview((self?.webView)!)
                self?.webView?.snp.makeConstraints { (make: ConstraintMaker) in
                    make.edges.equalTo((self?.view)!)
                }
                
                let url: URL = URL(string: (self?.urlString)!)!
                let request: URLRequest = URLRequest(url: url)
                _ = self?.webView?.load(request)
                
                self?.setProgressView()//设置进度条
            }
        })
    }
    
    //MARK: 设置进度条
    fileprivate func setProgressView(){
        webView?.addObserver(self, forKeyPath: "estimatedProgress", options: NSKeyValueObservingOptions.new, context: nil)//检测网页加载进度
        hasObserver = true
        progressView = UIProgressView()
        progressView?.trackTintColor = UIColor.clear
        view.addSubview(progressView!)
        progressView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
        })
    }
    
    //MARK: 导航栏左键的点击事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        if webView?.canGoBack == true{
            _ = webView?.goBack()
        }else{
            if canUsePopToDismiss{
                navigationController?.popViewController(animated: true)
            }else{
                dismissSelf(animated: true, completion: {[weak self] _ in
                    self?.dismissClosure?()
                })
            }
        }
    }
    
    //MARK: 点击导航栏右按钮，提供刷新功能
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        let ac: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        // 刷新
        let reloadAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("刷新", comment: ""), style: UIAlertActionStyle.default) { [weak self] (_) in
            _ = self?.webView?.reload()
        }
        ac.addAction(reloadAction)
        // 分享
        let shareAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("分享", comment: ""), style: UIAlertActionStyle.default) { [weak self](_) in
            if let url: URL = self?.webView?.url{
                let shareVC: UIActivityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
                self?.present(shareVC, animated: true, completion: nil)
            }
        }
        ac.addAction(shareAction)
        //取消
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        ac.addAction(cancelAction)
        present(ac, animated: true, completion: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress"{//检测网页加载进度
            if (webView != nil) {
                progressView?.isHidden = webView?.estimatedProgress == 1.0
                progressView?.setProgress(Float(webView!.estimatedProgress), animated: false)
            }
        }
    }
    
    deinit {
        webView?.configuration.userContentController.removeAllUserScripts()
        if hasObserver == true{
            webView?.removeObserver(self, forKeyPath: "estimatedProgress")
        }
    }
}

extension BaseWebView: WKScriptMessageHandler, WKUIDelegate, WKNavigationDelegate{
    
    //MARK: 设置webView的title
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        if let title: String = webView.title{// 设置标题
            setNavigationAttributeTitle(title, attributeDic: attDic)
        }
    }
    
    //MARK: 执行JS端的方法
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == "ios_native" {
            
            if let dic: [String: Any] = message.body as? [String: Any], let funcName: String = dic["func"] as? String{
                if funcName == "alert"{//弹窗
                    if let title: String = dic["body"] as? String{
                        showMessage(title: title, andMessage: nil)
                    }
                }else if funcName == "getUserToken_ios"{//发送用户的token
                    sendUserToken()
                }else if funcName == "close_ios"{
                    dismissSelf()
                }
            }
        }
    }
    
    //MARK: 向前端发送用户的token
    fileprivate func sendUserToken(){
        let jsFunction: String = "getToken(\"\(getUserToken())\")"
        webView?.evaluateJavaScript(jsFunction, completionHandler: { (result: Any?, error: Error?) in
            print(result ?? "")
            print(error?.localizedDescription ?? "")
        })
    }
    
    //MARK: 执行JS的弹窗
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        //        print(message)
        completionHandler(
            showMessage(title: message, andMessage: nil)
        )
    }
    
    //MARK: 参数转字典
    fileprivate func queryStringToDictionary(string: String)->[String: Any]{
        let elements: [String] = string.components(separatedBy: "&")
        var retval: [String: Any] = [String: Any]()
        for e: String in elements {
            let pair: [String] = e.components(separatedBy: "=")
            retval[pair[0]] = pair[1]
        }
        return retval
    }
    
    //MARK: 执行JS的弹窗
    /// 执行JS的弹窗
    ///
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 消息
    fileprivate func showMessage(title: String?, andMessage message: String?){
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel: UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        ac.addAction(cancel)
        present(ac, animated: true, completion: nil)
    }

}
