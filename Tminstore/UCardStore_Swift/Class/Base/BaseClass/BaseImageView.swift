//
//  BaseImageView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseImageView: UIImageView {
    
    /// 点击图片的事件，需要先调用addTapGesture()增加点击事件之后，才能使用
    var tapImageView: (()->())?
    
    //MARK: 设置阴影
    /// 设置阴影
    func setShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 4
    }
    
    //MARK: 带圆角UIImageView
    /// 带圆角UIImageView
    ///
    /// - Parameter cornerRadius: 圆角大小
    /// - Returns: BaseImageView
    class func userPortraitView(cornerRadius: CGFloat)->BaseImageView{
        let imageView: BaseImageView = BaseImageView()
        imageView.layer.cornerRadius = cornerRadius
        imageView.layer.masksToBounds = true
        imageView.contentMode = UIViewContentMode.scaleToFill
        imageView.isUserInteractionEnabled = true
        return imageView
    }

    //MARK: 普通的imageView
    /// 普通的imageView
    ///
    /// - Parameters:
    ///   - contentMode: 图片缩放模式，默认等比例不铺满
    ///   - imageName: 本地icon的名字，默认为空。若为空，则不加载图片
    /// - Returns: BaseImageView
    class func normalImageView(contentMode: UIViewContentMode = UIViewContentMode.scaleAspectFit, andImageName imageName: String? = nil)->BaseImageView{
        let imageView: BaseImageView = BaseImageView()
        imageView.contentMode = contentMode
        imageView.clipsToBounds = true
        if imageName != nil{
            imageView.image = UIImage(named: imageName!)
        }
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }
    
    //MARK: 增加点击事件
    /// 为imageView增加点击事件
    func addTapGesture() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapImage(tap:)))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    //MARK: 图片的点击事件
    func tapImage(tap: UITapGestureRecognizer){
        tapImageView?()
    }
    
    //MARK: 获取当前页面的截图
    /// 获取当前页面的截图
    ///
    /// - Returns: UIImage?
    func getBitmapImage()->UIImage?{
        //绘制背景
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 0.0);
        //背景
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}
