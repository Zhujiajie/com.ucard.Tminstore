//
//  BaseBarButtonItem.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseBarButtonItem: UIBarButtonItem {

    /// 点击按钮的事件
    var touchItemClosure: (()->())?
    
    //MARK: 普通的带icon的BarButtonItem
    /// 普通的带icon的BarButtonItem
    ///
    /// - Parameter iconName: icon的名字
    /// - Returns: BaseBarButtonItem
    class func normalIconBarButtonItem(iconName: String)->BaseBarButtonItem{
        let barItem: BaseBarButtonItem = BaseBarButtonItem()
        barItem.image = UIImage(named: iconName)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)//始终保持图片原始状态
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchBarButtonItem(barItem:))
        return barItem
    }
    
    //MARK: 主界面点击展示个人界面的按钮
    /// 主界面点击展示个人界面的按钮
    ///
    /// - Returns: BaseBarButtonItem
    class func showPersonItem()->BaseBarButtonItem{
        let barItem: BaseBarButtonItem = BaseBarButtonItem()
        barItem.image = UIImage(named: "Person")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)//始终保持图片原始状态
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchBarButtonItem(barItem:))
        return barItem
    }
    
    //MARK: 主界面点击进去AR的按钮
    /// 主界面点击进去AR的按钮
    ///
    /// - Returns: BaseBarButtonItem
    class func showARItem()->BaseBarButtonItem{
        let barItem: BaseBarButtonItem = BaseBarButtonItem()
        barItem.image = UIImage(named: "enterAR")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)//始终保持图片原始状态
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchBarButtonItem(barItem:))
        return barItem
    }
    
    //MARK: 点击更多按钮的事件
    func touchBarButtonItem(barItem: UIBarButtonItem){
        touchItemClosure?()
    }
}
