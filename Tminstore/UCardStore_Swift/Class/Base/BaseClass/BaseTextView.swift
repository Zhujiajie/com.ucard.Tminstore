//
//  BaseTextView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BaseTextView: FSTextView {

    //MARK: 普通的textView
    /// 普通的textView
    ///
    /// - Parameters:
    ///   - font: 字体
    ///   - placeHolder: 占位符，默认为空
    ///   - cornerRadius: 圆角，默认为空
    ///   - borderWidth: 边框宽度，默认为空
    ///   - borderColorHexString: 边框颜色，默认为空
    class func normalTextView(font: UIFont, andPlaceHolder placeHolder: String? = nil, andCornerRadius cornerRadius: CGFloat? = nil, andBorderWidth borderWidth: CGFloat? = nil, andBorderColorHexString borderColorHexString: String? = nil)->BaseTextView{
        let textView: BaseTextView = BaseTextView()
        textView.font = font
        textView.placeholder = placeHolder
        if cornerRadius != nil{
            textView.cornerRadius = cornerRadius!
        }
        if borderWidth != nil{
            textView.borderWidth = borderWidth!
        }
        if borderColorHexString != nil{
            textView.borderColor = UIColor(hexString: borderColorHexString!)
        }
        return textView
    }

}
