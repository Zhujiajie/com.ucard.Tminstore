//
//  BaseDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/2.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Alamofire

class BaseDataModel: NSObject {
    
    /// 对上传到又拍云的图片进行添加缩略图的参数
    let imageWatermarkParameters: [String: Any] = [
        "x-gmkerl-thumb": "/watermark/url/L2lPU0ltYWdlcy93YXRlcm1hcmswMS5wbmc="
    ]
    
    /// 网络错误的闭包
    var errorClosure: ((_ error: Error?)->())?
    
    /// 网络请求完成的闭包
    var completionClosure: (()->())?
    
    /// Alamofire的manager, 连接超时为20秒
    fileprivate let manager: SessionManager = {
        
        let configuraton: URLSessionConfiguration = URLSessionConfiguration.default
        configuraton.timeoutIntervalForRequest = 20
        return Alamofire.SessionManager(configuration: configuraton)
    }()
    
    /// 当前的网络请求
    fileprivate var request: Alamofire.Request?
    
    //MARK: GET方法
    /**
     GET方法
     
     - parameter URLString:  URL
     - parameter parameters: 参数
     - parameter success:    成功返回的闭包
     - parameter failure:    失败返回的闭包
     - parameter completion: 结束返回的闭包
     */
    func sendGETRequestWithURLString(URLStr URLString: String, ParametersDictionary parameters: [String: Any]?, urlEncoding encoding: ParameterEncoding = JSONEncoding.default, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())? = nil){
        print("url is: \(URLString)")
        request = manager.request(URLString, method: HTTPMethod.get, parameters: parameters, encoding: encoding, headers: nil)
            .validate()
            .responseJSON { (response: DataResponse<Any>) in
                switch response.result{
                case Result.success(_):
                    if let json: [String: Any] = response.result.value as? [String: Any]{
                        success?(json)
                    }else{
                        failure?(nil)
                    }
                case Result.failure(_):
                    failure?(response.result.error)//网络请求失败的闭包
                }
                completion?()//网络请求结束的闭包
        }
    }
    
    //MARK: POST方法
    /**
     POST方法
     
     - parameter URLString:  URL
     - parameter parameters: 参数
     - parameter success:    成功返回的闭包
     - parameter failure:    失败返回的闭包
     */
    func sendPOSTRequestWithURLString(URLStr URLString: String, ParametersDictionary parameters: [String: Any]?, urlEncoding encoding: ParameterEncoding = JSONEncoding.default, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())? = nil){
        request = manager.request(URLString, method: HTTPMethod.post, parameters: parameters, encoding: encoding, headers: nil)
            .validate()
            .responseJSON { (response: DataResponse<Any>) in
                switch response.result{
                case Result.success(_):
                    if let json: [String: Any] = response.result.value as? [String: Any]{
                        success?(json)
                    }else{
                        failure?(nil)
                    }
                case Result.failure(_):
                    failure?(response.result.error)//网络请求失败的闭包
                }
                completion?()
        }
    }
    
    //MARK: PUT方法
    /**
     PUT方法
     
     - parameter URLString:  URL
     - parameter parameters: 参数
     - parameter success:    成功返回的闭包
     - parameter failure:    失败返回的闭包
     */
    func sendPUTRequestWithURLString(URLStr URLString: String, ParametersDictionary parameters: [String: Any]?, urlEncoding encoding: ParameterEncoding = JSONEncoding.default, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())? = nil){
        
        request = manager.request(URLString, method: HTTPMethod.put, parameters: parameters, encoding: encoding, headers: nil)
            .validate()
            .responseJSON { (response: DataResponse<Any>) in
                switch response.result{
                case Result.success(_):
                    if let json: [String: Any] = response.result.value as? [String: Any]{
                        success?(json)
                    }else{
                        failure?(nil)
                    }
                case Result.failure(_):
                    failure?(response.result.error)//网络请求失败的闭包
                }
                completion?()
        }
    }
    
    //MARK: DELETE方法
    /**
     DELETE方法
     
     - parameter URLString:  URL
     - parameter parameters: 参数
     - parameter success:    成功返回的闭包
     - parameter failure:    失败返回的闭包
     */
    func sendDELETERequestWithURLString(URLStr URLString: String, ParametersDictionary parameters: [String: Any]?, urlEncoding encoding: ParameterEncoding = JSONEncoding.default, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())? = nil){
        
        request = manager.request(URLString, method: HTTPMethod.delete, parameters: parameters, encoding: encoding, headers: nil)
            .validate()
            .responseJSON { (response: DataResponse<Any>) in
                switch response.result{
                case Result.success(_):
                    if let json: [String: Any] = response.result.value as? [String: Any]{
                        success?(json)
                    }else{
                        failure?(nil)
                    }
                case Result.failure(_):
                    failure?(response.result.error)//网络请求失败的闭包
                }
                completion?()
        }
    }
    
    //MARK: 向服务器提交极光推送ID
    /**
     向服务器提交极光推送ID
     */
    func postJPushID() {
        
        if JPUSHService.registrationID() == nil || JPUSHService.registrationID() == "" || getUserToken() == ""{
            return
        }
        
        let parameters: [String: Any] = ["jpushId": JPUSHService.registrationID()!,
                          "token": getUserToken()] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.postJPushIDAPI, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
        }, failureClourse: { (error: Error?) in
            if error != nil{
                print(error!.localizedDescription)
            }
        }, completionClosure: nil)
    }
    
    //MARK: 没有请求头的网络请求
    /// 没有请求头的网络请求，可用于登录注册
    ///
    /// - Parameters:
    ///   - urlStr: API地址
    ///   - method: 方法
    ///   - para: 参数（可选）
    ///   - encoding: encoding模式，默认JSON
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - complete: 完成的闭包
    func requestWithoutHeaders(urlStr: String, httpMethod method: HTTPMethod, parameters para: [String: Any]?, urlEncoding encoding: ParameterEncoding = JSONEncoding.default, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?, completionClosure complete: (()->())?){
        
        request = manager.request(urlStr, method: method, parameters: para, encoding: encoding, headers: nil)
            .validate()
            .responseJSON(completionHandler: { (response: DataResponse<Any>) in
                switch response.result{
                case Result.success(_):
                    if let json: [String: Any] = response.result.value as? [String: Any]{
                        success?(json)
                    }else{
                        failure?(nil)
                    }
                case Result.failure(_):
                    failure?(response.result.error)//网络请求失败的闭包
                }
                complete?()
            })
    }
    
    //MARK: 删除coreData
    /// 删除coreData
    ///
    /// - Parameter postcardData: Postcard
    func deletePostcardData(postcardData: Postcard){
        if let videoPath: String = postcardData.videoPath {
            do {
//                print(videoPath)
                try FileManager.default.removeItem(at: URL(string: videoPath)!)
//                print("delete video success")
            }catch{ print(error.localizedDescription) }
        }
        app.managedObjectContext.delete(postcardData)
        app.saveContext()
//        print("delete core data success")
    }
    
    //MARK: 上传文件到又拍云
    /// 上传文件到又拍云
    ///
    /// - Parameters:
    ///   - data: 需要将数据转换为Data
    ///   - saveKey: 存储的路径
    ///   - parameters: 额外的参数，如添加水印等
    ///   - successClosure: 成功的闭包
    ///   - fail: 失败的闭包
    ///   - progress: 上传进度的闭包
    func uploadToUpyun(data: Data, andSaveKey saveKey: String, andParameters parameters: [String: Any]?, successClosure success: ((_ response: HTTPURLResponse?, _ responseBody: [AnyHashable : Any]?)->())?, failClosure fail: ((_ error: Error?, _ response: HTTPURLResponse?, _ responseBody: [AnyHashable : Any]?)->())?, progressClosure progress: ((_ completedBytesCount: Float, _ totalBytesCount: Float)->())?){
        
        /// 又拍云空间名
        let upyunBucketName: String = "ucardstorevideo"
        /// 又拍云管理员名
        let upyunOperator: String = "wenslow"
        /// 又拍云管理员密码
        let upyunPassword: String = "1443940727a/"
        
        let up: UpYunFormUploader = UpYunFormUploader()
        
        up.upload(withBucketName: upyunBucketName, operator: upyunOperator, password: upyunPassword, fileData: data, fileName: nil, saveKey: saveKey, otherParameters: parameters, success: { (response: HTTPURLResponse?, responseBody: [AnyHashable : Any]?) in
            success?(response, responseBody)
        }, failure: { (error: Error?, response: HTTPURLResponse?, responseBody: [AnyHashable : Any]?) in
            DispatchQueue.main.async {
                fail?(error, response, responseBody)
            }
        }, progress: { (completedBytesCount: Int64, totalBytesCount: Int64) in
            DispatchQueue.main.async {
                progress?(Float(completedBytesCount), Float(totalBytesCount))
            }
        })
    }
    
    //MARK: 上传进度
    func uploadImageProgress(percent: Float){
        SVProgressHUD.showProgress(percent, status: NSLocalizedString("上传图片中", comment: ""))
    }
    
    //MARK: 上传视频的进度
    func uploadVideoProgress(percent: Float){
        SVProgressHUD.showProgress(percent, status: NSLocalizedString("上传AR视频中", comment: ""))
    }
}
