//
//  BaseRefreshFooter.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/13.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MJRefresh

class BaseRefreshFooter: MJRefreshAutoStateFooter {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setTitle(NSLocalizedString("--------------我是有底线的--------------", comment: ""), for: MJRefreshState.noMoreData)
        stateLabel.textColor = UIColor(hexString: "878686")
        stateLabel.font = UIFont.systemFont(ofSize: 11)
    }
    
    override func beginRefreshing() {
        super.beginRefreshing()
        //上拉时，轻微震动
        if #available(iOS 10.0, *) {
            let feedbackGenerator: UIImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: UIImpactFeedbackStyle.light)
            feedbackGenerator.prepare()
            feedbackGenerator.impactOccurred()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
