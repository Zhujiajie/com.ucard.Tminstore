//
//  BaseViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/2.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class BaseViewModel: NSObject {
    
    /// 出错的闭包
    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    //MARK: 展示加载器
    /**
     展示加载器
     
     - parameter title:    加载器上的标题
     - parameter maskType: mask类型，默认为.Clear
     */
    func showSVProgress(title: String?, andMaskType maskType :SVProgressHUDMaskType = .clear){
        if title == nil {
            SVProgressHUD.show()
        }else{
            SVProgressHUD.show(withStatus: NSLocalizedString(title!, comment: ""))
        }
        SVProgressHUD.setDefaultMaskType(maskType)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
    }
    
    //MARK: 隐藏加载指示器
    /**
     隐藏加载指示器
     */
    func dismissSVProgress(){
        SVProgressHUD.dismiss()
        SVProgressHUD.setDefaultMaskType(.none)
    }
    
    //MARK: 展示进度
    /// 展示进度
    ///
    /// - Parameters:
    ///   - current: 目前完成
    ///   - total: 总共需要完成
    ///   - title: 标题
    func showProcess(current: Float, andTotal total: Float, andTitle title: String? = nil){
        if title != nil{
            SVProgressHUD.setStatus(title)
        }
        SVProgressHUD.showProgress(current/total)
    }
}
