//
//  BaseTabbarView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class BaseTabbarView: UIView {

    /// 点击商城按钮
    var touchMarketButton: (()->())?
    /// 点击ARdemo按钮
    var touchARKitDemoButton: (()->())?
    /// 点击发布到地图按钮的事件
    var touchMapButtonClosure: (()->())?
    /// 点击发布到社区按钮的事件
    var touchCommunityButtonClosure: (()->())?
    /// 释放的闭包
    var dismissCompleteClosure: (()->())?
    /// 发布到社区的按钮
    fileprivate var releaseToCommunityButton: UIButton?
    /// 发布到社区的label
    fileprivate var releaseToCommunityLabel: UILabel?
    /// 发布到地图的按钮
    fileprivate var releaseToMapButton: UIButton?
    /// 发布到地图的label
    fileprivate var releaseToMapLabel: UILabel?
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(white: 0.0, alpha: 0.0)
        
        //需要镂空的位置
        let iconFrame: CGRect = CGRect(x: screenWidth/2 - 13.5, y: screenHeight - 37.5, width: 27, height: 27)
        //外层蒙版的路径
        let path: UIBezierPath = UIBezierPath(rect: UIScreen.main.bounds)
        //镂空蒙版的路径
        let emptyPath: UIBezierPath = UIBezierPath(roundedRect: iconFrame, cornerRadius: 5)
        path.append(emptyPath.reversing())
        
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.clear.cgColor
        
        layer.mask = shapeLayer
        
        //发布到地图
        releaseToMapButton = UIButton()
        releaseToMapButton?.setImage(UIImage(named: "releaseToMap"), for: UIControlState.normal)
        addSubview(releaseToMapButton!)
        releaseToMapButton?.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(self).offset(50/baseWidth)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        }
        releaseToMapButton?.addTarget(self, action: #selector(touchReleaseToMapButton(button:)), for: UIControlEvents.touchUpInside)
        
        releaseToMapLabel = UILabel()
        releaseToMapLabel?.textColor = UIColor.white
        releaseToMapLabel?.font = UIFont.systemFont(ofSize: 14)
        releaseToMapLabel?.text = NSLocalizedString("发布到地图", comment: "")
        addSubview(releaseToMapLabel!)
        releaseToMapLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(releaseToMapButton!.snp.right).offset(8/baseWidth)
            make.bottom.equalTo(self).offset(50/baseWidth)
        })
        
        //发布到社区
        releaseToCommunityButton = UIButton()
        releaseToCommunityButton?.setImage(UIImage(named: "releaseToCommunity"), for: UIControlState.normal)
        addSubview(releaseToCommunityButton!)
        releaseToCommunityButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(50/baseWidth)
            make.size.equalTo(releaseToMapButton!)
        })
        releaseToCommunityButton?.addTarget(self, action: #selector(touchReleaseToCommunity(button:)), for: UIControlEvents.touchUpInside)
        
        releaseToCommunityLabel = UILabel()
        releaseToCommunityLabel?.textColor = UIColor.white
        releaseToCommunityLabel?.font = UIFont.systemFont(ofSize: 14)
        releaseToCommunityLabel?.text = NSLocalizedString("发布到社区", comment: "")
        addSubview(releaseToCommunityLabel!)
        releaseToCommunityLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(releaseToMapLabel!)
            make.bottom.equalTo(self).offset(50/baseWidth)
        })
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapSelfToDismiss(tap:)))
        addGestureRecognizer(tap)
        
        appearAnimation()
        
        //进入记忆店铺的按钮
        let button: BaseButton = BaseButton.normalTitleButton(title: "商城", andTextFont: UIFont.systemFont(ofSize: 18), andTextColorHexString: "EEC75C", andBorderColorHexString: "EEC75C", andBorderWidth: 3)
        addSubview(button)
        button.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(self).offset(50)
            make.size.equalTo(CGSize(width: 200, height: 200))
            make.centerX.equalTo(self)
        }
        button.touchButtonClosure = {[weak self] _ in
            self?.touchMarketButton?()
        }
    }
    
    //MARK: 点击发布到地图按钮
    func touchReleaseToMapButton(button: UIButton){
        touchMapButtonClosure?()
        dismissAnimation()
    }
    
    //MARK: 点击发布到社区按钮
    func touchReleaseToCommunity(button: UIButton){
        touchCommunityButtonClosure?()
        dismissAnimation()
    }
    
    //MARK: 点击视图，消失
    func tapSelfToDismiss(tap: UITapGestureRecognizer){
        dismissAnimation()
    }
    
    //MARK: 消失的动画
    fileprivate func dismissAnimation(){
        UIView.animate(withDuration: 0.5, animations: { [weak self] _ in
            self?.alpha = 0
            }, completion: {[weak self] _ in
                self?.dismissCompleteClosure?()
        })
    }
    
    //MARK: 出现的动画
    fileprivate func appearAnimation(){
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.backgroundColor = UIColor(white: 0.0, alpha: 0.57)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.6, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.releaseToMapButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.bottom.equalTo(self!).offset(-68/baseWidth)
            })
            autoLayoutAnimation(view: self!)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.8, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.releaseToMapLabel?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.bottom.equalTo(self!).offset(-81/baseWidth)
            })
            autoLayoutAnimation(view: self!)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 1.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.releaseToCommunityButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.bottom.equalTo(self!).offset(-136/baseWidth)
            })
            autoLayoutAnimation(view: self!)
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 1.2, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.releaseToCommunityLabel?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.bottom.equalTo(self!).offset(-148/baseWidth)
            })
            autoLayoutAnimation(view: self!)
        }, completion: nil)
        
//        UIView.animateKeyframes(withDuration: 2.0, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModePaced, animations: {[weak self] _ in
//            //视图出现
//            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.25, animations: {
//                self?.backgroundColor = UIColor(white: 0.0, alpha: 0.57)
//            })
//            //发布到地图的按钮
//            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5, animations: {
//                self?.releaseToMapButton?.snp.updateConstraints({ (make: ConstraintMaker) in
//                    make.bottom.equalTo(self!).offset(-68/baseWidth)
//                })
//                autoLayoutAnimation(view: self!)
//            })
//            //发布到地图的label
//            UIView.addKeyframe(withRelativeStartTime: 0.2, relativeDuration: 0.5, animations: {
//                self?.releaseToMapLabel?.snp.updateConstraints({ (make: ConstraintMaker) in
//                    make.bottom.equalTo(self!).offset(-81/baseWidth)
//                })
//                autoLayoutAnimation(view: self!)
//            })
//            //发布到社区按钮出现
//            UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.5, animations: {
//                self?.releaseToCommunityButton?.snp.updateConstraints({ (make: ConstraintMaker) in
//                    make.bottom.equalTo(self!).offset(-136/baseWidth)
//                })
//                autoLayoutAnimation(view: self!)
//            })
//            //发布到社区的label
//            UIView.addKeyframe(withRelativeStartTime: 0.6, relativeDuration: 0.5, animations: {
//                self?.releaseToCommunityLabel?.snp.updateConstraints({ (make: ConstraintMaker) in
//                    make.bottom.equalTo(self!).offset(-148/baseWidth)
//                })
//                autoLayoutAnimation(view: self!)
//            })
//        }, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
