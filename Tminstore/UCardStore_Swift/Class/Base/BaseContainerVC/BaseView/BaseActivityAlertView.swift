//
//  BaseActivityAlertView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 活动推广的弹窗
import UIKit
import SnapKit
import ChameleonFramework

class BaseActivityAlertView: UIView {

    ///更新的文案
    var alertText: String = ""{
        didSet{
            textView?.text = alertText
        }
    }
    
    fileprivate var textView: UITextView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.35)
        alpha = 0
        
//        //点击退出
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToDismiss(tap:)))
//        addGestureRecognizer(tap)
        
        //容器视图，用于添加动画
        let containerView: UIView = UIView()
        containerView.backgroundColor = UIColor.clear
        addSubview(containerView)
        containerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(self).offset(77/baseWidth)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 303/baseWidth, height: 462/baseWidth))
        }
        
        //背景图片
        let imageView: UIImageView = UIImageView()
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        containerView.addSubview(imageView)
        imageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(containerView)
            make.height.equalTo(398/baseWidth)
        }
        imageView.sd_setImage(with: appAPIHelp.activityAlertBackgroundImage())
        
        //添加textView
        textView = UITextView()
        textView?.isEditable = false
        textView?.isSelectable = false
        textView?.textColor = UIColor(hexString: "696969")
        textView?.font = UIFont.systemFont(ofSize: 14)
        containerView.addSubview(textView!)
        textView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(containerView).offset(163/baseWidth)
            make.left.equalTo(containerView).offset(20/baseWidth)
            make.right.equalTo(containerView).offset(-20/baseWidth)
            make.bottom.equalTo(containerView).offset(-86/baseWidth)
        })
        
//        //更新的按钮
//        let updateButton: UIButton = UIButton()
//        updateButton.backgroundColor = UIColor(hexString: "#4BFFFB")
//        updateButton.layer.cornerRadius = 20.5/baseWidth
//        updateButton.layer.masksToBounds = true
//        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor.white]
//        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("立即更新", comment: ""), attributes: attDic)
//        updateButton.setAttributedTitle(attStr, for: UIControlState.normal)
//        containerView.addSubview(updateButton)
//        updateButton.snp.makeConstraints { (make: ConstraintMaker) in
//            make.centerX.equalTo(containerView)
//            make.bottom.equalTo(containerView).offset(-86/baseWidth)
//            make.size.equalTo(CGSize(width: 243/baseWidth, height: 41/baseWidth))
//        }
//        updateButton.addTarget(self, action: #selector(touchUpdateButton(button:)), for: UIControlEvents.touchUpInside)
        
        let dismissButton: UIButton = UIButton()
        dismissButton.setImage(UIImage(named: "dismissIntrol"), for: UIControlState.normal)
        containerView.addSubview(dismissButton)
        dismissButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(imageView.snp.bottom).offset(28/baseWidth)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        }
        dismissButton.addTarget(self, action: #selector(touchDismissButton(button:)), for: UIControlEvents.touchUpInside)
        
        //渐变和缩放动画
        containerView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
            self?.alpha = 1
            containerView.transform = CGAffineTransform.identity
            }, completion: nil)
    }
    
//    //MARK: 点击更新按钮的事件
//    func touchUpdateButton(button: UIButton){
//        let url: URL = URL(string: "https://itunes.apple.com/us/app/ucardstore-send-share-real/id966372400?mt=8")!
//        if UIApplication.shared.canOpenURL(url){
//            UIApplication.shared.openURL(url)
//        }
//        dismissAnimation()
//    }
    
    //MARK: 点击退出按钮的事件
    func touchDismissButton(button: UIButton){
        dismissAnimation()
    }
    
//    //MARK: 点击视图退出
//    func tapToDismiss(tap: UITapGestureRecognizer){
//        dismissAnimation()
//    }
    
    //MARK: 消失的动画
    /// 消失的动画
    fileprivate func dismissAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            self.alpha = 0
        }) { (_) in
            self.layer.removeAllAnimations()
            self.removeFromSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
