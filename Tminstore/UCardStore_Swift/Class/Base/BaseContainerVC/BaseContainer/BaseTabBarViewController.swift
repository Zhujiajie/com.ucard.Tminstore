//
//  BaseTabBarViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import Photos
import CoreLocation

class BaseTabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    /// TabbarItem的tag
    ///
    /// - time: 时光
    /// - discovery: 探索
    /// - print: 印记
    /// - find: 发现
    /// - me: 个人界面
    enum TabBarTag: Int{
        case time
        case discovery
        case print
        case find
        case me
    }
    
    fileprivate var releaseView: BaseTabbarView?
    /// 是否已经进入地图视图
    fileprivate var hasLoadMapVC: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 不透明
        tabBar.isTranslucent = false
        
        // 五个item，时光，探索，印记，发现，个人
        let tabBarTime: UITabBarItem = UITabBarItem.init(title: "时光", image: #imageLiteral(resourceName: "tab_time").withRenderingMode(UIImageRenderingMode.alwaysOriginal), tag: TabBarTag.time.rawValue)
        tabBarTime.selectedImage = #imageLiteral(resourceName: "tab_time_selected").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarTime.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "828282")], for: UIControlState.normal)
        tabBarTime.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "FFB568")], for: UIControlState.selected)
        let tabBarDiscovery: UITabBarItem = UITabBarItem(title: "探索", image: #imageLiteral(resourceName: "tab_discover").withRenderingMode(UIImageRenderingMode.alwaysOriginal), tag: TabBarTag.discovery.rawValue)
        tabBarDiscovery.selectedImage = #imageLiteral(resourceName: "tab_discover_selected").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarDiscovery.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "828282")], for: UIControlState.normal)
        tabBarDiscovery.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "FFB568")], for: UIControlState.selected)
        let tabBarPrint: UITabBarItem = UITabBarItem(title: "印记", image: #imageLiteral(resourceName: "tab_print").withRenderingMode(UIImageRenderingMode.alwaysOriginal), tag: TabBarTag.print.rawValue)
        tabBarPrint.selectedImage = #imageLiteral(resourceName: "tab_print_selected").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarPrint.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "828282")], for: UIControlState.normal)
        tabBarPrint.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "FFB568")], for: UIControlState.selected)
        let tabBarFind: UITabBarItem = UITabBarItem(title: "发现", image: #imageLiteral(resourceName: "tab_find").withRenderingMode(UIImageRenderingMode.alwaysOriginal), tag: TabBarTag.find.rawValue)
        tabBarFind.selectedImage = #imageLiteral(resourceName: "tab_find_selected").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarFind.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "828282")], for: UIControlState.normal)
        tabBarFind.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "FFB568")], for: UIControlState.selected)
        let tabBarMe: UITabBarItem = UITabBarItem(title: "个人", image: #imageLiteral(resourceName: "tab_me").withRenderingMode(UIImageRenderingMode.alwaysOriginal), tag: TabBarTag.me.rawValue)
        tabBarMe.selectedImage = #imageLiteral(resourceName: "tab_me_selected").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        tabBarMe.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "828282")], for: UIControlState.normal)
        tabBarMe.setTitleTextAttributes([NSForegroundColorAttributeName :UIColor(hexString: "FFB568")], for: UIControlState.selected)
        
//        let mainVC: MainController = MainController()
        let mainContentVC: MainContentVC = MainContentVC()
//        mainContentVC.umengViewName = "社区"
        let mainNav: UINavigationController = UINavigationController(rootViewController: mainContentVC)
        mainNav.tabBarItem = tabBarTime
        
        let vc2: UIViewController = UIViewController()
//        let imageView: UIImageView = UIImageView()
//        imageView.contentMode = UIViewContentMode.scaleAspectFill
//        vc2.view.addSubview(imageView)
//        imageView.frame = vc2.view.bounds
//        imageView.sd_setImage(with: mapTestInfoImageURL)
        vc2.tabBarItem = tabBarDiscovery
        
        let vc3: UIViewController = UIViewController()
        vc3.tabBarItem = tabBarPrint
        
        let vc4: UIViewController = UIViewController()
        vc4.tabBarItem = tabBarFind
        
        let pMainVC: PMainController = PMainController()
        pMainVC.umengViewName = "个人"
        let pMainNav: PMainNavigationVC = PMainNavigationVC(rootViewController: pMainVC)
        pMainNav.tabBarItem = tabBarMe
        
        viewControllers = [mainNav, vc2, vc3, vc4, pMainNav]
        
        //将图片向下偏移
        _ = viewControllers?.map({ (vc: UIViewController) in
            vc.tabBarItem.imageInsets = UIEdgeInsets(top: 5.5, left: 0, bottom: -5.5, right: 0)
        })
        
        self.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: 点击barItem，进入相应的界面
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        switch viewController.tabBarItem.tag {
            
        case TabBarTag.time.hashValue://点击了社区主页的按钮，刷新界面
            MobClick.event("tabBar_community")
            if let nav: UINavigationController = viewController as? UINavigationController, let mainVC: MainController = nav.viewControllers.first as? MainController{
                if selectedViewController == nav{
                    mainVC.loadData()
                }
            }
            
        case TabBarTag.discovery.rawValue:// 点击地图按钮，进入地图视图
            
//            if isTestUser() == false {
//                return true
//            }
            
            MobClick.event("tabBar_map")
            
//            alert(alertTitle: NSLocalizedString("功能开发中\n敬请期待", comment: ""))
            enterMapVC(viewController: viewController)
            return false
            
        case TabBarTag.print.rawValue://点击了发布按钮
            MobClick.event("tabBar_release")
            touchMiddleButton()
            return false
            
        case TabBarTag.find.rawValue://点击扫描按钮，进入AR界面
            MobClick.event("tabBar_AR")
            enterARVC(viewController: viewController)
            return false
            
        default://点击个人界面
            MobClick.event("tabBar_profile")
            break
        }
        return true
    }
    
    //MARK: 进入地图界面
    func enterMapVC(viewController: UIViewController?){
        
        //如果用户未授权定位，则提醒用户去
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse{
            //引导用户去开启摄像头权限
            alert(alertTitle: NSLocalizedString("地图功能需要取得定位权限才能正常工作", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("就不给你权限", comment: ""), rightButtonTitle: NSLocalizedString("这就去设置", comment: ""), leftClosure: nil, rightClosure: {
                let settingURL: URL = URL(string: UIApplicationOpenSettingsURLString)!
                if UIApplication.shared.canOpenURL(settingURL){
                    UIApplication.shared.openURL(settingURL)
                }
            }, presentComplition: nil)
        }
        //已经授权定位权限，进入地图视图
        else{
            viewController?.tabBarItem.image = UIImage(named: "mapItemSeleced")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            let mapVC: MapController = MapController()
            mapVC.umengViewName = "2D地图"
            present(mapVC, animated: true, completion: nil)
            //视图退出时，将tabBarItem的图片还原
            mapVC.dismissClosure = {
                viewController?.tabBarItem.image = UIImage(named: "mapItem")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            }
        }
    }
    
    //MARK: 进入AR界面
    fileprivate func enterARVC(viewController: UIViewController){
        // 请求摄像头权限
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { [weak self](result: Bool) in
            DispatchQueue.main.async {
                if result == true {
                    //更改item的icon
                    viewController.tabBarItem.image = UIImage(named: "scanItemSelected")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                    let arVC: EasyARVC = EasyARVC()
                    arVC.umengViewName = "AR"
                    self?.present(arVC, animated: true, completion: nil)
                    //视图退出时，将tabBarItem的图片还原
                    arVC.dismissClosure = {
                        viewController.tabBarItem.image = UIImage(named: "scanItem")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                    }
                }else{
                    //引导用户去开启摄像头权限
                    self?.alert(alertTitle: NSLocalizedString("AR功能取得摄像头权限才能正常工作", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("就不给你权限", comment: ""), rightButtonTitle: NSLocalizedString("这就去设置", comment: ""), leftClosure: nil, rightClosure: {
                        let settingURL: URL = URL(string: UIApplicationOpenSettingsURLString)!
                        if UIApplication.shared.canOpenURL(settingURL){
                            UIApplication.shared.openURL(settingURL)
                        }
                    }, presentComplition: nil)
                }
            }
        }
    }

    //MARK: 进入明信片制作界面
    fileprivate func enterMarkCardVC(){
        // 请求照片权限
        PHPhotoLibrary.requestAuthorization { [weak self](status: PHAuthorizationStatus) in
            if status == PHAuthorizationStatus.authorized{
                DispatchQueue.main.async {
                    let mvc: MUViewController = MUViewController()
                    mvc.umengViewName = "明信片制作界面"
                    mvc.backgroundImage = self?.getScreenImage()
                    let nav: UINavigationController = UINavigationController(rootViewController: mvc)
                    self?.present(nav, animated: false, completion: nil)
                }
            }else{
                //引导用户去开启照片权限
                self?.alert(alertTitle: NSLocalizedString("时记需要获得相册权限才能正常工作哦", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("就不给你权限", comment: ""), rightButtonTitle: NSLocalizedString("这就去设置", comment: ""), leftClosure: nil, rightClosure: {
                    let settingURL: URL = URL(string: UIApplicationOpenSettingsURLString)!
                    if UIApplication.shared.canOpenURL(settingURL){
                        UIApplication.shared.openURL(settingURL)
                    }
                }, presentComplition: nil)
            }
        }
    }
    
    //MARK: 进入发布模块
    fileprivate func enterReleaseModel(){
        //判断相机是否可用
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) == true{
            
            let previewVC: MapPreviewController = MapPreviewController()
            previewVC.backgroundImage = getScreenImage()
            previewVC.umengViewName = "地图预览"
            let nav: UINavigationController = UINavigationController(rootViewController: previewVC)
            present(nav, animated: false, completion: nil)
        }else{
            alert(alertTitle: nil, messageString: NSLocalizedString("相机不可用", comment: ""), leftButtonTitle: NSLocalizedString("好的", comment: ""))//提醒用户相机不可用
        }
    }
    
    //MARK: 点击中间按钮的事件
    fileprivate func touchMiddleButton(){
        
        releaseView = BaseTabbarView()
        self.view.addSubview(releaseView!)
        releaseView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(screenHeight)
        }
        releaseView?.dismissCompleteClosure = {[weak self] _ in
            self?.releaseView = nil
        }
        
        //点击发布到地图
        releaseView?.touchMapButtonClosure = {[weak self] _ in
//            if isTestUser() == true{
//                self?.enterReleaseModel()
//            }else{
//                self?.alert(alertTitle: NSLocalizedString("功能开发中\n敬请期待", comment: ""))
//            }
            self?.enterReleaseModel()
        }
        //点击发布到社区
        releaseView?.touchCommunityButtonClosure = {[weak self] _ in
            self?.enterMarkCardVC()
        }
        //点击商城按钮
        releaseView?.touchMarketButton = {[weak self] _ in
            let vc: StoreMainController = StoreMainController()
            let nav: UINavigationController = UINavigationController(rootViewController: vc)
            self?.present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入预览界面之前，先截图
    fileprivate func getScreenImage()->UIImage?{
        //绘制背景
        let scale = CGFloat(1748.0 / 296.0 * 1.0)
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, scale);
        //背景
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    //MARK: 创建提示框
    /**
     创建提示框
     
     - parameter title:      标题
     - parameter message:    信息
     - parameter leftTitle:  左按钮标题 默认 字符串 “OK” or “好的”
     - parameter rightTitle: 右按钮标题
     - parameter left:       左按钮点击事件
     - parameter right:      右按钮点击事件
     - parameter complition: 提示框弹出完成之后的闭包
     */
    func alert(alertTitle title: String?, messageString message: String? = nil, leftButtonTitle leftTitle: String = NSLocalizedString("好的", comment: ""), rightButtonTitle rightTitle: String? = nil, leftClosure left: (()->())? = nil, rightClosure right: (()->())? = nil, presentComplition complition: (()->())? = nil){
        
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let leftAc: UIAlertAction = UIAlertAction(title: leftTitle, style: .cancel) { (_) in
            left?()
        }// 左按钮
        ac.addAction(leftAc)
        
        if (rightTitle != nil) {
            let rightAc: UIAlertAction = UIAlertAction(title: rightTitle, style: .default, handler: { (_) in
                right?()
            })//右按钮
            ac.addAction(rightAc)
        }
        
        present(ac, animated: true) {
            complition?()
        }//提示框弹出结束后的事件
    }
}
