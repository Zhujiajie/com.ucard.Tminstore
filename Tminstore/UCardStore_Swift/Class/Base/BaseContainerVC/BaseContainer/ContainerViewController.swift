//
//  ContainerViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: App主界面的控制器，放置地图控制器和个人控制器
import UIKit
import SnapKit

class ContainerViewController: BaseViewController {

    /// 个人
    fileprivate var personVC: PNewMainController?
    /// 地图
    fileprivate var storeVC: StoreMainController?
    /// 高斯模糊视图
    fileprivate var visualView: BaseVisualEffectView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setControllers()
        closures()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: 设置控制器
    fileprivate func setControllers(){
        // tab控制器
        storeVC = StoreMainController()
        //
        let timeNav: BaseNavigationController = BaseNavigationController(rootViewController: storeVC!)
        addChildViewController(timeNav)
        view.addSubview(timeNav.view)
        timeNav.didMove(toParentViewController: self)
        
        personVC = PNewMainController()
        addChildViewController(personVC!)
        view.addSubview(personVC!.view)
        personVC?.didMove(toParentViewController: self)
        personVC?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.bottom.equalTo(view)
            make.right.equalTo(view).offset(-screenWidth)
            make.width.equalTo(350/baseWidth)
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击视频按钮的事件
        storeVC?.touchVideoButtonClosure = {[weak self] _ in
            self?.enterARCardVC(isVideo: true)
        }
        
        //MARK: 点击图片按钮的事件
        storeVC?.touchImageButtonClosure = {[weak self] _ in
            self?.enterARCardVC(isVideo: false)
        }
        
        //MARK: 点击个人按钮的事件
        storeVC?.touchLeftBarButtonItemClosure = {[weak self] _ in
            if getUserLoginStatus() {
                doInBackground(inBackground: {
                    self?.personVC?.getUserInfo()//获取个人信息
                })
            }
            self?.setVisualView()
            self?.personVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
                make.right.equalTo((self?.view)!).offset(-102/baseWidth)
            })
            autoLayoutAnimation(view: (self?.view)!)
        }
        
        //MARK: 点击设置按钮，进入设置界面
        personVC?.touchSetButtonClosure = {[weak self] _ in
            if getUserLoginStatus() {
                self?.enterSettingVC()
            }else{
                self?.enterLoginVCAndGetUserInfo()
            }
        }
        
        //MARK: 点击用户指南按钮, 进入教程页面
        personVC?.touchTutorialButtonClosure = {[weak self] _ in
            self?.enterStrategyWebview()
        }
        
        //MARK: 点击了tableView的Cell
        personVC?.selectTableView = {[weak self] (indexPath: IndexPath) in
            if getUserLoginStatus() == false {
                self?.enterLoginVCAndGetUserInfo()
                return
            }
            switch indexPath.row {
            case 0:
                self?.enterOrderVC()
            case 1:
                self?.enterUserPhotoVC()
            case 2:
                self?.enterMessageVC()
            default:
                self?.enterCouponVC()
            }
        }
        
        //MARK: 发现未读消息
        personVC?.getUnreadMessageNumber = { [weak self] (count: Int) in
            self?.storeVC?.setMessageStatus(hasNewMessage: count != 0)
        }
        
        //MARK: 点击了用户头像，去更换头像
        personVC?.tapUserPortraitView = {[weak self] (userPortrait: UIImage?) in
            if getUserLoginStatus() == false {
                self?.enterLoginVCAndGetUserInfo()
                return
            }
            self?.enterChangePortraitVC(userPortrait: userPortrait)
        }
        
//        //MARK: 点击好友, 关注或粉丝按钮
//        personVC?.touchFriendsFollowFansButton = {[weak self] (type: UserOtherInfoType) in
//            self?.enterFriendsFollowFansVC(type: type)
//        }
    }
    
    //MARK: 设置高斯模糊视图
    fileprivate func setVisualView(){
        
        visualView = BaseVisualEffectView.noEffectVisualView()
        view.insertSubview(visualView!, belowSubview: personVC!.view)
        visualView?.frame = view.bounds
        visualView?.appearLightEffectAnimation()
        
        //MARK: 点击高斯模糊视图，隐藏个人视图
        visualView?.tapClosure = {[weak self] _ in
            self?.personVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
                make.right.equalTo((self?.view)!).offset(-screenWidth)
            })
            autoLayoutAnimation(view: (self?.view)!)
        }
    }
    
    //MARK: mapView的dismiss方法
    /// mapView的dismiss方法
    func mapViewDismiss(){
//        mapVC?.dismiss(animated: true, completion: nil)
    }
    
    //MARK: 进入设置界面
    fileprivate func enterSettingVC(){
        if personVC?.userInfoModel != nil{
            let settingVC: PSettingController = PSettingController()
            settingVC.umengViewName = "设置"
            settingVC.userInfo = personVC!.userInfoModel!
            let nav: BaseNavigationController = BaseNavigationController(rootViewController: settingVC)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入时光圈界面
    fileprivate func enterMapVC(){
        if personVC?.userInfoModel != nil{
            let vc: PNewMapController = PNewMapController()
            vc.umengViewName = "我的时光圈"
            let nav: BaseNavigationController = BaseNavigationController(rootViewController: vc)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入订单界面
    fileprivate func enterOrderVC(){
        if personVC?.userInfoModel != nil{
            let vc: POrderController = POrderController()
            vc.umengViewName = "我的订单"
            let nav: BaseNavigationController = BaseNavigationController(rootViewController: vc)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    //MARK: 进入优惠券及界面
    fileprivate func enterCouponVC(){
        if personVC?.userInfoModel != nil{
            
            let vc: MUCouponMainViewController = MUCouponMainViewController()
            vc.umengViewName = "我的钱包"
            let nav: UINavigationController = UINavigationController(rootViewController: vc)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
            
//            let couponVC: MUCouponVC = MUCouponVC()
//            couponVC.canPay = false
//            couponVC.umengViewName = "查看优惠券"
//            couponVC.useDismiss = true
//            let nav: BaseNavigationController = BaseNavigationController(rootViewController: couponVC)
//            nav.transitioningDelegate = self
//            present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入消息列表
    fileprivate func enterMessageVC(){
        if personVC?.userInfoModel != nil{
            let messageVC: MessageController = MessageController()
            messageVC.umengViewName = "消息列表"
            let nav: BaseNavigationController = BaseNavigationController(rootViewController: messageVC)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入更换头像的界面
    fileprivate func enterChangePortraitVC(userPortrait: UIImage?){
        if personVC?.userInfoModel != nil{
            let pcVC: PChangePortraitVC = PChangePortraitVC()
            pcVC.userPortrait = userPortrait
            pcVC.umengViewName = "更换头像"
            pcVC.canUseDismiss = true
            let nav: BaseNavigationController = BaseNavigationController(rootViewController: pcVC)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入用户录屏信息的界面
    fileprivate func enterUserRecordInfoVC(){
        if personVC?.userInfoModel != nil{
            let vc: PRecordController = PRecordController()
            vc.umengViewName = "我的录屏"
            let nav: BaseNavigationController = BaseNavigationController(rootViewController: vc)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入用户图库
    fileprivate func enterUserPhotoVC(){
        if personVC?.userInfoModel != nil{
            let vc: PPhotoController = PPhotoController()
            vc.umengViewName = "我的图库"
            let nav: BaseNavigationController = BaseNavigationController(rootViewController: vc)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入教程网页
    fileprivate func enterStrategyWebview(){
        let webView: BaseWebView = BaseWebView()
        webView.canUsePopToDismiss = false
        webView.urlString = "http://ucardstorevideo.b0.upaiyun.com/iOSImages/strategy.html"
        let nav: BaseNavigationController = BaseNavigationController(rootViewController: webView)
        nav.transitioningDelegate = self
        present(nav, animated: true, completion: nil)
    }
    
    //MARK: 进入好友, 关注或粉丝列表
    fileprivate func enterFriendsFollowFansVC(type: UserOtherInfoType){
        if personVC?.userInfoModel != nil{
            let vc: PUserOtherController = PUserOtherController()
            vc.type = type
            switch type {
            case UserOtherInfoType.friends:
                vc.umengViewName = "好友列表"
            case UserOtherInfoType.follows:
                vc.umengViewName = "关注列表"
            default:
                vc.umengViewName = "粉丝列表"
            }
            let nav: BaseNavigationController = BaseNavigationController(rootViewController: vc)
            nav.transitioningDelegate = self
            present(nav, animated: true, completion: nil)
        }
    }
    
    //MARK: 进入AR名片界面
    fileprivate func enterARCardVC(isVideo: Bool){
        let arVC: ARCardViewController = ARCardViewController()
        arVC.isVideo = isVideo
        present(arVC, animated: true, completion: nil)
    }
    
    //MARK: 进入登录界面, 完成登录之后, 刷新用户数据
    fileprivate func enterLoginVCAndGetUserInfo(){
        enterLoginView(loginSuccessClosure: {[weak self] _ in
            self?.personVC?.getUserInfo()//获取个人信息
        })
    }
}

extension ContainerViewController: UIViewControllerTransitioningDelegate{
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        interactive.addGesture(presented)
        animator.presenting = true
        return animator
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.presenting = false
        return animator
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactive.isInteracting ? interactive: nil
    }
}
