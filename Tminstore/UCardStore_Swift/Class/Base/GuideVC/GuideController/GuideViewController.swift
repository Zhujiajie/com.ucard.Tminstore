//
//  GuideViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/3.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import ChameleonFramework

class GuideViewController: BaseViewController {

    /// 是否来自关于界面
    var comeFromAboutView: Bool = false
    
    fileprivate var collectionView: GuideCollectionView?
    fileprivate var pageControl: UIPageControl?
    fileprivate var guideLabel01: GuideLabel?
    fileprivate var guideLabel02: GuideLabel?
    fileprivate var startButton: BaseButton?
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setStartButton()
        setUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView?.scrollToItem(at: IndexPath.init(item: 900, section: 0), at: UICollectionViewScrollPosition.left, animated: false)
        setTitle(index: 0)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        collectionView = GuideCollectionView()
        view.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(101/baseHeight)
            make.left.right.equalTo(view)
            make.height.equalTo(250/baseHeight)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        //标题
        guideLabel01 = GuideLabel()
        guideLabel01?.font = UIFont.systemFont(ofSize: 18)
        guideLabel01?.textColor = UIColor(hexString: "828282")
        guideLabel01?.isTitle = true
        view.addSubview(guideLabel01!)
        guideLabel01?.snp.makeConstraints({ (make) in
            make.top.equalTo(collectionView!.snp.bottom).offset(40/baseHeight)
            make.centerX.equalTo(view)
        })
        
        //副标题
        guideLabel02 = GuideLabel()
        guideLabel02?.font = UIFont.systemFont(ofSize: 14)
        guideLabel02?.textColor = UIColor(hexString: "989898")
        guideLabel02?.isTitle = false
        view.addSubview(guideLabel02!)
        guideLabel02?.snp.makeConstraints({ (make) in
            make.top.equalTo(guideLabel01!.snp.bottom).offset(2/baseHeight)
            make.centerX.equalTo(view)
        })
        
        pageControl = UIPageControl()
        pageControl?.numberOfPages = 3
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D8D8D8")
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "7E7E7E")
        view.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(guideLabel02!.snp.bottom).offset(10/baseHeight)
            make.centerX.equalTo(view)
        })
    }
    
    //MARK: 设置开始按钮
    fileprivate func setStartButton(){
        
        //实现阴影的视图
        let shadowView: UIView = UIView()
        view.addSubview(shadowView)
        shadowView.snp.makeConstraints { (make) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-69/baseHeight)
            make.size.equalTo(CGSize(width: 160/baseWidth, height: 50/baseWidth))
        }
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowRadius = 25/baseWidth
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 8)
        shadowView.layer.shadowOpacity = 0.4
        
        //添加圆角的按钮
        startButton = BaseButton.normalTitleButton(title: NSLocalizedString("立即体验", comment: ""), andTextFont: UIFont.systemFont(ofSize: 18), andTextColorHexString: "FFFFFF", andBackgroundColorHexString: "505050", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: 25/baseWidth)
        shadowView.addSubview(startButton!)
        startButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottomMargin.equalTo(shadowView)
            make.centerX.equalTo(view)
            make.size.equalTo(shadowView)
        })
        startButton?.touchButtonClosure = {[weak self] _ in
            self?.touchStartButton()
        }
    }
    
    //MARK: 点击开始体验按钮，退出视图
    fileprivate func touchStartButton(){
        
        if comeFromAboutView == true{
            dismissSelf()
        }else{
            _ = app.window?.subviews.map({ (view: UIView) in
                view.removeFromSuperview()
            })
            //判断用户是否已登录
            if getUserLoginStatus() == false{
                app.configLoginVC()
            }else{
                app.setMainVC()//创建主页
            }
        }
    }
    
    //MARK: 设置标题
    fileprivate func setTitle(index: Int){
        guideLabel01?.index = index
        guideLabel02?.index = index
        pageControl?.currentPage = index
    }
}

extension GuideViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1500
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: GuideCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.GuideCollectionViewCellReuseIdentifier, for: indexPath) as! GuideCollectionViewCell
        cell.index = indexPath.item % 3
        return cell
    }
    
    //MARK: 停止滚动时，改变下方的文字
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index: Int = Int(scrollView.contentOffset.x / screenWidth) % 3
        setTitle(index: index)
    }
}
