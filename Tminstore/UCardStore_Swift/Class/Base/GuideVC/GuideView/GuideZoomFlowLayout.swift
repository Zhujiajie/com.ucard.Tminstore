//
//  GuideZoomFlowLayout.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/3.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

let guideCollectionItemSize: CGSize = CGSize(width: 258/baseWidth, height: 340/baseHeight)

class GuideZoomFlowLayout: UICollectionViewFlowLayout {

    override init() {
        super.init()
        itemSize = guideCollectionItemSize
        minimumLineSpacing = 30/baseWidth
        scrollDirection = UICollectionViewScrollDirection.horizontal
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        collectionView?.decelerationRate = UIScrollViewDecelerationRateNormal
    }
    
    // 滑动停止时，自动滚到合适的位置
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        let lastRect: CGRect = CGRect(x: proposedContentOffset.x, y: proposedContentOffset.y, width: self.collectionView!.frame.width, height: self.collectionView!.frame.height)
        
        // 获得collectionView在中央的X值
        let centerX: CGFloat = proposedContentOffset.x + self.collectionView!.frame.width * 0.5
        // 获得该范围内的所有属性
        guard let array: [UICollectionViewLayoutAttributes] = self.layoutAttributesForElements(in: lastRect) else { return CGPoint(x: proposedContentOffset.x, y: proposedContentOffset.y) }
        
        // 需要移动的距离
        var adjustOffsetX: CGFloat = CGFloat.infinity
        for att: UICollectionViewLayoutAttributes in array{
            if abs(att.center.x - centerX) < abs(adjustOffsetX){
                adjustOffsetX = att.center.x - centerX
            }
        }
        return CGPoint(x: proposedContentOffset.x + adjustOffsetX, y: proposedContentOffset.y)
    }
    
    /** 有效距离:当item的中间x距离屏幕的中间x在HMActiveDistance以内,才会开始放大, 其它情况都是缩小 */
    let ActiveDistance: CGFloat = 60/baseWidth;
    /** 缩放因素: 值越大, item就会越大 */
    let ScaleFactor: CGFloat = 0.1;
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        // 获取cell对应的attributes对象
        guard let superAttris: [UICollectionViewLayoutAttributes] = super.layoutAttributesForElements(in: rect) else {return nil}
        let arrayAttris: [UICollectionViewLayoutAttributes] = NSArray(array: superAttris, copyItems: true) as! [UICollectionViewLayoutAttributes]
        
        //可见的矩阵
        let visibleRect: CGRect = CGRect(x: self.collectionView!.contentOffset.x, y: self.collectionView!.contentOffset.y, width: self.collectionView!.frame.width, height: self.collectionView!.frame.height)
        
        for attributes: UICollectionViewLayoutAttributes in arrayAttris{
            
            // 不可见区域的attributes不变化
            if attributes.frame.intersects(visibleRect) == false{continue}
            
            let distance: CGFloat = visibleRect.midX - attributes.center.x//距离中点的距离
            let normalizedDistance = distance / ActiveDistance
            if abs(distance) < ActiveDistance{
                let zoom: CGFloat = 1 + ScaleFactor * (1 - abs(normalizedDistance))
                attributes.transform3D = CATransform3DMakeScale(zoom, zoom, 1.0)
                attributes.zIndex = 1
            }
        }
        
        return arrayAttris
    }
    
    // 当bounds发生改变时，需要重新布局
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
