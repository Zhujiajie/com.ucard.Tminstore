//
//  GuideCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/3.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class GuideCollectionViewCell: UICollectionViewCell {
    
    var index: Int = 0{
        didSet{
            setImages()
        }
    }
    
    /// imageView
    fileprivate var imageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = UIColor.white
        
        imageView = UIImageView()
        imageView?.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    //MARK: 设置图片
    fileprivate func setImages(){
        switch index {
        case 0:
            imageView?.sd_setImage(with: appAPIHelp.introImage01URL)
        case 1:
            imageView?.sd_setImage(with: appAPIHelp.introImage02URL)
        default:
            imageView?.sd_setImage(with: appAPIHelp.introImage03URL)
        }
        
    }
    
//    //MARK: 切换播放器的视频源
//    fileprivate func setVideoURL(){
//        let url: URL = Bundle.main.url(forResource: "guide0\(index)", withExtension: "mp4")!
//        player?.url = url
//        
//        if firstPlay == false{
//            firstPlay = true
//            if index == 0{
//                player?.playFromBeginning()
//            }
//        }
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
