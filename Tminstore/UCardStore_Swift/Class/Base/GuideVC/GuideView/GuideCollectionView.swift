//
//  GuideCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/3.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class GuideCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: screenWidth, height: 245/baseWidth)
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        
        isPagingEnabled = true
        register(GuideCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.GuideCollectionViewCellReuseIdentifier)
        showsHorizontalScrollIndicator = false//隐藏滚动指示器
        self.backgroundColor = UIColor.white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
