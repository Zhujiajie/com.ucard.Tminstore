//
//  GuideLabel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/4.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class GuideLabel: UILabel {
    
    /// 显示哪些文字
    var index: Int = 0{
        didSet{
            setAttributeString()
        }
    }
    
    /// 是否是标题
    var isTitle: Bool = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        textAlignment = NSTextAlignment.center
        numberOfLines = 1
    }
    
    /// 渐变设置label的文字内容
    fileprivate func setAttributeString(){
        
        var text: String
        if isTitle {
            switch index {
            case 0:
                text = NSLocalizedString("AR购物", comment: "")
            case 1:
                text = NSLocalizedString("扫描商品", comment: "")
            default:
                text = NSLocalizedString("上传作品", comment: "")
            }
        }else{
            switch index {
            case 0:
                text = NSLocalizedString("挑选商品添加AR视频", comment: "")
            case 1:
                text = NSLocalizedString("观看AR记忆", comment: "")
            default:
                text = NSLocalizedString("赚取RMB人人都是设计师", comment: "")
            }
        }
        
        
        UIView.transition(with: self, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: { 
            self.text = text
        }, completion: nil)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
