//
//  LaunchScreenView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class LaunchScreenView: UIView {

    /// 退出的闭包
    var dismissClosure: (()->())?
    
    /// 图片
    var imageView: UIImageView?
    /// 启动页跳转的网页地址
    var webViewURL: URL?
    /// 倒计时按钮
    var timeCountLabel: UILabel?
    /// 倒计时
    var timeCount: Int = 0
    /// 倒计时的计时器
    var countTimer: Timer?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        let tuple: (shouldShowLaunchScreen: Bool, imageURL: URL, webViewURL: URL?, timeCount: TimeInterval) = shouldShowLaunchScreen()
        
        if tuple.shouldShowLaunchScreen == true{
            launchScreenView(tuple: tuple)//动态的启动页
        }else{
            dismissSelf()
        }
//        else{
//            defaultLaunchScreen()//默认的启动页
//            //启动页显示2秒钟
//            Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(dismissSelf), userInfo: nil, repeats: false)
//            //添加渐变动画
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
//                self?.imageView?.alpha = 1
//                }, completion: nil)
//        }
    }
    
    //MARK: 设置默认的启动页
    fileprivate func defaultLaunchScreen(){
        //背景
        imageView = UIImageView()
        imageView?.sd_setImage(with: appAPIHelp.launchBackgroundImageURL())//加载远程的启动页背景图片
        imageView?.contentMode = UIViewContentMode.scaleToFill
        self.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        imageView?.alpha = 0
        
        //icon
        let launchIcon: UIImageView = UIImageView(image: UIImage(named: "launchIcon"))
        launchIcon.contentMode = UIViewContentMode.scaleAspectFit
        imageView?.addSubview(launchIcon)
        launchIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(88/baseHeight)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 81/baseWidth, height: 80/baseWidth))
        }
        //大标题
        let titleIcon: UIImageView = UIImageView(image: UIImage(named: "titleIcon"))
        launchIcon.contentMode = UIViewContentMode.scaleAspectFit
        imageView?.addSubview(titleIcon)
        titleIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(launchIcon.snp.bottom).offset(8/baseHeight)
            make.centerX.equalTo(launchIcon)
            make.size.equalTo(CGSize(width: 208/baseWidth, height: 45/baseWidth))
        }
        
        //副标题
        let subTitle1: UILabel = UILabel()
        subTitle1.textAlignment = NSTextAlignment.center
        
        //字体
        var font1: UIFont
        if #available(iOS 8.2, *) {
            font1 = UIFont.systemFont(ofSize: 14, weight: UIFontWeightSemibold)
        } else {
            font1 = UIFont.systemFont(ofSize: 14)
        }
        
        let attDic1: [String: Any] = [NSKernAttributeName: 10.9, NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: font1]//字间距为10.9
        let attStr1: NSAttributedString = NSAttributedString(string: NSLocalizedString(" 穿越时光分享记忆", comment: ""), attributes: attDic1)
        subTitle1.attributedText = attStr1
        
        imageView?.addSubview(subTitle1)
        subTitle1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(titleIcon.snp.bottom)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: screenWidth, height: 18))
        }
        
        //底部的文字
        let subTitle2: UILabel = UILabel()
        subTitle2.textAlignment = NSTextAlignment.center
        
        let attDic2: [String: Any] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: font1]
        let attStr2: NSAttributedString = NSAttributedString(string: NSLocalizedString("全球AR影像定制分享平台", comment: ""), attributes: attDic2)
        subTitle2.attributedText = attStr2
        
        imageView?.addSubview(subTitle2)
        subTitle2.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(imageView!).offset(-20/baseHeight)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: screenWidth, height: 21))
        }
    }
    
    //MARK: 设置动态的启动页
    fileprivate func launchScreenView(tuple: (shouldShowLaunchScreen: Bool, imageURL: URL, webViewURL: URL?, timeCount: TimeInterval)){
        
        //背景
        imageView = UIImageView()
        imageView?.contentMode = UIViewContentMode.scaleToFill
        self.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        imageView?.sd_setImage(with: tuple.imageURL)//设置图片
        
        // 点击进入webView
        if tuple.webViewURL != nil{
            webViewURL = tuple.webViewURL!
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(enterWebView(tap:)))
            imageView?.isUserInteractionEnabled = true
            imageView?.addGestureRecognizer(tap)
        }
        
        let dismissView: UIView = UIView()
        dismissView.backgroundColor = UIColor.clear
        imageView?.addSubview(dismissView)
        dismissView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(imageView!)
            make.right.equalTo(imageView!)
            make.size.equalTo(CGSize(width: 100/baseWidth, height: 100/baseWidth))
        }
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToDismiss(tap:)))
        dismissView.addGestureRecognizer(tap)
        
        //倒计时的Label
        timeCountLabel = UILabel()
        timeCountLabel?.textAlignment = NSTextAlignment.center
        timeCountLabel?.textColor = UIColor.white
        timeCountLabel?.backgroundColor = UIColor.init(white: 0.0, alpha: 0.3)
        dismissView.addSubview(timeCountLabel!)
        timeCountLabel?.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(dismissView)
            make.size.equalTo(CGSize(width: 70/baseWidth, height: 30/baseWidth))
        }
        timeCountLabel?.layer.cornerRadius = 8
        timeCountLabel?.layer.masksToBounds = true
        
        timeCount = Int(tuple.timeCount)
        
        timeCountLabel?.text = "\(timeCount)" + "  " + NSLocalizedString("跳过", comment: "")
        
        countTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(timeCountTimer(timer:)), userInfo: nil, repeats: true)
        RunLoop.current.add(countTimer!, forMode: RunLoopMode.defaultRunLoopMode)   
    }
    
    
    //进入app
    func dismissSelf(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 0.0
            }, completion: {[weak self] _ in
                self?.removeFromSuperview()
                self?.dismissClosure?()
        })
    }
    
    //MARK: 倒计时
    func timeCountTimer(timer: Timer) {
        timeCount -= 1
        timeCountLabel?.text = "\(timeCount)" + "  " + NSLocalizedString("跳过", comment: "")
        if timeCount == 0{
            countTimer?.invalidate()
            dismissSelf()
        }
    }
    
    //MARK: 点击倒计时按钮的事件
    func tapToDismiss(tap: UITapGestureRecognizer){
        if countTimer != nil{
            countTimer?.invalidate()
        }
        dismissSelf()
    }
    
    //MARK: 进入webView
    /// 进入webView
    ///
    /// - Parameter url: URL
    func enterWebView(tap: UITapGestureRecognizer){
        countTimer?.invalidate()
        
        let webView: BaseWebView = BaseWebView()
        webView.urlString = webViewURL?.absoluteString
        let nav: UINavigationController = UINavigationController(rootViewController: webView)
        topViewController().present(nav, animated: true, completion: {[weak self] _ in
            self?.removeFromSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
