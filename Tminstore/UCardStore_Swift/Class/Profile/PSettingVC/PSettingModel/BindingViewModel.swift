//
//  BindingViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BindingViewModel: BaseViewModel {

    fileprivate let dataModel: BindingDataModel = BindingDataModel()
    /// 用于验证邮箱和手机号码的合法性
    fileprivate let loginDateModel: PLoginDataModel = PLoginDataModel()
    
    /// 登录错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 邮箱不正确的闭包
    var emailErrorClosure: (()->())?
    
    /// 手机号码不正确的闭包
    var phoneNumberErrorClosure: (()->())?
    
    /// 获取验证码失败
    var requestVerificationCodeFailed: (()->())?
    
    /// 获取验证码成功
    var requestVerificationCodeSuccess: (()->())?
    
    /// 验证验证成功的闭包
    var verifySuccess: (()->())?
    
    //MARK: 绑定邮箱，请求验证码
    /// 绑定邮箱，请求验证码
    ///
    /// - Parameter email: 邮箱
    func bindingEmailRequestCode(email: String?){
        
        //检查邮箱地址的合法性
        if loginDateModel.checkEmailAddress(emailString: email) == false{
            emailErrorClosure?()
            return
        }
        showSVProgress(title: nil)
        dataModel.bingdingEmailRequestCode(email: email!, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.requestVerificationCodeSuccess?()
            }else{
                self?.requestVerificationCodeFailed?()
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self] (error: Error?) in
                self?.requestVerificationCodeFailed?()
                self?.errorClosure?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 绑定邮箱，验证邮箱验证码
    /// 绑定邮箱，验证邮箱验证码
    ///
    /// - Parameters:
    ///   - email: 邮箱
    ///   - code: 验证码
    func bindingEmailVerificationCode(email: String?, andCode code: String){
        // 验证邮箱
        if loginDateModel.checkEmailAddress(emailString: email) == false{
            emailErrorClosure?()
            return
        }
        showSVProgress(title: nil)
        dataModel.bingdingEmailVerification(email: email!, andVerificationCode: code, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.verifySuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
            }, completionClosure: {[weak self] _ in
               self?.dismissSVProgress()
        })
    }
    
    //MARK: 绑定手机号，请求验证码
    /// 绑定手机号，请求验证码
    ///
    /// - Parameter phoneNumber: 手机号码
    func bingdingPhoneRequestCode(phoneNumber: String?){
        //验证手机号
        if loginDateModel.checkPhoneNumber(phoneNumber: phoneNumber) == false{
            phoneNumberErrorClosure?()
            return
        }
        
        showSVProgress(title: nil)
        dataModel.bindingPhoneRequestCode(phoneNumber: phoneNumber!, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.requestVerificationCodeSuccess?()
            }else{
                self?.requestVerificationCodeFailed?()
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self] (error: Error?) in
                self?.requestVerificationCodeFailed?()
                self?.errorClosure?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 绑定手机-验证手机验证码
    /// 绑定手机-验证手机验证码
    ///
    /// - Parameters:
    ///   - phoneNumber: 手机号码
    ///   - code: 验证码
    func bindingPhoneNumberVerification(phoneNumber: String?, andCode code: String){
        if loginDateModel.checkPhoneNumber(phoneNumber: phoneNumber) == false{
            phoneNumberErrorClosure?()
            return
        }
        
        showSVProgress(title: nil)
        
        dataModel.bindingPhoneVerification(phoneNumber: phoneNumber!, andVeirificationCode: code, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.verifySuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
}
