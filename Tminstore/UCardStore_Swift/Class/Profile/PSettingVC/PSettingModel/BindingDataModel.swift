//
//  BindingDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class BindingDataModel: BaseDataModel {

    //MARK: 绑定邮箱-请求验证码
    /// 绑定邮箱-请求验证码
    ///
    /// - Parameters:
    ///   - email: 邮箱
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func bingdingEmailRequestCode(email: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "email": email
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.bingingEmailRequestCodeAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 绑定邮箱-验证邮箱验证码的请求
    /// 绑定邮箱-验证邮箱验证码的请求
    ///
    /// - Parameters:
    ///   - email: 邮箱
    ///   - code: 邮箱验证码
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func bingdingEmailVerification(email: String, andVerificationCode code: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "email": email,
            "emailCode": code
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.bingingEmailVerificationAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 绑定手机，请求手机验证码的接口
    /// 绑定手机，请求手机验证码的接口
    ///
    /// - Parameters:
    ///   - phoneNumber: 手机号码
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func bindingPhoneRequestCode(phoneNumber: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "phone": phoneNumber
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.bingdingPhoneRequestCodeAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 绑定手机-验证手机验证码的请求
    /// 绑定手机-验证手机验证码的请求
    ///
    /// - Parameters:
    ///   - phoneNumber: 手机号码
    ///   - code: 验证码
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func bindingPhoneVerification(phoneNumber: String, andVeirificationCode code: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "phone": phoneNumber,
            "phoneCode": code
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.bingdingPhoneVerificationAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
}
