//
//  ProfileDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/11.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Instabug

class ProfileDataModel: BaseDataModel {
    
    //MARK: 注销的请求
    /// 注销的请求
    func logout(){
        let parameter: [String: Any] = [
            "token": getUserToken()
            ] as [String: Any]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.LogoutAPI, ParametersDictionary: parameter, successClosure: nil, failureClourse: nil)//发送退出登陆的请求
        
        MobClick.profileSignOff()//用户退出登录，清空友盟的账号信息
        
        //清空所有数据
        if let appDomain: String = Bundle.main.bundleIdentifier{
            UserDefaults.standard.removePersistentDomain(forName: appDomain)
        }
        Instabug.logOut()//注销Instabug
//        saveUserID(userId: "")//清空用户的id
//        saveUserName(name: nil)//清空用户昵称
//        setLoginStatus(loginStatus: false)//设置app为未登陆状态
//        setUserHasLoadAPP(false)//设置用户未登陆过app
//        setGuideVCStatus(result: false)//重置引导页
    }
    
    
    //MARK: 提交用户信息
    /// 提交用户信息
    ///
    /// - Parameters:
    ///   - userInfo: 用户信息
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func putUsetInfo(userInfo: UserInfoModel, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        let parameters: [String: Any] = [
            "nickName": userInfo.nickName,
            "gender": userInfo.gender.rawValue,
            "firstName": userInfo.firstName,
            "lastName": userInfo.lastName,
            "remark": "",
            "birthDate": "",
            "token": getUserToken()
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.putUserInfoAPI, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 获取用户信息
    /// 获取用户信息
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func getUserInfo(success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameter: [String: Any] = [
            "token": getUserToken()
            ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.requestUserInfo, ParametersDictionary: parameter, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) {
            completion?()
        }
    }
}
