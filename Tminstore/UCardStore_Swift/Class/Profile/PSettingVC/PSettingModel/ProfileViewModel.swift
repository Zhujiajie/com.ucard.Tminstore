//
//  ProfileViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/11.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class ProfileViewModel: BaseViewModel {

    fileprivate let dataModel = ProfileDataModel()
    
    /// 提交用户数据成功的闭包
    var updateUserInfoSuccess: (()->())?
    
    /// 成功获取用户信息
    var getUsetInfoSuccess: ((_ userInfo: UserInfoModel)->())?
    /// 网络错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    //MARK: 注销的请求
    /// 注销的请求
    func logout(){
        dataModel.logout()
    }
    
    //MARK: 请求用户的个人信息
    /// 请求用户的个人信息
    func requestUserInfo(){
        dataModel.getUserInfo(success: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let user: [String: Any] = data["user"] as! [String: Any]
                
                let userInfo: UserInfoModel = UserInfoModel.initFromDic(dic: user)
                
                let userID: String      = user["memberCode"] as! String
                let userName: String    = user["nickName"] as! String
                
                saveUserID(userId: userID)
                saveUserName(name: userName)
                
                self?.getUsetInfoSuccess?(userInfo)
            }
        }, failureClourse: nil, completionClosure: nil)
    }
    
    //MARK: 提交用户信息
    /// 提交用户信息
    ///
    /// - Parameter userInfo: UserInfoModel
    func updateUserInfo(userInfo: UserInfoModel){
        showSVProgress(title: nil)
        dataModel.putUsetInfo(userInfo: userInfo, successClosure: { [weak self](result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.updateUserInfoSuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
            self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
}
