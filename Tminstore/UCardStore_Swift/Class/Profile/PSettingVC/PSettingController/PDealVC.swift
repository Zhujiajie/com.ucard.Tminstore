//
//  PDealVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/12.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:--------------用户隐私协议的界面---------------

import UIKit
import WebKit
import SnapKit

class PDealVC: BaseViewController {
    
    var webView: WKWebView?
    var progressView: UIProgressView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()//设置导航栏
        setUI()//设置UI
        webView?.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        let request = URLRequest(url: appAPIHelp.privateURL())
        _ = webView?.load(request)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationTitle(NSLocalizedString("隐私协议", comment: ""))
        setNavigationBarLeftButtonWithImageName("backArrow")
    }
    
    
    //MARK: 设置UI
    fileprivate func setUI(){
        webView = WKWebView()
        view.addSubview(webView!)
        webView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        setProcessView()
    }
    
    //MARK: 设置进度条
    fileprivate func setProcessView(){
        progressView = UIProgressView()
        view.addSubview(progressView!)
        progressView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view)
            make.left.equalTo(view)
            make.right.equalTo(view)
        })
        progressView?.trackTintColor = UIColor.clear
    }
    
    
    //MARK: 导航栏左按钮的点击事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    deinit {
        webView?.removeObserver(self, forKeyPath: "estimatedProgress")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            if let progress = webView?.estimatedProgress{
                progressView?.setProgress(Float(progress), animated: true)
                if progress == 1.0 {
                    progressView?.isHidden = true
                }
            }
        }
    }
}
