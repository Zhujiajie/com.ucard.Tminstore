//
//  PVersionVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/10.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: ---------------查看版本号的页面----------------

import UIKit
import SnapKit

class PVersionVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        //获取软件版本号
        let versionLabel: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20))
        let systemVersion: String? = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let buildVersion: String? = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
        
        let versionText: String = NSLocalizedString("版本", comment: "版本")
        versionLabel.text = "\(versionText) \(systemVersion!) (Build \(buildVersion!))"
        versionLabel.textColor = UIColor.darkGray
        versionLabel.textAlignment = NSTextAlignment.center
        
        view.addSubview(versionLabel)
        
        versionLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(view)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        setNavigationTitle(NSLocalizedString("版本信息", comment: ""))//设置导航栏标题
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
    }

}
