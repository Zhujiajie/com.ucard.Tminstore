//
//  ProfileVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/11.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:-------------更改个人信息的页面-----------------

import UIKit
import SnapKit
import SDWebImage


class ProfileVC: BaseViewController {

    fileprivate let viewModel: ProfileViewModel = ProfileViewModel()
    
    var userInfo: UserInfoModel?
    var userPortrait: UIImage?
    
    var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setTableView()
        
        //MARK: 获取用户信息成功
        viewModel.getUsetInfoSuccess = {[weak self] (userInfo: UserInfoModel) in
            self?.userInfo = userInfo
            self?.tableView?.reloadData()
        }
        
        //MARK: 网络出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        setNavigationTitle(NSLocalizedString("我的信息", comment: ""))//设置导航栏标题
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
    }

    //MARK: 设置tableView
    fileprivate func setTableView(){
        
        tableView = UITableView()
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.ProfileVCReuseIdentifier)
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        tableView?.tableFooterView = UIView()
        tableView?.estimatedRowHeight = 50
        tableView?.delegate = self
        tableView?.dataSource = self
    }
}

extension ProfileVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 74
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 6
    }
    
    //MARK: 设置cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: appReuseIdentifier.ProfileVCReuseIdentifier)
        
        cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        var titleText: String = ""
        var detailText: String = ""
        
        switch indexPath.row {
        case 0://头像
            
            titleText = NSLocalizedString("头像", comment: "")
            
            let imageView: UIImageView = UIImageView()
            imageView.layer.cornerRadius = 56/2
            imageView.layer.masksToBounds = true
            
            if let url: URL = userInfo?.userLogoURL  {
                imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "defaultLogo"), options: SDWebImageOptions.lowPriority, completed: { [weak self] (image: UIImage?, _, _, _) in
                    if image != nil{
                        self?.userPortrait = image
                    }
                })
            }else{
                imageView.image = #imageLiteral(resourceName: "defaultLogo")
                userPortrait = imageView.image
            }
            cell.accessoryView = imageView
            cell.accessoryView?.frame = CGRect(x: 0, y: 0, width: 56, height: 56)
        case 1://昵称
            
            titleText = NSLocalizedString("昵称", comment: "")
            detailText = userInfo!.nickName
            
        case 2://性别
            
            titleText = NSLocalizedString("性别", comment: "")
            switch userInfo!.gender {
            case UserGender.male:
                detailText = NSLocalizedString("男", comment: "")
            case UserGender.female:
                detailText = NSLocalizedString("女", comment: "")
            default:
                detailText = NSLocalizedString("未知", comment: "")
            }
            
        case 3://名字
            
            titleText = NSLocalizedString("名字", comment: "")
            detailText = userInfo!.firstName
            
        case 4://姓氏
            titleText = NSLocalizedString("姓氏", comment: "")
            detailText = userInfo!.lastName
            
        case 5://地址
            titleText = NSLocalizedString("地址", comment: "")
        default:
            break
        }
        
        cell.textLabel?.text = titleText
        cell.detailTextLabel?.text = detailText
        
        return cell
    }
    
    //MARK: 点击cell，进入编辑信息的界面
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case 0://编辑头像
            
            let proVC = PChangePortraitVC()
            proVC.userPortrait = userPortrait
            proVC.umengViewName = "更换头像"
            proVC.changePortraitSuccess = {[weak self] _ in
                self?.viewModel.requestUserInfo()
            }//更换头像成功，刷新用户数据
            navigationController?.pushViewController(proVC, animated: true)
            
        case 1://编辑昵称
            
            let nickVC = PNickNameVC()
            nickVC.nameType = 0
            nickVC.userInfo = userInfo
            nickVC.umengViewName = "修改昵称"
            nickVC.updateUserInfoSuccess = {[weak self] _ in
                self?.viewModel.requestUserInfo()
            }
            navigationController?.pushViewController(nickVC, animated: true)
            
        case 2://编辑性别
            
            let genderVC = PGenderVC()
            genderVC.userInfo = userInfo
            genderVC.umengViewName = "修改性别"
            genderVC.updateGenderSuccess = {[weak self] _ in
                self?.viewModel.requestUserInfo()
            }
            
            navigationController?.pushViewController(genderVC, animated: true)
            
        case 3://编辑名字
            let nickVC = PNickNameVC()
            nickVC.nameType = 1
            nickVC.userInfo = userInfo
            nickVC.updateUserInfoSuccess = {[weak self] _ in
                self?.viewModel.requestUserInfo()
            }
            navigationController?.pushViewController(nickVC, animated: true)
            
        case 4://编辑姓氏
            
            let nickVC = PNickNameVC()
            nickVC.nameType = 2
            nickVC.userInfo = userInfo
            nickVC.umengViewName = "修改姓名"
            nickVC.updateUserInfoSuccess = {[weak self] _ in
                self?.viewModel.requestUserInfo()
            }
            navigationController?.pushViewController(nickVC, animated: true)
            
        case 5://编辑地址
            
            let addressVC: MUAddressListController = MUAddressListController()
            addressVC.umengViewName = "修改用户地址"
            addressVC.isComeFromProfile = true
            navigationController?.pushViewController(addressVC, animated: true)
            
        default:
            break
        }
        
    }
}
