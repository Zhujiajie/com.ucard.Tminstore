//
//  AboutUsController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/16.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import Instabug

class AboutUsController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexString: "#F3F6F8")
        
        setNavigationTitle(NSLocalizedString("关于我们", comment: ""))
        setNavigationBarLeftButtonWithImageName("backArrow")
        
        setUI()
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        let upperContainerView: UIView = UIView()
        upperContainerView.backgroundColor = UIColor.white
        view.addSubview(upperContainerView)
        upperContainerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view).offset(5)
            make.left.right.equalTo(view)
            make.height.equalTo(176/baseWidth)
        }
        
        //logo和标题
        let iconImageView: UIImageView = UIImageView()
        iconImageView.contentMode = UIViewContentMode.scaleAspectFit
        iconImageView.image = UIImage(named: "logoAndTitle")
        upperContainerView.addSubview(iconImageView)
        iconImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(23/baseWidth)
            make.centerX.equalTo(upperContainerView)
            make.size.equalTo(CGSize(width: 118/baseWidth, height: 102/baseWidth))
        }
        //版本号
        let versionLabel: UILabel = UILabel()
        versionLabel.textColor = UIColor(hexString: "858585")
        versionLabel.textAlignment = NSTextAlignment.center
        if #available(iOS 8.2, *) {
            versionLabel.font = UIFont.systemFont(ofSize: 10, weight: UIFontWeightLight)
        } else {
            versionLabel.font = UIFont.systemFont(ofSize: 10)
        }
        //获取软件版本号
        let systemVersion: String? = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let buildVersion: String? = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String
        let versionText: String = NSLocalizedString("版本", comment: "版本")
        versionLabel.text = "\(versionText) \(systemVersion!) (Build \(buildVersion!))"
        view.addSubview(versionLabel)
        versionLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(iconImageView.snp.bottom).offset(21/baseWidth)
            make.centerX.equalTo(iconImageView)
            make.height.equalTo(12)
        }
        
        //底部的视图, 用于放置公司title
        let bottomContainerView: UIView = UIView()
        bottomContainerView.backgroundColor = UIColor.white
        view.addSubview(bottomContainerView)
        bottomContainerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.height.equalTo(123/baseWidth)
            make.left.right.bottom.equalTo(view)
        }
        
        //公司名字Label
        let companyLabel: UILabel = UILabel()
        companyLabel.textColor = UIColor(hexString: "7A7A7A")
        if #available(iOS 8.2, *) {
            companyLabel.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightLight)
        } else {
            companyLabel.font = UIFont.systemFont(ofSize: 12)
        }
        companyLabel.textAlignment = NSTextAlignment.center
        companyLabel.text = NSLocalizedString("上海市尤卡城信息科技有限责任公司", comment: "")
        bottomContainerView.addSubview(companyLabel)
        companyLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(bottomContainerView).offset(-31/baseHeight)
            make.centerX.equalTo(bottomContainerView)
        }
        
        //介绍App的Label
        let introLabel: BaseLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "3C3C3C", andTextAlignment: NSTextAlignment.center, andText: NSLocalizedString("你的AR时光店铺", comment: ""))
        bottomContainerView.addSubview(introLabel)
        introLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(bottomContainerView)
            make.bottom.equalTo(companyLabel.snp.top).offset(-10/baseWidth)
        }
        
        //字体
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "5F5F5F"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        
        
        //放置帮助与隐私 和 隐私条款按钮的View
        let centerContainerView: BaseView = BaseView.colorLine(colorString: "FFFFFF")
        view.addSubview(centerContainerView)
        centerContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(upperContainerView.snp.bottom).offset(5)
            make.bottom.equalTo(bottomContainerView.snp.top).offset(2)
            make.left.right.equalTo(view)
        }
        
        //帮助与反馈
        let feedBackButton: UIButton = UIButton()
        feedBackButton.backgroundColor = UIColor.white
        feedBackButton.setAttributedTitle(NSAttributedString(string: NSLocalizedString("帮助与反馈", comment: ""), attributes: attDic), for: UIControlState.normal)
        feedBackButton.addTarget(self, action: #selector(touchFeedBackButton(button:)), for: UIControlEvents.touchUpInside)
        view.addSubview(feedBackButton)
        feedBackButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(centerContainerView).offset(48/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(43/baseWidth)
        }
        
        //隐私条款按钮
        let privaceButton: UIButton = UIButton()
        privaceButton.backgroundColor = UIColor.white
        privaceButton.setAttributedTitle(NSAttributedString(string: NSLocalizedString("隐私条款", comment: ""), attributes: attDic), for: UIControlState.normal)
        privaceButton.addTarget(self, action: #selector(touchPrivacyButton(button:)), for: UIControlEvents.touchUpInside)
        view.addSubview(privaceButton)
        privaceButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(feedBackButton.snp.bottom).offset(18/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(43/baseWidth)
        }
    }
    
    //MARK: 点击教程按钮
    func touchIntroButton(button: UIButton){
        let guideVC: GuideViewController = GuideViewController()
        guideVC.comeFromAboutView = true
        guideVC.umengViewName = "引导页"
        guideVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        present(guideVC, animated: true, completion: nil)
    }
    
    //MARK: 点击反馈按钮
    func touchFeedBackButton(button: UIButton){
        Instabug.invoke()
    }
    
    //MARK: 点击隐私条款按钮
    func touchPrivacyButton(button: UIButton){
        let pdealVC: PDealVC = PDealVC()
        pdealVC.umengViewName = "隐私协议"
        navigationController?.pushViewController(pdealVC, animated: true)
    }
}
