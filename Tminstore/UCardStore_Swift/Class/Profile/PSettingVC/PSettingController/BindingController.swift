//
//  BindingController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/16.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import IQKeyboardManagerSwift

//MARK: 绑定手机或邮箱的页面
class BindingController: BaseViewController {

    fileprivate let viewModel: BindingViewModel = BindingViewModel()
    fileprivate let pMainViewModel: PMainViewModel = PMainViewModel()
    
    /// 用户信息模型
    var userInfo: UserInfoModel?
    /// 绑定成功的闭包
    var bindingSuccess: (()->())?
    /// 是否是绑定手机号
    var isBindingPhone: Bool = true
    /// 输入手机号或者邮箱的textField
    var phoneOrEmailTextField: PLoginViewTextField?
    /// 输入验证码的textField
    var codeTextField: PLoginViewTextField?
    /// 确认按钮
    var confirmButton: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        setPhoneOrEmailTextField()
        setCodeTextField()
        setConfirmButton()
        closures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transparentNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cancelTansparentNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationBarLeftButtonWithImageName("backArrow")
        var titleString: String
        if isBindingPhone == true{
            titleString = NSLocalizedString("手机号绑定", comment: "")
        }else{
            titleString = NSLocalizedString("邮箱绑定", comment: "")
        }
        setNavigationTitle(titleString)
        
        let whiteView: UIView = UIView(frame: CGRect(x: 0, y: -nav_statusHeight, width: screenWidth, height: nav_statusHeight))
        whiteView.backgroundColor = UIColor.white
        view.addSubview(whiteView)
    }
    
    //MARK: 设置输入手机号或者邮箱的textField
    fileprivate func setPhoneOrEmailTextField(){
        if isBindingPhone == true{
            phoneOrEmailTextField = PLoginViewTextField.phoneTextField()
            phoneOrEmailTextField?.placeholderText = NSLocalizedString("请输入绑定的手机号", comment: "")
        }else{
            phoneOrEmailTextField = PLoginViewTextField.emailTextField()
            phoneOrEmailTextField?.placeholderText = NSLocalizedString("请输入绑定的邮箱", comment: "")
        }
        phoneOrEmailTextField?.layer.borderColor = UIColor(hexString: "74CCD3").cgColor
        view.addSubview(phoneOrEmailTextField!)
        phoneOrEmailTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(84/baseHeight)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 318/baseWidth, height: 52))
        })
        phoneOrEmailTextField?.layer.cornerRadius = 3
        phoneOrEmailTextField?.layer.masksToBounds = true
    }
    
    //MARK: 设置输入验证码的textField
    fileprivate func setCodeTextField(){
        codeTextField = PLoginViewTextField.getVerificationTextField()
        codeTextField?.placeholderText = NSLocalizedString("请输入收到的验证码", comment: "")
        codeTextField?.layer.borderColor = UIColor(hexString: "74CCD3").cgColor
        view.addSubview(codeTextField!)
        codeTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(phoneOrEmailTextField!.snp.bottom).offset(18/baseHeight)
            make.centerX.equalTo(phoneOrEmailTextField!)
            make.size.equalTo(phoneOrEmailTextField!)
        })
        codeTextField?.layer.cornerRadius = 3
        codeTextField?.layer.masksToBounds = true
    }
    
    //MARK: 设置确认按钮
    fileprivate func setConfirmButton(){
        confirmButton = UIButton()
        confirmButton?.backgroundColor = UIColor(hexString: "60DBE8")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor.white]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("确认", comment: ""), attributes: attDic)
        confirmButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        view.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.top.equalTo(codeTextField!.snp.bottom).offset(37/baseHeight)
            make.size.equalTo(phoneOrEmailTextField!)
        })
        confirmButton?.layer.cornerRadius = 3
        confirmButton?.layer.masksToBounds = true
        confirmButton?.addTarget(self, action: #selector(touchConfirmButton(button:)), for: UIControlEvents.touchUpInside)
        
        //一开始，按钮不可点击
        confirmButton?.alpha = 0.8
        confirmButton?.isEnabled = false
    }
    
    //MARK: 点击确认按钮的事件
    func touchConfirmButton(button: UIButton){
        
        if codeTextField?.textContent == nil || codeTextField?.textContent == "" || phoneOrEmailTextField?.textContent == nil || phoneOrEmailTextField?.textContent == ""{return}
        
        if isBindingPhone == false {
            viewModel.bindingEmailVerificationCode(email: phoneOrEmailTextField!.textContent!, andCode: codeTextField!.textContent!)
        }else{
            viewModel.bindingPhoneNumberVerification(phoneNumber: phoneOrEmailTextField!.textContent!, andCode: codeTextField!.textContent!)
        }
    }
    
    
    
    //MARK: 各种闭包回调
    func closures(){
        
        //MARK: 获取验证码
        codeTextField?.touchGetCodeButtonClosure = {[weak self] _ in
            if self?.isBindingPhone == false{
                self?.viewModel.bindingEmailRequestCode(email: self?.phoneOrEmailTextField?.textContent)
            }else{
                self?.viewModel.bingdingPhoneRequestCode(phoneNumber: self?.phoneOrEmailTextField?.textContent)
            }
        }
        
        //MARK: 邮箱不合法
        viewModel.emailErrorClosure = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("请输入正确的邮箱地址", comment: ""), messageString: nil, rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//当邮箱地址和手机号码不合法时，则提醒用户
        }
        
        //MARK: 手机号码不合法的闭包
        viewModel.phoneNumberErrorClosure = {[weak self] _ in
            self?.codeTextField?.fireTimer()
            self?.alert(alertTitle: NSLocalizedString("请输入正确的手机号码", comment: ""), messageString: nil, rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//当手机号码不合法时，则提醒用户
        }
        
        //MARK: 请求验证法失败的闭包
        viewModel.requestVerificationCodeFailed = {[weak self] _ in
            self?.codeTextField?.fireTimer()// 重置计时器
        }
        
        //MARK:请求验证码成功的闭包
        viewModel.requestVerificationCodeSuccess = {[weak self] _ in
            self?.confirmButton?.alpha = 1.0
            self?.confirmButton?.isEnabled = true
        }
        
        //MARK: 调用接口错误的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)//处理Java服务器返回错误
        }
        
        //MARK: 绑定成功
        viewModel.verifySuccess = {[weak self] _ in
            self?.pMainViewModel.getUserInfo()
        }
        
        //MARK: 成功刷新用户数据
        pMainViewModel.getUserInfoSuccess = {[weak self] (userInfo: UserInfoModel) in
            self?.userInfo?.phoneNumber = userInfo.phoneNumber
            self?.userInfo?.email = userInfo.email
            //返回
            self?.alert(alertTitle: NSLocalizedString("绑定成功", comment: ""), leftClosure: {
                self?.bindingSuccess?()
             _ = self?.navigationController?.popViewController(animated: true)
            })
        }
        
        //MARK: 出错
        pMainViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
}
