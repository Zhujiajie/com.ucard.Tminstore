//
//  PNickNameVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/11.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: ----------修改昵称的界面---------

import UIKit
import SnapKit

class PNickNameVC: BaseViewController, UITextFieldDelegate {

    fileprivate let viewModel: ProfileViewModel = ProfileViewModel()
    fileprivate let textField: UITextField = UITextField()
    
    /// 更新用户信息成功
    var updateUserInfoSuccess: (()->())?
    
    /// 0 设置昵称，1设置名字，2设置姓氏
    var nameType: Int = 0
    
    var userInfo: UserInfoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        textField.backgroundColor = UIColor.white
        textField.font = UIFont.systemFont(ofSize: 20)
        textField.textColor = UIColor(hexString: "565656")
        
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 17, height: 17))
        textField.leftViewMode = UITextFieldViewMode.always
        textField.clearButtonMode = UITextFieldViewMode.whileEditing
        
        var text: String = ""
        switch nameType {
        case 0:
            text = (userInfo?.nickName)!
        case 1:
            text = (userInfo?.firstName)!
        default:
            text = (userInfo?.lastName)!
        }
        textField.text = text
        
        textField.delegate = self
        
        textField.layer.borderColor = UIColor(hexString: "#E9E9E9").cgColor
        textField.layer.borderWidth = 1
        
        view.addSubview(textField)
        textField.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view).offset(29/baseHeight)
            make.left.equalTo(view).offset(-1)
            make.right.equalTo(view).offset(1)
            make.height.equalTo(50)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        
        var title: String = ""
        
        switch nameType {
        case 0:
            title = NSLocalizedString("昵称", comment: "")
        case 1:
            title = NSLocalizedString("名字", comment: "")
        default:
            title = NSLocalizedString("姓氏", comment: "")
        }
        
        setNavigationTitle(title)//设置导航栏标题
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
        setNavigationBarRightButtonWithTitle(NSLocalizedString("保存", comment: ""))
    }

    //MARK: 点击保存按钮，提交新的昵称
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        if textField.isEditing {
            textField.endEditing(true)
        }else{
            putNewUsetInfo()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        putNewUsetInfo()
    }
    
    //MARK: 提交新的昵称
    fileprivate func putNewUsetInfo(){
        
        switch nameType {
        case 0:
            userInfo?.nickName = textField.text!
        case 1:
            userInfo?.firstName = textField.text!
        default:
            userInfo?.lastName = textField.text!
        }
        
        viewModel.updateUserInfo(userInfo: userInfo!)
        
        //MARK: 提交成功
        viewModel.updateUserInfoSuccess = {[weak self] _ in
            self?.updateUserInfoSuccess?()
            _ = self?.navigationController?.popViewController(animated: true)
        }
        
        //MARK: 提交失败
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
}

