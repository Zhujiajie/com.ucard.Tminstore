//
//  PSettingController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/10.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:----------------设置界面------------------

import UIKit
import SnapKit

class PSettingController: BaseViewController {
    
    fileprivate let viewModel: ProfileViewModel = ProfileViewModel()
    
    var userInfo: UserInfoModel?
    
    /// 注销按钮
    fileprivate var logoutButton: UIButton?
    fileprivate var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        
        setNavigationBar()//设置导航栏
        setLogoutButton()//设置注销按钮
        setTableView()//设置tableView
        closure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.edgesForExtendedLayout = []
        self.transitionCoordinator?.animate(alongsideTransition: { [weak self] (_) in
            self?.cancelTansparentNavigationBar()
        }, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactive.pan?.isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactive.pan?.isEnabled = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        setNavigationTitle(NSLocalizedString("设置", comment: ""))//设置导航栏标题
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
    }
    
    //MARK: 设置注销按钮
    fileprivate func setLogoutButton(){
        logoutButton = UIButton()
        logoutButton?.backgroundColor = UIColor.white
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "676767"), NSFontAttributeName: UIFont.systemFont(ofSize: 20)]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("退出登录", comment: ""), attributes: attDic)
        logoutButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        view.addSubview(logoutButton!)
        logoutButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.bottom.equalTo(view)
            make.height.equalTo(56)
        })
        logoutButton?.addTarget(self, action: #selector(touchLogoutButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 设置tableView
    fileprivate func setTableView(){
        
        tableView = UITableView(frame: CGRect.zero, style: UITableViewStyle.grouped)
        tableView?.backgroundColor = UIColor(hexString: "#F9F9F9")
        tableView?.register(PSettingTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.PSettingTableCellReuseIdentifier)
        tableView?.separatorStyle = .none
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(logoutButton!.snp.top)
        })

        tableView?.delegate = self
        tableView?.dataSource = self
        
        tableView?.contentInset = UIEdgeInsets(top: -30, left: 0, bottom: 0, right: 0)
    }
    
    //MARK: 退出登录
    func touchLogoutButton(button: UIButton) {
        
        let sheet: UIAlertController = UIAlertController(title: NSLocalizedString("确认退出登录？", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let logoutAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("退出登录", comment: ""), style: UIAlertActionStyle.default, handler: {[weak self] (_) in
            self?.viewModel.logout()
            let loginVC: PLoginViewController = PLoginViewController()
            self?.present(loginVC, animated: true, completion: nil)
        })
        sheet.addAction(logoutAction)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        sheet.addAction(cancelAction)
        
        present(sheet, animated: true, completion: nil)
    }
    
    //MARK: 闭包回调
    func closure(){
        //MARK: app进入前台，检查通知是否被关闭
        app.appWillEnterForeground = {[weak self] _ in
            self?.tableView?.reloadData()
        }
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
}

//MARK: -----------UITableViewDelegate, UITableViewDataSource------------
extension PSettingController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        switch section {
        case 0:
            return 3
        case 1:
            return 1
        default:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell: PSettingTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.PSettingTableCellReuseIdentifier, for: indexPath) as! PSettingTableViewCell
        cell.userInfo = userInfo
        cell.indexPath = indexPath
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            if let setting: UIUserNotificationType = UIApplication.shared.currentUserNotificationSettings?.types {
                if setting == UIUserNotificationType() {
                    return NSLocalizedString("请在设置－通知中开启通知权限", comment: "")
                }
            }
        }
        return nil
    }
    
    //MARK: 点击cell进入相应的设置界面
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            
            switch indexPath.row {
                
            case 0://设置个人信息的界面
                if userInfo == nil { return }
                let pvc: ProfileVC = ProfileVC()
                pvc.userInfo = userInfo
                _ = navigationController?.pushViewController(pvc, animated: true)
            case 1://清除缓存
                let cell: PSettingTableViewCell = tableView.cellForRow(at: indexPath) as! PSettingTableViewCell
                cell.clearCache(completionClosure: { [weak self] _ in
                    self?.showSVProgress(title: nil)
                    delay(seconds: 2, completion: {
                        cell.cacheLabel?.text = "0.0MB"
                        self?.dismissSVProgress()
                    })
                })
            default://更改密码的界面
                if userInfo == nil {return}
                let cpVC = PLoginResetPasswordVC()
                cpVC.umengViewName = "修改密码"
                cpVC.isForgetPassword = true
                navigationController?.pushViewController(cpVC, animated: true)
            }
        case 2:
            switch indexPath.row {
            case 2://进入关于我们的界面
                let aboutVC: AboutUsController = AboutUsController()
                navigationController?.pushViewController(aboutVC, animated: true)
            default://进入绑定手机号或者邮箱的界面
                if userInfo == nil {return}
                
                if indexPath.row == 0 && userInfo!.phoneNumber != ""{
                    return
                }else if indexPath.row == 1 && userInfo!.email != ""{
                    return
                }
                
                var isBindingPhone: Bool = true
                if indexPath.row != 0{
                    isBindingPhone = false
                }
                let bindingVC: BindingController = BindingController()
                bindingVC.userInfo = userInfo
                bindingVC.isBindingPhone = isBindingPhone
                navigationController?.pushViewController(bindingVC, animated: true)
                bindingVC.bindingSuccess = {[weak self] _ in
                    self?.tableView?.reloadData()
                }//绑定成功之后，刷新页面
            }
        default:
            break
        }
    }
}
