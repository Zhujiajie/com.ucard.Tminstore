//
//  PGenderVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/11.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:------------更改性别的视图--------------

import UIKit
import SnapKit

class PGenderVC: BaseViewController {

    fileprivate let viewModel: ProfileViewModel = ProfileViewModel()
    
    /// 更新性别成功
    var updateGenderSuccess: (()->())?
    
    var userInfo: UserInfoModel?
    var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()//设置导航栏
        setTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        setNavigationTitle(NSLocalizedString("性别", comment: ""))//设置导航栏标题
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
        setNavigationBarRightButtonWithTitle(NSLocalizedString("保存", comment: ""))
    }
    
    //MARK: 设置tableView
    fileprivate func setTableView(){
        tableView = UITableView()
        tableView?.register(UITableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.PGenderCellReuseidentifier)
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        tableView?.tableFooterView = UIView()
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        //MARK: 更新用户性别
        viewModel.updateUserInfo(userInfo: userInfo!)
        
        //更新成功
        viewModel.updateUserInfoSuccess = {[weak self] _ in
            self?.updateGenderSuccess?()
            _ = self?.navigationController?.popViewController(animated: true)
        }
        
        //网络出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
}

extension PGenderVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell: UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: appReuseIdentifier.PGenderCellReuseidentifier)
        
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = NSLocalizedString("男", comment: "")
            if userInfo?.gender == UserGender.male {
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        case 1:
            cell.textLabel?.text = NSLocalizedString("女", comment: "")
            if userInfo?.gender == UserGender.female {
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        default:
            cell.textLabel?.text = NSLocalizedString("未知", comment: "")
            if userInfo?.gender == UserGender.undefine{
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        for cell: UITableViewCell in tableView.visibleCells {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
        
        let cell: UITableViewCell? = tableView.cellForRow(at: indexPath)
        
        cell?.accessoryType = UITableViewCellAccessoryType.checkmark
        
        switch indexPath.row {
        case 0:
            userInfo?.gender = UserGender.male
        case 1:
            userInfo?.gender = UserGender.female
        default:
            userInfo?.gender = UserGender.undefine
        }
    }
}
