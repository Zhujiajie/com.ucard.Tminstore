//
//  CheckEmailPhonrController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/16.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

//MARK: 确认是否绑定了邮箱和手机号码的界面
class CheckEmailPhonrController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    ///用户信息
    var userInfo: UserInfoModel?
    
    fileprivate var tableView: UITableView?
    
    fileprivate let cellReuseIdentifier: String = "cellReuseIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView = UITableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.transitionCoordinator?.animate(alongsideTransition: { [weak self] (_) in
            self?.transparentNavigationBar()
            }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier, for: indexPath)
        
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = NSLocalizedString("手机号", comment: "")
            if userInfo?.phoneNumber == ""{
                cell.detailTextLabel?.text = NSLocalizedString("未绑定", comment: "")
            }
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        case 1:
            cell.textLabel?.text = NSLocalizedString("邮箱", comment: "")
            if userInfo?.phoneNumber == ""{
                cell.detailTextLabel?.text = NSLocalizedString("未绑定", comment: "")
            }
            cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        default:
            cell.textLabel?.text = NSLocalizedString("更改密码", comment: "")
        }
        
        return cell
    }
}
