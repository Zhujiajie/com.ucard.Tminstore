//
//  PSettingTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/6.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SDWebImage
import SnapKit

class PSettingTableViewCell: UITableViewCell {
    
    var indexPath: IndexPath?{
        didSet{
            setCell()
        }
    }
    
    /// 用户信息
    var userInfo: UserInfoModel?
    
    //记录下缓存的文件路径
    fileprivate var cacheFiles: [String] = [String]()
    
    /// 通知的开关
    fileprivate var switchView: UISwitch?
    
    /// 退出登录的label
    fileprivate var logoutLabel: UILabel?
    
    /// 缓存Label
    var cacheLabel: UILabel?
    
    /// 显示手机或者邮箱是否已被绑定
    var detailInfoLabel: UILabel?
    
    /// cell上的标题
    fileprivate let cellTitleDic: [Int: [String]] = [
                        0: [NSLocalizedString("我的信息", comment: ""),
                            NSLocalizedString("清除缓存", comment: ""),
                            NSLocalizedString("更改密码", comment: "")],
                        1: [NSLocalizedString("消息通知", comment: "")],
                        2: [NSLocalizedString("手机号", comment: ""),
                            NSLocalizedString("邮箱", comment: ""),
                            NSLocalizedString("关于我们", comment: "")]]
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    //MARK: 设置cell
    fileprivate func setCell(){
        
        switch indexPath!.section {
        case 0, 2:
            
            //添加灰线
            switch indexPath!.row {
            case 0:
                addBottomLine()
            case 1:
                addTopLine()
                addBottomLine()
            default:
                break
            }
            
            if indexPath?.section == 0 && indexPath?.row == 1 { break }
            self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            
        case 1:
            setSwitchView()
            self.selectionStyle = UITableViewCellSelectionStyle.none
        default:
            break
        }
        setTitle()
    }
    
    //MARK: 设置标题
    fileprivate func setTitle(){
        
        self.textLabel?.text = cellTitleDic[indexPath!.section]![indexPath!.row]
        
        // 清除缓存的cell
        if indexPath?.section == 0 && indexPath?.row == 1 {
            
            let cacheSize: Double = getCacheSize()
            cacheLabel = UILabel()
            cacheLabel?.font = UIFont.systemFont(ofSize: 17)
            cacheLabel?.textColor = UIColor(hexString: "808083")
            cacheLabel?.text = String(format: "%0.1f MB", cacheSize/1000/1000)
            contentView.addSubview(cacheLabel!)
            cacheLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(contentView)
                make.right.equalTo(contentView).offset(-13/baseWidth)
            })
        }
        //是否绑定手机号或邮箱
        else if indexPath?.section == 2 && indexPath?.row != 2{
            
            detailInfoLabel = UILabel()
            detailInfoLabel?.textColor = UIColor(hexString: "B7B7B7")
            detailInfoLabel?.font = UIFont.systemFont(ofSize: 13)
            contentView.addSubview(detailInfoLabel!)
            detailInfoLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(contentView)
                make.centerY.equalTo(contentView)
            })
            
            var detailString: String = ""
            if indexPath?.row == 0{
                if userInfo?.phoneNumber == ""{
                    detailString = NSLocalizedString("未绑定", comment: "")
                }else{
                    if userInfo != nil{
                        detailString = userInfo!.phoneNumber
                    }
                }
            }else{
                if userInfo?.email == ""{
                    detailString = NSLocalizedString("未绑定", comment: "")
                }else{
                    if userInfo != nil{
                        detailString = userInfo!.email
                    }
                }
            }
            detailInfoLabel?.text = detailString
        }
    }
    
    //MARK: 设置开关通知的switchView
    fileprivate func setSwitchView(){
        
        switchView = UISwitch()
        switchView?.onTintColor = UIColor(hexString: "68cfd6")
        switchView?.addTarget(self, action: #selector(touchSwitchView(_:)), for: UIControlEvents.valueChanged)
        
        self.accessoryView = switchView
        
        if let setting: UIUserNotificationType = UIApplication.shared.currentUserNotificationSettings?.types {
            
            if setting == UIUserNotificationType() {
                switchView?.isEnabled = false
            }
                
            else{
                switchView?.isEnabled = true
                //判断用户是否开启了通知权限
                if UIApplication.shared.isRegisteredForRemoteNotifications{
                    switchView?.setOn(true, animated: true)
                }else{
                    switchView?.setOn(false, animated: true)
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        textLabel?.text = nil
        cacheLabel?.removeFromSuperview()
        switchView?.removeFromSuperview()
        logoutLabel?.removeFromSuperview()
        detailInfoLabel?.removeFromSuperview()
    }
    
    //MARK: 获取缓存的大小
    fileprivate func getCacheSize()->Double{
        
        // 用于统计文件夹内所有文件大小
        var fileSize: Double = Double()
        
        fileSize += Double(SDImageCache.shared().getSize())
        fileSize += calculateFileSize(folderName: "/videoCache")
        
        return fileSize
    }
    
    //MARK: 分别计算不同缓存文件夹中文件的大小
    fileprivate func calculateFileSize(folderName: String)->Double{
        
        // 取出cache文件夹路径
        let cachePath: String? = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first
        var fileSize: Double = Double()
        //统计图片
        if cachePath != nil{
            let folderPath: String = cachePath! + folderName
            if let files: [String] = FileManager.default.subpaths(atPath: folderPath){
                // 快速枚举取出所有文件名
                for p: String in files{
                    // 把文件名拼接到路径中
                    let path: String = folderPath.appendingFormat("/\(p)")
                    // 取出文件属性
                    let floder: [FileAttributeKey: Any] = try! FileManager.default.attributesOfItem(atPath: path)
                    // 用元组取出文件大小属性
                    for (key, value) in floder {
                        // 对文件大小进行拼接
                        if key == FileAttributeKey.size{
                            fileSize += (value as AnyObject).doubleValue
                        }
                    }
                    cacheFiles.append(path)
                }
            }
        }
        return fileSize
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension PSettingTableViewCell{
    
    //MARK: 点击是否开启通知的按钮
    func touchSwitchView(_ switchView: UISwitch) {
        
        let application: UIApplication = UIApplication.shared
        
        if switchView.isOn {
            let setting: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(setting)
            application.registerForRemoteNotifications()//开启通知
        }else{
            application.unregisterForRemoteNotifications()//关闭通知
        }
    }
    
    //MARK: 清除缓存
    func clearCache(completionClosure completion: (()->())?){
        
        if indexPath?.section == 0 && indexPath?.row == 1{
            if cacheFiles.isEmpty == false {
                for file: String in cacheFiles {
                    do {
                        try FileManager.default.removeItem(atPath: file)
                    }catch {print(error)}
                }
            }//清空视频
            SDImageCache.shared().clearDisk(onCompletion: {
                completion?()
            })//清空图片
        }
    }
    
    //MARK: 添加上边的灰线
    fileprivate func addTopLine(){
        let view0: UIView = UIView()
        view0.backgroundColor = UIColor(hexString: "#F9F9F9")
        
        self.addSubview(view0)
        view0.layer.zPosition = 1
        view0.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(self)
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(1)
        }
    }
    
    //MARK: 添加底部灰线
    fileprivate func addBottomLine(){
        let view1: UIView = UIView()
        view1.backgroundColor = UIColor(hexString: "#F9F9F9")
        
        self.addSubview(view1)
        view1.layer.zPosition = 1
        view1.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(self)
            make.left.equalTo(self)
            make.right.equalTo(self)
            make.height.equalTo(1)
        }
    }
}
