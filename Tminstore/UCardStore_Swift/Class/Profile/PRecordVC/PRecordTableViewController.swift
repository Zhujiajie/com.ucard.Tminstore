//
//  PRecordTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import MJRefresh
import SDWebImage

fileprivate let PRecordTableViewCellReuseIdentifier: String = "PRecordTableViewCellReuseIdentifier"

class PRecordTableViewController: UITableViewController {
    
    /// 点击更多按钮的事件
    var touchMoreButtonClosure: ((_ indexPath: IndexPath)->())?
    /// 点击播放按钮的事件
    var touchPlayButtonClosure: ((_ indexPath: IndexPath)->())?
    
    weak var viewModel: PRecordViewModel?
    weak var userViewModel: PUserViewModel?
    weak var userInfoModel: UserInfoModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(PRecordTableViewCell.self, forCellReuseIdentifier: PRecordTableViewCellReuseIdentifier)
        
        self.tableView.mj_header = BaseAnimationHeader()
        self.tableView.mj_footer = BaseRefreshFooter()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 360/baseWidth
        
        closure()
    }

    //MARK: 闭包回调
    fileprivate func closure(){
        //MARK: 下拉刷新
        tableView.mj_header.refreshingBlock = {[weak self] _ in
            if self?.viewModel != nil {return}
            self?.userViewModel?.recordPageNum = 1
            self?.userViewModel?.requestOtherRecordData(memberCode: (self?.userInfoModel?.memberCode)!)
        }
        
        //MARK: 上拉刷新
        tableView.mj_footer.refreshingBlock = {[weak self] _ in
            if self?.viewModel != nil {return}
            self?.userViewModel?.requestOtherRecordData(memberCode: (self?.userInfoModel?.memberCode)!)
        }
        
        //MARK: 成功获得录屏数据
        userViewModel?.getRecordDataSuccess = {[weak self] _ in
            self?.stopRefresh()
            self?.tableView.reloadData()
        }
        
        //MARK: 没有更多录屏数据了
        userViewModel?.noMoreRecordData = {[weak self] _ in
            self?.tableView.mj_header.endRefreshing()
            self?.tableView.mj_footer.endRefreshingWithNoMoreData()
        }
    }
    
    //MARK: 停止刷新
    /// 停止刷新
    func stopRefresh(){
        self.tableView.mj_header.endRefreshing()
        self.tableView.mj_footer.endRefreshing()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel == nil && userViewModel == nil{
            return 0
        }else if viewModel != nil {
            return viewModel!.recordModelArray.count
        }else{
            return userViewModel!.recordModelArray.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: PRecordTableViewCell = tableView.dequeueReusableCell(withIdentifier: PRecordTableViewCellReuseIdentifier, for: indexPath) as! PRecordTableViewCell
        
        if viewModel != nil {
            cell.recordModel = viewModel!.recordModelArray[indexPath.row]
        }else{
            cell.recordModel = userViewModel!.recordModelArray[indexPath.row]
            cell.hideMoreButton = true
        }
        
        //MARK: 点击更多按钮的事件
        cell.touchMoreButtonClosure = {[weak self] _ in
            self?.touchMoreButtonClosure?(indexPath)
        }
        
        //MARK: 点击播放按钮的事件
        cell.touchPlayButtonClosure = {[weak self] _ in
            self?.touchPlayButtonClosure?(indexPath)
        }
        
        return cell
    }
}

class PRecordTableViewCell: UITableViewCell {
    
    /// 点击更多按钮的事件
    var touchMoreButtonClosure: (()->())?
    /// 点击播放按钮的事件
    var touchPlayButtonClosure: (()->())?
    
    /// 数据
    var recordModel: RecordModel?{
        didSet{
            setData()
        }
    }
    
    /// 是否需要英寸更多按钮
    var hideMoreButton: Bool = false{
        didSet{
            moreButton?.isHidden = hideMoreButton
            moreButtonImageView?.isHidden = hideMoreButton
        }
    }
    
    /// 用户头像
    fileprivate var portraitView: BaseImageView?
    /// 用户昵称
    fileprivate var userNameLabel: BaseLabel?
    /// 明信片制作完成的时间
    fileprivate var createdTimeLabel: BaseLabel?
    /// 点击选择更多的按钮
    fileprivate var moreButton: BaseButton?
    /// 更多按钮的icon
    fileprivate var moreButtonImageView: BaseImageView?
    /// 展示缩略图
    fileprivate var thumbnailView: BaseImageView?
    /// 播放按钮
    fileprivate var playButton: BaseButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        //增加灰色分隔区间
        let grayLine1: BaseView = BaseView.colorLine(colorString: "#F6F6F6")
        contentView.addSubview(grayLine1)
        grayLine1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.right.equalTo(contentView)
            make.height.equalTo(3)
        }
        
        //头像
        portraitView = BaseImageView.userPortraitView(cornerRadius: 21.5/baseWidth)
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayLine1.snp.bottom).offset(11/baseWidth)
            make.left.equalTo(contentView).offset(14/baseWidth)
            make.size.equalTo(CGSize(width: 43/baseWidth, height: 43/baseWidth))
        })
        portraitView?.sd_setImage(with: currentUserLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
        
        // 更多按钮
        moreButtonImageView = BaseImageView.normalImageView(andImageName: "moreButton")
        contentView.addSubview(moreButtonImageView!)
        moreButtonImageView!.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(grayLine1.snp.bottom).offset(29/baseWidth)
            make.right.equalTo(contentView).offset(-14/baseWidth)
        }
        moreButton = BaseButton.normalIconButton()
        moreButton?.backgroundColor = UIColor.clear
        contentView.addSubview(moreButton!)
        moreButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.equalTo(contentView)
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 60/baseWidth))
        })
        //MARK: 点击更多按钮的事件
        moreButton?.touchButtonClosure = {[weak self] _ in
            self?.touchMoreButtonClosure?()
        }
        
        //设置昵称
        userNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 20), andTextColorHex: "6B6B6B")
        contentView.addSubview(userNameLabel!)
        userNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.left.equalTo(portraitView!.snp.right).offset(8/baseWidth)
            make.right.equalTo(moreButton!.snp.left)
            make.height.equalTo(25/baseWidth)
        })
        userNameLabel?.text = userNickName
        
        //明信片制作完成的时间
        createdTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 11), andTextColorHex: "878686")
        contentView.addSubview(createdTimeLabel!)
        createdTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(userNameLabel!)
            make.bottomMargin.equalTo(portraitView!)
            make.height.equalTo(13/baseWidth)
        })
        
        //缩略图
        thumbnailView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentView.addSubview(thumbnailView!)
        thumbnailView?.snp.makeConstraints({ (make) in
            make.top.equalTo(portraitView!.snp.bottom).offset(8/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 351/baseWidth, height: 249/baseWidth))
            make.bottom.equalTo(contentView).offset(-10/baseWidth)
        })
        thumbnailView?.isUserInteractionEnabled = false
        
        playButton = BaseButton.normalIconButton(iconName: "play")
        contentView.addSubview(playButton!)
        playButton?.snp.makeConstraints({ (make) in
            make.center.equalTo(thumbnailView!)
            make.size.equalTo(CGSize(width: 45/baseWidth, height: 41/baseWidth))
        })
        playButton?.touchButtonClosure = {[weak self] _ in
            self?.touchPlayButtonClosure?()
        }
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        if let url = recordModel?.thumbnailURL{
            //先判断是否已缓存图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                if result == true{
                    self?.thumbnailView?.sd_setImage(with: url)
                }else{
                    self?.thumbnailView?.sd_setImage(with: self?.recordModel?.thumbnailPlaceHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                        if image != nil{
                            self?.thumbnailView?.sd_setImage(with: url, placeholderImage: image)
                        }else{
                            self?.thumbnailView?.sd_setImage(with: url, placeholderImage: nil)
                        }
                    })
                }
            })
        }
        
        if let date: Date = recordModel?.createDate{
            let formatter: DateFormatter = DateFormatter()
            formatter.timeStyle = DateFormatter.Style.none
            formatter.dateStyle = DateFormatter.Style.medium
            createdTimeLabel?.text = formatter.string(from: date)
        }
        thumbnailView?.sd_setImage(with: recordModel?.thumbnailURL)
        
        if recordModel?.userLogoURL != nil {
            portraitView?.sd_setImage(with: recordModel?.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
