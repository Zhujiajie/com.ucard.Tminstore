//
//  PRecordDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PRecordDataModel: BaseDataModel {
    
    //MARK: 请求录屏信息的接口
    /// 请求录屏信息的接口
    ///
    /// - Parameters:
    ///   - pageNum: 页数
    ///   - success: 成功的闭包
    func requestUserRecordInfo(pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        let urlString: String = appAPIHelp.checkUserRecordInfoAPI + "\(pageNum)"
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 删除录屏数据
    /// 删除录屏数据
    ///
    /// - Parameters:
    ///   - screencapId: 录屏的ID
    ///   - success: 成功的闭包
    func deleteRecordInfo(screencapId: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "screencapId": screencapId
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.deleteRecordInfoAPI, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self](error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
