//
//  PRecordController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MJRefresh
import SnapKit

class PRecordController: BaseViewController {
    
    fileprivate let shareViewModel: VideoShareViewModel = VideoShareViewModel()
    fileprivate let viewModel: PRecordViewModel = PRecordViewModel()
    fileprivate var tableViewController: PRecordTableViewController?
    ///播放器
    fileprivate var playerView: PMapPlayerView?
    /// 分享操作的视图
    fileprivate var shareView: PRecordShareView?
    /// 半透明黑色视图
    fileprivate var blackView: BaseView?
    /// 点击分享视图之后,当前操作的indexPath
    fileprivate var currentIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        setUI()
        closures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactive.pan?.isEnabled = true
        tableViewController?.tableView.mj_header.beginRefreshing()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactive.pan?.isEnabled = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("录屏", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        tableViewController = PRecordTableViewController()
        view.addSubview(tableViewController!.view)
        tableViewController!.view.snp.makeConstraints { (make) in
            make.edges.equalTo(view)
        }
        tableViewController?.viewModel = viewModel
        
        shareView = PRecordShareView()
        navigationController?.view.addSubview(shareView!)
        shareView?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(navigationController!.view)
            make.bottom.equalTo(navigationController!.view).offset(250/baseWidth)
            make.height.equalTo(250/baseWidth)
        })
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayerView(recordModel: RecordModel){
        NotificationCenter.default.post(name:  NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)//停止播放视频
        if playerView == nil {
            playerView = PMapPlayerView()
        }
        playerView?.recordModel = recordModel
        navigationController?.view.addSubview(playerView!)
        playerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(navigationController!.view)
        })
        playerView?.appearAnimation()
    }
    
    //MARK: 展示分享视图
    fileprivate func showOrHideShareView(isShow: Bool){
        if isShow{
            shareView?.snp.updateConstraints({ (make) in
                make.bottom.equalTo(navigationController!.view).offset(5/baseWidth)
            })
            blackView = BaseView.colorLine(colorString: "2B2B2B")
            blackView?.alpha = 0.0
            blackView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
            blackView?.setTapGesture()//添加点击事件
            blackView?.tapClosure = {[weak self] _ in
                self?.showOrHideShareView(isShow: false)
            }
            navigationController?.view.insertSubview(blackView!, belowSubview: shareView!)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
                self?.blackView?.alpha = 0.57
            }, completion: nil)
            
        }else{
            shareView?.snp.updateConstraints({ (make) in
                make.bottom.equalTo(navigationController!.view).offset(250/baseWidth)
            })
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
                self?.blackView?.alpha = 0.0
                }, completion: {[weak self] _ in
                    self?.blackView?.removeFromSuperview()
            })
        }
        autoLayoutAnimation(view: navigationController!.view)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击更多按钮的事件
        tableViewController?.touchMoreButtonClosure = {[weak self] (indexPath: IndexPath) in
            self?.showOrHideShareView(isShow: true)
            self?.currentIndexPath = indexPath
        }
        
        //MARK: 点击播放按钮的事件
        tableViewController?.touchPlayButtonClosure = {[weak self] (indexPath: IndexPath) in
            self?.setPlayerView(recordModel: (self?.viewModel.recordModelArray[indexPath.row])!)
        }
        
        //MARK: 下拉刷新
        tableViewController?.tableView.mj_header.refreshingBlock = {[weak self] _ in
            self?.viewModel.pageNum = 1
            self?.viewModel.requestUserRecordInfo()
        }
        
        //MARK: 上拉刷新
        tableViewController?.tableView.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.requestUserRecordInfo()
        }
        
        //MARK: 成功获得数据
        viewModel.getRecordDataSuccess = {[weak self] _ in
            self?.tableViewController?.tableView.mj_header.endRefreshing()
            self?.tableViewController?.tableView.mj_footer.endRefreshing()
            self?.tableViewController?.tableView.reloadData()
        }
        
        //MARK: 没有更多数据了
        viewModel.noMoreData = {[weak self] _ in
           self?.tableViewController?.tableView.mj_header.endRefreshing()
            self?.tableViewController?.tableView.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.tableViewController?.tableView.mj_header.endRefreshing()
            self?.tableViewController?.tableView.mj_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 删除录屏数据成功
        viewModel.deleteRecordInfoSuccess = {[weak self] _ in
            self?.tableViewController?.tableView.deleteRows(at: [(self?.currentIndexPath)!], with: UITableViewRowAnimation.automatic)
        }
        
        //MARK: 点击分享视图上的取消按钮
        shareView?.touchCancelButtonClosure = {[weak self] _ in
            self?.showOrHideShareView(isShow: false)
        }
        
        //MARK: 点击分享视图上的删除按钮
        shareView?.touchDeleteButtonClosure = {[weak self] _ in
            self?.showOrHideShareView(isShow: false)
            self?.deleteAlert(rightClosure: {
                self?.viewModel.deleteRecordInfo(indexPath: (self?.currentIndexPath)!)
            })
        }
        
        //MARK: 点击分享按钮的事件
        shareView?.touchButtonClosure = {[weak self] (buttonType: ShareButtonType) in
            self?.showOrHideShareView(isShow: false)
            self?.shareViewModel.shareButtonType = buttonType
            self?.shareViewModel.shareToThreePart(recordModel: (self?.viewModel.recordModelArray[(self?.currentIndexPath?.row)!])!)
        }
        
        //MARK: 分享成功的闭包
        shareViewModel.shareSuccess = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("分享成功", comment: ""))
        }
        
        //MARK: 分享出错的闭包
        shareViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }

    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
}
