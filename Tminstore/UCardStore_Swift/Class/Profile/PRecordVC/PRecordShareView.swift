//
//  PRecordShareView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PRecordShareView: UIView {
    
    /// 点击按钮的事件
    var touchButtonClosure: ((_ buttonType: ShareButtonType)->())?
    /// 点击删除按钮的事件
    var touchDeleteButtonClosure: (()->())?
    /// 点击取消按钮的事件
    var touchCancelButtonClosure: (()->())?
    
    fileprivate var shareLabel: UILabel?
    fileprivate var weChatSpaceButton: UIButton?
    fileprivate var weChatFriendButton: UIButton?
    fileprivate var weiBoButton: UIButton?
    fileprivate var moreButton: UIButton?
    /// 分享按钮
    fileprivate var shareView: VideoShareView?
    /// 删除的按钮
    fileprivate var deleteButton: BaseButton?
    /// 取消的按钮
    fileprivate var cancelButton: BaseButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        if UMSocialManager.default().isInstall(UMSocialPlatformType.wechatSession) == false{
            
            weiBoButton = setButtons(buttonType: ShareButtonType.weiBo)
            addSubview(weiBoButton!)
            weiBoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(self).offset(26/baseWidth)
                make.size.equalTo(CGSize(width: 47/baseWidth, height: 47/baseWidth))
                make.centerX.equalTo(self).offset(-37/baseWidth)
            })
            
            moreButton = setButtons(buttonType: ShareButtonType.more)
            addSubview(moreButton!)
            moreButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.topMargin.equalTo(weiBoButton!)
                make.size.equalTo(weiBoButton!)
                make.left.equalTo(weiBoButton!.snp.right).offset(34/baseWidth)
            })
        }else{
            weChatFriendButton = setButtons(buttonType: ShareButtonType.weChatFriend)
            addSubview(weChatFriendButton!)
            weChatFriendButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(self).offset(26/baseWidth)
                make.size.equalTo(CGSize(width: 47/baseWidth, height: 47/baseWidth))
                make.centerX.equalTo(self).offset(-37/baseWidth)
            })
            
            weiBoButton = setButtons(buttonType: ShareButtonType.weiBo)
            addSubview(weiBoButton!)
            weiBoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.topMargin.equalTo(weChatFriendButton!)
                make.size.equalTo(weChatFriendButton!)
                make.left.equalTo(weChatFriendButton!.snp.right).offset(34/baseWidth)
            })
            
            weChatSpaceButton = setButtons(buttonType: ShareButtonType.weChatSpace)
            addSubview(weChatSpaceButton!)
            weChatSpaceButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.topMargin.equalTo(weChatFriendButton!)
                make.right.equalTo(weChatFriendButton!.snp.left).offset(-34/baseWidth)
                make.size.equalTo(weChatFriendButton!)
            })
            
            moreButton = setButtons(buttonType: ShareButtonType.more)
            addSubview(moreButton!)
            moreButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.topMargin.equalTo(weChatFriendButton!)
                make.left.equalTo(weiBoButton!.snp.right).offset(34/baseWidth)
                make.size.equalTo(weChatFriendButton!)
            })
        }
        
        setLabel()
        
        let grayLine1: BaseView = BaseView.colorLine(colorString: "#F1F1F1")
        addSubview(grayLine1)
        grayLine1.snp.makeConstraints { (make) in
            make.top.equalTo(shareLabel!.snp.bottom).offset(25/baseWidth)
            make.left.right.equalTo(self)
            make.height.equalTo(2)
        }
        
        deleteButton = BaseButton.normalTitleButton(title: NSLocalizedString("删除", comment: ""), andTextFont: UIFont.systemFont(ofSize: 18), andTextColorHexString: "6F6F6F")
        addSubview(deleteButton!)
        deleteButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine1.snp.bottom)
            make.left.right.equalTo(self)
            make.height.equalTo(52/baseWidth)
        })
        deleteButton?.touchButtonClosure = {[weak self] _ in
            self?.touchDeleteButtonClosure?()
        }
        
        let grayLine2: BaseView = BaseView.colorLine(colorString: "#F1F1F1")
        addSubview(grayLine2)
        grayLine2.snp.makeConstraints { (make) in
            make.top.equalTo(deleteButton!.snp.bottom)
            make.left.right.equalTo(self)
            make.height.equalTo(2)
        }
        
        cancelButton = BaseButton.normalTitleButton(title: NSLocalizedString("取消", comment: ""), andTextFont: UIFont.systemFont(ofSize: 18), andTextColorHexString: "6F6F6F")
        addSubview(cancelButton!)
        cancelButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine2.snp.bottom)
            make.left.right.equalTo(self)
            make.height.equalTo(deleteButton!)
        })
        cancelButton?.touchButtonClosure = {[weak self] _ in
            self?.touchCancelButtonClosure?()
        }
    }
    
    //MARK: 设置标题
    fileprivate func setLabel(){
        shareLabel = UILabel()
        shareLabel?.font = UIFont.systemFont(ofSize: 12)
        shareLabel?.textColor = UIColor(hexString: "9E9E9E")
        shareLabel?.text = NSLocalizedString("分享到", comment: "")
        addSubview(shareLabel!)
        shareLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(moreButton!.snp.bottom).offset(20/baseWidth)
            make.centerX.equalTo(self)
        })
        
        let grayLine01: UIView = UIView()
        grayLine01.backgroundColor = UIColor(hexString: "C6C6C6")
        addSubview(grayLine01)
        grayLine01.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(shareLabel!)
            make.right.equalTo(shareLabel!.snp.left).offset(-18/baseWidth)
            make.size.equalTo(CGSize(width: 88/baseWidth, height: 1))
        }
        
        let grayLine02: UIView = UIView()
        grayLine02.backgroundColor = UIColor(hexString: "C6C6C6")
        addSubview(grayLine02)
        grayLine02.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(shareLabel!)
            make.left.equalTo(shareLabel!.snp.right).offset(18/baseWidth)
            make.size.equalTo(grayLine01)
        }
    }
    
    //MARK: 设置按钮
    fileprivate func setButtons(buttonType: ShareButtonType)->UIButton{
        
        let button: UIButton = UIButton()
        
        var imageName: String
        switch buttonType {
        case ShareButtonType.weChatFriend:
            imageName = "weChatFriend"
        case ShareButtonType.weChatSpace:
            imageName = "weChatSpace"
        case ShareButtonType.weiBo:
            imageName = "weiBoShare"
        default:
            imageName = "shareMore"
        }
        button.setImage(UIImage(named: imageName), for: UIControlState.normal)
        
        button.addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
        button.tag = buttonType.rawValue
        
        return button
    }
    
    //MARK: 点击按钮的事件
    func touchButton(button: UIButton){
        touchButtonClosure?(ShareButtonType(rawValue: button.tag)!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
