//
//  PRecordViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/30.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PRecordViewModel: BaseViewModel {
    
    fileprivate let dataModel: PRecordDataModel = PRecordDataModel()
    
    /// 录屏数据
    var recordModelArray: [RecordModel] = [RecordModel]()
    
    /// 成功获得数据
    var getRecordDataSuccess: (()->())?
    /// 没有更多数据
    var noMoreData: (()->())?
    /// 成功删除录屏数据
    var deleteRecordInfoSuccess: (()->())?
    /// 页数
    var pageNum: Int = 1
    
    override init() {
        super.init()
        
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 获取用户录屏信息
    /// 获取用户录屏信息
    func requestUserRecordInfo(){
        dataModel.requestUserRecordInfo(pageNum: pageNum) { [weak self] (result: [String: Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if list.isEmpty {
                            self?.noMoreData?()
                        }else{
                            if self?.pageNum == 1{
                                self?.recordModelArray.removeAll()
                            }
                            for dic: [String: Any] in list{
                                let model: RecordModel = RecordModel.initFromDic(dic: dic)
                                self?.recordModelArray.append(model)
                            }
                            self?.getRecordDataSuccess?()
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 删除录屏信息的接口
    /// 删除录屏信息的接口
    ///
    /// - Parameter indexPath: IndexPath
    func deleteRecordInfo(indexPath: IndexPath){
        showSVProgress(title: nil)
        dataModel.deleteRecordInfo(screencapId: recordModelArray[indexPath.row].screencapId, successClosure: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.recordModelArray.remove(at: indexPath.row)
                self?.deleteRecordInfoSuccess?()
                return
            }
            self?.errorClosure?(nil, result)
        })
    }
}
