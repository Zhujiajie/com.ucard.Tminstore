//
//  PDraftViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import CoreData

class PDraftViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {

    fileprivate let viewModel: PDraftViewModel = PDraftViewModel()
    
    fileprivate var tableView: PMainDraftTableView?
    
    fileprivate var draftEmptyView: PMainView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        tableView = PMainDraftTableView.setTableView(setDelegate: self, andDateSource: self)
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        viewModel.draftTableViewShouldReload = {[weak self] _ in
            self?.tableView?.reloadData()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cancelTansparentNavigationBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //进入视图后，自动更新草稿内容
        if viewModel.fetchedResultController == nil{
            doInBackground(inBackground: { [weak self] _ in
                self?.viewModel.setFetchedResultController()
                self?.viewModel.fetchedResultController?.delegate = self
                }, backToMainQueue: { [weak self] _ in
                    self?.viewModel.reloadDraft()
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.fetchedResultController?.delegate = nil
        viewModel.fetchedResultController = nil
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationTitle(NSLocalizedString("我的草稿", comment: ""))//设置导航栏标题
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
    }
    
    //MARK: 点击左按钮退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: 弹出让用户选择怎么操作草稿
    fileprivate func draftAlertSheet(indexPath: IndexPath){
        
        let sheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        //继续编辑
        let editAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("继续编辑", comment: ""), style: UIAlertActionStyle.default) { [weak self] (_) in
            
            let mvc: MUViewController = MUViewController()
            
            if let postcardInCoreData = self?.viewModel.fetchedResultController?.object(at: indexPath) as? Postcard{
                mvc.postcardData = postcardInCoreData
            }
            
            mvc.umengViewName = "明信片制作界面"
            let nav: UINavigationController = UINavigationController(rootViewController: mvc)
            self?.present(nav, animated: true, completion: nil)
            CFRunLoopWakeUp(CFRunLoopGetCurrent())
        }
        sheet.addAction(editAction)
        
        //删除
        let delete: UIAlertAction = UIAlertAction(title: NSLocalizedString("删除", comment: ""), style: UIAlertActionStyle.destructive) { [weak self] (_) in
            //删除的提示框
            self?.deleteAlert(alertTitle: NSLocalizedString("确认删除？", comment: ""), messageString: nil, rightClosure: { 
                self?.viewModel.deleteVideoAndARImage(indexPath: indexPath)
            })
        }
        sheet.addAction(delete)
        
        //取消
        let cancel: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        sheet.addAction(cancel)
        
        present(sheet, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.draftArray.count
    }
    
    //MARK: 数据为空时，提示用户草稿数据为空
    func numberOfSections(in tableView: UITableView) -> Int {
        if viewModel.draftArray.isEmpty == true{
            if draftEmptyView == nil{
                draftEmptyView = PMainView.emptyDraftView()
                tableView.addSubview(draftEmptyView!)
                draftEmptyView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
            }
            return 0
        }else{
            if draftEmptyView != nil{
                UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
                    self?.draftEmptyView?.alpha = 0
                    }, completion: {[weak self] (_) in
                        self?.draftEmptyView?.removeFromSuperview()
                        self?.draftEmptyView = nil
                })
            }
            return 1
        }
    }
    
    //MARK:设置cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: PMainDraftCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.PMainDraftCellReuseIdentifier, for: indexPath) as! PMainDraftCell
        cell.postcard = viewModel.draftArray[indexPath.row]
        cell.indexPath = indexPath
        
        //点击更多按钮，重新编辑或者删除草稿
        cell.touchMoreButtonClosure = {[weak self] (indexPath: IndexPath) in
            self?.draftAlertSheet(indexPath: indexPath)
        }
        
        return cell
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if let cells: [PMainDraftCell] = tableView?.visibleCells as? [PMainDraftCell]{
            for cell: PMainDraftCell in cells{
                cell.pauseVideo()
            }
        }
    }
    
    //MARK: Fetched Results Controller Delegate Methods
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView?.endUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        //判断change的类型
        switch type {
        case NSFetchedResultsChangeType.delete:
            if let indexPath: IndexPath = indexPath {
                //判断要删除的row是否是最后一行，若是最后一行，直接删除整个section
                tableView?.beginUpdates()
                
                if tableView?.numberOfRows(inSection: indexPath.section) == 1{
                    tableView?.deleteSections(IndexSet(integer: indexPath.section), with: UITableViewRowAnimation.automatic)
                }else{
                    tableView?.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                }
                tableView?.endUpdates()
                
                delay(seconds: 1, completion: { [weak self] _ in
                    self?.tableView?.reloadData()//reloadDate避免删除bug
                })
                
                app.saveContext()
            }
        default: break
        }
    }
}
