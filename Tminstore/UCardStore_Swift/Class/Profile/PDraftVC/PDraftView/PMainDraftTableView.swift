//
//  PMainDraftTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/12.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class PMainDraftTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        self.backgroundColor = UIColor.clear
        self.register(PMainDraftCell.self, forCellReuseIdentifier: appReuseIdentifier.PMainDraftCellReuseIdentifier)
        self.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tag = draftTableViewTag
        self.estimatedRowHeight = 335/baseWidth
        rowHeight = UITableViewAutomaticDimension//用户cell高度变化动画
        
    }
    
    //MARK: 设置tableView
    class func setTableView(setDelegate: UITableViewDelegate?, andDateSource setDataSource: UITableViewDataSource?)->PMainDraftTableView{
        let tableView: PMainDraftTableView = PMainDraftTableView()
        tableView.delegate = setDelegate
        tableView.dataSource = setDataSource
        return tableView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
