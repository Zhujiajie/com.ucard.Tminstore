//
//  PDraftImageCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

let PDraftImageCollectionViewCellappReuseIdentifier: String = "PDraftImageCollectionViewCellappReuseIdentifier"

class PDraftImageCollectionViewCell: UICollectionViewCell {
    
    /// 图片
    var image: UIImage?{
        didSet{
            imageView?.image = image
        }
    }
    
    /// 图片的地址
    var imageURL: URL?{
        didSet{
            imageView?.sd_setImage(with: imageURL)
        }
    }
    
    var imageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //图片
        imageView = UIImageView()
        imageView?.backgroundColor = UIColor.white
        imageView?.contentMode = UIViewContentMode.scaleToFill
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(17/baseWidth)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.bottom.equalTo(contentView).offset(-13.5/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
