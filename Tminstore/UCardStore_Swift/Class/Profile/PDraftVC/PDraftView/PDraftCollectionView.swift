//
//  PDraftCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PDraftCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: screenWidth, height: 275/baseWidth)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        super.init(frame: frame, collectionViewLayout: layout)
        
        backgroundColor = UIColor.white
        isPagingEnabled = true
        register(PDraftImageCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.PDraftImageCollectionViewCellappReuseIdentifier)
        register(PDraftVideoCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.PDraftVideoCollectionViewCellappReuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
