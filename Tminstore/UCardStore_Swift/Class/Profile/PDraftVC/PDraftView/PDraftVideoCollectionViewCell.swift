//
//  PDraftVideoCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

let PDraftVideoCollectionViewCellappReuseIdentifier: String = "PDraftVideoCollectionViewCellappReuseIdentifier"

class PDraftVideoCollectionViewCell: UICollectionViewCell {

    /// 视频路径
    var videoPath: URL?{
        didSet{
            if videoPath != nil && videoPath != oldValue{
                player?.stop()
                player?.url = videoPath
                player?.playFromBeginning()
            }
        }
    }
    
    fileprivate var player: ZTPlayer?
    fileprivate var playAgainButton: UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        player = ZTPlayer()
        player?.view.frame = CGRect(x: 17/baseWidth, y: 12/baseWidth, width: 351/baseWidth, height: 249/baseWidth)
        player?.playbackLoops = true
        contentView.addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            if self?.videoPath != nil{
                self?.playAgainButton?.isHidden = false
            }
        }
        
        //重新开始播放的按钮
        playAgainButton = UIButton()
        playAgainButton?.setImage(UIImage(named: "playButton"), for: UIControlState.normal)
        playAgainButton?.contentMode = UIViewContentMode.scaleAspectFit
        playAgainButton?.addTarget(self, action: #selector(playFromCurrentTime(button:)), for: UIControlEvents.touchUpInside)
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 80/baseWidth, height: 80/baseWidth))
        })
        playAgainButton?.isHidden = true
        
        //注册通知，暂停视频播放
        NotificationCenter.default.addObserver(self, selector: #selector(stopVideoPlayNotification(info:)), name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)
    }
    
    //MARK: 播放视频
    /// 播放视频
    func playVideo(){
        player?.playFromCurrentTime()
    }
    
    //MARK: 暂停视频
    /// 暂停视频
    ///
    /// - Parameter hidePlayButton: 是否需要显示播放按钮
    func pausePlayer(hidePlayButton: Bool = false){
        playAgainButton?.isHidden = hidePlayButton
        player?.pause()
    }
    
    //MARK: 重新开始播放视频
    func playFromCurrentTime(button: UIButton){
        playAgainButton?.isHidden = true
        player?.playFromCurrentTime()
    }
    
    //MARK: 接受通知，暂停视频播放
    func stopVideoPlayNotification(info: Notification){
        if player?.playbackState == PlaybackState.playing{
            pausePlayer()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        videoPath = nil
        playAgainButton?.isHidden = true
        player?.stop()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

