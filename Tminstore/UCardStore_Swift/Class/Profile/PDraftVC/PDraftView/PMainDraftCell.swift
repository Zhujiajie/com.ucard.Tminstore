//
//  PMainDraftCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/12.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMainDraftCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    var indexPath: IndexPath?
    
    /// cell的数据源
    var postcard: MUFinalPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 点击更多按钮，让用户选择删除草稿或者继续编辑
    var touchMoreButtonClosure: ((_ indexPath: IndexPath)->())?
    
    /// 创建时间的Label
    fileprivate var timeLabel: UILabel?
    ///放置图片和视频的collectionView
    fileprivate var collectionView: PDraftCollectionView?
    /// 展示页数的指示器
    fileprivate var pageControl: UIPageControl?
    /// 更多按钮
    fileprivate var moreButton: UIButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = UITableViewCellSelectionStyle.none
        setUI()
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#F6F6F6")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(5)
        }
        
        let containerView: UIView = UIView()
        containerView.backgroundColor = UIColor.white
        contentView.addSubview(containerView)
        containerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(grayLine.snp.bottom)
            make.left.bottom.right.equalTo(contentView)
        }
        
        timeLabel = UILabel()
        timeLabel?.textColor = UIColor(hexString: "878686")
        timeLabel?.font = UIFont.systemFont(ofSize: 12)
        containerView.addSubview(timeLabel!)
        timeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(containerView).offset(17/baseWidth)
            make.left.equalTo(containerView).offset(12/baseWidth)
            make.height.equalTo(15)
        })
        
        moreButton = UIButton()
        moreButton?.setImage(UIImage(named: "dot"), for: UIControlState.normal)
        containerView.addSubview(moreButton!)
        moreButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(timeLabel!)
            make.right.equalTo(containerView).offset(-13/baseWidth)
            make.size.equalTo(CGSize(width: 50/baseWidth, height: 50/baseWidth))
        })
        moreButton?.addTarget(self, action: #selector(touchMoreButton(button:)), for: UIControlEvents.touchUpInside)
        
        
        collectionView = PDraftCollectionView()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        containerView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(timeLabel!.snp.bottom)
            make.left.right.equalTo(containerView)
            make.height.equalTo(278/baseWidth)
        })
        
        // 页数指示器
        pageControl = UIPageControl()
        pageControl?.numberOfPages = 2
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "60DBE8")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D6D6D6")
        containerView.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom)
            make.centerX.equalTo(containerView)
            make.bottom.equalTo(containerView)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        if let creatDate: Date = postcard?.createDate{
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.none
            let dateString = dateFormatter.string(from: creatDate)
            timeLabel?.text = dateString
        }
        collectionView?.reloadData()
    }
    
    //MARK: 暂停视频播放
    func pauseVideo() {
        if let cell: PDraftVideoCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? PDraftVideoCollectionViewCell{
            cell.pausePlayer()
        }
    }
    
    //MARK: 点击更多按钮的事件
    func touchMoreButton(button: UIButton){
        if let indexPath: IndexPath = indexPath{
            touchMoreButtonClosure?(indexPath)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    //MARK: 设置cell的内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0{
            let cell: PDraftImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftImageCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftImageCollectionViewCell
            cell.image = postcard?.frontImage
            return cell
        }else{
            let cell: PDraftVideoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftVideoCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftVideoCollectionViewCell
            cell.videoPath = postcard?.videoPath
            return cell
        }
    }
    
    //MARK: 不显示Cell时。需要暂停播放视频
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell: PDraftVideoCollectionViewCell = cell as? PDraftVideoCollectionViewCell{
            cell.pausePlayer(hidePlayButton: true)
        }
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == screenWidth{
            if let cell: PDraftVideoCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? PDraftVideoCollectionViewCell{
                cell.playVideo()
            }
        }
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
