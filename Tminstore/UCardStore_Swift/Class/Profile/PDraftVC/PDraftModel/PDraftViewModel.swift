//
//  PDraftViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import CoreData

class PDraftViewModel: BaseViewModel {

    fileprivate let dataModel: PDraftDataModel = PDraftDataModel()
    
    /// 草稿需要重新载入
    var draftTableViewShouldReload: (()->())?
    
    /// 草稿箱的数据源
    var draftArray: [MUFinalPostcardModel] = [MUFinalPostcardModel]()
    
    /// 明信片草稿的数据源
    var fetchedResultController: NSFetchedResultsController<NSFetchRequestResult>?
    
    //MARK: 初始化fetchedResultController
    /// 初始化fetchedResultController
    ///
    /// - returns: NSFetchedResultsController
    func setFetchedResultController(){
        fetchedResultController = dataModel.setFetchedResultController()
    }
    
    //MARK: 刷新草稿箱
    /**
     刷新草稿箱
     */
    func reloadDraft(){
        do{
            try fetchedResultController!.performFetch()
            draftArray.removeAll()
            if fetchedResultController?.fetchedObjects?.isEmpty == false {
                for object: NSFetchRequestResult in fetchedResultController!.fetchedObjects! {
                    let postcard = MUFinalPostcardModel.initFromCoreData(postcardData: object as! Postcard)
                    draftArray.append(postcard)
                }
            }
            draftTableViewShouldReload?()
        }catch{ print(error.localizedDescription) }
    }
    
    //MARK: 删除EasyAR云端的图片
    /**
     删除EasyAR云端的图片
     */
    func deleteVideoAndARImage(indexPath: IndexPath) {
        //删除视频
        if let videoPath = draftArray[indexPath.row].videoPath{
            do {
                try FileManager.default.removeItem(at: videoPath)
            }catch{ print(error) }
        }
        //删除草稿
        draftArray.remove(at: indexPath.row)
        
        let postCardRecord: Postcard = fetchedResultController!.object(at: indexPath) as! Postcard
        app.managedObjectContext.delete(postCardRecord)
    }
}
