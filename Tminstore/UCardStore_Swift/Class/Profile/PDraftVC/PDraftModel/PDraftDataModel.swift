//
//  PDraftDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/31.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import CoreData

class PDraftDataModel: BaseDataModel {
    
    //MARK: 初始化一个 NSFetchResultController
    /// 初始化一个 NSFetchResultController
    ///
    /// - returns: NSFetchedResultsController
    func setFetchedResultController()->NSFetchedResultsController<NSFetchRequestResult>{
        
        let fetchRequest: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: PostcardEntityName)
        
        let sortDescription: NSSortDescriptor = NSSortDescriptor(key: "createdDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescription]
        
        //搜索条件
        let predicate: NSPredicate = NSPredicate(format: "(memberCode contains [cd] %@)", getUserID())
        fetchRequest.predicate = predicate
        
        let fetchedRC: NSFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: app.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedRC
    }
}
