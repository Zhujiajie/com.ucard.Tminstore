//
//  PMapDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PMapDataModel: BaseDataModel {

    //MARK: 取消发布到地图
    /// 取消发布到地图
    ///
    /// - Parameters:
    ///   - videoID: 视频ID
    ///   - success: 成功的闭包
    func cancelReleaseToMap(videoID: String, andOriginalId originalId: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId,
            "videoId": videoID
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.canelMapDataAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 重新发布到地图
    /// 重新发布到地图
    ///
    /// - Parameters:
    ///   - videoID: 视频ID
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    func reReleaseToMap(videoID: String, andOriginalId originalId: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId,
            "videoId": videoID
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.reReleaseToMapAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 删除发布到地图
    /// 删除发布到地图
    ///
    /// - Parameters:
    ///   - videoID: 视频ID
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包    
    func deleteReleaseToMap(videoID: String, andOriginalId originalId: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId,
            "videoId": videoID
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.deletaMapDataAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 获取某一张图片的所有视频评论
    /// 获取某一张图片的所有视频评论
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - should: 是否处理错误
    ///   - success: 成功的闭包
    func checkVideoComment(videoId: String, shouldHandelError should: Bool = true, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.checkSpaceTimeVideoCommentAPI + videoId
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self](error: Error?) in
            if should == true{
                self?.errorClosure?(error)
            }
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看别人留下的视频记忆
    /// 查看别人留下的视频记忆
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - should: 是否要处理错误
    ///   - success: 成功的闭包
    func checkVideoMemory(originalId: String, shouldHandelError should: Bool = true, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.checkSpaceTimeVideoMemoryAPI + originalId
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self](error: Error?) in
            if should == true{
                self?.errorClosure?(error)
            }
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看点赞详情
    /// 查看点赞详情
    ///
    /// - Parameters:
    ///   - videoId: 视频ID
    ///   - pageIndex: 页数
    ///   - success: 成功的闭包  
    func checkLikeDetail(videoId: String, andPageIndex pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.checkSpaceTimeLikeDetailAPI + videoId + "/\(pageIndex)"
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        },failureClourse: { [weak self](error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
