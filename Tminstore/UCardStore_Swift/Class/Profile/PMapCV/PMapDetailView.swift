//
//  PMapDetailView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMapLeftCalloutAccessoryView: UIView {

    var userLogoURL: URL?{
        didSet{
            if userLogoURL != nil {
                portraitView?.sd_setImage(with: userLogoURL)
            }
        }
    }
    
    fileprivate var portraitView: BaseImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        portraitView = BaseImageView.userPortraitView(cornerRadius: 17.5/baseWidth)
        addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        
        portraitView?.sd_setImage(with: currentUserLogoURL)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
