//
//  PMapTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MJRefresh
class PMapTableView: UITableView {
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        self.backgroundColor = UIColor.clear
        self.register(PNewMapContentCell.self, forCellReuseIdentifier: appReuseIdentifier.PNewMapContentCellReuseIdentifier)
        self.register(PNewMapTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.PNewMapTableViewCellReuseIdentifer)
        
        self.estimatedRowHeight = 335/baseWidth
        
        self.register(PMapTableViewHeaderView.self, forHeaderFooterViewReuseIdentifier: appReuseIdentifier.PMapTableViewHeaderViewReuseIdentifer)
        
        tableFooterView = UIView()
        
        mj_footer = BaseRefreshFooter()//增加上拉刷新
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
