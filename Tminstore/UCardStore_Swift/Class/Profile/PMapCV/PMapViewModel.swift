//
//  PMapViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PMapViewModel: BaseViewModel {

    fileprivate let dataModel: PMapDataModel = PMapDataModel()
    
    /// 视频评论数据源
    var videoCommentArray: [VideoCommentModel] = [VideoCommentModel]()
    /// 视频记忆数据源
    var videoMemoryArray: [VideoCommentModel] = [VideoCommentModel]()
    /// 点赞的模型
    var likeModelArray: [VideoCommentModel] = [VideoCommentModel]()
    
    /// 获取某一张图片所有信息成功
    var getPictureInfoSuccess: ((_ mapData: SpaceTimeDataModel)->())?
    /// 获取视频评论数据成功
    var getVideoCommentSuccess: (()->())?
    /// 获取视频记忆成功
    var getVideoMemorySuccess: (()->())?
    /// 获取点赞数成功
    var getLikeNumberSuccess: (()->())?
    /// 获得点赞数据成功，需要reloadTableView
    var getLikeDataSuccess: (()->())?
    /// 没有更多点赞数据了
    var noMoreLikeData: (()->())?
    
    /// 重新发布成功
    var reReleaseSuccessClosure: (()->())?
    
    /// 取消发布成功
    var cancelSuccessClosure: (()->())?
    
    /// 删除成功
    var deleteSuccessClosure: (()->())?
    
    /// 是否已经加载完点赞数了
    var isNoMoreLikeData: Bool = false
    
    fileprivate var likePageNum: Int = 1
    
    override init() {
        super.init()
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 完成的闭包
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 取消发布
    /// 取消发布
    ///
    /// - Parameter videoID: 视频的ID
    func cancelRelease(videoID: String, andOriginalId originalId: String){
        dataModel.cancelReleaseToMap(videoID: videoID, andOriginalId: originalId, successClosure: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.cancelSuccessClosure?()
                return
            }
            self?.errorClosure?(nil, result)
        })
    }
    
    //MARK: 重新发布
    /// 重新发布
    ///
    /// - Parameters:
    ///   - videoID: 视频ID
    ///   - originalId: 原创ID
    func reRelease(videoID: String, andOriginalId originalId: String){
        dataModel.reReleaseToMap(videoID: videoID, andOriginalId: originalId, successClosure: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.reReleaseSuccessClosure?()
                return
            }
            self?.errorClosure?(nil, result)
        })
    }
    
    //MARK: 删除发布
    /// 删除发布
    ///
    /// - Parameters:
    ///   - videoID: 视频ID
    ///   - originalId: 原创ID
    func deleteReleaseToMap(videoID: String, andOriginalId originalId: String){
        dataModel.deleteReleaseToMap(videoID: videoID, andOriginalId: originalId, successClosure: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.deleteSuccessClosure?()
                return
            }
            self?.errorClosure?(nil, result)
        })
    }
    
    //MARK: 获取视频评论内容
    /// 获取视频评论内容
    ///
    /// - Parameter originalId: 原创ID
    func getVideoComment(videoId: String){
        dataModel.checkVideoComment(videoId: videoId, shouldHandelError: true) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let video: [String: Any] = data["video"] as? [String: Any]{
                        if let commentList: [[String: Any]] = video["commentList"] as? [[String: Any]]{
                            for dic: [String: Any] in commentList{
                                let videoComment: VideoCommentModel = VideoCommentModel.initFromVideoCommentDic(dic: dic)
                                self?.videoCommentArray.append(videoComment)
                            }
                            self?.getVideoCommentSuccess?()
                            return
                        }
                    }
                }
            }
            self?.errorClosure?(nil ,result)
        }
    }
    
    //MARK: 获取视频记忆
    /// 获取视频记忆
    ///
    /// - Parameter originalId: 原创ID
    func getMemory(originalId: String){
        dataModel.checkVideoMemory(originalId: originalId, shouldHandelError: true) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let videoList: [[String: Any]] = data["videoList"] as? [[String: Any]]{
                        for dic: [String: Any] in videoList{
                            let memory: VideoCommentModel = VideoCommentModel.initFromDic(dic: dic)
                            self?.videoMemoryArray.append(memory)
                        }
                        self?.getVideoMemorySuccess?()
                        return
                    }
                }
            }
            self?.errorClosure?(nil ,result)
        }
    }
    
    //MARK: 查看点赞详情
    func getLike(videoId: String) {
        dataModel.checkLikeDetail(videoId: videoId, andPageIndex: likePageNum) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let likeList: [[String: Any]] = data["likeList"] as? [[String: Any]]{
                        if likeList.isEmpty {
                            self?.isNoMoreLikeData = true
                            self?.noMoreLikeData?()
                        }else{
                            for dic: [String: Any] in likeList{
                                let likeModel: VideoCommentModel = VideoCommentModel.initFromDic(dic: dic)
                                self?.likeModelArray.append(likeModel)
                            }
                            if self?.likePageNum == 1{
                                self?.getLikeNumberSuccess?()
                            }else{
                                self?.getLikeDataSuccess?()
                            }
                        }
                        self?.likePageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 获取某一图片的所有信息
    /// 获取某一图片的所有信息
    ///
    /// - Parameter videoId: 视频ID
    func getPictureInfo(videoId: String){
        showSVProgress(title: nil)
        dataModel.checkVideoComment(videoId: videoId, shouldHandelError: true) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let video: [String: Any] = data["video"] as? [String: Any]{
                        let mapData: SpaceTimeDataModel = SpaceTimeDataModel.initFromDic(dic: video)
                        mapData.videoId = videoId
                        if let image: String = data["image"] as? String{
                            mapData.frontImageURL = URL(string: image + "!/format/webp")
                            mapData.frontPlaceImageURL = URL(string: image + "!placeHolder")
                        }
                        self?.getPictureInfoSuccess?(mapData)
                        return
                    }
                }
            }
            self?.errorClosure?(nil ,result)
        }
    }
}
