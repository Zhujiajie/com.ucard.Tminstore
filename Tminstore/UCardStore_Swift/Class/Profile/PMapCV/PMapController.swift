//
//  PMapController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMapController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    fileprivate let viewModel: PMapViewModel = PMapViewModel()
    fileprivate let arViewModel: LocalARViewModel = LocalARViewModel()
    
    /// 删除成功
    var deleteSuccess: (()->())?
    
    /// 数据源
    var mapModel: SpaceTimeDataModel?
    
    fileprivate var tableView: PMapTableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arViewModel.mapModel = mapModel
        
        setNavigationBar()
        
        tableView = PMapTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.reloadData()
        
        closure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cancelTansparentNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        arViewModel.showARList()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationTitle(NSLocalizedString("时光圈", comment: ""))//设置导航栏标题
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
    }
    
    //MARK: 对数据进行操作的弹窗
    fileprivate func sheetAlert(){
        let ac: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        if mapModel?.status == SpaceTimeStatus.normal{
            //取消发布
            let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消发布", comment: ""), style: UIAlertActionStyle.default, handler: {[weak self] (_) in
                self?.viewModel.cancelRelease(videoID: (self?.mapModel?.videoId)!, andOriginalId: (self?.mapModel?.originalId)!)
            })
            ac.addAction(cancelAction)
        }else{
            //重新发布
            let reReleaseAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("重新发布", comment: ""), style: UIAlertActionStyle.default, handler: { [weak self] (_) in
                self?.viewModel.reRelease(videoID: (self?.mapModel?.videoId)!, andOriginalId: (self?.mapModel?.originalId)!)
            })
            ac.addAction(reReleaseAction)
        }
        //删除
        let deleteAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("删除", comment: ""), style: UIAlertActionStyle.destructive) { [weak self] (_) in
            self?.deleteAlert(alertTitle: NSLocalizedString("确认删除？", comment: ""),  rightClosure: {
                self?.viewModel.deleteReleaseToMap(videoID: (self?.mapModel?.videoId)!, andOriginalId: (self?.mapModel?.originalId)!)
            })
        }
        ac.addAction(deleteAction)
        
        //取消按钮
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        ac.addAction(cancelAction)
        
        present(ac, animated: true, completion: nil)
    }
    
    //MARK: 闭包回调
    fileprivate func closure(){
        
        //MARK: 重新发布成功
        viewModel.reReleaseSuccessClosure = {[weak self] _ in
            self?.mapModel?.status = SpaceTimeStatus.normal
            self?.successSVProgress(title: NSLocalizedString("重新发布成功", comment: ""))
        }
        //MARK: 取消发布成功
        viewModel.cancelSuccessClosure = {[weak self] _ in
            self?.mapModel?.status = SpaceTimeStatus.cancel
            self?.successSVProgress(title: NSLocalizedString("取消发布成功", comment: ""))
        }
        //MARK: 删除成功
        viewModel.deleteSuccessClosure = {[weak self] _ in
            self?.successSVProgress(title: NSLocalizedString("删除成功", comment: ""))
            delay(seconds: 2, completion: {
                self?.deleteSuccess?()
                self?.navigationController?.popViewController(animated: true)
            })
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 成功获得评论数据
        arViewModel.collectionViewShouldReload = {[weak self] (_) in
            if let cell: PMapCommentTableViewCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 0)) as? PMapCommentTableViewCell{
                cell.stopRefresh()
                cell.postcardModelArray = (self?.arViewModel.postcardArray)!
            }
        }
        //MARK: 最后一页
        arViewModel.noMoreData = {[weak self] _ in
            if let cell: PMapCommentTableViewCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 0)) as? PMapCommentTableViewCell{
                cell.stopRefresh()
                cell.isLastPage = true
//                if self?.arViewModel.postcardArray.isEmpty == true{
//                    cell.postcardModelArray = (self?.arViewModel.postcardArray)!
//                }
            }
        }
        //MARK: 出错的闭包
        arViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            if let cell: PMapCommentTableViewCell = self?.tableView?.cellForRow(at: IndexPath(row: 1, section: 0)) as? PMapCommentTableViewCell{
                cell.stopRefresh()
            }
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 切换视频
    fileprivate func switchVideo(indexPath: IndexPath){
        if let cell: PMapImageTableViewCell = tableView?.cellForRow(at: IndexPath(row: 0, section: 0)) as? PMapImageTableViewCell{
            cell.currentVideoURL = arViewModel.postcardArray[indexPath.row].videoURL
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell: PMapImageTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.PMapImageTableViewCellappReuseIdentifier, for: indexPath) as! PMapImageTableViewCell
            cell.mapModel = mapModel
            //MARK: 点击更多按钮的事件
            cell.touchMoreButtonClosure = {[weak self] _ in
                self?.sheetAlert()
            }
            return cell
        }else{
            let cell: PMapCommentTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.PMapCommentTableViewCellappReuseIdentifier, for: indexPath) as! PMapCommentTableViewCell
            
            //MARK: 点击cell切换视频
            cell.touchCellClosure = {[weak self] (indexPath: IndexPath) in
                self?.switchVideo(indexPath: indexPath)
            }
            //MARK: 滑动刷新
            cell.headerRefreshClosure = {[weak self] _ in
                self?.arViewModel.pageNum = 1
                self?.arViewModel.showARList()
            }
            cell.footerRefreshClosure = {[weak self] _ in
                self?.arViewModel.showARList()
            }
            return cell
        }
    }
}
