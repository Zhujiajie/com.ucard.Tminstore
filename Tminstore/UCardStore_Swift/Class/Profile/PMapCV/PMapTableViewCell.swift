//
//  PMapTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

class PNewMapContentCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource{
    
    var mapModel: SpaceTimeDataModel?{
        didSet{
            setData()
        }
    }
    
    var userInfoModel: UserInfoModel?{
        didSet{
            if userInfoModel != nil {
                moreButton?.isHidden = true
                moreButtonImageView?.isHidden = true
                portraitView?.sd_setImage(with: userInfoModel?.userLogoURL)
                userNameLabel?.text = userInfoModel?.nickName
            }
        }
    }
    
    /// 点击更多按钮的事件
    var touchMoreButtonClosure: (()->())?
    
    /// 用户头像
    fileprivate var portraitView: BaseImageView?
    /// 用户昵称
    fileprivate var userNameLabel: BaseLabel?
    /// 明信片制作完成的时间
    fileprivate var createdTimeLabel: BaseLabel?
    /// 点击选择更多的按钮
    fileprivate var moreButton: BaseButton?
    /// 更多按钮ICON
    fileprivate var moreButtonImageView: BaseImageView?
    /// 承载图片和视频的collectionView
    fileprivate var collectionView: PDraftCollectionView?
    /// 展示页数的指示器
    fileprivate var pageControl: UIPageControl?
    /// 位置Icon
    fileprivate var locationIcon: BaseImageView?
    /// 国家label
    fileprivate var addressLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        //增加灰色分隔区间
        let grayLine1: BaseView = BaseView.colorLine(colorString: "#F6F6F6")
        contentView.addSubview(grayLine1)
        grayLine1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.right.equalTo(contentView)
            make.height.equalTo(3)
        }
        
        //头像
        portraitView = BaseImageView.userPortraitView(cornerRadius: 21.5/baseWidth)
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayLine1.snp.bottom).offset(11/baseWidth)
            make.left.equalTo(contentView).offset(14/baseWidth)
            make.size.equalTo(CGSize(width: 43/baseWidth, height: 43/baseWidth))
        })
        
        // 更多按钮
        moreButtonImageView = BaseImageView.normalImageView(andImageName: "moreButton")
        contentView.addSubview(moreButtonImageView!)
        moreButtonImageView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(grayLine1.snp.bottom).offset(29/baseWidth)
            make.right.equalTo(contentView).offset(-14/baseWidth)
        }
        moreButton = BaseButton.normalIconButton()
        moreButton?.backgroundColor = UIColor.clear
        contentView.addSubview(moreButton!)
        moreButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.equalTo(contentView)
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 60/baseWidth))
        })
        //MARK: 点击更多按钮的事件
        moreButton?.touchButtonClosure = {[weak self] _ in
            self?.touchMoreButtonClosure?()
        }
        
        //设置昵称
        userNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 20), andTextColorHex: "6B6B6B")
        contentView.addSubview(userNameLabel!)
        userNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.left.equalTo(portraitView!.snp.right).offset(8/baseWidth)
            make.right.equalTo(moreButton!.snp.left)
            make.height.equalTo(25/baseWidth)
        })
        
        //明信片制作完成的时间
        createdTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 11), andTextColorHex: "878686")
        contentView.addSubview(createdTimeLabel!)
        createdTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(userNameLabel!)
            make.bottomMargin.equalTo(portraitView!)
            make.height.equalTo(13/baseWidth)
        })
        
        collectionView = PDraftCollectionView()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(portraitView!.snp.bottom)
            make.left.right.equalTo(contentView)
            make.height.equalTo(278/baseWidth)
        })
        
        // 页数指示器
        pageControl = UIPageControl()
        pageControl?.numberOfPages = 2
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "60DBE8")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D6D6D6")
        contentView.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom)
            make.centerX.equalTo(contentView)
        })
        
        locationIcon = BaseImageView.normalImageView(andImageName: "mapDetailLocation")
        contentView.addSubview(locationIcon!)
        locationIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(pageControl!.snp.bottom)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.size.equalTo(CGSize(width: 17/baseWidth, height: 22/baseWidth))
            make.bottom.equalTo(contentView).offset(-12/baseWidth)
        })
        
        addressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "8C8A8A", andNumberOfLines: 2)
        contentView.addSubview(addressLabel!)
        addressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(locationIcon!.snp.right).offset(5/baseWidth)
            make.centerY.equalTo(locationIcon!)
            make.right.equalTo(contentView).offset(-5/baseWidth)
        })
        
        //注册通知，暂停视频播放
        NotificationCenter.default.addObserver(self, selector: #selector(stopVideoPlayNotification(info:)), name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        portraitView?.sd_setImage(with: currentUserLogoURL)
        userNameLabel?.text = userNickName
        if let creatDate: Date = mapModel?.createDate{
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.none
            let dateString = dateFormatter.string(from: creatDate)
            createdTimeLabel?.text = dateString
        }
        collectionView?.reloadData()
        addressLabel?.text = mapModel?.detailedAddress
    }
    
    //MARK: 接受通知，暂停视频播放
    func stopVideoPlayNotification(info: Notification){
        collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    //MARK: 设置cell的内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0{
            let cell: PDraftImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftImageCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftImageCollectionViewCell
            cell.imageURL = mapModel?.frontImageURL
            return cell
        }else{
            let cell: PDraftVideoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftVideoCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftVideoCollectionViewCell
            cell.videoPath = mapModel?.videoURL
            return cell
        }
    }
    
    //MARK: 不显示Cell时。需要暂停播放视频
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell: PDraftVideoCollectionViewCell = cell as? PDraftVideoCollectionViewCell{
            cell.pausePlayer(hidePlayButton: true)
        }
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == screenWidth{
            if let cell: PDraftVideoCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? PDraftVideoCollectionViewCell{
                cell.playVideo()
            }
        }
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

//MARK: 展示视频评论的cell
class PNewMapTableViewCell: UITableViewCell{
    
    /// 点击播放按钮的事件
    var touchPlayButton: ((_ comment: VideoCommentModel)->())?
    
    /// 点击头像的事件
    var tapUserPortrait: ((_ memberCode: String)->())?
    
    /// 数据源
    var comment: VideoCommentModel?{
        didSet{
            setData()
        }
    }
    /// 需要展示的数据类型
    var currentDataType: DetailMapDataType = DetailMapDataType.comment
    
    ///用户头像
    fileprivate var portraitView: BaseImageView?
    /// 昵称
    fileprivate var nickNameLabel: BaseLabel?
    /// 创建时间的label
    fileprivate var createTimeLabel: BaseLabel?
    /// 内容的label
    fileprivate var contentLabel: BaseLabel?
    /// 点击查看视频的按钮
    fileprivate var playButton: BaseButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        portraitView = BaseImageView.userPortraitView(cornerRadius: 20/baseWidth)
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(9/baseWidth)
            make.left.equalTo(contentView).offset(17/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        })
        portraitView?.addTapGesture()
        //MARK: 点击头像的事件
        portraitView?.tapImageView = { [weak self] _ in
            self?.tapUserPortrait?((self?.comment?.memberCode)!)
        }
        
        createTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 10), andTextColorHex: "979797", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(createTimeLabel!)
        createTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.right.equalTo(contentView).offset(-17/baseWidth)
        })
        
        nickNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "494949")
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.left.equalTo(portraitView!.snp.right).offset(14/baseWidth)
            make.right.equalTo(createTimeLabel!.snp.left).offset(-5/baseWidth)
        })
        
        contentLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "A7A7A7")
        contentView.addSubview(contentLabel!)
        contentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(3/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
        })
        
        playButton = BaseButton.normalTitleButton(title: NSLocalizedString("点击查看", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "9BF9FF")
        contentView.addSubview(playButton!)
        playButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentLabel!)
            make.left.equalTo(contentLabel!.snp.right).offset(14/baseWidth)
            make.size.equalTo(CGSize(width: 70/baseWidth, height: 20/baseWidth))
        })
        //MARK: 点击播放按钮
        playButton?.touchButtonClosure = {[weak self] _ in
            self?.touchPlayButton?((self?.comment)!)
        }
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        portraitView?.sd_setImage(with: comment?.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
        if let date: Date = comment?.createDate{
            createTimeLabel?.text = getDateText(date: date)
        }
        nickNameLabel?.text = comment?.nickName
        
        switch currentDataType {
        case DetailMapDataType.comment:
            contentLabel?.text = NSLocalizedString("的视频评论", comment: "")
            playButton?.isHidden = false
        case DetailMapDataType.memory:
            contentLabel?.text = NSLocalizedString("留下的记忆", comment: "")
            playButton?.isHidden = false
        default:
            contentLabel?.text = NSLocalizedString("的点赞", comment: "")
            playButton?.isHidden = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PMapImageTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

    var mapModel: SpaceTimeDataModel?{
        didSet{
            setData()
        }
    }
    
    /// 正在播放的视频的地址
    var currentVideoURL: URL?{
        didSet{
            if collectionView != nil{
                collectionView?.scrollToItem(at: IndexPath(item: 1, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                delay(seconds: 0.5, completion: { [weak self] _ in
                    if let cell: PDraftVideoCollectionViewCell = self?.collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? PDraftVideoCollectionViewCell{
                        cell.videoPath = self?.currentVideoURL
                    }
                })
            }
        }
    }
    
    /// 点击更多按钮的事件
    var touchMoreButtonClosure: (()->())?
    
    /// 用户头像
    fileprivate var portraitView: UIImageView?
    /// 用户昵称
    fileprivate var userNameLabel: UILabel?
    /// 明信片制作完成的时间
    fileprivate var createdTimeLabel: UILabel?
    /// 点击选择更多的按钮
    fileprivate var moreButton: UIButton?
    /// 承载图片和视频的collectionView
    fileprivate var collectionView: PDraftCollectionView?
    /// 展示页数的指示器
    fileprivate var pageControl: UIPageControl?
    /// 位置Icon
    fileprivate var locationIcon: UIImageView?
    /// 国家label
    fileprivate var countryLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        //增加灰色分隔区间
        let grayLine1: UIView = UIView()
        grayLine1.backgroundColor = UIColor(hexString: "#F2F6F8")
        contentView.addSubview(grayLine1)
        grayLine1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.right.equalTo(contentView)
            make.height.equalTo(4)
        }
        
        //头像
        portraitView                     = UIImageView()
        portraitView?.contentMode         = UIViewContentMode.scaleAspectFill
        portraitView?.layer.cornerRadius  = 43/baseWidth/2
        portraitView?.layer.masksToBounds = true
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayLine1).offset(11/baseWidth)
            make.left.equalTo(contentView).offset(14/baseWidth)
            make.size.equalTo(CGSize(width: 43/baseWidth, height: 43/baseWidth))
        })
        
        // 更多按钮
        let moreButtonImageView: UIImageView = UIImageView(image: UIImage(named: "moreButton"))
        contentView.addSubview(moreButtonImageView)
        moreButtonImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(grayLine1).offset(29/baseWidth)
            make.right.equalTo(contentView).offset(-14/baseWidth)
        }
        moreButton = UIButton()
        moreButton?.backgroundColor = UIColor.clear
        contentView.addSubview(moreButton!)
        moreButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.equalTo(contentView)
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 60/baseWidth))
        })
        moreButton?.addTarget(self, action: #selector(touchMoreButton(button:)), for: UIControlEvents.touchUpInside)
        
        //设置昵称
        userNameLabel                = UILabel()
        userNameLabel?.textColor     = UIColor(hexString: "6B6B6B")
        userNameLabel?.font          = UIFont.systemFont(ofSize: 20)
        userNameLabel?.textAlignment = NSTextAlignment.left
        contentView.addSubview(userNameLabel!)
        userNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.left.equalTo(portraitView!.snp.right).offset(8/baseWidth)
            make.right.equalTo(moreButton!.snp.left)
            make.height.equalTo(25/baseWidth)
        })
        
        //明信片制作完成的时间
        createdTimeLabel            = UILabel()
        createdTimeLabel?.textColor = UIColor(hexString: "878686")
        createdTimeLabel?.font      = UIFont.systemFont(ofSize: 11)
        contentView.addSubview(createdTimeLabel!)
        createdTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(userNameLabel!)
            make.bottomMargin.equalTo(portraitView!)
            make.height.equalTo(13/baseWidth)
        })
        
        collectionView = PDraftCollectionView()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(portraitView!.snp.bottom)
            make.left.right.equalTo(contentView)
            make.height.equalTo(278/baseWidth)
        })
        
        // 页数指示器
        pageControl = UIPageControl()
        pageControl?.numberOfPages = 2
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "60DBE8")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D6D6D6")
        contentView.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom)
            make.centerX.equalTo(contentView)
        })
        
        locationIcon = UIImageView(image: UIImage(named: "mapDetailLocation"))
        contentView.addSubview(locationIcon!)
        locationIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(pageControl!.snp.bottom)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.size.equalTo(CGSize(width: 19/baseWidth, height: 19/baseWidth))
            make.bottom.equalTo(contentView).offset(-20/baseWidth)
        })
        
        countryLabel = BaseLabel()
        countryLabel?.font = UIFont.systemFont(ofSize: 14)
        countryLabel?.textColor = UIColor(hexString: "8C8A8A")
        contentView.addSubview(countryLabel!)
        countryLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(locationIcon!.snp.right).offset(11/baseWidth)
            make.bottomMargin.equalTo(locationIcon!)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        portraitView?.sd_setImage(with: mapModel?.userLogoURL)
        userNameLabel?.text = mapModel?.nickName
        
        if let creatDate: Date = mapModel?.createDate{
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.short
            dateFormatter.timeStyle = DateFormatter.Style.none
            let dateString = dateFormatter.string(from: creatDate)
            createdTimeLabel?.text = dateString
        }
        collectionView?.reloadData()
        countryLabel?.text = mapModel?.country
        currentVideoURL = mapModel?.videoURL
    }
    
    
    //MARK: 点击更多按钮的事件
    func touchMoreButton(button: UIButton){
        touchMoreButtonClosure?()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    //MARK: 设置cell的内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0{
            let cell: PDraftImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftImageCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftImageCollectionViewCell
            cell.imageURL = mapModel?.frontImageURL
            return cell
        }else{
            let cell: PDraftVideoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftVideoCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftVideoCollectionViewCell
            cell.videoPath = mapModel?.videoURL
            return cell
        }
    }
    
    //MARK: 不显示Cell时。需要暂停播放视频
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell: PDraftVideoCollectionViewCell = cell as? PDraftVideoCollectionViewCell{
            cell.pausePlayer(hidePlayButton: true)
        }
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == screenWidth{
            if let cell: PDraftVideoCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? PDraftVideoCollectionViewCell{
                cell.playVideo()
            }
        }
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PMapCommentTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    /// 点击collectionViewCell的事件
    var touchCellClosure: ((_ indexPath: IndexPath)->())?
    /// 左边刷新的闭包
    var headerRefreshClosure: (()->())?
    /// 右边刷新的闭包
    var footerRefreshClosure: (()->())?
    /// 是否是最后一页
    var isLastPage: Bool = false{
        didSet{
            if collectionView != nil{
//                collectionView?.isLastPage = isLastPage
            }
        }
    }
    
    /// 视频信息
    var postcardModelArray: [PostcardModel] = [PostcardModel](){
        didSet{
            if collectionView != nil{
                collectionView?.reloadData()
            }
        }
    }
    
//    /// 视屏评论的数组
//    var videoCommentArray: [CommentModel] = [CommentModel](){
//        didSet{
//            if videoCommentArray.count == 1{
//                let lable: UILabel = UILabel()
//                lable.font = UIFont.systemFont(ofSize: 14)
//                lable.textColor = UIColor(hexString: "A0A0A0")
//                lable.text = NSLocalizedString("还没有人留下记忆哦", comment: "")
//                contentView.addSubview(lable)
//                lable.snp.makeConstraints { (make: ConstraintMaker) in
//                    make.centerX.equalTo(contentView)
//                    make.top.equalTo(introLabel!.snp.bottom).offset(20/baseWidth)
//                    make.bottom.equalTo(contentView)
//                }
//                collectionView?.isHidden = true
//            }else{
//                if collectionView != nil{
//                    collectionView?.reloadData()
//                }
//            }
//        }
//    }
    
    fileprivate var introLabel: UILabel?
    fileprivate var collectionView: PMapCollectionView?
    /// 当前正在播放的视频地址
    var currentVideoURL: URL?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#F2F6F8")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.right.equalTo(contentView)
            make.height.equalTo(2)
        }
        
        let icon: UIImageView = UIImageView(image: UIImage(named: "PMainIcon"))
        icon.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(icon)
        icon.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(grayLine.snp.bottom).offset(20/baseWidth)
            make.left.equalTo(contentView).offset(20/baseWidth)
            make.size.equalTo(CGSize(width: 12/baseWidth, height: 11/baseWidth))
        }
        
        introLabel = UILabel()
        introLabel?.font = UIFont.systemFont(ofSize: 14)
        introLabel?.textColor = UIColor(hexString: "A0A0A0")
        introLabel?.text = NSLocalizedString("留下的视频记忆", comment: "")
        contentView.addSubview(introLabel!)
        introLabel?.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(icon)
            make.left.equalTo(icon.snp.right).offset(7/baseWidth)
        }
        
        collectionView = PMapCollectionView()
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(introLabel!.snp.bottom).offset(27/baseWidth)
            make.left.right.equalTo(contentView)
            make.height.equalTo(105/baseWidth)
            make.bottom.equalTo(contentView)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        collectionView?.headerRefreshClosure = {[weak self] _ in
            self?.isLastPage = false
            self?.headerRefreshClosure?()
        }
        collectionView?.footerRefreshClosure = {[weak self] _ in
            self?.footerRefreshClosure?()
        }
    }
    
    //MARK: 停止刷新
    func stopRefresh(){
//        collectionView?.endRefreshing()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postcardModelArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PMapCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PMapCollectionViewCellReuseIdentifier, for: indexPath) as! PMapCollectionViewCell
        cell.postcardModel = postcardModelArray[indexPath.row]
        if postcardModelArray[indexPath.row].videoURL == currentVideoURL{
            cell.setPlayState(isPlaying: true)
        }else{
            cell.setPlayState(isPlaying: false)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell: PMapCollectionViewCell = collectionView.cellForItem(at: indexPath) as? PMapCollectionViewCell{
            NotificationCenter.default.post(name: NSNotification.Name(LocalTimeLineCellNotificationName), object: nil)
            cell.setPlayState(isPlaying: true)
            touchCellClosure?(indexPath)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
