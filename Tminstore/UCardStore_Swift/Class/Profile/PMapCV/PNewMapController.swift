//
//  PNewMapController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MapKit
import SnapKit

class PNewMapController: BaseViewController {

    fileprivate let viewModel: PMainViewModel = PMainViewModel()
    
    /// 地图
    fileprivate var mapView: MKMapView?
    /// collectionView
    fileprivate var collectionViewController: PNewMapCollectionViewController?
    /// detailCallOutView
    fileprivate var mapLeftView: PMapLeftCalloutAccessoryView?
    /// 之前的数据数量
    fileprivate var lastMapDataCount: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closures()
        viewModel.getUserMapData()//获取数据
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactive.pan?.isEnabled = true        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactive.pan?.isEnabled = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("我的时空", comment: ""), attributeDic: attDic)
    }

    //MARK: 设置MapView
    fileprivate func setUI(){
        
        collectionViewController = PNewMapCollectionViewController()
        view.addSubview(collectionViewController!.view)
        collectionViewController?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(310/baseWidth)
        })
        collectionViewController?.viewModel = viewModel
        
        mapView = MKMapView()
        mapView?.showsUserLocation = false//展示用户点
        mapView?.showsPointsOfInterest = false//不展示景点
        mapView?.showsBuildings = false//不展示建筑物
        //不允许交互
        mapView?.isRotateEnabled = false
        mapView?.isPitchEnabled = false
        mapView?.isZoomEnabled = false
        mapView?.isScrollEnabled = false
        
        mapView?.delegate = self
        
        view.addSubview(mapView!)
        mapView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(collectionViewController!.view.snp.top)
        })
    }
    
    //MARK: 设置mapView的位置
    fileprivate func setMapViewRegin(location: CLLocation, andIndex index: Int){
        //先删除原先的 annotation
        if let annotation: ZTAnnotation = mapView?.annotations.first as? ZTAnnotation{
            mapView?.removeAnnotation(annotation)
        }
        
        //显示精度
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(location.coordinate, span)
        mapView?.setRegion(region, animated: false)
        
        var dateString: String
        if let date: Date = viewModel.mapDataArray[index].createDate{
            dateString = getDateText(date: date, andTimeStyle: DateFormatter.Style.none, andDateStyle: DateFormatter.Style.medium)
        }else{
            dateString = "时光记忆"
        }
        
        let annotation: ZTAnnotation = ZTAnnotation(coordinate: location.coordinate, title: dateString, subTitle: nil)
        mapView?.addAnnotation(annotation)
        mapView?.selectAnnotation(annotation, animated: true)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 成功获得地图数据
        viewModel.tableViewShouldReloadData = {[weak self] _ in
            self?.collectionViewController?.collectionView?.xzm_header.endRefreshing()
            self?.collectionViewController?.collectionView?.xzm_footer.endRefreshing()
            self?.collectionViewController?.collectionView?.reloadData()
            if self?.viewModel.mapDataPageIndex == 1{
                self?.collectionViewController?.collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
                if let mapDataModel: SpaceTimeDataModel = self?.viewModel.mapDataArray[0], mapDataModel.photoLocation != nil{
                    self?.setMapViewRegin(location: mapDataModel.photoLocation!, andIndex: 0)
                }
            }
            else{
                self?.collectionViewController?.collectionView?.scrollToItem(at: IndexPath(item: (self?.lastMapDataCount)! - 1, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            }
            self?.lastMapDataCount = (self?.viewModel.mapDataArray.count)!
        }
        
        //MARK: 没有更多数据了
        viewModel.tableViewGetNoMoreData = { [weak self] _ in
            self?.collectionViewController?.collectionView?.xzm_header.endRefreshing()
            self?.collectionViewController?.collectionView?.xzm_footer.endRefreshing()
            if self?.viewModel.mapDataPageIndex != 1 {
                self?.collectionViewController?.collectionView?.scrollToItem(at: IndexPath(item: (self?.viewModel.mapDataArray.count)! - 1, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            }
        }
        
        //MARK: 出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.collectionViewController?.collectionView?.xzm_header.endRefreshing()
            self?.collectionViewController?.collectionView?.xzm_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 左划刷新
        collectionViewController?.headerRefreshClosure = {[weak self] _ in
            self?.viewModel.mapDataPageIndex = 1
            self?.viewModel.getUserMapData()
        }
        
        //MARK: 右划刷新
        collectionViewController?.footerRefreshClosure = {[weak self] _ in
            self?.viewModel.getUserMapData()
        }
        
        //MARK: collectionView滚动了
        collectionViewController?.collectionViewScrolled = {[weak self] (indexPath: IndexPath) in
            if let mapDataModel: SpaceTimeDataModel = self?.viewModel.mapDataArray[indexPath.item], mapDataModel.photoLocation != nil{
                self?.setMapViewRegin(location: mapDataModel.photoLocation!, andIndex: indexPath.item)
            }
        }
        
        //MARK: 点击了cell
        collectionViewController?.didSelecteCell = {[weak self] (indexPath: IndexPath) in
            let vc: PNewMapDetailController = PNewMapDetailController()
            vc.mapData = self?.viewModel.mapDataArray[indexPath.item]
            //MARK: 删除成功
            vc.deleteSuccess = {[weak self] _ in
                self?.viewModel.mapDataPageIndex = 1
                self?.viewModel.getUserMapData()
            }
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
}

extension PNewMapController: MKMapViewDelegate{
    
    //MARK: 在地图上添加一个点
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: ZTAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: appReuseIdentifier.annotationReuseIndetifier) as? ZTAnnotationView
        if annotationView == nil {
            annotationView = ZTAnnotationView(annotation: annotation, reuseIdentifier: appReuseIdentifier.annotationReuseIndetifier)
            annotationView?.leftCalloutAccessoryView = nil
            annotationView?.rightCalloutAccessoryView = nil
        }
        annotationView?.image = UIImage(named: "annotationImageView")
        annotationView?.canShowCallout = true
        
        if mapLeftView == nil{
            mapLeftView = PMapLeftCalloutAccessoryView(frame: CGRect(x: 0, y: 0, width: 40/baseWidth, height: 40/baseWidth))
        }
        annotationView?.leftCalloutAccessoryView = mapLeftView
        
        return annotationView
    }
}
