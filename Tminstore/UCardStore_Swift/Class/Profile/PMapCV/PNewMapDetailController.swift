//
//  PNewMapDetailController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PNewMapDetailController: BaseViewController {
    
    fileprivate let userViewModel: PUserViewModel = PUserViewModel()
    
    fileprivate var currentDataType: DetailMapDataType = DetailMapDataType.comment
    
    fileprivate let viewModel: PMapViewModel = PMapViewModel()
    
    var mapData: SpaceTimeDataModel?
    
    var userInfoModel: UserInfoModel?
    
    /// 删除成功
    var deleteSuccess: (()->())?
    
    fileprivate var tableView: PMapTableView?
    fileprivate var tableViewHeaderView: PMapTableViewHeaderView?
    fileprivate var playerView: PMapPlayerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.getVideoComment(videoId: mapData!.videoId!)//请求视频评论的数据
        viewModel.getMemory(originalId: mapData!.originalId)//请求视频记忆的数据
        viewModel.getLike(videoId: mapData!.videoId!)//请求点赞数据
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("我的时空", comment: ""), attributeDic: attDic)
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        
        tableViewHeaderView = PMapTableViewHeaderView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 50/baseWidth))
        
        tableView = PMapTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
        
    }
    
    //MARK: 对数据进行操作的弹窗
    fileprivate func sheetAlert(){
        let ac: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        if mapData?.status == SpaceTimeStatus.normal{
            //取消发布
            let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消发布", comment: ""), style: UIAlertActionStyle.default, handler: {[weak self] (_) in
                self?.viewModel.cancelRelease(videoID: (self?.mapData?.videoId)!, andOriginalId: (self?.mapData?.originalId)!)
            })
            ac.addAction(cancelAction)
        }else{
            //重新发布
            let reReleaseAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("重新发布", comment: ""), style: UIAlertActionStyle.default, handler: { [weak self] (_) in
                self?.viewModel.reRelease(videoID: (self?.mapData?.videoId)!, andOriginalId: (self?.mapData?.originalId)!)
            })
            ac.addAction(reReleaseAction)
        }
        //删除
        let deleteAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("删除", comment: ""), style: UIAlertActionStyle.destructive) { [weak self] (_) in
            self?.deleteAlert(alertTitle: NSLocalizedString("确认删除？", comment: ""),  rightClosure: {
                self?.viewModel.deleteReleaseToMap(videoID: (self?.mapData?.videoId)!, andOriginalId: (self?.mapData?.originalId)!)
            })
        }
        ac.addAction(deleteAction)
        
        //取消按钮
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        ac.addAction(cancelAction)
        
        present(ac, animated: true, completion: nil)
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayerView(commentModel: VideoCommentModel){
        NotificationCenter.default.post(name:  NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)//停止播放视频
        if playerView == nil {
            playerView = PMapPlayerView()
        }
        playerView?.comment = commentModel
        navigationController?.view.addSubview(playerView!)
        playerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(navigationController!.view)
        })
        playerView?.appearAnimation()
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 请求视频评论数据成功
        viewModel.getVideoCommentSuccess = {[weak self] _ in
            self?.tableView?.reloadData()
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
            self?.tableViewHeaderView?.setCommentButtonTitle(number: (self?.viewModel.videoCommentArray.count)!)
        }
        
        //MARK: 请求视频记忆成功
        viewModel.getVideoMemorySuccess = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
            self?.tableViewHeaderView?.setMemoryButtonTitle(number: (self?.viewModel.videoMemoryArray.count)!)
        }
        
        //MARK: 获得点赞数成功
        viewModel.getLikeNumberSuccess = {[weak self] _ in
            self?.tableViewHeaderView?.setLikeButtonTitle(number: (self?.viewModel.likeModelArray.count)!)
        }
        
        //MARK: 获得点赞数据成功
        viewModel.getLikeDataSuccess = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshing()
            self?.tableView?.reloadData()
        }
        
        //MARK: 没有更多点赞数据了
        viewModel.noMoreLikeData = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 重新发布成功
        viewModel.reReleaseSuccessClosure = {[weak self] _ in
            self?.mapData?.status = SpaceTimeStatus.normal
            self?.successSVProgress(title: NSLocalizedString("重新发布成功", comment: ""))
        }
        //MARK: 取消发布成功
        viewModel.cancelSuccessClosure = {[weak self] _ in
            self?.mapData?.status = SpaceTimeStatus.cancel
            self?.successSVProgress(title: NSLocalizedString("取消发布成功", comment: ""))
        }
        //MARK: 删除成功
        viewModel.deleteSuccessClosure = {[weak self] _ in
            self?.successSVProgress(title: NSLocalizedString("删除成功", comment: ""))
            delay(seconds: 2, completion: {
                self?.deleteSuccess?()
                _ = self?.navigationController?.popViewController(animated: true)
            })
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击视频评论按钮的事件
        tableViewHeaderView?.touchCommentButton = {[weak self] _ in
            self?.currentDataType = DetailMapDataType.comment
            self?.tableView?.reloadData()
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        //MARK: 点击视频记忆按钮的事件
        tableViewHeaderView?.touchMemoryButton = {[weak self] _ in
            self?.currentDataType = DetailMapDataType.memory
            self?.tableView?.reloadData()
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        //MARK: 点击点赞的事件
        tableViewHeaderView?.touchLikeButton = {[weak self] _ in
            self?.currentDataType = DetailMapDataType.like
            self?.tableView?.reloadData()
            if self?.viewModel.isNoMoreLikeData == false{
                self?.tableView?.mj_footer.resetNoMoreData()
            }
        }
        
        //MARK: 上拉刷新
        tableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.getLike(videoId: (self?.mapData?.videoId)!)
        }
        
        //MARK: 成功获得其他用户信息
        userViewModel.getOtherUserInfoSuccess = {[weak self] (userInfoModel: UserInfoModel) in
            let vc: PUserController = PUserController()
            vc.userInfoModel = userInfoModel
            vc.umengViewName = "其他用户信息"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 获取用户信息失败
        userViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
}

extension PNewMapDetailController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        switch currentDataType {
        case DetailMapDataType.comment:
            return viewModel.videoCommentArray.count
        case DetailMapDataType.memory:
            return viewModel.videoMemoryArray.count
        default:
            return viewModel.likeModelArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell: PNewMapContentCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.PNewMapContentCellReuseIdentifier, for: indexPath) as! PNewMapContentCell
            cell.mapModel = mapData
            cell.userInfoModel = userInfoModel
            //MARK: 点击更多按钮的事件
            cell.touchMoreButtonClosure = {[weak self] _ in
                self?.sheetAlert()
            }
            return cell
        }else{
            let cell: PNewMapTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.PNewMapTableViewCellReuseIdentifer, for: indexPath) as! PNewMapTableViewCell
            
            cell.currentDataType = currentDataType
            
            //MARK: 点击播放按钮的事件
            cell.touchPlayButton = { [weak self] (comment: VideoCommentModel) in
                self?.setPlayerView(commentModel: comment)
            }
            
            //MARK: 点击用户头像, 进入用户详情界面
            cell.tapUserPortrait = { [weak self] (memberCode: String) in
                self?.userViewModel.requestOtherUserInfo(memberCode: memberCode)
            }
            
            switch currentDataType {
            case DetailMapDataType.comment:
                cell.comment = viewModel.videoCommentArray[indexPath.row]
            case DetailMapDataType.memory:
                cell.comment = viewModel.videoMemoryArray[indexPath.row]
            default:
                cell.comment = viewModel.likeModelArray[indexPath.row]
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0
        }
        return 50/baseWidth
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return nil
        }
        return tableViewHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 400/baseWidth
        }else{
            return 65/baseWidth
        }
    }
}
