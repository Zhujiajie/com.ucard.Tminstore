//
//  PMapCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMapCollectionViewCell: UICollectionViewCell {
    
    var postcardModel: PostcardModel?{
        didSet{
            setData()
        }
    }
    
    fileprivate var portraitView: UIImageView?
    fileprivate var nickNameLabel: UILabel?
    fileprivate var createTimeLabel: UILabel?
    fileprivate var videoThumbnailView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        portraitView = UIImageView()
        portraitView?.contentMode = UIViewContentMode.scaleAspectFill
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.equalTo(contentView)
            make.size.equalTo(CGSize(width: 23/baseWidth, height: 23/baseWidth))
        })
        portraitView?.layer.cornerRadius = 11.5/baseWidth
        portraitView?.layer.masksToBounds = true
        
        nickNameLabel = UILabel()
        nickNameLabel?.textColor = UIColor(hexString: "40F1F8")
        nickNameLabel?.font = UIFont.systemFont(ofSize: 12)
        nickNameLabel?.numberOfLines = 1
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.equalTo(contentView)
            make.left.equalTo(portraitView!.snp.right).offset(2.5/baseWidth)
        })
        
        createTimeLabel = UILabel()
        createTimeLabel?.font = UIFont.systemFont(ofSize: 8)
        createTimeLabel?.textColor = UIColor(hexString: "929292")
        createTimeLabel?.numberOfLines = 1
        contentView.addSubview(createTimeLabel!)
        createTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(2/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.right.equalTo(contentView)
        })
        
        videoThumbnailView = UIImageView()
        videoThumbnailView?.contentMode = UIViewContentMode.scaleAspectFill
        videoThumbnailView?.clipsToBounds = true
        contentView.addSubview(videoThumbnailView!)
        videoThumbnailView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(createTimeLabel!.snp.bottom)
            make.left.equalTo(contentView)
            make.size.equalTo(CGSize(width: 73/baseWidth, height: 68/baseWidth))
        })
        videoThumbnailView?.layer.masksToBounds = true
        
        let playerIconImageView: UIImageView = UIImageView(image: UIImage(named: "playButton"))
        playerIconImageView.contentMode = UIViewContentMode.scaleAspectFit
        videoThumbnailView?.addSubview(playerIconImageView)
        playerIconImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(videoThumbnailView!)
            make.size.equalTo(CGSize(width: 23/baseWidth, height: 23/baseWidth))
        }
        
        //添加通知
        NotificationCenter.default.addObserver(self, selector: #selector(setPlayerStateToFalse(info:)), name: NSNotification.Name(rawValue: LocalTimeLineCellNotificationName), object: nil)
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        portraitView?.sd_setImage(with: postcardModel?.userLogoURL)
        nickNameLabel?.text = postcardModel?.nickName ?? " "
        if let createDate: Date = postcardModel?.createDate{
            createTimeLabel?.text = getDateText(date: createDate, andDateStyle: DateFormatter.Style.short)
        }
        videoThumbnailView?.sd_setImage(with: postcardModel?.videoThumbnailURL)
    }
    
    //MARK: 设置播放状态
    /// 设置播放状态
    ///
    /// - Parameter isPlaying: 是否在播放
    func setPlayState(isPlaying: Bool){
        if isPlaying{
            videoThumbnailView?.layer.borderColor = UIColor(hexString: "67E4EF").cgColor
        }else{
            videoThumbnailView?.layer.borderColor = UIColor.clear.cgColor
        }
    }
    
    //MARK: 接受通知，将cell的播放状态设置为不播放
    func setPlayerStateToFalse(info: Notification){
        setPlayState(isPlaying: false)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        portraitView?.image = nil
        nickNameLabel?.text = nil
        createTimeLabel?.text = nil
        videoThumbnailView?.image = nil
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
