//
//  PMapPlayerView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMapPlayerView: UIVisualEffectView {

    var comment: VideoCommentModel?{
        didSet{
            if comment != nil{
                playAgainButton?.isHidden = true
                videoThumbnalImageView?.sd_setImage(with: comment?.thumbnailURL)
                player?.setVideoFullURL(fullVideoURL: comment!.videoURL!)
                player?.playFromBeginning()
            }
        }
    }
    
    var recordModel: RecordModel?{
        didSet{
            if recordModel != nil{
                playAgainButton?.isHidden = true
                videoThumbnalImageView?.sd_setImage(with: recordModel?.thumbnailURL)
                player?.setVideoFullURL(fullVideoURL: recordModel!.videoURL!)
                player?.playFromBeginning()
            }
        }
    }
    
    /// 展示视频缩略的imageView
    fileprivate var videoThumbnalImageView: BaseImageView?
    /// 用来展示摄像头的layer
    fileprivate var player: ZTPlayer?
    /// 重新播放的按钮
    fileprivate var playAgainButton: BaseImageView?
    /// 加载动画
    fileprivate var activeIndicator: UIActivityIndicatorView?
    /// 退出的按钮
    fileprivate var dismissButton: BaseButton?
    
    override init(effect: UIVisualEffect?) {
        
        if #available(iOS 10.0, *) {
            super.init(effect: nil)
        }else{
            super.init(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
            self.alpha = 0
        }
        
        player = ZTPlayer()
        player?.view.frame = CGRect(x: 34/baseWidth, y: 52/baseWidth, width: 306/baseWidth, height: 484/baseWidth)
        player?.playbackLoops = true
        player?.playerDelegate = self
        player?.playbackDelegate = self
        player?.layerBackgroundColor = UIColor.clear//背景透明
        contentView.addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            self?.playAgainButton?.isHidden = false
        }
        
        //添加手势。控制播放暂停
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToControlPlayer(tap:)))
        player?.view.addGestureRecognizer(tap)

        //视频缩略图
        videoThumbnalImageView = BaseImageView.normalImageView()
        contentView.insertSubview(videoThumbnalImageView!, belowSubview: player!.view)
        videoThumbnalImageView?.frame = player!.view.frame
        
        //视频加载指示器
        activeIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        videoThumbnalImageView?.addSubview(activeIndicator!)
        activeIndicator?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(videoThumbnalImageView!)
        })
        activeIndicator?.hidesWhenStopped = true
        
        //重新开始播放的按钮
        playAgainButton = BaseImageView.normalImageView(andImageName: "playButton")
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 80/baseWidth, height: 80/baseWidth))
        })
        playAgainButton?.isHidden = true
        
        //退出的按钮
        dismissButton = BaseButton.normalIconButton(iconName: "detailMapDismiss")
        contentView.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(player!.view.snp.bottom).offset(34/baseWidth)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 38/baseWidth, height: 38/baseWidth))
        })
        
        //MARK: 点击退出按钮的事件
        dismissButton?.touchButtonClosure = {[weak self] _ in
            self?.player?.stop()
            self?.dismissAnimation()
        }
    }
    
    //MARK: 出现的动画
    /// 出现的动画
    func appearAnimation(){
        if #available(iOS 10.0, *) {
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
                self?.effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
                self?.contentView.alpha = 1
                }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.3, animations: { [weak self] _ in
                self?.alpha = 1
            })
        }
    }
    
    //MARK: 消失的动画
    /// 消失的动画
    func dismissAnimation(){
        if #available(iOS 10.0, *) {
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
                self?.effect = nil
                self?.contentView.alpha = 0
                }, completion: {[weak self] _ in
                    self?.removeFromSuperview()
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: { [weak self] _ in
                self?.alpha = 0
            })
        }
    }
    
    //MARK: 点击开始播放，或者停止播放
    func tapToControlPlayer(tap: UITapGestureRecognizer){
        if player?.playbackState == PlaybackState.playing{
            player?.pause()
            playAgainButton?.isHidden = false
        }else if player?.playbackState == PlaybackState.paused{
            player?.playFromCurrentTime()
            playAgainButton?.isHidden = true
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PMapPlayerView: PlayerDelegate, PlayerPlaybackDelegate{
    
    func playerReady(_ player: Player) {
    }
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    //MARK: 根据缓存情况，展示加载提示器
    func playerBufferingStateDidChange(_ player: Player) {
        switch player.bufferingState {
        case BufferingState.ready:
            activeIndicator?.stopAnimating()
        default:
            //            activeIndicator?.startAnimating()
            break
        }
    }
    
    func playerCurrentTimeDidChange(_ player: Player){
    }
    func playerPlaybackWillStartFromBeginning(_ player: Player){
    }
    func playerPlaybackDidEnd(_ player: Player){
    }
    func playerPlaybackWillLoop(_ player: Player){
    }
}
