//
//  PMapTableViewHeaderView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMapTableViewHeaderView: UITableViewHeaderFooterView {
    
    /// 点击视频评论按钮的事件
    var touchCommentButton: (()->())?
    /// 点击视频记忆按钮的事件
    var touchMemoryButton: (()->())?
    /// 点击点赞的事件
    var touchLikeButton: (()->())?
    
    /// 视频评论的按钮
    fileprivate var commentButton: BaseButton?
    /// 视频记忆的按钮
    fileprivate var memoryButton: BaseButton?
    /// 点赞的按钮
    fileprivate var likeButton: BaseButton?
    /// 底部绿色的线
    fileprivate var greenLine: BaseView?
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.backgroundView = UIView(frame: self.bounds)
        self.backgroundView?.backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        let visualView: BaseVisualEffectView = BaseVisualEffectView.lightVisualEffectView()
        contentView.addSubview(visualView)
        visualView.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(self)
        }
        
        commentButton = BaseButton.leftImageAndRightTitleButton(iconName: "videoComent", andFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "A7A7A7", andTitle: "0")
        contentView.addSubview(commentButton!)
        commentButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(33/baseWidth)
            make.size.equalTo(CGSize(width: 80/baseWidth, height: 24/baseWidth))
        })
        //MARK: 点击评论按钮的事件
        commentButton?.touchButtonClosure = {[weak self] _ in
            self?.setGreenLineLocation(button: (self?.commentButton)!)
            self?.touchCommentButton?()
        }
        
        memoryButton = BaseButton.leftImageAndRightTitleButton(iconName: "leavedMemory", andFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "A7A7A7", andTitle: "0")
        contentView.addSubview(memoryButton!)
        memoryButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(commentButton!)
            make.left.equalTo(commentButton!.snp.right).offset(11/baseWidth)
            make.size.equalTo(commentButton!)
        })
        //MARK: 点击记忆按钮的事件
        memoryButton?.touchButtonClosure = {[weak self] _ in
            self?.setGreenLineLocation(button: (self?.memoryButton)!)
            self?.touchMemoryButton?()
        }
        
        likeButton = BaseButton.leftImageAndRightTitleButton(iconName: "mapDetailLikeButton", andFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "A7A7A7", andTitle: "0")
        contentView.addSubview(likeButton!)
        likeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(commentButton!)
            make.left.equalTo(memoryButton!.snp.right).offset(11/baseWidth)
            make.size.equalTo(memoryButton!)
        })
        //MARK: 点击点赞按钮的事件
        likeButton?.touchButtonClosure = {[weak self] _ in
            self?.setGreenLineLocation(button: (self?.likeButton)!)
            self?.touchLikeButton?()
        }
        
        greenLine = BaseView.colorLine(colorString: "00EAFF")
        contentView.addSubview(greenLine!)
        greenLine?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(contentView)
            make.centerX.equalTo(commentButton!)
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 1))
        })
    }
    
    //MARK: 设置绿色线的位置
    fileprivate func setGreenLineLocation(button: BaseButton){
        greenLine?.snp.remakeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(contentView)
            make.centerX.equalTo(button)
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 1))
        })
        autoLayoutAnimation(view: self)
    }
    
    //MARK: 设置视频评论按钮的标题
    /// 设置视频评论按钮的标题
    ///
    /// - Parameter number: 评论数量
    func setCommentButtonTitle(number: Int){
        commentButton?.setTitle(font: UIFont.systemFont(ofSize: 14), andTextColorHexString: "A7A7A7", andTitle: "\(number)")
    }
    
    //MARK: 设置记忆按钮的标题
    /// 设置记忆按钮的标题
    ///
    /// - Parameter number: 记忆数量
    func setMemoryButtonTitle(number: Int){
        memoryButton?.setTitle(font:UIFont.systemFont(ofSize: 14), andTextColorHexString: "A7A7A7", andTitle: "\(number)")
    }
    
    //MARK: 设置点赞按钮的数量
    /// 设置点赞按钮的数量
    ///
    /// - Parameter number: 点赞数
    func setLikeButtonTitle(number: Int){
        likeButton?.setTitle(font:UIFont.systemFont(ofSize: 14), andTextColorHexString: "A7A7A7", andTitle: "\(number)")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
