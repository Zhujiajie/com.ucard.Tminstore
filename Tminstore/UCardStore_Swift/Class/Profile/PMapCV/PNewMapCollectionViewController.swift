//
//  PNewMapCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: -----------展示时光圈数据的collectionView----------------

import UIKit
import SnapKit
import SDWebImage

private let reuseIdentifier = "Cell"

class PNewMapCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout{

    weak var viewModel: PMainViewModel?
    
    /// header刷新
    var headerRefreshClosure: (()->())?
    /// footer刷新
    var footerRefreshClosure: (()->())?
    /// 滚动了collectionView
    var collectionViewScrolled: ((_ indexPath: IndexPath)->())?
    /// 点击了cell
    var didSelecteCell: ((_ indexPath: IndexPath)->())?
    
//    ///每个cell的缩进
//    fileprivate let cellInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
    
    init() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: screenWidth, height: 309/baseWidth)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView?.backgroundColor = UIColor(hexString: "#F8F8F8")
        self.collectionView?.isPagingEnabled = true

        // Register cell classes
        self.collectionView!.register(PNewMapCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        self.collectionView?.xzm_addNormalHeader(callback: {[weak self] _ in
            self?.headerRefreshClosure?()
        })
        
        self.collectionView?.xzm_addNormalFooter(callback: {[weak self] _ in
            self?.footerRefreshClosure?()
        })
        
        self.collectionView?.xzm_header.isUpdatedTimeHidden = true
    }

    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        return viewModel!.mapDataArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PNewMapCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PNewMapCollectionViewCell
        cell.spaceTimeModel = viewModel!.mapDataArray[indexPath.item]
        return cell
    }
    
//    //MARK: 每个Cell的缩进
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return cellInsets
//    }
    
    // MARK: UICollectionViewDelegate
    //MARK: 选择一个cell
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelecteCell?(indexPath)
    }
    
    //MARK: 滚动cell
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexPath: IndexPath = IndexPath(item: Int(scrollView.contentOffset.x / screenWidth), section: 0)
        collectionViewScrolled?(indexPath)
    }
}

class PNewMapCollectionViewCell: UICollectionViewCell{
    
    var spaceTimeModel: SpaceTimeDataModel?{
        didSet{
            setData()
        }
    }
    
    ///图片
    fileprivate var imageView: BaseImageView?
    /// 地址label
    fileprivate var addressLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.equalTo(contentView).offset(8)
            make.right.equalTo(contentView).offset(-8)
            make.height.equalTo(238/baseWidth)
        })
        
        let whiteContainerView: BaseView = BaseView.colorLine(colorString: "#FFFFFF")
        contentView.addSubview(whiteContainerView)
        whiteContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(imageView!.snp.bottom)
            make.bottom.equalTo(contentView)
            make.leftMargin.equalTo(imageView!)
            make.rightMargin.equalTo(imageView!)
        }
        
        let locationIcon: BaseImageView = BaseImageView.normalImageView(andImageName: "clueLocationIcon")
        whiteContainerView.addSubview(locationIcon)
        locationIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(imageView!.snp.bottom).offset(18/baseWidth)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.size.equalTo(CGSize(width: 25/baseWidth, height: 25/baseWidth))
        }
        
        addressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "747474", andNumberOfLines: 2)
        whiteContainerView.addSubview(addressLabel!)
        addressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(imageView!.snp.bottom).offset(10/baseWidth)
            make.bottom.equalTo(contentView).offset(-10/baseWidth)
            make.left.equalTo(locationIcon.snp.right).offset(10/baseWidth)
            make.right.equalTo(contentView).offset(-10/baseWidth)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        if let url: URL = spaceTimeModel?.frontImageURL{
            //先判断是否已缓存图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                
                if result == true{
                    self?.imageView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.spaceTimeModel?.frontPlaceImageURL{
                        self?.imageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.imageView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.imageView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.imageView?.sd_setImage(with: url)
                    }
                }
            })
        }
        
        addressLabel?.text = spaceTimeModel?.detailedAddress
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
