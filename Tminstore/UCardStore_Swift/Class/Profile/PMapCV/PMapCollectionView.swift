//
//  PMapCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PMapCollectionView: UICollectionView {

    /// header刷新
    var headerRefreshClosure: (()->())?
    /// footer刷新
    var footerRefreshClosure: (()->())?
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumInteritemSpacing = 30
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: 79/baseWidth, height: 101/baseWidth)
        
        super.init(frame: frame, collectionViewLayout: layout)
        
        backgroundColor = UIColor.white
        scrollsToTop = false
        register(PMapCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.PMapCollectionViewCellReuseIdentifier)
        
//        addRefreshFooter(closure: {[weak self] _ in
//            self?.footerRefreshClosure?()
//        })
//        addRefreshHeader(closure: {[weak self] _ in
//            self?.headerRefreshClosure?()
//        })
//        
//        refreshFooterFont = UIFont.systemFont(ofSize: 11)
//        refreshFooterTextColor = UIColor(hexString: "878686")
//        refreshHeaderFont = refreshFooterFont
//        refreshHeaderTextColor = refreshFooterTextColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
