//
//  PLoginViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/14.
//  Copyright © 2016年 ucard. All rights reserved.
//

import Instabug

/// 用于微信回调方法的验证
let WXStateForCheck: String = "UCardStore_Swift_WeChat"

class PLoginViewModel: BaseViewModel {

    fileprivate let dataModel: PLoginDataModel = PLoginDataModel()
    
    /// .net 用户需要更新密码
    var dotNetUpdatePassword: (()->())?
    
    /// 登录成功的闭包
    var loginSuccessClosure: (()->())?
    
    /// 登录错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 邮箱或手机号不正确的闭包
    var accountErrorClosure: (()->())?
    
    /// 手机号码不正确的闭包
    var phoneNumberErrorClosure: (()->())?
    
    /// 获取验证码失败
    var requestVerificationCodeFailed: (()->())?
    
    /// 验证验证成功的闭包
    var verifySuccess: (()->())?
    
    /// 验证码验证失败的闭包
    var verifyFalied: ((_ message: String?)->())?
    
    /// 密码不合法的闭包
    var passwordErrorClosure: (()->())?
    
    /// 邮箱注册成功的闭包
    var emailRegistSuccess: (()->())?
    
    /// 邮箱不合法
    var emailErrorClosure: (()->())?
    
    //MARK: 上传pushID
    /**
     上传pushID
     */
    func postPushID() {
        dataModel.postJPushID()
    }
    
//MARK: -------------第三方登录请求--------------------
    //MARK: 微信发送登录请求
    /**
     微信发送登录请求
     */
    func weChatLogin() {
        
        UMSocialManager.default().getUserInfo(with: UMSocialPlatformType.wechatSession, currentViewController: nil) { [weak self] (result: Any?, error: Error?) in
            if let response: UMSocialUserInfoResponse = result as? UMSocialUserInfoResponse, error == nil{
                self?.handelWeChatCallBack(response: response)
            }
                //授权失败
            else{
                let result: [String: Any] = ["message": NSLocalizedString("授权失败", comment: "")] as [String: Any]
                self?.errorClosure?(nil, result)
            }
        }
    }

    //MARK: 发送微博第三方登录请求
    /**
     发送微博第三方登录请求
     */
    func weiboLogin() {
        
        UMSocialManager.default().getUserInfo(with: UMSocialPlatformType.sina, currentViewController: nil) { [weak self] (result: Any?, error: Error?) in
            if let response: UMSocialUserInfoResponse = result as? UMSocialUserInfoResponse, error == nil{
                self?.handleWeiBoCallBack(response: response)
            }
            //授权失败
            else{
                let result: [String: Any] = ["message": NSLocalizedString("授权失败", comment: "")] as [String: Any]
                self?.errorClosure?(nil, result)
            }
        }
    }
    
    //MARK: 从微信返回到本app，根据回调的code获取accessToken
    /**
     从微信返回到本app，根据回调的code获取accessToken
     
     - parameter url:     微信返回的url，包含code参数
     */
    fileprivate func handelWeChatCallBack(response: UMSocialUserInfoResponse){
        
        showSVProgress(title: nil)
        
        dataModel.weChatLogin(accessToken: response.accessToken, andOpenId: response.openid, successClosure: {[weak self](result: [String: Any]) in
            self?.analysisLoginResult(result: result, withAccount: "weChat")
            }, failureClourse: { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
        }, completionClosure: {[weak self] _ in
            self?.dismissSVProgress()
        })
    }
    
    //MARK: 处理微博登录的回调
    fileprivate func handleWeiBoCallBack(response: UMSocialUserInfoResponse){
        showSVProgress(title: nil)
        dataModel.weiBoLogin(accessToken: response.accessToken, andUserId: response.uid, successClosure: { [weak self] (result: [String: Any]) in
            self?.analysisLoginResult(result: result, withAccount: "weiBo")
            }, failureClourse: {[weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
}
//
//extension PLoginViewModel{
//    
//    //MARK: 处理各种闭包回调
//    /**
//     处理各种闭包回调
//     */
//    fileprivate func cloureResponse(){
//        //MARK:----------------------第三方回调-------------------
//        //MARK: ---------------------微信回调。返回url-------------------------
//        app.weChatLoginClosure = {[weak self] (response: SendAuthResp) in
//            //获取accsessToken
//            self?.handelWeChatCallBack(response: response)
//        }
//        
//        //MARK: ---------------------微博回调。返回url-------------------------
//        app.weiboLoginClosure = {[weak self] (response: WBAuthorizeResponse) in
//            self?.handleWeiBoCallBack(response: response)
//        }
//    }
//}

//MARK: 登录相关网络请求
extension PLoginViewModel{
    
    //MARK: 登录的请求
    /**
     登录的请求，自动判断邮箱和手机号
     
     - parameter account:  账号
     - parameter password: 密码
     */
    func login(account: String?, andPassword password: String?) {
        
        if account == nil || account == ""{ return }
        if password == nil || password == "" { return }
        
        showSVProgress(title: "登录中")
        if checkEmailAndPassword(email: account!, andPassword: password!) == true {
            //邮箱登录
            dataModel.loginRequest(email: account!, andPassword: password!, successClosure: { [weak self](result: [String: Any]) in
                self?.analysisLoginResult(result: result, withAccount: account!)//分析登录结果
                }, failureClourse: { [weak self] (error: Error?) in
                    self?.errorClosure?(error, nil)
                }, completionClosure: { [weak self] _ in
                    self?.dismissSVProgress()
            })
        }
        
        else if checkPhoneNumber(number: account!, andPassword: password!) == true{
            //手机登录
            dataModel.phoneNumberLoginRequest(number: account!, andPassword: password!, successClosure: { [weak self](result: [String: Any]) in
                self?.analysisLoginResult(result: result, withAccount: account!)
                }, failureClourse: { [weak self] (error: Error?) in
                    self?.errorClosure?(error, nil)
                }, completionClosure: { [weak self] _ in
                    self?.dismissSVProgress()
            })
        }
        else{
            accountErrorClosure?()//邮箱和手机号码都错误
            dismissSVProgress()
        }
    }
    
    //MARK: 请求验证码
    /// 请求验证码，判断手机号码是否正确
    ///
    /// - Parameter phoneNumber: 手机号码
    /// - Returns: Bool
    func requestVerificationCode(phoneNumber: String?)->Bool{
        // 判断手机号码是否正确
        if phoneNumber == nil || phoneNumber == "" || dataModel.checkPhoneNumber(phoneNumber: phoneNumber!) == false{
            phoneNumberErrorClosure?()
            return false
        }
        dataModel.requestVerificationCode(phoneNumber: phoneNumber!, successClosure: { [weak self](result: [String: Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                print("发送验证码成功")
            }else{
                self?.errorClosure?(nil, result)
                self?.requestVerificationCodeFailed?()
            }
        }) { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
                self?.requestVerificationCodeFailed?()
        }
        return true
    }
    
    //MARK: 验证验证码的请求
    /// 验证验证码的请求
    ///
    /// - Parameters:
    ///   - phoneNumber: 手机号码
    ///   - verificationCode: 验证码
    func verifyVerificationCode(phoneNumber: String?, withVerificationCode verificationCode: String?){

        if phoneNumber == nil || phoneNumber == "" || verificationCode == nil || verificationCode == ""{
            return
        }
        if dataModel.checkPhoneNumber(phoneNumber: phoneNumber!) == false{
            phoneNumberErrorClosure?()//手机号码错误
        }else{
            showSVProgress(title: nil)
            dataModel.verifyVerificationCode(phoneNumber: phoneNumber!, withVerificationCode: verificationCode!, successClosure: { [weak self](result: [String: Any]) in
                if let code: Int = result["code"] as? Int, code == 1000{
                    savePhoneVerificationCode(code: verificationCode!)//保存验证码
                    savePhoneNumber(number: phoneNumber!)//保存手机号码
                    self?.verifySuccess?()//验证码验证正确
                }else{
                    self?.errorClosure?(nil, result)
                }
                }, failureClourse: { [weak self](error: Error?) in
                    self?.errorClosure?(error, nil)
                }, completionClosure: { [weak self] _ in
                    self?.dismissSVProgress()
            })
        }
    }
    
    //MARK: 邮箱注册的请求
    /**
     邮箱注册的请求
     
     - parameter email:    邮箱
     - parameter password: 密码
     */
    func emailSignUp(email: String?, andPassword password: String?) {
        
        if email == nil || password == nil || email == "" || password == ""{
            return
        }
        
        if checkEmailAndPassword(email: email!, andPassword: password!) == false {
            emailErrorClosure?()
            return
        }//检测邮箱和密码的合法性
        
        showSVProgress(title: "注册中")
        
        dataModel.signupRequest(email: email!, andPassword: password!, successClosure: { [weak self] (result: [String: Any]) in
            self?.analysisLoginResult(result: result, withAccount: email!, andIsSignUp: true)
            }, failureClourse: { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
}

extension PLoginViewModel{
    
    //MARK: 检查邮箱地址和密码是否合法
    /**
     检查邮箱地址和密码是否合法
     
     - parameter email:    邮箱
     - parameter password: 密码
     - parameter ignore:   是否忽略检查密码，默认不忽略，false
     
     - returns: Bool
     */
    fileprivate func checkEmailAndPassword(email: String, andPassword password: String) -> Bool{
        
        //检查邮箱地址是否合法
        if dataModel.checkEmailAddress(emailString: email) == false{
            return false
        }
        
        //检查密码是否合法
        switch password.characters.count {
        case 6...16:
            return true
        default:
            passwordErrorClosure?()
            return false
        }
    }
    
    //MARK: 检查手机号码的正确性
    fileprivate func checkPhoneNumber(number: String, andPassword password: String)->Bool{
        
        //检查邮箱地址是否合法
        if dataModel.checkPhoneNumber(phoneNumber: number) == false{
            return false
        }
        
        //检查密码是否合法
        switch password.characters.count {
        case 6...16:
            return true
        default:
            passwordErrorClosure?()
            return false
        }
    }
    
    //MARK: 解析登录或注册的结果
    /// 解析登录或注册的结果
    ///
    /// - Parameters:
    ///   - result: 服务器返回数据
    ///   - account: 用户账号
    fileprivate func analysisLoginResult(result: [String: Any], withAccount account: String, andIsSignUp isSignUp: Bool = false){
        
//        print(result)
        //登录成功
        if let code: Int = result["code"] as? Int, code == 1000{
            
            if let message: String = result["message"] as? String{
                if message == ".net密码修改"{
                    dotNetUpdatePassword?()
                    return
                }
            }
            let data: [String: Any] = result["data"] as! [String: Any]
            let user: [String: Any] = data["user"] as! [String: Any]
            let token: String       = user["tminstoreToken"] as! String
            let userID: String      = user["memberCode"] as! String
            let userName: String    = user["nickName"] as! String
            //注册Instabug
            if let email: String = user["email"] as? String{
                Instabug.identifyUser(withEmail: email, name: userName)
            }else if let phone: String = user["phone"] as? String{
                Instabug.identifyUser(withEmail: phone, name: userName)
            }
            
            setLoginStatus(loginStatus: true)//保存用户登录状态
            saveUserID(userId: userID)
            saveUserName(name: userName)
            saveUserToken(token: token)//登录成功，保存token
            
            if isSignUp == true{
                emailRegistSuccess?()//注册成功的闭包
            }else{
                loginSuccessClosure?()//成功的闭包
            }
        }else{
            errorClosure?(nil, result)//登录失败
        }
    }
}
