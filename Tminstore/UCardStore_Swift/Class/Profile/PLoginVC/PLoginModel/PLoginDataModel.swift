//
//  PLoginDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/15.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Alamofire

let USUserInfoEntity: String = "USUserInfo"

class PLoginDataModel: BaseDataModel {
    
//MARK:-------------------网络请求----------------------
    
    //MARK:邮箱登录
    /**
     邮箱登录
     
     - parameter email:      邮箱地址
     - parameter password:   密码
     - parameter success:    成功的闭包
     - parameter failure:    失败的闭包
     - parameter completion: 结束的闭包
     */
    func loginRequest(email: String, andPassword password: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        let parameters: [String: Any] = [
            "email": email,
            "password": password] as [String: Any]
        
        requestWithoutHeaders(urlStr: appAPIHelp.emailLoginAPI, httpMethod: HTTPMethod.post, parameters: parameters, successClosure: {(result: [String: Any]) in
            success?(result)
        }, failureClosure: {(error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
 
    //MARK: 手机号码登录
    /// 手机号码登录
    ///
    /// - Parameters:
    ///   - number: 手机号码
    ///   - password: 密码
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 结束的闭包
    func phoneNumberLoginRequest(number: String, andPassword password: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        let parameters: [String: Any] = [
            "phone": number,
            "password": password] as [String: Any]
        
        requestWithoutHeaders(urlStr: appAPIHelp.phoneNumberLoginAPI, httpMethod: HTTPMethod.post, parameters: parameters, successClosure: {(result: [String: Any]) in
            success?(result)
        }, failureClosure: {(error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 请求验证码的请求
    /// 请求验证码的请求
    ///
    /// - Parameters:
    ///   - phoneNumber: 手机号码
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    func requestVerificationCode(phoneNumber: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?){
        
        let parameters: [String: Any] = ["phone": phoneNumber] as [String: Any]
        requestWithoutHeaders(urlStr: appAPIHelp.requestPhoneVerificationCodeAPI, httpMethod: HTTPMethod.get, parameters: parameters, urlEncoding: URLEncoding.default, successClosure: {(result: [String: Any]) in
            success?(result)
        }, failureClosure: {(error: Error?) in
            failure?(error)
        }, completionClosure: nil)
    }
    
    //MARK: 验证验证码的请求
    /// 验证验证码的请求
    ///
    /// - Parameters:
    ///   - phoneNumber: 手机号码
    ///   - verificationCode: 验证码
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func verifyVerificationCode(phoneNumber: String, withVerificationCode verificationCode: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "phone": phoneNumber,
            "code": verificationCode] as [String: Any]
        
        requestWithoutHeaders(urlStr: appAPIHelp.verifyVerificationCodeAPI, httpMethod: HTTPMethod.get, parameters: parameters, urlEncoding: URLEncoding.default, successClosure: {(result: [String: Any]) in
            success?(result)
        }, failureClosure: {(error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK:注册的网络请求
    /**
     注册的网络请求
     
     - parameter email:      邮箱地址
     - parameter password:   密码
     - parameter success:    成功的闭包
     - parameter failure:    失败的闭包
     - parameter completion: 完成的闭包
     */
    func signupRequest(email: String, andPassword password: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        let parameters: [String: Any] = ["email": email,
                          "password": password] as [String: Any]
        
        requestWithoutHeaders(urlStr: appAPIHelp.emailRegistAPI, httpMethod: HTTPMethod.post, parameters: parameters, successClosure: {(result: [String: Any]) in
            success?(result)
        }, failureClosure: {(error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 获取微信access_token
    /**
     根据微信回调的code，获取微信access_token

     
     - parameter code:    微信回调code
     - parameter success: 成功的闭包
     - parameter failure: 失败的闭包
     */
    func getWeChatAccessToken(code: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?){
        
        let parameters: [String: Any] = [
            "appid": appAPIHelp.WeiChatAppKey,
            "secret": appAPIHelp.WeiChatAppSecret,
            "code": code,
            "grant_type": "authorization_code"] as [String: Any]
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.GetWeiChatAccessAPI, ParametersDictionary: parameters, urlEncoding: URLEncoding.default, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: nil)
    }
    
    //MARK: 微信登录接口
    /// 微信登录接口
    ///
    /// - Parameters:
    ///   - accessToken: accessToken
    ///   - openId: openId
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func weChatLogin(accessToken: String, andOpenId openId: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "access_token": accessToken,
            "openid": openId] as [String: Any]
        
        requestWithoutHeaders(urlStr: appAPIHelp.weChatLoginAPI, httpMethod: HTTPMethod.post, parameters: parameters, successClosure: {(result: [String: Any]) in
            success?(result)
        }, failureClosure: {(error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 微博登录的接口
    /// 微博登录的接口
    ///
    /// - Parameters:
    ///   - accessToken: accessToken
    ///   - userId: userId
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func weiBoLogin(accessToken: String, andUserId userId: String, successClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "access_token": accessToken,
            "uid": userId] as [String: Any]
        
        requestWithoutHeaders(urlStr: appAPIHelp.weiBoLoginAPI, httpMethod: HTTPMethod.post, parameters: parameters, successClosure: {(result: [String: Any]) in
            success?(result)
        }, failureClosure: {(error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
}

//MARK: 辅助方法
extension PLoginDataModel{
    
    //MARK: 检查输入的邮箱地址是否合法
    /**
     检查输入的邮箱地址是否合法
     
     - parameter emailString: 邮箱地址的字符串
     
     - returns: 合法则返回true，否则返回nil
     */
    func checkEmailAddress(emailString: String?) -> Bool {
        if emailString == nil { return false }
        do {
            let regex: NSRegularExpression = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
            return regex.firstMatch(in: emailString!, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, emailString!.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    //MARK: 判断手机号码合法性
    /// 判断手机号码合法性
    ///
    /// - Parameter phoneNumber: 手机号码
    /// - Returns: Bool
    func checkPhoneNumber(phoneNumber: String?)->Bool{
        
        if phoneNumber == nil {return false}
        
        /**
         * 手机号码
         * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
         * 联通：130,131,132,152,155,156,185,186
         * 电信：133,1349,153,180,189
         */

        let mobile: String = "^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$"
        
        /**
         10         * 中国移动：China Mobile
         11         * 134[0-8],135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188 (147,178)
         12         */
        
        let cm: String = "^1(34[0-8]|(3[5-9]|47|5[0127-9]|78|8[2-478])\\d)\\d{7}$"
        
        /**
         15         * 中国联通：China Unicom
         16         * 130,131,132,155,156,176,185,186
         17         */
        let cu: String = "^1(3[0-2]|5[56]|76|8[56])\\d{8}$"
        
        /**
         20         * 中国电信：China Telecom
         21         * 133,153,173,177,180,181,189
         22         */
        let ct: String = "^1((33|53|7[39]|8[019])[0-9]|349)\\d{7}$"
        
        /**
         25         * 大陆地区固话及小灵通
         26         * 区号：010,020,021,022,023,024,025,027,028,029
         27         * 号码：七位或八位
         28         */
        // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
        
        let regextestmobile: NSPredicate = NSPredicate(format: "SELF MATCHES %@", mobile)
        let regextestcm: NSPredicate     = NSPredicate(format: "SELF MATCHES %@", cm)
        let regextestcu: NSPredicate     = NSPredicate(format: "SELF MATCHES %@", cu)
        let regextestct: NSPredicate     = NSPredicate(format: "SELF MATCHES %@", ct)
        
        if (regextestmobile.evaluate(with: phoneNumber) == true
            || regextestcm.evaluate(with: phoneNumber) == true
            || regextestct.evaluate(with: phoneNumber) == true
            || regextestcu.evaluate(with: phoneNumber) == true){
            return true
        }else{
            return false
        }
    }
    
    //MARK: 保存用户发送忘记密码请求时，输入的邮箱地址
    /**
     保存用户发送忘记密码请求时，输入的邮箱地址
     
     - parameter email: email
     */
    func saveEmailOfForgetPassword(email: String) {
        UserDefaults.standard.set(email, forKey: "emailOfForgetPassword")
    }
}
