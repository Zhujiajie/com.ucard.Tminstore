//
//  PLoginLabel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/22.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class PLoginLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    //MARK: 标题lable
    /**
     标题lable
     
     - returns: PLoginTitleLabel
     */
    class func titleLabel()->PLoginLabel{
        
        let titleLabel: PLoginLabel = PLoginLabel()
        
        titleLabel.text          = "Tminstore"
        titleLabel.textAlignment = NSTextAlignment.center
        titleLabel.font          = UIFont(name: LobsterTwo_Bold, size: 36)
        titleLabel.textColor     = UIColor(hexString: "4CDFED")
        
        return titleLabel
    }
    
    //MARK: 第三方登录的Label
    /**
     第三方登录的Label
     
     - returns: PLoginTitleLabel
     */
    class func threePartLoginLabel()->PLoginLabel{
        
        let label: PLoginLabel = PLoginLabel()//用于显示“使用第三方登录”的label
        
        let str: String = NSLocalizedString("其他方式登录", comment: "")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "A1A1A1"), NSFontAttributeName: UIFont.systemFont(ofSize: 9)] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        label.attributedText = attStr
        
        return label
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
