//
//  PLoginPlayerView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/5.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PLoginPlayerView: UIView {

    var player: ZTPlayer?
    
    var titleLabel: UILabel?
    
    var collectionView: UICollectionView?
    
    var dismissButton: UIButton?
    
    var displayLink: CADisplayLink?
    
    var indexPathItem: Int = 0
    
    let titleArray: [String] = [NSLocalizedString("定制你的AR卡片", comment: ""),
                      NSLocalizedString("寄送你手写的祝福", comment: ""),
                      NSLocalizedString("扫描观看AR视频", comment: ""),
                      NSLocalizedString("遇见你的不二情书", comment: "")]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        self.backgroundColor = UIColor.black
//        
//        player = Player()
//        player?.delegate = self
//        
//        player?.view.frame = self.bounds
//        self.addSubview(player!.view)
//        player?.fillMode = AVLayerVideoGravityResizeAspectFill//全屏播放
//        
//        let url: URL = Bundle.main.url(forResource: "WeChatSight1", withExtension: "mp4")!
//        player?.setUrl(url)
//        player?.playbackLoops = true
        
//        //监控app是否进入后台
//        app.appDidEnterBackground = {[weak self] _ in
//            self?.player?.pause()
//        }
//        app.appDidBecomeActive = {[weak self] _ in
//            self?.player?.playFromCurrentTime()
//        }
        
//        setTitle()//设置标题
//        setDismissButton()//设置退出按钮
//        setCollectionView()//设置轮播的collectionView
//        setTimer()//设置计时器，滚动collectionView
    }
    
    //MARK: 设置标题
    fileprivate func setTitle(){
        titleLabel = UILabel()
        titleLabel?.textColor = UIColor(hexString: "68CFD6")
        titleLabel?.font = UIFont(name: LobsterTwo_Bold, size: 48)
        titleLabel?.text = "Timeory"
        
        self.insertSubview(titleLabel!, aboveSubview: player!.view)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.top.equalTo(self).offset(184/baseHeight)
        })
        
        titleLabel?.alpha = 0
    }
    
    //MARK: 设置轮播图的collectionView
    fileprivate func setCollectionView(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: screenWidth, height: 50)
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView?.isUserInteractionEnabled = false//不响应交互
        collectionView?.isPagingEnabled = true
        collectionView?.backgroundColor = UIColor.clear
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(PLoginCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.PLoginCollectionViewCellReuseIdentifier)
        
        self.insertSubview(collectionView!, aboveSubview: player!.view)
        
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-84/baseHeight)
            make.height.equalTo(50)
            make.width.equalTo(screenWidth)
        })
        
        collectionView?.alpha = 0
    }
    
    //MARK: 设置按钮
    fileprivate func setDismissButton(){
        
        dismissButton = UIButton()
        dismissButton?.backgroundColor = RGBAColor(R: 104, G: 207, B: 214, A: 0.28)
        dismissButton?.layer.cornerRadius = 6
        dismissButton?.layer.masksToBounds = true
        
        var font: UIFont
        
        if languageCode() == "CN" {
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightLight)
            } else {
                font = UIFont.systemFont(ofSize: 24)
            }
        }else{
            font = UIFont(name: LobsterTwo_Bold, size: 24)!
        }
        
        let str: String = NSLocalizedString("进入", comment: "")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "68CFD6"), NSFontAttributeName: font] as [String : Any]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)//设置带下划线的文字
        dismissButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        
        self.insertSubview(dismissButton!, aboveSubview: player!.view)
        dismissButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-16/baseHeight)
            make.size.equalTo(CGSize(width: 180/baseWidth, height: 50/baseWidth))
        })
        
        dismissButton?.addTarget(self, action: #selector(touchDismissButton(_:)), for: UIControlEvents.touchUpInside)
        
        dismissButton?.alpha = 0
    }
    
    //MARK: 设置定时器
    fileprivate func setTimer(){
        displayLink = CADisplayLink(target: self, selector: #selector(scrollCollectionView(_:)))
        displayLink?.add(to: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
        displayLink?.frameInterval = 150
    }
    
    //MARK: 定时器，将collectionView滚动到下一页
    func scrollCollectionView(_ displayLink: CADisplayLink) {
        if collectionView == nil { return }
        let indexPath: IndexPath = IndexPath(item: indexPathItem, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        indexPathItem += 1
    }
    
    //MARK: UI空间渐变动画
    fileprivate func uiComesOutAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            self.titleLabel?.alpha = 1
            self.collectionView?.alpha = 1
            self.dismissButton?.alpha = 1
            }, completion: nil)
    }
    
    //MARK: 点击进入按钮的事件
    func touchDismissButton(_ button: UIButton) {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            self.alpha = 0
            }) { (_) in
                self.player?.stop()
                self.player = nil
                self.displayLink?.invalidate()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PLoginPlayerView: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1000
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: PLoginCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PLoginCollectionViewCellReuseIdentifier, for: indexPath) as! PLoginCollectionViewCell
        
        let index: Int = (indexPath.item + 4) % 4
        
        cell.titleLable?.text = titleArray[index]
        
        return cell
    }
}

//MARK: 视频上轮播图的cell
class PLoginCollectionViewCell: UICollectionViewCell{
    
    var titleLable: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        
        titleLable = UILabel()
        titleLable?.textColor = UIColor(hexString: "68CFD6")
        
        var font: UIFont
        
        if languageCode() == "CN" {
            if #available(iOS 8.2, *) {
                font = UIFont.systemFont(ofSize: 24, weight: UIFontWeightLight)
            } else {
                font = UIFont.systemFont(ofSize: 24)
            }
        }else{
            font = UIFont(name: LobsterTwo_Bold, size: 24)!
        }
        
        titleLable?.font = font
        contentView.addSubview(titleLable!)
        titleLable?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(contentView)
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLable?.text = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
