//
//  PLoginView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/6.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class PLoginView: UIView {

    //MARK: logoImage
    /// logo
    class func logoImageView()->UIImageView{
        let logoImageView: UIImageView = UIImageView(image: UIImage(named: "loginLogo"))
        return logoImageView
    }

    //MARK: 绿色线条
    /// 绿色线条
    ///
    /// - Returns: PLoginView
    class func greenLine()->PLoginView{
        let greenLine: PLoginView = PLoginView()
        greenLine.backgroundColor = UIColor(hexString: "6EE3EF")
        return greenLine
    }
    
    //MARK: 灰色线条
    /// 灰色线条
    ///
    /// - Returns: PLoginView
    class func grayLine()->PLoginView{
        let grayLine: PLoginView = PLoginView()
        grayLine.backgroundColor = UIColor(hexString: "BBBBBB")
        return grayLine
    }
}
