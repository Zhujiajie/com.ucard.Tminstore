//
//  PLoginViewButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/22.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class PLoginViewButton: UIButton {

    /// 点击按钮的事件
    var touchButtonClosure: ((_ tag: Int)->())?
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        self.addTarget(self, action: #selector(touchButton(_:)), for: .touchUpInside)
    }
    
    //MARK: 设置登录按钮
    /**
     设置登录按钮
     
     - returns: PLoginViewButton
     */
    class func bigButton(placeHolder: String) ->PLoginViewButton{
        
        let bigButton: PLoginViewButton = PLoginViewButton()
        
        bigButton.backgroundColor = UIColor(hexString: "505050")
        
        var attDic: [String: Any]
        
        if #available(iOS 8.2, *) {
            attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 16, weight: UIFontWeightLight), NSForegroundColorAttributeName: UIColor.white] as [String: Any]
        } else {
            attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor.white] as [String: Any]
        }
        
        let attStr: NSAttributedString = NSAttributedString(string: placeHolder, attributes: attDic)
        bigButton.setAttributedTitle(attStr, for: UIControlState.normal)//设置按钮文字
        
        return bigButton
    }
    
    //MARK: 忘记密码的按钮
    /// 忘记密码的按钮
    ///
    /// - Returns: PLoginViewButton
    class func forgetPasswordButton()->PLoginViewButton{
        
        let forgetPasswordButton: PLoginViewButton = PLoginViewButton()
        forgetPasswordButton.backgroundColor = UIColor.clear
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "8F8F8F"), NSFontAttributeName: UIFont.systemFont(ofSize: 12)] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("忘记密码", comment: ""), attributes: attDic)
        
        forgetPasswordButton.setAttributedTitle(attStr, for: UIControlState.normal)
        return forgetPasswordButton
    }
    
    //MARK: 切换到注册的按钮
    /// 切换到注册的按钮
    ///
    /// - Returns: PLoginViewButton
    class func switchToSignUpButton()->PLoginViewButton{
        
        let switchToSignUpButton: PLoginViewButton = PLoginViewButton()
        switchToSignUpButton.backgroundColor = UIColor.clear
        
        let str: String = NSLocalizedString("立即注册", comment: "")
        
        let attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 12), NSForegroundColorAttributeName: UIColor(hexString:"575757")] as [String: Any]
        
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        switchToSignUpButton.setAttributedTitle(attStr, for: UIControlState.normal)
        return switchToSignUpButton
    }
    
    
    //MARK: 切换到邮箱注册的按钮
    /// 切换到邮箱注册的按钮
    ///
    /// - Returns: PLoginViewButton
    class func switchToEmailSignUpButton()->PLoginViewButton{
        
        let switchToEmailSignUpButton: PLoginViewButton = PLoginViewButton()
        
        switchToEmailSignUpButton.backgroundColor = UIColor.clear
        
//        var range1: NSRange
//        var range2: NSRange
//        
//        if languageCode() == "CN" {
//            range1 = NSMakeRange(0, 7)
//            range2 = NSMakeRange(7, 4)
//        }else{
//            range1 = NSMakeRange(0, 16)
//            range2 = NSMakeRange(16, 18)
//        }
//        
//        let att1: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "959595"), NSFontAttributeName: UIFont.systemFont(ofSize: 12)] as [String: Any]
//        let att2: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "47CBD5"), NSFontAttributeName: UIFont.systemFont(ofSize: 12)] as [String: Any]
//        
//        let attStr: NSMutableAttributedString = NSMutableAttributedString(string: )
//        
//        attStr.addAttributes(att1, range: range1)
//        attStr.addAttributes(att2, range: range2)
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "575757"), NSFontAttributeName: UIFont.systemFont(ofSize: 12)]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("邮箱注册", comment: ""), attributes: attDic)
        
        switchToEmailSignUpButton.setAttributedTitle(attStr, for: UIControlState.normal)
        
        return switchToEmailSignUpButton
    }
    
    //MARK: 切换到登录的按钮
    class func switchToLoginButton()->PLoginViewButton{
        
        let switchToLoginButton: PLoginViewButton = PLoginViewButton()
        switchToLoginButton.backgroundColor = UIColor.clear
        
//        var range1: NSRange
//        var range2: NSRange
//        
//        if languageCode() == "CN" {
//            range1 = NSMakeRange(0, 6)
//            range2 = NSMakeRange(6, 3)
//        }else{
//            range1 = NSMakeRange(0, 23)
//            range2 = NSMakeRange(23, 10)
//        }
//        
//        var font1: UIFont
//        var font2: UIFont
//        
//        if #available(iOS 8.2, *) {
//            font1 = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
//            font2 = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
//        } else {
//            font1 = UIFont.systemFont(ofSize: 14)
//            font2 = UIFont.systemFont(ofSize: 14)
//        }
//        
//        let att1: [String : Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4CDFED"), NSFontAttributeName: font1] as [String : Any]
//        let att2: [String : Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4CDFED"), NSFontAttributeName: font2] as [String : Any]
//        
//        let attStr: NSMutableAttributedString = NSMutableAttributedString(string: NSLocalizedString("已有账户了？请登录", comment: ""))
//        
//        attStr.addAttributes(att1, range: range1)
//        attStr.addAttributes(att2, range: range2)
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 12), NSForegroundColorAttributeName: UIColor(hexString: "8F8F8F")]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("已有账户", comment: ""), attributes: attDic)
        
        switchToLoginButton.setAttributedTitle(attStr, for: UIControlState.normal)
        
        return switchToLoginButton
    }
    
    //MARK: 切换到手机号码注册的按钮
    /// 切换到手机号码注册的按钮
    ///
    /// - Returns: PLoginViewButton
    class func switchToPhoneNumberSignUpButton()->PLoginViewButton{
        
        let switchToPhoneNumberSignUpButton: PLoginViewButton = PLoginViewButton()
        switchToPhoneNumberSignUpButton.backgroundColor = UIColor.clear
        
        let str: String = NSLocalizedString("手机号码注册", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 12), NSForegroundColorAttributeName: UIColor(hexString:"575757")] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        switchToPhoneNumberSignUpButton.setAttributedTitle(attStr, for: UIControlState.normal)
        return switchToPhoneNumberSignUpButton
    }
        
    //MARK: 微信登录按钮
    /**
     微信登录按钮
     
     - returns: PLoginViewButton
     */
    class func weChatButton()->PLoginViewButton{
        let weChatButton: PLoginViewButton = PLoginViewButton()
        weChatButton.setImage(UIImage(named: "WeChatLogo"), for: UIControlState.normal)
        weChatButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
        weChatButton.contentVerticalAlignment   = UIControlContentVerticalAlignment.center
        return weChatButton
    }
    
    //MARK: 微博登录按钮
    /**
     微博登录按钮
     
     - returns: PLoginViewButton
     */
    class func weiboButton()->PLoginViewButton{
        let weiboButton: PLoginViewButton = PLoginViewButton()
        weiboButton.setImage(UIImage(named: "WeiBoLogo"), for: UIControlState.normal)
        weiboButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
        weiboButton.contentVerticalAlignment   = UIControlContentVerticalAlignment.center
        return weiboButton
    }
        
    //MARK: 点击按钮的事件
    func touchButton(_ button: UIButton){
        touchButtonClosure?(button.tag)
    }
    
    //MARK: 启用按钮
    /// 启用按钮
    ///
    /// - Parameter isEnable: 是否启用
    func enableOrDisableButton(isButtonEnable: Bool) {
        if isButtonEnable == false{
            self.backgroundColor = UIColor(hexString: "878787")
        }else{
            self.backgroundColor = UIColor(hexString: "505050")
        }
        self.isEnabled = isButtonEnable
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
