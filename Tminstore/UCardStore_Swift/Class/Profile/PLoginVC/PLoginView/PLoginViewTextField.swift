//
//  PLoginViewTextField.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/22.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

let emailTextFieldTag: Int    = 1
let passwordTextFieldTag: Int = 2

let beginEditPasswordTextFieldNotification: String = "beginEditPasswordTextField"

class PLoginViewTextField: UIView {
    
    fileprivate var textField: UITextField?
    
    /// 完成输入
    var textFieldDidEndTexting: (()->())?
    
    /// 展示或者隐藏密码的按钮
    var showOrHidePasswordButton: BaseButton?
    
    /// textField的内容
    var textContent: String?{
        get{
            return textField?.text
        }
    }
    /// 邮箱textField停止编辑的闭包
    var emailClosure: ((_ email: String)->())?
    /// 密码textField停止编辑的闭包

    var passwordClosure:((_ password: String)->())?
    /// 点击获取验证码按钮的事件
    var touchGetCodeButtonClosure: (()->())?
    
    /// 获取验证码的按钮
    fileprivate var getCodeButton: UIButton?
    /// 获取验证码的计时器
    fileprivate var getCodeTimer: Timer?
    /// 倒计时60秒
    fileprivate var timeCount: Int = 60
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textField                = UITextField()
        textField?.font          = UIFont.systemFont(ofSize: 14)
        
        self.addSubview(textField!)
        textField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        
        textField?.delegate = self
        
//        self.layer.borderColor = UIColor(hexString: "EAEAEA").cgColor
//        self.layer.borderWidth = 1
//        self.layer.masksToBounds = true
        
        // 使输入的文字向右偏移
        let leftView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 14/baseWidth, height: 1))
        leftView.backgroundColor = UIColor.clear
        textField?.leftView = leftView
        textField?.leftViewMode = UITextFieldViewMode.always
        
        textField?.autocorrectionType = UITextAutocorrectionType.no//取消自动改成
    }
    
    //MARK: 登录的textField
    /// 登录的textField
    ///
    /// - Returns: PLoginViewTextField
    class func loginTextField()->PLoginViewTextField{
        let loginTextField: PLoginViewTextField = PLoginViewTextField()
        loginTextField.textField?.placeholder   = NSLocalizedString("请填写邮箱或手机号", comment: "")
        loginTextField.textField?.autocapitalizationType = UITextAutocapitalizationType.none
        loginTextField.textField?.keyboardType = UIKeyboardType.emailAddress
        loginTextField.backgroundColor = UIColor(hexString: "F4F4F4")
        return loginTextField
    }
    
    //MARK: 输入手机号码的textField
    /// 输入手机号码的textField
    ///
    /// - Returns: PLoginViewTextField
    class func phoneTextField()->PLoginViewTextField{
        let phoneTextField: PLoginViewTextField = PLoginViewTextField()
        phoneTextField.textField?.keyboardType = UIKeyboardType.numberPad
        phoneTextField.textField?.placeholder = NSLocalizedString("请填写手机号", comment: "")
        phoneTextField.backgroundColor = UIColor(hexString: "F4F4F4")
        return phoneTextField
    }
    
    //MARK: 填写验证码的按钮
    class func getVerificationTextField()->PLoginViewTextField{
        
        let getVerificationTextField: PLoginViewTextField = PLoginViewTextField()
        getVerificationTextField.textField?.snp.remakeConstraints({ (make: ConstraintMaker) in
            make.top.left.bottom.equalTo(getVerificationTextField)
            make.right.equalTo(getVerificationTextField).offset(-102/baseWidth)
        })
        
        getVerificationTextField.backgroundColor = UIColor(hexString: "F4F4F4")
        
        getVerificationTextField.textField?.placeholder = NSLocalizedString("请填写验证码", comment: "")
        getVerificationTextField.textField?.keyboardType = UIKeyboardType.numberPad//默认输入数字
        
        // 中间的灰色分割线
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "C1C1C1")
        getVerificationTextField.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(getVerificationTextField).offset(15)
            make.left.equalTo(getVerificationTextField.textField!.snp.right)
            make.bottom.equalTo(getVerificationTextField).offset(-8)
            make.width.equalTo(1)
        }
        
        // 获取验证码的按钮
        getVerificationTextField.getCodeButton = UIButton()
        getVerificationTextField.addSubview(getVerificationTextField.getCodeButton!)
        getVerificationTextField.getCodeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.bottom.equalTo(getVerificationTextField)
            make.left.equalTo(grayLine.snp.right)
        })
        
        let str: String = NSLocalizedString("获取验证码", comment: "")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "666666"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        getVerificationTextField.getCodeButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        
        getVerificationTextField.getCodeButton?.addTarget(getVerificationTextField, action: #selector(getVerificationTextField.touchGetCodeButton(button:)), for: UIControlEvents.touchUpInside)//添加点击事件
        
        return getVerificationTextField
    }
    
    
    //MARK: 设置邮箱textField
    /**
     设置邮箱textField
     */
    class func emailTextField()-> PLoginViewTextField{
        
        let emailTextField: PLoginViewTextField = PLoginViewTextField()
        emailTextField.textField?.placeholder   = NSLocalizedString("请填写邮箱地址", comment: "")
        emailTextField.textField?.keyboardType = UIKeyboardType.emailAddress//输入法不可更改
        emailTextField.textField?.autocapitalizationType = UITextAutocapitalizationType.none
        emailTextField.backgroundColor = UIColor(hexString: "F4F4F4")
        
        return emailTextField
    }
    
    //MARK: 填写密码textField
    /**
     填写密码textField
    */
    class func passwordField()->PLoginViewTextField{
        
        let passwordTextField: PLoginViewTextField = PLoginViewTextField()
        
        passwordTextField.textField?.placeholder = NSLocalizedString("请填写密码", comment: "")
        passwordTextField.textField?.isSecureTextEntry = true//隐藏输入的密码
        
        passwordTextField.textField?.tag = passwordTextFieldTag
        passwordTextField.textField?.returnKeyType = UIReturnKeyType.done
        passwordTextField.textField?.keyboardType = UIKeyboardType.default//输入法不可更改
        passwordTextField.backgroundColor = UIColor(hexString: "F4F4F4")
        
        passwordTextField.addShowPasswordButton()
        
        return passwordTextField
    }
    
    //MARK: 设置邮箱密码textField
    /**
     设置邮箱密码textField
     */
    class func emailPasswordField()->PLoginViewTextField{
        
        let emailPasswordField: PLoginViewTextField = PLoginViewTextField()
        
        emailPasswordField.textField?.placeholder     = NSLocalizedString("填写6-16位密码", comment: "")
        emailPasswordField.textField?.isSecureTextEntry = true//隐藏输入的密码
        
        emailPasswordField.textField?.tag = passwordTextFieldTag
        emailPasswordField.textField?.keyboardType = UIKeyboardType.default//输入法不可更改
        emailPasswordField.backgroundColor = UIColor(hexString: "F4F4F4")
        
        return emailPasswordField
    }
    
    //MARK: 点击获取验证码按钮的点击事件
    func touchGetCodeButton(button: UIButton){
        startTimer()
        touchGetCodeButtonClosure?()
    }
    
    //MARK: 开始计时
    func startTimer(){
        getCodeButton?.isEnabled = false
        getCodeTimer = Timer(timeInterval: 1, target: self, selector: #selector(getVerificationTimer(timer:)), userInfo: nil, repeats: true)
        RunLoop.current.add(getCodeTimer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    //MARK: 计时器，改变getCodeButton的文字
    func getVerificationTimer(timer: Timer){
        
        if let button: UIButton = getCodeButton {
            if timeCount >= 0 {
                let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "7DE1ED"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)] as [String: Any]
                let str: String = NSLocalizedString("已发送", comment: "") + "(\(timeCount))"
                timeCount -= 1
                button.setAttributedTitle(NSAttributedString(string: str, attributes: attDic), for: UIControlState.normal)
            }else{
                fireTimer()
            }
        }
    }
    
    //MARK: 销毁计时器
    func fireTimer(){
        if getCodeButton == nil || getCodeTimer == nil { return }
        getCodeTimer?.invalidate()
        timeCount = 60
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "7DE1ED"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)] as [String: Any]
        let str: String = NSLocalizedString("重新获取", comment: "")
        getCodeButton?.setAttributedTitle(NSAttributedString(string: str, attributes: attDic), for: UIControlState.normal)
        getCodeButton?.isEnabled = true
    }
    
    //MARK: 增加一个显示或隐藏密码的按钮
    fileprivate func addShowPasswordButton(){
        showOrHidePasswordButton = BaseButton.normalIconButton(iconName: "showPasswordButton")
        showOrHidePasswordButton?.frame = CGRect(x: 0, y: 0, width: 50/baseWidth, height: 20)
        self.textField?.rightView = showOrHidePasswordButton!
        self.textField?.rightViewMode = UITextFieldViewMode.always
        
        //MARK: 点击按钮的事件
        showOrHidePasswordButton?.touchButtonClosure = {[weak self] _ in
            if self?.textField?.isSecureTextEntry == true{
                self?.textField?.isSecureTextEntry = false
                self?.showOrHidePasswordButton?.setImage(UIImage(named: "hidePasswordButton"), for: UIControlState.normal)
            }else{
                self?.textField?.isSecureTextEntry = true
                self?.showOrHidePasswordButton?.setImage(UIImage(named: "showPasswordButton"), for: UIControlState.normal)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: ------UITextFieldDelegate-----------
extension PLoginViewTextField: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //通过闭包传递textField的数据
        if textField.tag == emailTextFieldTag {
            emailClosure?(textField.text!)
        }else{
            passwordClosure?(textField.text!)
        }
        textFieldDidEndTexting?()
    }
}
