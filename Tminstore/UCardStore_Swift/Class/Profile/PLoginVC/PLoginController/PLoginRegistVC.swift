//
//  PLoginPhoneRegistVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/8.
//  Copyright © 2016年 ucard. All rights reserved.
//
//MARK: ------------------ 手机验证码验证成功，输入密码的页面----------------------
//MARK: ------------------ 邮箱注册成功，提示用户去激活邮箱的页面 ---------------------
import UIKit
import Alamofire
import SnapKit

class PLoginRegistVC: BaseViewController {

    /// 是否是注册手机的页面
    var isPhoneRegist = true
    
    /// 手机注册成功的闭包
    var registSuccess: (()->())?
    
    fileprivate let dataModel: BaseDataModel = BaseDataModel()
    
    /// 输入密码的textField
    fileprivate var passwordTextField: UITextField?
    /// 隐藏或者显示密码的按钮
    fileprivate var showPasswordButton: UIButton?
    /// 提交的按钮
    fileprivate var confirmButton: UIButton?
    
    //邮箱注册成功之后展示信息的两个label
    fileprivate var label1: UILabel?
    fileprivate var label2: UILabel?
    
    fileprivate var passwordShouldShow: Bool = false{
        didSet{
            if let textField: UITextField = passwordTextField {
                textField.isSecureTextEntry = !passwordShouldShow
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setNavigationBar()
        
        if isPhoneRegist == true{
            setTextField()
        }else{
            setLabels()
        }
        setConfirmButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("手机注册")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("手机注册")
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 20), NSForegroundColorAttributeName: UIColor(hexString: "696969")] as [String: Any]
        if isPhoneRegist == true{
            setNavigationAttributeTitle(NSLocalizedString("填写密码", comment: ""), attributeDic: attDic)
            setNavigationBarLeftButtonWithImageName("backArrow")
        }else{
            setNavigationAttributeTitle(NSLocalizedString("激活邮箱账号", comment: ""), attributeDic: attDic)
        }
        
        //使导航栏透明
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.view.backgroundColor = UIColor.clear
        navigationController?.navigationBar.backgroundColor = UIColor.clear
    }
    
    //MARK: 设置label
    fileprivate func setLabels(){
        
        label1 = UILabel()
        label1?.textColor = UIColor(hexString: "696969")
        label1?.font = UIFont.systemFont(ofSize: 16)
        label1?.text = NSLocalizedString("您的账号已创建成功，请查收激活邮件完成注册！", comment: "")
        label1?.numberOfLines = 0
        
        view.addSubview(label1!)
        label1?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(39/baseHeight)
            make.centerX.equalTo(view)
            make.width.equalTo(278/baseWidth)
        })
        
        label2 = UILabel()
        label2?.textColor = UIColor(hexString: "9C9C9C")
        label2?.font = UIFont.systemFont(ofSize: 16)
        label2?.text = NSLocalizedString("未激活账户将不能领取优惠券。", comment: "")
        view.addSubview(label2!)
        label2?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(label1!.snp.bottom).offset(20/baseHeight)
            make.centerX.equalTo(view)
            make.width.equalTo(label1!)
        })
    }
    
    //MARK: 设置textField
    fileprivate func setTextField(){
        //文字向右偏移一点
        let leftView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 17/baseWidth, height: 1))
        leftView.backgroundColor = UIColor.clear
        
        //右侧的按钮
        showPasswordButton = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        showPasswordButton?.setImage(UIImage(named: "passwordHided"), for: UIControlState.normal)
        showPasswordButton?.addTarget(self, action: #selector(touchShowOrHidePasswordButton(button:)), for: UIControlEvents.touchUpInside)
        
        
        passwordTextField = UITextField()
        passwordTextField?.placeholder = NSLocalizedString("填写6-10位密码", comment: "")
        passwordTextField?.keyboardType = UIKeyboardType.default
        passwordTextField?.font = UIFont.systemFont(ofSize: 16)
        passwordTextField?.isSecureTextEntry = true
        
        passwordTextField?.leftView = leftView
        passwordTextField?.leftViewMode = UITextFieldViewMode.always
        passwordTextField?.rightView = showPasswordButton!
        passwordTextField?.rightViewMode = UITextFieldViewMode.always
        
        view.addSubview(passwordTextField!)
        passwordTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(134/baseHeight)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 292/baseWidth, height: 40/baseWidth))
        })
        
        // 底部灰色横线
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "E4E4E4")
        view.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(passwordTextField!.snp.bottom)
            make.centerX.equalTo(view)
            make.width.equalTo(passwordTextField!)
            make.height.equalTo(1)
        }
    }
    
    //MARK: 确认按钮
    fileprivate func setConfirmButton(){
        confirmButton = UIButton()
        confirmButton?.backgroundColor = UIColor(hexString: "60DBE8")
        
        let str: String = NSLocalizedString("确认", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor(hexString: "FFFFFF")] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        confirmButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        
        view.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            if isPhoneRegist == true {
                make.top.equalTo(passwordTextField!.snp.bottom).offset(30/baseHeight)
            }else{
                make.top.equalTo(label2!.snp.bottom).offset(186/baseHeight)
            }
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 318/baseWidth, height: 52/baseWidth))
        })
        
        confirmButton?.addTarget(self, action: #selector(touchConfirmButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 导航栏左键的点击事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
    
    //MARK: 点击textField右按钮的事件
    func touchShowOrHidePasswordButton(button: UIButton){
        passwordShouldShow = !passwordShouldShow
        if passwordShouldShow == true {
            showPasswordButton?.setImage(UIImage(named: "passwordShowed"), for: UIControlState.normal)
        }else{
            showPasswordButton?.setImage(UIImage(named: "passwordHided"), for: UIControlState.normal)
        }
    }
    
    //MARK: 点击确认按钮，发送手机号码注册的请求
    func touchConfirmButton(button: UIButton){
        // 邮箱注册的，确认
        if isPhoneRegist == false{
            registSuccess?()
            return
        }
        // 手机注册的，设置密码
        if passwordTextField?.text == "" || passwordTextField?.text == nil {
            return
        }
        //检查密码是否合法
        switch passwordTextField!.text!.characters.count {
        case 6...16://密码合法
            setPassword()
        default://密码不合法
            alert(alertTitle: nil, messageString: NSLocalizedString("密码要在6到16位之间", comment: ""), rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//当密码少于6位或多于20位时，则提醒用户
        }
    }
    
    //MARK: 手机注册设置密码的请求
    fileprivate func setPassword(){
        
        showSVProgress(title: nil)
        
        let parameters: [String: Any] = [
                "phone": getPhoneNumber()!,
                "password": passwordTextField!.text!,
                "phoneCode": getPhoneVerificationCode()!]
        
        dataModel.requestWithoutHeaders(urlStr: appAPIHelp.phoneRegistAPI, httpMethod: .post, parameters: parameters, successClosure: {[weak self] (result: [String: Any]) in
            print(result)
            self?.analysisLoginResult(result: result, withAccount: getPhoneNumber()!, andIsSignUp: true)
            }, failureClosure: {[weak self] (error: Error?) in
            self?.handelNetWorkError(error, withResult: nil)
        }, completionClosure: {[weak self] _ in
            self?.dismissSVProgress()
        })
    }
    
    //MARK: 解析登录的结果
    /// 解析登录的结果
    ///
    /// - Parameters:
    ///   - result: 服务器返回数据
    ///   - account: 用户账号
    fileprivate func analysisLoginResult(result: [String: Any], withAccount account: String, andIsSignUp isSignUp: Bool = false){
        //登录成功
        if let code: Int = result["code"] as? Int, code == 1000{
            
            let data: [String: Any] = result["data"] as! [String: Any]
            let user: [String: Any] = data["user"] as! [String: Any]
            let token: String = user["token"] as! String
            let userID: String = user["memberCode"] as! String
            let userName: String = user["nickName"] as! String
            
            setLoginStatus(loginStatus: true)//保存用户登录状态
            saveUserID(userId: userID)
            saveUserName(name: userName)
            saveUserToken(token: token)//注册成功，保存token
            registSuccess?()//手机注册成功
        }else{
            handelNetWorkError(nil, withResult: result)//注册失败
        }
    }
}
