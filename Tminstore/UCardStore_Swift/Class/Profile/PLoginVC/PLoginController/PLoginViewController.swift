//
//  PLoginViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/14.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import IQKeyboardManagerSwift

class PLoginViewController: BaseViewController {

    let viewModel: PLoginViewModel = PLoginViewModel()
    
    /// 从第三方注册成功的闭包
    var threePartRegist: ((_ userInfo: UserInfoModel)->())?
    
    /// 是否使用dismiss方法退出
    var shouldUseDismiss: Bool = false
    
    /// 登录成功的闭包
    var loginSuccessClosure: (()->())?
    
    /// logo
    fileprivate var logoImageView: BaseImageView?
    /// timeory label
    fileprivate var titleLabel: BaseLabel?
    
    /// 登录框，输入邮箱或者手机号码
    fileprivate var loginTextField: PLoginViewTextField?
    /// 登录的密码框
    fileprivate var passwordTextField: PLoginViewTextField?
    /// 登录按钮
    fileprivate var loginButton: PLoginViewButton?
    /// 忘记密码的按钮
    fileprivate var forgetPasswordButton: PLoginViewButton?
    /// 切换到注册的按钮
    fileprivate var switchToSignUpButton: PLoginViewButton?
    
    /// 输入手机号码的textField
    fileprivate var phoneNumberTextField: PLoginViewTextField?
    /// 输入验证码的textField
    fileprivate var verificationTextField: PLoginViewTextField?
    /// 点击手机注册的按钮
    fileprivate var phoneSignUpButton: PLoginViewButton?
    /// 切换到邮箱注册的按钮
    fileprivate var switchToEmailSignUpButton: PLoginViewButton?
    /// 切换到登录的按钮
    fileprivate var switchToLoginButton: PLoginViewButton?
    
    /// 输入邮箱
    fileprivate var emailTextField: PLoginViewTextField?
    /// 输入邮箱密码的textField
    fileprivate var emailPasswordTextField: PLoginViewTextField?
    /// 邮箱注册的按钮
    fileprivate var emailSignUpButton: PLoginViewButton?
    /// 切换到手机号码注册的按钮
    fileprivate var switchToPhoneNumberSignUpButton: PLoginViewButton?
    
    /// 第三方登录的Label
    fileprivate var threePartLoginLabel: PLoginLabel?
    /// 微信登录按钮
    fileprivate var weChatButton: PLoginViewButton?
    /// 微博登录按钮
    fileprivate var weiboButton: PLoginViewButton?
    /// facebook登录按钮
    fileprivate var facebookButton: PLoginViewButton?

    /// 承载播放器的view
    fileprivate var playerView: PLoginPlayerView?
    
    /// 稍后登录的按钮
    fileprivate var laterButton: BaseButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPlayer()//设置播放器
        setView()
        setLogoImageViewAndTitleLabel()//设置标题
        setLoginTextFieldAndButton()//设置登录textField和按钮
        phoneNumberSignUpTextFieldsAndButton()//设置手机注册的textField和按钮
        setEmailSignUpTextFieldAndButtons()//设置邮箱注册的textField和按钮
        setThreePartLoginButtons()//设置第三方登录按钮
        
        closureResponse()//处理各种闭包
        viewModelClosure()//viewModel的闭包回调
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("登录")
        IQKeyboardManager.sharedManager().enable = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("登录")
    }
    
    //MARK: 设置view和其点击手势
    fileprivate func setView(){
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapViewToEndEditing(tap:)))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
        
        //稍后登录的按钮
        laterButton = BaseButton.normalTitleButton(title: NSLocalizedString("稍后登录", comment: ""), andTextFont: UIFont.systemFont(ofSize: 12), andTextColorHexString: "474747")
        view.addSubview(laterButton!)
        laterButton?.snp.makeConstraints({ (make) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-30/baseWidth)
            make.size.equalTo(CGSize(width: 62/baseWidth, height: 20/baseWidth))
        })
    }
    
    //MARK: 设置标题
    fileprivate func setLogoImageViewAndTitleLabel(){
        
        logoImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "loginLogo")
        view.insertSubview(logoImageView!, belowSubview: playerView!)
        logoImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(77/baseHeight)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 61/baseWidth, height: 59/baseWidth))
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.init(name: "Arial Rounded MT Bold", size: 14)!, andTextColorHex: "454545", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: "TMINSTORE")
        view.insertSubview(titleLabel!, belowSubview: playerView!)
        titleLabel?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(logoImageView!.snp.bottom).offset(18/baseHeight)
            make.left.equalTo(view)
            make.right.equalTo(view)
        }
    }
    
    //MARK: 登录的textField和按钮
    fileprivate func setLoginTextFieldAndButton(){
        
        loginTextField = PLoginViewTextField.loginTextField()
        view.insertSubview(loginTextField!, belowSubview: playerView!)
        loginTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(63/baseHeight)
            make.centerX.equalTo(view).offset(0)
            make.size.equalTo(CGSize(width: 284/baseWidth, height: 48))
        })
        
        passwordTextField = PLoginViewTextField.passwordField()
        view.insertSubview(passwordTextField!, belowSubview: playerView!)
        passwordTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(loginTextField!.snp.bottom).offset(7/baseHeight)
            make.centerX.equalTo(view).offset(0)
            make.size.equalTo(loginTextField!)
        })
        
        loginButton = PLoginViewButton.bigButton(placeHolder: NSLocalizedString("登录", comment: ""))
        view.insertSubview(loginButton!, belowSubview: playerView!)
        loginButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(passwordTextField!.snp.bottom).offset(31/baseHeight)
            make.centerX.equalTo(view).offset(0)
            make.size.equalTo(loginTextField!)
        })
        loginButton?.enableOrDisableButton(isButtonEnable: false)//先禁用按钮
        
        forgetPasswordButton = PLoginViewButton.forgetPasswordButton()
        view.insertSubview(forgetPasswordButton!, belowSubview: playerView!)
        forgetPasswordButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(loginButton!.snp.bottom).offset(10/baseHeight)
            make.rightMargin.equalTo(loginButton!)
            make.height.equalTo(16)
        })
        
        switchToSignUpButton = PLoginViewButton.switchToSignUpButton()
        view.insertSubview(switchToSignUpButton!, belowSubview: playerView!)
        switchToSignUpButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(forgetPasswordButton!)
            make.leftMargin.equalTo(loginButton!)
            make.height.equalTo(20)
        })
    }
    
    //MARK: 手机注册的textField和按钮
    fileprivate func phoneNumberSignUpTextFieldsAndButton(){
        
        phoneNumberTextField = PLoginViewTextField.phoneTextField()
        view.insertSubview(phoneNumberTextField!, belowSubview: playerView!)
        phoneNumberTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(loginTextField!)
            make.centerX.equalTo(view).offset(screenWidth)
            make.size.equalTo(loginTextField!)
        })
        
        verificationTextField = PLoginViewTextField.getVerificationTextField()
        view.insertSubview(verificationTextField!, belowSubview: playerView!)
        verificationTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(passwordTextField!)
            make.centerX.equalTo(view).offset(screenWidth)
            make.size.equalTo(loginTextField!)
        })
        
        phoneSignUpButton = PLoginViewButton.bigButton(placeHolder: NSLocalizedString("注册", comment: ""))
        view.insertSubview(phoneSignUpButton!, belowSubview: playerView!)
        phoneSignUpButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(loginButton!)
            make.centerX.equalTo(view).offset(screenWidth)
            make.size.equalTo(loginButton!)
        })
        phoneSignUpButton?.enableOrDisableButton(isButtonEnable: false)//暂时禁用按钮
        
        switchToEmailSignUpButton = PLoginViewButton.switchToEmailSignUpButton()
        view.insertSubview(switchToEmailSignUpButton!, belowSubview: playerView!)
        switchToEmailSignUpButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(phoneSignUpButton!.snp.bottom).offset(10/baseHeight)
            make.leftMargin.equalTo(phoneSignUpButton!)
            make.height.equalTo(16)
        })
        
        switchToLoginButton = PLoginViewButton.switchToLoginButton()
        view.insertSubview(switchToLoginButton!, belowSubview: playerView!)
        switchToLoginButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(switchToEmailSignUpButton!)
            make.rightMargin.equalTo(phoneSignUpButton!)
            make.height.equalTo(16)
        })
    }
    
    
    //MARK: 设置邮箱输入textField和输入密码的textField
    fileprivate func setEmailSignUpTextFieldAndButtons(){

        emailTextField = PLoginViewTextField.emailTextField()
        view.insertSubview(emailTextField!, belowSubview: playerView!)
        emailTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(loginTextField!)
            make.centerX.equalTo(view).offset(screenWidth)
            make.size.equalTo(loginTextField!)
        })
        
        emailPasswordTextField = PLoginViewTextField.emailPasswordField()
        view.insertSubview(emailPasswordTextField!, belowSubview: playerView!)
        emailPasswordTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(passwordTextField!)
            make.centerX.equalTo(view).offset(screenWidth)
            make.size.equalTo(loginTextField!)
        })
        
        emailSignUpButton = PLoginViewButton.bigButton(placeHolder: NSLocalizedString("注册", comment: ""))
        view.insertSubview(emailSignUpButton!, belowSubview: playerView!)
        emailSignUpButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(loginButton!)
            make.centerX.equalTo(view)
            make.size.equalTo(loginButton!)
        })
        emailSignUpButton?.isHidden = true
        emailSignUpButton?.enableOrDisableButton(isButtonEnable: false)
        
        
        switchToPhoneNumberSignUpButton = PLoginViewButton.switchToPhoneNumberSignUpButton()
        view.insertSubview(switchToPhoneNumberSignUpButton!, belowSubview: playerView!)
        switchToPhoneNumberSignUpButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(switchToEmailSignUpButton!)
            make.leftMargin.equalTo(emailSignUpButton!)
            make.height.equalTo(16)
        })
        switchToPhoneNumberSignUpButton?.isHidden = true
    }
    
    //MARK: 设置第三方登录按钮
    func setThreePartLoginButtons() {
        
        threePartLoginLabel = PLoginLabel.threePartLoginLabel()
        view.insertSubview(threePartLoginLabel!, belowSubview: playerView!)
        threePartLoginLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(-116/baseHeight)
            make.centerX.equalTo(view)
        })
        
        //左边灰色线
        let leftGreenLine = PLoginView.grayLine()
        view.insertSubview(leftGreenLine, belowSubview: playerView!)
        leftGreenLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.equalTo(view).offset(80/baseWidth)
            make.right.equalTo(threePartLoginLabel!.snp.left).offset(-5/baseWidth)
            make.height.equalTo(0.5)
            make.centerY.equalTo(threePartLoginLabel!)
        }
        //右边灰色线
        let rightGreenLine = PLoginView.grayLine()
        view.insertSubview(rightGreenLine, belowSubview: playerView!)
        rightGreenLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.equalTo(threePartLoginLabel!.snp.right).offset(5/baseWidth)
            make.right.equalTo(view).offset(-80/baseWidth)
            make.height.equalTo(leftGreenLine)
            make.centerY.equalTo(leftGreenLine)
        }
        
        weChatButton   = PLoginViewButton.weChatButton()
        weiboButton    = PLoginViewButton.weiboButton()
        
        view.insertSubview(weChatButton!, belowSubview: playerView!)
        view.insertSubview(weiboButton!, belowSubview: playerView!)
        
        //已安装微信
        if UMSocialManager.default().isInstall(UMSocialPlatformType.wechatSession) {
            weChatButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.bottom.equalTo(view).offset(-65/baseHeight)
                make.left.equalTo(view).offset(141/baseWidth)
                make.size.equalTo(CGSize(width: 33/baseWidth, height: 33/baseWidth))
            })
            weiboButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(view).offset(-141/baseWidth)
                make.centerY.equalTo(weChatButton!)
                make.size.equalTo(CGSize(width: 33/baseWidth, height: 33/baseWidth))
            })
        }
            //未安装微信
        else{
            weiboButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.bottom.equalTo(view).offset(-65/baseHeight)
                make.centerX.equalTo(view)
                make.size.equalTo(CGSize(width: 33/baseWidth, height: 33/baseWidth))
            })
        }
    }
    
    //MARK: 检查登录按钮是否能使用
    fileprivate func checkLoginButtonAvailable(){
        if loginTextField?.textContent == nil || loginTextField?.textContent == "" || passwordTextField?.textContent == nil || passwordTextField?.textContent == ""{
            loginButton?.enableOrDisableButton(isButtonEnable: false)
        }else{
            loginButton?.enableOrDisableButton(isButtonEnable: true)
        }
    }
    
    //MARK: 检查手机注册按钮是否可用
    fileprivate func checkPhoneNumberSignupButtonAvailable(){
        if phoneNumberTextField?.textContent == nil || phoneNumberTextField?.textContent == "" || verificationTextField?.textContent == nil || verificationTextField?.textContent == ""{
            phoneSignUpButton?.enableOrDisableButton(isButtonEnable: false)
        }else{
            phoneSignUpButton?.enableOrDisableButton(isButtonEnable: true)
        }
    }
    
    //MARK: 检查邮箱注册的按钮是否可用
    fileprivate func checkEmailSignupButtonAvailable(){
        if emailTextField?.textContent == nil || emailTextField?.textContent == "" || emailPasswordTextField?.textContent == nil || emailPasswordTextField?.textContent == ""{
            emailSignUpButton?.enableOrDisableButton(isButtonEnable: false)
        }else{
            emailSignUpButton?.enableOrDisableButton(isButtonEnable: true)
        }
    }
}

extension PLoginViewController{
    
    //MARK: 点击空白区域，收回键盘
    /// 点击空白区域，收回键盘
    ///
    /// - Parameter tap: UITapGestureRecognizer
    func tapViewToEndEditing(tap: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    //MARK: 登录成功之后做的事，并保存用户token
    /**
     登录成功之后做的事，并保存用户token
     
     - parameter token: String
     */
    fileprivate func loginSuccessFunction(){
        if shouldUseDismiss {
            dismissSelf(animated: true, completion: {[weak self] _ in
                self?.loginSuccessClosure?()
            })
        }else{
            viewModel.postPushID()//上传pushID
            _ = app.window?.subviews.map({ (view: UIView) in
                view.removeFromSuperview()
            })
            app.setMainVC()
        }
    }
}

extension PLoginViewController{
    
    //MARK: 处理各种闭包回调
    /**
     处理各种闭包回调
     */
    fileprivate func closureResponse(){
        
        //MARK: 输入邮箱或手机号码的输入框
        loginTextField?.textFieldDidEndTexting = {[weak self] _ in
            self?.checkLoginButtonAvailable()
        }
        
        //MARK: 登录时输入密码的是输入框
        passwordTextField?.textFieldDidEndTexting = {[weak self] _ in
            self?.checkLoginButtonAvailable()
        }
        
        //MARK: 注册时输入手机号码的输入框
        phoneNumberTextField?.textFieldDidEndTexting = {[weak self] _ in
            self?.checkPhoneNumberSignupButtonAvailable()
        }
        
        //MARK: 注册时, 输入验证码的输入框
        verificationTextField?.textFieldDidEndTexting = {[weak self] _ in
            self?.checkPhoneNumberSignupButtonAvailable()
        }
        
        //MARK: 注册时, 输入邮箱的输入框
        emailTextField?.textFieldDidEndTexting = {[weak self] in
            self?.checkEmailSignupButtonAvailable()
        }
        
        //MARK: 注册时, 输入邮箱密码的输入框
        emailPasswordTextField?.textFieldDidEndTexting = {[weak self] _ in
            self?.checkEmailSignupButtonAvailable()
        }
        
        //MARK: 点击登录按钮的事件
        loginButton?.touchButtonClosure = {[weak self] _ in
            self?.viewModel.login(account: self?.loginTextField?.textContent, andPassword: self?.passwordTextField?.textContent)
        }
        
        //MARK: 点击忘记密码按钮的事件
        forgetPasswordButton?.touchButtonClosure = {[weak self] _ in
            let vc: PLoginResetPasswordVC = PLoginResetPasswordVC()
            let nav: UINavigationController = UINavigationController(rootViewController: vc)
            self?.present(nav, animated: true, completion: nil)
            vc.resetPasswordSuccess = {[weak self](account: String, password: String) in
                self?.viewModel.login(account: account, andPassword: password)
            }//重置密码成功，返回直接登录
        }
        
        //MARK: 点击获取验证码按钮的事件
        verificationTextField?.touchGetCodeButtonClosure = {[weak self] _ in
            if self?.viewModel.requestVerificationCode(phoneNumber: self?.phoneNumberTextField?.textContent) == true{
                self?.verificationTextField?.startTimer()//手机号码正确。开始计时
            }
        }
        
        //MARK: 点击手机注册按钮，验证验证码
        phoneSignUpButton?.touchButtonClosure = {[weak self] (_) in
            self?.viewModel.verifyVerificationCode(phoneNumber: self?.phoneNumberTextField?.textContent, withVerificationCode: self?.verificationTextField?.textContent)
        }
        
        //MARK: 点击邮箱注册按钮
        emailSignUpButton?.touchButtonClosure = {[weak self] (_) in
            self?.viewModel.emailSignUp(email: self?.emailTextField?.textContent, andPassword: self?.emailPasswordTextField?.textContent)
        }
        
        //MARK: 点击微信登录的按钮
        weChatButton?.touchButtonClosure = {[weak self] _ in
            self?.viewModel.weChatLogin()
        }
        
        //MARK: 点击微博登录的按钮
        weiboButton?.touchButtonClosure = {[weak self] _ in
            self?.viewModel.weiboLogin()
        }
        
        //MARK:点击切换到注册的按钮
        switchToSignUpButton?.touchButtonClosure = {[weak self] (_) in
            self?.switchToSignUpAnimation()
        }
        
        //MARK: 点击切换到登录的按钮
        switchToLoginButton?.touchButtonClosure = {[weak self] (_) in
            self?.switchToLoginAnimation()
        }
        
        //MARK: 点击切换到邮箱注册的按钮
        switchToEmailSignUpButton?.touchButtonClosure = {[weak self] (_) in
            self?.switchToEmailSignUpAnimation()
        }
        
        //MARK: 点击切换到手机注册按钮
        switchToPhoneNumberSignUpButton?.touchButtonClosure = {[weak self] (_) in
            self?.switchToPhoneNumberSignUpAnimation()
        }
    }
    
    //MARK: viewModel的闭包回调
    /**
     viewModel的闭包回调
     */
    fileprivate func viewModelClosure(){
        
        //MARK: 点击稍后登录按钮的事件
        laterButton?.touchButtonClosure = {[weak self] _ in
            if self?.shouldUseDismiss == true {
                self?.dismissSelf()
            }else{
                self?.loginSuccessFunction()
            }
        }
        
        //MARK: 登录时邮箱或手机号不合法的闭包
        viewModel.accountErrorClosure = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("请输入正确的邮箱地址和手机号码", comment: ""), messageString: nil, rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//当邮箱地址和手机号码不合法时，则提醒用户
        }
        
        //MARK: 密码不合法的闭包
        viewModel.passwordErrorClosure = {[weak self] _ in
            self?.alert(alertTitle: nil, messageString: NSLocalizedString("密码要在6到20位之间", comment: ""), rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//当密码少于6位或多于20位时，则提醒用户
        }
        
        //MARK: 登录成功的闭包
        viewModel.loginSuccessClosure = {[weak self] _ in
            self?.loginSuccessFunction()
        }
        
        //MARK: 手机号码不合法的闭包
        viewModel.phoneNumberErrorClosure = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("请输入正确的手机号码", comment: ""), messageString: nil, rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//当手机号码不合法时，则提醒用户
        }
        
        //MARK: 验证法发送失败的闭包
        viewModel.requestVerificationCodeFailed = {[weak self] _ in
            self?.verificationTextField?.fireTimer()// 重置计时器
        }
        
        //MARK: 验证码验证成功的闭包
        viewModel.verifySuccess = {[weak self] _ in
            let registVC: PLoginRegistVC = PLoginRegistVC()
            registVC.isPhoneRegist = true
            let nav: UINavigationController = UINavigationController(rootViewController: registVC)
            nav.modalTransitionStyle = .crossDissolve
            self?.present(nav, animated: true, completion: nil)
            registVC.registSuccess = {[weak self] _ in
                self?.loginSuccessFunction()
            }//手机注册成功，进入主页
        }
        
        //MARK: 验证码验证失败的闭包
        viewModel.verifyFalied = {[weak self] (message: String?) in
            self?.alert(alertTitle: message, messageString: nil, rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)
        }
                
        //MARK: 邮箱不合法
        viewModel.emailErrorClosure = {[weak self] _ in
            self?.alert(alertTitle: nil, messageString: NSLocalizedString("请输入正确的邮箱地址", comment: ""), rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)
        }
        
        //MARK: 邮箱注册成功的闭包
        viewModel.emailRegistSuccess = {[weak self] _ in
            let registVC: PLoginRegistVC = PLoginRegistVC()
            registVC.isPhoneRegist = false
            let nav: UINavigationController = UINavigationController(rootViewController: registVC)
            nav.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self?.present(nav, animated: true, completion: nil)
            registVC.registSuccess = {[weak self] _ in
                self?.loginSuccessFunction()
            }
        }
        
        //MARK: 调用接口错误的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)//处理Java服务器返回错误
        }
        
        //MARK: .net用户需要更新密码
        viewModel.dotNetUpdatePassword = {[weak self] _ in
            let resetVC: PLoginResetPasswordVC = PLoginResetPasswordVC()
            let nav: UINavigationController = UINavigationController(rootViewController: resetVC)
            nav.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self?.present(nav, animated: true, completion: nil)
            resetVC.resetPasswordSuccess = {[weak self](account: String, password: String) in
                self?.viewModel.login(account: account, andPassword: password)
            }//重置密码成功，返回直接登录
        }
    }
}

extension PLoginViewController{
    //MARK: 设置播放器
    fileprivate func setPlayer(){
        playerView = PLoginPlayerView(frame: view.bounds)
        view.addSubview(playerView!)
        playerView?.alpha = 0
    }
    
    // MARK: 切换到注册的动画
    fileprivate func switchToSignUpAnimation(){
        
        switchAnimation(animationView: loginTextField!, withOffset: -screenWidth, withDelay: 0.0)
        switchAnimation(animationView: passwordTextField!, withOffset: -screenWidth, withDelay: 0.1)
        switchAnimation(animationView: loginButton!, withOffset: -screenWidth, withDelay: 0.2)
//        switchAnimation(animationView: forgetPasswordButton!, withOffset: -screenWidth, withDelay: 0.3)
//        switchAnimation(animationView: switchToSignUpButton!, withOffset: -screenWidth, withDelay: 0.4)
        switchAnimation(animationView: phoneNumberTextField!, withOffset: 0, withDelay: 0.5)
        switchAnimation(animationView: verificationTextField!, withOffset: 0, withDelay: 0.6)
        switchAnimation(animationView: phoneSignUpButton!, withOffset: 0, withDelay: 0.7)
//        switchAnimation(animationView: switchToEmailSignUpButton!, withOffset: 0, withDelay: 0.8)
//        switchAnimation(animationView: switchToLoginButton!, withOffset: 0, withDelay: 0.9)
    }
    
    //MARK: 切换到登录的动画
    fileprivate func switchToLoginAnimation(){
        phoneSignUpButton?.isHidden = false
        emailSignUpButton?.isHidden = true
        switchToPhoneNumberSignUpButton?.isHidden = true
        switchToEmailSignUpButton?.isHidden = false
        
        switchAnimation(animationView: phoneNumberTextField!, withOffset: screenWidth, withDelay: 0.0)
        switchAnimation(animationView: verificationTextField!, withOffset: screenWidth, withDelay: 0.1)
        switchAnimation(animationView: phoneSignUpButton!, withOffset: screenWidth, withDelay: 0.2)
//        switchAnimation(animationView: switchToEmailSignUpButton!, withOffset: screenWidth, withDelay: 0.3)
//        switchAnimation(animationView: switchToLoginButton!, withOffset: screenWidth, withDelay: 0.4)
        switchAnimation(animationView: emailTextField!, withOffset: screenWidth, withDelay: 0.0)
        switchAnimation(animationView: emailPasswordTextField!, withOffset: screenWidth, withDelay: 0.1)
//        switchAnimation(animationView: switchToPhoneNumberSignUpButton!, withOffset: screenWidth, withDelay: 0.3)
        switchAnimation(animationView: loginTextField!, withOffset: 0, withDelay: 0.5)
        switchAnimation(animationView: passwordTextField!, withOffset: 0, withDelay: 0.6)
        switchAnimation(animationView: loginButton!, withOffset: 0, withDelay: 0.7)
//        switchAnimation(animationView: forgetPasswordButton!, withOffset: 0, withDelay: 0.8)
//        switchAnimation(animationView: switchToSignUpButton!, withOffset: 0, withDelay: 0.9)
    }
    
    //MARK: 切换到邮箱注册的动画
    fileprivate func switchToEmailSignUpAnimation(){
        emailSignUpButton?.isHidden = false
        phoneSignUpButton?.isHidden = true
        switchToPhoneNumberSignUpButton?.isHidden = false
        switchToEmailSignUpButton?.isHidden = true
        
        switchAnimation(animationView: phoneNumberTextField!, withOffset: -screenWidth, withDelay: 0.0)
        switchAnimation(animationView: verificationTextField!, withOffset: -screenWidth, withDelay: 0.1)
//        switchAnimation(animationView: switchToEmailSignUpButton!, withOffset: -screenWidth, withDelay: 0.2)
        switchAnimation(animationView: emailTextField!, withOffset: 0, withDelay: 0.3)
        switchAnimation(animationView: emailPasswordTextField!, withOffset: 0, withDelay: 0.4)
//        switchAnimation(animationView: switchToPhoneNumberSignUpButton!, withOffset: 0, withDelay: 0.5)
    }
    
    //MARK: 切换到手机号码注册的动画
    fileprivate func switchToPhoneNumberSignUpAnimation(){
        phoneSignUpButton?.isHidden = false
        emailSignUpButton?.isHidden = true
        switchToPhoneNumberSignUpButton?.isHidden = true
        switchToEmailSignUpButton?.isHidden = false
        switchAnimation(animationView: emailTextField!, withOffset: screenWidth, withDelay: 0.0)
        switchAnimation(animationView: emailPasswordTextField!, withOffset: screenWidth, withDelay: 0.1)
//        switchAnimation(animationView: switchToPhoneNumberSignUpButton!, withOffset: screenWidth, withDelay: 0.2)
        switchAnimation(animationView: phoneNumberTextField!, withOffset: 0, withDelay: 0.3)
        switchAnimation(animationView: verificationTextField!, withOffset: 0, withDelay: 0.4)
//        switchAnimation(animationView: switchToEmailSignUpButton!, withOffset: 0, withDelay: 0.5)
    }
    
    // MARK: 切换动画
    fileprivate func switchAnimation(
        animationView: UIView, withOffset offset: CGFloat, withDelay delay: Double){
        animationView.snp.updateConstraints { (make: ConstraintMaker) in
            make.centerX.equalTo(view).offset(offset)
        }
        view.setNeedsUpdateConstraints()//约束需要更新
        view.updateConstraintsIfNeeded()//检测需要更新的约束
        UIView.animate(withDuration: 0.3, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.view.layoutIfNeeded()//强制更新约束
            }, completion: nil)
    }
}
