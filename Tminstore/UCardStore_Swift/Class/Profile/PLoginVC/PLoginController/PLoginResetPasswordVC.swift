//
//  PLoginResetPasswordVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/16.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: ------------修改密码的页面----------------

import UIKit
import WebKit
import SnapKit

class PLoginResetPasswordVC: BaseViewController {
    
    /// 是否是忘记密码
    var isForgetPassword: Bool = false
    
    /// 通过忘记密码，重置密码成功
    var resetPasswordSuccess: ((_ account: String, _ password: String)->())?
    
    /// 进度条
    fileprivate var progressView: UIProgressView?
    
    fileprivate var webView: WKWebView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("忘记密码")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("忘记密码")
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationBarLeftButtonWithImageName("backArrow")
    }
    
    //MARK: 设置webView
    fileprivate func setWebView(){
        
        webView = WKWebView()
        webView?.evaluateJavaScript("navigator.userAgent", completionHandler: { [weak self] (result: Any?, _) in
            
            if result != nil{//设置UserAgent
                let userAgent: String = result! as! String
                let newUserAgent: String = userAgent + "/timeory"
                let userAgentDefaults: [String: Any] = ["UserAgent": newUserAgent] as [String: Any]
                UserDefaults.standard.register(defaults: userAgentDefaults)
                UserDefaults.standard.synchronize()//立即保存
            }
            
            //回到主线程重新设置webView，并加载网页
            DispatchQueue.main.async {
                
                let config: WKWebViewConfiguration = WKWebViewConfiguration()
                config.userContentController = WKUserContentController()
                config.userContentController.add(self!, name: "ios_native")
                
                self?.webView = WKWebView(frame: CGRect.zero, configuration: config)
                self?.webView?.uiDelegate = self
                self?.webView?.navigationDelegate = self
                self?.view.addSubview((self?.webView)!)
                self?.webView?.snp.makeConstraints { (make: ConstraintMaker) in
                    make.edges.equalTo((self?.view)!)
                }
                let url: URL = URL(string: "http://www.ucardstore.com/app_forget_password/forget.html")!
                let request: URLRequest = URLRequest(url: url)
                _ = self?.webView?.load(request)
                
                self?.setProgressView()//设置进度条
            }
        })
    }
    
    //MARK: 设置进度条
    fileprivate func setProgressView(){
        webView?.addObserver(self, forKeyPath: "estimatedProgress", options: NSKeyValueObservingOptions.new, context: nil)//检测网页加载进度
        progressView = UIProgressView()
        progressView?.trackTintColor = UIColor.clear
        view.addSubview(progressView!)
        progressView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
        })
    }
    
    //MARK: 导航栏左键的点击事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        if webView?.canGoBack == true{
            _ = webView?.goBack()
        }else{
            if isForgetPassword == true {
                _ = navigationController?.popViewController(animated: true)
            }else{
                dismissSelf()
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress"{//检测网页加载进度
            if (webView != nil) {
                progressView?.isHidden = webView?.estimatedProgress == 1.0
                progressView?.setProgress(Float(webView!.estimatedProgress), animated: false)
            }
        }
    }
    
    deinit {
        webView?.configuration.userContentController.removeAllUserScripts()
        webView?.removeObserver(self, forKeyPath: "estimatedProgress")
    }
}

extension PLoginResetPasswordVC: WKScriptMessageHandler, WKUIDelegate, WKNavigationDelegate{
    
    //MARK: 设置webView的title
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        
        if isForgetPassword == true{
            setNavigationAttributeTitle(NSLocalizedString("更改密码", comment: ""), attributeDic: attDic)
            return
        }
        if let title: String = webView.title{// 设置标题
            setNavigationAttributeTitle(title, attributeDic: attDic)
        }
    }
    
    //MARK: 执行JS端的方法
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        
        if message.name == "ios_native" {
            
//            print("message.body: \(message.body)")
            
            let para: [String: Any] = queryStringToDictionary(string: message.body as! String)
//            print(para)
            
            let funcName: String = para["func"] as! String
            
            if funcName == "alert"{
                if let title: String = para["message"] as? String{
                    showMessage(title: title, andMessage: nil)
                }
            }else if funcName == "msgFromjs"{//接受JS返回的账号和密码
                if let accountFromJS: String = para["account"] as? String{
                    if let passwordFromJS: String = para["password"] as? String{
                        if isForgetPassword == true {
                            _ = navigationController?.popViewController(animated: true)
                        }else{
                            dismissSelf(animated: true, completion: { [weak self] _ in
                                self?.resetPasswordSuccess?(accountFromJS, passwordFromJS)
                            })
                        }
                    }
                }
            }
        }
    }
    
    //MARK: 执行JS的弹窗
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
//        print(message)
        completionHandler(
            showMessage(title: message, andMessage: nil)
        )
    }
    
    //MARK: 参数转字典
    fileprivate func queryStringToDictionary(string: String)->[String: Any]{
        let elements: [String] = string.components(separatedBy: "&")
        var retval: [String: Any] = [String: Any]()
        for e: String in elements {
            let pair: [String] = e.components(separatedBy: "=")
            retval[pair[0]] = pair[1]
        }
        return retval
    }

    //MARK: 执行JS的弹窗
    /// 执行JS的弹窗
    ///
    /// - Parameters:
    ///   - title: 标题
    ///   - message: 消息
    fileprivate func showMessage(title: String?, andMessage message: String?){
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel: UIAlertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        ac.addAction(cancel)
        present(ac, animated: true, completion: nil)
    }
}
