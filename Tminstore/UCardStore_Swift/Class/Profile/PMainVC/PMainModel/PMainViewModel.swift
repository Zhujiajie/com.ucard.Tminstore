//
//  PMainViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreData
import SDWebImage

class PMainViewModel: BaseViewModel {

    fileprivate let dataModel: PMainDataModel = PMainDataModel()
    
    /// 获取用户数据成功
    var getUserInfoSuccess: ((_ userInfo: UserInfoModel)->())?
    
    /// 已寄出的明信片数组
    var orderPostcardArray: [OrderPostcardModel] = [OrderPostcardModel]()
    
    /// 发布到时空圈的明信片的数组
    var releasePostcardArray: [PostcardModel] = [PostcardModel]()
    
    /// 买图明信片数组
    var purchaseArray: [OrderPostcardModel] = [OrderPostcardModel]()
    
    /// 发布到时空圈的数组
    var mapDataArray: [SpaceTimeDataModel] = [SpaceTimeDataModel]()
    
    /// tableView需要重新载入数据
    var tableViewShouldReloadData: ((_ type: ProfileButtonType)->())?
    /// 没有更多数据了
    var tableViewGetNoMoreData: ((_ type: ProfileButtonType)->())?
    
    /// 是否已经没有更多数据了
    var isNoMoreData: Bool = false
    
    /// 是否没有更多地图数据了
    var isNoMoreMapData: Bool = false
    
    var releasePageIndex: Int = 1
    var orderPageIndex: Int = 1{
        didSet{
            if orderPageIndex == 1{
                isNoMoreData = true
            }
        }
    }
    var purchasePageIndex: Int = 1
    
    var mapDataPageIndex: Int = 1{
        didSet{
            if mapDataPageIndex == 1{
                isNoMoreMapData = false
            }
        }
    }
    
    override init() {
        super.init()
        
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
    }
    
    //MARK: 获取用户信息
    /**
     获取用户信息
     
     - parameter success: 成功的闭包，返回ProfileModel
     - parameter failure: 失败的闭包
     */
    func getUserInfo(){
        dataModel.getUserInfo(success: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let userDic: [String: Any] = data["user"] as! [String: Any]
                let userInfo: UserInfoModel = UserInfoModel.initFromDic(dic: userDic)
                if let follows: Int = data["follows"] as? Int{
                    userInfo.followNumber = follows
                }
                if let friends: Int = data["friends"] as? Int{
                    userInfo.friendNumber = friends
                }
                if let fans: Int = data["fans"] as? Int{
                    userInfo.fansNumber = fans
                }
                self?.getUserInfoSuccess?(userInfo)
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
            self?.errorClosure?(error, nil)
        }, completionClosure: nil)
    }
    
    //MARK: 获取已寄出的明信片列表
    /**
     获取已寄出的明信片列表
     */
    func getOrderPostCard() {
        
        if isNoMoreData == true{
            tableViewGetNoMoreData?(ProfileButtonType.order)
            return
        }
        
        dataModel.getOrderPostCard(pageIndex: orderPageIndex, succesClosure: { [weak self](result: [String : Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let mailOrderList: [[String: Any]] = data["mailOrder"] as! [[String: Any]]
                
                if mailOrderList.isEmpty == true{
                    self?.isNoMoreData = true
                    self?.tableViewGetNoMoreData?(ProfileButtonType.order)//没有更多数据了
                }else{
                    
                    var orderArray: [OrderPostcardModel] = [OrderPostcardModel]()
                    var placeHolderURLArray: [URL] = [URL]()
                    for dic: [String: Any] in mailOrderList{
                        let order: OrderPostcardModel = OrderPostcardModel.initFromDic(dic: dic)
                        if let url: URL = order.frontPlaceImageURL{
                            placeHolderURLArray.append(url)
                        }//预先下载占位图片
                        orderArray.append(order)
                    }
                    SDWebImagePrefetcher.shared().prefetchURLs(placeHolderURLArray)
                    
                    // 重新获取数据
                    if self?.orderPageIndex == 1 {
                        self?.orderPostcardArray.removeAll()
                    }
                    self?.orderPostcardArray.append(contentsOf: orderArray)
                    self?.tableViewShouldReloadData?(ProfileButtonType.order)

                }
                self?.orderPageIndex += 1
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }, completionClosure: nil)
    }
    
    //MARK: 获取买图列表
    /**
     获取买图列表
     */
    func getPurchaseCard() {
        
        dataModel.getOrderPostCard(pageIndex: purchasePageIndex, succesClosure: { [weak self](result: [String : Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let mailOrderList: [[String: Any]] = data["mailOrder"] as! [[String: Any]]
                
                if mailOrderList.isEmpty == true{
                    self?.tableViewGetNoMoreData?(ProfileButtonType.purchase)//没有更多数据了
                }else{
                    
                    var purchaseArray: [OrderPostcardModel] = [OrderPostcardModel]()
                    var placeHolderURLArray: [URL] = [URL]()
                    for dic: [String: Any] in mailOrderList{
                        let order: OrderPostcardModel = OrderPostcardModel.initFromDic(dic: dic)
                        if let url: URL = order.frontPlaceImageURL{
                            placeHolderURLArray.append(url)
                        }//预先下载占位图片
                        purchaseArray.append(order)
                    }
                    SDWebImagePrefetcher.shared().prefetchURLs(placeHolderURLArray)
                    
                    // 重新获取数据
                    if self?.purchasePageIndex == 1 {
                        self?.purchaseArray.removeAll()
                    }
                    self?.purchaseArray.append(contentsOf: purchaseArray)
                    self?.tableViewShouldReloadData?(ProfileButtonType.purchase)
                    
                }
                self?.purchasePageIndex += 1
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
            }, completionClosure: nil)
    }
    
    //MARK: 获取用户已分享到时空圈的明信片
    /// 获取用户已分享到时空圈的明信片
    func getReleasedPostCard() {
        
        dataModel.getReleasePostCard(pageIndex: releasePageIndex, successClosure: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                
                let data: [String: Any] = result["data"] as! [String: Any]
                let communityList: [[String: Any]] = data["community"] as! [[String: Any]]
                
                if communityList.isEmpty == true{
                    self?.tableViewGetNoMoreData?(ProfileButtonType.community)//没有更多数据了
                }else{
                    
                    var postcardArray: [PostcardModel] = [PostcardModel]()
                    var placeHolderURLArray: [URL] = [URL]()
                    for dic: [String: Any] in communityList{
                        let postcard: PostcardModel = PostcardModel.initFromDic(dic: dic)
                        if let url: URL = postcard.frontPlaceImageURL{
                            placeHolderURLArray.append(url)
                        }//预先下载占位图片
                        postcardArray.append(postcard)
                    }
                    SDWebImagePrefetcher.shared().prefetchURLs(placeHolderURLArray)

                    // 重新获取数据
                    if self?.releasePageIndex == 1 {
                        self?.releasePostcardArray.removeAll()
                    }
                    self?.releasePostcardArray.append(contentsOf: postcardArray)
                    self?.tableViewShouldReloadData?(ProfileButtonType.community)
                }
                self?.releasePageIndex += 1
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
            self?.errorClosure?(error, nil)
        }, completionClosure: nil)
    }
    
    //MARK: 获取用户发布到地图的数据
    func getUserMapData(){
        
        if isNoMoreMapData == true{
            tableViewGetNoMoreData?(ProfileButtonType.map)
            return
        }
        
        dataModel.getMapData(pageIndex: mapDataPageIndex) { [weak self] (result: [String : Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let spaceTimeCircleList: [[String: Any]] = data["spaceTimeCircleList"] as? [[String: Any]]{
                        
                        if spaceTimeCircleList.isEmpty == true{
                            self?.isNoMoreMapData = true
                            self?.tableViewGetNoMoreData?(ProfileButtonType.map)//没有更多数据了
                        }else{
                            var mapArray: [SpaceTimeDataModel] = [SpaceTimeDataModel]()
                            var placeHolderURLArray: [URL] = [URL]()
                            for dic: [String: Any] in spaceTimeCircleList{
                                let mapModel: SpaceTimeDataModel = SpaceTimeDataModel.initFromDic(dic: dic)
                                if let url: URL = mapModel.frontPlaceImageURL{
                                    placeHolderURLArray.append(url)
                                }//预先下载占位图片
                                mapArray.append(mapModel)
                            }
                            SDWebImagePrefetcher.shared().prefetchURLs(placeHolderURLArray)
                            
                            // 重新获取数据
                            if self?.mapDataPageIndex == 1 {
                                self?.mapDataArray.removeAll()
                            }
                            self?.mapDataArray.append(contentsOf: mapArray)
                            self?.tableViewShouldReloadData?(ProfileButtonType.map)
                        }
                        self?.mapDataPageIndex += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
}
