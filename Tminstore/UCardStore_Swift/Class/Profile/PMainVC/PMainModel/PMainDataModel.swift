//
//  PMainDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreData

class PMainDataModel: BaseDataModel {
    
    //MARK: 获取用户信息
    /// 获取用户信息
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func getUserInfo(success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameter: [String: Any] = [
            "tminstoreToken": getUserToken()
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.requestUserInfo, ParametersDictionary: parameter, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            failure?(error)
            self?.errorClosure?(error)
        }) { [weak self] _ in
            completion?()
            self?.completionClosure?()
        }
    }
    
    //MARK: 获取邮寄的明信片列表
    /// 取邮寄的明信片列表
    ///
    /// - Parameters:
    ///   - pageIndex: 分页展示
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func getOrderPostCard(pageIndex: Int, succesClosure success: ((_ result: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        let url: String = appAPIHelp.checkOrderAPI + "/\(pageIndex)"
        
        let parameter: [String: Any] = [
            "tminstoreToken": getUserToken()
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: url, ParametersDictionary: parameter, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 获取发布到社区的明信片列表
    /**
     获取已收到社区的明信片列表
     
     - parameter success: 成功的闭包，返回[PostCardModel]
     - parameter failure: 失败的闭包
     */
    func getReleasePostCard(pageIndex: Int, successClosure success: ((_ postCardArray: [String: Any])->())?, failureClourse failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        let url: String = appAPIHelp.releaseCardAPI + "/\(pageIndex)"
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: url, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 获取用户发布到地图的数据
    /// 获取用户发布到地图的数据
    ///
    /// - Parameters:
    ///   - pageIndex: 页数
    ///   - success: 成功的闭包
    func getMapData(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlStr: String = appAPIHelp.checkMapDataAPI + "\(pageIndex)"
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: {[weak self] (error: Error?) in
            self?.errorClosure?(error)
        })
    }
}
