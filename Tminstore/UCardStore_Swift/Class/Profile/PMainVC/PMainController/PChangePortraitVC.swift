//
//  PChangePortraitVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/10.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: -------------设置头像的视图-------------

import UIKit

class PChangePortraitVC: BaseViewController {
    
    /// 是否使用dismiss退出
    var canUseDismiss: Bool = false
    
    /// 用户头像地址
    fileprivate let userLogoBase: String = "http://ucardstorevideo.b0.upaiyun.com" + portraitUpun
    
    var userPortrait: UIImage?{
        didSet{
            if userPortrait == nil {
                portraitView.image = UIImage(named: "testPortrait")
            }else{
                portraitView.image = userPortrait
            }
            configScrollView()
            setupGestureRecognizer()
        }
    }
    
    /// 更换头像成功
    var changePortraitSuccess: (()->())?
    
    fileprivate  let pickController: UIImagePickerController = UIImagePickerController()
    fileprivate let dataModel: BaseDataModel = BaseDataModel()
    fileprivate let portraitView: UIImageView = UIImageView()
    fileprivate let scrollView: UIScrollView = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
        setNavigationBar()//设置导航栏
        setPortraitView()//设置头像
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.transitionCoordinator?.animate(alongsideTransition: { [weak self] (_) in
            self?.navigationController?.navigationBar.barTintColor = UIColor.white
            self?.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
            self?.navigationController?.navigationBar.isTranslucent = false
            }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        view.backgroundColor = UIColor.white
        setNavigationTitle(NSLocalizedString("头像", comment: ""))//设置导航栏标题
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
        setNavigationBarRightButtonWithImageName("dot")//设置右按钮，右按钮为更换头像
    }
    
    //MARK: 设置头像
    fileprivate func setPortraitView(){
        
        scrollView.frame = CGRect(x: 0, y: -nav_statusHeight, width: screenWidth, height: screenHeight + nav_statusHeight)
        scrollView.backgroundColor = UIColor(hexString: "F9F9F9")
        view.addSubview(scrollView)
        
        portraitView.contentMode = UIViewContentMode.scaleAspectFit
        portraitView.clipsToBounds = true
        portraitView.isUserInteractionEnabled = true
        portraitView.frame = scrollView.bounds
        portraitView.backgroundColor = UIColor(hexString: "F9F9F9")
        
        scrollView.addSubview(portraitView)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPortraitView(_:)))
        tap.numberOfTapsRequired = 1
        portraitView.addGestureRecognizer(tap)
        
        pickController.delegate = self
    }
    
    //MARK: 设置scrollView的缩放
    fileprivate func configScrollView(){
        scrollView.contentSize = portraitView.bounds.size
        scrollView.autoresizingMask = [UIViewAutoresizing.flexibleHeight, UIViewAutoresizing.flexibleWidth]
        scrollView.delegate = self
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 2.0
        scrollView.zoomScale = 1.0
    }
    
    //MARK: 双击缩放图片
    fileprivate func setupGestureRecognizer(){
        let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(_:)))
        doubleTap.numberOfTapsRequired = 2
        portraitView.addGestureRecognizer(doubleTap)
    }
    
    //MARK: 上传正面图片
    fileprivate func uploadPortraitToUPun(image: UIImage){
        
        showSVProgress(title: nil)
        
        let uuidString: String = UUID().uuidString
        
        let newImage: UIImage = UIImage(image: image, scaledToWidth: 500)
        
        guard let imageData: Data = UIImagePNGRepresentation(newImage) else {
            handelNetWorkError(nil)
            return }
        
        dataModel.uploadToUpyun(data: imageData, andSaveKey: "/\(portraitUpun)/\(uuidString).png", andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.uploadNewPortrait(uuidString: uuidString)
            }, failClosure: { [weak self] (error: Error?, _, _) in
                DispatchQueue.main.async {
                    self?.handelNetWorkError(error)
                    self?.dismissSVProgress()
                }
            }, progressClosure: {(completeCount: Float, totalCount: Float) in
                SVProgressHUD.showProgress(completeCount/totalCount, status: NSLocalizedString("上传图片中", comment: ""))
        })
    }
    
    //MARK: 上传新头像
    fileprivate func uploadNewPortrait(uuidString: String){
        
        let userLogoStr: String = userLogoBase + uuidString + ".png"
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "userLogo": userLogoStr
            ] as [String: Any]
        
        dataModel.sendPOSTRequestWithURLString(URLStr: appAPIHelp.uploadPortraitAPI, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String: Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.changePortraitSuccess?()
                self?.backToFrontVC()
            }else{
                self?.handelNetWorkError(nil, withResult: result)
            }
            }, failureClourse: { [weak self](error: Error?) in
                self?.handelNetWorkError(error)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        backToFrontVC()
    }
    
    //MARK: 点击导航栏右按钮，切换头像
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        alertSheet()
    }
    
    //MARK: 退出
    fileprivate func backToFrontVC(){
        if canUseDismiss {
            dismissSelf()
        }else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
}

extension PChangePortraitVC: UIScrollViewDelegate{
    //MARK: 允许image缩放
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return portraitView
    }
}

//MARK:--------手势和点击时间----------
extension PChangePortraitVC{
    
    //MARK: 双击
    func handleDoubleTap(_ recognizer: UITapGestureRecognizer) {
        //缩小
        if scrollView.zoomScale > scrollView.minimumZoomScale {
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        }
            //放大
        else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }

    
    //MARK: 点击头像开始选择照片
    func tapPortraitView(_ tap: UITapGestureRecognizer) {
        alertSheet()
    }
    
    //MARK: 更换头像的sheet
    fileprivate func alertSheet(){
        let sheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePhotoAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("拍照", comment: ""), style: .default) { [weak self] (_) in
            self?.beginTakePhoto()
        }//拍照
        sheet.addAction(takePhotoAction)
        
        let photoAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("相册", comment: ""), style: .default) { [weak self] (_) in
            self?.beginChoosePhoto()
        }//从相册选择
        sheet.addAction(photoAction)
        
        let savealert: UIAlertAction = UIAlertAction(title: NSLocalizedString("保存", comment: ""), style: .default) { [weak self] (_) in
            
            if self?.userPortrait != nil {
//                UIImageWriteToSavedPhotosAlbum((self?.userPortrait)!, nil, nil, nil)
                self?.alert(alertTitle: NSLocalizedString("保存成功", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("好的", comment: ""), rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)
            }
        }
        sheet.addAction(savealert)
        
        let cancel: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: .cancel, handler: nil)
        sheet.addAction(cancel)
        
        present(sheet, animated: true, completion: nil)
    }
}

//MARK: -------------UIImagePickerControllerDelegate---------------------
extension PChangePortraitVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK: 进入拍照视图
    /**
     进入拍照视图
     */
    func beginTakePhoto() {
        
        //判断相机是否可用
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            
            pickController.sourceType = UIImagePickerControllerSourceType.camera
            pickController.showsCameraControls = true
            pickController.allowsEditing = true
            
            present(pickController, animated: true, completion: nil)
        }else{
            alert(alertTitle: nil, messageString: NSLocalizedString("相机不可用", comment: ""), leftButtonTitle: NSLocalizedString("好的", comment: ""), rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//提醒用户相机不可用
        }
    }
    
    //MARK: 进入相册
    /**
     进入相册
    */
    func beginChoosePhoto() {
        //判断相册是否可用
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum) {
            
            pickController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            pickController.allowsEditing = true
        
            present(pickController, animated: true, completion: nil)
        }else{
            alert(alertTitle: nil, messageString: NSLocalizedString("相册不可用", comment: ""), leftButtonTitle: NSLocalizedString("好的", comment: ""), rightButtonTitle: nil, leftClosure: nil, rightClosure: nil, presentComplition: nil)//提醒用户相册不可用
        }
    }
    
    //MARK: 成功获取了照片
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image: UIImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            portraitView.image = image
            uploadPortraitToUPun(image: image)
        }
        dismiss(animated: true, completion: nil)
    }
    //MARK: 取消获取照片
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
