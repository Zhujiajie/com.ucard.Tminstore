//
//  PMainController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

//---------------------个人界面-------------------------
import UIKit
import CoreData
import SnapKit

class PMainController: BaseViewController {
    
    /// 数据为空时显示的视图大小
    fileprivate let emptyViewFrame: CGRect = CGRect(x: 0, y: 0, width: screenWidth, height: 392/baseHeight)
    
    fileprivate let viewModel: PMainViewModel = PMainViewModel()
    fileprivate lazy var orderViewModel: PDetialViewModel = PDetialViewModel()
    
    /// 用户信息
    fileprivate var userInfo: UserInfoModel?
    
    /// 是否是第一次获取买图数据
    fileprivate var isFirstTimeGetPurchaseData: Bool = true
    /// 是否是第一次获取发布的信息
    fileprivate var isFirstTimeGetReleaseData: Bool = true
    /// 是否是第一次获取订单信息
    fileprivate var isFirstTimeGetOrderData: Bool = true
    /// 是否是第一次获取地图数据
    fileprivate var isFirstTimeGetMapData: Bool = true
    /// 展示用户头像和四个按钮的视图
    fileprivate var userInfoView: PMainView?
    
    /// 社区的tableView
    fileprivate var releaseTableView: PMainTableView?
    /// 订单的tableView
    fileprivate var orderTableView: PMainTableView?
    /// 买图的tableView
    fileprivate var purchaseTableView: PMainTableView?
    /// 时光圈的collectionView
    fileprivate var mapCollectionView: PMainMapCollectionView?
    /// 承载tableView的collectionView
    fileprivate var collectionView: PMainCollectionView?
    /// 复用标签的数组
    fileprivate var reuseIdentifierArray: [String?] = [nil, nil, nil, nil]
    
    /// 寄件箱为空时展示的透明视图，寄件箱为空时，不显示
    fileprivate var emptySendView: PMainView?
    /// 发布到时空圈为空时，展示的视图
    fileprivate var emptyReleaseView: PMainView?
    /// 收件箱为空时的视图
    fileprivate var emptyReceiveView: PMainView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hidesBottomBarWhenPushed = false
        
        view.backgroundColor = UIColor(hexString: "#F8F8F8")
        
        setNavigationBar()//设置导航栏
        setUserInfoView()
        setTableViews()//设置tableView
        setCollectionView()
        closures()//各种闭包回调
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.transitionCoordinator?.animate(alongsideTransition: { [weak self]  (_) in
            self?.transparentNavigationBar()
        }, completion: nil)
        guard let userInfoView: PMainView = userInfoView else { return }
        userInfoView.setBackgroundImageViewBlur()
    }
    
    //MARK: 进入该界面之后，获取用户信息，并保存用户的账户信息用于友盟统计
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //用户已登录，每次进入改页面，都要刷新用户用数据
        if getUserLoginStatus() == true{
            viewModel.getUserInfo()
        }
        
        getPurchaseData()//尝试获取买图数据
    }
    
    //MARK: 视图将要消失时，取消所有网络请求
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationBarRightButtonWithImageName("set")
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        transparentNavigationBar()
    }
    
    //MARK: 顶部展示个人头像和四个按钮的视图
    fileprivate func setUserInfoView(){
        userInfoView = PMainView.userInfoView()
        view.addSubview(userInfoView!)
        userInfoView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(-nav_statusHeight)
            make.left.right.equalTo(view)
            make.height.equalTo(253/baseWidth)
        })
    }
    
    //MARK:设置草稿tableView
    fileprivate func setTableViews(){
        releaseTableView = PMainTableView.setTableView(setDelegate: self, andDateSource: self, isRefreshAvailable: true)
        releaseTableView?.tag = ProfileButtonType.community.rawValue
        releaseTableView?.scrollsToTop = false
        orderTableView = PMainTableView.setTableView(setDelegate: self, andDateSource: self, isRefreshAvailable: true)
        orderTableView?.tag = ProfileButtonType.order.rawValue
        orderTableView?.scrollsToTop = false
        purchaseTableView = PMainTableView.setTableView(setDelegate: self, andDateSource: self, isRefreshAvailable: true)
        purchaseTableView?.tag = ProfileButtonType.purchase.rawValue
        purchaseTableView?.scrollsToTop = true
        mapCollectionView = PMainMapCollectionView()
        mapCollectionView?.tag = ProfileButtonType.map.rawValue
        mapCollectionView?.scrollsToTop = false
        mapCollectionView?.delegate = self
        mapCollectionView?.dataSource = self
    }
    
    //MARK: 设置collectionView
    fileprivate func setCollectionView(){
        collectionView = PMainCollectionView.profileCollectionView()
        view.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(userInfoView!.snp.bottom).offset(1)
            make.left.bottom.right.equalTo(view)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    //MARK: 点击设置按钮，进入设置界面
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        let settingVC: PSettingController = PSettingController()
        settingVC.hidesBottomBarWhenPushed = true
        settingVC.umengViewName = "设置"
        settingVC.userInfo = userInfo
        navigationController?.pushViewController(settingVC, animated: true)
    }
    
    //MARK: 提示用户功能正在开发中
    fileprivate func warmAlert(){
        alert(alertTitle: NSLocalizedString("功能开发中\n敬请期待", comment: ""))
    }
    
    //MARK: 进入优惠券及界面
    fileprivate func enterCouponVC(){
        let couponVC = MUCouponVC()
        couponVC.hidesBottomBarWhenPushed = true
        couponVC.canPay = false
        couponVC.umengViewName = "查看优惠券"
        navigationController?.pushViewController(couponVC, animated: true)
    }
}

//MARK:--------------手势和点击事件-----------------
extension PMainController{
    
    //MARK: 各种闭包回调
    func closures(){
        
        //MARK: 点击头像，去更换头像
        userInfoView?.tapPortraitViewClosure = {[weak self] _ in
            let pcVC = PChangePortraitVC()
            pcVC.hidesBottomBarWhenPushed = true
            pcVC.userPortrait = self?.userInfoView?.userPortrait
            pcVC.umengViewName = "更换头像"
            self?.navigationController?.pushViewController(pcVC, animated: true)
        }
        
        //MARK: 下拉刷新，上拉刷新
        releaseTableView?.mj_header.refreshingBlock = {[weak self] _ in
            self?.viewModel.releasePageIndex = 1
            self?.viewModel.getReleasedPostCard()
        }
        releaseTableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.getReleasedPostCard()
        }
        orderTableView?.mj_header.refreshingBlock = {[weak self] _ in
            self?.viewModel.orderPageIndex = 1
            self?.viewModel.getOrderPostCard()
        }
        orderTableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.getOrderPostCard()
        }
        purchaseTableView?.mj_header.refreshingBlock = {[weak self] _ in
            self?.viewModel.purchasePageIndex = 1
            self?.viewModel.getPurchaseCard()
        }
        purchaseTableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.getPurchaseCard()
        }
        mapCollectionView?.mj_header.refreshingBlock = {[weak self] _ in
            self?.viewModel.mapDataPageIndex = 1
            self?.viewModel.getUserMapData()
        }
        mapCollectionView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.getUserMapData()
        }
        
        //MARK: 点击按钮，切换collectionView的cell
        userInfoView?.touchButtonClosure = {[weak self] (buttonType: ProfileButtonType) in
            var item: Int = 0
            switch buttonType {
            case ProfileButtonType.purchase:
                item = 0
            case ProfileButtonType.community:
                item = 1
            case ProfileButtonType.map:
                item = 2
            case ProfileButtonType.order:
                item = 3
            case ProfileButtonType.record:
                self?.warmAlert()
                return
            case ProfileButtonType.wallet:
                self?.enterCouponVC()
                return
            case ProfileButtonType.draft:
                let draftVC: PDraftViewController = PDraftViewController()
                draftVC.umengViewName = "草稿箱"
                draftVC.hidesBottomBarWhenPushed = true
                _ = self?.navigationController?.pushViewController(draftVC, animated: true)
                return
            }
            self?.collectionView?.scrollToItem(at: IndexPath(item: item, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            delay(seconds: 0.5, completion: {
                _ = self?.checkTableViewToScrollToTop(scrollView: (self?.collectionView)!)// 更改tableView是否可以滚动到顶部
            })
        }
        
        //MARK: 成功获取到用户的信息
        viewModel.getUserInfoSuccess = {[weak self] (info: UserInfoModel) in
            self?.userInfo = info
            self?.userInfoView?.userNickName = info.nickName
            if let url: URL = info.userLogoURL {
                self?.userInfoView?.userPortraitURL = url//设置用户头像
            }
        }
        
        //MARK: 网络失败
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
            if self?.releaseTableView != nil && self?.orderTableView != nil && self?.purchaseTableView != nil && self?.mapCollectionView != nil{
                self?.releaseTableView?.mj_header.endRefreshing()
                self?.releaseTableView?.mj_footer.endRefreshing()
                self?.orderTableView?.mj_header.endRefreshing()
                self?.orderTableView?.mj_footer.endRefreshing()
                self?.purchaseTableView?.mj_footer.endRefreshing()
                self?.purchaseTableView?.mj_header.endRefreshing()
                self?.mapCollectionView?.mj_header.endRefreshing()
                self?.mapCollectionView?.mj_footer.endRefreshing()
            }
        }
        
        //MARK: tableView需要刷新
        viewModel.tableViewShouldReloadData = {[weak self] (type: ProfileButtonType) in
            
            if type == ProfileButtonType.map{
                self?.mapCollectionView?.mj_header.endRefreshing()
                self?.mapCollectionView?.mj_footer.endRefreshing()
                self?.mapCollectionView?.mj_footer.resetNoMoreData()
                self?.mapCollectionView?.reloadData()
                return
            }
            
            var tableView: PMainTableView
            switch type {
            case ProfileButtonType.community:
                tableView = (self?.releaseTableView)!
            case ProfileButtonType.order:
                tableView = (self?.orderTableView)!
            
            default:
                tableView = (self?.purchaseTableView)!
            }
            tableView.mj_header.endRefreshing()
            tableView.mj_footer.endRefreshing()
            tableView.mj_footer.resetNoMoreData()
            tableView.reloadData()
        }
        
        //MARK: 没有更多数据了
        viewModel.tableViewGetNoMoreData = {[weak self] (type: ProfileButtonType) in
            
            if type == ProfileButtonType.map{
                self?.mapCollectionView?.mj_header.endRefreshing()
                self?.mapCollectionView?.mj_footer.endRefreshingWithNoMoreData()
                self?.mapCollectionView?.reloadData()
                return
            }

            var tableView: PMainTableView
            switch type {
            case ProfileButtonType.community:
                tableView = (self?.releaseTableView)!
            case ProfileButtonType.order:
                tableView = (self?.orderTableView)!
            default:
                tableView = (self?.purchaseTableView)!
            }
            tableView.mj_header.endRefreshing()
            tableView.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 删除订单成功的回调
        orderViewModel.deleteSuccess = {[weak self] (indexPath: IndexPath?) in
            if let indexPath: IndexPath = indexPath{
                self?.viewModel.orderPostcardArray.remove(at: indexPath.row)
                self?.orderTableView?.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
                self?.orderTableView?.reloadData()
            }
        }
        
        //MARK: 删除出错的回调
        orderViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
}
//MARK: UICollectionViewDelegate, UICollectionViewDataSource{
extension PMainController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == ProfileButtonType.map.rawValue{
            mapCollectionView?.mj_footer.isHidden = viewModel.mapDataArray.isEmpty
            return viewModel.mapDataArray.count
        }else{
            return 4
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == ProfileButtonType.map.rawValue{
            let cell: PMainMapCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PMainMapCollectionViewCellappReuseIdentifier, for: indexPath) as! PMainMapCollectionViewCell
            cell.mapModel = viewModel.mapDataArray[indexPath.item]
            return cell
        }
        
        // 防止cell复用
        var identifier = reuseIdentifierArray[indexPath.item]
        if identifier == nil {
            identifier = "\(indexPath.item)"
            reuseIdentifierArray[indexPath.item] = identifier!
            self.collectionView?.register(PMainCollectionViewCell.self, forCellWithReuseIdentifier: identifier!)
        }
        let cell: PMainCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier!, for: indexPath) as! PMainCollectionViewCell
        switch indexPath.item {
        case 0:
            cell.tableView = purchaseTableView
        case 1:
            cell.tableView = releaseTableView
        case 2:
            cell.collectionView = mapCollectionView
        default:
            cell.tableView = orderTableView
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if collectionView.tag == ProfileButtonType.map.rawValue{
            let mapDataVC: PMapController = PMapController()
            mapDataVC.hidesBottomBarWhenPushed = true
            mapDataVC.mapModel = viewModel.mapDataArray[indexPath.item]
            mapDataVC.umengViewName = "时光圈"
            //MARK: 删除时光圈内容，成功。刷新时光圈数据
            mapDataVC.deleteSuccess = {[weak self] _ in
                self?.mapCollectionView?.mj_header.beginRefreshing()
            }
            _ = navigationController?.pushViewController(mapDataVC, animated: true)
        }
    }
    
    //MARK: 滚动collectionView时，上方的绿色线的动画
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.isKind(of: PMainCollectionView.self) == true{
            let buttonType: ProfileButtonType = checkTableViewToScrollToTop(scrollView: scrollView)
            userInfoView?.touchButtonAnimation(buttonType: buttonType)
        }
    }
    
    //MARK: 根据scrollView的偏移量，决定哪个tableView可以点击状态栏返回顶端
    fileprivate func checkTableViewToScrollToTop(scrollView: UIScrollView)->ProfileButtonType{
        var buttonType: ProfileButtonType
        switch scrollView.contentOffset.x {
        case 0:
            buttonType = ProfileButtonType.purchase
        case screenWidth:
            buttonType = ProfileButtonType.community
            getCommunituReleaseData()//尝试获取发布到社区的数据
        case screenWidth * 2:
            buttonType = ProfileButtonType.map
            getMapData()//尝试获取地图数据
        default:
            buttonType = ProfileButtonType.order
            getOrderInfo()//尝试自动获取订单信息
        }
        
        purchaseTableView?.scrollsToTop = scrollView.contentOffset.x == 0
        releaseTableView?.scrollsToTop = scrollView.contentOffset.x == screenWidth
        orderTableView?.scrollsToTop = scrollView.contentOffset.x == screenWidth * 3
        mapCollectionView?.scrollsToTop = scrollView.contentOffset.x == screenWidth * 2
        return buttonType
    }
    
    //MARK: 尝试自动获取订单信息
    fileprivate func getOrderInfo(){
        if isFirstTimeGetOrderData == true{
            orderTableView?.mj_header.beginRefreshing()
            isFirstTimeGetOrderData = false
        }
    }
    
    //MARK: 尝试获取发布到社区的数据
    fileprivate func getCommunituReleaseData(){
        if isFirstTimeGetReleaseData == true{
            releaseTableView?.mj_header.beginRefreshing()
            isFirstTimeGetReleaseData = false
        }
    }
    
    //MARK: 尝试获取买图数据
    fileprivate func getPurchaseData(){
        if isFirstTimeGetPurchaseData == true{
            purchaseTableView?.mj_header.beginRefreshing()
            isFirstTimeGetPurchaseData = false
        }
    }
    
    //MARK: 获取发布到地图的数据
    fileprivate func getMapData(){
        if isFirstTimeGetMapData == true{
            mapCollectionView?.mj_header.beginRefreshing()
            isFirstTimeGetMapData = false
        }
    }
    //MARK: 刷新订单数据
    func refreshOrderData(){
        orderTableView?.mj_header.beginRefreshing()
    }
}

//MARK: ------------UITableViewDelegate, UITableViewDataSource----------
extension PMainController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //MARK: 设置cell的个数，已经数据为空时的展示的空白视图
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView.tag {
        case ProfileButtonType.community.rawValue:
            if viewModel.releasePostcardArray.isEmpty == true{
                if emptyReleaseView == nil{
                    emptyReleaseView = PMainView.emptyReleaseView()
                    releaseTableView?.addSubview(emptyReleaseView!)
                    emptyReleaseView?.frame = emptyViewFrame
                }
            }else{
                if emptyReleaseView != nil{
                    emptyReleaseView?.removeFromSuperview()
                    emptyReleaseView = nil
                }
            }
            releaseTableView?.mj_footer.isHidden = viewModel.releasePostcardArray.isEmpty
            return viewModel.releasePostcardArray.count
        case ProfileButtonType.order.rawValue:
            if viewModel.orderPostcardArray.isEmpty == true{
                if emptySendView == nil {
                    emptySendView = PMainView.emptySendView()
                    orderTableView?.addSubview(emptySendView!)
                    emptySendView?.frame = emptyViewFrame
                }
            }else{
                if emptySendView != nil{
                    emptySendView?.removeFromSuperview()
                    emptySendView = nil
                }
            }
            orderTableView?.mj_footer.isHidden = viewModel.orderPostcardArray.isEmpty
            return viewModel.orderPostcardArray.count
        case ProfileButtonType.purchase.rawValue:
            purchaseTableView?.mj_footer.isHidden = viewModel.purchaseArray.isEmpty
            return viewModel.purchaseArray.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView.tag {
        case ProfileButtonType.purchase.rawValue:
            return purchaseTableViewCell(indexPath: indexPath)
        case ProfileButtonType.community.rawValue:
            return releaseTableViewCell(indexPath: indexPath)
        default:
            return orderTableViewCell(indexPath: indexPath)
        }
    }
    //MARK: 点击cell，进入相应的详情视图
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if userInfo == nil{return}
        
        switch tableView.tag {
        case ProfileButtonType.purchase.rawValue:
            let purchseVC: PPurchaseController = PPurchaseController()
            purchseVC.umengViewName = "买图详情"
            purchseVC.order = viewModel.purchaseArray[indexPath.row]
            purchseVC.hidesBottomBarWhenPushed = true
            _ = navigationController?.pushViewController(purchseVC, animated: true)
            
        case ProfileButtonType.community.rawValue:
            let releaseVC: MainDetialController = MainDetialController()
            releaseVC.hidesBottomBarWhenPushed = true
            releaseVC.isReleaseVC = true
            releaseVC.postcard = viewModel.releasePostcardArray[indexPath.row]
            _ = navigationController?.pushViewController(releaseVC, animated: true)
            // 取消分享，分享或删除成功
            releaseVC.cancelOrReleaseOrDeleteSuccess = { [weak self] _ in
                self?.releaseTableView?.mj_header.beginRefreshing()//刷新数据
            }
        case ProfileButtonType.order.rawValue:
            let orderVC: PDetialVC = PDetialVC()
            orderVC.hidesBottomBarWhenPushed = true
            orderVC.userInfo = userInfo
            orderVC.order = viewModel.orderPostcardArray[indexPath.row]
            _ = navigationController?.pushViewController(orderVC, animated: true)
            // 分享到社区成功
            orderVC.shareToCommunityOrDeleteSuccess = {[weak self] _ in
                self?.refreshOrderData()//刷新订单数据
                self?.releaseTableView?.mj_header.beginRefreshing()//刷新发布数据
            }// 分享到时空圈成功，返回刷新数据
        default:
            break
        }
    }
    
    //MARK: 设置发布详细的cell
    fileprivate func releaseTableViewCell(indexPath: IndexPath)->PMainReleaseTableViewCell{
        let cell: PMainReleaseTableViewCell = releaseTableView!.dequeueReusableCell(withIdentifier: appReuseIdentifier.PMainReleaseCellReuseIdentifier, for: indexPath) as! PMainReleaseTableViewCell
        cell.postcard = viewModel.releasePostcardArray[indexPath.row]
        return cell
    }
    
    //MARK: 设置订单详情的cell
    fileprivate func orderTableViewCell(indexPath: IndexPath)->PMainOrderTableViewCell{
        let cell: PMainOrderTableViewCell = orderTableView!.dequeueReusableCell(withIdentifier: appReuseIdentifier.PMainOrderCellReuseIdentifier, for: indexPath) as! PMainOrderTableViewCell
        cell.order = viewModel.orderPostcardArray[indexPath.row]
        cell.indexPath = indexPath
        //点击删除按钮的事件
        cell.touchDeleteButtonClosure = {[weak self] (indexPath: IndexPath) in
            //删除的提示框
            if let paymentOrderNo: String = self?.viewModel.orderPostcardArray[indexPath.row].paymentOrderNo{
                self?.deleteAlert(alertTitle: NSLocalizedString("确认删除？", comment: ""), messageString: nil, rightClosure: {
                    self?.orderViewModel.deleteOrder(paymentOrderNo: paymentOrderNo, andIndexPath: indexPath)
                })
            }
        }
        return cell
    }
    
    //MARK: 设置买图cell
    fileprivate func purchaseTableViewCell(indexPath: IndexPath)->PMainPurchaseTableViewCell{
        let cell: PMainPurchaseTableViewCell = orderTableView!.dequeueReusableCell(withIdentifier: appReuseIdentifier.PMainPurchaseCellReuseIdentifier, for: indexPath) as! PMainPurchaseTableViewCell
        cell.order = viewModel.purchaseArray[indexPath.row]
        return cell
    }
}

