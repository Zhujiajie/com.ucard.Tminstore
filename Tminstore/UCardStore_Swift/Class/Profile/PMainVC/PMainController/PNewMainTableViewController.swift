//
//  PNewMainTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 在个人界面，展示分类的tableView

import UIKit
import SnapKit

class PNewMainTableViewController: UITableViewController {

    ///点击tableView的Cell
    var selectTableView: ((_ indexPath: IndexPath)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor.white
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.register(PNewMainTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.PNewMainTableViewCellReuseIdentifier)
        
//        tableView.contentInset = UIEdgeInsets(top: 30.8/baseWidth, left: 0, bottom: 0, right: 0)
    }

    //MARK: 设置未读消息数
    /// 设置未读消息数
    ///
    /// - Parameter count: 消息数量
    func setUnreadMessageNumber(count: Int){
        if let cell: PNewMainTableViewCell = tableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? PNewMainTableViewCell{
            cell.setUnreadMessageNumber(count: count)
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PNewMainTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.PNewMainTableViewCellReuseIdentifier, for: indexPath) as! PNewMainTableViewCell
        
        cell.indexPath = indexPath

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectTableView?(indexPath)
    }
}

class PNewMainTableViewCell: UITableViewCell{
    
    var indexPath: IndexPath?{
        didSet{
            if indexPath != nil{
                setUI()
            }
        }
    }

    fileprivate var iconView: BaseImageView?
    fileprivate var titleLabel: BaseLabel?
    fileprivate var messageNumberLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.layer.masksToBounds = true
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        if iconView != nil {return}
        
        var iconNameString: String
        var titleString: String
        
//        switch indexPath!.row {
//        case 0:
//            iconNameString = "mySpace"
//            titleString = NSLocalizedString("我的时空", comment: "")
//        case 1:
//            iconNameString = "myRecord"
//            titleString = NSLocalizedString("我的录屏", comment: "")
//        case 2:
//            iconNameString = "myOrder"
//            titleString = NSLocalizedString("我的订单", comment: "")
//        case 3:
//            iconNameString = "myImage"
//            titleString = NSLocalizedString("我的图片", comment: "")
//        case 4:
//            iconNameString = "myMessage"
//            titleString = NSLocalizedString("我的消息", comment: "")
//        default:
//            iconNameString = "myWallet"
//            titleString = NSLocalizedString("我的钱包", comment: "")
//        }
        
        switch indexPath!.row {
        case 0:
            iconNameString = "myOrder"
            titleString = NSLocalizedString("我的订单", comment: "")
        case 1:
            iconNameString = "myImage"
            titleString = NSLocalizedString("我的图片", comment: "")
        case 2:
            iconNameString = "myMessage"
            titleString = NSLocalizedString("我的消息", comment: "")
        default:
            iconNameString = "myWallet"
            titleString = NSLocalizedString("我的钱包", comment: "")
        }
        
        iconView = BaseImageView.normalImageView(andImageName: iconNameString)
        contentView.addSubview(iconView!)
        iconView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(34/baseWidth)
            make.size.equalTo(CGSize(width: 18/baseWidth, height: 18/baseWidth))
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 16), andTextColorHex: "646464", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: titleString)
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(iconView!)
            make.left.equalTo(iconView!.snp.right).offset(14/baseWidth)
        })
        
        messageNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "B2B2B2", andTextAlignment: NSTextAlignment.center)
        contentView.addSubview(messageNumberLabel!)
        messageNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(iconView!)
            make.right.equalTo(contentView).offset(-28/baseWidth)
        })
    }
    
    //MARK: 设置未读消息数
    /// 设置未读消息数
    ///
    /// - Parameter count: 消息数量
    func setUnreadMessageNumber(count: Int){
        
        messageNumberLabel?.isHidden = count == 0
        messageNumberLabel?.text = "\(count)"
        messageNumberLabel?.backgroundColor = UIColor(hexString: "FF5C5C")
        messageNumberLabel?.textColor = UIColor(hexString: "FFFFFF")
        messageNumberLabel?.layer.masksToBounds = true
        
        if var textSize: CGSize = messageNumberLabel?.intrinsicContentSize{
            if textSize.width < textSize.height{
                textSize.width = textSize.height
                messageNumberLabel?.layer.cornerRadius = textSize.height/2
            }else{
                textSize.height = textSize.width
                messageNumberLabel?.layer.cornerRadius = textSize.width/2
            }
            messageNumberLabel?.snp.remakeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(iconView!)
                make.right.equalTo(contentView).offset(-28/baseWidth)
                make.size.equalTo(textSize)
            })
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
