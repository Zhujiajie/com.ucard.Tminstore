//
//  PMainNavigationVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/2.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMainNavigationVC: UINavigationController {
    
    /// 是否要隐藏点击头像视图
    var shouldHidePortraitView: Bool = false{
        didSet{
            if portraitTouchView != nil{
                portraitTouchView?.isHidden = shouldHidePortraitView
            }
        }
    }
    
    /// 点击头像的事件
    var tapPortraitView: (()->())?
    
    /// 用于实现点击头像的效果
    fileprivate var portraitTouchView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        portraitTouchView = UIView()
//        portraitTouchView?.backgroundColor = UIColor.clear
//        view.addSubview(portraitTouchView!)
//        portraitTouchView?.snp.makeConstraints({ (make: ConstraintMaker) in
//            make.top.equalTo(view)
//            make.centerX.equalTo(view)
//            make.size.equalTo(CGSize(width: 75/baseWidth, height: 108/baseHeight))
//        })
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPortrait(tap:)))
//        portraitTouchView?.addGestureRecognizer(tap)
    }
    
    //MARK: 点击头像
    func tapPortrait(tap: UITapGestureRecognizer){
        tapPortraitView?()
    }
}
