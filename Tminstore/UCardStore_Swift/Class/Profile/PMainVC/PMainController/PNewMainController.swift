
//
//  PNewMainController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PNewMainController: BaseViewController {

    fileprivate let messageViewModel: MainViewModel = MainViewModel()
    fileprivate let userInfoViewModel: PMainViewModel = PMainViewModel()
    
    /// 点击了头像，去更换头像
    var tapUserPortraitView: ((_ userPortrait: UIImage?)->())?
    
    /// 获得未读消息数量
    var getUnreadMessageNumber: ((_ count: Int)->())?
    
    /// 点击好友, 关注或粉丝按钮
    var touchFriendsFollowFansButton: ((_ type: UserOtherInfoType)->())?
    
    /// 点击了tableView
    var selectTableView: ((_ indexPath: IndexPath)->())?
    
    /// 点击设置按钮的事件
    var touchSetButtonClosure: (()->())?
    
    /// 点击了教程按钮的事件
    var touchTutorialButtonClosure: (()->())?
    
    /// 用户信息模型
    var userInfoModel: UserInfoModel?{
        didSet{
            userInfoView?.userModel = userInfoModel
        }
    }
    
    /// 展示用户信息的视图
    fileprivate var userInfoView: PNewUserView?
    
    /// 展示分类的tableView
    fileprivate var tableViewController: PNewMainTableViewController?
    /// 设置按钮
    fileprivate var settingButton: BaseButton?
    /// 用户指南按钮
    fileprivate var tutorialButton: BaseButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        closure()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        userInfoView = PNewUserView()
        view.addSubview(userInfoView!)
        userInfoView?.snp.makeConstraints({ (make) in
            make.top.right.equalTo(view)
            make.width.equalTo(281/baseWidth)
            make.height.equalTo(148/baseWidth)
        })
        
//        tutorialButton = BaseButton.normalTitleButton(title: NSLocalizedString("用户指南", comment: ""), andTextFont: UIFont.systemFont(ofSize: 16), andTextColorHexString: "8D8D8D")
//        view.addSubview(tutorialButton!)
//        tutorialButton?.snp.makeConstraints({ (make) in
//            make.bottom.right.equalTo(view)
//            make.size.equalTo(CGSize(width: 173.5/baseWidth, height: 67.5/baseWidth))
//        })
        
        let horizonGrayLine: BaseView = BaseView.colorLine(colorString: "D6D6D6")
        view.addSubview(horizonGrayLine)
        horizonGrayLine.snp.makeConstraints { (make) in
            make.bottom.equalTo(view).offset(-46/baseWidth)
            make.right.equalTo(view)
            make.width.equalTo(userInfoView!)
            make.height.equalTo(0.5)
        }
        
        let verticalGrayLine: BaseView = BaseView()
        verticalGrayLine.backgroundColor = UIColor.clear
        view.addSubview(verticalGrayLine)
        verticalGrayLine.snp.makeConstraints { (make) in
            make.right.equalTo(view).offset(-173.5/baseWidth)
            make.top.equalTo(horizonGrayLine.snp.bottom).offset(18.5/baseWidth)
            make.bottom.equalTo(view).offset(-15/baseWidth)
            make.width.equalTo(0.5)
        }
        
        settingButton = BaseButton.normalTitleButton(title: NSLocalizedString("设置", comment: ""), andTextFont: UIFont.systemFont(ofSize: 16), andTextColorHexString: "8D8D8D")
        view.addSubview(settingButton!)
        settingButton?.snp.makeConstraints({ (make) in
            make.right.equalTo(verticalGrayLine.snp.left)
            make.top.equalTo(horizonGrayLine.snp.bottom)
            make.bottom.equalTo(view)
            make.leftMargin.equalTo(horizonGrayLine)
        })
        
        tableViewController = PNewMainTableViewController()
        view.addSubview(tableViewController!.view)
        tableViewController?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(userInfoView!.snp.bottom)
            make.right.equalTo(view)
            make.bottom.equalTo(horizonGrayLine.snp.top)
            make.width.equalTo(userInfoView!)
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closure(){
        
        //MARK: 点击设置按钮的事件
        settingButton?.touchButtonClosure = {[weak self] _ in
            self?.touchSetButtonClosure?()
        }
        
        //MARK: 点击用户指南的事件
        tutorialButton?.touchButtonClosure = {[weak self] _ in
            self?.touchTutorialButtonClosure?()
        }
        
        //MARK: 成功获取用户信息
        userInfoViewModel.getUserInfoSuccess = {[weak self] (info: UserInfoModel) in
            self?.userInfoModel = info
            delay(seconds: 5, completion: nil, completionBackground: {
                self?.messageViewModel.checkMessageStatus()//检查是否有未读消息
            })
        }
        
        //MARK: 获取用户信息失败
        userInfoViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击了tableView上的cell
        tableViewController?.selectTableView = {[weak self] (indexPath: IndexPath) in
            self?.selectTableView?(indexPath)
        }
        
        //MARK: 成功获得未读消息数量
        messageViewModel.checkMessageStatusClosure = {[weak self] (count: Int) in
            self?.getUnreadMessageNumber?(count)
            self?.tableViewController?.setUnreadMessageNumber(count: count)
        }
        
        //MARK: 点击头像，进入更换头像的界面
        userInfoView?.tapUserPortraitClosure = {[weak self] (image: UIImage?) in
            self?.tapUserPortraitView?(image)
        }
        
        //MARK: 点击好友, 关注或粉丝按钮
        userInfoView?.touchFriendButtonClosure = {[weak self] _ in
            self?.touchFriendsFollowFansButton?(UserOtherInfoType.friends)
        }
        userInfoView?.touchAttentionButtonClosure = {[weak self] _ in
            self?.touchFriendsFollowFansButton?(UserOtherInfoType.follows)
        }
        userInfoView?.touchFansButtonClosure = {[weak self] _ in
            self?.touchFriendsFollowFansButton?(UserOtherInfoType.fans)
        }
    }
    
    //MARK: 获取用户信息
    func getUserInfo(){
        userInfoViewModel.getUserInfo()
    }
    
    //MARK: 点击settingIcon的事件
    func tapSettingIcon(tap: UITapGestureRecognizer){
        touchSetButtonClosure?()
    }
}
