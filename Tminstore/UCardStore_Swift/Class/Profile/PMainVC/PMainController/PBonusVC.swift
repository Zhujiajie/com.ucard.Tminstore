//
//  PBonusVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/11.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: -------------积分视图---------------

import UIKit
import SnapKit

class PBonusVC: BaseViewController {
    ///是否是积分视图
    var isPoint: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        //获取软件版本号
        let label: UILabel = UILabel()

        label.text = NSLocalizedString("该功能暂未开放，敬请期待", comment: "")
        label.textColor = UIColor.darkGray
        label.textAlignment = NSTextAlignment.center
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 2
        
        view.addSubview(label)
        
        label.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.right.centerY.equalTo(view)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        if isPoint == true{
            setNavigationTitle(NSLocalizedString("积分", comment: ""))//设置导航栏标题
        }else{
            setNavigationTitle(NSLocalizedString("买图", comment: ""))//设置导航栏标题
        }
        setNavigationBarLeftButtonWithImageName("backArrow")//设置返回按钮
    }
}
