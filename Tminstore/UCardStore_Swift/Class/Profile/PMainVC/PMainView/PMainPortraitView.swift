//
//  PMainPortraitView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/14.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

/// 用户头像大小
let portraitViewSize: CGSize = CGSize(width: 75/baseWidth, height: 75/baseWidth)

class PMainPortraitView: UIImageView {

    /// 点击头像的事件
    var tapPortraitView: (()->())?
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.contentMode         = UIViewContentMode.scaleAspectFill
        self.layer.cornerRadius  = portraitViewSize.width/2
        self.layer.masksToBounds = true
        
        self.isUserInteractionEnabled = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPortraiView(_:)))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
        
        if let userPhotoURL: URL = getUserPortritURL() {
            self.sd_setImage(with: userPhotoURL)
        }else{
            self.image = #imageLiteral(resourceName: "defaultLogo")
        }
        
        //添加边框
        layer.borderColor = UIColor.white.cgColor
        layer.borderWidth = 2
    }

    //MARK: 点击头像的事件
    func tapPortraiView(_ tap: UITapGestureRecognizer){
        tapPortraitView?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
