//
//  PMainPurchaseTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/6.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: ----------------买图的Cell---------------

import UIKit
import SnapKit
import SDWebImage

class PMainPurchaseTableViewCell: UITableViewCell {

    /// 数据源
    var order: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 正面预览
    fileprivate var photoView: UIImageView?
    /// 昵称
    fileprivate var nickNameLabel: UILabel?
    /// 购买时间
    fileprivate var orderTimeLabel: UILabel?
    /// 地点icon
    fileprivate var locationIconImageView: UIImageView?
    /// 地点label
    fileprivate var locationLabel: UILabel?
    /// 剩余次数
    fileprivate var scanTimeLabel: UILabel?
    /// ar密码label
    fileprivate var arCodeLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        photoView = UIImageView()
        photoView?.contentMode = UIViewContentMode.scaleAspectFill
        photoView?.clipsToBounds = true
        contentView.addSubview(photoView!)
        photoView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(12/baseWidth)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.bottom.equalTo(contentView).offset(-12/baseWidth)
            make.size.equalTo(CGSize(width: 121/baseWidth, height: 86/baseWidth))
        })
        
        nickNameLabel = UILabel()
        nickNameLabel?.textColor = UIColor(hexString: "6B6B6B")
        nickNameLabel?.font = UIFont.systemFont(ofSize: 20)
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(10/baseWidth)
            make.left.equalTo(photoView!.snp.right).offset(4/baseWidth)
            make.right.equalTo(contentView).offset(-16/baseWidth)
            make.height.equalTo(22)
        })
        
        orderTimeLabel = UILabel()
        orderTimeLabel?.textColor = UIColor(hexString: "878686")
        orderTimeLabel?.font = UIFont.systemFont(ofSize: 12)
        contentView.addSubview(orderTimeLabel!)
        orderTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
            make.height.equalTo(14)
        })
        
        locationIconImageView = UIImageView(image: UIImage(named: "purchaseLocationIcon"))
        locationIconImageView?.contentMode = UIViewContentMode.scaleAspectFill
        contentView.addSubview(locationIconImageView!)
        locationIconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(orderTimeLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.size.equalTo(CGSize(width: 14/baseWidth, height: 14/baseWidth))
        })
        
        locationLabel = UILabel()
        locationLabel?.font = UIFont.systemFont(ofSize: 12)
        locationLabel?.textColor = UIColor(hexString: "878686")
        contentView.addSubview(locationLabel!)
        locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(locationIconImageView!)
            make.left.equalTo(locationIconImageView!.snp.right).offset(5/baseWidth)
            make.rightMargin.equalTo(nickNameLabel!)
            make.height.equalTo(14)
        })
        
        scanTimeLabel = UILabel()
        scanTimeLabel?.textColor = UIColor(hexString: "878686")
        scanTimeLabel?.font = UIFont.systemFont(ofSize: 12)
        contentView.addSubview(scanTimeLabel!)
        scanTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(contentView).offset(-11/baseWidth)
            make.leftMargin.equalTo(locationIconImageView!)
        })
        
        arCodeLabel = UILabel()
        arCodeLabel?.font = UIFont.systemFont(ofSize: 14)
        arCodeLabel?.textColor = UIColor(hexString: "4CDFED")
        contentView.addSubview(arCodeLabel!)
        arCodeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottomMargin.equalTo(scanTimeLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
        })
        arCodeLabel?.textAlignment = NSTextAlignment.right
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "C1C1C1")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.right.equalTo(contentView)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.height.equalTo(0.5)
        }
    }
    
    fileprivate func setData(){
        
        if let url: URL = order?.frontImageURL{
            //先判断是否已换成图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                
                if result == true{
                    self?.photoView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.order?.frontPlaceImageURL{
                        self?.photoView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.photoView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.photoView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.photoView?.sd_setImage(with: url)
                    }
                }
            })
        }
        
        nickNameLabel?.text = order?.addressModel.mailName
        
        if let createTime: Date = order?.createDate{
            let timeFormatter: DateFormatter = DateFormatter()
            timeFormatter.dateStyle = DateFormatter.Style.short
            timeFormatter.timeStyle = DateFormatter.Style.none
            let timeString: String = timeFormatter.string(from: createTime)
            orderTimeLabel?.text = NSLocalizedString("购买时间", comment: "") + " " + timeString
        }
        
        if languageCode() == "CN"{
            locationLabel?.text = order!.addressModel.country + order!.addressModel.province + order!.addressModel.district + order!.addressModel.city
        }else{
            locationLabel?.text = order!.addressModel.district + order!.addressModel.city + order!.addressModel.province + order!.addressModel.country
        }

        scanTimeLabel?.text = ""
        
        if let arCodeString: String = order?.arCode{
            arCodeLabel?.text = NSLocalizedString("密码:", comment: "") + " " + arCodeString
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        photoView?.image = nil
        nickNameLabel?.text = nil
        orderTimeLabel?.text = nil
        locationLabel?.text = nil
        scanTimeLabel?.text = nil
        arCodeLabel?.text = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
