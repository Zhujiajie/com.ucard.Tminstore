//
//  PMainOrderTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 展示订单信息的Cell
import UIKit
import SnapKit
import SDWebImage

class PMainOrderTableViewCell: UITableViewCell {
    
    var indexPath: IndexPath?
    
    /// 点击删除按钮额事件
    var touchDeleteButtonClosure: ((_ indexPath: IndexPath)->())?
    
    var order: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 正面图片预览
    fileprivate var photoView: UIImageView?
    /// 密码展示label
    fileprivate var passwordLabel: UILabel?
    /// 价格label
    fileprivate var priceLabel: UILabel?
    /// 数量的label
    fileprivate var orderNumberLabel: UILabel?
    /// 删除的按钮
    fileprivate var deleteButton: UIButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        selectionStyle = UITableViewCellSelectionStyle.none
        
        //图片
        photoView = UIImageView()
        photoView?.contentMode = UIViewContentMode.scaleToFill
        contentView.addSubview(photoView!)
        photoView?.snp.makeConstraints({ (make) in
            make.left.equalTo(contentView).offset(10/baseWidth)
            make.top.equalTo(contentView).offset(9/baseWidth)
            make.bottom.equalTo(contentView).offset(-9/baseWidth)
            make.size.equalTo(CGSize(width: 111/baseWidth, height: 86/baseWidth))
        })
        
        //密码
        passwordLabel = UILabel()
        passwordLabel?.font = UIFont.systemFont(ofSize: 14)
        passwordLabel?.textColor = UIColor(hexString: "6B6B6B")
        contentView.addSubview(passwordLabel!)
        passwordLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(photoView!)
            make.left.equalTo(photoView!.snp.right).offset(18/baseWidth)
            make.height.equalTo(16)
        })
        
        // 价格
        priceLabel = UILabel()
        priceLabel?.font = UIFont.systemFont(ofSize: 14)
        priceLabel?.textColor = UIColor(hexString: "878686")
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(passwordLabel!.snp.bottom).offset(6/baseWidth)
            make.leftMargin.equalTo(passwordLabel!)
            make.height.equalTo(16)
        })
        
        //数量
        orderNumberLabel = UILabel()
        orderNumberLabel?.font = UIFont.systemFont(ofSize: 14)
        orderNumberLabel?.textColor = UIColor(hexString: "878686")
        contentView.addSubview(orderNumberLabel!)
        orderNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(priceLabel!.snp.bottom).offset(6/baseWidth)
            make.leftMargin.equalTo(passwordLabel!)
            make.height.equalTo(16)
        })
        
        //删除的按钮
        deleteButton = UIButton()
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4CDFED"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("删除", comment: ""), attributes: attDic)
        deleteButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        deleteButton?.layer.borderColor = UIColor(hexString: "4CDFED").cgColor
        deleteButton?.layer.borderWidth = 1
        deleteButton?.layer.cornerRadius = 1
        deleteButton?.layer.masksToBounds = true
        deleteButton?.addTarget(self, action: #selector(touchDeleteButton(button:)), for: UIControlEvents.touchUpInside)
        contentView.addSubview(deleteButton!)
        deleteButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(contentView).offset(-10/baseWidth)
            make.bottom.equalTo(contentView).offset(-9/baseWidth)
            make.size.equalTo(CGSize(width: 99/baseWidth, height: 28/baseWidth))
        })
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "C1C1C1")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.right.bottom.equalTo(contentView)
            make.left.equalTo(contentView).offset(10/baseWidth)
            make.height.equalTo(0.5)
        }
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        if let url: URL = order?.frontImageURL{
            //先判断是否已换成图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self](result: Bool) in
                
                if result == true{
                    self?.photoView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.order?.frontPlaceImageURL{
                        self?.photoView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.photoView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.photoView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.photoView?.sd_setImage(with: url)
                    }
                }
                
            })
        }
        
        if let arCode: String = order?.arCode{
            passwordLabel?.text = NSLocalizedString("时光密码：", comment: "") + "\(arCode)"
        }else{
            passwordLabel?.text = " "
        }
        
        priceLabel?.text = "¥ \(order!.orderAmount)"
        orderNumberLabel?.text = "x 1"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoView?.image = nil
        passwordLabel?.text = nil
        priceLabel?.text = nil
        orderNumberLabel?.text = nil
    }
    
    //MARK: 点击删除按钮的事件
    func touchDeleteButton(button: UIButton){
        if let indexPath: IndexPath = indexPath{
            touchDeleteButtonClosure?(indexPath)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
