//
//  PMainMoreView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMainMoreView: UIView {

    /// 点击按钮的事件
    var touchButtonClosure: ((_ tag: PMainMoreViewButtonTag)->())?
    
//    //// 优惠券
//    var couponClosure: (()->())?
//    /// 积分
//    var pointClosure: (()->())?
//    /// 买图
//    var purchasedClosure: (()->())?
    
    fileprivate var containerView: UIImageView?
    fileprivate var couponButton: UIButton?
    fileprivate var pointButton: UIButton?
    fileprivate var purchasedButton: UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        
        self.alpha = 0
        
        //添加手势，退出界面
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapSelf(tap:)))
        self.addGestureRecognizer(tap)
        let swipe: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeSelf(swipe:)))
        swipe.direction = [
            UISwipeGestureRecognizerDirection.up,
            UISwipeGestureRecognizerDirection.left,
            UISwipeGestureRecognizerDirection.down,
            UISwipeGestureRecognizerDirection.right
        ]
        self.addGestureRecognizer(swipe)
        
        setUI()
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 1
        }, completion: nil)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        containerView = UIImageView(image: UIImage(named: "moreContainer"))
        containerView?.contentMode = UIViewContentMode.scaleToFill
        self.addSubview(containerView!)
        containerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(nav_statusHeight - 15)
            make.left.equalTo(self).offset(17)
            make.size.equalTo(CGSize(width: 106, height: 120))
        })
        containerView?.isUserInteractionEnabled = true
        
        purchasedButton = setButton(tag: PMainMoreViewButtonTag.purchase)
        containerView?.addSubview(purchasedButton!)
        purchasedButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(containerView!)
            make.left.equalTo(containerView!).offset(9)
            make.right.equalTo(containerView!).offset(-9)
            make.height.equalTo(35)
        })
//        purchasedButton?.addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
        
        
        pointButton = setButton(tag: PMainMoreViewButtonTag.point)
        containerView?.addSubview(pointButton!)
        pointButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(purchasedButton!.snp.top)
            make.centerX.equalTo(purchasedButton!)
            make.size.equalTo(purchasedButton!)
        })
//        pointButton?.addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
        
        couponButton = setButton(tag: PMainMoreViewButtonTag.coupon)
        containerView?.addSubview(couponButton!)
        couponButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(pointButton!.snp.top)
            make.centerX.equalTo(pointButton!)
            make.size.equalTo(pointButton!)
        })
//        couponButton?.addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 设置按钮
    fileprivate func setButton(tag: PMainMoreViewButtonTag)->UIButton{
        let button: UIButton = UIButton()
        button.tag = tag.rawValue
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 13)]
        
        var title: String
        switch tag {
        case PMainMoreViewButtonTag.coupon:
            title = NSLocalizedString("我的优惠券", comment: "")
        case PMainMoreViewButtonTag.point:
            title = NSLocalizedString("我的积分", comment: "")
        default:
            title = NSLocalizedString("我购买的图", comment: "")
        }
        let attStr: NSAttributedString = NSAttributedString(string: title, attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left//文字居左
        
        button.addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
        
        self.setArrow(button: button)//添加右箭头
        
        return button
    }
    
    //MARK: 设置箭头
    fileprivate func setArrow(button: UIButton){
        let rightArrowImageView: UIImageView = UIImageView(image: UIImage(named: "rightArrow"))
        rightArrowImageView.contentMode = UIViewContentMode.scaleAspectFit
        button.addSubview(rightArrowImageView)
        rightArrowImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(button)
            make.right.equalTo(button)
            make.size.equalTo(CGSize(width: 5, height: 10))
        }
    }
    
    func tapSelf(tap: UITapGestureRecognizer){
        dismissSelf()
    }
    func swipeSelf(swipe: UISwipeGestureRecognizer){
        dismissSelf()
    }
    
    //MARK: 点击按钮的事件
    func touchButton(button: UIButton){
        dismissSelf()
        self.touchButtonClosure?(PMainMoreViewButtonTag(rawValue: button.tag)!)
    }
    
    //MARK: 渐变消失
    fileprivate func dismissSelf(){
        UIView.animate(withDuration: 0.5, animations: { [weak self] _ in
            self?.alpha = 0
        }) { [weak self] (_) in
            self?.removeFromSuperview()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
