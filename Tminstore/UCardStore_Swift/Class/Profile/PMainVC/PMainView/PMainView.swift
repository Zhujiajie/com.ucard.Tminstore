//
//  PMainView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/13.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

/// 优惠券
let couponLabelTag: Int = 123
/// 积分
let pointLabelTag: Int = 321
/// 购买的
let purchasedLabelTag: Int = 1234

class PMainView: UIView {
    
    /// 用户头像
    var userPortrait: UIImage?{
        get{
            if userPortraitView != nil {
                return userPortraitView?.image
            }else{
                return nil
            }
        }
    }
    
    /// 用户头像路径
    var userPortraitURL: URL?{
        didSet{
            if userPortraitView != nil && userPortraitURL != nil{
                userPortraitView?.sd_setImage(with: userPortraitURL!, placeholderImage: UIImage(named: "defaultLogo"))
            }
        }
    }
    
    // 用户昵称
    var userNickName: String = ""{
        didSet{
            if userNickNameLabel != nil{
                userNickNameLabel?.text = userNickName
            }
        }
    }
    
    ///点击头像去更换头像
    var tapPortraitViewClosure: (()->())?
    
    /// 点击button的事件
    var touchButtonClosure: ((_ buttonType: ProfileButtonType)->())?
    
    /// 当寄件箱为空时，点击制作按钮的事件
    var touchMakrCardClosure: (()->())?
    
    /// 背景图片
    fileprivate var backgroundImageView: UIImageView?
    /// 背景图片上的高斯模糊
    fileprivate var visualView: UIVisualEffectView?
    /// 展示用户头像的视图
    fileprivate var userPortraitView: PMainPortraitView?
    /// 用户昵称Label
    fileprivate var userNickNameLabel: UILabel?
    
    /// 草稿按钮
    fileprivate var draftButton: PMainButton?
    /// 录屏按钮
    fileprivate var recordButton: PMainButton?
    /// 钱包按钮
    fileprivate var walletButton: PMainButton?
    
    /// 四个按钮的承载视图
    fileprivate var fourButtonContainerView: UIView?
    ///买图按钮
    fileprivate var purchaseButton: PMainButton?
    /// 社区按钮
    fileprivate var communityButton: PMainButton?
    /// 地图按钮
    fileprivate var mapButton: PMainButton?
    /// 订单按钮
    fileprivate var orderButton: PMainButton?
    
    /// 底部可移动的绿色线
    fileprivate var greenLine: UIView?
    
    //MARK: 设置背景视图的模糊程度
    func setBackgroundImageViewBlur(){
        if #available(iOS 10.0, *) {
            guard let visualView: UIVisualEffectView = self.visualView else { return }
            if visualView.effect == nil{
                visualView.effect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            }
            let animator = UIViewPropertyAnimator(duration: 5, curve: UIViewAnimationCurve.linear, animations: {
                visualView.effect = nil
            })
            animator.startAnimation()
            if #available(iOS 11.0, *){
            }else{
                animator.pauseAnimation()
                animator.fractionComplete = 0.9
            }
        }
    }
    
    //MARK: 展示用户个人信息的视图
    class func userInfoView()->PMainView{
        
        let userInfoView: PMainView = PMainView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 207/baseHeight))
        userInfoView.backgroundColor = UIColor(hexString: "#F5F5F5")
        
        // 背景图片
        userInfoView.backgroundImageView = UIImageView()
        userInfoView.backgroundImageView?.contentMode = UIViewContentMode.scaleAspectFill
        userInfoView.backgroundImageView?.sd_setImage(with: appAPIHelp.userInfoBackground())
        userInfoView.addSubview(userInfoView.backgroundImageView!)
        userInfoView.backgroundImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.top.right.equalTo(userInfoView)
            make.height.equalTo(197/baseWidth)
        })
        userInfoView.backgroundImageView?.isUserInteractionEnabled = true
        
        // 背景图片上的高斯模糊
        let blur: UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        userInfoView.visualView = UIVisualEffectView(effect: blur)
        userInfoView.backgroundImageView?.addSubview(userInfoView.visualView!)
        userInfoView.visualView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(userInfoView.backgroundImageView!)
        })
        
        // 昵称
        userInfoView.userNickNameLabel = UILabel()
        userInfoView.userNickNameLabel?.textColor = UIColor.white
        userInfoView.userNickNameLabel?.font = UIFont.systemFont(ofSize: 18)
        userInfoView.userNickNameLabel?.textAlignment = NSTextAlignment.center
        userInfoView.visualView?.contentView.addSubview(userInfoView.userNickNameLabel!)
        userInfoView.userNickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(userInfoView.visualView!.contentView).offset(40/baseWidth)
            make.centerX.equalTo(userInfoView.visualView!.contentView)
            make.height.equalTo(18)
        })
        
        //头像
        userInfoView.userPortraitView = PMainPortraitView(frame: CGRect.zero)
        userInfoView.userPortraitView?.tapPortraitView = {[weak userInfoView] _ in
            userInfoView?.tapPortraitViewClosure?()
        }
        userInfoView.visualView?.contentView.addSubview(userInfoView.userPortraitView!)
        userInfoView.userPortraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(userInfoView.userNickNameLabel!.snp.bottom).offset(15/baseWidth)
            make.centerX.equalTo(userInfoView)
            make.size.equalTo(portraitViewSize)
        })
        //点击头像的事件
        userInfoView.userPortraitView?.tapPortraitView = { [weak userInfoView] _ in
            userInfoView?.tapPortraitViewClosure?()
        }
        
        //设置按钮
        userInfoView.setThreeButton()
        userInfoView.setButtons()
        
        //底部绿色的线
        userInfoView.greenLine = UIView()
        userInfoView.greenLine?.backgroundColor = UIColor(hexString: "4CDFED")
        userInfoView.fourButtonContainerView?.addSubview(userInfoView.greenLine!)
        userInfoView.greenLine?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 2/baseWidth))
            make.bottom.equalTo(userInfoView.fourButtonContainerView!.snp.bottom)
            make.centerX.equalTo(userInfoView.purchaseButton!)
        })
        
        return userInfoView
    }
    
    //MARK: 设置三个按钮
    fileprivate func setThreeButton(){
        //录屏按钮
        recordButton = PMainButton.profileButtons(buttonType: ProfileButtonType.record)
        visualView?.contentView.addSubview(recordButton!)
        recordButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(userPortraitView!.snp.bottom).offset(16/baseWidth)
            make.size.equalTo(CGSize(width: 100/baseWidth, height: 18/baseWidth))
            make.centerX.equalTo(self)
        })
        
        let whiteLine1: UIView = UIView()
        whiteLine1.backgroundColor = UIColor.white
        visualView?.contentView.addSubview(whiteLine1)
        whiteLine1.snp.makeConstraints { (make: ConstraintMaker) in
            make.right.equalTo(recordButton!.snp.left)
            make.centerY.equalTo(recordButton!)
            make.size.equalTo(CGSize(width: 0.5, height: 18/baseWidth))
        }
        
        //草稿按钮
        draftButton = PMainButton.profileButtons(buttonType: ProfileButtonType.draft)
        visualView?.contentView.addSubview(draftButton!)
        draftButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(recordButton!)
            make.size.equalTo(recordButton!)
            make.right.equalTo(whiteLine1.snp.left)
        })
        
        let whiteLine2: UIView = UIView()
        whiteLine2.backgroundColor = UIColor.white
        visualView?.contentView.addSubview(whiteLine2)
        whiteLine2.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.equalTo(recordButton!.snp.right)
            make.centerY.equalTo(recordButton!)
            make.size.equalTo(whiteLine1)
        }
        
        //钱包按钮
        walletButton = PMainButton.profileButtons(buttonType: ProfileButtonType.wallet)
        visualView?.contentView.addSubview(walletButton!)
        walletButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(recordButton!)
            make.size.equalTo(recordButton!)
            make.left.equalTo(whiteLine2.snp.right)
        })
        
        draftButton?.touchButton = {[weak self] _ in
            self?.touchButtons(buttonType: ProfileButtonType.draft)
        }
        recordButton?.touchButton = {[weak self] _ in
            self?.touchButtons(buttonType: ProfileButtonType.record)
        }
        walletButton?.touchButton = {[weak self] _ in
            self?.touchButtons(buttonType: ProfileButtonType.wallet)
        }
    }
    
    //MARK: 设置按钮
    fileprivate func setButtons(){
        
        //承载4个按钮的视图
        fourButtonContainerView = UIView()
        fourButtonContainerView?.backgroundColor = UIColor.white
        addSubview(fourButtonContainerView!)
        fourButtonContainerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(backgroundImageView!.snp.bottom).offset(2)
            make.left.right.bottom.equalTo(self)
            make.height.equalTo(54/baseWidth)
        })
        
        //买图按钮
        purchaseButton = PMainButton.profileButtons(buttonType: ProfileButtonType.purchase)
        fourButtonContainerView?.addSubview(purchaseButton!)
        purchaseButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.top.bottom.equalTo(fourButtonContainerView!)
            make.width.equalTo(screenWidth/4)
        })
        purchaseButton?.buttonIsSelected = true
        
        //社区的按钮
        communityButton = PMainButton.profileButtons(buttonType: ProfileButtonType.community)
        fourButtonContainerView?.addSubview(communityButton!)
        communityButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.bottom.equalTo(fourButtonContainerView!)
            make.left.equalTo(purchaseButton!.snp.right)
            make.width.equalTo(purchaseButton!)
        })
        
        //地图按钮
        mapButton = PMainButton.profileButtons(buttonType: ProfileButtonType.map)
        fourButtonContainerView?.addSubview(mapButton!)
        mapButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.bottom.equalTo(fourButtonContainerView!)
            make.left.equalTo(communityButton!.snp.right)
            make.width.equalTo(purchaseButton!)
        })
        
        //订单的按钮
        orderButton = PMainButton.profileButtons(buttonType: ProfileButtonType.order)
        fourButtonContainerView?.addSubview(orderButton!)
        orderButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.bottom.equalTo(fourButtonContainerView!)
            make.left.equalTo(mapButton!.snp.right)
        })
        
        purchaseButton?.touchButton = {[weak self] _ in
            self?.touchButtons(buttonType: ProfileButtonType.purchase)
        }
        communityButton?.touchButton = {[weak self] _ in
            self?.touchButtons(buttonType: ProfileButtonType.community)
        }
        mapButton?.touchButton = {[weak self] _ in
            self?.touchButtons(buttonType: ProfileButtonType.map)
        }
        orderButton?.touchButton = {[weak self] _ in
            self?.touchButtons(buttonType: ProfileButtonType.order)
        }
    }
    
    //MARK: 发件箱为空时的视图
    /// 发件箱为空时的视图
    ///
    /// - Returns: PMainView
    class func emptySendView()->PMainView{
        
        let view: PMainView = PMainView()
        
        let label1: UILabel = UILabel()
        let attDic1: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "ABABAB")] as [String: Any]
        let str1: String = NSLocalizedString("你还没有属于自己的卡片喔", comment: "")
        label1.attributedText = NSAttributedString(string: str1, attributes: attDic1)
        view.addSubview(label1)
        label1.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
        })
        
        let button: UIButton = UIButton()
        let attDic2: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "68CFD6")] as [String: Any]
        let str2: String = NSLocalizedString("快去制作吧!", comment: "")
        let attStr2: NSAttributedString = NSAttributedString(string: str2, attributes: attDic2)
        button.setAttributedTitle(attStr2, for: UIControlState.normal)
        view.addSubview(button)
        button.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(label1.snp.bottom)
            make.centerX.equalTo(view)
        })
        button.addTarget(view, action: #selector(view.touchMakeCardButton(button:)), for: UIControlEvents.touchUpInside)
        
        return view
    }
    
    //MARK: 草稿箱为空时显示的视图
    /// 草稿箱为空时显示的视图
    ///
    /// - Returns: PMainView
    class func emptyDraftView()->PMainView{
        
        let view: PMainView = PMainView()
        
        let label: UILabel = UILabel()
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "ABABAB")] as [String: Any]
        let str: String = NSLocalizedString("你还没有草稿噢～", comment: "")
        label.attributedText = NSAttributedString(string: str, attributes: attDic)
        
        view.addSubview(label)
        label.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
        })
        
        return view
    }
    
    //MARK: 发布为空时，显示的视图
    /// 发布为空时，显示的视图
    ///
    /// - Returns: PMainView
    class func emptyReleaseView()->PMainView{
        let view: PMainView = PMainView()
        
        let label: UILabel = UILabel()
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "ABABAB")] as [String: Any]
        let str: String = NSLocalizedString("你的时空圈还是空的哦~", comment: "")
        label.attributedText = NSAttributedString(string: str, attributes: attDic)
        view.addSubview(label)
        label.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
        })
        
        return view
    }
    
    //MARK:收件箱空时，显示的视图
    /// 收件箱为空时，显示的视图
    ///
    /// - Returns: PMainView
    class func emptyReceiveView()->PMainView{
        let view: PMainView = PMainView()
        
        let label: UILabel = UILabel()
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "ABABAB")] as [String: Any]
        let str: String = NSLocalizedString("空空如也～", comment: "")
        label.attributedText = NSAttributedString(string: str, attributes: attDic)
        view.addSubview(label)
        label.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
        })
        
        return view
    }
    
    //MARK: 点击发件箱制作按钮的事件
    func touchMakeCardButton(button: UIButton){
        touchMakrCardClosure?()
    }
    
    //MARK: 点击按钮的事件
    fileprivate func touchButtons(buttonType: ProfileButtonType){
        touchButtonClosure?(buttonType)
        switch buttonType {
        case ProfileButtonType.purchase, ProfileButtonType.community, ProfileButtonType.map, ProfileButtonType.order:
            touchButtonAnimation(buttonType: buttonType)
        default:
            break
        }
    }
    
    //MARK: 点击按钮的事件
    func touchButtonAnimation(buttonType: ProfileButtonType){
        
        guard let line: UIView = greenLine else { return }
        
        // 取消按钮的选择状态
        purchaseButton?.buttonIsSelected = false
        communityButton?.buttonIsSelected = false
        mapButton?.buttonIsSelected = false
        orderButton?.buttonIsSelected = false
        
        var button: PMainButton
        switch buttonType {
        case ProfileButtonType.purchase:
            button = purchaseButton!
        case ProfileButtonType.community:
            button = communityButton!
        case ProfileButtonType.map:
            button = mapButton!
        default:
            button = orderButton!
        }
        button.buttonIsSelected = true//选择按钮
        
        line.snp.remakeConstraints({ (make: ConstraintMaker) in
            make.size.equalTo(CGSize(width: 70/baseWidth, height: 3/baseWidth))
            make.bottom.equalTo(self)
            make.centerX.equalTo(button)
        })
        autoLayoutAnimation(view: self, withDelay: 0.0)
    }
}
