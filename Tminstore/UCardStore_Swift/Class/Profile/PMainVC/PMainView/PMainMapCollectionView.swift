//
//  PMainMapCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 展示个人假面，时光圈的cell

import UIKit
import SnapKit
import SDWebImage
import MJRefresh

let PMainMapCollectionViewCellappReuseIdentifier: String = "PMainMapCollectionViewCellappReuseIdentifier"

class PMainMapCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.vertical
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: screenWidth/3, height: 125/baseWidth)
        super.init(frame: frame, collectionViewLayout: layout)
        
        backgroundColor = UIColor.white
        
        register(PMainMapCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.PMainMapCollectionViewCellappReuseIdentifier)
        
        //设置上拉刷新
        mj_footer = BaseRefreshFooter()
        //设置下拉刷新
        mj_header = BaseAnimationHeader()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

class PMainMapCollectionViewCell: UICollectionViewCell{
    
    var mapModel: SpaceTimeDataModel?{
        didSet{
            if let url: URL = mapModel?.frontImageURL{
                //先判断是否已换成图片
                SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                    
                    if result == true{
                        self?.photoView?.sd_setImage(with: url)
                    }else{
                        if let placeHolderURL: URL = self?.mapModel?.frontPlaceImageURL{
                            self?.photoView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                                DispatchQueue.main.async {
                                    if image != nil {
                                        self?.photoView?.sd_setImage(with: url, placeholderImage: image!)
                                    }else{
                                        self?.photoView?.sd_setImage(with: url)
                                    }
                                }
                            })
                        }else{
                            self?.photoView?.sd_setImage(with: url)
                        }
                    }
                })
            }
        }
    }
    
    fileprivate var photoView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        photoView = UIImageView()
        photoView?.contentMode = UIViewContentMode.scaleAspectFill
        photoView?.clipsToBounds = true
        contentView.addSubview(photoView!)
        photoView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
