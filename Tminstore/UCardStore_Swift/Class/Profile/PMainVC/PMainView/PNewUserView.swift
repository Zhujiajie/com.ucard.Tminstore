//
//  PNewUserView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PNewUserView: UIView {

    var userModel: UserInfoModel?{
        didSet{
            setData()
        }
    }
    
    /// 点击用户头像的事件
    var tapUserPortraitClosure: ((_ image: UIImage?)->())?
    /// 点击好友按钮的事件
    var touchFriendButtonClosure: (()->())?
    /// 点击粉丝按钮的事件
    var touchFansButtonClosure: (()->())?
    /// 点击关注按钮的事件
    var touchAttentionButtonClosure: (()->())?
    
    /// 用户头像
    fileprivate var userPortrait: BaseImageView?
    /// 用户昵称
    fileprivate var nickNameLabel: BaseLabel?
//    /// 好友按钮
//    fileprivate var friendButton: BaseButton?
//    /// 好友数量
//    fileprivate var friendNumberLabel: BaseLabel?
//    /// 好友title
//    fileprivate var friendTitleLabel: BaseLabel?
//    /// 粉丝按钮
//    fileprivate var fansButton: BaseButton?
//    /// 粉丝数量
//    fileprivate var fansNumberLabel: BaseLabel?
//    /// 粉丝title
//    fileprivate var fansTitleLabel: BaseLabel?
//    /// 关注按钮
//    fileprivate var attentionButton: BaseButton?
//    /// 关注数量的label
//    fileprivate var attentionNumberLabel: BaseLabel?
//    /// 关注title
//    fileprivate var attentionTitleLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        userPortrait = BaseImageView.userPortraitView(cornerRadius: 33/baseWidth)
        userPortrait?.image = UIImage(named: "defaultLogo")
        addSubview(userPortrait!)
        userPortrait?.snp.makeConstraints({ (make) in
            make.top.equalTo(self).offset(42/baseWidth)
            make.left.equalTo(self).offset(29/baseWidth)
            make.size.equalTo(CGSize(width: 63/baseWidth, height: 63/baseWidth))
        })
        userPortrait?.addTapGesture()
        //MARK: 点击头像的事件
        userPortrait?.tapImageView = {[weak self] _ in
            self?.tapUserPortraitClosure?(self?.userPortrait?.image)
        }
        
        nickNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "383838", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 2, andText: nil)
        addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(userPortrait!)
            make.left.equalTo(userPortrait!.snp.right).offset(14/baseWidth)
            make.right.equalTo(self).offset(-5/baseWidth)
        })
        
        // 灰色线条
        let grayLine: BaseView = BaseView.colorLine(colorString: "E8E8E8")
        addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.bottom.equalTo(self)
            make.left.equalTo(self).offset(16/baseWidth)
            make.right.equalTo(self).offset(-16/baseWidth)
            make.height.equalTo(0.5)
        }
        
//        //好友
//        friendButton = BaseButton()
//        addSubview(friendButton!)
//        friendButton?.snp.makeConstraints({ (make) in
//            make.left.bottom.equalTo(self)
//            make.width.equalTo(93.5/baseWidth)
//            make.height.equalTo(72/baseWidth)
//        })
//        //MARK: 点击好友按钮的事件
//        friendButton?.touchButtonClosure = { [weak self] _ in
//            self?.touchFriendButtonClosure?()
//        }
//        friendTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "9B9B9B", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("好友", comment: ""))
//        addSubview(friendTitleLabel!)
//        friendTitleLabel?.snp.makeConstraints({ (make) in
//            make.bottom.equalTo(self).offset(-15/baseWidth)
//            make.centerX.equalTo(friendButton!)
//        })
//        friendNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "6DF9FF", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: nil)
//        addSubview(friendNumberLabel!)
//        friendNumberLabel?.snp.makeConstraints({ (make) in
//            make.centerX.equalTo(friendButton!)
//            make.bottom.equalTo(friendTitleLabel!.snp.top).offset(-7/baseWidth)
//        })
//        
//        //粉丝
//        fansButton = BaseButton()
//        addSubview(fansButton!)
//        fansButton?.snp.makeConstraints({ (make) in
//            make.bottom.equalTo(self)
//            make.left.equalTo(friendButton!.snp.right)
//            make.size.equalTo(friendButton!)
//        })
//        //MARK: 点击粉丝按钮的事件
//        fansButton?.touchButtonClosure = { [weak self] _ in
//            self?.touchFansButtonClosure?()
//        }
//        fansTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "9B9B9B", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("粉丝", comment: ""))
//        addSubview(fansTitleLabel!)
//        fansTitleLabel?.snp.makeConstraints({ (make) in
//            make.bottomMargin.equalTo(friendTitleLabel!)
//            make.centerX.equalTo(fansButton!)
//        })
//        fansNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "6DF9FF", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: nil)
//        addSubview(fansNumberLabel!)
//        fansNumberLabel?.snp.makeConstraints({ (make) in
//            make.centerX.equalTo(fansButton!)
//            make.bottomMargin.equalTo(friendNumberLabel!)
//        })
//        
//        //关注
//        attentionButton = BaseButton()
//        addSubview(attentionButton!)
//        attentionButton?.snp.makeConstraints({ (make) in
//            make.bottom.equalTo(self)
//            make.right.equalTo(self)
//            make.left.equalTo(fansButton!.snp.right)
//            make.height.equalTo(friendButton!)
//        })
//        //MARK: 点击关注按钮的事件
//        attentionButton?.touchButtonClosure = { [weak self] _ in
//            self?.touchAttentionButtonClosure?()
//        }
//        attentionTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "9B9B9B", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("关注", comment: ""))
//        addSubview(attentionTitleLabel!)
//        attentionTitleLabel?.snp.makeConstraints({ (make) in
//            make.bottomMargin.equalTo(friendTitleLabel!)
//            make.centerX.equalTo(attentionButton!)
//        })
//        attentionNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "6DF9FF", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: nil)
//        addSubview(attentionNumberLabel!)
//        attentionNumberLabel?.snp.makeConstraints({ (make) in
//            make.centerX.equalTo(attentionButton!)
//            make.bottomMargin.equalTo(friendNumberLabel!)
//        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        userPortrait?.sd_setImage(with: userModel?.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
        nickNameLabel?.text = userModel?.nickName
//        friendNumberLabel?.text = "\(userModel!.friendNumber)"
//        fansNumberLabel?.text = "\(userModel!.fansNumber)"
//        attentionNumberLabel?.text = "\(userModel!.followNumber)"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
