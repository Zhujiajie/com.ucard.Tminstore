//
//  PMainCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/11.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMainCollectionViewCell: UICollectionViewCell {
    
    var tableView: UITableView?{
        didSet{
            if tableView != nil{
                setTableView()
            }
        }
    }
    
    var collectionView: UICollectionView?{
        didSet{
            if collectionView != nil{
                setCollectionView()
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = UIColor.clear
    }
    
    fileprivate func setTableView(){
        contentView.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    fileprivate func setCollectionView(){
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
