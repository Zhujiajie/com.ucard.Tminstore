//
//  PMainTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MJRefresh

/// 邮寄订单
let orderTableViewTag: Int = 123
/// 时空圈发布
let releaseTableViewTag: Int = 321
/// 签收
let receiveTableViewTag: Int = 456
/// 草稿
let draftTableViewTag: Int = 654

class PMainTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        self.backgroundColor = UIColor.clear
        
        self.register(PMainReleaseTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.PMainReleaseCellReuseIdentifier)
        self.register(PMainOrderTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.PMainOrderCellReuseIdentifier)
        self.register(PMainPurchaseTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.PMainPurchaseCellReuseIdentifier)
        
        self.separatorStyle = UITableViewCellSeparatorStyle.none
        self.estimatedRowHeight = 300/baseWidth
        
        rowHeight = UITableViewAutomaticDimension//用户cell高度变化动画
        
//        //设置footerView
//        let view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: tabbarHeight + 50/baseHeight))
//        view.backgroundColor = UIColor.clear
//        self.tableFooterView = view
    }
    
    //MARK: 设置tableViewView
    /// 设置tableViewView
    ///
    /// - Parameters:
    ///   - setDelegate: 代理
    ///   - setDataSource: 数据源
    /// - Returns: PMainTableView
    class func setTableView(setDelegate: UITableViewDelegate?, andDateSource setDataSource: UITableViewDataSource?, isRefreshAvailable available: Bool)->PMainTableView{
        let tableView: PMainTableView = PMainTableView()
        tableView.delegate = setDelegate
        tableView.dataSource = setDataSource
        
        if available == true{
            //设置上拉刷新
            tableView.mj_footer                          = BaseRefreshFooter()
            //设置下拉刷新
            tableView.mj_header = BaseAnimationHeader()
        }
        
        return tableView
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
