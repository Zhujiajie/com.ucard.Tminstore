//
//  PMainButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/14.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PMainButton: UIButton {

    /// 点击按钮的事件
    var touchButton: (()->())?

    /// 按钮是否被选中
    var buttonIsSelected: Bool = false{
        didSet{
            setButtonSelectedStatus(selected: buttonIsSelected)
        }
    }
    
    
    // MARK: 个人界面，切换数据的button
    /// 个人界面，切换数据的button
    ///
    /// - Parameter buttonType: button的类型
    /// - Returns: PMainButton
    class func profileButtons(buttonType: ProfileButtonType)->PMainButton{
        
        let button: PMainButton = PMainButton()
        
        var title: String = ""
        
        switch buttonType {
        case ProfileButtonType.draft:
            title = "草稿"
        case ProfileButtonType.record:
            title = "录屏"
        case ProfileButtonType.wallet:
            title = "钱包"
        case ProfileButtonType.purchase:
            title = "买图"
        case ProfileButtonType.community:
            title = "社区"
        case ProfileButtonType.map:
            title = "时光圈"
        default:
            title = "订单"
        }
        
        button.tag = buttonType.rawValue
        
        //标题的格式
        var attDic: [String: Any]
        
        switch buttonType {
        case ProfileButtonType.draft, ProfileButtonType.record, ProfileButtonType.wallet:
            attDic = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        default:
            attDic = [NSForegroundColorAttributeName: UIColor(hexString: "898989"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        }
        
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString(title, comment: ""), attributes: attDic)
        
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        
        button.addTarget(button, action: #selector(touchButton(_:)), for: UIControlEvents.touchUpInside)
        
        return button
    }
    
    //MARK: 设置按钮为选择状态
    fileprivate func setButtonSelectedStatus(selected: Bool){
        var color: UIColor
        if selected == true{
            color = UIColor(hexString: "00DFED")
        }else{
            color = UIColor(hexString: "898989")
        }
        let attStr: NSAttributedString? = self.attributedTitle(for: UIControlState.normal)
        if let title: String = attStr?.string {
            let attDic: [String: Any] = [NSForegroundColorAttributeName: color, NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
            let newAttStr: NSAttributedString = NSAttributedString(string: title, attributes: attDic)
            setAttributedTitle(newAttStr, for: UIControlState.normal)
        }
    }
    
    //MARK: 点击按钮的事件
    func touchButton(_ button: UIButton){
        touchButton?()
    }
}
