//
//  PMainCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/14.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class PMainCollectionView: UICollectionView {

    /// 承载各个tableView的collectionView
    class func profileCollectionView()->PMainCollectionView{
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: screenWidth, height: screenHeight - 260/baseWidth - tabbarHeight)
        
        let collectionView: PMainCollectionView = PMainCollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        collectionView.scrollsToTop = false
        
        return collectionView
    }

}
