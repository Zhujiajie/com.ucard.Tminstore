//
//  PMainReleaseTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 展示发布信息的cell
import UIKit
import SnapKit
import SDWebImage

class PMainReleaseTableViewCell: UITableViewCell {

    var postcard: PostcardModel?{
        didSet{
            setData()
        }
    }
    
//    /// 容器视图
//    fileprivate var containerView: UIView?
    /// icon
    fileprivate var releaseIconImageView: UIImageView?
    /// 日期
    fileprivate var dateLabel: UILabel?
    /// 展示正面图片的imageView
    fileprivate var photoView: UIImageView?
//    /// 卡片内容的label
//    fileprivate var contentLabel: UILabel?
    /// 地理位置ImageView
    fileprivate var locationImageView: UIImageView?
    /// 地理位置Label
    fileprivate var locationLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = UIColor.white
        contentView.backgroundColor = UIColor.white
        selectionStyle = UITableViewCellSelectionStyle.none
        
        //icon
        releaseIconImageView = UIImageView(image: UIImage(named: "releaseIcon"))
        releaseIconImageView?.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(releaseIconImageView!)
        releaseIconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(10/baseHeight)
            make.left.equalTo(contentView).offset(22/baseWidth)
            make.size.equalTo(CGSize(width: 26/baseWidth, height: 26/baseWidth))
        })
        //日期
        dateLabel = UILabel()
        dateLabel?.textColor = UIColor(hexString: "606060")
        dateLabel?.font = UIFont.systemFont(ofSize: 14)
        contentView.addSubview(dateLabel!)
        dateLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(releaseIconImageView!.snp.right).offset(5/baseWidth)
            make.centerY.equalTo(releaseIconImageView!)
            make.height.equalTo(17)
        })
        //图片
        photoView = UIImageView()
        photoView?.contentMode = UIViewContentMode.scaleToFill
        contentView.addSubview(photoView!)
        photoView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(releaseIconImageView!.snp.bottom).offset(13/baseHeight)
            make.left.equalTo(contentView).offset(25/baseWidth)
            make.right.equalTo(contentView).offset(-25/baseWidth)
            make.height.equalTo(219/baseWidth)
        })
        //定位
        locationImageView = UIImageView(image: UIImage(named: "publishLocation"))
        locationImageView?.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(locationImageView!)
        locationImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(photoView!.snp.bottom).offset(7/baseHeight)
            make.left.equalTo(contentView).offset(24/baseWidth)
            make.size.equalTo(CGSize(width: 13/baseWidth, height: 18/baseWidth))
        })
        
        locationLabel = UILabel()
        locationLabel?.font = UIFont.systemFont(ofSize: 12)
        locationLabel?.textColor = UIColor(hexString: "868686")
        locationLabel?.numberOfLines = 1
        contentView.addSubview(locationLabel!)
        locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(locationImageView!)
            make.left.equalTo(locationImageView!.snp.right).offset(9/baseWidth)
            make.right.equalTo(contentView).offset(-9/baseWidth)
            make.bottom.equalTo(contentView).offset(-8/baseHeight)
        })
        
//        // 文字
//        contentLabel = UILabel()
//        contentLabel?.font = UIFont.systemFont(ofSize: 13)
//        contentLabel?.textColor = UIColor(hexString: "828080")
//        contentLabel?.numberOfLines = 2
//        contentView.addSubview(contentLabel!)
//        contentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
//            make.top.equalTo(locationImageView!.snp.bottom).offset(4/baseHeight)
//            make.left.equalTo(contentView).offset(44/baseWidth)
//            make.right.equalTo(contentView).offset(-44/baseWidth)
//            make.bottom.equalTo(contentView).offset(-10/baseHeight)
//        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        // 日期
        if let date: Date = postcard?.createDate{
            let formater: DateFormatter = DateFormatter()
            formater.dateStyle = DateFormatter.Style.medium
            formater.timeStyle = DateFormatter.Style.short
            dateLabel?.text = formater.string(from: date)
        }
        
        if let url: URL = postcard?.frontImageURL{
            //先判断是否已换成图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                
                if result == true{
                    self?.photoView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.postcard?.frontPlaceImageURL{
                        self?.photoView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.photoView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.photoView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.photoView?.sd_setImage(with: url)
                    }
                }
            })
        }
        
        locationLabel?.text = postcard!.city
        
//        if languageCode() == "CN"{
//            locationLabel?.text = postcard!.country + postcard!.province + postcard!.city
//        }else{
//            locationLabel?.text = postcard!.city + postcard!.province + postcard!.country
//        }
        
//        contentLabel?.text = postcard?.remark
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoView?.image = nil
//        contentLabel?.text = nil
        locationLabel?.text = nil
        dateLabel?.text = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
