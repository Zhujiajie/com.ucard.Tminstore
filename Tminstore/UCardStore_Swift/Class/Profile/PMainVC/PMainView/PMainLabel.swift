//
//  PMainLabel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PMainLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        textAlignment = NSTextAlignment.center//文字居中
        numberOfLines = 1
    }
    
    //MARK: 展示数量的label
    /// 展示数量的label
    ///
    /// - Returns: PMainLabel
    class func numberLabel()->PMainLabel{
        let label: PMainLabel = PMainLabel()
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
        } else {
            font = UIFont.systemFont(ofSize: 14)
        }
        label.font = font
        label.textColor = UIColor(hexString: "4F4F4F")
        label.text = "0"
        return label
    }
    
    //MARK: 展示数据名字的label
    /// 展示数据名字的label
    ///
    /// - Returns: PMainLabel
    class func valueNameLabel()->PMainLabel{
        let label: PMainLabel = PMainLabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor(hexString: "969696")
        return label
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
