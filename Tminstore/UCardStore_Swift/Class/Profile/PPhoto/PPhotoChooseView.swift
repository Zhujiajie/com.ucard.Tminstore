//
//  PPhotoChooseView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/4.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PPhotoChooseView: UIView {

    /// 点击创意图片按钮的事件
    var touchIdealButtonClosure: (()->())?
    /// 点击上传图片按钮的事件
    var touchUploadButtonClosure: (()->())?
    
    /// 切换到创意图片的按钮
    fileprivate var idealButton: BaseButton?
    /// 切换到上传的按钮
    fileprivate var uploadButton: BaseButton?
    /// 指示用的绿色线条
    fileprivate var grayLine: UIView?
    /// 是否正在显示全部
    var isIdeal: Bool = true{
        didSet{
            if isIdeal == oldValue{return}
            switchBetweenIdealAndUpload(isIdeal: isIdeal)
        }
    }
    
    /// 选中的字体
    fileprivate let selectedAttDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "505050")]
    
    /// 未选中的字体
    fileprivate let deSelectdAttDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "B0B0B0")]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        idealButton = BaseButton.normalTitleButton(title: NSLocalizedString("创意图片", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "505050")
        addSubview(idealButton!)
        idealButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.bottom.left.equalTo(self)
            make.width.equalTo(screenWidth/2)
        })
        idealButton?.touchButtonClosure = {[weak self] _ in
            self?.isIdeal = true
        }
        
        uploadButton = BaseButton.normalTitleButton(title: NSLocalizedString("上传图片", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "B0B0B0")
        addSubview(uploadButton!)
        uploadButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.bottom.equalTo(self)
            make.left.equalTo(idealButton!.snp.right)
        })
        uploadButton?.touchButtonClosure = {[weak self] _ in
            self?.isIdeal = false
        }
        
        grayLine = UIView()
        grayLine?.backgroundColor = UIColor(hexString: "505050")
        addSubview(grayLine!)
        grayLine?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self)
            make.size.equalTo(CGSize(width: 129/baseWidth, height: 2))
            make.centerX.equalTo(idealButton!)
        })
    }
    
    //MARK: 在全部和个人之间切换
    fileprivate func switchBetweenIdealAndUpload(isIdeal: Bool){
        grayLine?.snp.remakeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self)
            make.size.equalTo(CGSize(width: 141/baseWidth, height: 2))
            if isIdeal{
                make.centerX.equalTo(idealButton!)
                idealButton?.setAttributedTitle(NSAttributedString.init(string: NSLocalizedString("创意图片", comment: ""), attributes: selectedAttDic), for: UIControlState.normal)
                uploadButton?.setAttributedTitle(NSAttributedString.init(string: NSLocalizedString("上传图片", comment: ""), attributes: deSelectdAttDic), for: UIControlState.normal)
                touchIdealButtonClosure?()
            }else{
                make.centerX.equalTo(uploadButton!)
                idealButton?.setAttributedTitle(NSAttributedString.init(string: NSLocalizedString("创意图片", comment: ""), attributes: deSelectdAttDic), for: UIControlState.normal)
                uploadButton?.setAttributedTitle(NSAttributedString.init(string: NSLocalizedString("上传图片", comment: ""), attributes: selectedAttDic), for: UIControlState.normal)
                touchUploadButtonClosure?()
            }
        })
        autoLayoutAnimation(view: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
