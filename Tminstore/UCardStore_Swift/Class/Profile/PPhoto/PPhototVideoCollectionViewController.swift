//
//  PPhototVideoCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/5.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

private let reuseIdentifier = "Cell"

class PPhototVideoCollectionViewController: UICollectionViewController {
    
    weak var idealModel: IdealPhotoModel?
    
    init() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: screenWidth, height: 320/baseWidth)
        
        super.init(collectionViewLayout: layout)
    }
        
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = UIColor.white
        // Register cell classes
        self.collectionView!.register(PPhototVideoCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        self.collectionView?.isPagingEnabled = true
    }

    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if idealModel == nil {return 0}
        return idealModel!.videoList.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PPhototVideoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PPhototVideoCollectionViewCell
        cell.videoPath = idealModel!.videoList[indexPath.row]
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)
    }
    
    //MARK: 滚动cell
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexPath: IndexPath = IndexPath(item: Int(scrollView.contentOffset.x / screenWidth), section: 0)
        if let cell: PPhototVideoCollectionViewCell = collectionView?.cellForItem(at: indexPath) as? PPhototVideoCollectionViewCell{
            cell.playVideo()
        }
    }
}

class PPhototVideoCollectionViewCell: UICollectionViewCell, PlayerPlaybackDelegate, PlayerDelegate{
    /// 视频路径
    var videoPath: URL?{
        didSet{
            if videoPath != nil && videoPath != oldValue{
                player?.stop()
                player?.setVideoFullURL(fullVideoURL: videoPath!)
                player?.playFromBeginning()
            }
        }
    }
    
    fileprivate var player: ZTPlayer?
    fileprivate var playAgainButton: UIButton?
    fileprivate var activeIndicator: UIActivityIndicatorView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = UIColor.white
        
        player = ZTPlayer()
        player?.view.frame = CGRect(x: 11/baseWidth, y: 11/baseWidth, width: 353/baseWidth, height: 288/baseWidth)
        player?.view.backgroundColor = UIColor.black//背景黑色
        player?.playbackLoops = true
        player?.playerDelegate = self
        player?.playbackDelegate = self
        contentView.addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            if self?.videoPath != nil{
                self?.playAgainButton?.isHidden = false
            }
        }
        
        //视频加载指示器
        activeIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        player?.view.addSubview(activeIndicator!)
        activeIndicator?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view)
        })
        activeIndicator?.hidesWhenStopped = true
        
        //重新开始播放的按钮
        playAgainButton = UIButton()
        playAgainButton?.setImage(UIImage(named: "play"), for: UIControlState.normal)
        playAgainButton?.contentMode = UIViewContentMode.scaleAspectFit
        playAgainButton?.addTarget(self, action: #selector(playFromCurrentTime(button:)), for: UIControlEvents.touchUpInside)
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 45/baseWidth, height: 45/baseWidth))
        })
        playAgainButton?.isHidden = true
        
        //注册通知，暂停视频播放
        NotificationCenter.default.addObserver(self, selector: #selector(stopVideoPlayNotification(info:)), name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)
    }
    
    //MARK: 播放视频
    /// 播放视频
    func playVideo(){
        player?.playFromCurrentTime()
        playAgainButton?.isHidden = true
    }
    
    //MARK: 暂停视频
    /// 暂停视频
    ///
    /// - Parameter hidePlayButton: 是否需要显示播放按钮
    func pausePlayer(hidePlayButton: Bool = false){
        playAgainButton?.isHidden = hidePlayButton
        player?.pause()
    }
    
    //MARK: 重新开始播放视频
    func playFromCurrentTime(button: UIButton){
        playAgainButton?.isHidden = true
        player?.playFromCurrentTime()
    }
    
    //MARK: 接受通知，暂停视频播放
    func stopVideoPlayNotification(info: Notification){
        if player?.playbackState == PlaybackState.playing{
            pausePlayer()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        videoPath = nil
        playAgainButton?.isHidden = true
        player?.stop()
    }
    
    func playerReady(_ player: Player) {
    }
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    //MARK: 根据缓存情况，展示加载提示器
    func playerBufferingStateDidChange(_ player: Player) {
        switch player.bufferingState {
        case BufferingState.ready:
            playVideo()
            activeIndicator?.stopAnimating()
        default:
            //            activeIndicator?.startAnimating()
            break
        }
    }
    
    func playerCurrentTimeDidChange(_ player: Player){
    }
    func playerPlaybackWillStartFromBeginning(_ player: Player){
    }
    func playerPlaybackDidEnd(_ player: Player){
    }
    func playerPlaybackWillLoop(_ player: Player){
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
