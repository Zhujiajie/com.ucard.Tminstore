//
//  PPhotoDetailController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/5.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage
class PPhotoDetailController: BaseViewController {
    
    fileprivate let viewModel: PPhotoViewModel = PPhotoViewModel()
    
    var idealModel: IdealPhotoModel?
    
    fileprivate var tableView: PPhotoTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexString: "#F6F6F6")
        setNavigationBar()
        setUI()
        closures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.checkUserPhotoDetail(idealModel: idealModel!)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("我的图库", comment: ""), attributeDic: attDic)
    }
    
    //MARK:设置UI
    fileprivate func setUI(){
        tableView = PPhotoTableViewController()
        view.addSubview(tableView!.view)
        tableView?.view.snp.makeConstraints({ (make) in
            make.top.equalTo(view).offset(6)
            make.left.bottom.right.equalTo(view)
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 获取数据成功
        viewModel.shouldReloadData = {[weak self] _ in
            self?.tableView?.idealModel = self?.idealModel
            self?.tableView?.tableView.reloadData()
        }
        
        //MARK: 获取数据失败
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击制作实物按钮的事件
        tableView?.touchMakeButtonClosure = {[weak self] in
            self?.enterGoosListVC(idealModel: self?.idealModel)
        }
    }
    
    //MARK: 进入实物列表
    fileprivate func enterGoosListVC(idealModel: IdealPhotoModel?){
        let vc: StoreGoodsController = StoreGoodsController()
        if let image: UIImage = SDImageCache.shared().imageFromCache(forKey: idealModel?.imgUrl?.absoluteString){
            idealModel?.originalImage = image
        }
        vc.umengViewName = "定制列表"
        vc.idealModel = idealModel
        _ = navigationController?.pushViewController(vc, animated: true)
    }
}
