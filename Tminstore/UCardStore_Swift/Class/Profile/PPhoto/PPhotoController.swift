//
//  PPhotoController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/4.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import CHTCollectionViewWaterfallLayout

class PPhotoController: BaseViewController {
    
    fileprivate let viewModel: PPhotoViewModel = PPhotoViewModel()
    
    /// 展示上传图片的collectionView
    fileprivate var uploadCollectionView: StoreIdealCollectionView?
    /// 展示图片下载进度的进度条
    fileprivate var progressView: UIProgressView?
    /// 在创意图片和上传图片之间进行选择的视图
    fileprivate var chooseView: PPhotoChooseView?
    /// 在创意图片和上传图片之间进行选择的collectionView
    fileprivate var chooseCollectionView: PPhotoChooseCollectionView?
    /// 展示创意图片的collectionViewController
    fileprivate var idealCollectionViewController: StoreMainIdealCollectionViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexString: "#F6F6F6")
        setNavigationBar()
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactive.pan?.isEnabled = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactive.pan?.isEnabled = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("我的图库", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        chooseView = PPhotoChooseView()
        view.addSubview(chooseView!)
        chooseView?.snp.makeConstraints({ (make) in
            make.left.top.right.equalTo(view)
            make.height.equalTo(45/baseWidth)
        })
        
        chooseCollectionView = PPhotoChooseCollectionView()
        view.addSubview(chooseCollectionView!)
        chooseCollectionView?.snp.makeConstraints({ (make) in
            make.left.bottom.right.equalTo(view)
            make.top.equalTo(chooseView!.snp.bottom)
        })
        chooseCollectionView?.delegate = self
        chooseCollectionView?.dataSource = self
        
        uploadCollectionView = StoreIdealCollectionView()
        
        idealCollectionViewController = StoreMainIdealCollectionViewController()
        idealCollectionViewController?.photoViewModel = viewModel
        
        progressView = UIProgressView()
        view.addSubview(progressView!)
        progressView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.top.right.equalTo(view)
        })
        progressView?.progressTintColor = UIColor(hexString: "59FFF4")
        progressView?.trackTintColor = UIColor.clear
    }
    
    //MARK: 回调
    fileprivate func closures(){
        //MARK: 需要刷新
        viewModel.shouldReloadData = {[weak self] _ in
            self?.uploadCollectionView?.mj_header.endRefreshing()
            self?.uploadCollectionView?.mj_footer.endRefreshing()
            self?.uploadCollectionView?.reloadData()
        }
        
        //MARK:没有更多数据了
        viewModel.noMoreData = {[weak self] _ in
            self?.uploadCollectionView?.mj_header.endRefreshing()
            self?.uploadCollectionView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.uploadCollectionView?.mj_header.endRefreshing()
            self?.uploadCollectionView?.mj_footer.endRefreshing()
            self?.idealCollectionViewController?.collectionView?.mj_header.endRefreshing()
            self?.idealCollectionViewController?.collectionView?.mj_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 下拉刷新
        uploadCollectionView?.mj_header.refreshingBlock = {[weak self] _ in
            self?.viewModel.pageNum = 1
            self?.viewModel.checkUploadImage()
        }
        
        //MARK: 上拉刷新
        uploadCollectionView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.checkUploadImage()
        }
        
        //MARK: 下载图片的进度
        viewModel.imageDownLoadProgress = {[weak self] (progress: Float) in
            self?.progressView?.isHidden = progress == 1
            self?.progressView?.setProgress(progress, animated: true)
        }
        
        //MARK: 点击切换到创意图片的按钮
        chooseView?.touchIdealButtonClosure = {[weak self] _ in
            self?.chooseCollectionView?.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
        }
        
        //MARK: 点击切换到上传图片的按钮
        chooseView?.touchUploadButtonClosure = {[weak self] _ in
            self?.chooseCollectionView?.scrollToItem(at: IndexPath.init(item: 1, section: 0), at: UICollectionViewScrollPosition.right, animated: true)
        }
        
        //MARK: 点击了Cell
        idealCollectionViewController?.selectCell = {[weak self] (indexPath: IndexPath) in
            self?.enterDetailVC(indexPath: indexPath, andIsIdeal: true)
        }
    }
    
    //MARK: 进入图片详情界面
    fileprivate func enterDetailVC(indexPath: IndexPath, andIsIdeal isIdeal: Bool){
        let vc: StoreIdealDetailController = StoreIdealDetailController()
        if isIdeal {
            vc.idealModel = viewModel.userIdealDataArray[indexPath.item]
        }else{
            vc.idealModel = viewModel.idealImageArray[indexPath.item]
            vc.idealModel?.portraitURL = currentUserLogoURL
            vc.idealModel?.nickName = userNickName
        }
        present(vc, animated: true, completion: nil)
        //MARK: 获得了创意图片原图
        vc.getOriginalImage = {[weak self] (idealModel: IdealPhotoModel?) in
            self?.enterGoosListVC(idealModel: idealModel)
        }
    }
    
    //MARK: 进入视图列表
    fileprivate func enterGoosListVC(idealModel: IdealPhotoModel?){
        let vc: StoreGoodsController = StoreGoodsController()
        vc.umengViewName = "定制列表"
        vc.idealModel = idealModel
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
}

extension PPhotoController: UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.isKind(of: PPhotoChooseCollectionView.self){
            return 2
        }
        return viewModel.idealImageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.isKind(of: PPhotoChooseCollectionView.self){
            let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(indexPath.item)", for: indexPath)
            
            if indexPath.item == 0{
                if cell.contentView.subviews.contains(idealCollectionViewController!.view) == false{
                    cell.contentView.addSubview(idealCollectionViewController!.view)
                    idealCollectionViewController?.view.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                    idealCollectionViewController?.collectionView?.mj_header.beginRefreshing()
                }
            }else{
                if cell.contentView.subviews.contains(uploadCollectionView!) == false {
                    cell.contentView.addSubview(uploadCollectionView!)
                    uploadCollectionView?.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                    uploadCollectionView?.delegate = self
                    uploadCollectionView?.dataSource = self
                    //获取数据
                    uploadCollectionView?.mj_header.beginRefreshing()
                }
            }
            
            return cell
        }
        let cell: StoreIdealCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.StoreIdealCollectionViewCellReuseIdentifier, for: indexPath) as! StoreIdealCollectionViewCell
        cell.idealModel = viewModel.idealImageArray[indexPath.item]
        return cell
    }
    
    //MARK: 实现瀑布流
    func collectionView(_ collectionView: UICollectionView!, layout collectionViewLayout: UICollectionViewLayout!, sizeForItemAt indexPath: IndexPath!) -> CGSize {
        if collectionView.isKind(of: PPhotoChooseCollectionView.self){
            let layout: UICollectionViewFlowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            return layout.itemSize
        }
        return viewModel.idealImageArray[indexPath.item].imageSize
    }
    
    //MARK: 点击cell，选择图片
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.isKind(of: PPhotoChooseCollectionView.self){
            return
        }
        enterDetailVC(indexPath: indexPath, andIsIdeal: false)
    }
    
    //MARK: 滚动cell
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.isKind(of: PPhotoChooseCollectionView.self){
            let isIdeal: Bool = Int(scrollView.contentOffset.x / screenWidth) == 0
            chooseView?.isIdeal = isIdeal
        }
    }
}

