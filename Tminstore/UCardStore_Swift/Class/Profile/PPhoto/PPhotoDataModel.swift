//
//  PPhotoDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/4.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PPhotoDataModel: BaseDataModel {
    
    //MARK: 获取用户的个人图片
    /// 获取用户的个人图片
    ///
    /// - Parameters:
    ///   - pageNum: 页数
    ///   - success: 成功的闭包
    func requestUserPhotos(pageNum: Int, successClosure success: ((_ result: [String: Any])-> ())?){
        let urlString: String = appAPIHelp.checkUserPhotoAPI + "\(pageNum)"
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看用户个人图片对应的详细信息
    /// 查看用户个人图片对应的详细信息
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    func checkUserPhotoDetailInfo(originalId: String, successClosure success: ((_ result: [String: Any])-> ())?){
        let urlString: String = appAPIHelp.checkUserPhotoDetailInfoAPI + originalId
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看我的创意图片的接口
    /// 查看我的创意图片的接口
    ///
    /// - Parameters:
    ///   - pageNum: 页数
    ///   - success: 成功的闭包
    func checkMyIdeal(pageNum: Int, successClosure success: ((_ result: [String: Any])-> ())?){
        
        let urlString: String = appAPIHelp.checkMyIdealAPI + "\(pageNum)"
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看我的上传图片的接口
    /// 查看我的上传图片的接口
    ///
    /// - Parameters:
    ///   - pageNum: 页数
    ///   - success: 成功的闭包
    func checkMyUpload(pageNum: Int, successClosure success: ((_ result: [String: Any])-> ())?){
        
        let urlString: String = appAPIHelp.checkMyUploadAPI + "\(pageNum)"
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
