//
//  PPhotoTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/5.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PPhotoTableViewController: UITableViewController {
    
    weak var idealModel: IdealPhotoModel?
    
    /// 点击制作实物按钮的事件
    var touchMakeButtonClosure: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(PPhotoTableViewVideoCell.self, forCellReuseIdentifier: "01")
        self.tableView.register(PPhotoTableViewImageCell.self, forCellReuseIdentifier: "02")
        self.tableView.register(PPhotoTableViewPurchaseCell.self, forCellReuseIdentifier: "03")
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.tableFooterView = UIView()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if idealModel == nil {return 0}
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell: PPhotoTableViewVideoCell = tableView.dequeueReusableCell(withIdentifier: "01", for: indexPath) as! PPhotoTableViewVideoCell
            
            cell.idealModel = idealModel
            return cell
        case 1:
            let cell: PPhotoTableViewImageCell = tableView.dequeueReusableCell(withIdentifier: "02", for: indexPath) as! PPhotoTableViewImageCell
            cell.idealModel = idealModel
            return cell
        default:
            let cell: PPhotoTableViewPurchaseCell = tableView.dequeueReusableCell(withIdentifier: "03", for: indexPath) as! PPhotoTableViewPurchaseCell
            cell.idealModel = idealModel
            //MARK: 点击制作实物按钮的事件
            cell.touchMakeButtonClosure = {[weak self] _ in
                self?.touchMakeButtonClosure?()
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 330/baseWidth
        case 1:
            return 125/baseWidth
        default:
            return 160/baseWidth
        }
    }
}

//MARK: 展示视频
class PPhotoTableViewVideoCell: UITableViewCell{
    
    var idealModel: IdealPhotoModel?{
        didSet{
            collectionView?.idealModel = idealModel
            collectionView?.collectionView?.reloadData()
        }
    }
    
    fileprivate var collectionView: PPhototVideoCollectionViewController?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        collectionView = PPhototVideoCollectionViewController()
        contentView.addSubview(collectionView!.view)
        collectionView?.view.snp.makeConstraints({ (make) in
            make.edges.equalTo(contentView)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示图片
class PPhotoTableViewImageCell: UITableViewCell{
    
    var idealModel: IdealPhotoModel?{
        didSet{
            collectionView?.idealModel = idealModel
            collectionView?.collectionView?.reloadData()
        }
    }
    
    fileprivate var collectionView: PPhotoImageCollectionViewController?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F6F6F6")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(2)
        }
        
        collectionView = PPhotoImageCollectionViewController()
        contentView.addSubview(collectionView!.view)
        collectionView?.view.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine.snp.bottom)
            make.left.bottom.right.equalTo(contentView)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示购买信息的cell
class PPhotoTableViewPurchaseCell: UITableViewCell{
    
    weak var idealModel: IdealPhotoModel?{
        didSet{
            if idealModel?.moneyPrice == 0.0{
                priceDetailLabel?.text = NSLocalizedString("免费", comment: "")
            }else{
                priceDetailLabel?.text = "¥ \(idealModel!.moneyPrice)"
            }
            if let date: Date = idealModel?.createDate{
                purchaseTimeDetailLabel?.text = dateFomatter.string(from: date)
            }
        }
    }
    
    //MARK: 点击制作实物按钮的事件
    var touchMakeButtonClosure: (()->())?
    
    let dateFomatter: DateFormatter = {
        let fomatter: DateFormatter = DateFormatter()
        fomatter.dateStyle = DateFormatter.Style.short
        fomatter.timeStyle = DateFormatter.Style.none
        return fomatter
    }()
    
    fileprivate var priceLabel: BaseLabel?
    fileprivate var priceDetailLabel: BaseLabel?
    fileprivate var purchaseTimeLabel: BaseLabel?
    fileprivate var purchaseTimeDetailLabel: BaseLabel?
    /// 点击去制作的按钮
    fileprivate var makeButton: BaseButton?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F6F6F6")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(2)
        }
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "525252", andText: NSLocalizedString("图片价格", comment: ""))
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine.snp.bottom).offset(19/baseWidth)
            make.left.equalTo(contentView).offset(24/baseWidth)
        })
        
        purchaseTimeLabel = .normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "525252", andText: NSLocalizedString("购买时间", comment: ""))
        contentView.addSubview(purchaseTimeLabel!)
        purchaseTimeLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(priceLabel!.snp.bottom).offset(13/baseWidth)
            make.leftMargin.equalTo(priceLabel!)
        })
        
        priceDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "E0B347", andTextAlignment: NSTextAlignment.right, andNumberOfLines: 1, andText: nil)
        contentView.addSubview(priceDetailLabel!)
        priceDetailLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(priceLabel!)
            make.right.equalTo(contentView).offset(-21/baseWidth)
        })
        
        purchaseTimeDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "C8C8C8", andTextAlignment: NSTextAlignment.right, andNumberOfLines: 0, andText: nil)
        contentView.addSubview(purchaseTimeDetailLabel!)
        purchaseTimeDetailLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(purchaseTimeLabel!)
            make.rightMargin.equalTo(priceDetailLabel!)
        })
        
        makeButton = BaseButton.normalTitleButton(title: NSLocalizedString("制作实物", comment: ""), andTextFont: UIFont.systemFont(ofSize: 16), andTextColorHexString: "FFFFFF", andBackgroundColorHexString: "505050", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: nil)
        contentView.addSubview(makeButton!)
        makeButton?.snp.makeConstraints({ (make) in
            make.centerX.equalTo(contentView)
            make.top.equalTo(purchaseTimeLabel!.snp.bottom).offset(27/baseWidth)
            make.size.equalTo(CGSize(width: 321/baseWidth, height: 51/baseWidth))
        })
        //MARK: 点击制作实物按钮的事件
        makeButton?.touchButtonClosure = {[weak self] _ in
            self?.touchMakeButtonClosure?()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
