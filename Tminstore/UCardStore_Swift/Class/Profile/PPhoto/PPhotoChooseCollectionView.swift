//
//  PPhotoChooseCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/4.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 在创意图片和上传图片之间选择的CollectionView

import UIKit

class PPhotoChooseCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: screenWidth, height: screenHeight - nav_statusHeight - 50/baseWidth)
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        
        self.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "0")
        self.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "1")
        
        self.backgroundColor = UIColor.white
        self.isPagingEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

