//
//  PPhototViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/4.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

class PPhotoViewModel: BaseViewModel {
    
    fileprivate let dataModel: PPhotoDataModel = PPhotoDataModel()
    
    /// 用户上传的图片数据
    var idealImageArray: [IdealPhotoModel] = [IdealPhotoModel]()
    
    /// 下载图片的进度
    var imageDownLoadProgress: ((_ progress: Float)->())?
    
    /// 向服务器端请求第几页数据
    var pageNum: Int = 1
    /// 没有更多数据了
    var noMoreData: (()->())?
    /// 可以加载数据了
    var shouldReloadData: (()->())?
    
//    var getResut: ((_ result: [String: Any])-> ())?
    
    /// 创意图片页数
    var idealPageNum: Int = 1
    /// 没有更多创意数据了
    var noMoreIdealData: (()->())?
    /// 成功获得创意数据
    var getIdealData: (()->())?
    /// 用户创意图片数据
    var userIdealDataArray: [IdealPhotoModel] = [IdealPhotoModel]()
    
    override init() {
        
        super.init()
        //MARK: 出错
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        //MARK: 网络请求完成
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 显示创意图片的
    func requestUserPhoto(){
        
        dataModel.requestUserPhotos(pageNum: pageNum, successClosure: { [weak self] (result: [String : Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if list.isEmpty{
                            self?.noMoreData?()
                        }else{
                            if self?.pageNum == 1{
                                self?.idealImageArray.removeAll()
                            }
                            for dic: [String: Any] in list{
                                let model: IdealPhotoModel = IdealPhotoModel.initFromDic(dic: dic)
                                if model.imgUrl != nil{
                                    self?.idealImageArray.append(model)
                                }
                            }
                            self?.shouldReloadData?()
                            //在后台下载图片
                            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                                self?.downLoadImage()
                            }
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        })
    }
    
    //MARK: 批量下载图片
    fileprivate func downLoadImage(){
        
        let imageURLs: [URL] = idealImageArray.map { (model: IdealPhotoModel) -> URL in
            return model.imgUrl!
        }
        
        let downLoadManager: SDWebImagePrefetcher = SDWebImagePrefetcher.shared()
        downLoadManager.prefetchURLs(imageURLs, progress: { [weak self] (count: UInt, totalCount: UInt) in
            DispatchQueue.main.async {
                self?.imageDownLoadProgress?(Float(count)/Float(totalCount))
            }
        }) { [weak self] (count: UInt, skipCount: UInt) in
            if skipCount != 0{
                DispatchQueue.main.async {
                    let message: [String: Any] = [
                        "message": "图片下载失败"
                    ]
                    self?.errorClosure?(nil, message)
                }
            }
            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                self?.getImageSize()
            }
        }
    }
    
    //MARK: 获取图片的大小
    fileprivate func getImageSize(){
        let imageCache: SDImageCache = SDImageCache.shared()
        _ = idealImageArray.map({ (model: IdealPhotoModel) in
            if let image: UIImage = imageCache.imageFromCache(forKey: model.imgUrl!.absoluteString){
                model.imageSize = image.size
                model.isOriginalImageDownloaded = true
            }
        })
        DispatchQueue.main.async {[weak self] _ in
            self?.shouldReloadData?()
        }
    }
    
    //MARK: 查看用户图片的详细信息
    /// 查看用户图片的详细信息
    ///
    /// - Parameter idealModel: IdealPhotoModel
    func checkUserPhotoDetail(idealModel: IdealPhotoModel){
        showSVProgress(title: nil)
        idealModel.videoList.removeAll()
        idealModel.storePictures.removeAll()
        idealModel.storePicturesPlaceHolder.removeAll()
        dataModel.checkUserPhotoDetailInfo(originalId: idealModel.originalId) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let results: [[String: Any]] = data["results"] as? [[String: Any]]{
                        if results.isEmpty == false{
                            idealModel.addVideoAndStoreImageData(results: results)
                            self?.shouldReloadData?()
                            return
                        }
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 查看创意图片
    /// 查看创意图片
    func checkIdeal(){
        dataModel.checkMyIdeal(pageNum: idealPageNum) { [weak self] (result: [String: Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if list.isEmpty{
                            self?.noMoreIdealData?()
                        }else{
                            if self?.idealPageNum == 1{
                                self?.userIdealDataArray.removeAll()
                            }
                            for dic: [String: Any] in list{
                                let model: IdealPhotoModel = IdealPhotoModel.initFromDic(dic: dic)
                                if model.imgUrl != nil{
                                    self?.userIdealDataArray.append(model)
                                }
                            }
                            self?.getIdealData?()
                        }
                        self?.idealPageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 查看上传图片
    /// 查看上传图片
    func checkUploadImage(){
        dataModel.checkMyUpload(pageNum: pageNum) { [weak self] (result: [String: Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if list.isEmpty{
                            self?.noMoreData?()
                        }else{
                            if self?.pageNum == 1{
                                self?.idealImageArray.removeAll()
                            }
                            for dic: [String: Any] in list{
                                let model: IdealPhotoModel = IdealPhotoModel.initFromDic(dic: dic)
                                if model.imgUrl != nil{
                                    self?.idealImageArray.append(model)
                                }
                            }
                            self?.shouldReloadData?()
                            //在后台下载图片
                            DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                                self?.downLoadImage()
                            }
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
}
