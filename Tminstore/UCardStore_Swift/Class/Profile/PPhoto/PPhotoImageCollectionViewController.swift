//
//  PPhotoImageCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/5.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier: String = "Cell"

class PPhotoImageCollectionViewController: UICollectionViewController {
    
    weak var idealModel: IdealPhotoModel?
    
    init() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: 103/baseWidth, height: 103/baseWidth)
        
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = UIColor.white
        // Register cell classes
        self.collectionView!.register(PPhotoImageCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.isPagingEnabled = true
        self.collectionView?.contentInset = UIEdgeInsets(top: 0, left: 16/baseWidth, bottom: 0, right: 16/baseWidth)
    }

    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if idealModel == nil {return 0}
        return idealModel!.storePictures.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PPhotoImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PPhotoImageCollectionViewCell
        cell.placeHolderURL = idealModel!.storePicturesPlaceHolder[indexPath.item]
        cell.imageURL = idealModel!.storePictures[indexPath.item]
        return cell
    }
}

class PPhotoImageCollectionViewCell: UICollectionViewCell{
    
    var placeHolderURL: URL?
    var imageURL: URL?{
        didSet{
            SDWebImageManager.shared().cachedImageExists(for: imageURL) { [weak self] (result: Bool) in
                if result == true{
                    self?.imageView?.sd_setImage(with: self?.imageURL)
                }else{
                    self?.imageView?.sd_setImage(with: self?.placeHolderURL, completed: { (image: UIImage?, _, _, _) in
                        if image != nil{
                            self?.imageView?.sd_setImage(with: self?.imageURL, placeholderImage: image)
                        }else{
                            self?.imageView?.sd_setImage(with: self?.imageURL)
                        }
                    })
                }
            }
        }
    }
    fileprivate var imageView: BaseImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: nil)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(contentView)
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
