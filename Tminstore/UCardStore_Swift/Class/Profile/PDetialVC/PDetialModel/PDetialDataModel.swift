//
//  PDetialDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/21.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class PDetialDataModel: BaseDataModel {

    
    //MARK: 取消订单，删除订单的请求
    /// 取消订单，删除订单的请求
    ///
    /// - Parameters:
    ///   - paymentOrderNo: 订单号
    ///   - success: 成功
    ///   - failsure: 失败
    ///   - completion: 完成
    func deleteOrder(paymentOrderNo: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let urlStr: String = appAPIHelp.deleteOrderAPI + paymentOrderNo
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ] as [String: Any]
        
        sendDELETERequestWithURLString(URLStr: urlStr, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 请求单个订单的信息
    /// 请求单个订单的信息
    ///
    /// - Parameters:
    ///   - mailOrderNo: 订单的编号
    ///   - success: 成功的闭包
    func requestOrderInfo(mailOrderNo: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.requestOrderInfoAPI + mailOrderNo
        
        let parameter: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameter, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
