//
//  PDetialViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/10.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class PDetialViewModel: BaseViewModel {
    
    fileprivate let dataModel: PDetialDataModel = PDetialDataModel()
    
    /// 评论数组
    var commentArray: [CommentModel] = [CommentModel]()
    
    /// 要获取第几页评论
    var pageIndex = 1
    
    /// 删除成功
    var deleteSuccess: ((_ indexPath: IndexPath?)->())?
    
    /// 出错
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 成功获取订单信息
    var requestOrderInfoSuccess: ((_ orderModel: OrderPostcardModel)->())?
    
    override init() {
        super.init()
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 网络请求完成的闭包
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 删除订单的请求
    /// 删除订单的请求
    ///
    /// - Parameter paymentOrderNo: 订单号
    func deleteOrder(paymentOrderNo: String, andIndexPath indexPath: IndexPath? = nil){
        
        showSVProgress(title: nil)
        
        dataModel.deleteOrder(paymentOrderNo: paymentOrderNo, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.deleteSuccess?(indexPath)
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 请求单个订单的信息
    /// 请求单个订单的信息
    ///
    /// - Parameter mailOrderNo: 订单的ID
    func requestOrderInfo(mailOrderNo: String) {
        showSVProgress(title: nil)
        
        dataModel.requestOrderInfo(mailOrderNo: mailOrderNo) { [weak self](result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let mailOrder: [String: Any] = data["mailOrder"] as? [String: Any]{
                        let order: OrderPostcardModel = OrderPostcardModel.initFromDic(dic: mailOrder)
                        self?.requestOrderInfoSuccess?(order)
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
}
