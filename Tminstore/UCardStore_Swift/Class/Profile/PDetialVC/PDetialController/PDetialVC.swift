//
//  PDetialVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/10.
//  Copyright © 2016年 ucard. All rights reserved.
//

//-------------------------已寄出明信片的详情界面---------------------

import UIKit
import SnapKit
import IQKeyboardManagerSwift

class PDetialVC: BaseViewController {
    
    fileprivate let viewModel: PDetialViewModel = PDetialViewModel()
    fileprivate let scanViewModel: QRViewModel = QRViewModel()
    
    /// 成功分享到时空圈
    var shareToCommunityOrDeleteSuccess: (()->())?
    
    /// 是否需要使用dismiss方法
    var shouldUseDismiss: Bool = false
    
    /// 订单的ID
    var mailOrderNo: String = ""
    
    /// 明信片的模型
    var order: OrderPostcardModel?
    var userInfo: UserInfoModel?
    /// tableHeaderView
    fileprivate var tableHeaderView: PDetialHeaderView?
    /// 评论tableView
    fileprivate var tableView: PDetailTableView?
    /// 放置支付和分享按钮的footerView
    fileprivate var footerView: PDetailTableFooterView?
    
    /// 积分提示框
    fileprivate var creditAlerVC: MUPublishAlertVC?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModelClosures()//viewModel的闭包回调
        setNavigationBar()//设置导航栏
        
        if order == nil {
            viewModel.requestOrderInfo(mailOrderNo: mailOrderNo)
        }else{
            setFooterView()
            setUI()
            closure()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cancelTansparentNavigationBar()
        MobClick.beginLogPageView("订单")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)//发送通知，暂停播放视频
        MobClick.endLogPageView("订单")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK:设置导航栏
    fileprivate func setNavigationBar() {
        setNavigationBarLeftButtonWithImageName("backArrow")//设置左按钮
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D")] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("订单详情", comment: ""), attributeDic: attDic)
    }
    
    //MARK:设置导航栏的左击事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        if shouldUseDismiss == true{
            dismissSelf()
        }else{
            _ = navigationController?.popViewController(animated: true)
        }   
    }
    
    //MARK: 设置下方，取消订单，投递时空圈和去支付按钮的视图
    fileprivate func setFooterView(){
        // 判断是否需要加载footerView
        let paid: Bool = order!.status != OrderStaus.unPaied
        let isShared: Bool = order!.isShared
        
        if paid == true && isShared == true{
            return
        }
        
        footerView = PDetailTableFooterView.orderTableFooterView(paied: paid, isShared: isShared)
        view.addSubview(footerView!)
        footerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(54/baseWidth)
        })
    }
    
    //MARK:设置UI
    fileprivate func setUI(){
        
        tableHeaderView = PDetialHeaderView()
        tableHeaderView?.isOrder = true
        tableHeaderView?.order = order
        sizeHeaderToFit(shouldAnimate: true)
        
        tableView = PDetailTableView()
        view.addSubview(tableView!)
        tableView!.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.top.right.equalTo(view)
            if footerView == nil {
                make.bottom.equalTo(view)
            }else{
                make.bottom.equalTo(footerView!.snp.top)
            }
        }
        tableView!.tableHeaderView = tableHeaderView
        
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    //MARK: 设置积分视图
    fileprivate func showCreditView(){
        
        creditAlerVC = MUPublishAlertVC()
        
        let payModel: PayModel = PayModel()
        payModel.originalId = order!.originalId
        creditAlerVC?.payModel = payModel
        
        addChildViewController(creditAlerVC!)
        view.addSubview(creditAlerVC!.view)
        creditAlerVC?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        //MARK: 分享成功
        creditAlerVC?.shareToCommunitySuccess = {[weak self] _ in
            // 弹窗提示用户发布成功
            self?.alert(alertTitle: NSLocalizedString("发布成功", comment: ""), leftClosure: {
                self?.shareToCommunityOrDeleteSuccess?()
                _ = self?.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    //MARK: viewModel的闭包回调
    fileprivate func viewModelClosures(){
        //MARK: 取消订单成功
        viewModel.deleteSuccess = {[weak self] (_) in
            self?.shareToCommunityOrDeleteSuccess?()
            _ = self?.navigationController?.popViewController(animated: true)
        }
        
        //MARK: 出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 如果刚进来是order是空，则向后端请求订单信息，请求成功的闭包
        viewModel.requestOrderInfoSuccess = {[weak self] (order: OrderPostcardModel) in
            self?.order = order
            self?.setFooterView()
            self?.setUI()
            self?.closure()
        }
    }
    
    //MARK: 闭包回调
    fileprivate func closure(){
        
        //MARK: 点击发布按钮
        footerView?.touchShareButtonClosure = {[weak self] _ in
            self?.releaseOrCancelRelease()
        }
        
        //MARK: 点击支付按钮
        footerView?.touchPayButtonClosure = {[weak self] _ in
            self?.purchase()
        }
        
        //MARK: 点击取消订单按钮
        footerView?.touchDeleteButtonClosure = {[weak self] _ in
            self?.deleteAlert(alertTitle: NSLocalizedString("确认删除？", comment: ""), rightClosure: {
                self?.viewModel.deleteOrder(paymentOrderNo: (self?.order?.paymentOrderNo)!)
            })
        }
        
        //MARK: 成功下载AR识别图的闭包
        scanViewModel.requestARImageSuccess = {[weak self] (imagePath: String, videoURLString: String) in
            //进入本地识别
            let arVC: LocalARCardViewController = LocalARCardViewController()
            arVC.arImagePath = imagePath
            arVC.videoURLString = videoURLString
            self?.present(arVC, animated: true, completion: nil)
            CFRunLoopWakeUp(CFRunLoopGetCurrent())//解决延迟的现象
        }
        
        //MARK: 出错的闭包
        scanViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 支付行为
    fileprivate func purchase(){
        if userInfo?.status == UserStatus.unacticed{
            alert(alertTitle: NSLocalizedString("请先激活邮箱", comment: ""))
        }else{
            enterPayVC()
        }
    }
    
    //MARK: 发布行为
    fileprivate func releaseOrCancelRelease(){
        if order?.isShared == false{
            showCreditView()
        }
//        else{
//            alert(alertTitle: NSLocalizedString("是否取消分享到时空圈", comment: ""), rightButtonTitle: NSLocalizedString("确定", comment: ""), rightClosure: {[weak self] _ in
//                self?.payViewModel.cancelRelease(originalId: (self?.order?.originalId)!)
//            })
//        }
    }
    
    //MARK: 进入支付界面
    fileprivate func enterPayVC(){
        let payVC: MUPayController = MUPayController()
        payVC.order = order
        payVC.addressModel = order!.addressModel
        
        let payModel: PayModel = PayModel()
        payModel.originalId = order!.originalId
//        payModel.priceNeedToPay = order!.orderAmount
        payVC.payModel = payModel
        
        _ = navigationController?.pushViewController(payVC, animated: true)
    }
    
    //MARK: 计算tableHeaderView的高度
    /**
     计算tableHeaderView的高度
     */
    func sizeHeaderToFit(shouldAnimate: Bool) {
        
        tableHeaderView?.setNeedsLayout()
        tableHeaderView?.layoutIfNeeded()
        
        let height: CGFloat? = tableHeaderView?.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        
        if shouldAnimate {
            UIView.animate(withDuration: 0.5, delay: 0.1, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                self.tableHeaderView?.frame.size.height = height!
                self.tableView?.tableHeaderView = self.tableHeaderView
            }, completion: nil)
        }else{
            tableHeaderView?.frame.size.height = height!
        }
    }
}

//MARK:---------UITableViewDelegate, UITableViewDataSource-----------
extension PDetialVC:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3
    }
    //MARK: 设置cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch indexPath.row {
        case 0:
            let cell: PDetailOrderAddressCell = tableView.dequeueReusableCell(withIdentifier: PDetailCellReuseIdentifier.address.rawValue, for: indexPath) as! PDetailOrderAddressCell
            cell.order = order
            return cell
        case 1:
            let cell: PDetailOrderARCodeCell = tableView.dequeueReusableCell(withIdentifier: PDetailCellReuseIdentifier.arCode.rawValue, for: indexPath) as! PDetailOrderARCodeCell
            cell.order = order
            return cell
        default:
            let cell: PDetailOrderNumberCell = tableView.dequeueReusableCell(withIdentifier: PDetailCellReuseIdentifier.number.rawValue, for: indexPath) as! PDetailOrderNumberCell
            cell.order = order
            return cell
        }
    }
}
