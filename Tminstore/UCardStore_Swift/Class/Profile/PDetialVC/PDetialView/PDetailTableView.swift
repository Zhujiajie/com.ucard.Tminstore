//
//  PDetailTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: --------------用户展示评论的tableView---------------
import UIKit

class PDetailTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        register(PDetailOrderAddressCell.self, forCellReuseIdentifier: PDetailCellReuseIdentifier.address.rawValue)
        register(PDetailOrderARCodeCell.self, forCellReuseIdentifier: PDetailCellReuseIdentifier.arCode.rawValue)
        register(PDetailOrderNumberCell.self, forCellReuseIdentifier: PDetailCellReuseIdentifier.number.rawValue)
        
        estimatedRowHeight = 40
        separatorStyle = UITableViewCellSeparatorStyle.none
        
        contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 54/baseWidth, right: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
