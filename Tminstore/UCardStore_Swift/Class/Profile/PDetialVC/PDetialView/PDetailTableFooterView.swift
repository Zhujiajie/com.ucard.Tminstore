//
//  PDetailTableFooterView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PDetailTableFooterView: UIView {

    /// 按钮的大小
    fileprivate let buttonSize: CGSize = CGSize(width: 100/baseWidth, height: 34/baseWidth)
    
    /// 点击分享按钮的事件
    var touchShareButtonClosure: (()->())?
    /// 点击支付按钮的事件
    var touchPayButtonClosure: (()->())?
    /// 点击删除按钮的事件
    var touchDeleteButtonClosure: (()->())?
    /// 支付按钮
    fileprivate var payButton: UIButton?
    /// 分享按钮
    fileprivate var shareButton: UIButton?
    /// 取消订单的按钮
    fileprivate var deleteButton: UIButton?
    
    /// 放置支付和分享按钮的footerView
    ///
    /// - Parameters:
    ///   - paied: 是否已支付
    ///   - shared: 是否已分享
    /// - Returns: PDetailTableFooterView
    class func orderTableFooterView(paied: Bool, isShared shared: Bool)->PDetailTableFooterView{
        
        let footerView: PDetailTableFooterView = PDetailTableFooterView()
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "4CDFED")]
        
        //投递时空圈的按钮
        footerView.shareButton = UIButton()
        footerView.shareButton?.backgroundColor = UIColor.white
        
        var shareTitle: String
        if shared == false{
            shareTitle = NSLocalizedString("投递时空圈", comment: "")
        }else{
            shareTitle = ""
        }
        footerView.shareButton?.setAttributedTitle(NSAttributedString(string: shareTitle, attributes: attDic), for: UIControlState.normal)
        footerView.shareButton?.layer.cornerRadius = 1
        footerView.shareButton?.layer.masksToBounds = true
        footerView.shareButton?.layer.borderColor = UIColor(hexString: "4CDFED").cgColor
        footerView.shareButton?.layer.borderWidth = 1
        footerView.shareButton?.addTarget(footerView, action: #selector(footerView.touchShareButton(button:)), for: UIControlEvents.touchUpInside)
        
        // 删除的按钮
        footerView.deleteButton = UIButton()
        footerView.deleteButton?.backgroundColor = UIColor.white
        let deleteTitle: String = NSLocalizedString("取消订单", comment: "")
        footerView.deleteButton?.setAttributedTitle(NSAttributedString(string: deleteTitle, attributes: attDic), for: UIControlState.normal)
        footerView.deleteButton?.layer.cornerRadius = 1
        footerView.deleteButton?.layer.masksToBounds = true
        footerView.deleteButton?.layer.borderColor = UIColor(hexString: "4CDFED").cgColor
        footerView.deleteButton?.layer.borderWidth = 1
        footerView.deleteButton?.addTarget(footerView, action: #selector(footerView.touchDeleteButton(button:)), for: UIControlEvents.touchUpInside)
        
        //去支付的按钮
        footerView.payButton = UIButton()
        footerView.payButton?.backgroundColor = UIColor.white
        footerView.payButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("去支付", comment: ""), attributes: attDic), for: UIControlState.normal)
        footerView.payButton?.layer.cornerRadius = 1
        footerView.payButton?.layer.masksToBounds = true
        footerView.payButton?.layer.borderColor = UIColor(hexString: "4CDFED").cgColor
        footerView.payButton?.layer.borderWidth = 1
        footerView.payButton?.addTarget(footerView, action: #selector(footerView.touchPayButton(button:)), for: UIControlEvents.touchUpInside)
        
        //未支付，未分享
        if paied == false && shared == false{
            
            footerView.addSubview(footerView.payButton!)
            footerView.addSubview(footerView.shareButton!)
            footerView.addSubview(footerView.deleteButton!)
            
            footerView.payButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(footerView).offset(-20/baseWidth)
                make.centerY.equalTo(footerView)
                make.size.equalTo(footerView.buttonSize)
            })
            footerView.shareButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(footerView.payButton!.snp.left).offset(-22.5/baseWidth)
                make.centerY.equalTo(footerView)
                make.size.equalTo(footerView.payButton!)
            })
            footerView.deleteButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(footerView.shareButton!.snp.left).offset(-22.5/baseWidth)
                make.centerY.equalTo(footerView)
                make.size.equalTo(footerView.payButton!)
            })
        }
        // 已支付，未分享
        else if paied == true && shared == false{
            footerView.addSubview(footerView.shareButton!)
            footerView.shareButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(footerView).offset(-20/baseWidth)
                make.centerY.equalTo(footerView)
                make.size.equalTo(footerView.buttonSize)
            })
        }
        //未支付，已分享
        else if paied == false && shared == true{
            footerView.addSubview(footerView.payButton!)
            footerView.addSubview(footerView.deleteButton!)
            footerView.payButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(footerView).offset(-20/baseWidth)
                make.centerY.equalTo(footerView)
                make.size.equalTo(footerView.buttonSize)
            })
            footerView.deleteButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(footerView.payButton!.snp.left).offset(-22.5/baseWidth)
                make.centerY.equalTo(footerView)
                make.size.equalTo(footerView.payButton!)
            })
        }
        
        return footerView
    }
    
    func touchPayButton(button: UIButton){
        touchPayButtonClosure?()
    }
    
    func touchShareButton(button: UIButton){
        touchShareButtonClosure?()
    }
    
    func touchDeleteButton(button: UIButton){
        touchDeleteButtonClosure?()
    }
}
