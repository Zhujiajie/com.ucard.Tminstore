//
//  PDetialHeaderView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/21.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:----------------订单详情界面的tableHeaderView-----------------------
import UIKit
import SnapKit

class PDetialHeaderView: UIView {
    
    /// 是否需要展示背面
    var shouldShowBackImage: Bool = true

    /// 点击点赞按钮的事件
    var touchLikeButtonClosure: (()->())?
    
    /// 点击评论按钮的事件
    var touchCommentButtonClosure: (()->())?
    
    ///是否是订单界面
    var isOrder: Bool = false{
        didSet{
            setHeaderView()
        }
    }
    
    /// 明信片信息
    var postcard: PostcardModel?{
        didSet{
            setData()
        }
    }

    /// 订单信息
    var order: OrderPostcardModel?{
        didSet{
            if order != nil {
                setOrderData()
            }
        }
    }
    
    /// 点赞数
    var likeCount: Int = 0{
        didSet{
            setLikeButtonStatus(isLiked: postcard!.isLiked, shouldChangeLikeCount: true)//改变点赞按钮的状态
             likeCountLabel?.text = "\(postcard!.likeCount)" + NSLocalizedString("次赞", comment: "")
        }
    }
    
    /// 总的评论数
    var commentNumber: Int = 0{
        didSet{
            commentLabel?.text = "\(postcard!.commentTotal)" + NSLocalizedString("评论", comment: "")
        }
    }
    
    /// 展示页数的指示器
    fileprivate var pageControl: UIPageControl?
    /// 承载图片和视频的collectionView
    fileprivate var collectionView: UICollectionView?
    /// 展示评论数的label
    fileprivate var commentLabel: UILabel?
    /// 展示点赞数的label
    fileprivate var likeCountLabel: UILabel?
    /// 展示时光密码的label
    fileprivate var arCodeLabel: UILabel?
    
    /// 买图的按钮
    fileprivate var buyPhotoButton: UIButton?
    /// 点赞的按钮
    fileprivate var likeButton: UIButton?
    /// 评论的按钮
    fileprivate var commentButton: UIButton?
    /// 是否要不响应点击点赞按钮的事件
    fileprivate var ignoreLikeAction: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //MARK: 设置界面
    func setHeaderView(){
        
        collectionView = MainCollectionView()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        addSubview(collectionView!)
        collectionView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(self)
            make.left.right.equalTo(self)
            make.height.equalTo(266/baseWidth)
        }
        
        // 页数指示器
        pageControl = UIPageControl()
        if shouldShowBackImage == true{
            pageControl?.numberOfPages = 3
        }else{
            pageControl?.numberOfPages = 2
        }
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "60DBE8")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D6D6D6")
        addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom)
            make.centerX.equalTo(self)
            if isOrder == true{
                make.bottom.equalTo(self)
            }
        })
        
        if isOrder == false{
            
//            //买图的按钮
//            buyPhotoButton = UIButton()
//            buyPhotoButton!.setImage(UIImage(named: "buyPhoto"), for: UIControlState.normal)
//            addSubview(buyPhotoButton!)
//            buyPhotoButton!.snp.makeConstraints({ (make: ConstraintMaker) in
//                make.top.equalTo(collectionView!.snp.bottom).offset(11/baseWidth)
//                make.left.equalTo(self).offset(12/baseWidth)
//                make.size.equalTo(CGSize(width: 22/baseWidth, height: 22/baseWidth))
//            })
            
            //评论的按钮
            commentButton = UIButton()
            commentButton?.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            commentButton?.imageView?.clipsToBounds = false
            commentButton?.setImage(UIImage(named: "comment"), for: UIControlState.normal)
            addSubview(commentButton!)
            commentButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(collectionView!.snp.bottom).offset(26/baseWidth)
                make.left.equalTo(self).offset(16/baseWidth)
                make.size.equalTo(CGSize(width: 22/baseWidth, height: 22/baseWidth))
            })
            commentButton?.addTarget(self, action: #selector(touchCommentButton(button:)), for: UIControlEvents.touchUpInside)
            
            //评论数的label
            commentLabel = UILabel()
            commentLabel?.textColor = UIColor(hexString: "838383")
            commentLabel?.font = UIFont.systemFont(ofSize: 8)
            addSubview(commentLabel!)
            commentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(commentButton!)
                make.left.equalTo(commentButton!.snp.right).offset(5/baseWidth)
            })
            
            //点赞按钮
            likeButton = UIButton()
            likeButton?.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            likeButton?.imageView?.clipsToBounds = false
            setLikeButtonStatus(isLiked: false)
            addSubview(likeButton!)
            likeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(commentButton!)
                make.left.equalTo(commentLabel!.snp.right).offset(19/baseWidth)
                make.size.equalTo(commentButton!)
            })
            likeButton?.addTarget(self, action: #selector(touchLikeButton(button:)), for: UIControlEvents.touchUpInside)
            
            // 点赞数的label
            likeCountLabel = UILabel()
            likeCountLabel?.textColor = UIColor(hexString: "838383")
            likeCountLabel?.font = UIFont.systemFont(ofSize: 8)
            addSubview(likeCountLabel!)
            likeCountLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(likeButton!)
                make.left.equalTo(likeButton!.snp.right).offset(5/baseWidth)
            })
            
            //展示AR密码的label
            arCodeLabel = UILabel()
            arCodeLabel?.textColor = UIColor(hexString: "00DFED")
            arCodeLabel?.font = UIFont.systemFont(ofSize: 14)
            arCodeLabel?.textAlignment = NSTextAlignment.right
            addSubview(arCodeLabel!)
            arCodeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(commentButton!)
                make.right.equalTo(self).offset(-16/baseWidth)
            })
            
            //按钮下方的灰色分割线
            let grayLine = UIView()
            grayLine.backgroundColor = UIColor(hexString: "#EFEFEF")
            self.addSubview(grayLine)
            grayLine.snp.makeConstraints { (make: ConstraintMaker) in
                make.top.equalTo(likeCountLabel!.snp.bottom).offset(13/baseHeight)
                make.left.equalTo(self).offset(10/baseWidth)
                make.right.equalTo(self).offset(-10/baseWidth)
                make.height.equalTo(1)
                make.bottom.equalTo(self)
            }
        }
    }
    
    //MARK:设置数据
    fileprivate func setData(){
        
        collectionView?.reloadData()
        
        //设置点赞按钮的样式
        if let isLiked: Bool = postcard?.isLiked{
            setLikeButtonStatus(isLiked: isLiked, shouldChangeLikeCount: false)
        }
        
        commentLabel?.text = "\(postcard!.commentTotal)" + NSLocalizedString("评论", comment: "")
        
        likeCountLabel?.text = "\(postcard!.likeCount)" + NSLocalizedString("次赞", comment: "")
        
        if postcard?.arCode != nil{
            arCodeLabel?.text = NSLocalizedString("密码：", comment: "") + "\(postcard!.arCode!)"
        }else{
            arCodeLabel?.isHidden = true
        }
    }
    
    //MARK: 设置订单信息
    fileprivate func setOrderData(){
        collectionView?.reloadData()
    }
    
    //MARK: 点击评论按钮的事件
    func touchCommentButton(button: UIButton){
        touchCommentButtonClosure?()
    }
    
    //MARK: 点击点赞按钮的事件
    func touchLikeButton(button: UIButton){
        if ignoreLikeAction == false{
            ignoreLikeAction = true
            touchLikeButtonClosure?()
            //一秒钟之后，才能重新点击该按钮
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(enableLikeButton(timer:)), userInfo: nil, repeats: false)
        }
    }
    
    //MARK: 重新启用点赞
    func enableLikeButton(timer: Timer){
        ignoreLikeAction = false
    }
    
    //MARK: 设置点赞按钮的状态
    /// 设置点赞按钮的状态
    ///
    /// - Parameters:
    ///   - isLiked: 点赞或取消点赞
    ///   - should: 是否改变点赞数，默认不改变
    func setLikeButtonStatus(isLiked: Bool, shouldChangeLikeCount should: Bool = false){
        if isLiked == true{
            likeButton?.setImage(UIImage(named: "liked"), for: UIControlState.normal)
            if should == true{
                
                likeCountLabel?.text = "\(postcard!.orderTotal)" + NSLocalizedString("次购买", comment: "") + "     \(postcard!.likeCount)" + NSLocalizedString("次赞", comment: "") + "     \(postcard!.commentTotal)" + NSLocalizedString("条评论", comment: "")
                
                //向下位移，并缩小
                var transform1: CGAffineTransform = likeButton!.transform
                transform1 = transform1.scaledBy(x: 0.5, y: 0.5)
                transform1 = transform1.translatedBy(x: 0, y: 10)
                
                //向上位移，并放大
                var transform2: CGAffineTransform = likeButton!.transform
                transform2 = transform2.scaledBy(x: 1.5, y: 1.5)
                
                UIView.animateKeyframes(withDuration: 0.6, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModePaced, animations: { [weak self] _ in
                    
                    //向下移，并缩小
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.3, animations: {
                        self?.likeButton?.transform = transform1
                    })
                    //上移并放大
                    UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.3, animations: {
                        self?.likeButton?.transform = transform2
                    })
                    //回复原始
                    UIView.addKeyframe(withRelativeStartTime: 0.7, relativeDuration: 0.4, animations: {
                        self?.likeButton?.transform = CGAffineTransform.identity
                    })
                    
                    }, completion: nil)
            }
        }else{
            likeButton?.setImage(UIImage(named: "likeButton"), for: UIControlState.normal)
            if should == true{
                likeCountLabel?.text = "\(postcard!.orderTotal)" + NSLocalizedString("次购买", comment: "") + "     \(postcard!.likeCount)" + NSLocalizedString("次赞", comment: "")  + "     \(postcard!.commentTotal)" + NSLocalizedString("条评论", comment: "")
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension PDetialHeaderView: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if shouldShowBackImage == true{
            return 3
        }else{
            return 2
        }
    }
    //MARK: 设置cell的内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isOrder == false{
            switch indexPath.row {
            case 0:
                let cell: MainImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MainImageCollectionViewCellReuseIdentifier, for: indexPath) as! MainImageCollectionViewCell
                cell.placeholerImageURL = postcard?.frontPlaceImageURL
                cell.imageURL = postcard?.frontImageURL
                return cell
            case 1:
                let cell: MainCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MainCollectionViewCellReuseIdentifier, for: indexPath) as! MainCollectionViewCell
                cell.videoId = postcard!.videoId
                return cell
            default:
                let cell: MainImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MainImageCollectionViewCellReuseIdentifier, for: indexPath) as! MainImageCollectionViewCell
                cell.imageURL = postcard?.backImageURL
                return cell
            }
        }else{
            switch indexPath.row {
            case 0:
                let cell: MainImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MainImageCollectionViewCellReuseIdentifier, for: indexPath) as! MainImageCollectionViewCell
                cell.placeholerImageURL = order!.frontPlaceImageURL
                cell.imageURL = order!.frontImageURL
                return cell
            case 1:
                let cell: MainCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MainCollectionViewCellReuseIdentifier, for: indexPath) as! MainCollectionViewCell
                cell.videoId = order!.videoId
                return cell
            default:
                let cell: MainImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MainImageCollectionViewCellReuseIdentifier, for: indexPath) as! MainImageCollectionViewCell
                cell.imageURL = order!.backImageURL
                return cell
            }
        }
    }
    
    //MARK: 不显示Cell时。需要暂停播放视频
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell: MainCollectionViewCell = cell as? MainCollectionViewCell{
            cell.pausePlayer(hidePlayButton: true)
        }
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == screenWidth{
            if let cell: MainCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? MainCollectionViewCell{
                cell.playVideo()
            }
        }
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
    }
}
