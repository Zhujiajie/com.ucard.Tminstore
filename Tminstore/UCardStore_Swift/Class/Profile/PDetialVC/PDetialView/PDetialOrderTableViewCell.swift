//
//  PDetialOrderTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/21.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

//MARK: 展示地址的cell
class PDetailOrderAddressCell: UITableViewCell{
    
    /// 数据源
    var order: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    ///收件人label
    fileprivate var receiverLabel: UILabel?
    /// 收件人姓名的Label
    fileprivate var receiverNameLabel: UILabel?
    ///地址的Label
    fileprivate var addressLabel: UILabel?
    /// 地址内容的Label
    fileprivate var addressContentLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#F5F5F5")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(0.5)
        }
        
        let greenBackView: UIView = UIView()
        greenBackView.backgroundColor = UIColor(hexString: "4CDFED")
        contentView.addSubview(greenBackView)
        greenBackView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.bottom.equalTo(contentView)
            make.size.equalTo(CGSize(width: 61/baseWidth, height: 75/baseWidth))
        }
        
        let addressIconImageView: UIImageView = UIImageView(image: UIImage(named: "orderAddressIcon"))
        addressIconImageView.contentMode = UIViewContentMode.scaleAspectFill
        greenBackView.addSubview(addressIconImageView)
        addressIconImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(greenBackView)
            make.size.equalTo(CGSize(width: 30/baseWidth, height: 35/baseWidth))
        }
        
        receiverLabel = setLabel()
        contentView.addSubview(receiverLabel!)
        receiverLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(8/baseWidth)
            make.left.equalTo(greenBackView.snp.right).offset(13/baseWidth)
        })
        
        receiverNameLabel = setLabel()
        receiverNameLabel?.numberOfLines = 1
        contentView.addSubview(receiverNameLabel!)
        receiverNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(receiverLabel!)
            make.left.equalTo(greenBackView.snp.right).offset(82/baseWidth)
            make.right.equalTo(contentView).offset(-15/baseWidth)
        })
        
        addressLabel = setLabel()
        contentView.addSubview(addressLabel!)
        addressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(receiverLabel!.snp.bottom).offset(8/baseWidth)
            make.leftMargin.equalTo(receiverLabel!)
        })
        
        addressContentLabel = setLabel()
        contentView.addSubview(addressContentLabel!)
        addressContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(addressLabel!)
            make.leftMargin.equalTo(receiverNameLabel!)
            make.rightMargin.equalTo(receiverNameLabel!)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        receiverLabel?.text = NSLocalizedString("收件人", comment: "")
        addressLabel?.text = NSLocalizedString("收货地址", comment: "")
        receiverNameLabel?.text = order!.addressModel.mailName
        
        if languageCode() == "CN"{
            addressContentLabel?.text = order!.addressModel.country + order!.addressModel.province + order!.addressModel.district + order!.addressModel.city + order!.addressModel.detailedAddress
        }else{
            addressContentLabel?.text = order!.addressModel.detailedAddress + order!.addressModel.district + order!.addressModel.city + order!.addressModel.province + order!.addressModel.country
        }
    }
    
    //MARK: 设置Label
    fileprivate func setLabel()->UILabel{
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "535353")
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 0
        return label
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示AR密码的cell
class PDetailOrderARCodeCell: UITableViewCell{
    
    /// 数据源
    var order: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// AR密码的label
    fileprivate var arCodeContentLabel: UILabel?
    /// 订单价格的label
    fileprivate var originalPriceContentLabel: UILabel?
    /// 实付款的Label
    fileprivate var priceContentLabel: UILabel?
    /// 订单数量的Label
    fileprivate var orderNumberLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#F5F5F5")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(0.5)
        }
        
        let arCodeLabel: UILabel = setLabel()
        contentView.addSubview(arCodeLabel)
        arCodeLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(13/baseWidth)
            make.left.equalTo(contentView).offset(19/baseWidth)
        }
        arCodeLabel.text = NSLocalizedString("时光密码", comment: "")
        
        let originalPriceLabel: UILabel = setLabel()
        contentView.addSubview(originalPriceLabel)
        originalPriceLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(arCodeLabel.snp.bottom).offset(7/baseWidth)
            make.leftMargin.equalTo(arCodeLabel)
        }
        originalPriceLabel.text = NSLocalizedString("订单总价", comment: "")
        
        let payLabel: UILabel = setLabel()
        contentView.addSubview(payLabel)
        payLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(originalPriceLabel.snp.bottom).offset(7/baseWidth)
            make.leftMargin.equalTo(arCodeLabel)
        }
        payLabel.text = NSLocalizedString("实付款", comment: "")
        
        let numberLabel: UILabel = setLabel()
        contentView.addSubview(numberLabel)
        numberLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(payLabel.snp.bottom).offset(7/baseWidth)
            make.leftMargin.equalTo(arCodeLabel)
            make.bottom.equalTo(contentView).offset(-9/baseWidth)
        }
        numberLabel.text = NSLocalizedString("数量", comment: "")
        
        
        arCodeContentLabel = setContentLabel()
        contentView.addSubview(arCodeContentLabel!)
        arCodeContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(arCodeLabel)
            make.right.equalTo(contentView).offset(-19/baseWidth)
        })
        
        originalPriceContentLabel = setContentLabel()
        contentView.addSubview(originalPriceContentLabel!)
        originalPriceContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(originalPriceLabel)
            make.rightMargin.equalTo(arCodeContentLabel!)
        })
        
        priceContentLabel = setContentLabel()
        contentView.addSubview(priceContentLabel!)
        priceContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(payLabel)
            make.rightMargin.equalTo(arCodeContentLabel!)
        })
        priceContentLabel?.textColor = UIColor(hexString: "4CDFED")
        
        orderNumberLabel = setContentLabel()
        contentView.addSubview(orderNumberLabel!)
        orderNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(numberLabel)
            make.rightMargin.equalTo(arCodeContentLabel!)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        if let arCode: String = order?.arCode{
            arCodeContentLabel?.text = arCode
        }
        
        originalPriceContentLabel?.text = "¥ 5.00"
        priceContentLabel?.text = "¥ \(order!.orderAmount)"
        orderNumberLabel?.text = "x 1"
    }
    
    //MARK: 设置Label
    fileprivate func setLabel()->UILabel{
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "6B6B6B")
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 1
        return label
    }
    
    //MARK: 设置内容label
    fileprivate func setContentLabel()->UILabel{
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "6B6B6B")
        label.font = UIFont.systemFont(ofSize: 14)
        label.numberOfLines = 1
        label.textAlignment = NSTextAlignment.right
        return label
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示订单编号和下单时间的Cell
class PDetailOrderNumberCell: UITableViewCell{
    
    /// 数据源
    var order: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 是否支付的label
    fileprivate var payedContentLabel: UILabel?
    /// 支付方式的Label
    fileprivate var payChannelContentLabel: UILabel?
    /// 下单时间的label
    fileprivate var orderCreatTimeContentLabel: UILabel?
    /// 订单编号的label
    fileprivate var orderNumberContentLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#F5F5F5")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(0.5)
        }
        
        let orderStatusLabel: UILabel = setLabel()
        contentView.addSubview(orderStatusLabel)
        orderStatusLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(18/baseWidth)
            make.left.equalTo(contentView).offset(19/baseWidth)
        }
        orderStatusLabel.text = NSLocalizedString("支付状态", comment: "")
    
        let payChannelLabel: UILabel = setLabel()
        contentView.addSubview(payChannelLabel)
        payChannelLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(orderStatusLabel.snp.bottom).offset(4/baseWidth)
            make.leftMargin.equalTo(orderStatusLabel)
        }
        payChannelLabel.text = NSLocalizedString("支付方式", comment: "")
        
        let creatTimeLabel: UILabel = setLabel()
        contentView.addSubview(creatTimeLabel)
        creatTimeLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(payChannelLabel.snp.bottom).offset(4/baseWidth)
            make.leftMargin.equalTo(orderStatusLabel)
        }
        creatTimeLabel.text = NSLocalizedString("下单时间", comment: "")
        
        let orderNumberLabel: UILabel = setLabel()
        contentView.addSubview(orderNumberLabel)
        orderNumberLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(creatTimeLabel.snp.bottom).offset(4/baseWidth)
            make.leftMargin.equalTo(creatTimeLabel)
        }
        orderNumberLabel.text = NSLocalizedString("订单编号", comment: "")
        
        payedContentLabel = setContentLabel()
        contentView.addSubview(payedContentLabel!)
        payedContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(orderStatusLabel)
            make.right.equalTo(contentView).offset(-19/baseWidth)
        })
        
        payChannelContentLabel = setContentLabel()
        contentView.addSubview(payChannelContentLabel!)
        payChannelContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(payChannelLabel)
            make.rightMargin.equalTo(payedContentLabel!)
        })
        
        orderCreatTimeContentLabel = setContentLabel()
        contentView.addSubview(orderCreatTimeContentLabel!)
        orderCreatTimeContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(creatTimeLabel)
            make.rightMargin.equalTo(payedContentLabel!)
        })
        
        orderNumberContentLabel = setContentLabel()
        contentView.addSubview(orderNumberContentLabel!)
        orderNumberContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(orderNumberLabel)
            make.rightMargin.equalTo(payedContentLabel!)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        var orderStatusString: String
        switch order!.status {
        case OrderStaus.unPaied:
            orderStatusString = NSLocalizedString("未支付", comment: "")
        case OrderStaus.paid:
            orderStatusString = NSLocalizedString("已支付", comment: "")
        case OrderStaus.send:
            orderStatusString = NSLocalizedString("已发货", comment: "")
        default:
            orderStatusString = NSLocalizedString("已签收", comment: "")
        }
        payedContentLabel?.text = orderStatusString
        
        var payChannelString: String
        switch order!.payChannel {
        case PayStyle.coupon:
            payChannelString = NSLocalizedString("优惠券支付", comment: "")
        case PayStyle.aliPay:
            payChannelString = NSLocalizedString("支付宝支付", comment: "")
        case PayStyle.weChat:
            payChannelString = NSLocalizedString("微信支付", comment: "")
        default:
            payChannelString = NSLocalizedString("未知", comment: "")
        }
        payChannelContentLabel?.text = payChannelString
        
        if let createTime: Date = order?.createDate{
            let timeFormatter: DateFormatter = DateFormatter()
            timeFormatter.dateStyle = DateFormatter.Style.medium
            timeFormatter.timeStyle = DateFormatter.Style.short
            let timeString: String = timeFormatter.string(from: createTime)
            orderCreatTimeContentLabel?.text = timeString
        }
        
        orderNumberContentLabel?.text = order!.mailOrderNo
    }
    
    //MARK: 设置Label
    fileprivate func setLabel()->UILabel{
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "838383")
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 1
        return label
    }
    
    //MARK: 设置内容label
    fileprivate func setContentLabel()->UILabel{
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "838383")
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 1
        label.textAlignment = NSTextAlignment.right
        return label
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
