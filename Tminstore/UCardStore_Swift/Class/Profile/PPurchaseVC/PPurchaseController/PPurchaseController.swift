//
//  PPurchaseController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/6.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PPurchaseController: BaseViewController {

    var order: OrderPostcardModel?
    
    fileprivate let scanViewModel: QRViewModel = QRViewModel()
    
    /// tableHeaderView
    fileprivate var tableHeaderView: PDetialHeaderView?
    /// 评论tableView
    fileprivate var tableView: PPurchaseTableView?
    /// 放置制作、添加视频和扫描按钮的view
    fileprivate var footerView: PPurchaseFooterView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        setUI()
        closures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.cancelTansparentNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    //MARK:设置UI
    fileprivate func setUI(){
        
        tableHeaderView = PDetialHeaderView()
        tableHeaderView?.shouldShowBackImage = false
        tableHeaderView?.isOrder = true
        tableHeaderView?.order = order
        sizeHeaderToFit(shouldAnimate: true)
        
        footerView = PPurchaseFooterView()
        view.addSubview(footerView!)
        footerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(60/baseWidth)
        })
        
        tableView = PPurchaseTableView()
        view.addSubview(tableView!)
        tableView!.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.top.right.equalTo(view)
            make.bottom.equalTo(footerView!.snp.top)
        }
        tableView!.tableHeaderView = tableHeaderView
        
        tableView?.delegate = self
        tableView?.dataSource = self
        
        tableView?.reloadData()
    }
    
    //MARK:设置导航栏
    fileprivate func setNavigationBar() {
        setNavigationBarLeftButtonWithImageName("backArrow")//设置左按钮
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D")] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("购买详情", comment: ""), attributeDic: attDic)
    }

    //MARK: 点击导航栏左按钮退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: 计算tableHeaderView的高度
    /**
     计算tableHeaderView的高度
     */
    func sizeHeaderToFit(shouldAnimate: Bool) {
        
        tableHeaderView?.setNeedsLayout()
        tableHeaderView?.layoutIfNeeded()
        
        let height: CGFloat? = tableHeaderView?.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        
        if shouldAnimate {
            UIView.animate(withDuration: 0.5, delay: 0.1, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                self.tableHeaderView?.frame.size.height = height!
                self.tableView?.tableHeaderView = self.tableHeaderView
            }, completion: nil)
        }else{
            tableHeaderView?.frame.size.height = height!
        }
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击制作实物按钮的事件
        footerView?.touchMakeButtonClosure = {[weak self] _ in
            self?.warmAlert()
        }
        
        //MARK: 点击添加视频按钮的事件
        footerView?.touchAddVideoButtonClosure = {[weak self] _ in
            self?.warmAlert()
        }
        
        //MARK: 点击扫描按钮的事件
        footerView?.touchScanButtonClosure = {[weak self] _ in
            if let code: String = self?.order?.arCode{
                self?.scanViewModel.requestARImageInfo(code: code)
            }else{
                self?.alert(alertTitle: NSLocalizedString("该图片没有AR功能", comment: ""))
            }
        }
        
        //MARK: 成功下载AR识别图的闭包
        scanViewModel.requestARImageSuccess = {[weak self] (imagePath: String, videoURLString: String) in
            //进入本地识别
            let arVC: LocalARCardViewController = LocalARCardViewController()
            arVC.arImagePath = imagePath
            arVC.videoURLString = videoURLString
            self?.present(arVC, animated: true, completion: nil)
            CFRunLoopWakeUp(CFRunLoopGetCurrent())//解决延迟的现象
        }
        
        //MARK: 出错的闭包
        scanViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 提示用户功能正在开发中
    fileprivate func warmAlert(){
        alert(alertTitle: NSLocalizedString("功能开发中\n敬请期待", comment: ""))
    }
}

extension PPurchaseController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PPurchaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.PPurchaseTableViewCellReuseIdentifier, for: indexPath) as! PPurchaseTableViewCell
        
        cell.indexPath = indexPath
        cell.order = order
        
        //MARK: 点击充值按钮的事件
        cell.touchRechargeButtonClosure = {[weak self] _ in
            self?.warmAlert()
        }
        
        return cell
    }
}
