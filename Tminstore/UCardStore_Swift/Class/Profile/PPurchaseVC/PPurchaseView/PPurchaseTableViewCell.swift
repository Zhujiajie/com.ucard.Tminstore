//
//  PPurchaseTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/6.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PPurchaseTableViewCell: UITableViewCell {

    var indexPath: IndexPath?{
        didSet{
            if indexPath?.row == 0{
                setRemarkCell()
            }else{
                setDetailCell()
            }
        }
    }
    
    var order: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 点击充值按钮的事件
    var touchRechargeButtonClosure: (()->())?
    
    /// remarkLabel
    fileprivate var remarkLabel: UILabel?
    /// 昵称
    fileprivate var nickNameLabel: UILabel?
    /// 购买时间
    fileprivate var orderTimeLabel: UILabel?
    /// 地点icon
    fileprivate var locationIconImageView: UIImageView?
    /// 地点label
    fileprivate var locationLabel: UILabel?
    /// 剩余次数
    fileprivate var scanTimeLabel: UILabel?
    /// ar密码label
    fileprivate var arCodeLabel: UILabel?
    /// 充值按钮
    fileprivate var rechargeButton: UIButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    

    //MARK: 设置remark的cell
    fileprivate func setRemarkCell(){
        
        remarkLabel = UILabel()
        remarkLabel?.textColor = UIColor(hexString: "6B6B6B")
        remarkLabel?.font = UIFont.systemFont(ofSize: 14)
        remarkLabel?.numberOfLines = 2
        contentView.addSubview(remarkLabel!)
        remarkLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
            make.bottom.equalTo(contentView).offset(-16/baseWidth)
        })
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "C1C1C1")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.right.equalTo(contentView)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.height.equalTo(0.5)
        }
    }
    
    //MARK: 设置具体内容的cell
    fileprivate func setDetailCell(){
        
        nickNameLabel = UILabel()
        nickNameLabel?.textColor = UIColor(hexString: "6B6B6B")
        nickNameLabel?.font = UIFont.systemFont(ofSize: 20)
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(8/baseWidth)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.right.equalTo(contentView).offset(-16/baseWidth)
            make.height.equalTo(22)
        })
        
        orderTimeLabel = UILabel()
        orderTimeLabel?.textColor = UIColor(hexString: "878686")
        orderTimeLabel?.font = UIFont.systemFont(ofSize: 12)
        contentView.addSubview(orderTimeLabel!)
        orderTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
            make.height.equalTo(14)
        })
        
        locationIconImageView = UIImageView(image: UIImage(named: "purchaseLocationIcon"))
        locationIconImageView?.contentMode = UIViewContentMode.scaleAspectFill
        contentView.addSubview(locationIconImageView!)
        locationIconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(orderTimeLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.size.equalTo(CGSize(width: 14/baseWidth, height: 14/baseWidth))
        })
        
        locationLabel = UILabel()
        locationLabel?.font = UIFont.systemFont(ofSize: 12)
        locationLabel?.textColor = UIColor(hexString: "878686")
        contentView.addSubview(locationLabel!)
        locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(locationIconImageView!)
            make.left.equalTo(locationIconImageView!.snp.right).offset(5/baseWidth)
            make.rightMargin.equalTo(nickNameLabel!)
            make.height.equalTo(14)
        })
        
        scanTimeLabel = UILabel()
        scanTimeLabel?.textColor = UIColor(hexString: "878686")
        scanTimeLabel?.font = UIFont.systemFont(ofSize: 12)
        contentView.addSubview(scanTimeLabel!)
        scanTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(contentView).offset(-11/baseWidth)
            make.leftMargin.equalTo(locationIconImageView!)
            make.top.equalTo(locationIconImageView!.snp.bottom).offset(26/baseWidth)
        })
        
        arCodeLabel = UILabel()
        arCodeLabel?.font = UIFont.systemFont(ofSize: 14)
        arCodeLabel?.textColor = UIColor(hexString: "4CDFED")
        contentView.addSubview(arCodeLabel!)
        arCodeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
        })
        arCodeLabel?.textAlignment = NSTextAlignment.right
        
        rechargeButton = UIButton()
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4CDFED"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("充值", comment: ""), attributes: attDic)
        rechargeButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        rechargeButton?.layer.cornerRadius = 2
        rechargeButton?.layer.borderColor = UIColor(hexString: "4CDFED").cgColor
        rechargeButton?.layer.borderWidth = 1
        rechargeButton?.addTarget(self, action: #selector(touchRechargeButton(button:)), for: UIControlEvents.touchUpInside)
        contentView.addSubview(rechargeButton!)
        rechargeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottomMargin.equalTo(scanTimeLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
            make.size.equalTo(CGSize(width: 70/baseWidth, height: 32/baseWidth))
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        remarkLabel?.text = " "
        
        nickNameLabel?.text = order?.addressModel.mailName
        
        if let createTime: Date = order?.createDate{
            let timeFormatter: DateFormatter = DateFormatter()
            timeFormatter.dateStyle = DateFormatter.Style.short
            timeFormatter.timeStyle = DateFormatter.Style.none
            let timeString: String = timeFormatter.string(from: createTime)
            orderTimeLabel?.text = NSLocalizedString("购买时间", comment: "") + " " + timeString
        }
        
        if languageCode() == "CN"{
            locationLabel?.text = order!.addressModel.country + order!.addressModel.province + order!.addressModel.district + order!.addressModel.city
        }else{
            locationLabel?.text = order!.addressModel.district + order!.addressModel.city + order!.addressModel.province + order!.addressModel.country
        }
        
        scanTimeLabel?.text = ""
        
        if let arCodeString: String = order?.arCode{
            arCodeLabel?.text = NSLocalizedString("密码:", comment: "") + " " + arCodeString
        }
    }
    
    //MARK: 点击充值按钮的事件
    func touchRechargeButton(button: UIButton){
        touchRechargeButtonClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
