//
//  PPurchaseTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/6.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PPurchaseTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        separatorStyle = UITableViewCellSeparatorStyle.none
        estimatedRowHeight = 110/baseWidth
        
        register(PPurchaseTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.PPurchaseTableViewCellReuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
