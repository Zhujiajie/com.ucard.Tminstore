//
//  PPurchaseFooterView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/6.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class PPurchaseFooterView: UIView {

    /// 点击制作实物按钮的事件
    var touchMakeButtonClosure: (()->())?
    /// 点击添加视频按钮的事件
    var touchAddVideoButtonClosure: (()->())?
    /// 点击扫描按钮的事件
    var touchScanButtonClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let makeButton: UIButton = UIButton()
        let attDic1: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor.white]
        let attStr1: NSAttributedString = NSAttributedString(string: NSLocalizedString("制作实物", comment: ""), attributes: attDic1)
        makeButton.setAttributedTitle(attStr1, for: UIControlState.normal)
        makeButton.backgroundColor = UIColor(hexString: "4CDFED")
        addSubview(makeButton)
        makeButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.bottom.equalTo(self)
            make.width.equalTo(181/baseWidth)
        }
        makeButton.addTarget(self, action: #selector(touchMakeButton(button:)), for: UIControlEvents.touchUpInside)
        
        let addVideButton: UIButton = UIButton()
        let attDic2: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "4CDFED")]
        let attStr2: NSAttributedString = NSAttributedString(string: NSLocalizedString("添加视频", comment: ""), attributes: attDic2)
        addVideButton.setAttributedTitle(attStr2, for: UIControlState.normal)
        addVideButton.backgroundColor = UIColor(hexString: "#FBFBFB")
        addSubview(addVideButton)
        addVideButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.bottom.equalTo(self)
            make.left.equalTo(makeButton.snp.rightMargin)
            make.width.equalTo(96/baseWidth)
        }
        addVideButton.addTarget(self, action: #selector(touchAddVideoButton(button:)), for: UIControlEvents.touchUpInside)
        
        //添加一条灰色竖线
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "D7D7D7")
        addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.bottom.equalTo(self)
            make.left.equalTo(addVideButton.snp.right)
            make.width.equalTo(0.5)
        }
        
        let scanButton: UIButton = UIButton()
        let attStr3: NSAttributedString = NSAttributedString(string: NSLocalizedString("扫描", comment: ""), attributes: attDic2)
        scanButton.setAttributedTitle(attStr3, for: UIControlState.normal)
        scanButton.backgroundColor = UIColor(hexString: "#FBFBFB")
        addSubview(scanButton)
        scanButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.right.bottom.equalTo(self)
            make.left.equalTo(grayLine.snp.right)
        }
        scanButton.addTarget(self, action: #selector(touchScanButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    

    
    //MARK: 点击制作实物按钮
    func touchMakeButton(button: UIButton){
        touchMakeButtonClosure?()
    }
    
    //MARK: 点击添加视频按钮
    func touchAddVideoButton(button: UIButton){
        touchAddVideoButtonClosure?()
    }
    
    //MARK: 点击扫描按钮
    func touchScanButton(button: UIButton){
        touchScanButtonClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
