//
//  PUserSpaceTimeCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

class PUserSpaceTimeCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: screenWidth, height: 252/baseWidth)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        self.isPagingEnabled = true
        self.backgroundColor = UIColor.white
        
        self.register(PUserSpaceTimeCollectionCell.self, forCellWithReuseIdentifier: appReuseIdentifier.PUserSpaceTimeCollectionCellReuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class PUserSpaceTimeCollectionCell: UICollectionViewCell{
    var spaceTimeModel: SpaceTimeDataModel?{
        didSet{
            setData()
        }
    }
    
    ///图片
    fileprivate var imageView: BaseImageView?
    /// 地址label
    fileprivate var addressLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(5)
            make.left.equalTo(contentView).offset(5)
            make.right.equalTo(contentView).offset(-5)
            make.height.equalTo(169/baseWidth)
        })
        
        let whiteContainerView: BaseView = BaseView.colorLine(colorString: "#FFFFFF")
        contentView.addSubview(whiteContainerView)
        whiteContainerView.snp.makeConstraints { (make) in
            make.top.equalTo(imageView!.snp.bottom)
            make.bottom.equalTo(contentView)
            make.leftMargin.equalTo(imageView!)
            make.rightMargin.equalTo(imageView!)
        }
        
        let locationIcon: BaseImageView = BaseImageView.normalImageView(andImageName: "clueLocationIcon")
        whiteContainerView.addSubview(locationIcon)
        locationIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(whiteContainerView)
            make.left.equalTo(contentView).offset(11/baseWidth)
            make.size.equalTo(CGSize(width: 25/baseWidth, height: 25/baseWidth))
        }
        
        addressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "747474", andNumberOfLines: 2)
        whiteContainerView.addSubview(addressLabel!)
        addressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(imageView!.snp.bottom).offset(10/baseWidth)
            make.bottom.equalTo(contentView).offset(-10/baseWidth)
            make.left.equalTo(locationIcon.snp.right).offset(10/baseWidth)
            make.right.equalTo(contentView).offset(-10/baseWidth)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        if let url: URL = spaceTimeModel?.frontImageURL{
            //先判断是否已缓存图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                
                if result == true{
                    self?.imageView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.spaceTimeModel?.frontPlaceImageURL{
                        self?.imageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.imageView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.imageView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.imageView?.sd_setImage(with: url)
                    }
                }
            })
        }
        
        addressLabel?.text = spaceTimeModel?.detailedAddress
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
