//
//  PUserDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PUserDataModel: BaseDataModel {

    //MARK: 获取其他用户的信息
    /// 获取其他用户的信息
    ///
    /// - Parameters:
    ///   - memberCode: 用户ID
    ///   - success: 成功的闭包
    func requestOtherUserInfo(memberCode: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "memberCode": memberCode
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.checkOtherUserInfoAPI, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 请求其他用户时光圈数据
    /// 请求其他用户时光圈数据
    ///
    /// - Parameters:
    ///   - memberCode: 用户ID
    ///   - pageNum: 分页数
    ///   - success: 成功的闭包
    func requestOtherUserSpaceTimeData(memberCode: String, andPageNum pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.checkOtherUserSpaceTimeDataAPI + "\(pageNum)"
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "memberCode": memberCode
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 关注某个用户
    /// 关注某个用户
    ///
    /// - Parameters:
    ///   - memberCode: 用户ID
    ///   - success: 成功的闭包
    func followUser(memberCode: String, successClosure success: ((_ result: [String: Any])->())?){
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "followMemberCode": memberCode
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.followUserAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 取消关注某个用户
    /// 取消关注某个用户
    ///
    /// - Parameters:
    ///   - memberCode: 用户ID
    ///   - success: 成功的闭包
    func cancelFollowUser(memberCode: String, successClosure success: ((_ result: [String: Any])->())?){
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "followMemberCode": memberCode
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.cancelFollowUserAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 获取其他用户的关注列表
    /// 获取其他用户的关注列表
    ///
    /// - Parameters:
    ///   - memberCode: 用户ID
    ///   - pageNum: 页数
    ///   - success: 成功的闭包
    func requestOtherUserFollows(memberCode: String, andPageNum pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.requestOtherUserFollowsAPI + "\(pageNum)"
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "memberCode": memberCode
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 获取其他用户粉丝数据
    /// 获取其他用户粉丝数据
    ///
    /// - Parameters:
    ///   - memberCode: 用户ID
    ///   - pageNum: 粉丝页数
    ///   - success: 成功的闭包
    func requestOtherUserFans(memberCode: String, andPageNum pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.requestOtherUserFansAPI + "\(pageNum)"
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "memberCode": memberCode
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 获取其他用户录屏数据
    /// 获取其他用户录屏数据
    ///
    /// - Parameters:
    ///   - memberCode: 用户ID
    ///   - pageNum: 录屏数据页数
    ///   - success: 成功的闭包
    func requestOtherUserRecordInfo(memberCode: String, andPageNum pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.requestOtherUserRecordAPI + "\(pageNum)"
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "memberCode": memberCode
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看自己的好友
    /// 查看自己的好友
    ///
    /// - Parameters:
    ///   - pageIndex: 页数
    ///   - success: 成功的闭包
    func requestMyFriendsData(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?){
        let urlString: String = appAPIHelp.requestMyFriendsDataAPI + "\(pageIndex)"
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看自己的关注
    /// 查看自己的关注
    ///
    /// - Parameters:
    ///   - pageIndex: 页数
    ///   - success: 成功的闭包
    func requestMyFollowData(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?){
        let urlString: String = appAPIHelp.requestMyFollowsDataAPI + "\(pageIndex)"
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看自己的粉丝
    /// 查看自己的粉丝
    ///
    /// - Parameters:
    ///   - pageIndex: 页数
    ///   - success: 成功的闭包
    func requestMyFansData(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?){
        let urlString: String = appAPIHelp.requestMyFansDataAPI + "\(pageIndex)"
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
