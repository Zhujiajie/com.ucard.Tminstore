//
//  PUserTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 展示用户好友, 关注和粉丝的界面

import UIKit

class PUserTableViewController: UITableViewController {

    weak var viewModel: PUserViewModel?
    /// 控制器要展示的数据类型
    var type: UserOtherInfoType = UserOtherInfoType.friends
    
    /// 点击了Cell
    var selectCell: ((_ userInfoModel: UserInfoModel)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(PUserFollowTableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        self.tableView.tableFooterView = UIView()
        
        self.tableView.mj_header = BaseAnimationHeader()
        self.tableView.mj_footer = BaseRefreshFooter()
        
        closures()
    }

    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 下拉刷新
        tableView.mj_header.refreshingBlock = {[weak self] _ in
            self?.pullToRefresh()
        }
        
        //MARK: 上拉刷新
        tableView.mj_footer.refreshingBlock = {[weak self] _ in
            self?.pushToRefresh()
        }
        
        //MARK: 成功获得好友数据
        viewModel?.getFriendsDataSuccess = {[weak self] _ in
            self?.stopRefresh()
            self?.tableView.reloadData()
        }
        
        //MARK: 成功获得关注数据
        viewModel?.getFollowDataSuccess = {[weak self] _ in
            self?.stopRefresh()
            self?.tableView.reloadData()
        }
        
        //MARK: 成功获得粉丝数据
        viewModel?.getFansDataSuccess = {[weak self] _ in
            self?.stopRefresh()
            self?.tableView.reloadData()
        }
        
        //MARK: 没有更多好友数据了
        viewModel?.noMoreFriendsData = {[weak self] _ in
            self?.noMoreData()
        }
        
        //MARK: 没有更多好友数据了
        viewModel?.noMoreFollowsData = {[weak self] _ in
            self?.noMoreData()
        }
        
        //MARK: 没有更多好友数据了
        viewModel?.noMoreFansData = {[weak self] _ in
            self?.noMoreData()
        }
        
        //MARK: 关注成功
        viewModel?.followOtherSuccess = {[weak self] (indexPath: IndexPath) in
            self?.followOrUnFollowSuccess(indexPath: indexPath, andIsFollow: true)
        }
        
        //MARK: 取消关注成功
        viewModel?.cancelFollowOtherSuccess = {[weak self] (indexPath: IndexPath) in
            self?.followOrUnFollowSuccess(indexPath: indexPath, andIsFollow: false)
        }
    }
    
    //MARK: 关注成功
    fileprivate func followOrUnFollowSuccess(indexPath: IndexPath, andIsFollow isFollow: Bool){
        switch type {
        case UserOtherInfoType.friends:
            viewModel?.friendDataArray[indexPath.row].isFollowed = isFollow
        case UserOtherInfoType.follows:
            viewModel?.followsDataArray[indexPath.row].isFollowed = isFollow
        default:
            viewModel?.fansDataArray[indexPath.row].isFollowed = isFollow
        }
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
    }
    
    //MARK: 下拉刷新
    fileprivate func pullToRefresh(){
        switch type {
        case UserOtherInfoType.friends:
            viewModel?.friendsPageNum = 1
            viewModel?.requsetMyFriendsData()
        case UserOtherInfoType.follows:
            viewModel?.followPageNum = 1
            viewModel?.requestMyFollowData()
        default:
            viewModel?.fansPageNum = 1
            viewModel?.requestMyFansData()
        }
    }
    
    //MARK: 上拉刷新
    fileprivate func pushToRefresh(){
        switch type {
        case UserOtherInfoType.friends:
            viewModel?.requsetMyFriendsData()
        case UserOtherInfoType.follows:
            viewModel?.requestMyFollowData()
        default:
            viewModel?.requestMyFansData()
        }
    }
    
    //MARK: 停止刷新
    /// 停止刷新
    func stopRefresh(){
        tableView.mj_header.endRefreshing()
        tableView.mj_footer.endRefreshing()
    }
    
    //MARK: 没有更多数据了
    fileprivate func noMoreData(){
        tableView.mj_header.endRefreshing()
        tableView.mj_footer.endRefreshingWithNoMoreData()
    }
    
    //MARK: 开始下拉刷新
    /// 开始下拉刷新
    func beginRefresh(){
        tableView.mj_header.beginRefreshing()
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        switch type {
        case UserOtherInfoType.friends:
            return viewModel!.friendDataArray.count
        case UserOtherInfoType.follows:
            return viewModel!.followsDataArray.count
        default:
            return viewModel!.fansDataArray.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: PUserFollowTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! PUserFollowTableViewCell
        
        switch type {
        case UserOtherInfoType.friends:
            cell.userInfoModel = viewModel!.friendDataArray[indexPath.row]
        case UserOtherInfoType.follows:
            cell.userInfoModel = viewModel!.followsDataArray[indexPath.row]
        default:
            cell.userInfoModel = viewModel!.fansDataArray[indexPath.row]
        }
        
        //MARK: 点击关注按钮的事件
        cell.touchFollowButtonClosure = {[weak self] _ in
            if cell.userInfoModel?.isFollowed == true {
                self?.viewModel?.cancelFollowUser(memberCode: cell.userInfoModel!.memberCode, andIndexPath: indexPath)
            }else{
                self?.viewModel?.followUser(memberCode: cell.userInfoModel!.memberCode, andIndexPath: indexPath)
            }
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60/baseWidth
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch type {
        case UserOtherInfoType.friends:
            selectCell?(viewModel!.friendDataArray[indexPath.row])
        case UserOtherInfoType.follows:
            selectCell?(viewModel!.followsDataArray[indexPath.row])
        default:
            selectCell?(viewModel!.fansDataArray[indexPath.row])
        }
    }
}
