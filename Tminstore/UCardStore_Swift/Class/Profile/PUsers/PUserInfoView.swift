//
//  PUserInfoView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 展示其他用户基本信息的视图

import UIKit

class PUserInfoView: UIView {

    /// 用户信息模型
    var userInfoModel: UserInfoModel?{
        didSet{
            setData()
        }
    }
    
    /// 时光圈数量
    var spaceTimeNumber: Int = 0 {
        didSet{
            spaceTimeNumberLabel?.text = "\(spaceTimeNumber)"
        }
    }
    
    /// 录屏数量
    var recordNumber: Int = 0 {
        didSet{
            recordNumberLabel?.text = "\(recordNumber)"
        }
    }
    
    /// 关注数
    var followNumber: Int = 0 {
        didSet{
            attentionNumberLabel?.text = "\(followNumber)"
        }
    }
    
    /// 粉丝数
    var fansNumber: Int = 0 {
        didSet{
            fansNumberLabel?.text = "\(fansNumber)"
        }
    }
    
    /// 点击按钮事件
    var touchButtonClosure: ((_ buttongType: UserInfoButtonType)->())?
    
    /// 用户头像
    fileprivate var userPortrait: BaseImageView?
    /// 时光圈按钮
    fileprivate var spaceTimeButton: BaseButton?
    /// 时光圈数量
    fileprivate var spaceTimeNumberLabel: BaseLabel?
    /// 时光圈标题
    fileprivate var spaceTimeTitleLabel: BaseLabel?
    /// 录屏按钮
    fileprivate var recordButton: BaseButton?
    /// 录屏数量
    fileprivate var recordNumberLabel: BaseLabel?
    /// 录屏标题
    fileprivate var recordTitleLabel: BaseLabel?
    /// 关注按钮
    fileprivate var attentionButton: BaseButton?
    /// 关注数量
    fileprivate var attentionNumberLabel: BaseLabel?
    /// 关注标题
    fileprivate var attentionTitleLabel: BaseLabel?
    /// 粉丝按钮
    fileprivate var fansButton: BaseButton?
    /// 粉丝数量
    fileprivate var fansNumberLabel: BaseLabel?
    /// 粉丝标题
    fileprivate var fansTitleLabel: BaseLabel?
    /// 绿色线条
    fileprivate var greenLine: BaseView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        userPortrait = BaseImageView.userPortraitView(cornerRadius: 35/baseWidth)
        addSubview(userPortrait!)
        userPortrait?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(self).offset(-14/baseWidth)
            make.left.equalTo(self).offset(23/baseWidth)
            make.size.equalTo(70/baseWidth)
        })
        
        spaceTimeButton = BaseButton()
        addSubview(spaceTimeButton!)
        //MARK: 点击时光圈按钮事件
        spaceTimeButton?.touchButtonClosure = {[weak self] _ in
            self?.selectButton(buttonType: UserInfoButtonType.spaceTime)
            self?.touchButtonClosure?(UserInfoButtonType.spaceTime)
        }
        recordButton = BaseButton()
        addSubview(recordButton!)
        //MARK: 点击录屏按钮事件
        recordButton?.touchButtonClosure = {[weak self] _ in
            self?.selectButton(buttonType: UserInfoButtonType.record)
            self?.touchButtonClosure?(UserInfoButtonType.record)
        }
        attentionButton = BaseButton()
        addSubview(attentionButton!)
        //MARK: 点击关注按钮事件
        attentionButton?.touchButtonClosure = {[weak self] _ in
            self?.selectButton(buttonType: UserInfoButtonType.follow)
            self?.touchButtonClosure?(UserInfoButtonType.follow)
        }
        fansButton = BaseButton()
        addSubview(fansButton!)
        //MARK: 点击粉丝按钮事件
        fansButton?.touchButtonClosure = {[weak self] _ in
            self?.selectButton(buttonType: UserInfoButtonType.fans)
            self?.touchButtonClosure?(UserInfoButtonType.fans)
        }
        
        var numberFont: UIFont
        if #available(iOS 8.2, *) {
            numberFont = UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium)
        } else {
            numberFont = UIFont.systemFont(ofSize: 18)
        }
        
        spaceTimeNumberLabel = BaseLabel.normalLabel(font: numberFont, andTextColorHex: "666666", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: nil)
        addSubview(spaceTimeNumberLabel!)
        recordNumberLabel = BaseLabel.normalLabel(font: numberFont, andTextColorHex: "666666", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: nil)
        addSubview(recordNumberLabel!)
        attentionNumberLabel = BaseLabel.normalLabel(font: numberFont, andTextColorHex: "666666", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: nil)
        addSubview(attentionNumberLabel!)
        fansNumberLabel = BaseLabel.normalLabel(font: numberFont, andTextColorHex: "666666", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: nil)
        addSubview(fansNumberLabel!)
        
        spaceTimeTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "8C8A8A", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("时光圈", comment: ""))
        addSubview(spaceTimeTitleLabel!)
        recordTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "8C8A8A", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("录屏", comment: ""))
        addSubview(recordTitleLabel!)
        attentionTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "8C8A8A", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("关注", comment: ""))
        addSubview(attentionTitleLabel!)
        fansTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "8C8A8A", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("粉丝", comment: ""))
        addSubview(fansTitleLabel!)
        
        greenLine = BaseView.colorLine(colorString: "6DF8FF")
        addSubview(greenLine!)
        
        spaceTimeNumberLabel?.text = "0"
        recordNumberLabel?.text = "0"
        attentionNumberLabel?.text = "0"
        fansNumberLabel?.text = "0"
    }
    
    //MARK: 好友的UI
    /// 好友的UI布局
    func friendUI() {
        
        spaceTimeButton?.isHidden = false
        spaceTimeNumberLabel?.isHidden = false
        spaceTimeTitleLabel?.isHidden = false
        recordButton?.isHidden = false
        recordNumberLabel?.isHidden = false
        recordTitleLabel?.isHidden = false
        attentionButton?.isHidden = false
        attentionNumberLabel?.isHidden = false
        attentionTitleLabel?.isHidden = false
        fansButton?.isHidden = false
        fansNumberLabel?.isHidden = false
        fansTitleLabel?.isHidden = false
        
        spaceTimeButton?.snp.makeConstraints({ (make) in
            make.size.equalTo(userPortrait!)
            make.left.equalTo(userPortrait!.snp.right).offset(5/baseWidth)
            make.centerY.equalTo(userPortrait!)
        })
        spaceTimeNumberLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(spaceTimeButton!).offset(10/baseWidth)
            make.centerX.equalTo(spaceTimeButton!)
        })
        spaceTimeTitleLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(spaceTimeNumberLabel!.snp.bottom).offset(6/baseWidth)
            make.centerX.equalTo(spaceTimeButton!)
        })
        recordButton?.snp.makeConstraints({ (make) in
            make.size.equalTo(spaceTimeButton!)
            make.left.equalTo(spaceTimeButton!.snp.right)
            make.centerY.equalTo(spaceTimeButton!)
        })
        recordNumberLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(spaceTimeNumberLabel!)
            make.centerX.equalTo(recordButton!)
        })
        recordTitleLabel?.snp.makeConstraints({ (make) in
            make.bottomMargin.equalTo(spaceTimeTitleLabel!)
            make.centerX.equalTo(recordButton!)
        })
        attentionButton?.snp.makeConstraints({ (make) in
            make.size.equalTo(spaceTimeButton!)
            make.left.equalTo(recordButton!.snp.right)
            make.centerY.equalTo(spaceTimeButton!)
        })
        attentionNumberLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(spaceTimeNumberLabel!)
            make.centerX.equalTo(attentionButton!)
        })
        attentionTitleLabel?.snp.makeConstraints({ (make) in
            make.bottomMargin.equalTo(spaceTimeTitleLabel!)
            make.centerX.equalTo(attentionButton!)
        })
        fansButton?.snp.makeConstraints({ (make) in
            make.size.equalTo(spaceTimeButton!)
            make.left.equalTo(attentionButton!.snp.right)
            make.centerY.equalTo(spaceTimeButton!)
        })
        fansNumberLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(spaceTimeNumberLabel!)
            make.centerX.equalTo(fansButton!)
        })
        fansTitleLabel?.snp.makeConstraints({ (make) in
            make.bottomMargin.equalTo(spaceTimeTitleLabel!)
            make.centerX.equalTo(fansButton!)
        })
        
        greenLine?.snp.makeConstraints({ (make) in
            make.width.equalTo(spaceTimeButton!)
            make.bottom.equalTo(self)
            make.height.equalTo(3)
            make.centerX.equalTo(spaceTimeButton!)
        })
    }
    
    //MARK: 粉丝和关注的UI
    /// 粉丝和关注的UI
    func fansAndFollowsUI(){
        
        spaceTimeButton?.isHidden = false
        spaceTimeNumberLabel?.isHidden = false
        spaceTimeTitleLabel?.isHidden = false
        attentionButton?.isHidden = false
        attentionNumberLabel?.isHidden = false
        attentionTitleLabel?.isHidden = false
        fansButton?.isHidden = false
        fansNumberLabel?.isHidden = false
        fansTitleLabel?.isHidden = false
        
        spaceTimeButton?.snp.makeConstraints({ (make) in
            make.size.equalTo(userPortrait!)
            make.left.equalTo(userPortrait!.snp.right).offset(30/baseWidth)
            make.centerY.equalTo(userPortrait!)
        })
        spaceTimeNumberLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(spaceTimeButton!).offset(10/baseWidth)
            make.centerX.equalTo(spaceTimeButton!)
        })
        spaceTimeTitleLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(spaceTimeNumberLabel!.snp.bottom).offset(6/baseWidth)
            make.centerX.equalTo(spaceTimeButton!)
        })
        
        attentionButton?.snp.makeConstraints({ (make) in
            make.size.equalTo(spaceTimeButton!)
            make.left.equalTo(spaceTimeButton!.snp.right).offset(17/baseWidth)
            make.centerY.equalTo(spaceTimeButton!)
        })
        attentionNumberLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(spaceTimeNumberLabel!)
            make.centerX.equalTo(attentionButton!)
        })
        attentionTitleLabel?.snp.makeConstraints({ (make) in
            make.bottomMargin.equalTo(spaceTimeTitleLabel!)
            make.centerX.equalTo(attentionButton!)
        })
        fansButton?.snp.makeConstraints({ (make) in
            make.size.equalTo(spaceTimeButton!)
            make.left.equalTo(attentionButton!.snp.right).offset(17/baseWidth)
            make.centerY.equalTo(spaceTimeButton!)
        })
        fansNumberLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(spaceTimeNumberLabel!)
            make.centerX.equalTo(fansButton!)
        })
        fansTitleLabel?.snp.makeConstraints({ (make) in
            make.bottomMargin.equalTo(spaceTimeTitleLabel!)
            make.centerX.equalTo(fansButton!)
        })
        
        greenLine?.snp.makeConstraints({ (make) in
            make.width.equalTo(spaceTimeButton!)
            make.bottom.equalTo(self)
            make.height.equalTo(3)
            make.centerX.equalTo(spaceTimeButton!)
        })
    }
    
    //MARK: 陌生人UI
    /// 陌生人UI
    func strangerUI(){
        
        spaceTimeButton?.isHidden = false
        spaceTimeNumberLabel?.isHidden = false
        spaceTimeTitleLabel?.isHidden = false
        
        spaceTimeButton?.snp.makeConstraints({ (make) in
            make.size.equalTo(userPortrait!)
            make.left.equalTo(userPortrait!.snp.right).offset(30/baseWidth)
            make.centerY.equalTo(userPortrait!)
        })
        spaceTimeNumberLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(spaceTimeButton!).offset(10/baseWidth)
            make.centerX.equalTo(spaceTimeButton!)
        })
        spaceTimeTitleLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(spaceTimeNumberLabel!.snp.bottom).offset(6/baseWidth)
            make.centerX.equalTo(spaceTimeButton!)
        })
        
        greenLine?.snp.makeConstraints({ (make) in
            make.width.equalTo(spaceTimeButton!)
            make.bottom.equalTo(self)
            make.height.equalTo(3)
            make.centerX.equalTo(spaceTimeButton!)
        })
    }
    
    //MARK: 点击按钮, 移动绿色线条
    func selectButton(buttonType: UserInfoButtonType){
        greenLine?.snp.remakeConstraints({ (make) in
            make.width.equalTo(spaceTimeButton!)
            make.bottom.equalTo(self)
            make.height.equalTo(3)
            switch buttonType {
            case UserInfoButtonType.spaceTime:
                make.centerX.equalTo(spaceTimeButton!)
            case UserInfoButtonType.record:
                make.centerX.equalTo(recordButton!)
            case UserInfoButtonType.fans:
                make.centerX.equalTo(fansButton!)
            default:
                make.centerX.equalTo(attentionButton!)
            }
        })
        autoLayoutAnimation(view: self)
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        userPortrait?.sd_setImage(with: userInfoModel?.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
        attentionNumberLabel?.text = "\(userInfoModel!.followNumber)"
        fansNumberLabel?.text = "\(userInfoModel!.fansNumber)"
    }
    
    //MARK: 取消UI的自动约束
    func deleteAutoLayout(){
        spaceTimeButton?.snp.removeConstraints()
        spaceTimeNumberLabel?.snp.removeConstraints()
        spaceTimeTitleLabel?.snp.removeConstraints()
        recordButton?.snp.removeConstraints()
        recordNumberLabel?.snp.removeConstraints()
        recordTitleLabel?.snp.removeConstraints()
        attentionButton?.snp.removeConstraints()
        attentionNumberLabel?.snp.removeConstraints()
        attentionTitleLabel?.snp.removeConstraints()
        fansButton?.snp.removeConstraints()
        fansNumberLabel?.snp.removeConstraints()
        fansTitleLabel?.snp.removeConstraints()
        greenLine?.snp.removeConstraints()
        
        spaceTimeButton?.isHidden = true
        spaceTimeNumberLabel?.isHidden = true
        spaceTimeTitleLabel?.isHidden = true
        recordButton?.isHidden = true
        recordNumberLabel?.isHidden = true
        recordTitleLabel?.isHidden = true
        attentionButton?.isHidden = true
        attentionNumberLabel?.isHidden = true
        attentionTitleLabel?.isHidden = true
        fansButton?.isHidden = true
        fansNumberLabel?.isHidden = true
        fansTitleLabel?.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
