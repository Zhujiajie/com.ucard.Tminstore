//
//  PUserFollowTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PUserFollowTableViewController: UITableViewController {

    weak var viewModel: PUserViewModel?
    weak var userInfoModel: UserInfoModel?
    
    /// 是否是关注列表
    var isFollowVC: Bool = true
    
    /// 选中了cell
    var selectCell: ((_ userInfoModel: UserInfoModel)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(PUserFollowTableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        self.tableView.tableFooterView = UIView()
        
        self.tableView.mj_header = BaseAnimationHeader()
        self.tableView.mj_footer = BaseRefreshFooter()
        
        closures()
    }

    //MARK: 停止刷新
    /// 停止刷新
    func stopRefresh(){
        self.tableView.mj_header.endRefreshing()
        self.tableView.mj_footer.endRefreshing()
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 下拉刷新
        self.tableView.mj_header.refreshingBlock = {[weak self] _ in
            if self?.isFollowVC == true{
                self?.viewModel?.followPageNum = 1
                self?.viewModel?.requestOtherUserFollows(memberCode: (self?.userInfoModel?.memberCode)!)
            }else{
                self?.viewModel?.fansPageNum = 1
                self?.viewModel?.requestOtherUserFans(memberCode: (self?.userInfoModel?.memberCode)!)
            }
        }
        
        //MARK: 上拉刷新
        self.tableView.mj_footer.refreshingBlock = {[weak self] _ in
            if self?.isFollowVC == true{
                self?.viewModel?.requestOtherUserFollows(memberCode: (self?.userInfoModel?.memberCode)!)
            }else{
                self?.viewModel?.requestOtherUserFans(memberCode: (self?.userInfoModel?.memberCode)!)
            }
        }
        
        if isFollowVC {
            //MARK: 成功获得关注数据
            viewModel?.getFollowDataSuccess = {[weak self] _ in
                self?.stopRefresh()
                self?.tableView.reloadData()
            }
            
            //MARK: 没有更多数据
            viewModel?.noMoreFollowsData = {[weak self] _ in
                self?.tableView.mj_header.endRefreshing()
                self?.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
        }else{
            //MARK: 成功获得粉丝数据
            viewModel?.getFansDataSuccess = {[weak self] _ in
                self?.stopRefresh()
                self?.tableView.reloadData()
            }
            
            //MARK: 没有更多粉丝数据了
            viewModel?.noMoreFansData = {[weak self] _ in
                self?.tableView.mj_header.endRefreshing()
                self?.tableView.mj_footer.endRefreshingWithNoMoreData()
            }
        }
        
        //MARK: 关注成功
        viewModel?.followOtherSuccess = {[weak self] (indexPath: IndexPath) in
            if self?.isFollowVC == true{
                self?.viewModel?.followsDataArray[indexPath.row].isFollowed = true
            }else{
                self?.viewModel?.fansDataArray[indexPath.row].isFollowed = true
            }
            self?.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        
        //MARK: 取消关注成功
        viewModel?.cancelFollowOtherSuccess = {[weak self] (indexPath: IndexPath) in
            if self?.isFollowVC == true{
                self?.viewModel?.followsDataArray[indexPath.row].isFollowed = false
            }else{
                self?.viewModel?.fansDataArray[indexPath.row].isFollowed = false
            }
            self?.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        if isFollowVC == true{
            return viewModel!.followsDataArray.count
        }else{
            return viewModel!.fansDataArray.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PUserFollowTableViewCell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! PUserFollowTableViewCell
        if isFollowVC == true{
            cell.userInfoModel = viewModel?.followsDataArray[indexPath.row]
        }else{
            cell.userInfoModel = viewModel?.fansDataArray[indexPath.row]
        }
        
        //MARK: 点击关注按钮的事件
        cell.touchFollowButtonClosure = {[weak self] _ in
            if cell.userInfoModel?.isFollowed == true {
                self?.viewModel?.cancelFollowUser(memberCode: cell.userInfoModel!.memberCode, andIndexPath: indexPath)
            }else{
                self?.viewModel?.followUser(memberCode: cell.userInfoModel!.memberCode, andIndexPath: indexPath)
            }
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60/baseWidth
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if isFollowVC == true{
            selectCell?(viewModel!.followsDataArray[indexPath.row])
        }else{
            selectCell?(viewModel!.fansDataArray[indexPath.row])
        }
    }
}

class PUserFollowTableViewCell: UITableViewCell{
    
    var userInfoModel: UserInfoModel?{
        didSet{
            setData()
        }
    }
    
    /// 点击关注按钮的事件
    var touchFollowButtonClosure: (()->())?
    
    /// 用户头像
    fileprivate var userPortraitView: BaseImageView?
    /// 昵称
    fileprivate var nickNameLabel: BaseLabel?
    /// 关注按钮
    fileprivate var followButton: BaseButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        userPortraitView = BaseImageView.userPortraitView(cornerRadius: 18.5/baseWidth)
        contentView.addSubview(userPortraitView!)
        userPortraitView?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(14/baseWidth)
            make.size.equalTo(CGSize(width: 37/baseWidth, height: 37/baseWidth))
        })
        
        followButton = BaseButton.normalTitleButton(title: NSLocalizedString("关注", comment: ""), andTextFont: UIFont.systemFont(ofSize: 12), andTextColorHexString: "848484", andBorderColorHexString: "C6C6C6", andBorderWidth: 0.5, andCornerRadius: 3)
        contentView.addSubview(followButton!)
        followButton?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(userPortraitView!)
            make.right.equalTo(contentView).offset(-14/baseWidth)
            make.size.equalTo(CGSize(width: 70/baseWidth, height: 26/baseWidth))
        })
        //MARK: 点击关注按钮
        followButton?.touchButtonClosure = {[weak self] _ in
            self?.touchFollowButtonClosure?()
        }
        
        
        nickNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "737373", andNumberOfLines: 1)
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(userPortraitView!)
            make.left.equalTo(userPortraitView!.snp.right).offset(14/baseWidth)
            make.right.equalTo(followButton!.snp.left).offset(-5/baseWidth)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        userPortraitView?.sd_setImage(with: userInfoModel?.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
        nickNameLabel?.text = userInfoModel?.nickName
        setFollowButton()
    }
    
    //MARK: 设置关注按钮
    fileprivate func setFollowButton(){
        
        if userInfoModel?.memberCode == getUserID() {
            followButton?.isHidden = true
        }else{
            followButton?.isHidden = false
        }
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "848484"), NSFontAttributeName: UIFont.systemFont(ofSize: 12)]
        if userInfoModel?.isFollowed == false{
            followButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("加关注", comment: ""), attributes: attDic), for: UIControlState.normal)
        }else if userInfoModel?.isFollowed == true && userInfoModel?.isFans == true{
            followButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("互相关注", comment: ""), attributes: attDic), for: UIControlState.normal)
        }else{
            followButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("取消关注", comment: ""), attributes: attDic), for: UIControlState.normal)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
