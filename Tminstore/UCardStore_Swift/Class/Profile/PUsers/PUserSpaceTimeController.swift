//
//  PUserSpaceTimeController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MapKit
import SnapKit

class PUserSpaceTimeController: BaseViewController {

    ///数据源
    weak var viewModel: PUserViewModel?
    /// 用户信息
    weak var userInfoModel: UserInfoModel?
    
    /// 能否刷新获取数据
    var canRefreshToGetData: Bool = false
    /// 左划刷新
    var headerRefreshClosure: (()->())?
    /// 右划刷新
    var footerRefreshClosure: (()->())?
    
    /// 点击了collectionView
    var didSelecteCell: ((_ indexPath: IndexPath)->())?
    
    /// 地图
    fileprivate var mapView: MKMapView?
    /// collectionView
    fileprivate var collectionView: PUserSpaceTimeCollectionView?
    /// detailCallOutView
    fileprivate var mapLeftView: PMapLeftCalloutAccessoryView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI()
        closures()
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        
        collectionView = PUserSpaceTimeCollectionView()
        view.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(255/baseWidth)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        self.collectionView?.xzm_addNormalHeader(withTarget: self, action: #selector(headerRefresh))
        
        self.collectionView?.xzm_addNormalFooter(withTarget: self, action: #selector(footerRefresh))
        
        self.collectionView?.xzm_header.isUpdatedTimeHidden = true
        
        mapView = MKMapView()
        mapView?.showsUserLocation = false//展示用户点
        mapView?.showsPointsOfInterest = false//不展示景点
        mapView?.showsBuildings = false//不展示建筑物
        //不允许交互
        mapView?.isRotateEnabled = false
        mapView?.isPitchEnabled = false
        mapView?.isZoomEnabled = false
        mapView?.isScrollEnabled = false
        
        mapView?.delegate = self
        
        view.addSubview(mapView!)
        mapView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(collectionView!.snp.top)
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 成功获得时空圈数据
        viewModel?.getSpaceTimeDataSuccess = {[weak self] _ in
            self?.stopRefresh()
            self?.reloadData()
        }
    }
    
    //MARK: 设置mapView的位置
    fileprivate func setMapViewRegin(location: CLLocation, andIndex index: Int){
        //先删除原先的 annotation
        if let annotation: ZTAnnotation = mapView?.annotations.first as? ZTAnnotation{
            mapView?.removeAnnotation(annotation)
        }
        
        //显示精度
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(location.coordinate, span)
        mapView?.setRegion(region, animated: false)
        
        var dateString: String
        if let date: Date = viewModel?.spaceTimeDataArray[index].createDate{
            dateString = getDateText(date: date, andTimeStyle: DateFormatter.Style.none, andDateStyle: DateFormatter.Style.medium)
        }else{
            dateString = "时光记忆"
        }
        
        let annotation: ZTAnnotation = ZTAnnotation(coordinate: location.coordinate, title: dateString, subTitle: nil)
        mapView?.addAnnotation(annotation)
        mapView?.selectAnnotation(annotation, animated: true)
    }
    
    //MARK: 刷新数据
    /// 刷新数据
    func reloadData(){
        collectionView?.reloadData()
        if viewModel != nil || viewModel?.spaceTimeDataArray.isEmpty == false{
            collectionView?.scrollToItem(at: IndexPath.init(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
            setMapViewRegin(location: viewModel!.spaceTimeDataArray.first!.photoLocation!, andIndex: 0)
        }
    }
    
    //MARK: 开始左划刷新
    /// 开始左划刷新
    func headerBeginRefreshing(){
        collectionView?.xzm_header.beginRefreshing()
    }
    
    //MARK: 停止刷新动画
    /// 停止刷新动画
    func stopRefresh(){
        collectionView?.xzm_header.endRefreshing()
        collectionView?.xzm_footer.endRefreshing()
    }
    
    //MARK: 左划刷新
    func headerRefresh(){
        if canRefreshToGetData == false {return}
        viewModel?.spaceTimePageNum = 1
        viewModel?.isNoMoreSpaceTimeData = false
        viewModel?.requestOtherSpaceTimeData(memberCode: userInfoModel!.memberCode)
    }
    
    //MARK: 右划刷新
    func footerRefresh(){
        if canRefreshToGetData == false {return}
        viewModel?.requestOtherSpaceTimeData(memberCode: userInfoModel!.memberCode)
    }
}

extension PUserSpaceTimeController: MKMapViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        return viewModel!.spaceTimeDataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PUserSpaceTimeCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PUserSpaceTimeCollectionCellReuseIdentifier, for: indexPath) as! PUserSpaceTimeCollectionCell
        cell.spaceTimeModel = viewModel!.spaceTimeDataArray[indexPath.item]
        return cell
    }
    
    //MARK: 点击了cell
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        didSelecteCell?(indexPath)
    }
    
    //MARK: 滚动cell
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexPath: IndexPath = IndexPath(item: Int(scrollView.contentOffset.x / screenWidth), section: 0)
        setMapViewRegin(location: viewModel!.spaceTimeDataArray[indexPath.item].photoLocation!, andIndex: indexPath.item)
    }
    
    //MARK: 在地图上添加一个点
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: ZTAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: appReuseIdentifier.annotationReuseIndetifier) as? ZTAnnotationView
        if annotationView == nil {
            annotationView = ZTAnnotationView(annotation: annotation, reuseIdentifier: appReuseIdentifier.annotationReuseIndetifier)
            annotationView?.leftCalloutAccessoryView = nil
            annotationView?.rightCalloutAccessoryView = nil
        }
        annotationView?.image = UIImage(named: "annotationImageView")
        annotationView?.canShowCallout = true
        
        if mapLeftView == nil{
            mapLeftView = PMapLeftCalloutAccessoryView(frame: CGRect(x: 0, y: 0, width: 40/baseWidth, height: 40/baseWidth))
            mapLeftView?.userLogoURL = userInfoModel?.userLogoURL
        }
        annotationView?.leftCalloutAccessoryView = mapLeftView
        
        return annotationView
    }
}
