//
//  PUserCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 在用户信息界面, 承载各种UI的CollectionView

import UIKit

class PUserCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: screenWidth, height: screenHeight - 160/baseWidth)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        self.isPagingEnabled = true
        self.backgroundColor = UIColor.white
        self.isScrollEnabled = false
        
        self.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "0")
        self.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "1")
        self.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "2")
        self.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "3")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
