//
//  PUserViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PUserViewModel: BaseViewModel {

    fileprivate let dataModel: PUserDataModel = PUserDataModel()
    
    /// 成功获得其他用户的信息
    var getOtherUserInfoSuccess: ((_ userInfo: UserInfoModel)->())?
    /// 获取关注或粉丝的信息
    var getFollowOrFansInfo: ((_ userInfo: UserInfoModel)->())?
    
    /// 时光圈页数
    var spaceTimePageNum: Int = 1
    /// 是否已经没有更多时光圈数据了
    var isNoMoreSpaceTimeData: Bool = false
    /// 时光圈数据
    var spaceTimeDataArray: [SpaceTimeDataModel] = [SpaceTimeDataModel]()
    /// 成功获得时光圈数据
    var getSpaceTimeDataSuccess: (()->())?
    /// 返回时光圈数据数量
    var getSpaceTimeDataCount: (()->())?
    /// 关注用户或取消关注用户成功
    var followOrCancelFollowSuccess: (()->())?
    
    /// 关注页数
    var followPageNum: Int = 1
    /// 没有更多关注数据了
    var noMoreFollowsData: (()->())?
    /// 成功获得关注数据
    var getFollowDataSuccess: (()->())?
    /// 获得关注总数
    var getFollowsTotal: ((_ count: Int)->())?
    /// 关注数据数组
    var followsDataArray: [UserInfoModel] = [UserInfoModel]()
    
    /// 粉丝页数
    var fansPageNum: Int = 1
    /// 没有更多粉丝数据了
    var noMoreFansData: (()->())?
    /// 成功获得粉丝数据
    var getFansDataSuccess: (()->())?
    /// 获得粉丝总数
    var getFansTotal: ((_ count: Int)->())?
    /// 粉丝数据数组
    var fansDataArray: [UserInfoModel] = [UserInfoModel]()
    
    /// 好友页数
    var friendsPageNum: Int = 1
    /// 没有更多好友数据了
    var noMoreFriendsData: (()->())?
    /// 成功获得好友数据
    var getFriendsDataSuccess: (()->())?
    /// 好友数据的数组
    var friendDataArray: [UserInfoModel] = [UserInfoModel]()
    
    /// 录屏页数
    var recordPageNum: Int = 1
    /// 没有更多录屏数据了
    var noMoreRecordData: (()->())?
    /// 成功获得录屏数据
    var getRecordDataSuccess: (()->())?
    /// 录屏数据总数
    var getRecordDataTotal: ((_ count: Int)->())?
    /// 录屏数据
    var recordModelArray: [RecordModel] = [RecordModel]()
    
    /// 在关注列表关注其他用户成功
    var followOtherSuccess: ((_ indexPath: IndexPath)->())?
    /// 在关注列表取消关注其他用户成功
    var cancelFollowOtherSuccess: ((_ indexPath: IndexPath)->())?
    
    override init() {
        super.init()
        
        //MARK: 出错
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 完成的闭包
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 获取其他用户信息
    /// 获取其他用户信息
    ///
    /// - Parameter memberCode: 用户的ID
    func requestOtherUserInfo(memberCode: String, requestFollowOrFansInfo boolValue: Bool = false){
        showSVProgress(title: nil)
        dataModel.requestOtherUserInfo(memberCode: memberCode) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    let userInfoModel: UserInfoModel = UserInfoModel.otherUserInfoInitFromDic(dic: data)
                    if userInfoModel.memberCode == getUserID(){
                        userInfoModel.isFans = true
                        userInfoModel.isFollowed = true
                    }
                    if boolValue == false{
                        self?.getOtherUserInfoSuccess?(userInfoModel)
                    }else{
                        self?.getFollowOrFansInfo?(userInfoModel)
                    }
                    return
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 获取时光圈数据
    /// 获取时光圈数据
    ///
    /// - Parameter memberCode: 用户ID
    func requestOtherSpaceTimeData(memberCode: String){
        
        if isNoMoreSpaceTimeData == true{
            getSpaceTimeDataSuccess?()
            return
        }
        
        dataModel.requestOtherUserSpaceTimeData(memberCode: memberCode, andPageNum: spaceTimePageNum) { [weak self] (result: [String: Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if self?.spaceTimePageNum == 1{
                            self?.spaceTimeDataArray.removeAll()
                        }
                        if list.isEmpty {
                            self?.isNoMoreSpaceTimeData = true
                        }else{
                            for dic: [String: Any] in list{
                                let model: SpaceTimeDataModel = SpaceTimeDataModel.initFromDic(dic: dic)
                                if model.frontImageURL != nil && model.videoURL != nil {
                                    self?.spaceTimeDataArray.append(model)
                                }
                            }
                        }
                        self?.getSpaceTimeDataSuccess?()
                        self?.getSpaceTimeDataCount?()
                        self?.spaceTimePageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 关注用户
    /// 关注用户
    ///
    /// - Parameter memberCode: 用户ID
    func followUser(memberCode: String, andIndexPath indexPath: IndexPath? = nil){
        showSVProgress(title: nil)
        dataModel.followUser(memberCode: memberCode) { [weak self] (result: [String : Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if indexPath == nil {
                    self?.followOrCancelFollowSuccess?()
                }else{
                    self?.followOtherSuccess?(indexPath!)
                }
                return
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 取消关注某个用户
    /// 取消关注某个用户
    ///
    /// - Parameter memberCode: 用户ID
    func cancelFollowUser(memberCode: String, andIndexPath indexPath: IndexPath? = nil){
        showSVProgress(title: nil)
        dataModel.cancelFollowUser(memberCode: memberCode) { [weak self] (result: [String : Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if indexPath == nil {
                    self?.followOrCancelFollowSuccess?()
                }else{
                    self?.cancelFollowOtherSuccess?(indexPath!)
                }
                return
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 获取其他用户的关注列表
    /// 获取其他用户的关注列表
    ///
    /// - Parameter memberCode: 用户ID
    func requestOtherUserFollows(memberCode: String){
        dataModel.requestOtherUserFollows(memberCode: memberCode, andPageNum: followPageNum) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let total: Int = data["total"] as? Int{
                        self?.getFollowsTotal?(total)
                    }
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if self?.followPageNum == 1{
                            self?.followsDataArray.removeAll()
                        }
                        if list.isEmpty {
                            self?.noMoreFollowsData?()
                        }else{
                            for dic: [String: Any] in list{
                                let model: UserInfoModel = UserInfoModel.initWithOtherUserDic(dic: dic)
                                self?.followsDataArray.append(model)
                            }
                            self?.getFollowDataSuccess?()
                        }
                        self?.followPageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 获取其他用户的粉丝列表
    /// 获取其他用户的粉丝列表
    ///
    /// - Parameter memberCode: 用户ID
    func requestOtherUserFans(memberCode: String){
        dataModel.requestOtherUserFans(memberCode: memberCode, andPageNum: fansPageNum) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let total: Int = data["total"] as? Int{
                        self?.getFansTotal?(total)
                    }
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if self?.fansPageNum == 1{
                            self?.fansDataArray.removeAll()
                        }
                        if list.isEmpty {
                            self?.noMoreFansData?()
                        }else{
                            for dic: [String: Any] in list{
                                let model: UserInfoModel = UserInfoModel.initWithOtherFansDic(dic: dic)
                                self?.fansDataArray.append(model)
                            }
                            self?.getFansDataSuccess?()
                        }
                        self?.fansPageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 获取用户录屏信息
    /// 获取用户录屏信息
    ///
    /// - Parameter memberCode: 用户ID
    func requestOtherRecordData(memberCode: String){
        dataModel.requestOtherUserRecordInfo(memberCode: memberCode, andPageNum: recordPageNum) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let total: Int = data["total"] as? Int{
                        self?.getRecordDataTotal?(total)
                    }
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if self?.recordPageNum == 1{
                            self?.recordModelArray.removeAll()
                        }
                        if list.isEmpty {
                            self?.noMoreRecordData?()
                        }else{
                            for dic: [String: Any] in list{
                                let model: RecordModel = RecordModel.initFromDic(dic: dic)
                                self?.recordModelArray.append(model)
                            }
                            self?.getRecordDataSuccess?()
                        }
                        self?.recordPageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 查看自己的好友数据
    /// 查看自己的好友数据
    func requsetMyFriendsData(){
        dataModel.requestMyFriendsData(pageIndex: friendsPageNum) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if self?.friendsPageNum == 1{
                            self?.friendDataArray.removeAll()
                        }
                        if list.isEmpty {
                            self?.noMoreFriendsData?()
                        }else{
                            for dic: [String: Any] in list{
                                let model: UserInfoModel = UserInfoModel.initWithOtherUserDic(dic: dic)
                                model.isFollowed = true
                                model.isFans = true
                                self?.friendDataArray.append(model)
                            }
                            self?.getFriendsDataSuccess?()
                        }
                        self?.friendsPageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 查看自己的关注数据
    /// 查看自己的关注数据
    func requestMyFollowData(){
        dataModel.requestMyFollowData(pageIndex: followPageNum) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if self?.followPageNum == 1{
                            self?.followsDataArray.removeAll()
                        }
                        if list.isEmpty {
                            self?.noMoreFollowsData?()
                        }else{
                            for dic: [String: Any] in list{
                                let model: UserInfoModel = UserInfoModel.initWithOtherUserDic(dic: dic)
                                model.isFollowed = true
                                self?.followsDataArray.append(model)
                            }
                            self?.getFollowDataSuccess?()
                        }
                        self?.followPageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 查看自己的粉丝数据
    /// 查看自己的粉丝数据
    func requestMyFansData(){
        dataModel.requestMyFansData(pageIndex: fansPageNum) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if self?.fansPageNum == 1{
                            self?.fansDataArray.removeAll()
                        }
                        if list.isEmpty {
                            self?.noMoreFansData?()
                        }else{
                            for dic: [String: Any] in list{
                                let model: UserInfoModel = UserInfoModel.initWithOtherFansDic(dic: dic)
                                model.isFans = true
                                self?.fansDataArray.append(model)
                            }
                            self?.getFansDataSuccess?()
                        }
                        self?.fansPageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
}
