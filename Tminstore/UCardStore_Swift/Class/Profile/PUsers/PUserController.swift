//
//  PUserController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 展示其他用户个人信息的视图

import UIKit

class PUserController: BaseViewController {

    var userInfoModel: UserInfoModel?
    
    fileprivate let viewModel: PUserViewModel = PUserViewModel()
    
    /// 展示用户信息的视图
    fileprivate var userInfoView: PUserInfoView?
    /// 承载其他UI的collectionView
    fileprivate var collectionView: PUserCollectionView?
    /// 展示时空圈数据的控制器
    fileprivate var spaceTimeController: PUserSpaceTimeController?
    /// 展示关注数据的tableView
    fileprivate var followController: PUserFollowTableViewController?
    /// 展示粉丝数据的tableView
    fileprivate var fansController: PUserFollowTableViewController?
    /// 展示录屏信息的tableView
    fileprivate var recordController: PRecordTableViewController?
    ///播放器
    fileprivate var playerView: PMapPlayerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let whiteView: BaseView = BaseView.colorLine(colorString: "#FFFFFF")
        view.addSubview(whiteView)
        whiteView.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(-navBarHeight)
            make.left.bottom.right.equalTo(view)
        }
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        setNavigationBar()
        setUI()
        closures()
    }    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        requestSpaceTimeData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cancelTansparentNavigationBar()//取消导航栏透明
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        
        setNavigationBarLeftButtonWithImageName("backArrow")
        if userInfoModel?.memberCode != getUserID() {
            setNavigationBarRightButton()
        }
        setNavigationTitle(userInfoModel!.nickName)
    }
    
    //MARK: 设置导航栏右按钮
    fileprivate func setNavigationBarRightButton(){
        if userInfoModel?.isFollowed == true {
            setNavigationBarRightButtonWithImageName("cancelAttention")
        }else{
            setNavigationBarRightButtonWithImageName("payAttention")
        }
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        userInfoView = PUserInfoView()
        view.addSubview(userInfoView!)
        userInfoView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(view)
            make.height.equalTo(111/baseWidth)
        })
        userInfoView?.userInfoModel = userInfoModel
        
        checkAutoLayout()
        
        spaceTimeController = PUserSpaceTimeController()
        spaceTimeController?.viewModel = viewModel
        spaceTimeController?.userInfoModel = userInfoModel
        
        followController = PUserFollowTableViewController()
        followController?.viewModel = viewModel
        followController?.userInfoModel = userInfoModel
        
        fansController = PUserFollowTableViewController()
        fansController?.viewModel = viewModel
        fansController?.userInfoModel = userInfoModel
        fansController?.isFollowVC = false
        
        recordController = PRecordTableViewController()
        recordController?.userViewModel = viewModel
        recordController?.userInfoModel = userInfoModel
        
        collectionView = PUserCollectionView()
        view.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make) in
            make.top.equalTo(userInfoView!.snp.bottom)
            make.left.bottom.right.equalTo(view)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    //MARK: 判断用户信息视图使用哪种UI布局
    fileprivate func checkAutoLayout(){
        if userInfoModel?.isFollowed == true && userInfoModel?.isFans == true{
            userInfoView?.friendUI()
        }else if userInfoModel?.isFollowed == false && userInfoModel?.isFans == false{
            userInfoView?.strangerUI()
        }else{
            userInfoView?.fansAndFollowsUI()
        }
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击了collectionView的Cell
        spaceTimeController?.didSelecteCell = {[weak self] (indexPath: IndexPath) in
            let vc: PNewMapDetailController = PNewMapDetailController()
            vc.mapData = self?.viewModel.spaceTimeDataArray[indexPath.item]
            vc.userInfoModel = self?.userInfoModel
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 点击了userInfoView上的按钮
        userInfoView?.touchButtonClosure = {[weak self] (buttonType: UserInfoButtonType) in
            
            var indexPath: IndexPath
            if self?.userInfoModel?.isFollowed == true && self?.userInfoModel?.isFans == true{
                switch buttonType {
                case UserInfoButtonType.spaceTime:
                    indexPath = IndexPath.init(item: 0, section: 0)
                case UserInfoButtonType.record:
                    indexPath = IndexPath.init(item: 1, section: 0)
                case UserInfoButtonType.follow:
                    indexPath = IndexPath.init(item: 2, section: 0)
                default:
                    indexPath = IndexPath.init(item: 3, section: 0)
                }
            }else{
                switch buttonType {
                case UserInfoButtonType.spaceTime:
                    indexPath = IndexPath.init(item: 0, section: 0)
                case UserInfoButtonType.record:
                    indexPath = IndexPath.init(item: 4, section: 0)
                case UserInfoButtonType.follow:
                    indexPath = IndexPath.init(item: 1, section: 0)
                default:
                    indexPath = IndexPath.init(item: 2, section: 0)
                }
            }
            self?.collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.left, animated: true)
        }
        
        //MARK: 选择了关注列表上的cell
        followController?.selectCell = {[weak self] (userInfoModel: UserInfoModel) in
            self?.viewModel.requestOtherUserInfo(memberCode: userInfoModel.memberCode, requestFollowOrFansInfo: true)
        }
        
        //MARK: 点击播放按钮的事件
        recordController?.touchPlayButtonClosure = {[weak self] (indexPath: IndexPath) in
            self?.setPlayerView(recordModel: (self?.viewModel.recordModelArray[indexPath.row])!)
        }
        
        //MARK: 网络请求出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.spaceTimeController?.stopRefresh()
            self?.followController?.stopRefresh()
            self?.fansController?.stopRefresh()
            self?.recordController?.stopRefresh()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 关注或者取消关注成功
        viewModel.followOrCancelFollowSuccess = {[weak self] _ in
            self?.viewModel.requestOtherUserInfo(memberCode: (self?.userInfoModel?.memberCode)!)
        }
        
        //MARK: 更新用户信息成功
        viewModel.getOtherUserInfoSuccess = {[weak self] (userInfoModel: UserInfoModel) in
            self?.userInfoModel = userInfoModel
            self?.spaceTimeController?.userInfoModel = userInfoModel
            self?.updateUIAfterFollow()
        }
        
        //MARK: 获取时光圈数据成功
        viewModel.getSpaceTimeDataCount = {[weak self] _ in
            self?.userInfoView?.spaceTimeNumber = (self?.viewModel.spaceTimeDataArray.count)!
        }
        
        //MARK: 获取关注数成功
        viewModel.getFollowsTotal = {[weak self] (count: Int) in
            self?.userInfoView?.followNumber = count
        }
        
        //MARK: 获取粉丝数量成功
        viewModel.getFansTotal = {[weak self] (count: Int) in
            self?.userInfoView?.fansNumber = count
        }
        
        //MARK: 获取关注或粉丝的数据成功
        viewModel.getFollowOrFansInfo = {[weak self] (userInfo: UserInfoModel) in
            self?.enterOtherUserVC(userInfoModel: userInfo)
        }
        
        //MARK: 获取录屏数量成功
        viewModel.getRecordDataTotal = {[weak self] (count: Int) in
            self?.userInfoView?.recordNumber = count
        }
    }
    
    //MARK: 关注或者取消关注成功之后, 更新UI
    fileprivate func updateUIAfterFollow(){
        setNavigationBarRightButton()
        userInfoView?.userInfoModel = userInfoModel
        userInfoView?.deleteAutoLayout()
        checkAutoLayout()
        collectionView?.reloadData()
        requestSpaceTimeData()
        
        if userInfoModel?.isFollowed == true {
            alert(alertTitle: NSLocalizedString("关注成功!", comment: ""), messageString: NSLocalizedString("可以在个人界面-关注列表中\n快速查看（她）他的信息", comment: ""), leftButtonTitle: NSLocalizedString("知道了", comment: ""))
        }
    }
    
    //MARK: 点击导航栏右按钮, 进行关注或取消关注操作
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        if userInfoModel?.isFollowed == true {
            alert(alertTitle: NSLocalizedString("确定取消关注?", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("确定", comment: ""), leftClosure: nil, rightClosure: {[weak self] _ in
                self?.viewModel.cancelFollowUser(memberCode: (self?.userInfoModel!.memberCode)!)
            }, presentComplition: nil)
        }else{
            viewModel.followUser(memberCode: userInfoModel!.memberCode)
        }
    }
    
    //MARK: 请求时光圈数据
    fileprivate func requestSpaceTimeData(){
        if userInfoModel?.isFollowed == false && userInfoModel?.isFans == false{
            viewModel.spaceTimeDataArray = userInfoModel!.spaceTimeData
            if viewModel.spaceTimeDataArray.count > 5{
                for _ in 0..<5 {
                    viewModel.spaceTimeDataArray.removeLast()
                }
            }
            userInfoView?.spaceTimeNumber = viewModel.spaceTimeDataArray.count
            spaceTimeController?.canRefreshToGetData = false
            spaceTimeController?.reloadData()
        }else{
            spaceTimeController?.canRefreshToGetData = true
            spaceTimeController?.headerBeginRefreshing()
            viewModel.requestOtherUserFollows(memberCode: userInfoModel!.memberCode)
            viewModel.requestOtherUserFans(memberCode: userInfoModel!.memberCode)
        }
        
        if userInfoModel?.isFollowed == true && userInfoModel?.isFans == true{
            viewModel.requestOtherRecordData(memberCode: userInfoModel!.memberCode)
        }
    }
    
    //MARK: 进入其他用户详细信息界面
    fileprivate func enterOtherUserVC(userInfoModel: UserInfoModel){
        let vc: PUserController = PUserController()
        vc.userInfoModel = userInfoModel
        vc.umengViewName = "其他用户信息"
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayerView(recordModel: RecordModel){
        NotificationCenter.default.post(name:  NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)//停止播放视频
        if playerView == nil {
            playerView = PMapPlayerView()
        }
        playerView?.recordModel = recordModel
        navigationController?.view.addSubview(playerView!)
        playerView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(navigationController!.view)
        })
        playerView?.appearAnimation()
    }
}

extension PUserController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if userInfoModel?.isFollowed == true && userInfoModel?.isFans == true{
            return 4
        }else if userInfoModel?.isFollowed == false && userInfoModel?.isFans == false{
            return 1
        }else{
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(indexPath.item)", for: indexPath)
        
        if userInfoModel?.isFollowed == true && userInfoModel?.isFans == true{
            viewModel.spaceTimeDataArray = userInfoModel!.spaceTimeData
            switch indexPath.item {
            case 0:
                if cell.contentView.subviews.contains(spaceTimeController!.view) == false {
                    cell.contentView.addSubview(spaceTimeController!.view)
                    spaceTimeController?.view.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                }
            case 1:
                if cell.contentView.subviews.contains(recordController!.view) == false {
                    cell.contentView.addSubview(recordController!.view)
                    recordController?.view.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                }
            case 2:
                if cell.contentView.subviews.contains(followController!.view) == false{
                    cell.contentView.addSubview(followController!.view)
                    followController!.view.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                }
            case 3:
                if cell.contentView.subviews.contains(fansController!.view) == false{
                    cell.contentView.addSubview(fansController!.view)
                    fansController!.view.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                }
            default:
                break
            }
        }else{
            switch indexPath.item {
            case 0:
                if cell.contentView.subviews.contains(spaceTimeController!.view) == false {
                    cell.contentView.addSubview(spaceTimeController!.view)
                    spaceTimeController?.view.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                }
            case 1:
                if cell.contentView.subviews.contains(followController!.view) == false{
                    cell.contentView.addSubview(followController!.view)
                    followController!.view.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                }
            case 2:
                if cell.contentView.subviews.contains(fansController!.view) == false{
                    cell.contentView.addSubview(fansController!.view)
                    fansController!.view.snp.makeConstraints({ (make) in
                        make.edges.equalTo(cell.contentView)
                    })
                }
            default:
                break
            }
        }
        return cell
    }
    
    //MARK: 滚动cell
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index: Int = Int(scrollView.contentOffset.x / screenWidth)
        var buttonType: UserInfoButtonType
        
        if userInfoModel?.isFollowed == true && userInfoModel?.isFans == true{
            switch index {
            case 0:
                buttonType = UserInfoButtonType.spaceTime
            case 1:
                buttonType = UserInfoButtonType.record
            case 2:
                buttonType = UserInfoButtonType.follow
            default:
                buttonType = UserInfoButtonType.fans
            }
        }else{
            switch index {
            case 0:
                buttonType = UserInfoButtonType.spaceTime
            case 1:
                buttonType = UserInfoButtonType.follow
            case 2:
                buttonType = UserInfoButtonType.fans
            default:
                buttonType = UserInfoButtonType.record
            }
        }
        userInfoView?.selectButton(buttonType: buttonType)
    }
}

