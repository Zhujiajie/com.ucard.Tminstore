//
//  PUserOtherController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class PUserOtherController: BaseViewController {
    
    fileprivate let viewModel: PUserViewModel = PUserViewModel()
    
    /// 控制器要展示的数据类型
    var type: UserOtherInfoType = UserOtherInfoType.friends
    
    /// 展示数据的tableView
    fileprivate var tableView: PUserTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        tableView = PUserTableViewController()
        tableView?.type = type
        tableView?.viewModel = viewModel
        view.addSubview(tableView!.view)
        tableView?.view.snp.makeConstraints({ (make) in
            make.edges.equalTo(view)
        })
        
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //获取新数据
        if viewModel.friendDataArray.isEmpty == true && viewModel.followsDataArray.isEmpty == true && viewModel.fansDataArray.isEmpty == true {
            tableView?.beginRefresh()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationBarLeftButtonWithImageName("backArrow")
        
        switch type {
        case UserOtherInfoType.friends:
            setNavigationTitle("我的好友")
        case UserOtherInfoType.follows:
            setNavigationTitle("我的关注")
        default:
            setNavigationTitle("我的粉丝")
        }
    }

    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.tableView?.stopRefresh()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击了Cell
        tableView?.selectCell = {[weak self] (userInfoModel: UserInfoModel) in
            let vc: PUserController = PUserController()
            vc.userInfoModel = userInfoModel
            vc.umengViewName = "其他用户信息"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
    
}
