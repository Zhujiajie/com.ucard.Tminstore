//
//  POrderDetailController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class POrderDetailController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    var order: OrderPostcardModel?
    
    /// 删除订单成功
    var deleteOrderSuccess: (()->())?
    
    fileprivate let orderViewModel: PDetialViewModel = PDetialViewModel()
    fileprivate let purchaseViewModel: StorePurchaseViewModel = StorePurchaseViewModel()
    
    fileprivate var tableView: POrderDetailTableView?
    fileprivate var supportView: POrderDetailSupportView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        if order?.status != OrderStaus.paid{
            supportView = POrderDetailSupportView()
            view.addSubview(supportView!)
            supportView?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.left.bottom.right.equalTo(view)
            })
        }
        
        tableView = POrderDetailTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            if order?.status != OrderStaus.paid{
                make.top.left.right.equalTo(view)
                make.bottom.equalTo(supportView!.snp.top)
            }else{
                make.edges.equalTo(view)
            }
        })
        tableView?.delegate = self
        tableView?.dataSource = self
        
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)//发送通知，暂停播放视频
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("订单详情", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 回调
    fileprivate func closures(){
        
        //MARK: 点击删除按钮的事件
        supportView?.touchDeleteButtonClosure = {[weak self] _ in
            self?.deleteAlert(alertTitle: NSLocalizedString("确认删除？", comment: ""), rightClosure: {
                self?.orderViewModel.deleteOrder(paymentOrderNo: (self?.order?.paymentOrderNo)!, andIndexPath: nil)
            }, presentCompletion: nil)
        }
        
        //MARK: 二次支付
        supportView?.touchPayButtonClosure = {[weak self] _ in
            self?.purchaseViewModel.payAgain(payStyle: (self?.order?.payChannel)!, andOrderNO: (self?.order?.paymentOrderNo)!, andMailOrderNo: (self?.order?.mailOrderNo)!)
        }
        
        //MARK: 删除订单成功
        orderViewModel.deleteSuccess = {[weak self] (indexPath: IndexPath?) in
            self?.alert(alertTitle: NSLocalizedString("删除成功", comment: ""), leftClosure: { 
                self?.deleteOrderSuccess?()
                _ = self?.navigationController?.popViewController(animated: true)
            })
        }
        
        //MARK: 删除失败
        orderViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 支付成功
        purchaseViewModel.uploadOrderSuccess = {[weak self] _ in
            let vc: MUPaySuccessController = MUPaySuccessController()
            let payModel: PayModel = PayModel()
            payModel.orderNo = (self?.order?.paymentOrderNo)!
            payModel.payStyle = (self?.order?.payChannel)!
            vc.order = self?.order
            vc.payModel = payModel
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 二次支付出错的闭包
        purchaseViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: POrderDetailContentCell = tableView.dequeueReusableCell(withIdentifier: "0", for: indexPath) as! POrderDetailContentCell
            cell.order = order
            return cell
        case 1:
            let cell: PorderDetailAddressCell = tableView.dequeueReusableCell(withIdentifier: "1", for: indexPath) as! PorderDetailAddressCell
            cell.addressModel = order?.addressModel
            return cell
        case 2:
            let cell: POrderDetailPriceCell = tableView.dequeueReusableCell(withIdentifier: "2", for: indexPath) as! POrderDetailPriceCell
            cell.order = order
            return cell
        default:
            let cell: POrderDetailPurchaseCell = tableView.dequeueReusableCell(withIdentifier: "3", for: indexPath) as! POrderDetailPurchaseCell
            cell.order = order
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 300/baseWidth
        case 1:
            return 120
        case 2:
            return 190
        default:
            return 140
        }
        
    }
}
