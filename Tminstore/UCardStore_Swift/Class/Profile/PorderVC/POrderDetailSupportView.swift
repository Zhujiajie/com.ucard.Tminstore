//
//  POrderDetailSupportView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/16.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class POrderDetailSupportView: BaseView {

    /// 点击删除按钮
    var touchDeleteButtonClosure: (()->())?
    
    /// 点击支付按钮
    var touchPayButtonClosure: (()->())?
    
    /// 删除按钮
    fileprivate var deleteButton: BaseButton?
    /// 支付按钮
    fileprivate var payButton: BaseButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(hexString: "#F9F9F9")
        
        payButton = BaseButton.normalTitleButton(title: NSLocalizedString("去支付", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "4CDFED", andBorderColorHexString: "4CDFED", andBorderWidth: 1, andCornerRadius: 1)
        addSubview(payButton!)
        payButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(10/baseWidth)
            make.right.equalTo(self).offset(-20/baseWidth)
            make.bottom.equalTo(self).offset(-10/baseWidth)
            make.size.equalTo(CGSize(width: 100/baseWidth, height: 34/baseWidth))
        })
        
        payButton?.touchButtonClosure = {[weak self] _ in
            self?.touchPayButtonClosure?()
        }
        
        deleteButton = BaseButton.normalTitleButton(title: NSLocalizedString("删除", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "4CDFED", andBorderColorHexString: "4CDFED", andBorderWidth: 1, andCornerRadius: 1)
        addSubview(deleteButton!)
        deleteButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(payButton!)
            make.right.equalTo(payButton!.snp.left).offset(-20/baseWidth)
            make.bottomMargin.equalTo(payButton!)
            make.size.equalTo(payButton!)
        })
        
        deleteButton?.touchButtonClosure = {[weak self] _ in
            self?.touchDeleteButtonClosure?()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
