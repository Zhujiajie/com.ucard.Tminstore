//
//  POrderViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class POrderViewModel: BaseViewModel {

    fileprivate let dataModel: POrderDataModel = POrderDataModel()
    
    /// 成功获得数据
    var tableViewShouldReloadData: (()->())?
    /// 没有更多数据了
    var noMoreData: (()->())?
    /// 数据源
    var orderArray: [OrderPostcardModel] = [OrderPostcardModel]()
    /// 页数
    var pageIndex: Int = 1
    
    override init() {
        super.init()
        
        //MARK: 请求出错
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 请求完成
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 请求订单信息
    /// 请求订单信息
    func requestOrderData(){
        
        dataModel.requestOrderInfo(pageNum: pageIndex) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let list: [[String: Any]] = result["result"] as? [[String: Any]]{
                    if list.count == 1{
                        self?.noMoreData?()
                    }else{
                        if self?.pageIndex == 1{
                            self?.orderArray.removeAll()
                        }
                        for i in 1..<list.count{
                            if let mailOrder: [String: Any] = list[i]["mailOrder"] as? [String: Any]{
                                let orderModel: OrderPostcardModel = OrderPostcardModel.initFromDic(dic: mailOrder)
                                if let moneyPrice: Double = list[i]["moneyPrice"] as? Double{
                                    orderModel.imagePrice = moneyPrice
                                }
                                if let mailMethod: String = list[i]["mailMethod"] as? String, mailMethod == "01"{
                                    orderModel.useExpress = true
                                }
                                if let postAddress: [String: Any] = list[i]["postAddress"] as? [String: Any]{
                                    orderModel.addressModel = AddressModel.initFromOrderJSON(dic: postAddress)
                                    if let storeGoods: [String: Any] = list[i]["storeGoods"] as? [String: Any]{
                                        orderModel.goodsModel = GoodsModel.initFromJson(dic: storeGoods)
                                    }
                                }
                                self?.orderArray.append(orderModel)
                            }
                        }
                        self?.tableViewShouldReloadData?()
                    }
                    self?.pageIndex += 1
                    return
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
}
