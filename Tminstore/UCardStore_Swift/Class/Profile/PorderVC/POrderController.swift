//
//  POrderController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class POrderController: BaseViewController {

    fileprivate let viewModel: POrderViewModel = POrderViewModel()
    fileprivate let orderViewModel: PDetialViewModel = PDetialViewModel()
    fileprivate var tableView: POrderTableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if viewModel.orderArray.isEmpty{
            tableView?.mj_header.beginRefreshing()
        }
        interactive.pan?.isEnabled = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactive.pan?.isEnabled = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("订单", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        tableView = POrderTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    //MARK: 回调
    fileprivate func closures(){
        
        //MARK: 下拉刷新
        tableView?.mj_header.refreshingBlock = {[weak self] _ in
            self?.viewModel.pageIndex = 1
            self?.viewModel.requestOrderData()
        }
        
        //MARK: 上拉刷新
        tableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.requestOrderData()
        }
        
        //MARK: 成功获得数据
        viewModel.tableViewShouldReloadData = {[weak self] _ in
            self?.tableView?.mj_header.endRefreshing()
            self?.tableView?.mj_footer.endRefreshing()
            self?.tableView?.reloadData()
        }
        
        //MARK: 没有更多数据了
        viewModel.noMoreData = {[weak self] _ in
            self?.tableView?.mj_header.endRefreshing()
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.tableView?.mj_header.endRefreshing()
            self?.tableView?.mj_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 删除订单成功
        orderViewModel.deleteSuccess = {[weak self] (indexPath: IndexPath?) in
            self?.viewModel.orderArray.remove(at: indexPath!.row)
            self?.tableView?.deleteRows(at: [indexPath!], with: UITableViewRowAnimation.automatic)
        }
        
        //MARK: 删除失败
        orderViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
}

extension POrderController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.orderArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: POrderTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.POrderTableViewCellReuseIdentifier, for: indexPath) as! POrderTableViewCell
        cell.order = viewModel.orderArray[indexPath.row]
        //MARK: 点击删除按钮的事件
        cell.touchDeleteButtonClosure = {[weak self] _ in
            self?.deleteAlert(alertTitle: NSLocalizedString("确认删除？", comment: ""), rightClosure: { 
                self?.orderViewModel.deleteOrder(paymentOrderNo: (self?.viewModel.orderArray[indexPath.row].paymentOrderNo)!, andIndexPath: indexPath)
            }, presentCompletion: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc: POrderDetailController = POrderDetailController()
        vc.order = viewModel.orderArray[indexPath.row]
        _ = navigationController?.pushViewController(vc, animated: true)
        //MARK: 成功删除订单
        vc.deleteOrderSuccess = {[weak self] _ in
            self?.tableView?.mj_header.beginRefreshing()
        }
    }
}
