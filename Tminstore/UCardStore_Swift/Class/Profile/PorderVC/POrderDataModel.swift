//
//  POrderDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class POrderDataModel: BaseDataModel {

    //MARK: 获取订单信息
    /// 获取订单信息
    ///
    /// - Parameters:
    ///   - pageNum: 页数
    ///   - success: 成功的闭包
    func requestOrderInfo(pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlStr: String = appAPIHelp.checkOrderAPI + "\(pageNum)"
        
        let parameter: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameter, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
