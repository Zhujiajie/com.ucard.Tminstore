//
//  POrderDetailCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

//MARK: 展示视频
class POrderDetailCollectionViewVideoCell: UICollectionViewCell {
    
    /// 视频ID
    var videoId: String?{
        didSet{
            if videoId != "" && videoId != nil{
                player?.stop()
                player?.setVideoWithVideoID(videoID: videoId!, shouldSaveVideo: true)
                player?.view.isHidden = false
                activeIndicator?.startAnimating()
            }
        }
    }
    
    /// 视频地址
    var videoURL: URL?{
        didSet{
            if videoURL != nil{
                player?.stop()
                player?.setVideoFullURL(fullVideoURL: videoURL!, shouldSaveVideo: true)
                player?.view.isHidden = false
                activeIndicator?.startAnimating()
            }
        }
    }
    
    fileprivate var titleLabel: BaseLabel?
    fileprivate var player: ZTPlayer?
    fileprivate var playAgainButton: BaseButton?
    fileprivate var activeIndicator: UIActivityIndicatorView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "A7A7A7", andText: NSLocalizedString("视频", comment: ""))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(16/baseWidth)
            make.centerX.equalTo(contentView)
        })
        
        player = ZTPlayer()
        player?.view.frame = CGRect(x: 26/baseWidth, y: 39/baseWidth, width: screenWidth - 52/baseWidth, height: 229/baseWidth)
        player?.playbackLoops = true
        player?.playerDelegate = self
        player?.playbackDelegate = self
        contentView.addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            if self?.videoId != nil || self?.videoURL != nil{
                self?.playAgainButton?.isHidden = false
            }
        }
        
        //重新开始播放的按钮
        playAgainButton = BaseButton.normalIconButton(iconName: "playButton")
        playAgainButton?.touchButtonClosure = {[weak self] _ in
            self?.playAgainButton?.isHidden = true
            self?.player?.playFromCurrentTime()
        }
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 45/baseWidth, height: 45/baseWidth))
        })
        playAgainButton?.isHidden = true
        
        activeIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        contentView.addSubview(activeIndicator!)
        activeIndicator?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view)
        })
        activeIndicator?.hidesWhenStopped = true
        
        //注册通知，暂停视频播放
        NotificationCenter.default.addObserver(self, selector: #selector(stopVideoPlayNotification(info:)), name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)
    }
    
    //MARK: 播放视频
    /// 播放视频
    func playVideo(){
        player?.playFromCurrentTime()
    }
    
    //MARK: 暂停视频
    /// 暂停视频
    ///
    /// - Parameter hidePlayButton: 是否需要显示播放按钮
    func pausePlayer(hidePlayButton: Bool = false){
        playAgainButton?.isHidden = hidePlayButton
        player?.pause()
    }
    
    //MARK: 接受通知，暂停视频播放
    func stopVideoPlayNotification(info: Notification){
        if player?.playbackState == PlaybackState.playing{
            pausePlayer()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        videoId = nil
        videoURL = nil
        playAgainButton?.isHidden = true
        player?.stop()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
extension POrderDetailCollectionViewVideoCell: PlayerDelegate, PlayerPlaybackDelegate{
    func playerReady(_ player: Player) {
    }
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    //MARK: 根据缓存情况，展示加载提示器
    func playerBufferingStateDidChange(_ player: Player) {
        switch player.bufferingState {
        case BufferingState.ready:
            activeIndicator?.stopAnimating()
        default:
            activeIndicator?.startAnimating()
        }
    }
    
    func playerCurrentTimeDidChange(_ player: Player){
    }
    func playerPlaybackWillStartFromBeginning(_ player: Player){
    }
    func playerPlaybackDidEnd(_ player: Player){
    }
    func playerPlaybackWillLoop(_ player: Player){
    }
}

//MARK: 展示图片
class POrderDetailCollectionViewImageCell: UICollectionViewCell {
    
    /// 标题文字
    var titleString: String = ""{
        didSet{
            titleLabel?.text = titleString
        }
    }
    
    /// 占位图片的路径
    var placeholerImageURL: URL?
    
    /// 图片路径
    var imageURL: URL?{
        didSet{
            //先判断是否已缓存图片
            SDWebImageManager.shared().cachedImageExists(for: imageURL, completion: { [weak self] (result: Bool) in
                if result == true{
                    self?.imageView?.sd_setImage(with: self?.imageURL)
                }else{
                    self?.imageView?.sd_setImage(with: self?.placeholerImageURL, completed: { [weak self](image: UIImage?, _, _, _) in
                        if image != nil {
                        self?.imageView?.sd_setImage(with: self?.imageURL, placeholderImage: image)
                        }else{
                            self?.imageView?.sd_setImage(with: self?.imageURL, placeholderImage: nil)
                        }
                    })
                }
            })
        }
    }
    
    ///是否展示阴影
    var showShadow: Bool = false{
        didSet{
            shadosView?.isHidden = !showShadow
        }
    }
    
    fileprivate var titleLabel: BaseLabel?
    fileprivate var imageView: BaseImageView?
    fileprivate var shadosView: BaseView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "A7A7A7")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(16/baseWidth)
            make.centerX.equalTo(contentView)
        })
        
        shadosView = BaseView.shadowView()
        contentView.addSubview(shadosView!)
        shadosView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(6/baseWidth)
            make.left.equalTo(contentView).offset(26/baseWidth)
            make.bottom.equalTo(contentView).offset(-5/baseWidth)
            make.right.equalTo(contentView).offset(-26/baseWidth)
        })
        shadosView?.isHidden = true
        
        //图片
        imageView = BaseImageView.normalImageView()
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(6/baseWidth)
            make.left.equalTo(contentView).offset(26/baseWidth)
            make.bottom.equalTo(contentView).offset(-5/baseWidth)
            make.right.equalTo(contentView).offset(-26/baseWidth)
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
        imageURL = nil
        placeholerImageURL = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示明信片背面文字
class POrderDetailCollectionViewTextCell: UICollectionViewCell {
    
    var order: OrderPostcardModel?{
        didSet{
            textView?.text = order?.backContent
        }
    }
    
    fileprivate var titleLabel: BaseLabel?
    fileprivate var imageView: BaseImageView?
    fileprivate var textView: BaseTextView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "A7A7A7", andText: NSLocalizedString("明信片背面", comment: ""))
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(16/baseWidth)
            make.centerX.equalTo(contentView)
        })
        
        //图片
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleToFill)
        imageView?.backgroundColor = UIColor.white
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(6/baseWidth)
            make.left.equalTo(contentView).offset(26/baseWidth)
            make.bottom.equalTo(contentView).offset(-5/baseWidth)
            make.right.equalTo(contentView).offset(-26/baseWidth)
        })
        imageView?.sd_setImage(with: appAPIHelp.postcardOrderBackImageURL)
        imageView?.setShadow()
        
        
        textView = BaseTextView.normalTextView(font: UIFont.systemFont(ofSize: 12))
        imageView?.addSubview(textView!)
        textView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(imageView!).offset(41/baseWidth)
            make.left.equalTo(imageView!).offset(13/baseWidth)
            make.bottom.equalTo(imageView!).offset(-30/baseWidth)
            make.right.equalTo(imageView!).offset(-63/baseWidth)
        })
        textView?.isEditable = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

