//
//  POrderDetailTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class POrderDetailTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        estimatedRowHeight = 304/baseWidth
        
        register(POrderDetailContentCell.self, forCellReuseIdentifier: "0")
        register(PorderDetailAddressCell.self, forCellReuseIdentifier: "1")
        register(POrderDetailPriceCell.self, forCellReuseIdentifier: "2")
        register(POrderDetailPurchaseCell.self, forCellReuseIdentifier: "3")
        separatorStyle = UITableViewCellSeparatorStyle.none
        tableFooterView = UIView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class POrderDetailContentCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource{
    
    var order: OrderPostcardModel?{
        didSet{
            collectionView?.reloadData()
        }
    }
    
    fileprivate var collectionView: POrderDetailCollectionView?
    fileprivate var pageControl: UIPageControl?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        collectionView = POrderDetailCollectionView()
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(275/baseWidth)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        pageControl = UIPageControl()
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "60DBE8")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D6D6D6")
        contentView.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom)
            make.centerX.equalTo(contentView)
            make.bottom.equalTo(contentView)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if order == nil {
            pageControl?.numberOfPages = 0
            return 0
        }else{
            pageControl?.numberOfPages = 3
            return 3
        }
    }
    
    //MARK: 设置cell的内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.row {
        case 0:
            let cell: POrderDetailCollectionViewImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.POrderDetailCollectionViewImageCellReuseIdentifier, for: indexPath) as! POrderDetailCollectionViewImageCell
            var url: URL?
            var thumbnailURL: URL?
            if order?.storePictures != nil{
                url = order?.storePictures
                thumbnailURL = order?.storePicturesThumbnail
            }else{
                url = order?.frontImageURL
                thumbnailURL = order?.frontPlaceImageURL
            }
            cell.placeholerImageURL = thumbnailURL
            cell.imageURL = url
            if order?.goodsModel.goodsType == CustomizationType.postcard{
                cell.titleString = NSLocalizedString("AR明信片定制", comment: "")
            }else{
                cell.titleString = NSLocalizedString("实物", comment: "")
            }
            return cell
        case 1:
            if order?.goodsModel.goodsType == CustomizationType.postcard{
                if order?.backImageURL != nil{
                    let cell: POrderDetailCollectionViewImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.POrderDetailCollectionViewImageCellReuseIdentifier, for: indexPath) as! POrderDetailCollectionViewImageCell
                    cell.titleString = NSLocalizedString("明信片背面", comment: "")
                    cell.imageURL = order?.backImageURL
                    cell.showShadow = true
                    return cell
                }else{
                    let cell: POrderDetailCollectionViewTextCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.POrderDetailCollectionViewTextCellReuseIdentifier, for: indexPath) as! POrderDetailCollectionViewTextCell
                    cell.order = order
                    return cell
                }
            }else{
                let cell: POrderDetailCollectionViewImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.POrderDetailCollectionViewImageCellReuseIdentifier, for: indexPath) as! POrderDetailCollectionViewImageCell
                cell.titleString = NSLocalizedString("图片", comment: "")
                cell.placeholerImageURL = order?.frontPlaceImageURL
                cell.imageURL = order?.frontImageURL
                return cell
            }
        default:
            let cell: POrderDetailCollectionViewVideoCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.POrderDetailCollectionViewVideoCellReuseIdentifier, for: indexPath) as! POrderDetailCollectionViewVideoCell
            cell.videoId = order?.videoId
            cell.videoURL = order?.videoURL
            return cell
        }
    }
    
    //MARK: 不显示Cell时。需要暂停播放视频
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell: POrderDetailCollectionViewVideoCell = cell as? POrderDetailCollectionViewVideoCell{
            cell.pausePlayer(hidePlayButton: true)
        }
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == screenWidth * 2{
            if let cell: POrderDetailCollectionViewVideoCell = collectionView?.cellForItem(at: IndexPath(item: 2, section: 0)) as? POrderDetailCollectionViewVideoCell{
                cell.playVideo()
            }
        }
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示地址的cell
class PorderDetailAddressCell: UITableViewCell{
    
    /// 数据源
    var addressModel: AddressModel?{
        didSet{
            setData()
        }
    }
    
    /// 收件人姓名label
    fileprivate var postNameLabel: BaseLabel?
    /// 手机号码的label
    fileprivate var phoneNumberLabel: BaseLabel?
    /// 收货地址的label
    fileprivate var detailAddressLabel: BaseLabel?
    /// 邮编的label
    fileprivate var postcodeLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F6F6F6")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(2)
        }
        
        let greenView: BaseView = BaseView.colorLine(colorString: "4CDFED")
        contentView.addSubview(greenView)
        greenView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(grayLine.snp.bottom)
            make.left.bottom.equalTo(contentView)
            make.size.equalTo(CGSize(width: 61/baseWidth, height: 115/baseWidth))
        }
        
        let locationIcon: BaseImageView = BaseImageView.normalImageView(andImageName: "mapLocationIcon")
        contentView.addSubview(locationIcon)
        locationIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(greenView)
            make.size.equalTo(CGSize(width: 35/baseWidth, height: 35/baseWidth))
        }
        
        phoneNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "535353", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(phoneNumberLabel!)
        phoneNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(8/baseWidth)
            make.right.equalTo(contentView).offset(-15/baseWidth)
        })
        
        postNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "535353")
        contentView.addSubview(postNameLabel!)
        postNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(phoneNumberLabel!)
            make.left.equalTo(greenView.snp.right).offset(13/baseWidth)
            make.right.equalTo(phoneNumberLabel!.snp.left).offset(-5/baseWidth)
        })
        
        detailAddressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "535353", andNumberOfLines: 2)
        contentView.addSubview(detailAddressLabel!)
        detailAddressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(postNameLabel!.snp.bottom).offset(8/baseWidth)
            make.leftMargin.equalTo(postNameLabel!)
            make.rightMargin.equalTo(phoneNumberLabel!.snp.right)
        })
        
        postcodeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "535353")
        contentView.addSubview(postcodeLabel!)
        postcodeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(postNameLabel!)
            make.rightMargin.equalTo(phoneNumberLabel!)
            make.bottom.equalTo(contentView).offset(-15/baseWidth)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        phoneNumberLabel?.text = addressModel?.phoneNumber
        postNameLabel?.text = NSLocalizedString("收货人: ", comment: "") + addressModel!.mailName
        detailAddressLabel?.text = NSLocalizedString("收货地址: ", comment: "") + addressModel!.country + addressModel!.province + addressModel!.city + addressModel!.district + addressModel!.detailedAddress
        postcodeLabel?.text = NSLocalizedString("邮编: ", comment: "") + addressModel!.postcode
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示价格详情的cell
class POrderDetailPriceCell: UITableViewCell{
    
    var order: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 时光密码的label
    fileprivate var arCodeLabel: BaseLabel?
    fileprivate var arCodeDetailLabel: BaseLabel?
    
    /// 实物价格的label
    fileprivate var goodsLabel: BaseLabel?
    fileprivate var goodsDetailLabel: BaseLabel?
    
    /// 图片价格的Label
    fileprivate var imagePriceLabel: BaseLabel?
    fileprivate var imagePriceDetailLabel: BaseLabel?
    
    /// 快递费的Label
    fileprivate var expressPriceLabel: BaseLabel?
    fileprivate var expressPriceDetailLabel: BaseLabel?
    
    /// 订单数量的label
    fileprivate var orderNumberLabel: BaseLabel?
    fileprivate var orderNumberDetailLabel: BaseLabel?
    
    /// 订单总价的Label
    fileprivate var totalAmountLabel: BaseLabel?
    fileprivate var totalAmountDetailLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F6F6F6")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(2)
        }

        arCodeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "6B6B6B", andText: NSLocalizedString("时光密码", comment: ""))
        contentView.addSubview(arCodeLabel!)
        arCodeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayLine.snp.bottom).offset(13/baseWidth)
            make.left.equalTo(contentView).offset(19/baseWidth)
        })
        
        arCodeDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "4CDFED", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(arCodeDetailLabel!)
        arCodeDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(arCodeLabel!)
            make.right.equalTo(contentView).offset(-19/baseWidth)
        })
        
        goodsLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "B7B7B7", andText: NSLocalizedString("实物价格", comment: ""))
        contentView.addSubview(goodsLabel!)
        goodsLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(arCodeLabel!.snp.bottom).offset(7.3/baseWidth)
            make.leftMargin.equalTo(arCodeLabel!)
        })
        
        goodsDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "B7B7B7")
        contentView.addSubview(goodsDetailLabel!)
        goodsDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(goodsLabel!)
            make.rightMargin.equalTo(arCodeDetailLabel!)
        })
        
        imagePriceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "B7B7B7", andText: NSLocalizedString("图片价格", comment: ""))
        contentView.addSubview(imagePriceLabel!)
        imagePriceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(goodsLabel!.snp.bottom).offset(7.3/baseWidth)
            make.leftMargin.equalTo(arCodeLabel!)
        })
        
        imagePriceDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "B7B7B7")
        contentView.addSubview(imagePriceDetailLabel!)
        imagePriceDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(imagePriceLabel!)
            make.rightMargin.equalTo(arCodeDetailLabel!)
        })
        
        expressPriceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "B7B7B7", andText: NSLocalizedString("快递费", comment: ""))
        contentView.addSubview(expressPriceLabel!)
        expressPriceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(imagePriceLabel!.snp.bottom).offset(7.3/baseWidth)
            make.leftMargin.equalTo(arCodeLabel!)
        })
        
        expressPriceDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "B7B7B7")
        contentView.addSubview(expressPriceDetailLabel!)
        expressPriceDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(expressPriceLabel!)
            make.rightMargin.equalTo(arCodeDetailLabel!)
        })
        
        orderNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "B7B7B7", andText: NSLocalizedString("数量", comment: ""))
        contentView.addSubview(orderNumberLabel!)
        orderNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(expressPriceLabel!.snp.bottom).offset(7.3/baseWidth)
            make.leftMargin.equalTo(arCodeLabel!)
        })
        
        orderNumberDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "B7B7B7")
        contentView.addSubview(orderNumberDetailLabel!)
        orderNumberDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(orderNumberLabel!)
            make.rightMargin.equalTo(arCodeDetailLabel!)
        })
        
        totalAmountLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "6B6B6B", andText: NSLocalizedString("订单总价", comment: ""))
        contentView.addSubview(totalAmountLabel!)
        totalAmountLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(orderNumberLabel!.snp.bottom).offset(10.3/baseWidth)
            make.leftMargin.equalTo(arCodeLabel!)
            make.bottom.equalTo(contentView).offset(-13.3/baseWidth)
        })
        
        totalAmountDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "6B6B6B")
        contentView.addSubview(totalAmountDetailLabel!)
        totalAmountDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(totalAmountLabel!)
            make.rightMargin.equalTo(arCodeDetailLabel!)
            make.bottomMargin.equalTo(totalAmountLabel!)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        arCodeDetailLabel?.text = order!.arCode
        goodsDetailLabel?.text = "¥\(order!.goodsModel.goodsPrice)"
        imagePriceDetailLabel?.text = "¥\(order!.imagePrice)"
        if order?.useExpress == true{
            expressPriceDetailLabel?.text = "¥15"
        }else{
            expressPriceDetailLabel?.text = "¥0"
        }
        orderNumberDetailLabel?.text = "x\(order!.buyTotal)"
        totalAmountDetailLabel?.text = "¥ \(order!.orderAmount)"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示支付详情的cell
class POrderDetailPurchaseCell: UITableViewCell{
    
    var order: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 支付状态的label
    fileprivate var purchaseStatusLabel: BaseLabel?
    fileprivate var purchaseStatusDetailLabel: BaseLabel?
    
    /// 支付方式的label
    fileprivate var purchaseMethodLabel: BaseLabel?
    fileprivate var purchaseMethodDetailLabel: BaseLabel?
    
    /// 邮寄方式的label
    fileprivate var expressMethodLabel: BaseLabel?
    fileprivate var expressMethodDetailLabel: BaseLabel?
    
    /// 下单时间的label
    fileprivate var createDateLabel: BaseLabel?
    fileprivate var createDateDetailLabel: BaseLabel?
    
    /// 订单编号的label
    fileprivate var orderNumberLabel: BaseLabel?
    fileprivate var orderNumberDetailLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F6F6F6")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(2)
        }
        
        purchaseStatusLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383", andText: NSLocalizedString("支付状态", comment: ""))
        contentView.addSubview(purchaseStatusLabel!)
        purchaseStatusLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayLine.snp.bottom).offset(18/baseWidth)
            make.left.equalTo(contentView).offset(19/baseWidth)
        })
        
        purchaseStatusDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383")
        contentView.addSubview(purchaseStatusDetailLabel!)
        purchaseStatusDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(purchaseStatusLabel!)
            make.right.equalTo(contentView).offset(-19/baseWidth)
        })
        
        purchaseMethodLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383", andText: NSLocalizedString("支付方式", comment: ""))
        contentView.addSubview(purchaseMethodLabel!)
        purchaseMethodLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(purchaseStatusLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(purchaseStatusLabel!)
        })
        
        purchaseMethodDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383")
        contentView.addSubview(purchaseMethodDetailLabel!)
        purchaseMethodDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(purchaseMethodLabel!)
            make.rightMargin.equalTo(purchaseStatusDetailLabel!)
        })
        
        expressMethodLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383", andText: NSLocalizedString("邮寄方式", comment: ""))
        contentView.addSubview(expressMethodLabel!)
        expressMethodLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(purchaseMethodLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(purchaseStatusLabel!)
        })
        
        expressMethodDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383")
        contentView.addSubview(expressMethodDetailLabel!)
        expressMethodDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(expressMethodLabel!)
            make.rightMargin.equalTo(purchaseStatusDetailLabel!)
        })
        
        createDateLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383", andText: NSLocalizedString("下单时间", comment: ""))
        contentView.addSubview(createDateLabel!)
        createDateLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(expressMethodLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(purchaseStatusLabel!)
        })
        
        createDateDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383")
        contentView.addSubview(createDateDetailLabel!)
        createDateDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(createDateLabel!)
            make.rightMargin.equalTo(purchaseStatusDetailLabel!)
        })
        
        orderNumberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383", andText: NSLocalizedString("订单编号", comment: ""))
        contentView.addSubview(orderNumberLabel!)
        orderNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(createDateLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(purchaseStatusLabel!)
        })
        
        orderNumberDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "838383")
        contentView.addSubview(orderNumberDetailLabel!)
        orderNumberDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(orderNumberLabel!)
            make.rightMargin.equalTo(purchaseStatusDetailLabel!)
            make.bottomMargin.equalTo(orderNumberLabel!)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        switch order!.status {
        case OrderStaus.paid:
            purchaseStatusDetailLabel?.text = NSLocalizedString("已支付", comment: "")
        case OrderStaus.received:
            purchaseStatusDetailLabel?.text = NSLocalizedString("已签收", comment: "")
        case OrderStaus.send:
            purchaseStatusDetailLabel?.text = NSLocalizedString("已发货", comment: "")
        default:
            purchaseStatusDetailLabel?.text = NSLocalizedString("未支付", comment: "")
        }
        
        switch order!.payChannel {
        case PayStyle.coupon:
            purchaseMethodDetailLabel?.text = NSLocalizedString("优惠券支付", comment: "")
        case PayStyle.aliPay:
            purchaseMethodDetailLabel?.text = NSLocalizedString("支付宝支付", comment: "")
        case PayStyle.weChat:
            purchaseMethodDetailLabel?.text = NSLocalizedString("微信支付", comment: "")
        default:
            purchaseMethodDetailLabel?.text = NSLocalizedString("未知", comment: "")
        }
        
        if order?.goodsModel.goodsType != CustomizationType.postcard{
            expressMethodDetailLabel?.text = NSLocalizedString("快递", comment: "")
        }else{
            if order?.useExpress == true{
                expressMethodDetailLabel?.text = NSLocalizedString("快递", comment: "")
            }else{
                expressMethodDetailLabel?.text = NSLocalizedString("平邮", comment: "")
            }
        }
        
        if let date: Date = order?.createDate{
            createDateDetailLabel?.text = getDateText(date: date, andTimeStyle: DateFormatter.Style.medium, andDateStyle: DateFormatter.Style.short)
        }
        
        orderNumberDetailLabel?.text = order?.mailOrderNo
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
