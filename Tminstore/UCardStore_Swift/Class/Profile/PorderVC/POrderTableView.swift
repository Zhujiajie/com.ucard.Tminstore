//
//  POrderTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import MJRefresh
import SDWebImage

class POrderTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        register(POrderTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.POrderTableViewCellReuseIdentifier)
        estimatedRowHeight = 144/baseWidth
        mj_header = BaseAnimationHeader()
        mj_footer = BaseRefreshFooter()
        
        tableFooterView = UIView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class POrderTableViewCell: UITableViewCell{
    
    /// 点击删除按钮的事件
    var touchDeleteButtonClosure: (()->())?
    
    /// 订单信息
    var order: OrderPostcardModel?{
        didSet{
            if order != nil{
                setData()
            }
        }
    }
    
    /// 预览图片
    fileprivate var previewImageView: BaseImageView?
    /// 订单title
    fileprivate var titleLabel: BaseLabel?
    /// 价格Label
    fileprivate var priceLabel: BaseLabel?
    /// 价格详情的label
    fileprivate var priceDetailLabel: BaseLabel?
    /// 数量Label
    fileprivate var numberLabel: BaseLabel?
    /// 数量详情的label
    fileprivate var numberDetailLabel: BaseLabel?
    /// 支付状态Label
    fileprivate var payStatusLabel: BaseLabel?
    /// 支付状态详情的label
    fileprivate var payStatusDetailLabel: BaseLabel?
    /// 删除按钮
    fileprivate var deleteButton: BaseButton?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        previewImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentView.addSubview(previewImageView!)
        previewImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(10/baseWidth)
            make.left.equalTo(contentView).offset(10/baseWidth)
            make.bottom.equalTo(contentView).offset(-10/baseWidth)
            make.size.equalTo(CGSize(width: 130/baseWidth, height: 130/baseWidth))
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "404040")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(previewImageView!)
            make.left.equalTo(previewImageView!.snp.right).offset(6/baseWidth)
            make.right.equalTo(contentView).offset(-19/baseWidth)
        })
        
        priceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "878686", andText: NSLocalizedString("价格", comment: ""))
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(7/baseWidth)
            make.leftMargin.equalTo(titleLabel!)
            make.width.equalTo(117/baseWidth)
        })
        
        priceDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "878686", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(priceDetailLabel!)
        priceDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(priceLabel!)
            make.left.equalTo(priceLabel!.snp.right).offset(10/baseWidth)
            make.rightMargin.equalTo(titleLabel!)
        })
        
        numberLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "878686", andText: NSLocalizedString("数量", comment: ""))
        contentView.addSubview(numberLabel!)
        numberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(priceLabel!.snp.bottom).offset(6.5/baseWidth)
            make.leftMargin.equalTo(titleLabel!)
            make.width.equalTo(priceLabel!)
        })
        
        numberDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "878686", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(numberDetailLabel!)
        numberDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(numberLabel!)
            make.leftMargin.equalTo(priceDetailLabel!)
            make.rightMargin.equalTo(priceDetailLabel!)
        })
        
        payStatusLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "878686", andText: NSLocalizedString("支付状态", comment: ""))
        contentView.addSubview(payStatusLabel!)
        payStatusLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(numberLabel!.snp.bottom).offset(6.5/baseWidth)
            make.leftMargin.equalTo(priceLabel!)
            make.rightMargin.equalTo(priceLabel!)
        })
        
        payStatusDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "878686", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(payStatusDetailLabel!)
        payStatusDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(payStatusLabel!)
            make.leftMargin.equalTo(priceDetailLabel!)
            make.rightMargin.equalTo(priceDetailLabel!)
        })
        
        deleteButton = BaseButton.normalTitleButton(title: NSLocalizedString("删除", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "4CDFED", andBackgroundColorHexString: "#FFFFFF", andBorderColorHexString: "4CDFED", andBorderWidth: 1, andCornerRadius: 1)
        contentView.addSubview(deleteButton!)
        deleteButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(contentView).offset(-9/baseWidth)
            make.rightMargin.equalTo(titleLabel!)
            make.size.equalTo(CGSize(width: 99/baseWidth, height: 28/baseWidth))
        })
        //MARK: 点击删除按钮的事件
        deleteButton?.touchButtonClosure = {[weak self] _ in
            self?.touchDeleteButtonClosure?()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        var url: URL?
        var thumbnailURL: URL?
        if order?.storePictures != nil{
            url = order?.storePictures
            thumbnailURL = order?.storePicturesThumbnail
        }else{
            url = order?.frontImageURL
            thumbnailURL = order?.frontPlaceImageURL
        }
        
        //先判断是否已缓存图片
        SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
            if result == true{
                self?.previewImageView?.sd_setImage(with: url)
            }else{
                self?.previewImageView?.sd_setImage(with: thumbnailURL, completed: { [weak self](image: UIImage?, _, _, _) in
                    if image != nil{
                        self?.previewImageView?.sd_setImage(with: url, placeholderImage: image)
                    }else{
                        self?.previewImageView?.sd_setImage(with: url, placeholderImage: nil)
                    }
                })
            }
        })
        
        if let goodsName: String = order?.goodsModel.goodsName{
            titleLabel?.text = goodsName
        }else{
            titleLabel?.text = NSLocalizedString("AR明信片定制", comment: "")
        }
        
        priceDetailLabel?.text = "¥ \(order!.orderAmount)"
        if order?.buyTotal == 0{
            numberDetailLabel?.text = "x 1"
        }else{
            numberDetailLabel?.text = "x \(order!.buyTotal)"
        }
        
        switch order!.status {
        case OrderStaus.paid:
            payStatusDetailLabel?.text = NSLocalizedString("已支付", comment: "")
        case OrderStaus.received:
            payStatusDetailLabel?.text = NSLocalizedString("已签收", comment: "")
        case OrderStaus.send:
            payStatusDetailLabel?.text = NSLocalizedString("已发货", comment: "")
        default:
            payStatusDetailLabel?.text = NSLocalizedString("未支付", comment: "")
        }
        deleteButton?.isHidden = order?.status != OrderStaus.unPaied
    }
}
