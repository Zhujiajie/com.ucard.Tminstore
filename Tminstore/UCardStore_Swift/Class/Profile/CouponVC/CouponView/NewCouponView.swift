//
//  NewCouponView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class NewCouponView: UIView {

    /// 领取新用户优惠券
    var getNewCoupon: (()->())?
    /// 退出的闭包
    var dismissClosure: (()->())?
    
    /// 背景黑色视图
    fileprivate var blackBackgroundView: UIView?
    /// 红包icon
    fileprivate var newRedView: UIImageView?
    /// 承载其他UI的容器视图
    fileprivate var containerView: UIView?
    /// 点击领取优惠券的按钮
    fileprivate var getCouponButton: UIButton?
    /// 退出的按钮
    fileprivate var dismissButton: UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        self.alpha = 0
        setUI()
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 1
            }, completion: {[weak self] (_) in
                self?.appearAnimation()
        })
    }
    
    //MARK: 设置黑色背景
    fileprivate func setUI(){
        
        blackBackgroundView = UIView()
        blackBackgroundView?.backgroundColor = UIColor.black
        blackBackgroundView?.alpha = 0.49
        self.addSubview(blackBackgroundView!)
        blackBackgroundView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        
        containerView = UIView()
        containerView?.backgroundColor = UIColor.white
        containerView?.layer.cornerRadius = 14
        containerView?.layer.masksToBounds = true
        self.addSubview(containerView!)
        containerView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-screenHeight)
            make.size.equalTo(CGSize(width: 276/baseWidth, height: 254/baseWidth))
        }
        
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "4CDFED")
        label.font = UIFont.systemFont(ofSize: 18)
        label.text = NSLocalizedString("恭喜您获得", comment: "")
        label.textAlignment = NSTextAlignment.center
        containerView?.addSubview(label)
        label.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(containerView!).offset(37/baseWidth)
            make.centerX.equalTo(containerView!)
            make.height.equalTo(25)
        }
        
        let greenLine1: UIView = greenLine()
        containerView?.addSubview(greenLine1)
        greenLine1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(containerView!).offset(47/baseWidth)
            make.right.equalTo(label.snp.left).offset(-9/baseWidth)
            make.size.equalTo(CGSize(width: 130/baseWidth, height: 1))
        }
        
        let greenLine2: UIView = greenLine()
        containerView?.addSubview(greenLine2)
        greenLine2.snp.makeConstraints { (make: ConstraintMaker) in
            make.topMargin.equalTo(greenLine1)
            make.left.equalTo(label.snp.right).offset(9/baseWidth)
            make.size.equalTo(greenLine1)
        }
        
        let couponImageView: UIImageView = UIImageView(image: UIImage(named: "newCoupon"))
        couponImageView.contentMode = UIViewContentMode.scaleAspectFit
        containerView?.addSubview(couponImageView)
        couponImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(label.snp.bottom).offset(26/baseWidth)
            make.centerX.equalTo(containerView!)
            make.size.equalTo(CGSize(width: 244/baseWidth, height: 100/baseWidth))
        }
        
        getCouponButton = UIButton()
        getCouponButton?.backgroundColor = UIColor(hexString: "4CDFED")
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium)
        } else {
            font = UIFont.systemFont(ofSize: 18)
        }
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: font]
        getCouponButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("点击领取", comment: ""), attributes: attDic), for: UIControlState.normal)
        getCouponButton?.addTarget(self, action: #selector(touchGetCouponButton(button:)), for: UIControlEvents.touchUpInside)
        
        containerView?.addSubview(getCouponButton!)
        getCouponButton?.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(containerView!)
            make.height.equalTo(49/baseWidth)
        }
        
        dismissButton = UIButton()
        dismissButton?.setImage(UIImage(named: "dismissIntrol"), for: UIControlState.normal)
        self.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(self).offset(-screenHeight)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        }
        dismissButton?.addTarget(self, action: #selector(touchDismissButton(button:)), for: UIControlEvents.touchUpInside)
        
        newRedView = UIImageView(image: UIImage(named: "newCouponRed"))
        newRedView?.contentMode = UIViewContentMode.scaleAspectFit
        self.addSubview(newRedView!)
        newRedView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(containerView!).offset(-227/baseWidth)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 73/baseWidth, height: 48/baseWidth))
        })
    }
    
    //MARK: 动画
    fileprivate func appearAnimation(){
        dismissButton?.snp.updateConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(self).offset(-262/baseHeight)
        }
        autoLayoutAnimation(view: self, withDelay: 0.0)
        containerView?.snp.updateConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self).offset(-327/baseHeight)
        })
        autoLayoutAnimation(view: self, withDelay: 0.2)
    }
    
    fileprivate func greenLine()->UIView{
        let greenLine: UIView = UIView()
        greenLine.backgroundColor = UIColor(hexString: "4CDFED")
        return greenLine
    }
    
    //MARK: 点击按钮领取优惠券
    func touchGetCouponButton(button: UIButton) {
        dismissSelf()
        getNewCoupon?()
    }
    
    //MARK: 点击了退出按钮
    func touchDismissButton(button: UIButton){
        dismissSelf()
    }
    
    //MARK: 退出
    fileprivate func dismissSelf(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 0
        }) { [weak self] (_) in
            self?.removeFromSuperview()
            self?.dismissClosure?()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
