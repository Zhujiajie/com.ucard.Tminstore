//
//  MUCheckCodeView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/16.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 操作支付密码时, 获取验证码的视图

import UIKit

class MUCheckCodeView: UIView {

    weak var viewModel: MUPaymentManagementViewModel?{
        didSet{
            if viewModel != nil {
                
                //MARK: 点击获取验证码按钮的事件
                getCodeButton?.touchButtonClosure = {[weak self] _ in
                    self?.getCodeButton?.startTimer()
                    self?.viewModel?.getVerificationCode(phone: (self?.viewModel?.userInfo?.phoneNumber)!)
                }
                
                //MARK: 点击确定按钮, 验证验证码
                confirmCodeButton?.touchButtonClosure = {[weak self] _ in
                    self?.viewModel?.checkVerificationCode(phone: (self?.viewModel?.userInfo?.phoneNumber)!, andCode: (self?.codeTextField?.text)!)
                }
                
                //MARK: 获取验证码失败
                viewModel?.getVerificationFailed = {[weak self] _ in
                    self?.getCodeButton?.fireTimer()
                }
            }
        }
    }
    
    /// 点击确认按钮的事件
    var touchConformButtonClosure: (()->())?
    
    /// 点击获取验证码按钮的事件
    var touchGetCodeButtonClosure: (()->())?
    
    /// 输入验证码的输入框
    fileprivate var codeTextField: BaseTextField?
    /// 获取验证码的按钮
    fileprivate var getCodeButton: MUGetCodeButton?
    /// 确认验证码的按钮
    fileprivate var confirmCodeButton: BaseButton?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
        } else {
            font = UIFont.systemFont(ofSize: 14)
        }
        
        codeTextField = BaseTextField.numberTextField(font: font, andTextColorHexString: "#2D2D2D", andPlaceHolder: NSLocalizedString("输入验证码", comment: ""))
        codeTextField?.borderStyle = UITextBorderStyle.roundedRect
        addSubview(codeTextField!)
        codeTextField?.snp.makeConstraints({ (make) in
            make.top.equalTo(self).offset(34/baseWidth)
            make.left.equalTo(self).offset(29/baseWidth)
            make.size.equalTo(CGSize(width: 195/baseWidth, height: 46/baseWidth))
        })
        
        getCodeButton = MUGetCodeButton()
        addSubview(getCodeButton!)
        getCodeButton?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(codeTextField!)
            make.left.equalTo(codeTextField!.snp.right).offset(13/baseWidth)
            make.right.equalTo(self).offset(-29/baseWidth)
            make.height.equalTo(codeTextField!)
        })
//        //MARK: 点击获取验证码的事件
//        getCodeButton?.touchButtonClosure = {[weak self] _ in
//            self?.touchGetCodeButtonClosure?()
//        }
        
        confirmCodeButton = BaseButton.normalTitleButton(title: NSLocalizedString("确定", comment: ""), andTextFont: font, andTextColorHexString: "434343", andBackgroundColorHexString: "D8D8D8", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: 3)
        addSubview(confirmCodeButton!)
        confirmCodeButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(codeTextField!.snp.bottom).offset(35/baseWidth)
            make.centerX.equalTo(self)
            make.leftMargin.equalTo(codeTextField!)
            make.rightMargin.equalTo(getCodeButton!)
            make.height.equalTo(42/baseWidth)
        })
        confirmCodeButton?.isEnabled = false//按钮暂时不可点击

//        //MARK: 点击确认按钮的事件
//        confirmCodeButton?.touchButtonClosure = {[weak self] _ in
//            self?.touchConformButtonClosure?()
//        }
        
        //MARK: 输入了验证码之后, 确认按钮变为可点击
        codeTextField?.didEndEditing = {[weak self] (code: String) in
            self?.confirmButtonIsEnable()
        }
    }
    
    //MARK: 确认验证码的按钮状态变为可用
    fileprivate func confirmButtonIsEnable(){
        confirmCodeButton?.backgroundColor = UIColor(hexString: "505050")
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
        } else {
            font = UIFont.systemFont(ofSize: 14)
        }
        let attDic: [String: Any] = [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.white]
        confirmCodeButton?.setAttributedTitle(NSAttributedString.init(string: NSLocalizedString("确认", comment: ""), attributes: attDic), for: UIControlState.normal)
        confirmCodeButton?.isEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
