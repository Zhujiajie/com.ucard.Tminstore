//
//  MUSetPaymentPasswordSuccessView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/16.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 设置支付密码成功之后的toast

import UIKit

class MUSetPaymentPasswordSuccessView: UIView {

    /// 消失时的closure
    var dismissClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(white: 0.0, alpha: 0.57)
        
        let whiteView: BaseView = BaseView.colorLine(colorString: "FFFFFF")
        addSubview(whiteView)
        whiteView.snp.makeConstraints { (make) in
            make.center.equalTo(self)
            make.size.equalTo(CGSize(width: 162/baseWidth, height: 167/baseWidth))
        }
        whiteView.layer.cornerRadius = 7
        whiteView.layer.masksToBounds = true
        
        let imageView: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "setPaymentPasswordSuccess")
        whiteView.addSubview(imageView)
        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(whiteView).offset(25/baseWidth)
            make.centerX.equalTo(whiteView)
            make.size.equalTo(CGSize(width: 92/baseWidth, height: 92/baseWidth))
        }
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
        } else {
            font = UIFont.systemFont(ofSize: 14)
        }
        
        let label: BaseLabel = BaseLabel.normalLabel(font: font, andTextColorHex: "8E8E8E", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("设置成功", comment: ""))
        whiteView.addSubview(label)
        label.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(12/baseWidth)
            make.centerX.equalTo(whiteView)
        }
        
        self.transform.scaledBy(x: 0.1, y: 0.1)
        showAnimation()
        //存活两秒钟
        delay(seconds: 2, completion: { [weak self] _ in
            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { 
                self?.alpha = 0
            }, completion: { (_) in
                self?.dismissClosure?()
            })
        }, completionBackground: nil)
    }
    
    //MARK: 出现的动画
    fileprivate func showAnimation(){
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
