//
//  MUCouponButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUCouponButton: UIButton {

    var touchButtonClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setImage(UIImage(named: "couponSupportButton"), for: UIControlState.normal)
        
        let str: String    = NSLocalizedString("优惠券说明", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "76DAD9")] as [String: Any]
        let attStr:NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        
        setAttributedTitle(attStr, for: UIControlState.normal)
        addTarget(self, action: #selector(touchCouponSupportButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: 点击优惠券说明按钮的事件
    func touchCouponSupportButton(button: UIButton){
        touchButtonClosure?()
    }
}
