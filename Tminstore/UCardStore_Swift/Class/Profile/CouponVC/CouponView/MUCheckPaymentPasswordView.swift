//
//  MUCheckPaymentPasswordView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/16.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 输入支付密码的视图, 点击确认按钮进行设置, 重置或修改支付密码

import UIKit

class MUCheckPaymentPasswordView: UIView {

    weak var viewModel: MUPaymentManagementViewModel?
    
    /// 密码二次输入不匹配
    var passwordError: (()->())?
    
    /// 是否是设置支付密码, 还是重置支付密码
    var isSetPaymentPassword: Bool = true
    
    /// 输入密码的title
    fileprivate var titleLable: BaseLabel?
    /// 输入密码的textField
    fileprivate var textField: BaseTextField?
    /// 副标题
    fileprivate var subTitleLabel: BaseLabel?
    /// 确认的按钮
    fileprivate var confirmButton: BaseButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLable = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "484848", andText: NSLocalizedString("请再次输入以确认", comment: ""))
        addSubview(titleLable!)
        titleLable?.snp.makeConstraints({ (make) in
            make.left.equalTo(self).offset(30/baseWidth)
            make.top.equalTo(self).offset(21/baseWidth)
        })
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
        } else {
            font = UIFont.systemFont(ofSize: 14)
        }
        textField = BaseTextField.passwordTextField(font: font, andTextColorHexString: nil, andPlaceHolder: NSLocalizedString("请输入6-16位密码", comment: ""))
        textField?.borderStyle = UITextBorderStyle.roundedRect
        addSubview(textField!)
        textField?.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLable!.snp.bottom).offset(13/baseWidth)
            make.leftMargin.equalTo(titleLable!)
            make.right.equalTo(self).offset(-30/baseWidth)
            make.height.equalTo(46/baseWidth)
        })
        
        subTitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "818181", andText: NSLocalizedString("请包含数字和大小写字母", comment: ""))
        addSubview(subTitleLabel!)
        subTitleLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(textField!.snp.bottom).offset(7/baseWidth)
            make.leftMargin.equalTo(titleLable!)
        })
        subTitleLabel?.isHidden = true//不需要这个UI, 隐藏
        
        confirmButton = BaseButton.normalTitleButton(title: NSLocalizedString("确认", comment: ""), andTextFont: font, andTextColorHexString: "434343", andBackgroundColorHexString: "D8D8D8", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: 3)
        addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make) in
            make.leftMargin.equalTo(titleLable!)
            make.rightMargin.equalTo(textField!)
            make.top.equalTo(subTitleLabel!.snp.bottom).offset(24/baseWidth)
            make.height.equalTo(42/baseWidth)
        })
        confirmButton?.isEnabled = false//按钮暂时不可用
        
        //MARK: 输入支付密码之后确认按钮变为可用
        textField?.didEndEditing = {[weak self] (code: String) in
            self?.confirmButtonIsEnable()
        }
        
        //MARK: 点击确认支付密码按钮的事件
        confirmButton?.touchButtonClosure = {[weak self] _ in
            //先判断两次输入的密码是否相同
            if self?.textField?.text == self?.viewModel?.password {
                if self?.isSetPaymentPassword == true {
                    self?.viewModel?.setPaymentPassword()
                }else{
                    self?.viewModel?.resetPaymentPassword()
                }
            }else{
                self?.passwordError?()
            }
        }
    }
    
    //MARK: 下一步的按钮状态变为可用
    fileprivate func confirmButtonIsEnable(){
        confirmButton?.backgroundColor = UIColor(hexString: "505050")
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
        } else {
            font = UIFont.systemFont(ofSize: 14)
        }
        let attDic: [String: Any] = [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor.white]
        confirmButton?.setAttributedTitle(NSAttributedString.init(string: NSLocalizedString("确认", comment: ""), attributes: attDic), for: UIControlState.normal)
        confirmButton?.isEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
