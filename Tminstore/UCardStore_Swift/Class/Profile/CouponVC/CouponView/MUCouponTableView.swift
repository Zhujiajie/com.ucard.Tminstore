//
//  MUCouponTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import MJRefresh

class MUCouponTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        register(MUCouponActivityTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MUCouponActivityTableViewCellReuseIdentifier)
        register(MUCouponTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MUCouponTableViewCellReuseIdentifier)
        register(MUGetCouponTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MUGetCouponTableViewCellReuseIdentifier)
        backgroundColor = UIColor(hexString: "#F1F1F1")
        
        estimatedRowHeight = 145
        separatorStyle = UITableViewCellSeparatorStyle.none
//        contentInset = UIEdgeInsets(top: -30, left: 0, bottom: 0, right: 0)
        
        mj_footer = BaseRefreshFooter()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
