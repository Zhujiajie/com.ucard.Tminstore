//
//  MUCouponTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:------------优惠券的tableviewCell-------------------
import UIKit
import SnapKit

fileprivate let kAlphaNum: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

//MARK: 对换优惠券的Cell
class MUGetCouponTableViewCell: UITableViewCell, UITextFieldDelegate {
    
    var touchExchangeButton: ((_ couponCode: String)->())?
    
    /// 输入优惠券码的textField
    fileprivate var couponTextField: UITextField?
    
    /// 点击确认兑换优惠码
    fileprivate var exchangeButton: BaseButton?
    
//    /// 展示优惠券图片的imageView
//    fileprivate var couponImageView: UIImageView?
//    
//    /// 优惠券说明Label
//    fileprivate var supporLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.white
        selectionStyle = UITableViewCellSelectionStyle.none
        setTextFieldCell()
    }
    
    //MARK: 设置输入优惠券码的textField
    func setTextFieldCell() {
        
        couponTextField = UITextField()
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "CDCDCD"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)] as [String: Any]
        let attPlaceHolder: NSAttributedString = NSAttributedString(string: NSLocalizedString("请输入优惠码", comment: ""), attributes: attDic)
        
        couponTextField?.attributedPlaceholder = attPlaceHolder
        
        couponTextField?.font = UIFont.systemFont(ofSize: 14)
        contentView.addSubview(couponTextField!)
        couponTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(33/baseWidth)
            make.left.equalTo(contentView).offset(26/baseWidth)
            make.bottom.equalTo(contentView).offset(-33/baseWidth)
            make.right.equalTo(contentView).offset(-92/baseWidth)
            make.height.equalTo(42)
        })
        
        couponTextField?.layer.borderColor = UIColor(hexString: "#CECECE").cgColor
        couponTextField?.layer.borderWidth = 1
        couponTextField?.layer.cornerRadius = 4
        couponTextField?.layer.masksToBounds = true
        
        //文字向右移动一点距离
        couponTextField?.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 11/baseWidth, height: 10))
        couponTextField?.leftViewMode = UITextFieldViewMode.always
        couponTextField?.keyboardType = UIKeyboardType.asciiCapable
        couponTextField?.autocorrectionType = UITextAutocorrectionType.no//取消自动更正
        couponTextField?.autocapitalizationType = UITextAutocapitalizationType.allCharacters//总是大写
        couponTextField?.delegate = self
        
        exchangeButton = BaseButton.normalTitleButton(title: NSLocalizedString("兑换", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "FFFFFF", andBackgroundColorHexString: "52E0ED", andCornerRadius: 4)
        contentView.addSubview(exchangeButton!)
        exchangeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(couponTextField!)
            make.left.equalTo(couponTextField!.snp.right).offset(8/baseWidth)
            make.bottomMargin.equalTo(couponTextField!)
            make.right.equalTo(contentView).offset(-26/baseWidth)
        })
        
        //MARK: 点击兑换的事件
        exchangeButton?.touchButtonClosure = {[weak self] _ in
            self?.couponTextField?.endEditing(true)
            self?.couponTextField?.resignFirstResponder()//退出编辑
            if let text: String = self?.couponTextField?.text, text != ""{
                let noNilCode: String = text.replacingOccurrences(of: " ", with: "")//删除空格字符
                self?.touchExchangeButton?(noNilCode)
                self?.couponTextField?.text = nil
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let cs: CharacterSet = CharacterSet(charactersIn: kAlphaNum).inverted
        let filterString: String = string.components(separatedBy: cs).joined(separator: "")
        
        return string == filterString
    }
}

//MARK: 展示普通优惠券的cell
class MUCouponTableViewCell: UITableViewCell{
    
    var couponModel: CouponModel?{
        didSet{
            setData()
        }
    }
    
    /// 展示优惠券图片的imageView
    fileprivate var couponImageView: BaseImageView?
    /// 优惠券名字的label
    fileprivate var couponTitleLabel: BaseLabel?
    /// 优惠券说明Label
    fileprivate var supporLabel: BaseLabel?
    /// 价格单位的label
    fileprivate var priceUnitLabel: BaseLabel?
    /// 价格金额的label
    fileprivate var priceDetailLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.white
        selectionStyle = UITableViewCellSelectionStyle.none
        
        setCouponCell()
    }
    
    //MARK: 设置优惠券cell
    func setCouponCell(){
        // 优惠券的图片
        couponImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleToFill)
        couponImageView?.sd_setImage(with: appAPIHelp.couponImageURL)
        contentView.addSubview(couponImageView!)
        
        couponImageView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(12/baseWidth)
            make.bottom.equalTo(contentView).offset(-12/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 323/baseWidth, height: 123/baseWidth))
        }
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 36, weight: UIFontWeightSemibold)
        } else {
            font = UIFont.systemFont(ofSize: 36)
        }
        
        couponTitleLabel = BaseLabel.normalLabel(font: font, andTextColorHex: "FFFFFF", andTextAlignment: NSTextAlignment.center, andText: NSLocalizedString("代金券", comment: ""))
        couponImageView?.addSubview(couponTitleLabel!)
        couponTitleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(couponImageView!).offset(46/baseWidth)
            make.left.equalTo(couponImageView!)
            make.right.equalTo(couponImageView!).offset(-169/baseWidth)
        })
        
        //对优惠券的详细说明
        supporLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "C4C4C4")
        couponImageView?.addSubview(supporLabel!)
        supporLabel?.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.equalTo(couponImageView!).offset(9/baseWidth)
            make.right.equalTo(couponImageView!).offset(-9/baseWidth)
            make.bottom.equalTo(couponImageView!).offset(-2/baseHeight)
        }
        
        //折扣金额单位
        priceUnitLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 24), andTextColorHex: "FFFFFF", andText: "¥")
        couponImageView!.addSubview(priceUnitLabel!)
        priceUnitLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(couponImageView!).offset(61/baseWidth)
            make.left.equalTo(couponImageView!).offset(181/baseWidth)
        })
        
        //折扣金额详情的label
        priceDetailLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 64), andTextColorHex: "FFFFFF")
        couponImageView?.addSubview(priceDetailLabel!)
        priceDetailLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(couponImageView!).offset(27/baseWidth)
            make.left.equalTo(couponImageView!).offset(206/baseWidth)
        })
    }
    
    //MARK: 设置优惠券数据
    fileprivate func setData(){
//        couponTitleLabel?.text = couponModel?.couponName
        priceDetailLabel?.text = "\(Int(couponModel!.couponPromotion))"
        
        if couponModel?.couponStartTime != nil && couponModel?.couponEndTime != nil{
            let dateString1: String = getDateText(date: couponModel!.couponStartTime!, andTimeStyle: DateFormatter.Style.none, andDateStyle: DateFormatter.Style.medium)
            let dateString2: String = getDateText(date: couponModel!.couponEndTime!, andTimeStyle: DateFormatter.Style.none, andDateStyle: DateFormatter.Style.medium)
            
            if languageCode() == "CN" {
                supporLabel?.text = "限" + dateString1 + "至" + dateString2
            }else{
                supporLabel?.text = "from" + dateString1 + "to" + dateString2
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 展示活动优惠券的cell
class MUCouponActivityTableViewCell: UITableViewCell{
    /// 展示优惠券图片的imageView
    fileprivate var couponImageView: UIImageView?
//    
//    /// 优惠券说明Label
//    fileprivate var supporLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.white
        selectionStyle = UITableViewCellSelectionStyle.none
        
        setCouponCell()
    }
    
    //MARK: 设置优惠券cell
    func setCouponCell(){
        // 优惠券的图片
        couponImageView = UIImageView(image: UIImage(named: "activityCoupon"))
        
        couponImageView?.contentMode = UIViewContentMode.scaleAspectFit
        couponImageView?.clipsToBounds = true
        contentView.addSubview(couponImageView!)
        
        couponImageView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(11/baseHeight)
            make.left.equalTo(contentView).offset(11/baseWidth)
            make.bottom.equalTo(contentView).offset(-7/baseHeight)
            make.right.equalTo(contentView).offset(-11/baseWidth)
        }
        
//        //对优惠券的详细说明
//        supporLabel = UILabel()
//        supporLabel?.textColor = UIColor(hexString: "#CFCFCF")
//        supporLabel?.font = UIFont.systemFont(ofSize: 12)
//        supporLabel?.text = NSLocalizedString("一张优惠券只能使用一次", comment: "")
//        couponImageView?.addSubview(supporLabel!)
//        supporLabel?.snp.makeConstraints { (make: ConstraintMaker) in
//            if screenWidth >= 414 {
//                make.left.equalTo(couponImageView!).offset(40/baseWidth)
//            }else{
//                make.left.equalTo(couponImageView!).offset(20/baseWidth)
//            }
//            
//            make.bottom.equalTo(couponImageView!).offset(-7/baseHeight)
//        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
