//
//  MUGetCodeButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 获取短信验证码的按钮

import UIKit

class MUGetCodeButton: BaseButton {

    /// 获取验证码的计时器
    fileprivate var getCodeTimer: Timer?
    /// 倒计时60秒
    fileprivate var timeCount: Int = 60
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightLight)
        } else {
            font = UIFont.systemFont(ofSize: 14)
        }
        
        let attDic: [String: Any] = [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor(hexString: "FFFFFF")]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("发送验证码", comment: ""), attributes: attDic)
        self.setAttributedTitle(attStr, for: UIControlState.normal)
        
        self.backgroundColor = UIColor(hexString: "505050")
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: 开始计时
    func startTimer(){
        self.isEnabled = false
        getCodeTimer = Timer(timeInterval: 1, target: self, selector: #selector(getVerificationTimer(timer:)), userInfo: nil, repeats: true)
        RunLoop.current.add(getCodeTimer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    //MARK: 计时器，改变getCodeButton的文字
    func getVerificationTimer(timer: Timer){
        if timeCount >= 0 {
            let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "FFFFFF"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)] as [String: Any]
            let str: String = NSLocalizedString("已发送", comment: "") + "(\(timeCount))"
            timeCount -= 1
            self.setAttributedTitle(NSAttributedString(string: str, attributes: attDic), for: UIControlState.normal)
        }else{
            fireTimer()
        }
    }
    
    //MARK: 销毁计时器
    func fireTimer(){
        if getCodeTimer == nil { return }
        getCodeTimer?.invalidate()
        timeCount = 60
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "7DE1ED"), NSFontAttributeName: UIFont.systemFont(ofSize: 14)] as [String: Any]
        let str: String = NSLocalizedString("重新获取", comment: "")
        self.setAttributedTitle(NSAttributedString(string: str, attributes: attDic), for: UIControlState.normal)
        self.isEnabled = true
    }
}
