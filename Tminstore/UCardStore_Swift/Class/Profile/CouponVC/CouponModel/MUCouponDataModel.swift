//
//  MUCouponDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/11/21.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Alamofire

class MUCouponDataModel: BaseDataModel {
    
    //MARK: 获取优惠券列表
    /// 获取优惠券列表
    ///
    /// - Parameters:
    ///   - pageIndex: 页数
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成闭包
    func getUserCouponList(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let url: String = appAPIHelp.getUserCouponListAPI + "\(pageIndex)"
        let para: [String: Any] = ["tminstoreToken": getUserToken()] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: url, ParametersDictionary: para, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 获取一张优惠券
    /// 获取一张优惠券
    ///
    /// - Parameters:
    ///   - couponId: 优惠券id
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func addCoupon(couponId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = ["tminstoreToken": getUserToken(), "couponId": couponId] as [String: Any]
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.addCouponAPI, ParametersDictionary: parameters, urlEncoding: URLEncoding.default, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 使用优惠券的请求
    /// 使用优惠券的请求
    ///
    /// - Parameters:
    ///   - couponId: 优惠券ID
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func useCoupon(couponId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let para: [String: Any] = ["couponCode": couponId] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.useCouponAPI, ParametersDictionary: para, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 领取优惠券之前，先检查版本更新
    /// 领取优惠券之前，先检查版本更新
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - update: 需要更新的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func iTunesAppUpdate(success: (()->())?, needToUpdate update: (()->())?, failureClosure failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let systemVersion: String? = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.GetITunesAPI(), ParametersDictionary: nil, urlEncoding: URLEncoding.default, successClosure: { (result: [String: Any]) in
            
            if let code: Int = result["resultCount"] as? Int, code == 1{
                if let results: [[String: Any]] = result["results"] as? [[String: Any]]{
                    if let version: String = results[0]["version"] as? String {
                        //本地版本比App Store里的版本低，才会提示更新
                        if version.compare(systemVersion!, options: NSString.CompareOptions.numeric, range: nil, locale: nil) == ComparisonResult.orderedDescending{
                            update?()//需要更新
                            return
                        }else{
                            success?()
                            return
                        }
                    }
                }
                failure?(nil)
            }else{
                failure?(nil)
            }
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 查看钱包余额
    /// 查看钱包余额
    ///
    /// - Parameter success: 成功的闭包
    func checkBalance(success: ((_ result: [String: Any])->())?){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
            ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.checkBalanceAPI, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 查看钱包交易明细
    /// 查看钱包交易明细
    ///
    /// - Parameters:
    ///   - pageNum: 页数
    ///   - success: 成功的闭包
    func checkWalletDetail(pageNum: Int, andSuccessClosure success: ((_ result:[String: Any])->())?){
        
        let urlString: String = appAPIHelp.checkWalletDetailAPI + "\(pageNum)"
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, urlEncoding: JSONEncoding.default, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
//    
//    //MARK: 获取礼品优惠券的接口
//    /// 获取礼品优惠券的接口
//    ///
//    /// - Parameters:
//    ///   - success: 成功的闭包
//    ///   - failure: 失败的闭包
//    func getActivityGiftCoupon(success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?){
//        
//        let urlString: String = getGiftCouponAPI + "1"
//        
//        let parameters: [String: Any] = [
//            "token": getUserToken()
//        ]
//        
//        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
//            success?(result)
//        }, failureClourse: { (error: Error?) in
//            failure?(error)
//        }, completionClosure: nil)
//    }
    
    //MARK: 获取新用户优惠券
    /// 获取新用户优惠券
    ///
    /// - Parameter success: 成功的闭包
    func requestNewUserCoupon(success: ((_ result: [String: Any])->())?){
        let para: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.newUserCouponAPI, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
