//
//  MUCouponViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUCouponViewModel: BaseViewModel {
    
    fileprivate let activityGiftDataModel: ActivityDataModel = ActivityDataModel()
    
    var couponArray: [CouponModel] = [CouponModel]()
    
    /// 可以领取新用户优惠券
    var canGetNewCoupon: ((_ code: String)->())?
    
    /// 当前优惠券的页数
    fileprivate var pageIndex: Int = 1
    /// 成功获取到优惠券
    var getCouponSuccess: (()->())?
    /// 没有更多数据的闭包
    var noMoreDataClosure: (()->())?
    /// 获取一共有多少张优惠券，以及是否有优惠券可领取
    var totalCoupon: ((_ total: Int, _ hasCouponNeedToGet: Bool)->())?
    /// 获取优惠券成功
    var addCouponSuccess: (()->())?
    /// 使用优惠券成功的闭包
    var useCouponSuccess: ((_ coupon: CouponModel)->())?
    
    fileprivate let dataModel: MUCouponDataModel = MUCouponDataModel()
    /// 活动优惠券
    var activityCouponModel: CouponModel?
    
    /// app需要更新
    var appNeedUpdate: (()->())?
    
    /// 获取余额成功
    var getBalancenSuccess: ((_ balance: Double)->())?
    
//    var giftResult: ((_ result: [String: Any])->())?
    
    /// 交易明细页数
    var detailIndex: Int = 0
    /// 成功获得交易数据
    var getDetailDataSuccess: (()->())?
    /// 没有更多交易数据了
    var noMoreDetailData: (()->())?
    /// 交易数据
    var orderDetailArray: [OrderPostcardModel] = [OrderPostcardModel]()
    
    /// 是否是已领取过新用户优惠券
    fileprivate var hasGetFirstCoupon: Bool = false
    
    override init() {
        super.init()
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 完成的闭包
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    
    //MARK: 获取礼品券的接口
    /// 获取礼品券的接口
    func getGiftCoupon(){
        showSVProgress(title: nil)
        pageIndex = 1
        activityGiftDataModel.getActivityGiftCoupon(success: { [weak self] (result: [String: Any]) in
//            self?.giftResult?(result)
            
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if let dic: [String: Any] = list.first{
                            if let status: String = dic["status"] as? String, status == "00"{
                                self?.activityCouponModel = CouponModel()
                                self?.activityCouponModel?.isActivityCoupon = true
                            }
                        }
                    }
                }
                self?.getCouponList(index: 1)
                return
            }
            self?.errorClosure?(nil, result)
        }, failureClosure: {[weak self] (error: Error?) in
            self?.dismissSVProgress()
            self?.errorClosure?(error, nil)
        })
    }
    
    //MARK: 获取优惠券列表
    /// 获取优惠券列表
    ///
    /// - Parameter index: 是否要获取第一页数据
    func getCouponList(index: Int? = nil){
        
        if pageIndex == 1{
            showSVProgress(title: nil)
        }
        
        dataModel.getUserCouponList(pageIndex: pageIndex, successClosure: { [weak self](result: [String: Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    
                    if let total: Int = data["total"] as? Int, let flag: Bool = data["flag"] as? Bool{
                        self?.totalCoupon?(total, flag)//返回优惠券个数，和是否有待领优惠券
                    }
                    
//                    // 判断是否可以领取新用户优惠券
//                    if let flag: Bool = data["flag"] as? Bool, flag == true{
//                        if let code: String = data["couponCode"] as? String{
//                            self?.canGetNewCoupon?(code)
//                        }
//                        //获取新用户的优惠券
//                        if self?.hasGetFirstCoupon == false {
//                            self?.hasGetFirstCoupon = true
//                            self?.requestNewUserCoupon()
//                        }
//                    }
                    
                    if let couponList: [[String: Any]] = data["couponList"] as? [[String: Any]]{
                        
                        if couponList.isEmpty == true{
                            self?.noMoreDataClosure?()//没有更多数据
                        }else{
                            var couponArray: [CouponModel] = [CouponModel]()
                            for dic: [String: Any] in couponList{//解析优惠券信息
                                let coupon: CouponModel = CouponModel.initWithDic(dic: dic)
                                if coupon.couponStatus == true{
                                    couponArray.append(coupon)
                                }
                            }
                            
                            if self?.pageIndex == 1{
                                self?.couponArray.removeAll()
                                if self?.activityCouponModel != nil{
                                    couponArray.insert((self?.activityCouponModel)!, at: 0)
                                }
                            }
                            self?.couponArray.append(contentsOf: couponArray)
                            self?.getCouponSuccess?()
                            self?.pageIndex += 1
                        }
                    }
                    return
                }
            }
            self?.errorClosure?(nil, result)
            }, failureClosure: { [weak self](error: Error?) in
            self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 获取一张优惠券
    /// 获取一张优惠券
    ///
    /// - Parameter couponId: 优惠券code
    func addCoupon(couponId: String){
        showSVProgress(title: nil)
        
        dataModel.iTunesAppUpdate(success: { [weak self] _ in
            self?.sendRequest(couponId: couponId)
        }, needToUpdate: { [weak self] _ in
            self?.appNeedUpdate?()
            }, failureClosure: { [weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 发送请求优惠券的请求
    fileprivate func sendRequest(couponId: String){
        dataModel.addCoupon(couponId: couponId, successClosure: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.addCouponSuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 使用优惠券的请求
    /// 使用优惠券的请求
    ///
    /// - Parameter couponId: 优惠券ID
    func useCoupon(couponId: String) {
        showSVProgress(title: nil)
        dataModel.useCoupon(couponId: couponId, successClosure: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let couponDic: [String: Any] = data["coupon"] as! [String: Any]
                let coupon: CouponModel = CouponModel.initWithDic(dic: couponDic)
                self?.useCouponSuccess?(coupon)
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClosure: { [weak self](error: Error?) in
            self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 查看钱包余额
    /// 查看钱包余额
    func checkBalance(){
        showSVProgress(title: nil)
        dataModel.checkBalance(success: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let wallet: [String: Any] = data["wallet"] as? [String: Any]{
                        if let moneyActual: Double = wallet["moneyActual"] as? Double{
                            self?.getBalancenSuccess?(moneyActual)
                            return
                        }
                    }
                }
            }
            self?.errorClosure?(nil, result)
        })
    }
    
    //MARK: 查看用户交易明细数据
    /// 查看用户交易明细数据
    func checkWalletDetailData(){
        dataModel.checkWalletDetail(pageNum: detailIndex) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if self?.detailIndex == 1{
                            self?.orderDetailArray.removeAll()
                        }
                        if list.isEmpty{
                            self?.noMoreDetailData?()
                        }else{
                            for dic: [String: Any] in list{
                                let model: OrderPostcardModel = OrderPostcardModel.initFromWalletDic(dic: dic)
                                self?.orderDetailArray.append(model)
                            }
                            self?.getDetailDataSuccess?()
                        }
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 获取新用户优惠券
    /// 获取新用户优惠券
    func requestNewUserCoupon(){
        dataModel.requestNewUserCoupon(success: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let flag: Bool = data["flag"] as? Bool, flag == true{
                        if let couponCode: String = data["couponCode"] as? String{
                            self?.addCoupon(couponId: couponCode)
                            return
                        }
                    }
                }
            }
        })
    }
}
