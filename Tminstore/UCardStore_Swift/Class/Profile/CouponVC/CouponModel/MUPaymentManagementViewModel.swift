//
//  MUPaymentManagementViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUPaymentManagementViewModel: BaseViewModel {

    fileprivate let userInfoDataModel: PMainDataModel = PMainDataModel()
    fileprivate let dataModel: MUPaymentPasswordDataModel = MUPaymentPasswordDataModel()
    
    /// 判断是否设置过支付密码的结果
    var checkPaymentPassword: ((_ result: Bool)->())?
    
    /// 用户信息
    var userInfo: UserInfoModel?
    
    /// 获取验证码失败
    var getVerificationFailed: (()->())?
    
    /// 验证验证码成功
    var checkVerificationCodeSuccess: (()->())?
    
    /// 用户输入的密码
    var paymentPassword: String?
    
    /// 保存的密码
    var password: String?
    
    /// 设置支付密码成功
    var setPaymentPasswordSuccess: (()->())?
    
    /// 用户输入的旧密码是正确
    var oldPasswordCurrect: (()->())?
    
    override init() {
        super.init()
        
        //MARK: 网络请求出错
        userInfoDataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        //MARK: 网络请求完成
        userInfoDataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
        
        //MARK: 网络请求出错
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        //MARK: 网络请求完成
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 获取用户信息
    /// 获取用户信息
    func checkUserPhoneNumber(hasSetPhoneNumber: ((_ result: Bool)->())?){
        showSVProgress(title: nil)
        userInfoDataModel.getUserInfo(success: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let userDic: [String: Any] = data["user"] as? [String: Any]{
                        self?.userInfo = UserInfoModel.initFromDic(dic: userDic)
                        hasSetPhoneNumber?(self?.userInfo?.phoneNumber != "")
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }, failureClourse: nil, completionClosure: nil)
    }
    
    //MARK: 操作支付密码时, 获取验证码
    /// 操作支付密码时, 获取验证码
    ///
    /// - Parameter phone: 手机号码
    func getVerificationCode(phone: String){
        dataModel.getVerificationCode(phone: phone) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                return
            }
            self?.getVerificationFailed?()
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 操作支付密码时, 判断验证码
    /// 操作支付密码时, 判断验证码
    ///
    /// - Parameters:
    ///   - phone: 手机号码
    ///   - code: 验证码
    func checkVerificationCode(phone: String, andCode code: String){
        dataModel.checkVerificationCode(phone: phone, andVerificationCode: code) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.checkVerificationCodeSuccess?()
                return
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 判断输入的密码是否合法
    /// 判断输入的密码是否合法
    ///
    /// - Parameter password: 输入的密码
    /// - Returns: Bool 密码是否合法
    func checkPassword(password: String)->Bool{
        do {
            let regex: NSRegularExpression = try NSRegularExpression(pattern: "^(?![0-9]+$)(?![a-z]+$)(?![A-Z]+$)[0-9A-Za-z]{8,16}$", options: .caseInsensitive)
            return regex.firstMatch(in: password, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, password.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    //MARK: 设置支付密码
    /// 设置支付密码
    func setPaymentPassword(){
        showSVProgress(title: nil)
        dataModel.setPaymentPassword(password: password!) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.setPaymentPasswordSuccess?()
                return
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 是否设置过支付密码
    /// 是否设置过支付密码
    func checkPaymentPasswordStatus(){
        showSVProgress(title: nil)
        dataModel.checkPaymentPassword { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let flag: Bool = data["flag"] as? Bool{
                        self?.checkPaymentPassword?(flag)
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 修改或重置支付密码
    /// 修改或重置支付密码
    ///
    /// - Parameter password: 密码
    func resetPaymentPassword(){
        showSVProgress(title: nil)
        dataModel.resetPaymentPassword(password: password!) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.setPaymentPasswordSuccess?()
                return
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 判断旧密码是否有效
    /// 判断旧密码是否有效
    ///
    /// - Parameter password: 密码
    func checkOldPassword(password: String){
        showSVProgress(title: nil)
        dataModel.checkOldPaymentPassword(password: password) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.oldPasswordCurrect?()
                return
            }
            self?.errorClosure?(nil, result)
        }
    }
}
