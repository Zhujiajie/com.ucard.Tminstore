//
//  MUPaymentPasswordDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/16.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUPaymentPasswordDataModel: BaseDataModel {

    //MARK: 操作支付密码时获取手机验证码
    /// 操作支付密码时获取手机验证码
    ///
    /// - Parameters:
    ///   - phone: 手机号码
    ///   - success: 成功获得数据的闭包
    func getVerificationCode(phone: String, andSuccessClosure success: ((_ result: [String: Any])->())?){
        
        let para: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "phone": phone
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.paymentPasswordGetCode, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 操作支付密码时, 验证手机验证码
    /// 操作支付密码时, 验证手机验证码
    ///
    /// - Parameters:
    ///   - phone: 手机号码
    ///   - code: 手机验证码
    ///   - success: 成功的闭包
    func checkVerificationCode(phone: String, andVerificationCode code: String, andSuccessClosure success: ((_ result: [String: Any])->())?){
        
        let para: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "phone": phone,
            "phoneCode": code
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.paymentPasswordCheck, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 设置支付密码的请求
    /// 设置支付密码的请求
    ///
    /// - Parameters:
    ///   - password: 密码
    ///   - success: 成功的闭包
    func setPaymentPassword(password: String, andSuccessClosure success: ((_ result: [String: Any])->())?){
        
        let para: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "payPassword": password
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.setPaymentPasswordAPI, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 判断用户是否设置过支付密码
    /// 判断用户是否设置过支付密码
    ///
    /// - Parameter success: 成功的闭包
    func checkPaymentPassword(success: ((_ result: [String: Any])->())?){
        let para: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.checkPaymentPasswordAPI, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 重置或修改支付密码
    /// 重置或修改支付密码
    ///
    /// - Parameters:
    ///   - password: 新的支付密码
    ///   - success: 成功的闭包
    func resetPaymentPassword(password: String, andSuccessClosure success: ((_ result: [String: Any])->())?){
        
        let para: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "payPassword": password
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.resetPaymentPasswordAPI, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 判断用户输入的旧密码是否有效
    /// 判断用户输入的旧密码是否有效
    ///
    /// - Parameters:
    ///   - password: 密码
    ///   - success: 成功的闭包
    func checkOldPaymentPassword(password: String, andSuccessClosure success: ((_ result: [String: Any])->())?){
        
        let para: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "payPassword": password
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.checkOldPaymentPasswordAPI, ParametersDictionary: para, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
