//
//  MUCouponMainViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/1.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUCouponMainViewController: BaseViewController {

    fileprivate let viewModel: MUCouponViewModel = MUCouponViewModel()
    
    /// 标题
    fileprivate var titleLabel: BaseLabel?
    /// 余额ICON
    fileprivate var balanceICON: BaseImageView?
    /// 货币label
    fileprivate var currencyLabel: BaseLabel?
    /// 余额Label
    fileprivate var balanceLabel: BaseLabel?
    /// 交易明细ICON
    fileprivate var purchaseICON: BaseImageView?
    /// 交易Label
    fileprivate var purchaseLabel: BaseLabel?
    /// 优惠券ICON
    fileprivate var couponICON: BaseImageView?
    /// 优惠券label
    fileprivate var couponLabel: BaseLabel?
    /// 交易明细的按钮
    fileprivate var purchaseButton: BaseButton?
    /// 优惠券的按钮
    fileprivate var couponButton: BaseButton?
    /// 支付管理ICON
    //fileprivate var paymentManageICON: BaseImageView?
    /// 支付管理标题
    //fileprivate var paymentManageLabel: BaseLabel?
    /// 支付管理按钮
    //fileprivate var paymentManageButton: BaseButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        
        closures()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        interactive.pan?.isEnabled = true
        
        viewModel.checkBalance()//获取钱余额
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactive.pan?.isEnabled = false
    }
    
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationTitle(NSLocalizedString("我的钱包", comment: ""))
        setNavigationBarLeftButtonWithImageName("backArrow")
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        
        //顶部灰色线条
        let grayLine01: BaseView = BaseView.colorLine(colorString: "#F8F8F8")
        view.addSubview(grayLine01)
        grayLine01.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(view)
            make.height.equalTo(4)
        }
        
        //标题
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "9B9B9B", andText: NSLocalizedString("账户余额", comment: ""))
        view.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine01.snp.bottom).offset(10/baseWidth)
            make.left.equalTo(view).offset(19/baseWidth)
        })
        
        //余额ICON
        balanceICON = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "balance")
        view.addSubview(balanceICON!)
        balanceICON?.snp.makeConstraints({ (make) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(10/baseWidth)
            make.leftMargin.equalTo(titleLabel!)
            make.size.equalTo(CGSize(width: 30/baseWidth, height: 30/baseWidth))
        })
        
        //货币label
        currencyLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "4CDFED", andText: "¥")
        view.addSubview(currencyLabel!)
        currencyLabel?.snp.makeConstraints({ (make) in
            make.left.equalTo(balanceICON!.snp.right).offset(19/baseWidth)
            make.bottomMargin.equalTo(balanceICON!)
        })
        
        //余额的label
        balanceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 36), andTextColorHex: "4CDFED")
        view.addSubview(balanceLabel!)
        balanceLabel?.snp.makeConstraints({ (make) in
            make.bottomMargin.equalTo(currencyLabel!).offset(5)
            make.left.equalTo(currencyLabel!.snp.right)
        })
        
        let grayLine02: BaseView = BaseView.colorLine(colorString: "#F8F8F8")
        view.addSubview(grayLine02)
        grayLine02.snp.makeConstraints { (make) in
            make.top.equalTo(grayLine01.snp.bottom).offset(92/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(2)
        }
        
        //交易ICON
        purchaseICON = BaseImageView.normalImageView(andImageName: "orderDetail")
        view.addSubview(purchaseICON!)
        purchaseICON?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine02.snp.bottom).offset(15/baseWidth)
            make.leftMargin.equalTo(balanceICON!)
            make.size.equalTo(CGSize(width: 20.3/baseWidth, height: 20.3/baseWidth))
        })
        
        //交易label
        purchaseLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "646464", andText: NSLocalizedString("交易明细", comment: ""))
        view.addSubview(purchaseLabel!)
        purchaseLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(purchaseICON!)
            make.left.equalTo(purchaseICON!.snp.right).offset(28.11/baseWidth)
        })
        
        let arrow01: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "grayRightArrow")
        view.addSubview(arrow01)
        arrow01.snp.makeConstraints { (make) in
            make.centerY.equalTo(purchaseICON!)
            make.right.equalTo(view).offset(-20.3/baseWidth)
            make.size.equalTo(CGSize(width: 5.8/baseWidth, height: 10.3/baseWidth))
        }
        
        
        // 灰色线条3
        let grayLine03: BaseView = BaseView.colorLine(colorString: "#F8F8F8")
        view.addSubview(grayLine03)
        grayLine03.snp.makeConstraints { (make) in
            make.top.equalTo(grayLine02.snp.bottom).offset(50/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(2)
        }
        
        //优惠券ICON
        couponICON = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "couponMain")
        view.addSubview(couponICON!)
        couponICON?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine03.snp.bottom).offset(14/baseWidth)
            make.leftMargin.equalTo(balanceICON!)
            make.size.equalTo(CGSize(width: 22/baseWidth, height: 22/baseWidth))
        })
        
        couponLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "646464", andText: NSLocalizedString("优惠券", comment: ""))
        view.addSubview(couponLabel!)
        couponLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(couponICON!)
            make.leftMargin.equalTo(purchaseLabel!)
        })
        
        let arrow02: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "grayRightArrow")
        view.addSubview(arrow02)
        arrow02.snp.makeConstraints { (make) in
            make.centerY.equalTo(couponICON!)
            make.rightMargin.equalTo(arrow01)
            make.size.equalTo(arrow01)
        }
        
        // 灰色线条4
        let grayLine04: BaseView = BaseView.colorLine(colorString: "#F8F8F8")
        view.addSubview(grayLine04)
        grayLine04.snp.makeConstraints { (make) in
            make.top.equalTo(grayLine03.snp.bottom).offset(50/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(2)
        }
        /*
        // 支付管理ICON
        paymentManageICON = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "paymentManage")
        view.addSubview(paymentManageICON!)
        paymentManageICON?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine04.snp.bottom).offset(15/baseWidth)
            make.left.equalTo(view).offset(21/baseWidth)
            make.size.equalTo(CGSize(width: 22/baseWidth, height: 22/baseWidth))
        })
        
        //支付管理title
        paymentManageLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "646464", andText: NSLocalizedString("支付管理", comment: ""))
        view.addSubview(paymentManageLabel!)
        paymentManageLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(paymentManageICON!)
            make.leftMargin.equalTo(couponLabel!)
        })
 
 
        let arrow03: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "grayRightArrow")
        view.addSubview(arrow03)
        arrow03.snp.makeConstraints { (make) in
            make.centerY.equalTo(paymentManageICON!)
            make.rightMargin.equalTo(arrow01)
            make.size.equalTo(arrow01)
        }
        */
        //进入交易明细的按钮
        purchaseButton = BaseButton.normalIconButton()
        view.addSubview(purchaseButton!)
        purchaseButton?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(grayLine02.snp.bottom)
            make.bottom.equalTo(grayLine03.snp.top)
        })
        
        //进入优惠券列表的按钮
        couponButton = BaseButton.normalIconButton()
        view.addSubview(couponButton!)
        couponButton?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(grayLine03.snp.bottom)
            make.height.equalTo(purchaseButton!)
        })
        
        //进入支付管理界面的按钮
        /*
        paymentManageButton = BaseButton.normalIconButton()
        view.addSubview(paymentManageButton!)
        paymentManageButton?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(view)
            make.top.equalTo(grayLine04.snp.bottom)
            make.height.equalTo(purchaseButton!)
        })
 */
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击优惠券按钮的事件
        couponButton?.touchButtonClosure = {[weak self] _ in
            MobClick.event("checkcoupon")//友盟点击事件
            let couponVC: MUCouponVC = MUCouponVC()
            couponVC.canPay = false
            couponVC.umengViewName = "查看优惠券"
            _ = self?.navigationController?.pushViewController(couponVC, animated: true)
        }
        
        //MARK: 点击交易明细按钮的事件
        purchaseButton?.touchButtonClosure = {[weak self] _ in
            let vc: MUWalletPurchaseController = MUWalletPurchaseController()
            vc.umengViewName = "交易明细"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 点击支付管理按钮进入支付管理界面
//        paymentManageButton?.touchButtonClosure = {[weak self] _ in
//            let vc: MUPaymentManagementViewController = MUPaymentManagementViewController()
//            vc.umengViewName = "支付管理"
//            _ = self?.navigationController?.pushViewController(vc, animated: true)
//        }
        
        //MARK: 成功获得余额
        viewModel.getBalancenSuccess = {[weak self] (balance: Double) in
            self?.balanceLabel?.text = "\(balance)"
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
}
