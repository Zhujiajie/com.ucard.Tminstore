//
//  MUCouponVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/6.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUCouponVC: BaseViewController {
    /// 成功选择了一张优惠券
    var useCouponSuccess: ((_ coupon: CouponModel)->())?
    
    let viewModel: MUCouponViewModel = MUCouponViewModel()
    
    /// 能不能使用优惠券
    var canPay: Bool = true
    
    /// 是否是从AR界面来的
    var useDismiss: Bool = false
    
    /// 需要添加的二维码
    var qrCodeNeedToAdd: String?
    
    fileprivate var tableFooterView: UIView?
    
    /// 用来跳转到优惠券说明的界面
    fileprivate var couponSupportButon: MUCouponButton?
    
    /// 用来展示优惠券的tableView
    fileprivate var tableView: MUCouponTableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        setNavigationBar()//设置导航栏
        setCouponSupportButton()//设置优惠券说明按钮
        setTableView()//设置tableView
        closures()
        viewModel.getCouponList()//获取优惠券列表
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.requestNewUserCoupon()//请求新用户优惠券
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationTitle(NSLocalizedString("我的优惠券", comment: ""))
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        setNavigationBarRightButtonWithImageName("scan")
    }
    
    //MARK: 设置优惠券说明按钮
    fileprivate func setCouponSupportButton(){
        tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 34/baseHeight))
        tableFooterView?.backgroundColor = UIColor.clear
        
        couponSupportButon = MUCouponButton()
        tableFooterView?.addSubview(couponSupportButon!)
        couponSupportButon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(tableFooterView!)
            make.right.equalTo(tableFooterView!).offset(-26/baseWidth)
        })
    }

    //MARK: 设置tableView
    fileprivate func setTableView(){
        tableView = MUCouponTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        tableView?.tableFooterView = tableFooterView!
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    //MARK: 进入扫描二维码界面
    func enterScanQRCodeVC(){
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) == false {
            alert(alertTitle: NSLocalizedString("相机不可用", comment: ""))
            return
        }
        //进入优惠券扫描界面
        let qrScanVC: QRViewController = QRViewController()
        qrScanVC.isFromCouponVC = true
        present(qrScanVC, animated: true, completion: nil)
        qrScanVC.getQRCode = {[weak self] (code: String) in
            self?.viewModel.addCoupon(couponId: code)
        }//获取优惠券
    }
    
    //MARK: 导航栏左按钮的点击事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        if useDismiss == true {
            dismissSelf()
        }else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: 导航栏右按钮的点击事件，进入扫描界面，回去二维码
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        enterScanQRCodeVC()
    }
}

//MARK:------------UITableViewDelegate, UITableViewDataSource---------------
extension MUCouponVC: UITableViewDelegate, UITableViewDataSource{
    
    //MARK: 点击优惠券，请求优惠券支付
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 1{
            //使用优惠券
            if viewModel.couponArray[indexPath.row].isActivityCoupon == false && canPay == true{
                let couponId: String = viewModel.couponArray[indexPath.row].couponCode
                viewModel.useCoupon(couponId: couponId)
            }else if viewModel.couponArray[indexPath.row].isActivityCoupon == true{
                let vc: ActivityVC = ActivityVC()
                vc.backImage = getScreenImage()
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                present(vc, animated: true, completion: nil)
                vc.dismissClosure = {[weak self] _ in
                    self?.viewModel.activityCouponModel = nil
                    self?.viewModel.getGiftCoupon()//刷新优惠券
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 { return 1 }
        return viewModel.couponArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {return 0}
        return 12
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.section == 0{
            let cell: MUGetCouponTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUGetCouponTableViewCellReuseIdentifier, for: indexPath) as! MUGetCouponTableViewCell
            cell.touchExchangeButton = {[weak self](couponId: String) in
                self?.viewModel.addCoupon(couponId: couponId)
            }//点击兑换优惠券的按钮，兑换优惠券
            return cell
        }else {
            if viewModel.couponArray[indexPath.row].isActivityCoupon == true{
                let cell: MUCouponActivityTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUCouponActivityTableViewCellReuseIdentifier, for: indexPath) as! MUCouponActivityTableViewCell
                return cell
            }else{
                let cell: MUCouponTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUCouponTableViewCellReuseIdentifier, for: indexPath) as! MUCouponTableViewCell
                cell.couponModel = viewModel.couponArray[indexPath.row]
                return cell
            }
        }
    }
}

//MARK: ----------网络请求的回调------------------
extension MUCouponVC{
    
    fileprivate func closures(){
        
        //MARK: 优惠券说明的按钮
        couponSupportButon?.touchButtonClosure = {[weak self] _ in
            let supportVC: MUCouponSupportVC = MUCouponSupportVC()
            supportVC.umengViewName = "优惠券说明"
            self?.navigationController?.pushViewController(supportVC, animated: true)
        }
        
        //MARK: 上拉获取更多数据
        tableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.getCouponList()
        }
        
        //MARK: 成功获取到优惠券
        viewModel.getCouponSuccess = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshing()
            self?.tableView?.reloadData()
            if self?.qrCodeNeedToAdd != nil{
                self?.viewModel.addCoupon(couponId: (self?.qrCodeNeedToAdd)!)
                self?.qrCodeNeedToAdd = nil
            }
        }
        
        //MARK: 没有更多数据了
        viewModel.noMoreDataClosure = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 获取优惠券成功，重新获取优惠券列表
        viewModel.addCouponSuccess = {[weak self] _ in
            self?.viewModel.getGiftCoupon()
        }
        
        //MARK: 成功选择了一张优惠券
        viewModel.useCouponSuccess = {[weak self] (coupon: CouponModel) in
            self?.useCouponSuccess?(coupon)
            _ = self?.navigationController?.popViewController(animated: true)
        }
        
        //MARK: app需要更新
        viewModel.appNeedUpdate = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("需要更新到最新版才能领取优惠券哦", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("更新", comment: ""), rightClosure: {
                let url: URL = URL(string: "https://itunes.apple.com/us/app/ucardstore-send-share-real/id966372400?mt=8")!
                if UIApplication.shared.canOpenURL(url){
                    UIApplication.shared.openURL(url)
                }
            })
        }
        
        //MARK: 可以领取新用户优惠券
        viewModel.canGetNewCoupon = {[weak self] (code: String) in
            MobClick.event("newCustomerGetCoupon")//友盟点击事件
            self?.showGetNewCouponView(code: code)
        }
    }
    
    //MARK: 呈现领取新用户优惠券页面
    fileprivate func showGetNewCouponView(code: String){
        let newCouponView: NewCouponView = NewCouponView()
        view.addSubview(newCouponView)
        newCouponView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view).offset(-nav_statusHeight)
            make.left.bottom.right.equalTo(view)
        }
        self.navigationController?.navigationBar.layer.zPosition = -1
        //点击了领取优惠券
        newCouponView.getNewCoupon = {[weak self] _ in
            self?.viewModel.addCoupon(couponId: code)
        }
        //点击了取消按钮
        newCouponView.dismissClosure = {[weak self] _ in
            self?.navigationController?.navigationBar.layer.zPosition = 0
        }
    }
}
