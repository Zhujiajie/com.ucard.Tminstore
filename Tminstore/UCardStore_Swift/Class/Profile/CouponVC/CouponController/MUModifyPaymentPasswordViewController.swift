//
//  MUModifyPaymentPasswordViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/17.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class MUModifyPaymentPasswordViewController: BaseViewController {

    let viewModel: MUPaymentManagementViewModel = MUPaymentManagementViewModel()
    
    /// 顶部灰色线条
    fileprivate var grayLine: BaseView?
    /// 获取和判断手机验证码的视图
    fileprivate var originalCodeView: MUEnterOriginalPaymentPasswordView?
    /// 输入支付密码的视图
    fileprivate var enterPasswordView: MUEnterPaymentPasswordView?
    /// 确认支付密码的视图
    fileprivate var checkPasswordView: MUCheckPaymentPasswordView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        grayLine = BaseView.colorLine(colorString: "#F8F8F8")
        view.addSubview(grayLine!)
        grayLine?.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(view)
            make.height.equalTo(4)
        }
        
        setUI()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
        view.layer.masksToBounds = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationTitle(NSLocalizedString("修改支付密码", comment: ""))
        setNavigationBarLeftButtonWithImageName("backArrow")
    }

    //MARK: 设置获取和验证验证码的UI
    fileprivate func setUI(){
        
        originalCodeView = MUEnterOriginalPaymentPasswordView()
        originalCodeView?.viewModel = viewModel
        view.addSubview(originalCodeView!)
        originalCodeView?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine!.snp.bottom).offset(4)
            make.bottom.equalTo(view)
            make.centerX.equalTo(view).offset(0)
            make.width.equalTo(screenWidth)
        })
        
        enterPasswordView = MUEnterPaymentPasswordView()
        enterPasswordView?.viewModel = viewModel
        view.addSubview(enterPasswordView!)
        enterPasswordView?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine!.snp.bottom).offset(4)
            make.bottom.equalTo(view)
            make.centerX.equalTo(view).offset(screenWidth)
            make.width.equalTo(screenWidth)
        })
        
        checkPasswordView = MUCheckPaymentPasswordView()
        checkPasswordView?.viewModel = viewModel
        checkPasswordView?.isSetPaymentPassword = false//修改密码
        view.addSubview(checkPasswordView!)
        checkPasswordView?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine!.snp.bottom).offset(4)
            make.bottom.equalTo(view)
            make.centerX.equalTo(view).offset(screenWidth)
            make.width.equalTo(screenWidth)
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 验证旧支付密码成功
        viewModel.oldPasswordCurrect = {[weak self] _ in
            self?.beginEnterPaymentPassword()
        }
        
        //MARK: 第一次输入密码时, 判断密码合法
        enterPasswordView?.checkPasswordSuccess = {[weak self] _ in
            self?.beginCheckPaymentPassword()
        }
        
        //MARK: 第一次输入密码时, 判断密码不合法
        enterPasswordView?.checkPasswordFailed = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("请输入6-16位密码", comment: "") + "," + NSLocalizedString("请包含数字和大小写字母", comment: ""))
        }
        
        //MARK: 第二次输入的密码不匹配
        checkPasswordView?.passwordError = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("两次输入的密码不同", comment: ""))
        }
        
        //MARK: 设置支付密码成功
        viewModel.setPaymentPasswordSuccess = {[weak self] _ in
            self?.setPaymentPasswordSuccess()
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 出现输入支付密码的视图
    fileprivate func beginEnterPaymentPassword(){
        originalCodeView?.snp.updateConstraints({ (make) in
            make.centerX.equalTo(view).offset(-screenWidth)
        })
        enterPasswordView?.snp.updateConstraints({ (make) in
            make.centerX.equalTo(view).offset(0)
        })
        autoLayoutAnimation(view: view)
    }
    
    //MARK: 出现确认支付密码的视图
    fileprivate func beginCheckPaymentPassword(){
        enterPasswordView?.snp.updateConstraints { (make) in
            make.centerX.equalTo(view).offset(-screenWidth)
        }
        checkPasswordView?.snp.makeConstraints({ (make) in
            make.centerX.equalTo(view).offset(0)
        })
        autoLayoutAnimation(view: view)
    }
    
    //MARK: 弹出设置成功的toast
    fileprivate func setPaymentPasswordSuccess(){
        let successView: MUSetPaymentPasswordSuccessView = MUSetPaymentPasswordSuccessView()
        navigationController?.view.addSubview(successView)
        successView.snp.makeConstraints { (make) in
            make.edges.equalTo(navigationController!.view)
        }
        // toast消失, 自动退出
        successView.dismissClosure = {[weak self] _ in
            _ = self?.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: 点击导航栏返回按钮的事件
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        alert(alertTitle: NSLocalizedString("放弃修改支付密码", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("确认", comment: ""), leftClosure: nil, rightClosure: { [weak self] _ in
            _ = self?.navigationController?.popViewController(animated: true)
            }, presentComplition: nil)
    }

}
