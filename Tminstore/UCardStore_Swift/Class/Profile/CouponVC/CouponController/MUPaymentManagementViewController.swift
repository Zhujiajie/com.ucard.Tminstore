//
//  MUPaymentManagementViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUPaymentManagementViewController: BaseViewController {

    fileprivate let viewModel: MUPaymentManagementViewModel = MUPaymentManagementViewModel()
    
    /// 展示入口的tableViewController
    fileprivate var tableViewController: MUPaymentManagementTableViewController?
    
    /// 是否设置了支付密码
    fileprivate var hasSetPasswordYet: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.checkPaymentPasswordStatus()//判断是否已设置过支付密码
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationTitle(NSLocalizedString("支付管理", comment: ""))
        setNavigationBarLeftButtonWithImageName("backArrow")
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F3F3F3")
        view.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(view)
            make.height.equalTo(4)
        }
        
        tableViewController = MUPaymentManagementTableViewController()
        view.addSubview(tableViewController!.view)
        tableViewController?.view?.snp.makeConstraints({ (make) in
            make.left.bottom.right.equalTo(view)
            make.top.equalTo(grayLine.snp.bottom)
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closure(){
        
        //MARK: 判断用户是否已设置过支付密码
        viewModel.checkPaymentPassword = {[weak self] (result: Bool) in
            self?.hasSetPasswordYet = result
            self?.tableViewController?.shouldShowNotSetPaymentPassword = !result
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击支付密码按钮的事件
        tableViewController?.selecteCell = {[weak self] (indexPath: IndexPath) in
            switch indexPath.row {
            case 0:
                if self?.hasSetPasswordYet == false{
                    self?.enterSetPaymentPasswordVC()
                }
            case 1:
                self?.enterModifyPaymentPasswordVC()
            default:
                self?.enterForgetPaymentPasswordVC()
            }
        }
    }
    
    //MARK: 判断是否进入设置支付密码界面
    fileprivate func enterSetPaymentPasswordVC(){
        //先判断是否设置过手机号码
        viewModel.checkUserPhoneNumber { [weak self] (result: Bool) in
            if result {
                //进入设置支付密码的界面
                let vc: MUCheckCodeViewController = MUCheckCodeViewController()
                vc.phoneNumber = (self?.viewModel.userInfo?.phoneNumber)!
                vc.umengViewName = "设置支付密码"
                vc.viewModel.userInfo = self?.viewModel.userInfo
                _ = self?.navigationController?.pushViewController(vc, animated: true)
            }else{
                // 未绑定手机, 提示用户先绑定手机
                self?.enterBindingPhoneVC()
            }
        }
    }
    
    //MARK: 进入修改支付密码的界面
    fileprivate func enterModifyPaymentPasswordVC(){
        if hasSetPasswordYet {
            //先判断是否设置过手机号码
            viewModel.checkUserPhoneNumber { [weak self] (result: Bool) in
                if result {
                    //进入设置支付密码的界面
                    let vc: MUModifyPaymentPasswordViewController = MUModifyPaymentPasswordViewController()
                    vc.umengViewName = "修改支付密码"
                    vc.viewModel.userInfo = self?.viewModel.userInfo
                    _ = self?.navigationController?.pushViewController(vc, animated: true)
                }else{
                    // 未绑定手机, 提示用户先绑定手机
                    self?.enterBindingPhoneVC()
                }
            }
        }else{
            alert(alertTitle: NSLocalizedString("您还未设置支付密码\n请先设置支付密码再操作", comment: ""))
        }
    }
    
    //MARK: 进入忘记支付密码的界面
    fileprivate func enterForgetPaymentPasswordVC(){
        if hasSetPasswordYet {
            //先判断是否设置过手机号码
            viewModel.checkUserPhoneNumber { [weak self] (result: Bool) in
                if result {
                    //进入忘记支付密码的界面
                    let vc: MUCheckCodeViewController = MUCheckCodeViewController()
                    vc.isSetPaymentPassword = false
                    vc.phoneNumber = (self?.viewModel.userInfo?.phoneNumber)!
                    vc.umengViewName = "忘记支付密码"
                    vc.viewModel.userInfo = self?.viewModel.userInfo
                    _ = self?.navigationController?.pushViewController(vc, animated: true)
                }else{
                    // 未绑定手机, 提示用户先绑定手机
                    self?.enterBindingPhoneVC()
                }
            }
        }else{
            alert(alertTitle: NSLocalizedString("您还未设置支付密码\n请先设置支付密码再操作", comment: ""))
        }
    }
    
    //MARK: 进入绑定手机号码的界面
    fileprivate func enterBindingPhoneVC(){
        alert(alertTitle: NSLocalizedString("需要先绑定手机号码", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("好的", comment: ""), leftClosure: nil, rightClosure: { [weak self] _ in
            let bindingVC: BindingController = BindingController()
            bindingVC.userInfo = self?.viewModel.userInfo
            bindingVC.isBindingPhone = true
            _ = self?.navigationController?.pushViewController(bindingVC, animated: true)
            bindingVC.bindingSuccess = {
                self?.viewModel.checkPaymentPasswordStatus()
            }//绑定成功之后，刷新页面
            }, presentComplition: nil)
    }
}
