//
//  MUPaymentManagementTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

fileprivate let reuseIdentifier: String = "reuseIdentifier"

class MUPaymentManagementTableViewController: UITableViewController {

    /// 是否需要展示还没有设定支付密码的label
    var shouldShowNotSetPaymentPassword: Bool = false {
        didSet{
            if let cell: MUPaymentManagementTableViewCell = self.tableView.cellForRow(at: IndexPath.init(row: 0, section: 0)) as? MUPaymentManagementTableViewCell{
                 cell.shouldShowSubTitle = shouldShowNotSetPaymentPassword
            }
        }
    }
    
    /// 点击cell的事件
    var selecteCell: ((_ indexPath: IndexPath)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(MUPaymentManagementTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        self.tableView.tableFooterView = UIView()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MUPaymentManagementTableViewCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MUPaymentManagementTableViewCell
        cell.indexPath = indexPath
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selecteCell?(indexPath)
    }
}

class MUPaymentManagementTableViewCell: UITableViewCell{
    
    var indexPath: IndexPath?{
        didSet{
            if indexPath != nil {
                setUI()
            }
        }
    }
    
    /// 需要展示副标题
    var shouldShowSubTitle: Bool = false{
        didSet{
            subTtitleLabel?.isHidden = !shouldShowSubTitle
        }
    }
    
    /// 标题
    fileprivate var titleLabel: BaseLabel?
    /// 富标题
    fileprivate var subTtitleLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "646464")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(20/baseWidth)
        })
        
        let rightArrow: BaseImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "grayRightArrow")
        contentView.addSubview(rightArrow)
        rightArrow.snp.makeConstraints { (make) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-20.3/baseWidth)
            make.size.equalTo(CGSize(width: 6/baseWidth, height: 10/baseWidth))
        }
        
        subTtitleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "BBBBBB", andText: NSLocalizedString("未设置", comment: ""))
        contentView.addSubview(subTtitleLabel!)
        subTtitleLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(titleLabel!)
            make.right.equalTo(rightArrow.snp.left).offset(-22/baseWidth)
        })
        subTtitleLabel?.isHidden = true
    }
    
    //MARK: 设置lable的文字
    fileprivate func setUI(){
        
        var titleText: String
        switch indexPath!.row {
        case 0:
            titleText = NSLocalizedString("支付密码", comment: "")
        case 1:
            titleText = NSLocalizedString("修改支付密码", comment: "")
        default:
            titleText = NSLocalizedString("忘记支付密码", comment: "")
        }
        titleLabel?.text = titleText
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
