//
//  MUWalletPurchaseTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/1.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

fileprivate let reuseIdentifier: String = "reuseIdentifier"

class MUWalletPurchaseTableViewController: UITableViewController {

    weak var viewModel: MUCouponViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.backgroundColor = UIColor(hexString: "#F8F8F8")
        self.tableView.register(MUWalletPurchaseTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        self.tableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 0, right: 0)
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        self.tableView.mj_header = BaseAnimationHeader()
        self.tableView.mj_footer = BaseRefreshFooter()
        
        closures()
    }
    
    //MARK: 下拉刷新
    /// 下拉刷新
    func beginRefresh(){
        self.tableView.mj_header.beginRefreshing()
    }
    
    //MARK: 停止刷新动画
    /// 停止刷新动画
    func stopRefreshAnimation(){
        self.tableView.mj_header.endRefreshing()
        self.tableView.mj_footer.endRefreshing()
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 下拉刷新
        self.tableView.mj_header.refreshingBlock = {[weak self] _ in
            self?.viewModel?.detailIndex = 1
            self?.viewModel?.checkWalletDetailData()
        }
        
        //MARK: 上拉刷新
        self.tableView.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel?.checkWalletDetailData()
        }
        
        //MARK: 成功获得数据
        viewModel?.getDetailDataSuccess = {[weak self] _ in
            self?.stopRefreshAnimation()
            self?.tableView.reloadData()
        }
        
        //MARK: 没有更多数据
        viewModel?.noMoreDetailData = {[weak self] _ in
            self?.tableView.mj_header.endRefreshing()
            self?.tableView.mj_footer.endRefreshingWithNoMoreData()
        }
    }
    
    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        return viewModel!.orderDetailArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MUWalletPurchaseTableViewCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! MUWalletPurchaseTableViewCell
        cell.orderModel = viewModel?.orderDetailArray[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100/baseWidth
    }
}

//MARK: 展示交易详情的cell
class MUWalletPurchaseTableViewCell: UITableViewCell{
    
    /// 数据源
    var orderModel: OrderPostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 交易类型
    fileprivate var businessTypeLabel: BaseLabel?
    /// 交易时间
    fileprivate var businessTimeLabel: BaseLabel?
    /// 余额label
    fileprivate var balanceLabel: BaseLabel?
    /// 变动Label
    fileprivate var changeLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        
        businessTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "909090", andTextAlignment: NSTextAlignment.right, andNumberOfLines: 1, andText: nil)
        contentView.addSubview(businessTimeLabel!)
        businessTimeLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(contentView).offset(18/baseWidth)
            make.right.equalTo(contentView).offset(-21/baseWidth)
        })
        
        businessTypeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "646464", andTextAlignment: NSTextAlignment.left, andNumberOfLines: 1, andText: nil)
        contentView.addSubview(businessTypeLabel!)
        businessTypeLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(businessTimeLabel!)
            make.left.equalTo(contentView).offset(34/baseWidth)
        })
        
        balanceLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "444444", andText: nil)
        contentView.addSubview(balanceLabel!)
        balanceLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(businessTypeLabel!.snp.bottom).offset(19/baseWidth)
            make.leftMargin.equalTo(businessTypeLabel!)
        })
        
        changeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "646464", andTextAlignment: NSTextAlignment.right, andNumberOfLines: 1, andText: nil)
        contentView.addSubview(changeLabel!)
        changeLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(balanceLabel!)
            make.rightMargin.equalTo(businessTimeLabel!)
        })
        
        //底部灰色线条
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F8F8F8")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(3)
        }
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        if let date: Date = orderModel?.createDate{
            businessTimeLabel?.text = getDateText(date: date)
        }
        businessTypeLabel?.text = NSLocalizedString("图片收入", comment: "")
        balanceLabel?.text = NSLocalizedString("余额: ", comment: "") + "\(orderModel!.balance)"
        changeLabel?.text = "+\(orderModel!.orderAmount)"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
