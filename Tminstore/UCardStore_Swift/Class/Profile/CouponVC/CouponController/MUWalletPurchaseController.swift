//
//  MUWalletPurchaseController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/1.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUWalletPurchaseController: BaseViewController {

    fileprivate let viewModel: MUCouponViewModel = MUCouponViewModel()
    
    /// 展示数据的tableViewController
    fileprivate var tableViewController: MUWalletPurchaseTableViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setUI()
        closures()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableViewController?.beginRefresh()//开始获取数据
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationTitle(NSLocalizedString("我的钱包", comment: ""))
        setNavigationBarLeftButtonWithImageName("backArrow")
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        tableViewController = MUWalletPurchaseTableViewController()
        tableViewController?.viewModel = viewModel
        view.addSubview(tableViewController!.view)
        tableViewController?.view.snp.makeConstraints({ (make) in
            make.edges.equalTo(view)
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.tableViewController?.stopRefreshAnimation()
            self?.handelNetWorkError(error, withResult: result)
        }
    }
}
