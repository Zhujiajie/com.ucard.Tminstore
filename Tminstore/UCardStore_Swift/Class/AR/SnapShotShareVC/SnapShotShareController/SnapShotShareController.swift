//
//  SnapShotShareController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage
import IQKeyboardManagerSwift

class SnapShotShareController: BaseViewController {

    fileprivate let viewModel: SnapShotShareViewModel = SnapShotShareViewModel()
    
    var snapShotImage: UIImage?
    
    fileprivate var scrollView: UIScrollView?
    fileprivate var snapShotImageView: SnapShotImageView?
    fileprivate var shareView: VideoShareView?
    fileprivate var userRemark: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        automaticallyAdjustsScrollViewInsets = false
        
        let whiteView: UIView = UIView()
        whiteView.backgroundColor = UIColor.white
        view.addSubview(whiteView)
        whiteView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view).offset(-nav_statusHeight)
            make.left.bottom.right.equalTo(view)
        }
        
        setNavigationBar()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        transparentNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        showSVProgress(title: nil)
        let prefetcher: SDWebImagePrefetcher = SDWebImagePrefetcher.shared()
        prefetcher.options = SDWebImageOptions.highPriority
        prefetcher.prefetchURLs([appAPIHelp.watermarkImageURL!], progress: nil, completed: { [weak self] (count: UInt, skipCount: UInt) in
            
            if skipCount == 0{
                if let imageCache: SDImageCache = SDWebImageManager.shared().imageCache, let downloadedImage: UIImage = imageCache.imageFromCache(forKey: appAPIHelp.watermarkImageURL?.absoluteString){
                    //成功下载图片
                    self?.snapShotImage = UIImage.addWatemarkImage(withLogoImage: (self?.snapShotImage)!, translucentWatemarkImage: downloadedImage, translucentWatemarkImageRect: CGRect(x: 20/baseWidth, y: 20/baseWidth, width: 300/baseWidth, height: 48/baseWidth))//添加水印
                }
            }
            DispatchQueue.main.async{
                self?.setUI()
                self?.dismissSVProgress()
                self?.closures()
            }
        })
        
        IQKeyboardManager.sharedManager().enable = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK:设置导航栏
    fileprivate func setNavigationBar() {
        setNavigationBarLeftButtonWithImageName("backArrow")//设置左按钮
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D")] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("分享", comment: ""), attributeDic: attDic)
    }

    //MARK: 设置UI
    fileprivate func setUI(){
        scrollView = UIScrollView(frame: view.bounds)
        view.addSubview(scrollView!)
        scrollView?.backgroundColor = UIColor(hexString: "#F1F4F6")
        scrollView?.contentSize = CGSize(width: screenWidth, height: 625/baseWidth)
        
        snapShotImageView = SnapShotImageView()
        snapShotImageView?.image = snapShotImage
        scrollView?.addSubview(snapShotImageView!)
        snapShotImageView?.frame = CGRect(x: 0, y: 8/baseWidth, width: screenWidth, height: 476/baseWidth)
        
        shareView = VideoShareView()
        scrollView?.addSubview(shareView!)
        shareView?.frame = CGRect(x: 0, y: 487/baseWidth, width: screenWidth, height: 140/baseWidth)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 获得备注
        snapShotImageView?.getContentTextSuccess = {[weak self] (text: String) in
            self?.userRemark = text
        }
        
        //MARK: 点击分享按钮的事件
        shareView?.touchButtonClosure = {[weak self] (type: ShareButtonType) in
            if self?.snapShotImage == nil {return}
            if type == ShareButtonType.more{
                self?.iOSShare()
            }else{
                self?.viewModel.shareToThreePath(description: (self?.userRemark)!, andImage: (self?.snapShotImage)!, andShareChannel: type)
            }
        }
        
        //MARK: 分享成功
        viewModel.shareSuccess = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("分享成功", comment: ""))
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, _) in
            self?.handelNetWorkError(error)
        }
    }
    
    //MARK: 系统自带分享
    fileprivate func iOSShare(){
        let ac: UIActivityViewController = UIActivityViewController(activityItems: [snapShotImage!], applicationActivities: nil)
        present(ac, animated: true, completion: nil)
    }
}
