//
//  SnapShotImageView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class SnapShotImageView: UIView, UITextViewDelegate {
    
    /// 成功获得文字
    var getContentTextSuccess: ((_ text: String)->())?
    
    /// 屏幕截图
    var image: UIImage?{
        didSet{
            imageView?.image = image
        }
    }
    
    fileprivate var imageView: UIImageView?
    fileprivate var textView: FSTextView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = UIImageView()
        imageView?.contentMode = UIViewContentMode.scaleAspectFit
        addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(8/baseWidth)
            make.left.equalTo(self).offset(8/baseWidth)
            make.right.equalTo(self).offset(-8/baseWidth)
            make.height.equalTo(413/baseWidth)
        })
        
        textView = FSTextView()
        textView?.font = UIFont.systemFont(ofSize: 12)
        textView?.placeholder = NSLocalizedString("点击添加文字描述...", comment: "")
        addSubview(textView!)
        textView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(imageView!.snp.bottom).offset(8/baseWidth)
            make.left.equalTo(self).offset(8/baseWidth)
            make.right.equalTo(self).offset(-8/baseWidth)
            make.height.equalTo(40/baseWidth)
            make.bottom.equalTo(self).offset(-8/baseWidth)
        })
        textView?.delegate = self
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text != ""{
            getContentTextSuccess?(textView.text)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
