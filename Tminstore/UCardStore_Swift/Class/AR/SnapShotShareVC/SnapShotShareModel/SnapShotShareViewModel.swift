//
//  SnapShotShareViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class SnapShotShareViewModel: BaseViewModel {

    /// 分享成功的闭包
    var shareSuccess: (()->())?
    /// 出错的闭包
//    var errorClosure: ((_ error: Error?)->())?
    
    
    
    //MARK: 分享功能
    /// 分享功能
    ///
    /// - Parameters:
    ///   - description: 描述
    ///   - image: 图片
    ///   - shareButtonType: 分享渠道
    func shareToThreePath(description: String, andImage image: UIImage, andShareChannel shareButtonType: ShareButtonType){
        
        var shareChannel: UMSocialPlatformType?
        
        switch shareButtonType {
        case ShareButtonType.weChatSpace:
            shareChannel = UMSocialPlatformType.wechatTimeLine
        case ShareButtonType.weChatFriend:
            shareChannel = UMSocialPlatformType.wechatSession
        case ShareButtonType.weiBo:
            shareChannel = UMSocialPlatformType.sina
        default:
            shareChannel = nil
            return
        }
        
        let shareObject: UMSocialMessageObject = UMSocialMessageObject()
        let shareImageObject: UMShareImageObject = UMShareImageObject.shareObject(withTitle: "来自时记的图片分享", descr: description, thumImage: image)
        shareImageObject.shareImage = image
        shareObject.shareObject = shareImageObject
        
        UMSocialManager.default().share(to: shareChannel!, messageObject: shareObject, currentViewController: nil) { [weak self] (_, error: Error?) in
            if error != nil{
                self?.errorClosure?(error, nil)
            }else{
                self?.shareSuccess?()
            }
        }
    }
}

