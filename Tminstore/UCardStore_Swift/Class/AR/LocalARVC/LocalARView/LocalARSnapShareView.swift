//
//  LocalARSnapShareView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/24.
//  Copyright © 2017年 ucard. All rights reserved.
//
//MARK: 提示用户截屏的视图
import UIKit
import SnapKit

class LocalARSnapShareView: UIView {

    /// 点击分享视图的事件
    var tapShareViewClosure: (()->())?
    
    /// 计时五秒
    var stopTimerClosure: (()->())?
    
    fileprivate var timer: Timer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(white: 1.0, alpha: 0.74)
        
        layer.cornerRadius = 20/baseWidth
        layer.masksToBounds = true
        
        let shareIcon: UIImageView = UIImageView(image: UIImage(named: "shareArrowIcon"))
        shareIcon.contentMode = UIViewContentMode.scaleAspectFit
        addSubview(shareIcon)
        shareIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(self)
            make.right.equalTo(self).offset(-13/baseWidth)
            make.size.equalTo(CGSize(width: 8.6/baseWidth, height: 15/baseWidth))
        }
        
        let shareLabel: UILabel = UILabel()
        shareLabel.textColor = UIColor(hexString: "8D8D8D")
        shareLabel.font = UIFont.systemFont(ofSize: 14)
        shareLabel.text = NSLocalizedString("分享", comment: "")
        addSubview(shareLabel)
        shareLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(self)
            make.right.equalTo(shareIcon.snp.left).offset(-7/baseWidth)
        }
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapShareView(tap:)))
        addGestureRecognizer(tap)
    }
    
    //MARK: 开始计时
    func startTimer(){
        if timer?.isValid == true{
            timer?.invalidate()
        }
        timer = Timer(timeInterval: 5, target: self, selector: #selector(stopTimer(timer:)), userInfo: nil, repeats: false)
        RunLoop.current.add(timer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    //MARK: 计时停止
    func stopTimer(timer: Timer){
        timer.invalidate()
        stopTimerClosure?()
    }
    
    //MARK: 点击分享视图的事件
    func tapShareView(tap: UITapGestureRecognizer){
        tapShareViewClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
