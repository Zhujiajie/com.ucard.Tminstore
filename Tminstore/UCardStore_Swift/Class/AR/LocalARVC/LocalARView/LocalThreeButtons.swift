//
//  LocalThreeButtons.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class LocalThreeButtons: UIView {

    /// 要停止录屏
    var shouldStopRecord: (()->())?
    /// 点击了录屏按钮
    var touchRecordButtonClosure: (()->())?
    /// 点击了时光轴按钮
    var touchTimeLineButtonClosure: (()->())?
    /// 点击了添加记忆按钮
    var touchAddVideoButtonClosure: (()->())?
    /// 录屏时间的label
    fileprivate var recordTimeLabel: BaseLabel?
    /// 录屏的计时器
    fileprivate var timer: Timer?
    /// 录屏的计时
    fileprivate var recordCount: Int = 0{
        didSet{
            recordTimeLabel?.text = "\(recordCount)s"
            if ovalShapeLayer != nil{
                ovalShapeLayer?.strokeEnd = CGFloat(recordCount)/CGFloat(30)
            }
        }
    }
    /// 时光轴按钮
    fileprivate var timeLineButton: BaseButton?
    /// 时光轴label
    fileprivate var timeLineLabel: BaseLabel?
    /// 录屏按钮
    fileprivate var recordButton: BaseButton?
    /// 录屏label
    fileprivate var recordLabel: BaseLabel?
    /// 留下记忆按钮
    fileprivate var addVideoButton: BaseButton?
    /// 留下记忆label
    fileprivate var addVideoLabel: BaseLabel?
    /// 录屏时的填充layer
    fileprivate var ovalShapeLayer: CAShapeLayer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //录屏
        recordButton = BaseButton.normalIconButton(iconName: "arRecord")
        addSubview(recordButton!)
        recordButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-38/baseWidth)
            make.size.equalTo(CGSize(width: 55/baseWidth, height: 55/baseWidth))
        })
        recordLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andText: NSLocalizedString("分享", comment: ""))
        addSubview(recordLabel!)
        recordLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(recordButton!.snp.bottom).offset(4/baseWidth)
            make.centerX.equalTo(recordButton!)
        })
        
        //时光轴
        timeLineButton = BaseButton.normalIconButton(iconName: "timeLine")
        addSubview(timeLineButton!)
        timeLineButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottomMargin.equalTo(recordButton!)
            make.right.equalTo(recordButton!.snp.left).offset(-50/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        })
        timeLineLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andText: NSLocalizedString("时光轴", comment: ""))
        addSubview(timeLineLabel!)
        timeLineLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottomMargin.equalTo(recordLabel!)
            make.centerX.equalTo(timeLineButton!)
        })
        
        //留下记忆
        addVideoButton = BaseButton.normalIconButton(iconName: "addMemory")
        addSubview(addVideoButton!)
        addVideoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottomMargin.equalTo(recordButton!)
            make.left.equalTo(recordButton!.snp.right).offset(50/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        })
        addVideoLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andText: NSLocalizedString("留下记忆", comment: ""))
        addSubview(addVideoLabel!)
        addVideoLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottomMargin.equalTo(recordLabel!)
            make.centerX.equalTo(addVideoButton!)
        })
        
        //录屏时的事件label
        recordTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "FFFFFF")
        addSubview(recordTimeLabel!)
        recordTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(recordButton!)
            make.bottom.equalTo(recordButton!.snp.top).offset(-15/baseWidth)
        })
        recordTimeLabel?.isHidden = true
        
        recordButton?.touchButtonClosure = {[weak self] _ in
            self?.touchRecordButtonClosure?()
        }
        timeLineButton?.touchButtonClosure = {[weak self] _ in
            self?.touchTimeLineButtonClosure?()
        }
        addVideoButton?.touchButtonClosure = {[weak self] _ in
            self?.touchAddVideoButtonClosure?()
        }
    }
    
    //MARK: 按钮隐藏或显示的动画
    /// 按钮隐藏或显示的动画
    ///
    /// - Parameter isHide: 是否是隐藏
    func hideOrShowButton(isHide: Bool){
        recordButton?.snp.updateConstraints({ (make: ConstraintMaker) in
            if isHide {
                make.bottom.equalTo(self).offset(screenHeight)
            }else{
                make.bottom.equalTo(self).offset(-38/baseWidth)
            }
        })
        autoLayoutAnimation(view: self)
    }
    
    //MARK: 进入录屏状态
    func beginRecord(){
        recordButton?.setImage(UIImage(named: "arRecording"), for: UIControlState.normal)
        recordTimeLabel?.isHidden = false
        recordCount = 0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.timeLineButton?.alpha = 0
            self?.timeLineLabel?.alpha = 0
            self?.recordLabel?.alpha = 0
            self?.addVideoButton?.alpha = 0
            self?.addVideoLabel?.alpha = 0
        }, completion: nil)
        
        if timer?.isValid == true{
            timer?.invalidate()
        }
        timer = Timer(timeInterval: 1, target: self, selector: #selector(recordCount(timer:)), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: RunLoopMode.defaultRunLoopMode)
        if ovalShapeLayer == nil{
            ovalShapeLayer = CAShapeLayer()
            ovalShapeLayer?.strokeColor = UIColor.white.cgColor//外部线条颜色
            ovalShapeLayer?.fillColor = UIColor.clear.cgColor//内部填充颜色
            ovalShapeLayer?.lineWidth = 4
            let bounds: CGRect = recordButton!.bounds
            ovalShapeLayer?.path = UIBezierPath(arcCenter: CGPoint(x: bounds.width/2, y: bounds.height/2), radius: bounds.height/2, startAngle: CGFloat.pi, endAngle: CGFloat.pi * 3, clockwise: true).cgPath//填充路径，以及开始的位置
            recordButton?.layer.addSublayer(ovalShapeLayer!)
            ovalShapeLayer?.strokeEnd = 0.0
        }
    }
    
    //MARK: 停止录屏
    func stopRecord(){
        if timer?.isValid == true{
            timer?.invalidate()
        }
        recordButton?.setImage(UIImage(named: "arRecord"), for: UIControlState.normal)
        recordTimeLabel?.isHidden = true
        recordCount = 0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.timeLineButton?.alpha = 1
            self?.timeLineLabel?.alpha = 1
            self?.recordLabel?.alpha = 1
            self?.addVideoButton?.alpha = 1
            self?.addVideoLabel?.alpha = 1
            }, completion: nil)
    }
    
    //MARK: 录屏的计时器
    func recordCount(timer: Timer){
        recordCount += 1
        if recordCount >= 30{
            shouldStopRecord?()
            timer.invalidate()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
