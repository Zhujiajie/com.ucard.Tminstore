//
//  LocalARPlayer.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/27.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class LocalARPlayer: UIView {
    
    var postcardModel: PostcardModel?{
        didSet{
            if postcardModel != oldValue{
                setData()
            }
        }
    }
    
    /// 点击评论按钮的事件
    var touchCommentButtonClosure: (()->())?
    /// 点击点赞按钮的事件
    var touchLikeButtonClosure: ((_ like: Bool, _ videoId: String)->())?
    /// 点击用户头像的事件
    var tapUserPortaitView: ((_ memberCode: String)->())?
    /// 获取摄像头数据成功
    var getCameraSuccess: (()->())?
    
    /// 视频的完整路径
    var videoURL: URL?
    
    fileprivate var session: AVCaptureSession?
    
    /// 展示视频缩略的imageView
    fileprivate var videoThumbnalImageView: BaseImageView?
    /// 用来展示摄像头的layer
    fileprivate var cameraLayer: AVCaptureVideoPreviewLayer?
    /// 用来展示摄像头的layer
    fileprivate var player: ZTPlayer?
    /// 重新播放的按钮
    fileprivate var playAgainButton: BaseImageView?
    /// 加载动画
    fileprivate var activeIndicator: UIActivityIndicatorView?
    /// 评论的按钮
    fileprivate var commentButton: BaseButton?
    /// 点赞的按钮
    fileprivate var likeButton: BaseButton?
    
//    /// 播放器的缩放比例
//    fileprivate var lastCenter: CGPoint = CGPoint.zero
    
    /// 播放器的原始transform
    fileprivate var originalTransform: CGAffineTransform?
    
    /// 用户头像
    fileprivate var userPortraitView: BaseImageView?
    /// 用户昵称
    fileprivate var nickNameLabel: BaseLabel?
    /// 时间label
    fileprivate var timeLabel: BaseLabel?
//    /// 退出视频选择按钮
//    fileprivate var dismissVideoChooseButton: BaseButton?
    
    /// 拖动手势
    fileprivate var pan: UIPanGestureRecognizer?
    /// 缩放手势
    fileprivate var pinch: UIPinchGestureRecognizer?
    /// 旋转手势
    fileprivate var rotate: UIRotationGestureRecognizer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        player = ZTPlayer()
        player?.view.frame = CGRect(x: 21/baseWidth, y: 102/baseWidth, width: 334/baseWidth, height: 337/baseWidth)
        player?.playbackLoops = true
        player?.playerDelegate = self
        player?.playbackDelegate = self
        player?.layerBackgroundColor = UIColor.clear//背景透明
        addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            self?.playAgainButton?.isHidden = false
        }
        
//        lastCenter = player!.view.center
        originalTransform = player!.view.transform//保存原始的originalTransform
        
        //视频缩略图
        videoThumbnalImageView = BaseImageView.normalImageView()
        insertSubview(videoThumbnalImageView!, belowSubview: player!.view)
        videoThumbnalImageView?.frame = player!.view.frame
        
        //视频加载指示器
        activeIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        videoThumbnalImageView?.addSubview(activeIndicator!)
        activeIndicator?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(videoThumbnalImageView!)
        })
        activeIndicator?.hidesWhenStopped = true
        
        //添加手势。控制播放暂停
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToControlPlayer(tap:)))
        player?.view.addGestureRecognizer(tap)
        
        //添加手势移动
        pan = UIPanGestureRecognizer(target: self, action: #selector(moveGesture(pan:)))
        player!.view.addGestureRecognizer(pan!)
        
        //添加手势缩放
        pinch = UIPinchGestureRecognizer(target: self, action: #selector(scaleGesture(pinch:)))
        player!.view.addGestureRecognizer(pinch!)
        
        //旋转的手势
        rotate = UIRotationGestureRecognizer(target: self, action: #selector(rotateGesture(rotate:)))
        player!.view.addGestureRecognizer(rotate!)
        
        //重新开始播放的按钮
        playAgainButton = BaseImageView.normalImageView(andImageName: "playButton")
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 80/baseWidth, height: 80/baseWidth))
        })
        playAgainButton?.isHidden = true
        
        //用户头像
        userPortraitView = BaseImageView.userPortraitView(cornerRadius: 20.5/baseWidth)
        addSubview(userPortraitView!)
        userPortraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(self).offset(23/baseWidth)
            make.bottom.equalTo(player!.view.snp.top).offset(-49/baseWidth)
            make.size.equalTo(CGSize(width: 41/baseWidth, height: 41/baseWidth))
        })
        userPortraitView?.addTapGesture()
        //MARK: 点击用户头像的事件
        userPortraitView?.tapImageView = {[weak self] _ in
            self?.tapUserPortaitView?((self?.postcardModel?.memberCode)!)
        }
        
        //用户昵称
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium)
        } else {
            font = UIFont.systemFont(ofSize: 18)
        }
        nickNameLabel = BaseLabel.normalLabel(font: font, andTextColorHex: "FFFFFF")
        addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(userPortraitView!)
            make.left.equalTo(userPortraitView!.snp.right).offset(5/baseWidth)
        })
        
        //时间
        timeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "FFFFFF")
        addSubview(timeLabel!)
        timeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(nickNameLabel!)
            make.top.equalTo(nickNameLabel!.snp.bottom)
        })
        
//        //退出选择视频的按钮
//        dismissVideoChooseButton = BaseButton.normalIconButton(iconName: "dismissPlayer")
//        addSubview(dismissVideoChooseButton!)
//        dismissVideoChooseButton?.snp.makeConstraints({ (make: ConstraintMaker) in
//            make.right.equalTo(self).offset(-31/baseWidth)
//            make.bottom.equalTo(player!.view.snp.top).offset(-24/baseWidth)
//            make.size.equalTo(CGSize(width: 30/baseWidth, height: 30/baseWidth))
//        })
//        dismissVideoChooseButton?.touchButtonClosure = {[weak self] _ in
//            self?.quitVideoChoose()
//        }
        
        //视频评论的按钮
        commentButton = BaseButton.normalIconButton(iconName: "videoReply")
        addSubview(commentButton!)
        commentButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self).offset(-56.5/baseWidth)
            make.top.equalTo(player!.view.snp.bottom).offset(11/baseWidth)
        })
        commentButton?.touchButtonClosure = {[weak self] _ in
            self?.touchCommentButtonClosure?()
        }
        
        //视频点赞的按钮
        likeButton = BaseButton.normalIconButton(iconName: "videoLike")
        addSubview(likeButton!)
        likeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(commentButton!)
            make.centerX.equalTo(self).offset(56.5/baseWidth)
        })
        likeButton?.touchButtonClosure = {[weak self] _ in
            self?.postcardModel?.isLiked = !(self?.postcardModel?.isLiked)!
            self?.touchLikeButtonClosure?((self?.postcardModel?.isLiked)!, (self?.postcardModel?.videoId)!)
            self?.setLikeButton(like: (self?.postcardModel?.isLiked)!)
        }
        
        userPortraitView?.alpha = 0
        nickNameLabel?.alpha = 0
        timeLabel?.alpha = 0
        commentButton?.alpha = 0
        likeButton?.alpha = 0
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {[weak self] _ in
            self?.getCameraPreview()
        }
    }
    
    //MARK: 重新开始播放播放器
    func replayPlayer(){
        if player?.playbackState == PlaybackState.paused{
            player?.playFromCurrentTime()
            playAgainButton?.isHidden = true
        }
    }
    
    //MARK: 停止获取摄像头数据
    /// 停止获取摄像头数据
    func stopSession(){
        if session?.isRunning == true{
            if let input: AVCaptureDeviceInput = session?.inputs.first as? AVCaptureDeviceInput{
                session?.removeInput(input)
                session?.stopRunning()
                session = nil
                cameraLayer?.removeFromSuperlayer()
                cameraLayer = nil
            }
        }
    }
    
    //MARK: 获取摄像头的捕捉到影像
    /**
     获取摄像头的捕捉到影像
     
     - parameter cell: PhotoCollectionViewCell
     */
    func getCameraPreview() {
        
        session = AVCaptureSession()
        session?.sessionPreset = AVCaptureSessionPreset1920x1080
        
        if let device: AVCaptureDevice = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).first as? AVCaptureDevice {
            
            do{
                let input: AVCaptureDeviceInput = try AVCaptureDeviceInput(device: device)
                session?.addInput(input)
                cameraLayer = AVCaptureVideoPreviewLayer(session: session)
                cameraLayer!.frame = self.bounds
                session?.startRunning()
                DispatchQueue.main.async(execute: {[weak self] _ in
                    self?.layer.insertSublayer((self?.cameraLayer)!, below: self?.videoThumbnalImageView?.layer)//添加视图
                    self?.getCameraSuccess?()
                })
                
            }catch{print(error.localizedDescription) }
        }
    }
    
    //MARK: 暂停播放
    /// 暂停播放
    func pausePlay(){
        if player?.playbackState == PlaybackState.playing{
            player?.pause()
            playAgainButton?.isHidden = false
        }
    }
    
    //MARK: 开始播放
    /// 重新开始播放
    func rePlay(){
        if player?.playbackState == PlaybackState.paused && player?.url != nil{
            player?.playFromCurrentTime()
        }
    }
    
    //MARK: 点击开始播放，或者停止播放
    func tapToControlPlayer(tap: UITapGestureRecognizer){
        if player?.playbackState == PlaybackState.playing{
            player?.pause()
            playAgainButton?.isHidden = false
        }else if player?.playbackState == PlaybackState.paused{
            player?.playFromCurrentTime()
            playAgainButton?.isHidden = true
        }
    }
    
    //MARK: 拖动的手势
    func moveGesture(pan: UIPanGestureRecognizer){
        switch pan.state {
        case UIGestureRecognizerState.began, UIGestureRecognizerState.changed:
            if let view: UIView = pan.view{
                let movePoint: CGPoint = pan.translation(in: view)
//                lastCenter = CGPoint(x: lastCenter.x + movePoint.x, y: lastCenter.y + movePoint.y)
                view.transform = view.transform.translatedBy(x: movePoint.x, y: movePoint.y)
                videoThumbnalImageView?.transform = videoThumbnalImageView!.transform.translatedBy(x: movePoint.x, y: movePoint.y)
            }
        default:

//            //防止播放器移除屏幕范围
//            if lastCenter.x < 0 || lastCenter.x > screenWidth || lastCenter.y < 0 || lastCenter.y > screenHeight{
//                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
//                    self?.player?.view.transform = CGAffineTransform.identity
//                    }, completion: {[weak self] _ in
//                       self?.lastCenter = (self?.player?.view.center)!
//                })
//            }
            break
        }
        pan.setTranslation(CGPoint.zero, in: pan.view)
    }
    
    //MARK: 缩放手势
    func scaleGesture(pinch: UIPinchGestureRecognizer){
        switch pinch.state {
        case UIGestureRecognizerState.began, UIGestureRecognizerState.changed:
            if let view: UIView = pinch.view{
                view.transform = view.transform.scaledBy(x: pinch.scale, y: pinch.scale)
                videoThumbnalImageView?.transform = videoThumbnalImageView!.transform.scaledBy(x: pinch.scale, y: pinch.scale)
                pinch.scale = 1.0
            }
        default:
            if let view: UIView = pinch.view, let currentScale: CGFloat = view.layer.value(forKeyPath: "transform.scale") as? CGFloat{
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
                    if currentScale > 2.0{
                        view.transform = (self?.originalTransform)!.scaledBy(x: 2.0, y: 2.0)
                        self?.videoThumbnalImageView?.transform = (self?.originalTransform)!.scaledBy(x: 2.0, y: 2.0)
                    }else if currentScale < 0.75{
                        view.transform = (self?.originalTransform)!.scaledBy(x: 0.75, y: 0.75)
                        self?.videoThumbnalImageView?.transform = (self?.originalTransform)!.scaledBy(x: 0.75, y: 0.75)
                    }
                }, completion: nil)
            }
        }
    }
    
    //MARK: 旋转的手势
    func rotateGesture(rotate: UIRotationGestureRecognizer){
        if let view: UIView = rotate.view{
            view.transform = view.transform.rotated(by: rotate.rotation)
            videoThumbnalImageView?.transform = videoThumbnalImageView!.transform.rotated(by: rotate.rotation)
        }
        rotate.rotation = 0//重置旋转角度
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        videoThumbnalImageView?.image = nil
        videoThumbnalImageView?.sd_setImage(with: postcardModel?.videoThumbnailURL)
        activeIndicator?.startAnimating()
        
        userPortraitView?.sd_setImage(with: postcardModel?.userLogoURL)
        nickNameLabel?.text = postcardModel?.nickName
        if let date: Date = postcardModel?.createDate{
            timeLabel?.text = getDateText(date: date, andDateStyle: DateFormatter.Style.short)
        }
        
        if let url: URL = postcardModel?.videoURL{
            playAgainButton?.isHidden = true
            player?.setVideoFullURL(fullVideoURL: url)
            player?.playFromBeginning()
            videoURL = url
        }
        
        setLikeButton(like: postcardModel!.isLiked)
    }
    
    //MARK: 设置点赞按钮的ICON
    fileprivate func setLikeButton(like: Bool){
        if like == true{
            likeButton?.setImage(UIImage(named: "videoLike"), for: UIControlState.normal)
        }else{
            likeButton?.setImage(UIImage(named: "videoUnlike"), for: UIControlState.normal)
        }
    }
    
    //MARK: 进入视频选择的状态
    /// 进入视频选择的状态, 播放器状态复原，并禁用交互手势。其他UI开始出现
    func beginChooseVideo(){
        pan?.isEnabled = false
        pinch?.isEnabled = false
        rotate?.isEnabled = false
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.5, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.player?.view.transform = (self?.originalTransform)!
            self?.videoThumbnalImageView?.transform = (self?.originalTransform)!
        }, completion: nil)
        UIView.animate(withDuration: 0.5, delay: 0.5, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.userPortraitView?.alpha = 1
            self?.nickNameLabel?.alpha = 1
            self?.timeLabel?.alpha = 1
            self?.commentButton?.alpha = 1
            self?.likeButton?.alpha = 1
        }, completion: nil)
    }
    
    //MARK: 点击退出视频选择按钮的事件
    /// 退出视频选择，隐藏各种UI，并开启交互手势
    func quitVideoChoose(){
        pan?.isEnabled = true
        pinch?.isEnabled = true
        rotate?.isEnabled = true
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.userPortraitView?.alpha = 0
            self?.nickNameLabel?.alpha = 0
            self?.timeLabel?.alpha = 0
            self?.commentButton?.alpha = 0
            self?.likeButton?.alpha = 0
            }, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LocalARPlayer: PlayerDelegate, PlayerPlaybackDelegate{
    func playerReady(_ player: Player) {
    }
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    //MARK: 根据缓存情况，展示加载提示器
    func playerBufferingStateDidChange(_ player: Player) {
        switch player.bufferingState {
        case BufferingState.ready:
            activeIndicator?.stopAnimating()
        default:
//            activeIndicator?.startAnimating()
            break
        }
    }
    
    func playerCurrentTimeDidChange(_ player: Player){
    }
    func playerPlaybackWillStartFromBeginning(_ player: Player){
    }
    func playerPlaybackDidEnd(_ player: Player){
    }
    func playerPlaybackWillLoop(_ player: Player){
    }
}
