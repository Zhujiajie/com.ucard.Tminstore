//
//  LocalARTimeLineCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/25.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 时光轴上的cell
import UIKit
import SnapKit

let LocalTimeLineCellNotificationName: String = "LocalTimeLineCellNotificationName"

class LocalARTimeLineCollectionViewCell: UICollectionViewCell {
    
    var postcardModel: PostcardModel?{
        didSet{
            setData()
        }
    }
    
    ///年label
    fileprivate var yearLabel: UILabel?
    ///月日Label
    fileprivate var monthAndDayLabel: UILabel?
    /// 正在播放的背景
    fileprivate var playingIndicatiorView: UIView?
    /// 缩略图
    fileprivate var thumbnailImageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        yearLabel = UILabel()
        yearLabel?.textColor = UIColor.white
        yearLabel?.font = UIFont.systemFont(ofSize: 12)
        contentView.addSubview(yearLabel!)
        yearLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.centerX.equalTo(contentView)
        })
        
        monthAndDayLabel = UILabel()
        monthAndDayLabel?.textColor = UIColor.white
        monthAndDayLabel?.font = UIFont.systemFont(ofSize: 8)
        contentView.addSubview(monthAndDayLabel!)
        monthAndDayLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(yearLabel!.snp.bottom).offset(3/baseWidth)
            make.centerX.equalTo(contentView)
        })
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#DBDBDB")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(40/baseWidth)
            make.left.equalTo(contentView)
            make.right.equalTo(contentView).offset(1)
            make.height.equalTo(2)
        }
        
        let timeLineDot: UIImageView = UIImageView(image: UIImage(named: "timeLineDot"))
        timeLineDot.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(timeLineDot)
        timeLineDot.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(grayLine)
            make.size.equalTo(CGSize(width: 17/baseWidth, height: 17/baseWidth))
        }
        
        playingIndicatiorView = UIView()
        playingIndicatiorView?.backgroundColor = UIColor(hexString: "4CDFED")
        contentView.addSubview(playingIndicatiorView!)
        playingIndicatiorView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayLine.snp.bottom).offset(10/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 69/baseWidth, height: 69/baseWidth))
        })
        
        thumbnailImageView = UIImageView()
        thumbnailImageView?.contentMode = UIViewContentMode.scaleAspectFill
        contentView.addSubview(thumbnailImageView!)
        thumbnailImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(playingIndicatiorView!)
            make.size.equalTo(CGSize(width: 62/baseWidth, height: 62/baseWidth))
        })
        thumbnailImageView?.layer.cornerRadius = 5
        thumbnailImageView?.layer.masksToBounds = true
        
        let playIcon: UIImageView = UIImageView(image: UIImage(named: "playButton"))
        playIcon.contentMode = UIViewContentMode.scaleAspectFit
        thumbnailImageView?.addSubview(playIcon)
        playIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(thumbnailImageView!)
            make.size.equalTo(CGSize(width: 20/baseWidth, height: 20/baseWidth))
        }
        
        //添加通知
        NotificationCenter.default.addObserver(self, selector: #selector(setPlayerStateToFalse(info:)), name: NSNotification.Name(rawValue: LocalTimeLineCellNotificationName), object: nil)
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        if let createDate: Date = postcardModel?.createDate{
            let calendar: Calendar = Calendar.current
            let components: DateComponents = calendar.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year, Calendar.Component.hour, Calendar.Component.minute], from: createDate)
            
            if let year: Int = components.year{
                yearLabel?.text = "\(year)"
            }
            if let month: Int = components.month, let day: Int = components.day{
                monthAndDayLabel?.text = "\(month)" + NSLocalizedString("月", comment: "") + "\(day)"
            }
        }
        
        thumbnailImageView?.sd_setImage(with: postcardModel?.videoThumbnailURL)
    }
    
    //MARK: 设置播放状态
    /// 设置播放状态
    ///
    /// - Parameter isPlaying: 是否在播放
    func setPlayState(isPlaying: Bool){
        if isPlaying == true{
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: LocalTimeLineCellNotificationName), object: nil)
        }
        playingIndicatiorView?.isHidden = !isPlaying
    }
    
    //MARK: 接受通知，将cell的播放状态设置为不播放
    func setPlayerStateToFalse(info: Notification){
        setPlayState(isPlaying: false)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        yearLabel?.text = nil
        monthAndDayLabel?.text = nil
        thumbnailImageView?.image = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
