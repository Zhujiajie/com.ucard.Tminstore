//
//  LocalInfoView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 展示线索图片的视图

import UIKit
import SnapKit

class LocalInfoView: UIImageView {

    /// 数据源
    var annotation: ZTAnnotation?{
        didSet{
            setData()
        }
    }
    
    /// 头像
    fileprivate var portraitView: BaseImageView?
    /// 昵称
    fileprivate var nickNameLabel: BaseLabel?
    /// 事件
    fileprivate var timeLabel: BaseLabel?
    /// 介绍
    fileprivate var infoLabel: BaseLabel?
    /// 线索图片
    fileprivate var clueImageView: BaseImageView?
    /// 地址label
    fileprivate var addressLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        alpha = 0.8
        contentMode = UIViewContentMode.scaleAspectFill
        sd_setImage(with: appAPIHelp.clueBackgroundImageURL)
        
        portraitView = BaseImageView.userPortraitView(cornerRadius: 20.5/baseWidth)
        addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(11/baseWidth)
            make.left.equalTo(self).offset(11/baseWidth)
            make.size.equalTo(CGSize(width: 41/baseWidth, height: 41/baseWidth))
        })
        
        timeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 10), andTextColorHex: "B8B8B8", andTextAlignment: NSTextAlignment.right)
        addSubview(timeLabel!)
        timeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(15/baseWidth)
            make.right.equalTo(self).offset(-11/baseWidth)
        })
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium)
        } else {
            font = UIFont.systemFont(ofSize: 18)
        }
        
        nickNameLabel = BaseLabel.normalLabel(font: font, andTextColorHex: "4D4D4D")
        addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.left.equalTo(portraitView!.snp.right).offset(5/baseWidth)
            make.right.equalTo(timeLabel!.snp.left).offset(-2)
        })
        
        infoLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "B8B8B8")
        addSubview(infoLabel!)
        infoLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(3/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(timeLabel!)
        })
        
        clueImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        addSubview(clueImageView!)
        clueImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(70/baseWidth)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 271/baseWidth, height: 271/baseWidth))
        })
        
        let addressIcon: BaseImageView = BaseImageView.normalImageView(andImageName: "clueLocationIcon")
        addSubview(addressIcon)
        addressIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(clueImageView!.snp.bottom).offset(12/baseWidth)
            make.left.equalTo(self).offset(21/baseWidth)
            make.size.equalTo(CGSize(width: 21/baseWidth, height: 21/baseWidth))
        }
        
        addressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "999999", andNumberOfLines: 2)
        addSubview(addressLabel!)
        addressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(addressIcon)
            make.left.equalTo(addressIcon.snp.right).offset(2/baseWidth)
            make.right.equalTo(self).offset(-31/baseWidth)
        })
    }
    
    //MARK: 设置内容
    fileprivate func setData(){
        
        guard let postcard: PostcardModel = annotation?.postcardModel else { return }
        
        portraitView?.sd_setImage(with: postcard.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
        
        if let date: Date = postcard.createDate{
            timeLabel?.text = getDateText(date: date, andDateStyle: DateFormatter.Style.short)
        }
        nickNameLabel?.text = postcard.nickName
        infoLabel?.text = postcard.remark
        clueImageView?.sd_setImage(with: postcard.frontImageURL)
        addressLabel?.text = postcard.detailedAddress
    }
    
    //MARK: dismissAnimation
    /// 消失的动画
    func dismissAnimation(){
        UIView.animate(withDuration: 0.3, animations: { [weak self] _ in
            self?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }, completion: {[weak self] (_) in
            self?.removeFromSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
