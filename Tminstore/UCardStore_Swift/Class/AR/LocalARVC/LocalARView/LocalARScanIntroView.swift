//
//  LocalARScanIntroView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/8.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 用户刚进入时光轴时，弹出的介绍视图

import UIKit
import SnapKit

class LocalARScanIntroView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.57)
        
        //放置介绍的
        let labelContainerView: UIView = UIView()
        labelContainerView.backgroundColor = UIColor(white: 1.0, alpha: 0.82)
        addSubview(labelContainerView)
        labelContainerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(self)
            make.size.equalTo(CGSize(width: 140, height: 80))
        }
        
        
        
        //需要镂空的位置
        let iconFrame: CGRect = CGRect(x: screenWidth/2 - 13.5, y: screenHeight - 37.5, width: 27, height: 27)
        //外层蒙版的路径
        let path: UIBezierPath = UIBezierPath(rect: UIScreen.main.bounds)
        //镂空蒙版的路径
        let emptyPath: UIBezierPath = UIBezierPath(roundedRect: iconFrame, cornerRadius: 5)
        path.append(emptyPath.reversing())
        
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = UIColor.clear.cgColor
        
        layer.mask = shapeLayer
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
