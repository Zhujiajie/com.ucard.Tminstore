//
//  LocalARBarButtonItem.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 导航栏上的按钮

import UIKit

class LocalARBarButtonItem: UIBarButtonItem {

    /// 点击了出现更多的按钮
    var touchMoreButtonClosure: (()->())?
    /// 是否需要自己控制闪光灯
    var shouldControllerTorchSelf: Bool = false
    
    /// 是否正在录制
    var isRecord: Bool = false{
        didSet{
            if isRecord == true{
                self.image = UIImage(named: "arRecording")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            }else{
                self.image = UIImage(named: "arRecord")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
            }
        }
    }
    
    //MARK: 在录屏时，打开闪光灯的按钮
    class func recordingTorchBarItem()->LocalARBarButtonItem{
        let barButton: LocalARBarButtonItem = LocalARBarButtonItem()
        barButton.image = UIImage(named: "arRecord")
        barButton.style = UIBarButtonItemStyle.plain
        barButton.target = barButton
        barButton.action = #selector(barButton.touchMoreBarButton(barItem:))
        barButton.tintColor = UIColor(hexString: "5BD8DA")
        return barButton
    }
    
    //MARK: 设置点击录屏的按钮
    /// 设置点击录屏的按钮
    ///
    /// - Returns: LocalARBarButtonItem
    class func recordBarItem()->LocalARBarButtonItem{
        
        let barButton: LocalARBarButtonItem = LocalARBarButtonItem()
        barButton.image = UIImage(named: "arRecord")
        barButton.style = UIBarButtonItemStyle.plain
        barButton.target = barButton
        barButton.action = #selector(barButton.touchMoreBarButton(barItem:))
        barButton.tintColor = UIColor(hexString: "5BD8DA")
        return barButton
    }
    
    
    //MARK: 设置闪光灯按钮
    /// 设置闪光灯按钮
    ///
    /// - Returns: LocalARBarButtonItem
    class func torchBarItem()->LocalARBarButtonItem{
        
        let barItem: LocalARBarButtonItem = LocalARBarButtonItem()
        barItem.image = UIImage(named: "localTorchOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchTorchButton(barItem:))
        barItem.tintColor = UIColor.white
        return barItem
    }
    
    //MARK: 切换摄像头正反面的按钮
    /// 切换摄像头正反面的按钮
    ///
    /// - Returns: LocalARBarButtonItem
    class func switchCameraBarItem()->LocalARBarButtonItem{
        
        let barItem: LocalARBarButtonItem = LocalARBarButtonItem()
        barItem.image = UIImage(named: "rotateCamera")
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchMoreBarButton(barItem:))
        barItem.tintColor = UIColor.white
        
        return barItem
        
    }
    
    //MARK: 点击截屏的按钮
    /// 点击截屏的按钮
    ///
    /// - Returns: LocalARBarButtonItem
    class func snapShotBarItem()->LocalARBarButtonItem{
        let barItem: LocalARBarButtonItem = LocalARBarButtonItem()
        barItem.image = UIImage(named: "snapShotIcon")
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchMoreBarButton(barItem:))
        barItem.tintColor = UIColor.white
        
        return barItem
    }
    
    //MARK: 点击退出视频选择的按钮
    class func quitSpaceTimeBarItem()->LocalARBarButtonItem{
        let barItem: LocalARBarButtonItem = LocalARBarButtonItem()
        barItem.image = UIImage(named: "dismissPlayer")
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchMoreBarButton(barItem:))
        barItem.tintColor = UIColor.white
        
        return barItem
    }
    
    //MARK: 透明按钮
    class func clearBarItem()->LocalARBarButtonItem{
        let barItem: LocalARBarButtonItem = LocalARBarButtonItem()
        barItem.image = UIImage(named: "clearPlaceHolder")
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchMoreBarButton(barItem:))
        barItem.tintColor = UIColor.white
        
        return barItem
    }
    
    //MARK: 点击更多按钮的事件
    func touchMoreBarButton(barItem: UIBarButtonItem){
        touchMoreButtonClosure?()
    }
    
    //MARK: 点击按钮打开或者关闭闪光灯
    func touchTorchButton(barItem: UIBarButtonItem){
        if shouldControllerTorchSelf == true{
            touchMoreButtonClosure?()
            return
        }
        //设置
        let device: AVCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if device.hasTorch == true{
            if device.torchMode == AVCaptureTorchMode.on{
                setTorch(on: false)
            }else if device.torchMode == AVCaptureTorchMode.off{
                setTorch(on: true)
            }
        }
    }
    
    //MARK: 打开或者关闭闪光灯
    /// 打开或者关闭闪光灯
    ///
    /// - Parameter on: Bool
    func setTorch(on: Bool){
        
        //设置
        let device: AVCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        if device.hasTorch == true{
            if on == true && device.torchMode == AVCaptureTorchMode.off{
                do {
                    try device.lockForConfiguration()
                    device.torchMode = AVCaptureTorchMode.on
                    device.unlockForConfiguration()
                    image = UIImage(named: "localTorchOn")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                }catch{print(error)}
            }else if on == false && device.torchMode == AVCaptureTorchMode.on{
                do {
                    try device.lockForConfiguration()
                    device.torchMode = AVCaptureTorchMode.off
                    device.unlockForConfiguration()
                    image = UIImage(named: "localTorchOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                }catch{print(error)}
            }
            
        }
    }
}
