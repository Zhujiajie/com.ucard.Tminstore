//
//  LocalTimeLineCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 展示时光轴视频的collectionView
import UIKit
import SnapKit

class LocalTimeLineCollectionView: UIView {

    /// 左拉刷新
    var headerRefreshClosure: (()->())?
    /// 右拉刷新
    var footerRefreshClosure: (()->())?
    /// collectionView
    var collectionView: UICollectionView?
    
    /// 向下滑动的事件
    var swipeDownClosure: (()->())?
    /// 第一个cell，设置为播放状态
    var shouldSetFirstCellPlaying: Bool = true
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: 77/baseWidth, height: 124/baseWidth)
        
        collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        
        collectionView?.register(LocalARTimeLineCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.LocalARTimeLineCollectionViewCellReuseIdentifier)
        
        collectionView?.backgroundColor = UIColor.clear
        collectionView?.backgroundView = UIView()
        collectionView?.backgroundView?.backgroundColor = UIColor.clear
        
//        collectionView?.addRefreshFooter(closure: {[weak self] _ in
//            self?.footerRefreshClosure?()
//        })
//        collectionView?.addRefreshHeader(closure: {[weak self] _ in
//            self?.headerRefreshClosure?()
//        })
//
//        collectionView?.refreshFooterFont = UIFont.systemFont(ofSize: 11)
//        collectionView?.refreshFooterTextColor = UIColor(hexString: "878686")
//        collectionView?.refreshHeaderFont = collectionView?.refreshFooterFont
//        collectionView?.refreshHeaderTextColor = collectionView?.refreshFooterTextColor
        
        self.collectionView?.xzm_addNormalHeader(callback: {[weak self] _ in
            self?.headerRefreshClosure?()
        })
        
        self.collectionView?.xzm_addNormalFooter(callback: {[weak self] _ in
            self?.footerRefreshClosure?()
        })
        
        self.collectionView?.xzm_header.isUpdatedTimeHidden = true
        
        addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        
        let swipe: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeGesture(swipe:)))
        swipe.direction = UISwipeGestureRecognizerDirection.down
        addGestureRecognizer(swipe)
    }
    
    //MARK: 下滑收货时光轴
    func swipeGesture(swipe: UISwipeGestureRecognizer){
        swipeDownClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

///// 时间轴 Cell的大小
//let timeLineCellSize = CGSize(width: 110/baseWidth, height: 50/baseWidth)
//
//let xOffset: CGFloat = 50/baseWidth
//
//class LocalTimeLineLayout: UICollectionViewFlowLayout {
//    override init() {
//        super.init()
//        
//        self.itemSize = timeLineCellSize
//        self.minimumInteritemSpacing = 0
//        self.minimumLineSpacing = 0
//        self.scrollDirection = .horizontal
//    }
//    
//    //MARK: 重写itemSize
//    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
//        
//        var attArray: [UICollectionViewLayoutAttributes] = [UICollectionViewLayoutAttributes]()
//        let cellCount: Int = collectionView!.numberOfItems(inSection: 0)
//        
//        for i in 0..<cellCount {
//            
//            let indexPath = IndexPath(item: i, section: 0)
//            
//            let att = layoutAttributesForItem(at: indexPath)
//            
//            attArray.append(att!)
//        }
//        
//        return attArray
//    }
//    
//    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
//        
//        if let att: UICollectionViewLayoutAttributes = super.layoutAttributesForItem(at: indexPath)?.copy() as? UICollectionViewLayoutAttributes{
//            
//            if indexPath.item % 2 == 0{
//                att.frame.origin.x += xOffset
//            }
//            att.frame.size = timeLineCellSize
//            return att
//        }else{
//            return nil
//        }
//    }
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//}
