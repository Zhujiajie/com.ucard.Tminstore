//
//  LocalTimeLineIntroView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/28.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 各种介绍视图
import UIKit

class LocalTimeLineIntroView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentMode = UIViewContentMode.scaleToFill
        
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToDismiss(tap:)))
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
    
    func tapToDismiss(tap: UITapGestureRecognizer){
        dismissAnimation()
    }
    
    //MARK: 录屏介绍页面
    /// 录屏介绍页面
    ///
    /// - Returns: LocalTimeLineIntroView
    class func recordIntroImageView()->LocalTimeLineIntroView{
        let imageView: LocalTimeLineIntroView = LocalTimeLineIntroView(frame: UIScreen.main.bounds)
        imageView.sd_setImage(with: appAPIHelp.timeLineRecordIntroImage())
        return imageView
    }
    
    //MARK: 时光轴介绍页面
    /// 时光轴介绍页面
    ///
    /// - Returns: LocalTimeLineIntroView
    class func timeLineIntroImageView()->LocalTimeLineIntroView{
        let imageView: LocalTimeLineIntroView = LocalTimeLineIntroView(frame: UIScreen.main.bounds)
        imageView.sd_setImage(with: appAPIHelp.timeLineIntroImage())
        return imageView
    }
    
    //MARK: 消失的动画
    func dismissAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 0
            }, completion: {[weak self] (_) in
                self?.removeFromSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
