//
//  LocalARViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import Photos
import ReplayKit

class LocalARViewModel: BaseViewModel {

    fileprivate let dataModel: LocalARDataModel = LocalARDataModel()
    
    /// 已经停止录屏
    var recordStoped: (()->())?
    /// 需要展示录屏结果
    var shouldPresentRecordResult: ((_ vc: RPPreviewViewController)->())?
    /// 展示警告弹窗
    var showWarmAlert: (()->())?
    /// 停止录屏的弹窗
    var stopRecordAlert: (()->())?
    /// 开始录屏的闭包
    var startRecordClosure: (()->())?
    /// 截屏图片
    var snapShotImage: UIImage?
    /// 截屏视图
    var snapShotView: UIView?
    
    fileprivate var assetsFetchResults: PHFetchResult<PHAsset>?
    
    /// 原图的各种数据
    var postcardModel: PostcardModel?{
        didSet{
            doInBackground(inBackground: {[weak self] _ in
//                self?.firstCommentModel = CommentModel.initFromPostCardModel(postCard: (self?.postcardModel)!)
//                self?.pageNum = 1
//                self?.getSpaceTimeComment()
                if oldValue == nil{
                    self?.postcardArray.append((self?.postcardModel)!)
                }
                self?.pageNum = 1
                self?.showARList()
            })
        }
    }
    /// 数据源
    var mapModel: SpaceTimeDataModel?{
        didSet{
//            firstCommentModel = CommentModel.initFromSpaceTimeModel(mapModel: mapModel!)
            pageNum = 1
        }
    }
//    /// 原图转化而来的视频评论数据
//    var firstCommentModel: CommentModel?
    
    /// 向服务器端请求第几页数据
    var pageNum: Int = 1{
        didSet{
            if pageNum == 1{
                noMoreDataFlag = false
            }
        }
    }
    
    /// 成功获得数据
    var collectionViewShouldReload: ((_ shouldScrollToTop: Bool)->())?
    /// 没有更多数据了
    var noMoreData: (()->())?
    fileprivate var noMoreDataFlag: Bool = false
    
    /// 出错的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
//    /// 视频评论的数组
//    var arCommentArray: [CommentModel] = [CommentModel]()
    /// 包含视频信息的数组
    var postcardArray: [PostcardModel] = [PostcardModel]()
    
    override init() {
        super.init()
        
        //MARK: 失败的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 获取时光轴的所有视频
    /// 获取时光轴的所有视频
    func showARList(){
        
        if noMoreDataFlag == true{
            noMoreData?()
            return
        }
        
        var originalId: String
        if postcardModel != nil{
            originalId = postcardModel!.originalId
        }else{
            originalId = mapModel!.originalId
        }
        
        dataModel.showARList(originalId: originalId, andPageNum: pageNum, successClosure: {[weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if list.isEmpty == true{
                            self?.noMoreDataFlag = true
                            self?.noMoreData?()
                        }else{
                            if self?.pageNum == 1{
                                self?.postcardArray.removeAll()
                            }
                            for dic: [String: Any] in list{
                                if let _: String = dic["videoUrl"] as? String{
                                    let postcard: PostcardModel = PostcardModel.initFromSpaceTimeCircle(dic: dic)
                                    self?.postcardArray.append(postcard)
                                }
                            }
                            DispatchQueue.main.async {
                                self?.collectionViewShouldReload?(true)
                            }
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        })
    }
    
    //MARK: 发送点赞或者取消点赞请求
    /// 发送点赞或者取消点赞请求
    ///
    /// - Parameters:
    ///   - like: 点赞或者取消点赞
    ///   - videoId: 视频ID
    func sendLikeOrCancelLike(like: Bool, andVideoId videoId: String){
        dataModel.sendLikeOrCancelLike(like: like, andVideoId: videoId)
    }
    
//    //MARK: 获取视频评论
//    /// 获取视频评论
//    func getSpaceTimeComment(){
//        
//        if noMoreDataFlag == true{
//            noMoreData?()
//            return
//        }
//        var originalId: String
//        if postcardModel != nil{
//            originalId = postcardModel!.originalId
//        }else{
//            originalId = mapModel!.originalId
//        }
//        dataModel.showSpaceTimeComment(pageIndex: pageNum, andOriginalId: originalId) { [weak self] (result: [String : Any]) in
//            
//            if let code: Int = result["code"] as? Int, code == 1000{
//                if let data: [String: Any] = result["data"] as? [String: Any]{
//                    if let arComments: [[String: Any]] = data["arComments"] as? [[String: Any]]{
//                        if arComments.isEmpty == true{
//                            self?.noMoreDataFlag = true
//                            self?.noMoreData?()
//                        }else{
//                            var commentArray: [CommentModel] = [CommentModel]()
//                            for dic: [String: Any] in arComments{
//                                if dic["contentUrl"] != nil{
//                                    let commentModel: CommentModel = CommentModel.initFromDic(dic: dic)
//                                    commentArray.append(commentModel)
//                                }
//                            }
//                            var shouldScrollToTop: Bool
//                            if self?.pageNum == 1{
//                                self?.arCommentArray.removeAll()
//                                self?.arCommentArray.append((self?.firstCommentModel)!)
//                                shouldScrollToTop = true
//                            }else{
//                                shouldScrollToTop = false
//                            }
//                            self?.arCommentArray.append(contentsOf: commentArray)
//                            self?.collectionViewShouldReload?(shouldScrollToTop)
//                        }
//                        self?.pageNum += 1
//                        return
//                    }
//                }
//            }
//            self?.errorClosure?(nil, result)
//        }
//    }
    
    //MARK: 检测是否正在录屏
    func isRecording()->Bool{
        if #available(iOS 9.0, *) {
            return RPScreenRecorder.shared().isRecording
        }else{
            return false
        }
    }
    
    //MARK: 停止录屏
    /// 停止录屏
    ///
    /// - Parameter shouldDiscard: 是否需要展示弹窗
    func stopRecordAndDiscard(){
        if #available(iOS 9.0, *) {
            if RPScreenRecorder.shared().isAvailable == true{
                if RPScreenRecorder.shared().isRecording == true{
                    stopRecord(shouldDiscard: true)
                }
            }
        }
    }
    
    //MARK: 点击录屏按钮的事件
    /// 点击录屏按钮的事件
    func startRecord(){
        if RPScreenRecorder.shared().isAvailable == true{
            if #available(iOS 9.0, *) {
                if RPScreenRecorder.shared().isRecording == false{
                    RPScreenRecorder.shared().startRecording(withMicrophoneEnabled: false, handler: { [weak self] (error: Error?) in
                        DispatchQueue.main.async {
                            if error == nil{
                                self?.startRecordClosure?()
                            }else{
                                self?.errorClosure?(error, nil)
                            }
                        }
                    })
                }else{
                    stopRecordAlert?()//让用户选择是否停止录制
                }
            } else {
                showWarmAlert?()//警告的弹窗
            }
        }else{
            showWarmAlert?()
        }
    }

    //MARK: 停止录屏
    /// 停止录屏
    ///
    /// - Parameter shouldDiscard: 是否要忽略录制的结果
    func stopRecord(shouldDiscard: Bool){
        
        RPScreenRecorder.shared().stopRecording(handler: { [weak self] (vc: RPPreviewViewController?, error: Error?) in
            DispatchQueue.main.async {
                //取消录制
                self?.recordStoped?()
                if shouldDiscard == true{
                    RPScreenRecorder.shared().discardRecording(handler: {
                    })
                    return
                }
                if error == nil && vc != nil{
                    self?.shouldPresentRecordResult?(vc!)//展示录屏的结果
                }else{
                    self?.errorClosure?(error, nil)
                }
            }
        })
    }
    
    //MARK: 获取录制好的视频
    /// 获取录制好的视频
    func requestVideo(success: ((_ videoPath: String)->())?){
        showSVProgress(title: NSLocalizedString("视频处理中", comment: ""))
        
        delay(seconds: 3.0, completion: nil, completionBackground: {[weak self] _ in
            self?.dataModel.requestVideo(success: { [weak self] (videoPath: String) in
                DispatchQueue.main.async {
                    success?(videoPath)
                    self?.dismissSVProgress()
                }
            })
        })   
    }
    
    //MARK: 获取截屏图片
    func getSnapShot(){
        dataModel.requestSnapShot(success: {[weak self] (image: UIImage) in
            self?.snapShotImage = image
        })
    }
}

