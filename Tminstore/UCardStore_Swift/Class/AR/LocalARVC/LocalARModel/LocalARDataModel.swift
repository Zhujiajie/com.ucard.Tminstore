//
//  LocalARDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/11.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import Alamofire
import Photos

class LocalARDataModel: BaseDataModel {

    //MARK: 查看时光轴图片对应的所有视频
    /// 查看时光轴图片对应的所有视频
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    func showARList(originalId: String, andPageNum pageNum: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.showARVideoMemoryAPI + originalId + "/\(pageNum)"
        
        let parameters: [String: Any] = [
            "token": getUserToken()
            ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 点赞或者取消点赞
    /// 点赞或者取消点赞
    ///
    /// - Parameters:
    ///   - like: 点赞或者取消点赞
    ///   - videoId: 视频的ID
    func sendLikeOrCancelLike(like: Bool, andVideoId videoId: String){
        var urlString: String
        if like{
            urlString = appAPIHelp.videoLikeAPI
        }else{
            urlString = appAPIHelp.videoCancelLikeAPI
        }
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "videoId": videoId
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil)
    }
//    //MARK: 查看时光圈的评论
//    /// 查看时光圈的评论
//    ///
//    /// - Parameters:
//    ///   - pageIndex: 页数
//    ///   - originalId: 原创ID
//    ///   - success: 成功的闭包
//    func showSpaceTimeComment(pageIndex: Int, andOriginalId originalId: String, successClosure success: ((_ result: [String: Any])->())?){
//        
//        let parameters: [String: Any] = [
//            "originalId": originalId,
//            "pageIndex": pageIndex
//        ]
//        
//        sendGETRequestWithURLString(URLStr: showSpaceTimeCommentAPI, ParametersDictionary: parameters, urlEncoding: URLEncoding.default, successClosure: { (result: [String : Any]) in
//            success?(result)
//        }, failureClourse: { [weak self] (error: Error?) in
//            self?.errorClosure?(error)
//            }, completionClosure: {[weak self] _ in
//                self?.completionClosure?()
//        })
//    }
    
    //MARK: 获取录制好的视频
    /// 获取录制好的视频
    ///
    /// - Parameter success: 成功的闭包
    func requestVideo(success: ((_ videoPath: String)->())?){
        
        // 请求照片权限
        PHPhotoLibrary.requestAuthorization {  (status: PHAuthorizationStatus) in
            if status == PHAuthorizationStatus.authorized{
                
                let fetchOptions: PHFetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.video.rawValue)
                
                if let asset: PHAsset = PHAsset.fetchAssets(with: fetchOptions).firstObject{
                    
                    let imageManager: PHImageManager = PHImageManager()
                    
                    let options: PHVideoRequestOptions = PHVideoRequestOptions()
                    options.deliveryMode = PHVideoRequestOptionsDeliveryMode.fastFormat
                    options.isNetworkAccessAllowed = false
                    
                    imageManager.requestExportSession(forVideo: asset, options: options, exportPreset: AVAssetExportPresetMediumQuality, resultHandler: { [weak self] (session: AVAssetExportSession?, dic: [AnyHashable : Any]?) in
                        
                        if session != nil {
                            
                            do{
                                let cachesURL: URL = try FileManager.default.url(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: true)//获取缓存文件夹路径
                                let cachesPath: String = cachesURL.path//获取视频存储的文件夹路径
                                let videoPathString: String = cachesPath + "/videoCache/\(UUID().uuidString).mp4"//设置视频保存路径
                                
                                let cacheDirectory: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!//获取缓存文件夹
                                let videoFolder: String = cacheDirectory + "/videoCache"
                                
                                if FileManager.default.fileExists(atPath: videoFolder) == false {
                                    do {
                                        try FileManager.default.createDirectory(atPath: videoFolder, withIntermediateDirectories: false, attributes: nil)
                                    }catch{
                                        self?.errorClosure?(error)
                                        print(error)
                                    }
                                }//如果文件夹不存在，则创建一个
                                
                                session!.outputURL = URL(fileURLWithPath: videoPathString)//输出位置
                                session!.outputFileType = AVFileTypeMPEG4//视频格式
                                session!.exportAsynchronously(completionHandler: {
                                    switch session!.status{
                                    case AVAssetExportSessionStatus.failed, AVAssetExportSessionStatus.cancelled:
                                        self?.errorClosure?(session?.error)
                                    default:
                                        success?(videoPathString)//输出视频成功
                                    }
                                })
                            }catch{
                                self?.errorClosure?(error)
                                print(error)
                            }
                        }
                    })
                }
            }
        }
    }
    
    //MARK: 获取刚刚截屏的图片
    /// 获取刚刚截屏的图片
    ///
    /// - Parameter success: 成功的闭包
    func requestSnapShot(success: ((_ image: UIImage)->())?){
        // 请求照片权限
        PHPhotoLibrary.requestAuthorization {  [weak self] (status: PHAuthorizationStatus) in
            if status == PHAuthorizationStatus.authorized{
                
                let fetchOptions: PHFetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                fetchOptions.predicate = NSPredicate(format: "mediaType == %d", PHAssetMediaType.image.rawValue)
                
                if let asset: PHAsset = PHAsset.fetchAssets(with: fetchOptions).firstObject{
                    
                    let imageManager: PHImageManager = PHImageManager()
                    
                    let options: PHImageRequestOptions = PHImageRequestOptions()
                    options.deliveryMode = PHImageRequestOptionsDeliveryMode.opportunistic//获取原图
                    options.resizeMode = PHImageRequestOptionsResizeMode.none
                    options.isSynchronous = false//异步获取
                    options.isNetworkAccessAllowed = false
                    
                    imageManager.requestImage(for: asset, targetSize: UIScreen.main.bounds.size, contentMode: PHImageContentMode.default, options: options, resultHandler: { (image: UIImage?, dic: [AnyHashable : Any]?) in
                        if image != nil{
                            success?(image!)
                        }else{
                            self?.errorClosure?(nil)
                        }
                    })
                }else{
                    self?.errorClosure?(nil)
                }
            }else{
                self?.errorClosure?(nil)
            }
        }
    }
}
