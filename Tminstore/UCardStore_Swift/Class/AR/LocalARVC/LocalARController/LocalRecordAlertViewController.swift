//
//  LocalRecordAlertViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: ---------------ReplayKit不可用时的弹窗---------------
import UIKit

class LocalRecordAlertViewController: UIAlertController {
    
    /// 完成录屏
    var stopRecord: (()->())?
    /// 取消录屏
    var cancel: (()->())?
    
    /// 提醒用户当前录屏功能不可用的弹窗
    ///
    /// - Returns: LocalRecordAlertViewController
    class func recordWarnAlert()->LocalRecordAlertViewController{
        let alert: LocalRecordAlertViewController = LocalRecordAlertViewController(title: NSLocalizedString("录屏功能只支持iPhone5s以上，及iOS9以上的设备", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("好的", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancelAction)
        
        return alert
    }
    
    
    /// 让用户选择是否停止录屏的弹窗
    ///
    /// - Returns: LocalRecordAlertViewController
    class func confirmStopAlert()->LocalRecordAlertViewController{
        
        let alert: LocalRecordAlertViewController = LocalRecordAlertViewController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        // 完成录屏
        let stop: UIAlertAction = UIAlertAction(title: NSLocalizedString("完成录制", comment: ""), style: UIAlertActionStyle.default, handler: { (_) in
            alert.stopRecord?()
        })
        alert.addAction(stop)
        // 继续录屏
        let resume: UIAlertAction = UIAlertAction(title: NSLocalizedString("继续录制", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(resume)
        //取消录屏
        let cancel: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消录制", comment: ""), style: UIAlertActionStyle.destructive, handler: {(_) in
            alert.cancel?()
        })
        alert.addAction(cancel)
        return alert
    }
}
