//
//  LocalARCardViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/15.
//  Copyright © 2017年 ucard. All rights reserved.
//
//MARK: -----------------AR卡片的本地识别----------------
import UIKit
import SnapKit

class LocalARCardViewController: BaseViewController {
    
//    let imageNameArray: [String] = [
//        "2B31CF03-053E-494B-B87F-24F6567590E1",
//        "6DD00713-A002-4C46-9F85-6F21F400EF9C",
//        "8c23ee0b-5225-482c-a5d2-11f509bd7db3",
//        "8ea2ec95-e549-48d4-ad4c-ed83a6463407",
//        "40DBD024-7ADC-4B4F-BFBB-B2F529E04584",
//        "123",
//        "305F9C4B-B406-4EC4-99D6-200E54C3A8E7",
//        "3167af32-03a8-4770-a823-c654f50db494",
//        "6148e54f-0f2d-4938-aab8-9c6b1745eed3",
//        "364738bb-93fb-4c7b-8a1d-a09e7eaa0ce0",
//        "14915024-B0D1-43E9-BBAA-055420484F3D",
//        "CBF1631B-B504-49F2-ACDC-44622CF3CEDB",
//        "EAA54A1F-ECEB-46D5-A135-29E618B88C21",
//        "f8cb9179-d641-4c23-b757-9fb95b223715",
//        "f32493dd-c5f8-42b0-bff3-63244df80701"
//    ]
//    
//    let videoURLArray: [String] = [
//        "0D80A6FF-1429-4604-85FB-A13A8E97C0F2",
//        "A6227B7C-F574-4CC7-B651-2645DE2F6744",
//        "F924CD9F-72FB-466B-AA71-08D488C83B3E",
//        "0310932C-6C85-4514-81C3-1857D6CAC40B",
//        "bcb7925b-42f7-4b07-9b64-5b2ac680b8a0",
//        "de45a8e7-1d03-4093-8247-1639049ebd15",
//        "9388D835-C2CB-49B6-BEF3-3821A134DB6C",
//        "6AC2048E-1ACF-486B-94EE-95FA285120B9",
//        "d1fb9286-42fd-497b-b88b-48fc319c249f",
//        "3632595D-6135-4BDA-97A5-82CFDDE27504",
//        "900D91E1-D306-49BE-AAC4-309C06316D03",
//        "bbe733ca-a151-4d6b-a5c0-9e10ee81c8ae",
//        "53cd853d-202f-44fe-bf02-92145566742b",
//        "985adbae-bc3f-4fc7-bdf3-19650961a4f9",
//        "60c64a47-1dc4-400b-984d-9e8796b176ba"
//    ]
    
    var videoIndex: Int?
    
    /// AR识别图的路径
    var arImagePath: String = ""
    /// 默认的视频地址
    var videoURLString: String = ""
    /// AR图层
    var localView: LocalView?
    
    fileprivate let arFrame: CGRect = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
    
    /// 是否加载过默认视频
    fileprivate var hasLoadDefaultVideo: Bool = false

    /// 是否找到目标
    fileprivate var foundTargetOrNot: Bool = false
    
    /// 退出的按钮
    fileprivate var dismissButton: EasyARButton?
    
    /// 是否需要隐藏状态栏
    fileprivate var isStatusBarHide = true{
        didSet{
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    // MARK: 是否隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return isStatusBarHide
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //添加一个黑色背景
        let backgroundView: UIView = UIView()
        backgroundView.backgroundColor = UIColor.black
        view.addSubview(backgroundView)
        backgroundView.frame = arFrame
    }
    
    //MARK: 进入视图，开启AR
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAR()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("AR卡片本地识别")//友盟页面统计
        isStatusBarHide = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("AR卡片本地识别")//友盟页面统计
        isStatusBarHide = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        fireAR()//释放AR
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        localView?.resize(arFrame, orientation: UIInterfaceOrientation.portrait)
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        localView?.setOrientation(toInterfaceOrientation)
    }
    
    //MARK: 开始AR扫描
    /// 开始AR扫描
    func startAR(){
        initAR()
        setButtonAndClueView()
        closures()
    }
    
    //MARK: 初始化AR
    fileprivate func initAR(){
        if localView == nil {
            localView = LocalView(frame: arFrame)
            view.addSubview(localView!)
            localView?.setOrientation(UIInterfaceOrientation.portrait)
        }
        
//        for i in 0..<imageNameArray.count{
//            let path: String = Bundle.main.path(forResource: imageNameArray[i], ofType: ".png")!
//            localView?.loadImage(path)
//        }
        localView?.loadImage(arImagePath)
        localView?.startAR()
    }
    
    //MARK: 停止AR扫描
    /// 停止AR扫描
    func stopAR(){
        localView?.active = false
        localView?.stop()
    }
    
    //MARK: 释放AR
    /// 释放AR
    func fireAR(){
        stopAR()
        localView?.clear()
        localView?.removeFromSuperview()
        localView = nil
    }
    
    //MARK: 设置按钮
    fileprivate func setButtonAndClueView(){
        dismissButton = EasyARButton.dismissButton()
        view.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view).offset(30)
            make.left.equalTo(view).offset(20)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        }
    }
    
    //MARK: 各种闭包回调
    func closures(){
        
        //MARK: C++的代理
        localView?.foundTarget = {[weak self] (found: Bool, index: Int32) in
            SVProgressHUD.showSuccess(withStatus: "识别成功")
            delay(seconds: 0.3, completion: {
                SVProgressHUD.dismiss()
            }, completionBackground: nil)
            if self?.videoIndex != Int(index) && found == true{
                self?.videoIndex = Int(index)
//                let videoPath: String = "http://ucardstorevideo.b0.upaiyun.com/test/" + (self?.videoURLArray[(self?.videoIndex)!])! + ".mp4"
//                self?.localView?.setVideoURL(videoPath)
                self?.localView?.setVideoURL((self?.videoURLString)!)
            }
        }
        
        //MARK: 点击退出按钮，退出
        dismissButton?.touchDismissButton = {[weak self] _ in
            self?.dismissSelf()
        }
        
        //MARK: app的代理
        app.appWillResignActive = {[weak self] _ in
            self?.localView?.stop()
        }
        app.appDidBecomeActive = {[weak self] _ in
            if self?.localView != nil && self?.localView?.active == false{
                self?.localView?.loadImage((self?.arImagePath)!)
            }
        }
    }
}
