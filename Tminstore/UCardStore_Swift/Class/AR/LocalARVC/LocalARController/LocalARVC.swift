//
//  LocalARVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import ReplayKit

class LocalARVC: BaseViewController {
    
    fileprivate let userViewModel: PUserViewModel = PUserViewModel()
    fileprivate let videoViewModel: LocalARViewModel = LocalARViewModel()
    
    /// 是否是从发布界面过来的
    var isComeFromReleaseVC: Bool = false
    
    /// AR识别图的路径
    var arImagePath: String = ""
    
    /// AR图层
    var localView: LocalView?
    
    /// 包含了图片的各种信息
    var annotation: ZTAnnotation?
    
    /// 闪光灯按钮
    fileprivate var torchBarItem: LocalARBarButtonItem?
    /// 截屏的按钮
    fileprivate var snapShotBarItem: LocalARBarButtonItem?
    /// 点击退出时光轴的按钮
    fileprivate var quitSpaceTimeBarItem: LocalARBarButtonItem?
    /// 点击获取用户信息的按钮
    fileprivate var getUserInfoBarItem: LocalARBarButtonItem?
    /// 截屏确认分享的按钮
    fileprivate var snapShotShareView: LocalARSnapShareView?
    
    fileprivate let arFrame: CGRect = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
    
    /// 显示时光轴，录屏，留下记忆按钮的视图
    fileprivate var threeButtonView: LocalThreeButtons?
    
    /// 展示线索内容的视图
    fileprivate var infoView: LocalInfoView?
    
    /// 切换视频的视图
    fileprivate var timeLineCollectionView: LocalTimeLineCollectionView?
    /// 扫描动画
    fileprivate var scanLine: UIImageView?
    
    /// 原生的视频播放器
    fileprivate var arPlayer: LocalARPlayer?

    /// 是否需要隐藏状态栏
    fileprivate var isStatusBarHide = true{
        didSet{
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    // MARK: 是否隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return isStatusBarHide
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //添加一个黑色背景
        let backgroundView: UIView = UIView()
        backgroundView.backgroundColor = UIColor.black
        view.addSubview(backgroundView)
        backgroundView.frame = CGRect(x: 0, y: -navBarHeight, width: screenWidth, height: screenHeight)
        
        setNavigationBar()
    }
    
    //MARK: 进入视图，开启AR
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if arPlayer == nil{
            startAR()
        }else{
            arPlayer?.getCameraPreview()
        }
        videoViewModel.postcardModel = annotation!.postcardModel!
        
        NotificationCenter.default.addObserver(self, selector: #selector(showShareSnapShotView(notification:)), name: Notification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("2D地图本地识别")//友盟页面统计
        transparentNavigationBar()//导航栏透明
        isStatusBarHide = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("2D地图本地识别")//友盟页面统计
        isStatusBarHide = false
        //如果还在录屏，则取消录屏
        videoViewModel.stopRecordAndDiscard()
        if arPlayer != nil{
            arPlayer?.pausePlay()
            arPlayer?.stopSession()//停止获取摄像头数据
        }
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if torchBarItem != nil{
            torchBarItem?.setTorch(on: false)//关闭闪关灯
        }
        fireAR()//释放AR
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        localView?.resize(arFrame, orientation: UIInterfaceOrientation.portrait)
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        localView?.setOrientation(toInterfaceOrientation)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("whiteBackArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        torchBarItem = LocalARBarButtonItem.torchBarItem()
        snapShotBarItem = LocalARBarButtonItem.snapShotBarItem()
        navigationItem.rightBarButtonItems = [snapShotBarItem!, torchBarItem!]
        snapShotBarItem?.isEnabled = false
        quitSpaceTimeBarItem = LocalARBarButtonItem.quitSpaceTimeBarItem()
        getUserInfoBarItem = LocalARBarButtonItem.clearBarItem()
    }
    
    //MARK: 开始AR扫描
    /// 开始AR扫描
    func startAR(){
        initAR()
        setButtonAndClueView()
        closures()
    }
    
    //MARK: 初始化AR
    fileprivate func initAR(){
        if localView == nil {
            localView = LocalView(frame: arFrame)
            view.addSubview(localView!)
            localView?.setOrientation(UIInterfaceOrientation.portrait)
            localView?.frame.origin.y = -navBarHeight
        }
        localView?.loadImage(arImagePath)
        localView?.startAR()
    }
    
    //MARK: 停止AR扫描
    /// 停止AR扫描
    func stopAR(){
        localView?.active = false
        localView?.stop()
    }
    
    //MARK: 释放AR
    /// 释放AR
    func fireAR(){
        stopAR()
        localView?.clear()
        localView?.removeFromSuperview()
        localView = nil
    }
    
    //MARK: 设置按钮
    fileprivate func setButtonAndClueView(){
        
        /// 承载三个按钮的视图
        threeButtonView = LocalThreeButtons()
        view.addSubview(threeButtonView!)
        threeButtonView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(130/baseWidth)
        })
        
        //图片信息的视图
        infoView = LocalInfoView(frame: CGRect.zero)
        view.addSubview(infoView!)
        infoView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(69/baseWidth)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 296/baseWidth, height: 404/baseWidth))
        })
        infoView?.annotation = annotation
        
        //切换视频的视图
        timeLineCollectionView = LocalTimeLineCollectionView()
        view.addSubview(timeLineCollectionView!)
        timeLineCollectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.equalTo(view)
            make.bottom.equalTo(view).offset(screenHeight)
            make.height.equalTo(136/baseWidth)
        })
        timeLineCollectionView?.collectionView?.delegate = self
        timeLineCollectionView?.collectionView?.dataSource = self
        
        //添加一个扫描的动画
        scanLine = UIImageView(image: UIImage(named: "scanLine"))
        view.addSubview(scanLine!)
        scanLine?.frame = CGRect(x: 0, y: 125/baseHeight, width: screenWidth, height: 3)
        UIView.animate(withDuration: 2.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseInOut, UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse], animations: { [weak self] _ in
            self?.scanLine?.frame.origin.y = 360/baseHeight
        }, completion: nil)
    }
    
    //MARK: 让用户选择是否停止录制
    fileprivate func setChooseRecordActionAlert(){
        let av: LocalRecordAlertViewController = LocalRecordAlertViewController.confirmStopAlert()
        //MARK: 完成录制
        av.stopRecord = {[weak self] _ in
            self?.videoViewModel.stopRecord(shouldDiscard: false)
        }
        //MARK: 取消录制
        av.cancel = {[weak self] _ in
            self?.videoViewModel.stopRecord(shouldDiscard: true)
        }
        present(av, animated: true, completion: nil)
    }
    
    //MARK: 提醒用户ReplayKit不可用
    fileprivate func setWarnAlert(){
        let ac: LocalRecordAlertViewController = LocalRecordAlertViewController.recordWarnAlert()
        present(ac, animated: true, completion: nil)
    }
    
    //MARK: 设置播放器
    fileprivate func setARLocalPlayer(){
        arPlayer = LocalARPlayer(frame: arFrame)
        arPlayer?.frame.origin.y = -navBarHeight 
        view.insertSubview(arPlayer!, belowSubview: threeButtonView!)
        
        navigationItem.rightBarButtonItems = [snapShotBarItem!]//隐藏闪光灯按钮
        snapShotBarItem?.isEnabled = true//启用截屏按钮
        
        scanLine?.layer.removeAllAnimations()//移除扫描动画
        scanLine?.removeFromSuperview()
        infoView?.dismissAnimation()//移除线索视图
        autoLayoutAnimation(view: view)
        
        //MARK: 获取摄像头数据成功，销毁AR，并设置视频播放
        arPlayer?.getCameraSuccess = {[weak self] _ in
            if self?.localView == nil {return}
            self?.fireAR()//销毁AR识别
            self?.arPlayer?.postcardModel = self?.videoViewModel.postcardArray.first
        }
        
        //MARK: 点击评论的事件
        arPlayer?.touchCommentButtonClosure = {[weak self] _ in
            //第一个视频。直接进入视频评论模块。否则进入视频回复模块
            let vc: VideoReplyController = VideoReplyController()
            vc.videoId = (self?.arPlayer?.postcardModel?.videoId)!
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 点击点赞按钮的事件
        arPlayer?.touchLikeButtonClosure = {[weak self] (like: Bool, videoId: String) in
            self?.videoViewModel.sendLikeOrCancelLike(like: like, andVideoId: videoId)
        }
        
//        //MARK: 点击用户头像
//        arPlayer?.tapUserPortaitView = {[weak self] (memberCode: String) in
//            self?.userViewModel.requestOtherUserInfo(memberCode: memberCode)
//        }
        
        //延迟一秒，展现时光轴
        delay(seconds: 1.0, completion: { [weak self] _ in
            if (self?.videoViewModel.postcardArray.count)! > 0{
//                self?.showOrHideTimeLine(shouldShowTimeLine: true)
                self?.setSnapShotShareView()//设置截屏分享
            }
        }, completionBackground: nil)
    }
    
    //MARK: 展示或者隐藏时光轴
    fileprivate func showOrHideTimeLine(shouldShowTimeLine: Bool){
        threeButtonView?.hideOrShowButton(isHide: shouldShowTimeLine)
        timeLineCollectionView?.snp.updateConstraints({ (make: ConstraintMaker) in
            if shouldShowTimeLine == true{
                make.bottom.equalTo(view).offset(10/baseWidth)
                arPlayer?.beginChooseVideo()//播放器开始进入选择视频模式
                navigationItem.leftBarButtonItem = getUserInfoBarItem!
                navigationItem.rightBarButtonItems = [quitSpaceTimeBarItem!]
            }else{
                make.bottom.equalTo(view).offset(screenWidth)
                arPlayer?.quitVideoChoose()//退出选择视频模式
                setNavigationBarLeftButtonWithImageName("whiteBackArrow")
                navigationItem.leftBarButtonItem?.tintColor = UIColor.white
                navigationItem.rightBarButtonItems = [snapShotBarItem!]
            }
        })
        autoLayoutAnimation(view: view)
    }
    
//    //MARK: 设置录屏介绍页面
//    fileprivate func showRecordIntroImageView(){
//        if hasLoadTimeLineRecordImageView() == false{
//            recordImageView = LocalTimeLineIntroView.recordIntroImageView()
//            navigationController?.view.addSubview(recordImageView!)
//            setLoadTimeLineRecordImageView()
//        }
//    }
//    
//    //MARK: 扫描成功之后设置时光轴介绍视图
//    fileprivate func showTimeLineIntroImageView(){
//        if hasLoadTimeLineIntroImageView() == false{
//            if recordImageView != nil{
//                recordImageView?.dismissAnimation()
//            }
//            let introView: LocalTimeLineIntroView = LocalTimeLineIntroView.timeLineIntroImageView()
//            navigationController?.view.addSubview(introView)
//            setLoadTimeLineIntroImageView()
//        }
//    }
    
    //MARK: 各种闭包回调
    func closures(){
        
        //MARK: C++的代理
        localView?.foundTarget = {[weak self] (found: Bool, index: Int32) in
            if found == true && self?.arPlayer == nil{
                self?.setARLocalPlayer()
            }
        }
        
        //MARK: 点击截屏按钮的事件
        snapShotBarItem?.touchMoreButtonClosure = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("同时按住电源键和主屏幕键来截屏分享", comment: ""))
        }
        
        //MARK: 点击退出视频选择的按钮
        quitSpaceTimeBarItem?.touchMoreButtonClosure = {[weak self] _ in
            self?.showOrHideTimeLine(shouldShowTimeLine: false)
        }
        
        //MARK: 点击获取用户信息按钮
        getUserInfoBarItem?.touchMoreButtonClosure = {[weak self] _ in
            self?.userViewModel.requestOtherUserInfo(memberCode: (self?.annotation?.postcardModel?.memberCode)!)
        }
        
        //MARK: 点击添加视频按钮
        threeButtonView?.touchAddVideoButtonClosure = {[weak self] _ in
            if self?.arPlayer == nil || self?.localView != nil{return}
            let videoCommentVC: VideoCommentController = VideoCommentController()
            videoCommentVC.umengViewName = "留下记忆"
            videoCommentVC.originalId = self?.annotation?.postcardModel?.originalId
            _ = self?.navigationController?.pushViewController(videoCommentVC, animated: true)
        }
        
        //MARK: 点击时间轴按钮的事件
        threeButtonView?.touchTimeLineButtonClosure = {[weak self] _ in
            if self?.arPlayer == nil || self?.videoViewModel.postcardArray.isEmpty == true || self?.localView != nil{return}
            self?.showOrHideTimeLine(shouldShowTimeLine: true)
        }
        
        //MARK: 点击录屏按钮的事件
        threeButtonView?.touchRecordButtonClosure = {[weak self] _ in
            if self?.localView != nil {return}
            if self?.videoViewModel.isRecording() == true{
                self?.setChooseRecordActionAlert()
            }else{
                self?.videoViewModel.startRecord()
            }
        }
        //MARK: 需要停止录屏
        threeButtonView?.shouldStopRecord = {[weak self] _ in
            self?.videoViewModel.stopRecord(shouldDiscard: false)
        }
        
        //MARK: app的代理
        app.appWillResignActive = {[weak self] _ in
            self?.localView?.stop()
        }
        app.appDidBecomeActive = {[weak self] _ in
            if self?.localView != nil && self?.localView?.active == false{
                self?.localView?.loadImage((self?.arImagePath)!)
            }
        }
        
        //MARK: 点击时光轴收回按钮，收回tableView
        timeLineCollectionView?.swipeDownClosure = {[weak self] _ in
            self?.arPlayer?.quitVideoChoose()//退出视频选择
        }
        //MARK: 左拉刷新
        timeLineCollectionView?.headerRefreshClosure = {[weak self] _ in
            self?.videoViewModel.pageNum = 1
            self?.videoViewModel.showARList()
        }
        //MARK: 右拉拉刷新
        timeLineCollectionView?.footerRefreshClosure = {[weak self] _ in
            self?.videoViewModel.showARList()
        }
        
        //MARK: 已经开始录屏的闭包
        videoViewModel.startRecordClosure = {[weak self] _ in
            self?.arPlayer?.replayPlayer()
            self?.threeButtonView?.beginRecord()
        }
        
        //MARK: 停止录屏的闭包
        videoViewModel.stopRecordAlert = {[weak self] _ in
            self?.setChooseRecordActionAlert()//让用户选择是否停止录制
        }
        
        //MARK: 警告弹窗
        videoViewModel.showWarmAlert = {[weak self] _ in
            self?.setWarnAlert()
        }
        
        //MARK: 已经停止录屏
        videoViewModel.recordStoped = {[weak self] _ in
            self?.threeButtonView?.stopRecord()
        }
        
        //MARK: 需要展示录屏的结果
        videoViewModel.shouldPresentRecordResult = {[weak self] (vc: RPPreviewViewController) in
            vc.previewControllerDelegate = self
            self?.present(vc, animated: true, completion: nil)
        }
        
        //MARK: 成功获得视频评论数据
        videoViewModel.collectionViewShouldReload = {[weak self] (shouldScrollToTop: Bool) in
            self?.timeLineCollectionView?.collectionView?.xzm_header.endRefreshing()
            self?.timeLineCollectionView?.collectionView?.xzm_footer.endRefreshing()
            self?.timeLineCollectionView?.collectionView?.reloadData()
            if self?.arPlayer != nil{
                self?.showOrHideTimeLine(shouldShowTimeLine: true)//弹出时光轴
            }
            if shouldScrollToTop == true{
                delay(seconds: 1.0, completion: {
                    self?.timeLineCollectionView?.collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
                    if self?.timeLineCollectionView?.shouldSetFirstCellPlaying == true{
                        if let cell: LocalARTimeLineCollectionViewCell = self?.timeLineCollectionView?.collectionView?.cellForItem(at: IndexPath(row: 0, section: 0)) as? LocalARTimeLineCollectionViewCell {
                            cell.setPlayState(isPlaying: true)
                        }
                        self?.timeLineCollectionView?.shouldSetFirstCellPlaying = false
                    }
                })
            }
        }
        
        //MARK: 没有更多数据了
        videoViewModel.noMoreData = {[weak self] _ in
            self?.timeLineCollectionView?.collectionView?.xzm_header.endRefreshing()
            self?.timeLineCollectionView?.collectionView?.xzm_footer.endRefreshing()
        }
        
        //MARK: 出错的闭包
        videoViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.timeLineCollectionView?.collectionView?.xzm_header.endRefreshing()
            self?.timeLineCollectionView?.collectionView?.xzm_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 成功获得其他用户信息
        userViewModel.getOtherUserInfoSuccess = {[weak self] (userInfoModel: UserInfoModel) in
            let vc: PUserController = PUserController()
            vc.userInfoModel = userInfoModel
            vc.umengViewName = "其他用户信息"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 获取用户信息失败
        userViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 点击了截屏按钮的事件
    fileprivate func setSnapShotShareView(){
        if snapShotShareView == nil{
            snapShotShareView = LocalARSnapShareView()
            view.addSubview(snapShotShareView!)
            snapShotShareView?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(arPlayer!).offset(70/baseWidth)
                make.right.equalTo(arPlayer!.snp.left).offset(0)
                make.size.equalTo(CGSize(width: 134/baseWidth, height: 40/baseWidth))
            })
        }
        //MARK: 截图分享视图五秒钟计时结束
        snapShotShareView?.stopTimerClosure = {[weak self] _ in
            self?.showSnapShotShareView(shouldShow: false)
        }
        
        //MARK: 点击了分享视图，进入截图分享控制器
        snapShotShareView?.tapShareViewClosure = {[weak self] _ in
            guard let image: UIImage = self?.videoViewModel.snapShotImage else { return }
            let shareVC: SnapShotShareController = SnapShotShareController()
            shareVC.umengViewName = "截图分享"
            shareVC.snapShotImage = image
            _ = self?.navigationController?.pushViewController(shareVC, animated: true)
        }
    }
    
    //MARK: 展示截屏分享视图
    fileprivate func showSnapShotShareView(shouldShow: Bool = true){
        if shouldShow == true{
            snapShotShareView?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(arPlayer!.snp.left).offset(67/baseWidth)
            })
            snapShotShareView?.startTimer()
        }else{
            videoViewModel.snapShotImage = nil
            snapShotShareView?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(arPlayer!.snp.left).offset(0)
            })
        }
        autoLayoutAnimation(view: view)
    }
    
    //MARK: 接受通知，弹出分享视图
    func showShareSnapShotView(notification: Notification){
        showSVProgress(title: nil)
        delay(seconds: 1.0, completion: {[weak self] _ in
            self?.dismissSVProgress()
            self?.videoViewModel.getSnapShot()
            self?.showSnapShotShareView(shouldShow: true)
        })
    }
    
    //MARK: 点击导航栏左按钮，退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        //如果是从发布界面过来。直接返回到地图界面
        if isComeFromReleaseVC == true{
            app.containerVC?.mapViewDismiss()
        }else{
            dismissSelf()
        }
    }
}

extension LocalARVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videoViewModel.postcardArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: LocalARTimeLineCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.LocalARTimeLineCollectionViewCellReuseIdentifier, for: indexPath) as! LocalARTimeLineCollectionViewCell
        cell.postcardModel = videoViewModel.postcardArray[indexPath.item]
        if cell.postcardModel?.videoURL == arPlayer?.videoURL{
            cell.setPlayState(isPlaying: true)
        }else{
            cell.setPlayState(isPlaying: false)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if arPlayer != nil{
            arPlayer?.postcardModel = videoViewModel.postcardArray[indexPath.row]
            if let cell: LocalARTimeLineCollectionViewCell = collectionView.cellForItem(at: indexPath) as? LocalARTimeLineCollectionViewCell{
                cell.setPlayState(isPlaying: true)
            }
        }
    }
}

extension LocalARVC: RPPreviewViewControllerDelegate{
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        previewController.dismiss(animated: true, completion: nil)
    }
    
    //MARK: 保存了视频。开始寻找视频
    func previewController(_ previewController: RPPreviewViewController, didFinishWithActivityTypes activityTypes: Set<String>) {
        if activityTypes.contains("com.apple.UIKit.activity.SaveToCameraRoll") == true{
            videoViewModel.requestVideo(success: { [weak self] (videoPath: String) in
                let shareVC: VideoShareController = VideoShareController()
                shareVC.umengViewName = "视频分享"
                shareVC.videoURLPath = videoPath
                _ = self?.navigationController?.pushViewController(shareVC, animated: true)
            })
        }
    }
}
