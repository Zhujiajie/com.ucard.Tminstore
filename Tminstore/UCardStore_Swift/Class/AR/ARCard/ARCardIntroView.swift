//
//  ARCardIntroView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ARCardIntroView: BaseView {

    /// 是否是视频
    var isVideo: Bool = true{
        didSet{
            if isVideo {
                introImageView?.sd_setImage(with: appAPIHelp.ARCardVideoIntroURL)
                introLabel?.text = NSLocalizedString("左右滑动查看内容", comment: "")
            }else{
                introImageView?.sd_setImage(with: appAPIHelp.ARCardImageIntroURL)
                introLabel?.text = NSLocalizedString("点击PPT翻页滑动查看内容", comment: "")
            }
        }
    }

    /// 绿色视图
    fileprivate var greenView: BaseView?
    /// 介绍图片
    fileprivate var introImageView: BaseImageView?
    /// 标题
    fileprivate var introLabel: BaseLabel?
    /// 退出的按钮
    fileprivate var dismissButton: BaseButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(white: 0.0, alpha: 0.62)
        
        greenView = BaseView.colorLine(colorString: "4EDAE9")
        addSubview(greenView!)
        greenView?.snp.makeConstraints({ (make) in
            make.centerX.equalTo(self)
            make.top.equalTo(self).offset(131/baseWidth)
            make.size.equalTo(CGSize(width: 255/baseWidth, height: 295/baseWidth))
        })
        greenView?.alpha = 0.85
        greenView?.layer.cornerRadius = 5
        greenView?.layer.masksToBounds = true
        
        
        introImageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: nil)
        addSubview(introImageView!)
        introImageView?.snp.makeConstraints({ (make) in
            make.top.equalTo(greenView!).offset(65/baseWidth)
            make.left.right.equalTo(greenView!)
            make.bottom.equalTo(greenView!).offset(-110/baseWidth)
        })
        
        introLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: nil)
        addSubview(introLabel!)
        introLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(introImageView!.snp.bottom).offset(30/baseWidth)
            make.centerX.equalTo(self)
        })
        
        dismissButton = BaseButton.normalTitleButton(title: NSLocalizedString("知道了", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "49D0E3", andBackgroundColorHexString: "FFFFFF", andBorderColorHexString: nil, andBorderWidth: nil, andCornerRadius: 17.5/baseWidth)
        addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(greenView!.snp.bottom).offset(33/baseWidth)
            make.centerX.equalTo(self)
            make.size.equalTo(CGSize(width: 118/baseWidth, height: 35/baseWidth))
        })
        //MARK: 点击知道了按钮隐藏视图
        dismissButton?.touchButtonClosure = {[weak self] _ in
            self?.hideAnimation()
        }
    }
    
    //MARK: 隐藏动画
    fileprivate func hideAnimation(){
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 0
        }, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
