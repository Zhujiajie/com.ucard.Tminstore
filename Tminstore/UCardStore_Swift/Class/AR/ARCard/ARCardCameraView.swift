//
//  ARCardCameraView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ARCardCameraView: BaseView {

    /// 获取摄像头数据成功
    var getCameraSuccess: (()->())?
    
    fileprivate let session: AVCaptureSession = AVCaptureSession()
    /// 用来展示摄像头的layer
    fileprivate var cameraLayer: AVCaptureVideoPreviewLayer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.black
        getCameraPreview()//设置摄像头预览画面
    }
    
    //MARK: 停止获取摄像头数据
    /// 停止获取摄像头数据
    func stopSession(){
        if session.isRunning == true{
            if let input: AVCaptureDeviceInput = session.inputs.first as? AVCaptureDeviceInput{
                session.removeInput(input)
                session.stopRunning()
                cameraLayer?.removeFromSuperlayer()
                cameraLayer = nil
            }
        }
    }
    
    //MARK: 获取摄像头的捕捉到影像
    fileprivate func getCameraPreview() {
        session.sessionPreset = AVCaptureSessionPreset1920x1080
        if let device: AVCaptureDevice = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).first as? AVCaptureDevice {
            do{
                let input: AVCaptureDeviceInput = try AVCaptureDeviceInput(device: device)
                session.addInput(input)
                cameraLayer = AVCaptureVideoPreviewLayer(session: session)
                cameraLayer!.frame = self.bounds
                session.startRunning()
                DispatchQueue.main.async(execute: {[weak self] _ in
                    self?.layer.addSublayer((self?.cameraLayer)!)//添加Layer
                    self?.getCameraSuccess?()
                })
            }catch{print(error.localizedDescription) }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
