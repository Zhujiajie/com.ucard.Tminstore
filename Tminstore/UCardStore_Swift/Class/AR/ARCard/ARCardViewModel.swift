//
//  ARCardViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ARCardViewModel: BaseViewModel {

    let videoArray:[URL] = [
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/video/616070b3-71e1-11e7-ac3a-00163e00a099.mp4")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/video/f998eb12-71e4-11e7-ac3a-00163e00a099.mp4")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/video/e8380615-71e3-11e7-ac3a-00163e00a099.mp4")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/video/e9a13308-71e3-11e7-ac3a-00163e00a099.mp4")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/video/f8ec093f-71e4-11e7-ac3a-00163e00a099.mp4")!
    ]
    
    let imageURLArray: [URL] = [
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/frontImage/61359021-71e1-11e7-ac3a-00163e00a099.png!/format/webp")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/frontImage/e714e2d0-71e3-11e7-ac3a-00163e00a099.png!/format/webp")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/frontImage/ecd052f2-71e3-11e7-ac3a-00163e00a099.png!/format/webp")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/frontImage/e824a523-71e3-11e7-ac3a-00163e00a099.png!/format/webp")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/frontImage/eac8ea29-71e3-11e7-ac3a-00163e00a099.png!/format/webp")!,
        URL(string: "http://ucardstorevideo.b0.upaiyun.com/frontImage/eb37ffac-71e3-11e7-ac3a-00163e00a099.png!/format/webp")!
    ]
}
