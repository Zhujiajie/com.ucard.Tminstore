//
//  ARCardCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

private let videoReuseIdentifier = "videoReuseIdentifier"
private let imageReuseIdentifier = "imageReuseIdentifier"

class ARCardCollectionViewController: UICollectionViewController {

    /// 是否展示视频
    fileprivate var isVideo: Bool = true
    /// 数据源
    weak var viewModel: ARCardViewModel?
    
    /// 标题
    fileprivate var titleLabel: BaseLabel?
    /// 页数指示器
    fileprivate var pageControl: UIPageControl?
    
    
    /// 初始化
    ///
    /// - Parameters:
    ///   - isVideo: 是否是展示视频的collectionView
    ///   - viewModel: 数据源
    init(isVideo: Bool, andViewModel viewModel: ARCardViewModel) {
        
        self.isVideo = isVideo
        self.viewModel = viewModel
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: screenWidth, height: 170/baseWidth)
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clear
        self.collectionView?.backgroundColor = UIColor.clear
        
        self.collectionView!.register(ARCardCollectionViewVideoCell.self, forCellWithReuseIdentifier: videoReuseIdentifier)
        self.collectionView?.register(ARCardCollectionViewImageCell.self, forCellWithReuseIdentifier: imageReuseIdentifier)
        self.collectionView?.isPagingEnabled = true
        
        
        var titleText: String
        
        if isVideo {
            titleText = "企业宣传视频"
        }else{
            titleText = "PPT展示"
        }
        
        //设置标题和底部pageControl
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andText: titleText)
        view.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(view.snp.top).offset(-50/baseWidth)
            make.centerX.equalTo(view)
        })
        
        pageControl = UIPageControl()
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "62FFF4")
        view.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make) in
            make.top.equalTo(view.snp.bottom)
            make.centerX.equalTo(view)
        })
    }

    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel == nil{
            return 0
        }else{
            if isVideo {
                pageControl?.numberOfPages = viewModel!.videoArray.count
                return viewModel!.videoArray.count
            }else{
                pageControl?.numberOfPages = viewModel!.imageURLArray.count
                return viewModel!.imageURLArray.count
            }
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isVideo {
            let cell: ARCardCollectionViewVideoCell = collectionView.dequeueReusableCell(withReuseIdentifier: videoReuseIdentifier, for: indexPath) as! ARCardCollectionViewVideoCell
            
            cell.videoPath = viewModel!.videoArray[indexPath.row]
            
            return cell
        }else {
            let cell: ARCardCollectionViewImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: imageReuseIdentifier, for: indexPath) as! ARCardCollectionViewImageCell
            
            cell.imageURL = viewModel!.imageURLArray[indexPath.row]
            
            return cell
        }
    }

    // MARK: UICollectionViewDelegate
    //MARK: 滚动cell
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
        if isVideo {
            //发送消息, 停止播放视频
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)
            //正在展示的Cell开始播放视频
            delay(seconds: 1, completion: { [weak self] _ in
                self?.cellPlayVideo()
                }, completionBackground: nil)
        }
    }
    
    //MARK: 设置cell开始播放视频
    fileprivate func cellPlayVideo(){
        if let cell: ARCardCollectionViewVideoCell = collectionView?.visibleCells.first as? ARCardCollectionViewVideoCell{
            cell.playVideo()
        }
    }
}

//MARK: 展示视频的Cell
class ARCardCollectionViewVideoCell: UICollectionViewCell, PlayerDelegate, PlayerPlaybackDelegate{
    
    /// 视频路径
    var videoPath: URL?{
        didSet{
            if videoPath != nil && videoPath != oldValue{
                activeIndicator?.startAnimating()
                player?.stop()
                player?.setVideoFullURL(fullVideoURL: videoPath!)
                player?.playFromBeginning()
            }
        }
    }
    
    fileprivate var player: ZTPlayer?
    fileprivate var playAgainButton: UIButton?
    fileprivate var activeIndicator: UIActivityIndicatorView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        
        player = ZTPlayer()
        player?.view.frame = CGRect(x: 43/baseWidth, y: 0, width: 289/baseWidth, height: 171/baseWidth)
        player?.view.backgroundColor = UIColor.black//背景黑色
        player?.playbackLoops = true
        player?.playerDelegate = self
        player?.playbackDelegate = self
        contentView.addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            if self?.videoPath != nil{
                self?.playAgainButton?.isHidden = false
            }
        }
        
        //视频加载指示器
        activeIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        player?.view.addSubview(activeIndicator!)
        activeIndicator?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view)
        })
        activeIndicator?.hidesWhenStopped = true
        
        //重新开始播放的按钮
        playAgainButton = UIButton()
        playAgainButton?.setImage(UIImage(named: "play"), for: UIControlState.normal)
        playAgainButton?.contentMode = UIViewContentMode.scaleAspectFit
        playAgainButton?.addTarget(self, action: #selector(playFromCurrentTime(button:)), for: UIControlEvents.touchUpInside)
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 45/baseWidth, height: 45/baseWidth))
        })
        playAgainButton?.isHidden = true
        
        //注册通知，暂停视频播放
        NotificationCenter.default.addObserver(self, selector: #selector(stopVideoPlayNotification(info:)), name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)
    }
    
    //MARK: 播放视频
    /// 播放视频
    func playVideo(){
        player?.playFromCurrentTime()
        playAgainButton?.isHidden = true
    }
    
    //MARK: 暂停视频
    /// 暂停视频
    ///
    /// - Parameter hidePlayButton: 是否需要显示播放按钮
    func pausePlayer(hidePlayButton: Bool = false){
        playAgainButton?.isHidden = hidePlayButton
        player?.pause()
    }
    
    //MARK: 重新开始播放视频
    func playFromCurrentTime(button: UIButton){
        playAgainButton?.isHidden = true
        player?.playFromCurrentTime()
    }
    
    //MARK: 接受通知，暂停视频播放
    func stopVideoPlayNotification(info: Notification){
        if player?.playbackState == PlaybackState.playing{
            pausePlayer()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        videoPath = nil
        playAgainButton?.isHidden = true
        player?.stop()
    }
    
    func playerReady(_ player: Player) {
    }
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    //MARK: 根据缓存情况，展示加载提示器
    func playerBufferingStateDidChange(_ player: Player) {
        switch player.bufferingState {
        case BufferingState.ready:
            playVideo()
            activeIndicator?.stopAnimating()
        default:
            activeIndicator?.startAnimating()
            break
        }
    }
    func playerCurrentTimeDidChange(_ player: Player){
    }
    func playerPlaybackWillStartFromBeginning(_ player: Player){
    }
    func playerPlaybackDidEnd(_ player: Player){
    }
    func playerPlaybackWillLoop(_ player: Player){
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

//MARk: 展示图片的Cell
class ARCardCollectionViewImageCell: UICollectionViewCell{
    
    var imageURL: URL?{
        didSet{
            imageView?.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeHolder"))
        }
    }
    
    fileprivate var imageView: BaseImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: nil)
        imageView?.clipsToBounds = true
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make) in
            make.center.equalTo(contentView)
            make.size.equalTo(CGSize(width: 312/baseWidth, height: 175/baseWidth))
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
