//
//  ARCardTorchButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ARCardTorchButton: BaseButton {

    //设置
    let device: AVCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setImage(UIImage(named: "localTorchOff"), for: UIControlState.normal)
        
        //MARK: 点击按钮的事件
        self.touchButtonClosure = {[weak self] _ in
            self?.turnOnOrTurnOffTorch()
        }
    }
    
    //MARK: 关闭闪关灯
    func turnOffTorch(){
        if device.hasTorch == true{
            if device.torchMode == AVCaptureTorchMode.on{
                setTorch(on: false)
            }
        }
    }
    
    //MARK: 开关闪光灯
    fileprivate func turnOnOrTurnOffTorch(){
        if device.hasTorch == true{
            if device.torchMode == AVCaptureTorchMode.on{
                setTorch(on: false)
            }else if device.torchMode == AVCaptureTorchMode.off{
                setTorch(on: true)
            }
        }
    }
    
    //MARK: 打开或者关闭闪光灯
    /// 打开或者关闭闪光灯
    ///
    /// - Parameter on: Bool
    func setTorch(on: Bool){
        if on == true && device.torchMode == AVCaptureTorchMode.off{
            do {
                try device.lockForConfiguration()
                device.torchMode = AVCaptureTorchMode.on
                device.unlockForConfiguration()
                self.setImage(UIImage(named: "localTorchOn"), for: UIControlState.normal)
            }catch{print(error)}
        }else if on == false && device.torchMode == AVCaptureTorchMode.on{
            do {
                try device.lockForConfiguration()
                device.torchMode = AVCaptureTorchMode.off
                device.unlockForConfiguration()
                self.setImage(UIImage(named: "localTorchOff"), for: UIControlState.normal)
            }catch{print(error)}
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
