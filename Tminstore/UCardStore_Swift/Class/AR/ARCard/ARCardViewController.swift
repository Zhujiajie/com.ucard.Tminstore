//
//  ARCardViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/8/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class ARCardViewController: BaseViewController {

    fileprivate let viewModel: ARCardViewModel = ARCardViewModel()
    
    /// 是否是展示视频的控制器
    var isVideo: Bool = true
    /// 识别图片路径
    var targetImagePath: String = ""
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    /// 退出ICON
    fileprivate var dismissICON: BaseImageView?
    /// 退出的按钮
    fileprivate var dismissButton: BaseButton?
    /// 标题label
    fileprivate var titleLabel: BaseLabel?
    /// 闪光灯按钮
    fileprivate var torchButton: ARCardTorchButton?
    /// AR图层
    fileprivate var localView: LocalView?
    /// AR图层的Frame
    fileprivate let arFrame: CGRect = UIScreen.main.bounds
    /// AR明信片的介绍视图
    fileprivate var arCardIntroView: ARCardIntroView?
    /// 摄像头预览的画面
    fileprivate var cameraPreviewView: ARCardCameraView?
    /// 展示视频或PPT的collectionView
    fileprivate var collectionViewController: ARCardCollectionViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        targetImagePath = Bundle.main.path(forResource: "IMG_2276", ofType: ".JPG")!
        
        print(targetImagePath)
        
        view.backgroundColor = UIColor.black
        setUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAR()//开启AR
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        torchButton?.turnOffTorch()//关闭闪光灯
        fireAR()//释放AR
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //停止获取摄像头画面
        if cameraPreviewView != nil {
            cameraPreviewView?.stopSession()
        }
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        localView?.resize(arFrame, orientation: UIInterfaceOrientation.portrait)
    }
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        localView?.setOrientation(toInterfaceOrientation)
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        dismissICON = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "whiteBackArrow")
        view.addSubview(dismissICON!)
        dismissICON?.snp.makeConstraints({ (make) in
            make.top.equalTo(view).offset(20/baseWidth)
            make.left.equalTo(view).offset(20/baseWidth)
            make.size.equalTo(CGSize(width: 12/baseWidth, height: 21/baseWidth))
        })
        
        dismissButton = BaseButton.normalIconButton()
        view.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(view)
            make.left.equalTo(view)
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 60/baseWidth))
        })
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andText: NSLocalizedString("AR名片", comment: ""))
        view.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(dismissButton!)
            make.left.equalTo(dismissButton!.snp.right).offset(25/baseWidth)
        })
        
        torchButton = ARCardTorchButton()
        view.addSubview(torchButton!)
        torchButton?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(dismissButton!)
            make.right.equalTo(view).offset(-26/baseWidth)
            make.size.equalTo(CGSize(width: 31/baseWidth, height: 31/baseWidth))
        })
    }
    
    //MARK: 开始AR扫描
    fileprivate func startAR(){
        initAR()
        closures()
    }
    
    //MARK: 初始化AR
    fileprivate func initAR(){
        if localView == nil {
            localView = LocalView(frame: arFrame)
            view.insertSubview(localView!, belowSubview: dismissICON!)
            localView?.setOrientation(UIInterfaceOrientation.portrait)
        }
        localView?.loadImage(targetImagePath)
        localView?.startAR()
    }
    
    //MARK: 释放AR
    fileprivate func fireAR(){
        localView?.active = false
        localView?.stop()
        localView?.clear()
        localView?.removeFromSuperview()
        localView = nil
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 点击退出按钮
        dismissButton?.touchButtonClosure = {[weak self] _ in
            self?.dismissSelf()
        }
        //MARK: C++的代理
        localView?.foundTarget = {[weak self] (found: Bool, index: Int32) in
            if found == true && self?.arCardIntroView == nil {
                self?.foundTarget()
            }
        }
    }
    
    //MARK: 识别成功设置介绍视图
    fileprivate func foundTarget(){
        arCardIntroView = ARCardIntroView(frame: arFrame)
        view.addSubview(arCardIntroView!)
        arCardIntroView?.isVideo = isVideo
        fireAR()//销毁AR识别
        cameraPreviewView = ARCardCameraView(frame: arFrame)
        view.insertSubview(cameraPreviewView!, belowSubview: dismissICON!)
        
        //设置collectionView
        collectionViewController = ARCardCollectionViewController(isVideo: isVideo, andViewModel: viewModel)
        view.insertSubview(collectionViewController!.view, belowSubview: arCardIntroView!)
        collectionViewController?.view.snp.makeConstraints({ (make) in
            make.top.equalTo(view).offset(213/baseWidth)
            make.left.right.equalTo(view)
            make.height.equalTo(175/baseWidth)
        })
    }
}
