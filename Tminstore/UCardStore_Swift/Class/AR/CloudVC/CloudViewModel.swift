//
//  CloudViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class CloudViewModel: BaseViewModel {

    fileprivate let dataModel: CloudDataModel = CloudDataModel()
    
    /// 获取子目录成功
    var getSubCatalogSuccess: (()->())?
    /// 获得子目录图片数据成功
    var getSubCatalogImageSuccess: (()->())?
    /// 没有更多数据了
    var noMoreData: (()->())?
    /// 云识别图片点赞的检查
    var isCLoudImageLiked: ((_ isLiked: Bool?)->())?
    
    /// 子目录的数据
    var subCatalogArray: [String] = [String]()
    /// 子目录图片数据数组
    var subCatalogImageArray: [PostcardModel] = [PostcardModel]()
    /// 当前页数
    var pageNum: Int = 1
    
    override init() {
        super.init()
        
        //MARK:出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
        
        //MARK: 网络请求结束的闭包
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 获取子目录数据
    /// 获取子目录数据
    ///
    /// - Parameter title: 父目录标题
    func requestSubCatalogData(title: String){
        showSVProgress(title: nil)
        dataModel.getSubCatalog(catalog: title) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let array: [String] = result["result"] as? [String]{
                    self?.subCatalogArray.removeAll()
                    self?.subCatalogArray.append(contentsOf: array)
                    self?.getSubCatalogSuccess?()
                    return
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 获取子目录数据
    /// 获取子目录数据
    ///
    /// - Parameter title: 子目录标题
    func requestImageData(indexPath: IndexPath){
        dataModel.getSubCatalogImages(subCatalog: subCatalogArray[indexPath.row], andPageIndex: pageNum) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let dicArray: [[String: Any]] = result["result"] as? [[String: Any]]{
                    if dicArray.isEmpty {
                        self?.noMoreData?()
                    }else{
                        if self?.pageNum == 1{
                            self?.subCatalogImageArray.removeAll()
                        }
                        for dic: [String: Any] in dicArray{
                            let model: PostcardModel = PostcardModel.initFromSpaceTimeCircle(dic: dic)
                            if model.frontImageURL != nil {
                                self?.subCatalogImageArray.append(model)
                            }
                        }
                        self?.getSubCatalogImageSuccess?()
                    }
                    self?.pageNum += 1
                    return
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 统计云识别次数
    /// 统计云识别次数
    ///
    /// - Parameter targrtId: id
    func countCloudAPI(targetId: String){
        dataModel.countCloudAPI(targetId: targetId)
        checkIsLiked(target: targetId)
    }
    
    //MARK: 判断是否已点赞
    /// 判断是否已点赞
    ///
    /// - Parameter target: 云识别图片ID
    fileprivate func checkIsLiked(target: String){
        dataModel.checkCloudIsLiked(targetId: target) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let message: String = result["message"] as? String{
                    if message == "未点赞" {
                        self?.isCLoudImageLiked?(false)
                    }else{
                        self?.isCLoudImageLiked?(true)
                    }
                    return
                }
            }
            self?.isCLoudImageLiked?(nil)
        }
    }
    
    //MARK: 点赞或者取消点赞
    /// 点赞或者取消点赞
    ///
    /// - Parameters:
    ///   - isLike: 点赞还是取消点赞
    ///   - targetId: 云识别图片的ID
    func setLikeOrCancelLike(isLike: Bool, andTargetId targetId: String){
        dataModel.setLikeOrCancelLike(isLike: isLike, andTargetId: targetId)
    }
}
