//
//  CloudController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/11.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 原先负责云识别的控制器, 改成本地识别

import UIKit
import ReplayKit
import SnapKit

class CloudController: BaseViewController {
    
//    /// 目标图片的URL
//    var targetImageURL: URL?
    /// 带识别图信息的json字符串
    var jsonString: String = ""
    fileprivate let localARViewModel: LocalNewARViewModel = LocalNewARViewModel()
    fileprivate let viewModel: CloudViewModel = CloudViewModel()
    fileprivate let recordViewModel: LocalARViewModel = LocalARViewModel()
    /// 闪光灯按钮
    fileprivate var torchBarItem: LocalARBarButtonItem?
    /// 分享相关的视图
    fileprivate var shareView: LocalNewARRecordView?
    /// 截屏确认分享的按钮
    fileprivate var snapShotShareView: LocalARSnapShareView?
    /// 当前已接受的targetId
    fileprivate var currentTargetId: String?
    /// AR识别时的加载圈
    fileprivate var arIndicator: BaseImageView?
    /// AR识别时的标题
    fileprivate var arIndicatorLabel: BaseLabel?
    
//    /// 线索图片
//    fileprivate var clueImage: BaseImageView?
//    /// 扫描动画
//    fileprivate var scanLine: UIImageView?
    // MARK: 隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        let blackView: BaseView = BaseView.colorLine(colorString: "3C3C3C")
        view.addSubview(blackView)
        blackView.frame = CGRect(x: 0, y: -navBarHeight, width: screenWidth, height: screenHeight)
        
        //初始化Unity视图
        view.addSubview(UnityGetGLView()!)
        UnityGetGLView()!.frame = CGRect(x: 0, y: -navBarHeight, width: screenWidth, height: screenHeight + navBarHeight)
        
        setNavigationBar()
        setUI()
        closures()
    }

    //MARK: 进入视图，开启AR
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //注册通知
        NotificationCenter.default.addObserver(self, selector: #selector(showShareSnapShotView(notification:)), name: Notification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getOriginalId(notification:)), name: NSNotification.Name(rawValue: "getOriginalId"), object: nil)

        
        UnitySendMessage("SceneManager", "LoadScene", "LocalAR");
        
        snapShotShareView = LocalARSnapShareView()
        view.addSubview(snapShotShareView!)
        snapShotShareView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(70/baseWidth)
            make.right.equalTo(view.snp.left).offset(0)
            make.size.equalTo(CGSize(width: 134/baseWidth, height: 40/baseWidth))
        })
        
        delay(seconds: 1, completion: {[weak self] _ in
            print((self?.jsonString)!)
            UnitySendMessage("EasyAR_ImageTracker_Local", "GetJsonData", self?.jsonString)
        }, completionBackground: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transparentNavigationBar()//导航栏透明
        app.currentUnityController.resumeUnity()//开启Unity
        snapShotShareView?.removeFromSuperview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        UnitySendMessage("EasyAR_ImageTracker_Local","LoadCLoudAR","2");
        app.currentUnityController.pauseUnity()//停止Unity
        if torchBarItem != nil{
            torchBarItem?.setTorch(on: false)//关闭闪关灯
        }
        
        snapShotShareView?.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("whiteBackArrow")
        torchBarItem = LocalARBarButtonItem.torchBarItem()
        navigationItem.rightBarButtonItem = torchBarItem!
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        shareView = LocalNewARRecordView()
        view.addSubview(shareView!)
        shareView?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(view)
            make.height.equalTo(100/baseWidth)
            make.bottom.equalTo(view).offset(100/baseWidth)
        })
        shareView?.setLikeButton()//设置点赞按钮
        
        arIndicator = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFit, andImageName: "arIndicator2")
        navigationController?.view.addSubview(arIndicator!)
        arIndicator?.snp.makeConstraints({ (make) in
            make.center.equalTo(navigationController!.view)
            make.size.equalTo(CGSize(width: 88/baseWidth, height: 88/baseWidth))
        })

        arIndicatorLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "#FFFFFF", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("识别中...", comment: ""))
        navigationController?.view.addSubview(arIndicatorLabel!)
        arIndicatorLabel?.snp.makeConstraints({ (make) in
            make.center.equalTo(navigationController!.view)
        })
        
        //添加旋转动画
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = -Double.pi * 2
        rotateAnimation.duration = 1.0
        rotateAnimation.repeatCount = MAXFLOAT
        
        arIndicator?.layer.add(rotateAnimation, forKey: nil)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击了录屏按钮的事件
        shareView?.touchRecordButtonClosure = {[weak self] _ in
            if self?.recordViewModel.isRecording() == true{
                self?.setChooseRecordActionAlert()
            }else{
                self?.recordViewModel.startRecord()
            }
        }
        
        //MARK: 点击截屏按钮的事件
        shareView?.touchSnapShotButtonClosure = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("同时按住电源键和主屏幕键来截屏分享", comment: ""))
        }
        
        //MARK: 需要停止录屏
        shareView?.shouldStopRecord = {[weak self] _ in
            self?.recordViewModel.stopRecord(shouldDiscard: false)
        }
        
        //MARK: 截图分享视图五秒钟计时结束
        snapShotShareView?.stopTimerClosure = {[weak self] _ in
            self?.showSnapShotShareView(shouldShow: false)
        }
        
        //MARK: 点击了分享视图，进入截图分享控制器
        snapShotShareView?.tapShareViewClosure = {[weak self] _ in
            guard let image: UIImage = self?.recordViewModel.snapShotImage else { return }
            let shareVC: SnapShotShareController = SnapShotShareController()
            shareVC.umengViewName = "截图分享"
            shareVC.snapShotImage = image
            _ = self?.navigationController?.pushViewController(shareVC, animated: true)
        }
        
        //MARK: 已经开始录屏的闭包
        recordViewModel.startRecordClosure = {[weak self] _ in
            self?.shareView?.beginRecord()
        }
        
        //MARK: 停止录屏的闭包
        recordViewModel.stopRecordAlert = {[weak self] _ in
            self?.setChooseRecordActionAlert()//让用户选择是否停止录制
        }
        
        //MARK: 警告弹窗
        recordViewModel.showWarmAlert = {[weak self] _ in
            self?.setWarnAlert()
        }
        
        //MARK: 已经停止录屏
        recordViewModel.recordStoped = {[weak self] _ in
            self?.shareView?.stopRecord()
        }
        
        //MARK: 需要展示录屏的结果
        recordViewModel.shouldPresentRecordResult = {[weak self] (vc: RPPreviewViewController) in
            vc.previewControllerDelegate = self
            self?.present(vc, animated: true, completion: nil)
        }
        
        //MARK: 检查点赞的结果
        viewModel.isCLoudImageLiked = {[weak self] (result: Bool?) in
            if result == nil {
                self?.shareView?.hideLikeButton()
            }else{
                self?.shareView?.setLikeButtonState(isLike: result!)
            }
        }
    }
    
    //MARK: 接受通知，弹出分享视图
    func showShareSnapShotView(notification: Notification){
        showSVProgress(title: nil)
        delay(seconds: 1.0, completion: {[weak self] _ in
            self?.dismissSVProgress()
            self?.recordViewModel.getSnapShot()
            self?.showSnapShotShareView(shouldShow: true)
        })
    }
    
    //MARK: 展示截屏分享视图
    fileprivate func showSnapShotShareView(shouldShow: Bool = true){
        if shouldShow == true{
            snapShotShareView?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(view.snp.left).offset(67/baseWidth)
            })
            snapShotShareView?.startTimer()
        }else{
            recordViewModel.snapShotImage = nil
            snapShotShareView?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(view.snp.left).offset(0)
            })
        }
        autoLayoutAnimation(view: view)
    }
    
    //MARK: 让用户选择是否停止录制
    fileprivate func setChooseRecordActionAlert(){
        let av: LocalRecordAlertViewController = LocalRecordAlertViewController.confirmStopAlert()
        //MARK: 完成录制
        av.stopRecord = {[weak self] _ in
            self?.recordViewModel.stopRecord(shouldDiscard: false)
        }
        //MARK: 取消录制
        av.cancel = {[weak self] _ in
            self?.recordViewModel.stopRecord(shouldDiscard: true)
        }
        present(av, animated: true, completion: nil)
    }
    
    //MARK: 提醒用户ReplayKit不可用
    fileprivate func setWarnAlert(){
        let ac: LocalRecordAlertViewController = LocalRecordAlertViewController.recordWarnAlert()
        present(ac, animated: true, completion: nil)
    }
    
    //MARK: 接受通知, 获取识别成功的图片的originalId
    func getOriginalId(notification: Notification){
        if let userInfo: [String: Any] = notification.userInfo as? [String: Any]{
            if let originalId: String = userInfo["originalId"] as? String{
                delay(seconds: 1.0, completion: { [weak self] _ in
                    self?.stopAnimation()
                }, completionBackground: nil)
                localARViewModel.computeARAlbumScanTimes(originalId: originalId)//统计扫描次数
            }
        }
    }
    
    //MARK: 停止动画
    fileprivate func stopAnimation(){
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.arIndicator?.alpha = 0
            self?.arIndicatorLabel?.alpha = 0
        }) { [weak self] (_) in
            self?.arIndicator?.layer.removeAllAnimations()
            self?.arIndicator?.removeFromSuperview()
            self?.arIndicatorLabel?.removeFromSuperview()
        }
        SVProgressHUD.showSuccess(withStatus: NSLocalizedString("识别成功", comment: " "))
        delay(seconds: 0.5, completion: {
            SVProgressHUD.dismiss()
        }, completionBackground: nil)
    }
}

extension CloudController: RPPreviewViewControllerDelegate{
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        previewController.dismiss(animated: true, completion: nil)
    }
    
    //MARK: 保存了视频。开始寻找视频
    func previewController(_ previewController: RPPreviewViewController, didFinishWithActivityTypes activityTypes: Set<String>) {
        if activityTypes.contains("com.apple.UIKit.activity.SaveToCameraRoll") == true{
            recordViewModel.requestVideo(success: { [weak self] (videoPath: String) in
                let shareVC: VideoShareController = VideoShareController()
                shareVC.umengViewName = "视频分享"
                shareVC.videoURLPath = videoPath
                _ = self?.navigationController?.pushViewController(shareVC, animated: true)
            })
        }
    }
}
