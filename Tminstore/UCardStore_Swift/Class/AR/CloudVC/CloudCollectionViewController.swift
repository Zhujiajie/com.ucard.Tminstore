//
//  CloudCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/13.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import MJRefresh
import SDWebImage

private let reuseIdentifier = "Cell"

class CloudCollectionViewController: UICollectionViewController {

    weak var viewModel: CloudViewModel?
    
    ///选中了cell
    var selecteCell: ((_ indexPath: IndexPath)->())?
    
    init() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 8/baseWidth
        layout.minimumInteritemSpacing = 6/baseWidth
        layout.scrollDirection = UICollectionViewScrollDirection.vertical
        layout.itemSize = CGSize(width: 176/baseWidth, height: 176/baseWidth)
        
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView!.register(CloudCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.backgroundColor = UIColor(hexString: "#F7F7F7")
        
        self.collectionView?.mj_header = BaseAnimationHeader()
        self.collectionView?.mj_footer = BaseRefreshFooter()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        return viewModel!.subCatalogImageArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CloudCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CloudCollectionViewCell
        cell.model = viewModel!.subCatalogImageArray[indexPath.item]
        return cell
    }

    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selecteCell?(indexPath)
    }
}

class CloudCollectionViewCell: UICollectionViewCell{
    
    ///数据
    var model: PostcardModel?{
        didSet{
            if let url: URL = model?.frontImageURL{
                //先判断是否已缓存图片
                SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                    
                    if result == true{
                        self?.imageView?.sd_setImage(with: url)
                    }else{
                        if let placeHolderURL: URL = self?.model?.frontPlaceImageURL{
                            self?.imageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                                DispatchQueue.main.async {
                                    if image != nil {
                                        self?.imageView?.sd_setImage(with: url, placeholderImage: image!)
                                    }else{
                                        self?.imageView?.sd_setImage(with: url)
                                    }
                                }
                            })
                        }else{
                            self?.imageView?.sd_setImage(with: url)
                        }
                    }
                })
            }
        }
    }
    
    fileprivate var imageView: BaseImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill, andImageName: nil)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
