//
//  CloudCatalogView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: --------------------展示主条目的视图---------------------

import UIKit

class CloudCatalogView: UIView {

    fileprivate enum ButtonType{
        case business
        case tour
        case college
        case start
    }
    
    ///被选中的按钮类型
    fileprivate var selectedButtonType: ButtonType = ButtonType.business{
        didSet{
            if selectedButtonType != oldValue{
                changeAllButtonColor()
            }
        }
    }
    
    /// 是否已选中
    fileprivate var isSelected: Bool = true
    
    /// 选择了主条目
    var catalogSelected: ((_ title: String)->())?
    /// 取消了选择
    var cancelSelected: (()->())?
    /// 商家
    var busniessButton: BaseButton?
    /// 路由
    var tourButton: BaseButton?
    /// 大学
    var collegeButton: BaseButton?
    /// 明星
    var startButton: BaseButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F7F7F7")
        addSubview(grayLine)
        grayLine.snp.makeConstraints { (make) in
            make.left.top.right.equalTo(self)
            make.height.equalTo(2)
        }
        
        busniessButton = setButton(title: "名片")
        addSubview(busniessButton!)
        busniessButton?.snp.makeConstraints({ (make) in
            make.top.equalTo(grayLine.snp.bottom)
            make.left.bottom.equalTo(self)
            make.width.equalTo(screenWidth/4)
        })
        //MARK: 点击了商家按钮
        busniessButton?.touchButtonClosure = {[weak self] _ in
            if self?.selectedButtonType == ButtonType.business{
                if self?.isSelected == true{
                    self?.cancelSelecteButton()
                }else{
                    self?.selecteButton(button: (self?.busniessButton)!)
                }
                return
            }
            self?.selectedButtonType = ButtonType.business
            self?.selecteButton(button: (self?.busniessButton)!)
        }
        
        tourButton = setButton(title: "明信片")
        addSubview(tourButton!)
        tourButton?.snp.makeConstraints({ (make) in
            make.top.bottom.equalTo(self)
            make.left.equalTo(busniessButton!.snp.right)
            make.width.equalTo(busniessButton!)
        })
        tourButton?.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40)
        tourButton?.imageEdgeInsets = UIEdgeInsets(top: 0, left: -15, bottom: 0, right: -15)
        //MARK: 点击了旅游按钮
        tourButton?.touchButtonClosure = {[weak self] _ in
            if self?.selectedButtonType == ButtonType.tour{
                if self?.isSelected == true{
                    self?.cancelSelecteButton()
                }else{
                   self?.selecteButton(button: (self?.tourButton)!)
                }
                return
            }
            self?.selectedButtonType = ButtonType.tour
            self?.selecteButton(button: (self?.tourButton)!)
        }
        
        collegeButton = setButton(title: "杂志")
        addSubview(collegeButton!)
        collegeButton?.snp.makeConstraints({ (make) in
            make.top.bottom.equalTo(self)
            make.left.equalTo(tourButton!.snp.right)
            make.width.equalTo(busniessButton!)
        })
        //MARK: 点击了大学按钮
        collegeButton?.touchButtonClosure = {[weak self] _ in
            if self?.selectedButtonType == ButtonType.college{
                if self?.isSelected == true{
                    self?.cancelSelecteButton()
                }else{
                    self?.selecteButton(button: (self?.collegeButton)!)
                }
                return
            }
            self?.selectedButtonType = ButtonType.college
            self?.selecteButton(button: (self?.collegeButton)!)
        }
        
        startButton = setButton(title: "其他")
        addSubview(startButton!)
        startButton?.snp.makeConstraints({ (make) in
            make.top.bottom.equalTo(self)
            make.left.equalTo(collegeButton!.snp.right)
            make.right.equalTo(self)
        })
        //MARK: 点击了明星按钮
        startButton?.touchButtonClosure = {[weak self] _ in
            if self?.selectedButtonType == ButtonType.start{
                if self?.isSelected == true{
                    self?.cancelSelecteButton()
                }else{
                    self?.selecteButton(button: (self?.startButton)!)
                }
                return
            }
            self?.selectedButtonType = ButtonType.start
            self?.selecteButton(button: (self?.startButton)!)
        }
        
        selecteButton(button: busniessButton!)
    }
    
    //MARK: 设置按钮
    fileprivate func setButton(title: String)->BaseButton{
        let button: BaseButton = BaseButton.leftTitleRightImageButton(iconName: "grayArrow", andFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "B6B6B6", andTitle: NSLocalizedString(title, comment: ""))
        return button
    }
    
    //MARK: 设置按钮被选中
    fileprivate func selecteButton(button: BaseButton){
        var title: String
        switch selectedButtonType {
        case ButtonType.business:
            title = "名片"
        case ButtonType.college:
            title = "杂志"
        case ButtonType.tour:
            title = "明信片"
        default:
            title = "其他"
        }
        button.setTitle(font: UIFont.systemFont(ofSize: 14), andTextColorHexString: "7AFBFF", andTitle: NSLocalizedString(title, comment: ""))
        button.setImage(UIImage(named: "greenArrowSelected"), for: UIControlState.normal)
        catalogSelected?(title)
        isSelected = true
    }
    
    //MARK: 取消按钮的选中
    /// 取消按钮的选中
    func cancelSelecteButton(){
        switch selectedButtonType {
        case ButtonType.business:
            cancelSeleteButtonStyle(button: busniessButton!)
        case ButtonType.college:
            cancelSeleteButtonStyle(button: collegeButton!)
        case ButtonType.tour:
            cancelSeleteButtonStyle(button: tourButton!)
        default:
            cancelSeleteButtonStyle(button: startButton!)
        }
        isSelected = false
    }
    
    //MARK: 取消按钮选中状态
    fileprivate func cancelSeleteButtonStyle(button: BaseButton){
        button.setImage(UIImage(named: "greenArrow"), for: UIControlState.normal)
        cancelSelected?()
    }
    
    //MARK: 改变所有button的颜色
    fileprivate func changeAllButtonColor(){
        busniessButton?.setTitle(font: UIFont.systemFont(ofSize: 14), andTextColorHexString: "B6B6B6", andTitle: NSLocalizedString("名片", comment: ""))
        busniessButton?.setImage(UIImage(named: "grayArrow"), for: UIControlState.normal)
        tourButton?.setTitle(font: UIFont.systemFont(ofSize: 14), andTextColorHexString: "B6B6B6", andTitle: NSLocalizedString("明信片", comment: ""))
        tourButton?.setImage(UIImage(named: "grayArrow"), for: UIControlState.normal)
        collegeButton?.setTitle(font: UIFont.systemFont(ofSize: 14), andTextColorHexString: "B6B6B6", andTitle: NSLocalizedString("杂志", comment: ""))
        collegeButton?.setImage(UIImage(named: "grayArrow"), for: UIControlState.normal)
        startButton?.setTitle(font: UIFont.systemFont(ofSize: 14), andTextColorHexString: "B6B6B6", andTitle: NSLocalizedString("其他", comment: ""))
        startButton?.setImage(UIImage(named: "grayArrow"), for: UIControlState.normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
