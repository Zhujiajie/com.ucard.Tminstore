//
//  CloudDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import Alamofire

class CloudDataModel: BaseDataModel {

    //MARK: 根据主条目获取所属子条目
    /// 根据主条目获取所属子条目
    ///
    /// - Parameters:
    ///   - catalog: 主条目的标题
    ///   - success: 成功的闭包
    func getSubCatalog(catalog: String, successClosure success: ((_ result: [String: Any])->())?){
        let urlString: String = appAPIHelp.checkSubCatalogAPI + catalog.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 根据子条目获取所属图片
    /// 根据子条目获取所属图片
    ///
    /// - Parameters:
    ///   - subCatalog: 子条目标题
    ///   - index: 页数
    ///   - success: 成功的闭包
    func getSubCatalogImages(subCatalog: String, andPageIndex index: Int, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.subCatalogImageAPI + subCatalog.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)! + "/\(index)"
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 统计云识别次数
    /// 统计云识别次数
    ///
    /// - Parameter targrtId: id
    func countCloudAPI(targetId: String){
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "targetId" : targetId
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.countCloudAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            print(result)
        }, failureClourse: nil)
    }
    
    //MARK: 判断是否已点赞
    /// 判断是否已点赞
    ///
    /// - Parameters:
    ///   - targetId: 云识别图的ID
    ///   - success: 成功的闭包
    func checkCloudIsLiked(targetId: String, successClosure success: ((_ result: [String: Any])->())?){
        let urlString: String = appAPIHelp.checkCloudIsLikedAPI + targetId + "/isLiked"
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 对用户进行点赞或者取消点赞
    /// 对用户进行点赞或者取消点赞
    ///
    /// - Parameters:
    ///   - isLike: 点赞还是取消点赞
    ///   - targetId: 目标ID
    func setLikeOrCancelLike(isLike: Bool, andTargetId targetId: String){
        let urlString: String = appAPIHelp.setCloudLikeAPI + "/" + targetId
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        if isLike {
            sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
                print(result)
            }, failureClourse: nil, completionClosure: nil)
        }else{
            sendPUTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
                print(result)
            }, failureClourse: nil, completionClosure: nil)
        }
    }
}
