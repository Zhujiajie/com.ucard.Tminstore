//
//  CloudTableViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 用来展示云识别字幕了的tableView
import UIKit

class CloudTableViewController: UITableViewController {

    weak var viewModel: CloudViewModel?
    
    ///选中了cell
    var selecteCell: ((_ IndexPath: IndexPath)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.clear
        self.tableView.backgroundColor = UIColor(white: 0.0, alpha: 0.79)
        self.tableView.register(CloudTableViewCell.self, forCellReuseIdentifier: "1")
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
//        let visualView: UIVisualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
//        view.addSubview(visualView)
//        visualView.frame = view.bounds
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        return viewModel!.subCatalogArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CloudTableViewCell = tableView.dequeueReusableCell(withIdentifier: "1", for: indexPath) as! CloudTableViewCell

         cell.title = viewModel!.subCatalogArray[indexPath.row]

        return cell
    }
 
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55/baseWidth
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selecteCell?(indexPath)
    }
}

class CloudTableViewCell: UITableViewCell{
    
    /// 标题文字
    var title: String?{
        didSet{
            titleLabel?.text = title
        }
    }
    
    ///标题
    fileprivate var titleLabel: BaseLabel?
    /// 右箭头
    fileprivate var rightArrowIcon: BaseImageView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        titleLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "FFFFFF")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(33/baseWidth)
        })
        
        rightArrowIcon = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill, andImageName: "rightArrow")
        contentView.addSubview(rightArrowIcon!)
        rightArrowIcon?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-22/baseWidth)
            make.size.equalTo(CGSize(width: 9, height: 15))
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
