//
//  CloudChooseController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/7/11.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class CloudChooseController: BaseViewController {
    
    /// 选择图片, 获得图片URL
    var getClueImageURL: ((_ imageURL: URL?)->())?
    
    fileprivate var enterCloudButton: BaseButton?
    
    fileprivate let viewModel: CloudViewModel = CloudViewModel()
    
    /// 选择目录的视图
    fileprivate var catalogView: CloudCatalogView?
    /// 展示子目录的tableView
    fileprivate var tableView: CloudTableViewController?
    /// 展示图片的collectionView
    fileprivate var collectionView: CloudCollectionViewController?
    /// 目前选中的子目录的indexPath
    fileprivate var selectedIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexString: "#F7F7F7")
        setNavigationBar()
        setUI()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        closures()
    }

    //MARK: 进入视图，开启AR
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if viewModel.subCatalogArray.isEmpty{
            viewModel.requestSubCatalogData(title: "名片")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cancelTansparentNavigationBar()//取消导航栏透明
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        setNavigationBarLeftButtonWithImageName("backArrow")
        setNavigationTitle(NSLocalizedString("更多扫描", comment: ""))
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        catalogView = CloudCatalogView()
        view.addSubview(catalogView!)
        catalogView?.snp.makeConstraints({ (make) in
            make.top.equalTo(view)
            make.left.right.equalTo(view)
            make.height.equalTo(43)
        })
        
        collectionView = CloudCollectionViewController()
        view.insertSubview(collectionView!.view, belowSubview: catalogView!)
        collectionView?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(catalogView!.snp.bottom)
            make.left.bottom.right.equalTo(view)
        })
        collectionView?.viewModel = viewModel
        
        tableView = CloudTableViewController()
        view.insertSubview(tableView!.view, belowSubview: catalogView!)
        tableView?.view.snp.makeConstraints({ (make) in
            make.top.equalTo(catalogView!.snp.bottom)
            make.centerX.equalTo(view)
            make.width.equalTo(screenWidth)
            make.height.equalTo(screenHeight - 87)
        })
        
        tableView?.viewModel = viewModel
    }
    
    //MARK: 是否展示tableView
    fileprivate func showOrHideTableView(isShow: Bool){
        tableView?.view.snp.updateConstraints({ (make: ConstraintMaker) in
            if isShow {
                make.top.equalTo(catalogView!.snp.bottom)
            }else{
                make.top.equalTo(catalogView!.snp.bottom).offset(-screenHeight)
            }
        })
        autoLayoutAnimation(view: view)
    }
    
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 选中分类
        catalogView?.cancelSelected = {[weak self] _ in
            self?.showOrHideTableView(isShow: false)
        }
        
        //MARK: 点击了目录选择视图上的按钮
        catalogView?.catalogSelected = {[weak self] (title: String) in
            self?.viewModel.requestSubCatalogData(title: title)
            self?.showOrHideTableView(isShow: true)
        }
        
        //MARK: 选择了tableView的cell
        tableView?.selecteCell = { [weak self] (indexPath: IndexPath) in
            self?.selectedIndexPath = indexPath
            self?.catalogView?.cancelSelecteButton()
            self?.viewModel.pageNum = 1
            self?.collectionView?.collectionView?.mj_header.beginRefreshing()
        }
        
        //MARK: 下拉刷新
        collectionView?.collectionView?.mj_header.refreshingBlock = {[weak self] _ in
            if self?.selectedIndexPath != nil {
                self?.viewModel.pageNum = 1
                self?.viewModel.requestImageData(indexPath: (self?.selectedIndexPath)!)
            }
        }
        
        //MARK: 上拉刷新
        collectionView?.collectionView?.mj_footer.refreshingBlock = {[weak self] _ in
            if self?.selectedIndexPath != nil {
                self?.viewModel.requestImageData(indexPath: (self?.selectedIndexPath)!)
            }
        }
        
        //MARK: 选中了collectionView的cell
        collectionView?.selecteCell = {[weak self] (indexPath: IndexPath) in
//            let vc: CloudController = CloudController()
//            vc.targetImageURL = self?.viewModel.subCatalogImageArray[indexPath.item].frontImageURL
//            _ = self?.navigationController?.pushViewController(vc, animated: true)
            
            self?.dismissSelf(animated: true, completion: {
                self?.getClueImageURL?(self?.viewModel.subCatalogImageArray[indexPath.item].frontImageURL)
            })
        }
        
        //MARK: 成功获取子目录数据
        viewModel.getSubCatalogSuccess = {[weak self] _ in
            self?.tableView?.tableView.reloadData()
            self?.showOrHideTableView(isShow: true)
        }
        
        //MARK: 成功获得子目录图片数据
        viewModel.getSubCatalogImageSuccess = {[weak self] _ in
            self?.collectionView?.collectionView?.mj_header.endRefreshing()
            self?.collectionView?.collectionView?.mj_footer.endRefreshing()
            self?.collectionView?.collectionView?.reloadData()
        }
        
        //MARK: 没有更多数据了
        viewModel.noMoreData = {[weak self] _ in
            self?.collectionView?.collectionView?.mj_header.endRefreshing()
            self?.collectionView?.collectionView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
}
