//
//  QRViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

class QRViewModel: BaseViewModel {
    
    fileprivate let dataModel: QRDataModel = QRDataModel()
    
    /// 成功获取到AR识别图的下载地址和视频地址
    var requestARImageSuccess: ((_ imagePath: String, _ videoURLString: String)->())?
    
    /// 错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    override init() {
        super.init()
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 获取AR识别图信息
    func requestARImageInfo(code: String){
        
        showSVProgress(title: NSLocalizedString("正在下载识别资源", comment: ""))
        
        dataModel.requestOriginalId(code: code) { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let originalId: String = data["originalId"] as? String{
                        self?.requestARImageURLAndVideoURL(originalId: originalId)
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 获取AR识别图的下载地址
    fileprivate func requestARImageURLAndVideoURL(originalId: String){
        
        dataModel.requestARImageInfo(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let frontImageUrl: String = data["frontImageUrl"] as? String{
                        if let videoUrl: String = data["videoUrl"] as? String{
                            let imageURLString: String = frontImageUrl + "!unity"
                            self?.downARImage(imageURLString: imageURLString, andVideoUrlString: videoUrl)
                            return
                        }
                    }
                }
            }
            self?.errorClosure?(nil, result)
            self?.dismissSVProgress()
        })
    }
    
    //MARK: 下载AR识别图
    fileprivate func downARImage(imageURLString: String, andVideoUrlString videoUrlString: String){
        
        let prefetcher: SDWebImagePrefetcher = SDWebImagePrefetcher.shared()
        prefetcher.options = SDWebImageOptions.highPriority
        prefetcher.prefetchURLs([URL(string: imageURLString)!], progress: nil, completed: { [weak self] (count: UInt, skipCount: UInt) in
            
            if skipCount == 0{
                if let imageCache: SDImageCache = SDWebImageManager.shared().imageCache, let imagePath: String = imageCache.defaultCachePath(forKey: imageURLString){
                    //成功下载识别图片
                    DispatchQueue.main.async{
                        
                        self?.requestARImageSuccess?(imagePath, videoUrlString)
                    }
                }
            }
            //下载图片出错
            else{
                let errorDic: [String: Any] = [
                    "message": NSLocalizedString("下载AR图片失败，请重试", comment: "")
                ]
                self?.errorClosure?(nil, errorDic)
            }
            DispatchQueue.main.async {
                self?.dismissSVProgress()
            }
        })
    }
}
