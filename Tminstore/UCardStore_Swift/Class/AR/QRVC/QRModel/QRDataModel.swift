//
//  QRVDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class QRDataModel: BaseDataModel {
    
    //MARK: 根据ARCode，向后台索取originalId
    /// 根据ARCode，向后台索取originalId
    ///
    /// - Parameters:
    ///   - code: AR图片的code
    ///   - success: 成功的闭包
    func requestOriginalId(code: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.requestARImageOriginalIdAPI + code
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 通过originalId向后端索取图片的下载地址和对应的视频
    /// 通过originalId向后端索取图片的下载地址和对应的视频
    ///
    /// - Parameters:
    ///   - originalId: 原创id
    ///   - success: 成功的闭包
    func requestARImageInfo(originalId: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.requestARImageInfoAPI + originalId
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
