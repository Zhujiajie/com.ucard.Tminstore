//
//  QRViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/8.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import IQKeyboardManagerSwift

class QRViewController: LBXScanViewController {

    fileprivate let viewModel: QRViewModel = QRViewModel()
    
    /// 是否需要展示密码输入框
    var shouldDisplayCodeInputView: Bool = false
    /// 获得了二维码
    var getQRCode: ((_ code: String)->())?
    /// 是否是来自二维码界面
    var isFromCouponVC: Bool = false
    /// 界面样式
    fileprivate var style: LBXScanViewStyle?
    /// 闪光灯是否开启
    fileprivate var isOpenFlash: Bool = false
    /// 退出按钮
    fileprivate var dismissButton: EasyARButton?
    /// 打开闪关灯的按钮
    fileprivate var torchButton: QRScanButton?
    /// 打开相册的按钮
    fileprivate var photoButton: QRScanButton?
    /// 输入AR密码code的textField
    fileprivate var arCodeView: QRTextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setStyle()
        self.isOpenInterestRect = true//限制在矩形区域内才识别
        self.navigationController?.navigationBar.isHidden = true
        setButtons()
        closure()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        view.bringSubview(toFront: dismissButton!)
        view.bringSubview(toFront: photoButton!)
        view.bringSubview(toFront: torchButton!)
        if shouldDisplayCodeInputView == true{
            view.bringSubview(toFront: arCodeView!)
        }
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().shouldShowTextFieldPlaceholder = false
        MobClick.beginLogPageView("二维码扫描")//友盟页面统计
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MobClick.endLogPageView("二维码扫描")//友盟页面统计
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置界面的样式
    fileprivate func setStyle(){
        
        style = LBXScanViewStyle()
        style?.centerUpOffset = 0//扫描区域上移
        style?.photoframeAngleStyle = LBXScanViewPhotoframeAngleStyle.On
        style?.photoframeLineW = 6
        style?.photoframeAngleW = 24
        style?.photoframeAngleH = 24
        style?.isNeedShowRetangle = true
        style?.anmiationStyle = LBXScanViewAnimationStyle.NetGrid
        
        style?.animationImage = UIImage(named: "qrcode_scan_part_net")
        
        self.scanStyle = style
    }

    //MARK: 设置按钮
    fileprivate func setButtons(){
        dismissButton = EasyARButton.dismissButton()
        view.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view).offset(3/baseHeight + statusBarHeight)
            make.left.equalTo(view).offset(13/baseWidth)
        }
        
        photoButton = QRScanButton.photoButton()
        view.addSubview(photoButton!)
        photoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(view).offset(81/baseWidth)
            make.bottom.equalTo(view).offset(-123/baseHeight)
            make.size.equalTo(CGSize(width: 65/baseWidth, height: 65/baseWidth))
        })
        
        torchButton = QRScanButton.torchButton()
        view.addSubview(torchButton!)
        torchButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(view).offset(-81/baseWidth)
            make.bottomMargin.equalTo(photoButton!)
            make.size.equalTo(photoButton!)
        })
        
        //是否展示可输入AR密码的视图
        if shouldDisplayCodeInputView == true{
            arCodeView = QRTextField()
            view.addSubview(arCodeView!)
            arCodeView?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(view).offset(118/baseWidth)
                make.centerX.equalTo(view)
                make.size.equalTo(CGSize(width: 328/baseWidth, height: 54/baseWidth))
            })
        }
    }
    
    //MARK: 处理扫描结果
    override func handleCodeResult(arrayResult: [LBXScanResult]) {
        if let result: LBXScanResult = arrayResult.first{
            if let str: String = result.strScanned{
                
                if str.hasPrefix(javaBaseAPI + "codeCard/code?code=") == true{
                    let code: String = str.replacingOccurrences(of: javaBaseAPI + "codeCard/code?code=", with: "")
                    viewModel.requestARImageInfo(code: code)
                }
                else if str.hasPrefix(javaBaseAPI + "ucardstore/codeCard/code?code=") == true{
                    let code: String = str.replacingOccurrences(of: javaBaseAPI + "ucardstore/codeCard/code?code=", with: "")
                    viewModel.requestARImageInfo(code: code)
                }
                else{
                    // 如果是来自优惠券界面的，则dismiss
                    if isFromCouponVC == true{
                        dismissSelf(completionClosure: { [weak self] _ in
                            self?.getQRCode?(str)
                        })
                    }
                        //若不是来自优惠券界面的，则打开优惠券界面
                    else{
                        let couponVC = MUCouponVC()
                        couponVC.canPay = false
                        couponVC.umengViewName = "查看优惠券"
                        couponVC.qrCodeNeedToAdd = str
                        couponVC.useDismiss = true
                        let nav: UINavigationController = UINavigationController(rootViewController: couponVC)
                        present(nav, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    //MARK: 各种回调
    fileprivate func closure(){
        //MARK: 点击退出按钮的事件
        dismissButton?.touchDismissButton = {[weak self] _ in
            self?.dismissSelf()
        }
        
        //MARK: 点击闪光灯按钮
        torchButton?.touchButtonClosure = {[weak self] _ in
            self?.isOpenFlash = !(self?.isOpenFlash)!
            self?.scanObj?.setTorch(torch: (self?.isOpenFlash)!)
            if self?.isOpenFlash == true{
                self?.torchButton?.setImage(UIImage(named: "torchOn"), for: UIControlState.normal)
            }else{
                self?.torchButton?.setImage(UIImage(named: "torch"), for: UIControlState.normal)
            }
        }
        
        //MARK: 点击相册按钮
        photoButton?.touchButtonClosure = {[weak self] _ in
            self?.openPhotoAlbum()
        }
        
        //MARK: 点击了确认AR密码的按钮
        arCodeView?.confirmCode = {[weak self] (code: String) in
            self?.viewModel.requestARImageInfo(code: code)
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 成功下载AR识别图的闭包
        viewModel.requestARImageSuccess = {[weak self] (imagePath: String, videoURLString: String) in
            //进入本地识别
            let arVC: LocalARCardViewController = LocalARCardViewController()
            arVC.arImagePath = imagePath
            arVC.videoURLString = videoURLString
            self?.present(arVC, animated: true, completion: nil)
        }
    }
    
    //MARK: 退出
    fileprivate func dismissSelf(animated: Bool = true, completionClosure completion: (()->())? = nil){
        let rootVC: UIViewController? = presentingViewController
        rootVC?.dismiss(animated: true, completion: {
            completion?()
        })
    }
    
    //MARK: 识别图片
    override func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        dismiss(animated: true, completion: nil)
        
        if let image: UIImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            let arrayResult = LBXScanWrapper.recognizeQRImage(image: image)
            if arrayResult.isEmpty == false{
                handleCodeResult(arrayResult: arrayResult)
            }
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:------------处理网络请求失败-----------------
    /**
     处理网络请求失败，token过期则让用户重新登陆。其他错误则弹窗
     
     - parameter error:        alamofire返回的错误
     - parameter loginSuccess: 重新登录成功的闭包
     */
    func handelNetWorkError(_ error: Error? = nil, withResult result: [String: Any]? = nil, loginSuccessClosure loginSuccess: (()->())? = nil){
        
        //提醒用户出错
        if #available(iOS 10.0, *) {
            let notificationGenerator: UINotificationFeedbackGenerator = UINotificationFeedbackGenerator()
            notificationGenerator.prepare()
            notificationGenerator.notificationOccurred(UINotificationFeedbackType.error)
        }
        
        // token过期，让用户重新登录
        if let code: Int = result?["code"] as? Int, code == 1003{
            app.configLoginVC()
            MobClick.profileSignOff()//用户退出登录，清空友盟的账号信息
            saveUserID(userId: "")//清空用户的id
            saveUserName(name: nil)//清空用户昵称
            setLoginStatus(loginStatus: false)//设置app为未登陆状态
            setUserHasLoadAPP(false)//设置用户未登陆过app
            return
        }
        
        if let message: String = result?["message"] as? String{// 打印Java后台返回的错误
            alert(alertTitle: message)
            return
        }else{
            //网络错误
            if error != nil{
                alert(alertTitle: error?.localizedDescription)
            }else{
                alert(alertTitle: NSLocalizedString("网络出错，请稍后再试", comment: ""))
            }
        }
    }
    
    //MARK: 创建提示框
    /**
     创建提示框
     
     - parameter title:      标题
     - parameter message:    信息
     - parameter leftTitle:  左按钮标题 默认 字符串 “OK” or “好的”
     - parameter rightTitle: 右按钮标题
     - parameter left:       左按钮点击事件
     - parameter right:      右按钮点击事件
     - parameter complition: 提示框弹出完成之后的闭包
     */
    func alert(alertTitle title: String?, messageString message: String? = nil, leftButtonTitle leftTitle: String = NSLocalizedString("好的", comment: ""), rightButtonTitle rightTitle: String? = nil, leftClosure left: (()->())? = nil, rightClosure right: (()->())? = nil, presentComplition complition: (()->())? = nil){
        
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let leftAc: UIAlertAction = UIAlertAction(title: leftTitle, style: .cancel) { (_) in
            left?()
        }// 左按钮
        ac.addAction(leftAc)
        
        if (rightTitle != nil) {
            let rightAc: UIAlertAction = UIAlertAction(title: rightTitle, style: .default, handler: { (_) in
                right?()
            })//右按钮
            ac.addAction(rightAc)
        }
        
        present(ac, animated: true) {
            complition?()
        }//提示框弹出结束后的事件
    }
}
