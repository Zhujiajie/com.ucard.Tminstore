//
//  QRScanButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/8.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class QRScanButton: UIButton {

    /// 点击按钮的事件
    var touchButtonClosure: (()->())?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 打开相册的按钮
    /// 打开相册的按钮
    ///
    /// - Returns: QRScanButton
    class func photoButton()->QRScanButton{
        let button: QRScanButton = QRScanButton()
        button.setImage(UIImage(named: "QRImage"), for: UIControlState.normal)
        return button
    }
    
    //MARK: 打开闪光灯的按钮
    /// 打开闪光灯的按钮
    ///
    /// - Returns: QRScanButton
    class func torchButton()->QRScanButton{
        let button: QRScanButton = QRScanButton()
        button.setImage(UIImage(named: "torch"), for: UIControlState.normal)
        return button
    }

    //MARK: 点击按钮的事件
    func touchButton(button: UIButton){
        touchButtonClosure?()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
