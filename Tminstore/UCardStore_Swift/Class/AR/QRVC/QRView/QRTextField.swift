//
//  QRTextField.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class QRTextField: UIView {

    var confirmCode: ((_ code: String)->())?
    
    fileprivate var textField: UITextField?
    fileprivate var confirmButton: UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        textField = UITextField()
        textField?.backgroundColor = UIColor.white
        textField?.autocorrectionType = UITextAutocorrectionType.no
        textField?.autocapitalizationType = UITextAutocapitalizationType.none
        textField?.spellCheckingType = UITextSpellCheckingType.no
        textField?.keyboardAppearance = UIKeyboardAppearance.dark
        textField?.keyboardType = UIKeyboardType.numberPad
        textField?.font = UIFont.systemFont(ofSize: 14)
        textField?.textAlignment = NSTextAlignment.center
        textField?.placeholder = NSLocalizedString("请输入密码查看AR视频", comment: "")
        addSubview(textField!)
        textField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.top.bottom.equalTo(self)
            make.right.equalTo(self).offset(-67/baseWidth)
        })
        
        confirmButton = UIButton()
        confirmButton?.backgroundColor = UIColor(hexString: "4CDFED")
        confirmButton?.setImage(UIImage(named: "arCodeConfirm"), for: UIControlState.normal)
        addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(textField!.snp.right)
            make.top.right.bottom.equalTo(self)
        })
        confirmButton?.addTarget(self, action: #selector(touchConfirmButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 点击确认按钮，获取用户输入的code
    func touchConfirmButton(button: UIButton){
        if textField?.text == nil || textField?.text == ""{return}
        confirmCode?(textField!.text!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
