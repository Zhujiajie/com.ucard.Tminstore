//
//  LocalNewARRecordView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/28.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class LocalNewARRecordView: UIView {
    
    /// 要停止录屏
    var shouldStopRecord: (()->())?
    /// 点击了录屏按钮
    var touchRecordButtonClosure: (()->())?
    /// 点击了截屏按钮
    var touchSnapShotButtonClosure: (()->())?
    /// 点击了返回按钮的事件
    var touchBackButtonClosure: (()->())?
    /// 点击了点赞按钮
    var touchLikeButtonClosure: ((_ isLike: Bool)->())?
    
    /// 录屏时间的label
    fileprivate var recordTimeLabel: BaseLabel?
    /// 录屏的计时器
    fileprivate var timer: Timer?
    /// 录屏的计时
    fileprivate var recordCount: Int = 0{
        didSet{
            recordTimeLabel?.text = "\(recordCount)s"
            if ovalShapeLayer != nil{
                ovalShapeLayer?.strokeEnd = CGFloat(recordCount)/CGFloat(30)
            }
        }
    }
    /// 录屏按钮
    fileprivate var recordButton: BaseButton?
    /// 录屏label
    fileprivate var recordLabel: BaseLabel?
    /// 截屏的按钮
    fileprivate var snapShotButton: BaseButton?
    /// 截屏的label
    fileprivate var snapShotLabel: BaseLabel?
    /// 返回的按钮
    fileprivate var backButton: BaseButton?
    /// 点赞的按钮
    fileprivate var likeButton: BaseButton?
    /// 点赞的label
    fileprivate var likeLabel: BaseLabel?
    
    /// 录屏时的填充layer
    fileprivate var ovalShapeLayer: CAShapeLayer?
    
    /// 是否已点赞状态
    fileprivate var isLiked: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        
        //录屏
        recordButton = BaseButton.normalIconButton(iconName: "arRecord")
        addSubview(recordButton!)
        recordButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-38/baseWidth)
            make.size.equalTo(CGSize(width: 55/baseWidth, height: 55/baseWidth))
        })
        recordLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andText: NSLocalizedString("分享", comment: ""))
        addSubview(recordLabel!)
        recordLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(recordButton!.snp.bottom).offset(4/baseWidth)
            make.centerX.equalTo(recordButton!)
        })
        //录屏时的事件label
        recordTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18), andTextColorHex: "FFFFFF")
        addSubview(recordTimeLabel!)
        recordTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(recordButton!)
            make.bottom.equalTo(recordButton!.snp.top).offset(-15/baseWidth)
        })
        recordTimeLabel?.isHidden = true
        
        //截屏按钮
        snapShotButton = BaseButton.normalIconButton(iconName: "snapShotIcon")
        addSubview(snapShotButton!)
        snapShotButton?.snp.makeConstraints({ (make) in
            make.bottom.equalTo(self).offset(-43/baseWidth)
            make.left.equalTo(recordButton!.snp.right).offset(56/baseWidth)
            make.size.equalTo(CGSize(width: 37/baseWidth, height: 37/baseWidth))
        })
        snapShotLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andText: NSLocalizedString("截屏", comment: ""))
        addSubview(snapShotLabel!)
        snapShotLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(snapShotButton!.snp.bottom).offset(9/baseWidth)
            make.centerX.equalTo(snapShotButton!)
        })
        
        //返回按钮
        backButton = BaseButton.normalIconButton(iconName: "localARBack")
        addSubview(backButton!)
        backButton?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(snapShotButton!)
            make.size.equalTo(snapShotButton!)
            make.right.equalTo(recordButton!.snp.left).offset(-43/baseWidth)
        })
        
        //MARK: 点击录屏按钮的事件
        recordButton?.touchButtonClosure = {[weak self] _ in
            self?.touchRecordButtonClosure?()
        }
        //MARK: 点击截屏按钮的事件
        snapShotButton?.touchButtonClosure = {[weak self] _ in
            self?.touchSnapShotButtonClosure?()
        }
        //MARK: 点击返回按钮的事件
        backButton?.touchButtonClosure = {[weak self] _ in
            if self?.timer?.isValid == true{return}
            self?.touchBackButtonClosure?()
        }
    }
    
    //MARK: 进入录屏状态
    /// 进入录屏状态
    func beginRecord(){
        recordButton?.setImage(UIImage(named: "arRecording"), for: UIControlState.normal)
        recordTimeLabel?.isHidden = false
        recordCount = 0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.recordLabel?.alpha = 0
            }, completion: nil)
        
        if timer?.isValid == true{
            timer?.invalidate()
        }
        timer = Timer(timeInterval: 1, target: self, selector: #selector(recordCount(timer:)), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: RunLoopMode.defaultRunLoopMode)
        if ovalShapeLayer == nil{
            ovalShapeLayer = CAShapeLayer()
            ovalShapeLayer?.strokeColor = UIColor.white.cgColor//外部线条颜色
            ovalShapeLayer?.fillColor = UIColor.clear.cgColor//内部填充颜色
            ovalShapeLayer?.lineWidth = 4
            let bounds: CGRect = recordButton!.bounds
            ovalShapeLayer?.path = UIBezierPath(arcCenter: CGPoint(x: bounds.width/2, y: bounds.height/2), radius: bounds.height/2, startAngle: CGFloat.pi, endAngle: CGFloat.pi * 3, clockwise: true).cgPath//填充路径，以及开始的位置
            recordButton?.layer.addSublayer(ovalShapeLayer!)
            ovalShapeLayer?.strokeEnd = 0.0
        }
    }
    
    //MARK: 停止录屏
    func stopRecord(){
        if timer?.isValid == true{
            timer?.invalidate()
        }
        recordButton?.setImage(UIImage(named: "arRecord"), for: UIControlState.normal)
        recordTimeLabel?.isHidden = true
        recordCount = 0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.recordLabel?.alpha = 1
            }, completion: nil)
    }
    
    //MARK: 录屏的计时器
    func recordCount(timer: Timer){
        recordCount += 1
        if recordCount >= 30{
            shouldStopRecord?()
            timer.invalidate()
        }
    }
    
    //MARK: 设置点赞按钮
    /// 设置点赞按钮
    func setLikeButton(){
        backButton?.removeFromSuperview()
        
        //点赞的按钮
        likeButton = BaseButton.normalIconButton(iconName: "cloudLIkeButton")
        
        addSubview(likeButton!)
        likeButton?.snp.makeConstraints({ (make) in
            make.bottomMargin.equalTo(snapShotButton!)
            make.right.equalTo(recordButton!.snp.left).offset(-56/baseWidth)
            make.size.equalTo(snapShotButton!)
        })
        likeButton?.touchButtonClosure = {[weak self] _ in
            self?.isLiked = !(self?.isLiked)!
            self?.setLikeButtonState(isLike: (self?.isLiked)!)
            self?.touchLikeButtonClosure?((self?.isLiked)!)
        }
        //点赞label
        likeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andText: NSLocalizedString("点赞", comment: ""))
        addSubview(likeLabel!)
        likeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(snapShotLabel!)
            make.centerX.equalTo(likeButton!)
        })
        
        likeButton?.isHidden = true
        likeLabel?.isHidden = true
    }
    
    //MARK: 设置点赞按钮的状态
    /// 设置点赞按钮的状态
    ///
    /// - Parameter isLike: 点赞的状态
    func setLikeButtonState(isLike: Bool){
        likeButton?.isHidden = false
        likeLabel?.isHidden = false
        self.isLiked = isLike
        if isLike {
            likeButton?.setImage(UIImage(named: "cloudLIkedButton"), for: UIControlState.normal)
        }else{
            likeButton?.setImage(UIImage(named: "cloudLIkeButton"), for: UIControlState.normal)
        }
    }
    
    //MARK: 隐藏点赞按钮
    /// 隐藏点赞按钮
    func hideLikeButton(){
        likeButton?.isHidden = true
        likeLabel?.isHidden = true
    }
    
    deinit {
        timer?.invalidate()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
