//
//  LocalNewARDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class LocalNewARDataModel: BaseDataModel {
    
    //MARK: 获取AR时光相册的接口
    /// 获取AR时光相册的接口
    ///
    /// - Parameters:
    ///   - code: 密码
    ///   - success: 成功的闭包
    func requsetARAlbum(code: String, successClosure success: ((_ result: [String: Any])->())?){
        let urlString: String = appAPIHelp.arAlbumAPI + code
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 请求用户的AR识别图片
    /// 请求用户的AR识别图片
    ///
    /// - Parameters:
    ///   - pageNum: 分页页数
    ///   - success: 成功的闭包
    func requestUserARImage(pageNum: Int, successClosure success: ((_ result: [String: Any])->())?) {
        let urlStr: String = appAPIHelp.checkUserARImageAPI + "\(pageNum)"
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 请求单张图片对应的视频
    /// 请求单张图片对应的视频
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    func requestSingleImageVideo(originalId: String, successClosure success: ((_ result: [String: Any])->())?){
        let urlStr: String = appAPIHelp.checkSingleImageVideoAPI + originalId + "/1"
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
    
    //MARK: 统计AR时光相册的扫描次数
    /// 统计AR时光相册的扫描次数
    ///
    /// - Parameter originalId: 原创ID
    func computeARAlbumScanTimes(originalId: String){
        let urlString: String = appAPIHelp.arAlbumScanTimesAPI + originalId
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result) in
            print(result)
        }, failureClourse: nil)
    }
}
