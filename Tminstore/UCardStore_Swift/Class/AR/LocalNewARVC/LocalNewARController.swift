//
//  LocalNewARController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import ReplayKit
import SnapKit
import IQKeyboardManagerSwift

class LocalNewARController: BaseViewController {
    
    fileprivate let cloudViewModel: CloudViewModel = CloudViewModel()
    fileprivate let qrViewModel: QRViewModel = QRViewModel()
    fileprivate let viewModel: LocalNewARViewModel = LocalNewARViewModel()
    fileprivate let recordViewModel: LocalARViewModel = LocalARViewModel()
    
    /// 介绍玩法的视图
    fileprivate var arIntroView: LocalNewARIntroView?
    /// 选择AR图片的视图
    fileprivate var chooseARView: LocalNewARChooseView?
    /// 闪光灯按钮
    fileprivate var torchBarItem: LocalARBarButtonItem?
    /// 更多的按钮
    fileprivate var moreBarItem: BaseBarButtonItem?
    /// 分享相关的视图
    fileprivate var shareView: LocalNewARRecordView?
    /// 截屏确认分享的按钮
    fileprivate var snapShotShareView: LocalARSnapShareView?
    /// 扫描动画
    fileprivate var scanLine: UIImageView?
    /// 线索图片
    fileprivate var clueImage: BaseImageView?
    /// 当前已接受的targetId
    fileprivate var currentTargetId: String?
    
    // MARK: 隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    /// 用来展示摄像头的layer
    fileprivate var cameraLayer: AVCaptureVideoPreviewLayer?
    fileprivate var session: AVCaptureSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let blackView: BaseView = BaseView.colorLine(colorString: "3C3C3C")
        view.addSubview(blackView)
        blackView.frame = CGRect(x: 0, y: -navBarHeight, width: screenWidth, height: screenHeight)
        
        setNavigationBar()
        setUI()
        closures()
    }

    //MARK: 进入视图，开启AR
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //注册通知
        NotificationCenter.default.addObserver(self, selector: #selector(showShareSnapShotView(notification:)), name: Notification.Name.UIApplicationUserDidTakeScreenshot, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(getTargetID(notification:)), name: NSNotification.Name("getTargetID"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDismiss), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        app.window?.rootViewController = app.currentUnityController.rootViewController
        app.currentUnityController.resumeUnity()
//        UnitySendMessage("SceneManager", "LoadScene", "CloudAR")
        
        IQKeyboardManager.sharedManager().enable = true
        
        if viewModel.hasRequestUserData == false{
            //判断用户是否登录
            if getUserLoginStatus() {
                viewModel.requestUserARImage()
            }
            viewModel.hasRequestUserData = true
        }
        
        delay(seconds: 2, completion: {[weak self] _ in
            self?.setIntroView()
        }, completionBackground: nil)
        
        //初始化Unity视图
        view.insertSubview(UnityGetGLView()!, belowSubview: chooseARView!)
        UnityGetGLView()!.frame = CGRect(x: 0, y: -navBarHeight, width: screenWidth, height: screenHeight + navBarHeight)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transparentNavigationBar()//导航栏透明
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        
//        UnitySendMessage("SceneManager", "LoadScene", "CloudAR")
        app.currentUnityController.pauseUnity()//停止Unity
       
        
        dismissClueImage()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if torchBarItem != nil{
            torchBarItem?.setTorch(on: false)//关闭闪关灯
        }
        IQKeyboardManager.sharedManager().enable = false
        
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("dismissPlayer")
        torchBarItem = LocalARBarButtonItem.torchBarItem()
        moreBarItem = BaseBarButtonItem.normalIconBarButtonItem(iconName: "arMore")
        navigationItem.rightBarButtonItems = [moreBarItem!, torchBarItem!]
    }
    
    //MARK: 设置介绍视图
    fileprivate func setIntroView(){
        if hasLoadLocalNewARIntroView() == false{
            arIntroView = LocalNewARIntroView()
            navigationController?.view.addSubview(arIntroView!)
            arIntroView?.snp.makeConstraints({ (make) in
                make.edges.equalTo(navigationController!.view)
            })
            setHasLoadLocalNewARIntroView()
        }
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        chooseARView = LocalNewARChooseView()
        view.addSubview(chooseARView!)
        chooseARView?.snp.makeConstraints({ (make) in
            make.centerX.left.right.equalTo(view)
            make.bottom.equalTo(view).offset(50/baseWidth)
            make.height.equalTo(150/baseWidth)
        })
        chooseARView?.viewModel = viewModel
        
        shareView = LocalNewARRecordView()
        view.addSubview(shareView!)
        shareView?.snp.makeConstraints({ (make) in
            make.left.right.equalTo(view)
            make.height.equalTo(100/baseWidth)
            make.bottom.equalTo(view).offset(100/baseWidth)
        })
        
        snapShotShareView = LocalARSnapShareView()
        view.addSubview(snapShotShareView!)
        snapShotShareView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(70/baseWidth)
            make.right.equalTo(view.snp.left).offset(0)
            make.size.equalTo(CGSize(width: 134/baseWidth, height: 40/baseWidth))
        })
    }
    
    //MARK: 开始动画
//    fileprivate func startAnimation(){
//        if scanLine == nil {
//            //添加一个扫描的动画
//            scanLine = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleToFill, andImageName: "scanLine")
//            view.addSubview(scanLine!)
//            scanLine?.frame = CGRect(x: 0, y: 125/baseHeight, width: screenWidth, height: 3)
//            UIView.animate(withDuration: 2.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseInOut, UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse], animations: { [weak self] _ in
//                self?.scanLine?.frame.origin.y = 360/baseHeight
//                }, completion: nil)
//        }
//        showOrHideShareView(shouldShow: true)
//    }
    
//    //MARK: 停止动画
//    fileprivate func stopAnimation(){
//        scanLine?.layer.removeAllAnimations()
//        scanLine?.removeFromSuperview()
//        scanLine = nil
//    }
    
    //MARK: 判断AR密码
    fileprivate func checkARCode(code: String){
        if code.characters.count == 6{
            qrViewModel.requestARImageInfo(code: code)
        }else{
            viewModel.requestARAlbumData(code: code)
        }
    }
    
    //MARK: 展示或者隐藏分享视图
    fileprivate func showOrHideShareView(shouldShow: Bool){
        if shouldShow == false{
            chooseARView?.snp.updateConstraints({ (make) in
                make.bottom.equalTo(view).offset(50/baseWidth)
            })
            shareView?.snp.updateConstraints({ (make) in
                make.bottom.equalTo(view).offset(100/baseWidth)
            })
        }else{
            chooseARView?.snp.updateConstraints({ (make) in
                make.bottom.equalTo(view).offset(250/baseWidth)
            })
            shareView?.snp.updateConstraints({ (make) in
                make.bottom.equalTo(view).offset(0)
            })
        }
        autoLayoutAnimation(view: view)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击了分享视图上的返回按钮
        shareView?.touchBackButtonClosure = {[weak self] _ in
            self?.showOrHideShareView(shouldShow: false)
        }
        
        //MARK: 点击了录屏按钮的事件
        shareView?.touchRecordButtonClosure = {[weak self] _ in
            if self?.recordViewModel.isRecording() == true{
                self?.setChooseRecordActionAlert()
            }else{
                self?.recordViewModel.startRecord()
            }
        }
        
        //MARK: 点击截屏按钮的事件
        shareView?.touchSnapShotButtonClosure = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("同时按住电源键和主屏幕键来截屏分享", comment: ""))
        }
        
        //MARK: 需要停止录屏
        shareView?.shouldStopRecord = {[weak self] _ in
            self?.recordViewModel.stopRecord(shouldDiscard: false)
        }
        
        //MARK: 截图分享视图五秒钟计时结束
        snapShotShareView?.stopTimerClosure = {[weak self] _ in
            self?.showSnapShotShareView(shouldShow: false)
        }
        
        //MARK: 点击了分享视图，进入截图分享控制器
        snapShotShareView?.tapShareViewClosure = {[weak self] _ in
            guard let image: UIImage = self?.recordViewModel.snapShotImage else { return }
            let shareVC: SnapShotShareController = SnapShotShareController()
            shareVC.umengViewName = "截图分享"
            shareVC.snapShotImage = image
            _ = self?.navigationController?.pushViewController(shareVC, animated: true)
        }
        
        //MARK: 选中cell
        chooseARView?.cellIsSelected = {[weak self] (indexPath: IndexPath) in
            self?.viewModel.requestSingleARImage(indexPath: indexPath)
        }
        
        //MARK: 输入了AR密码
        chooseARView?.getARCode = {[weak self] (code: String) in
            self?.checkARCode(code: code)
        }
        
        //MARK: 获取AR时光相册数据成功
        viewModel.requestARAlbumSuccess = {[weak self] (jsonString: String?) in
//            self?.startAnimation()
            if jsonString != nil{
                print(jsonString!)
//                UnitySendMessage("EasyAR_ImageTracker_Local", "GetJsonData", jsonString!)
                self?.enterLocalARVC(jsonString: jsonString!)
            }
        }
        
        //MARK: 获取用户AR是识别图
        viewModel.requestUserARImageSuccess = {[weak self] _ in
            self?.chooseARView?.collectionViewShouldReload()
        }
        
        //MARK: 获取用户单张AR是识别图成功
        viewModel.getSingleImageInfoSuccess = {[weak self] (indexPath: IndexPath) in
//            self?.startAnimation()
            if let jsonString: String = self?.viewModel.generateSingleImageJSON(imagePath: (self?.viewModel.userARImageArray[indexPath.item].arImagePath)!, andVideoURLString: (self?.viewModel.userARImageArray[indexPath.item].videoURL?.absoluteString)!){
                print(jsonString)
//                UnitySendMessage("EasyAR_ImageTracker_Local", "GetJsonData", jsonString)
                self?.enterLocalARVC(jsonString: jsonString)
            }
        }
        
        //MARK: 没有更多数据了
        viewModel.noMoreData = {[weak self] _ in
            self?.chooseARView?.setCollectionViewNoMoreData()
        }
        
        //MARK: 失败
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 获取单张AR识别图成功
        qrViewModel.requestARImageSuccess = {[weak self] (imagePath: String, videoURLString: String) in
//            self?.startAnimation()
            if let jsonString: String = self?.viewModel.generateSingleImageJSON(imagePath: imagePath, andVideoURLString: videoURLString){
                print(jsonString)
//                UnitySendMessage("EasyAR_ImageTracker_Local", "GetJsonData", jsonString)
                self?.enterLocalARVC(jsonString: jsonString)
            }
        }
        
        //MARK: 获取单张AR是识别图失败
        qrViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 左划刷新
        chooseARView?.headerRefreshClosure = {[weak self] _ in
            self?.viewModel.arImagesPageNum = 1
            self?.viewModel.requestUserARImage()
        }
        
        //MARK: 右划刷新
        chooseARView?.footerRefreshClosure = {[weak self] _ in
            self?.viewModel.requestUserARImage()
        }
        
        //MARK: 已经开始录屏的闭包
        recordViewModel.startRecordClosure = {[weak self] _ in
            self?.shareView?.beginRecord()
        }
        
        //MARK: 停止录屏的闭包
        recordViewModel.stopRecordAlert = {[weak self] _ in
            self?.setChooseRecordActionAlert()//让用户选择是否停止录制
        }
        
        //MARK: 警告弹窗
        recordViewModel.showWarmAlert = {[weak self] _ in
            self?.setWarnAlert()
        }
        
        //MARK: 已经停止录屏
        recordViewModel.recordStoped = {[weak self] _ in
            self?.shareView?.stopRecord()
        }
        
        //MARK: 需要展示录屏的结果
        recordViewModel.shouldPresentRecordResult = {[weak self] (vc: RPPreviewViewController) in
            vc.previewControllerDelegate = self
            self?.present(vc, animated: true, completion: nil)
        }
        
        //MARK: 点击导航栏右按钮进入云识别图片目录
        moreBarItem?.touchItemClosure = {[weak self] _ in
//            let arVC: EasyARVC = EasyARVC()
//            arVC.umengViewName = "AR"
//            self?.present(arVC, animated: true, completion: nil)
            let vc: CloudChooseController = CloudChooseController()
            let nav: UINavigationController = UINavigationController(rootViewController: vc)
            self?.present(nav, animated: true, completion: nil)
            
            //MARK: 获得了线索图片的URL
            vc.getClueImageURL = {(imageURL: URL?) in
                delay(seconds: 1, completion: {
                    self?.setClueImage(imageURL: imageURL)
                }, completionBackground: nil)
            }
        }
    }
    
    //MARK: 展示截屏分享视图
    fileprivate func showSnapShotShareView(shouldShow: Bool = true){
        if shouldShow == true{
            snapShotShareView?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(view.snp.left).offset(67/baseWidth)
            })
            snapShotShareView?.startTimer()
        }else{
            recordViewModel.snapShotImage = nil
            snapShotShareView?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.right.equalTo(view.snp.left).offset(0)
            })
        }
        autoLayoutAnimation(view: view)
    }
    
    //MARK: 接受通知，弹出分享视图
    func showShareSnapShotView(notification: Notification){
        showSVProgress(title: nil)
        delay(seconds: 1.0, completion: {[weak self] _ in
            self?.dismissSVProgress()
            self?.recordViewModel.getSnapShot()
            self?.showSnapShotShareView(shouldShow: true)
        })
    }
    
    //MARK: 监测键盘高度
    func keyboardWillChangeFrame(notification: Notification){
        guard let userInfo: [String: Any] = notification.userInfo as? [String: Any] else {return}
        
        if let duration: Double = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double{
            if let keyboardFrame: CGRect = userInfo[UIKeyboardFrameEndUserInfoKey] as? CGRect{
                chooseARView?.snp.updateConstraints({ (make) in
                    make.bottom.equalTo(view).offset(50/baseWidth - keyboardFrame.height)
                })
                autoLayoutAnimation(view: view, andAnimationTime: duration, withDelay: 0, completionClosure: nil)
            }
        }
    }
    
    //MARK: 检测键盘收回
    func keyboardDismiss(){
        showOrHideShareView(shouldShow: false)
    }
    
    //MARK: 让用户选择是否停止录制
    fileprivate func setChooseRecordActionAlert(){
        let av: LocalRecordAlertViewController = LocalRecordAlertViewController.confirmStopAlert()
        //MARK: 完成录制
        av.stopRecord = {[weak self] _ in
            self?.recordViewModel.stopRecord(shouldDiscard: false)
        }
        //MARK: 取消录制
        av.cancel = {[weak self] _ in
            self?.recordViewModel.stopRecord(shouldDiscard: true)
        }
        present(av, animated: true, completion: nil)
    }
    
    //MARK: 提醒用户ReplayKit不可用
    fileprivate func setWarnAlert(){
        let ac: LocalRecordAlertViewController = LocalRecordAlertViewController.recordWarnAlert()
        present(ac, animated: true, completion: nil)
    }
    
    //MARK: 设置线索图片的扫描动画
    fileprivate func setClueImage(imageURL: URL?){
        
        clueImage = BaseImageView.normalImageView()
        clueImage?.alpha = 0.74
        clueImage?.sd_setImage(with: imageURL)
        navigationController?.view.addSubview(clueImage!)
        clueImage?.snp.makeConstraints { (make) in
            make.center.equalTo(navigationController!.view)
            make.size.equalTo(CGSize(width: 302/baseWidth, height: 274/baseWidth))
        }
        
        //添加一个扫描的动画
        scanLine = UIImageView(image: UIImage(named: "scanLine"))
        view.addSubview(scanLine!)
        scanLine?.frame = CGRect(x: 0, y: 150/baseHeight, width: screenWidth, height: 3)
        UIView.animate(withDuration: 2.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseInOut, UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse], animations: { [weak self] _ in
            self?.scanLine?.frame.origin.y = 385/baseHeight
            }, completion: nil)
    }
    
    //MARK: 线索图片消失
    fileprivate func dismissClueImage(){
        scanLine?.layer.removeAllAnimations()//移除扫描动画
        scanLine?.removeFromSuperview()
        UIView.animate(withDuration: 0.3, animations: {[weak self] _ in
            self?.clueImage?.alpha = 0
            }, completion: { [weak self] (_) in
                self?.clueImage?.removeFromSuperview()
        })
    }
    
    //MARK: 接受通知，获取TargetID
    func getTargetID(notification: Notification){
        if let userInfo: [String: Any] = notification.userInfo as? [String: Any]{
            if let targetId: String = userInfo["targetId"] as? String{
                delay(seconds: 1, completion: { [weak self] _ in
                    self?.dismissClueImage()
                }, completionBackground: nil)
                showOrHideShareView(shouldShow: true)
                if currentTargetId != targetId {
                    currentTargetId = targetId
                    DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {[weak self] _ in
                        self?.cloudViewModel.countCloudAPI(targetId: targetId)
                    }
                }
            }
        }
    }
    
    //MARK: 进入本地识别界面
    fileprivate func enterLocalARVC(jsonString: String){
        let vc: CloudController = CloudController()
        vc.jsonString = jsonString
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 点击左按钮退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        UnitySendMessage("SceneManager", "HideARCard", "xxx")
        delay(seconds: 0.1, completion: { [weak self] in
            app.window?.rootViewController = app.containerVC
            self?.dismissSelf()
        }, completionBackground: nil)
    }
}

extension LocalNewARController: RPPreviewViewControllerDelegate{
    
    func previewControllerDidFinish(_ previewController: RPPreviewViewController) {
        previewController.dismiss(animated: true, completion: nil)
    }
    
    //MARK: 保存了视频。开始寻找视频
    func previewController(_ previewController: RPPreviewViewController, didFinishWithActivityTypes activityTypes: Set<String>) {
        if activityTypes.contains("com.apple.UIKit.activity.SaveToCameraRoll") == true{
            recordViewModel.requestVideo(success: { [weak self] (videoPath: String) in
                let shareVC: VideoShareController = VideoShareController()
                shareVC.umengViewName = "视频分享"
                shareVC.videoURLPath = videoPath
                _ = self?.navigationController?.pushViewController(shareVC, animated: true)
            })
        }
    }
}

