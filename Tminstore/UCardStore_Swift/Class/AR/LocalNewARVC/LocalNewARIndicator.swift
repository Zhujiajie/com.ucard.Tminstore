//
//  LocalNewARIndicator.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class LocalNewARIndicator: UIImageView {
    
    /// 是否正在执行动画
    var indicatorIsAnimating: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.image = UIImage(named: "arIndicator")
        self.alpha = 0
    }
    
    //MARK: 开始动画
    /// 开始动画
    func startAnimation(){
        indicatorIsAnimating = true
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = -Double.pi * 2
        rotateAnimation.duration = 3.0
        rotateAnimation.repeatCount = MAXFLOAT
        
        layer.add(rotateAnimation, forKey: nil)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
            self?.alpha = 1
            }, completion: nil)
    }
    
    //MARK: 停止动画
    /// 停止动画
    func stopAnimation(){
        indicatorIsAnimating = false
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
            self?.alpha = 0
            }, completion: { [weak self] (_) in
                self?.layer.removeAllAnimations()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
