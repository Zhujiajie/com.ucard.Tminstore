//
//  LocalNewARCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

private let reuseIdentifier = "Cell"

class LocalNewARCollectionViewController: UICollectionViewController {
    
    /// 选中了某个cell
    var cellIsSelected: ((_ indexPath: IndexPath)->())?
    
    /// header刷新
    var headerRefreshClosure: (()->())?
    /// footer刷新
    var footerRefreshClosure: (()->())?
    
    /// 数据源
    weak var viewModel: LocalNewARViewModel?
    
    init() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 7/baseWidth
        layout.minimumInteritemSpacing = 20/baseWidth
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: 85/baseWidth, height: 85/baseWidth)
        
        super.init(collectionViewLayout: layout)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        self.collectionView?.backgroundColor = UIColor.clear
        self.collectionView!.register(LocalNewARCollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView?.indicatorStyle = UIScrollViewIndicatorStyle.white
        
        self.collectionView?.xzm_addNormalHeader(callback: {[weak self] _ in
            self?.headerRefreshClosure?()
        })
        
        self.collectionView?.xzm_addNormalFooter(callback: {[weak self] _ in
            self?.footerRefreshClosure?()
        })
        
        self.collectionView?.xzm_header.isUpdatedTimeHidden = true
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if viewModel == nil {return 0}
        return viewModel!.userARImageArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: LocalNewARCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! LocalNewARCollectionViewCell
        
        cell.isCellSelected = viewModel!.userARImageArray[indexPath.item].isSelected
        cell.model = viewModel!.userARImageArray[indexPath.item]
        
        return cell
    }

    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for cell in collectionView.visibleCells{
            if let cell: LocalNewARCollectionViewCell = cell as? LocalNewARCollectionViewCell{
                cell.isCellSelected = false
            }
        }
        
        _ = viewModel!.userARImageArray.map({ (model: PostcardModel) in
            model.isSelected = false
        })
        if let cell: LocalNewARCollectionViewCell = collectionView.cellForItem(at: indexPath) as? LocalNewARCollectionViewCell{
            cell.isCellSelected = true
            viewModel!.userARImageArray[indexPath.item].isSelected = true
        }
        cellIsSelected?(indexPath)
    }
}

class LocalNewARCollectionViewCell: UICollectionViewCell {
    
    //MARK: 设置cell是否已选中
    var isCellSelected: Bool = false{
        didSet{
            greenBackView?.isHidden = !isSelected
        }
    }
    
    /// 数据源
    var model: PostcardModel?{
        didSet{
            if let url: URL = model?.arThumbnailURL{
                //先判断是否已缓存图片
                SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                    
                    if result == true{
                        self?.imageView?.sd_setImage(with: url)
                    }else{
                        if let placeHolderURL: URL = self?.model?.frontPlaceImageURL{
                            self?.imageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                                DispatchQueue.main.async {
                                    if image != nil {
                                        self?.imageView?.sd_setImage(with: url, placeholderImage: image!)
                                    }else{
                                        self?.imageView?.sd_setImage(with: url)
                                    }
                                }
                            })
                        }else{
                            self?.imageView?.sd_setImage(with: url)
                        }
                    }
                })
            }
        }
    }
    
    /// 绿色背景，用户展示被选中的状态
    fileprivate var greenBackView: BaseView?
    //图片
    fileprivate var imageView: BaseImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        greenBackView = BaseView.colorLine(colorString: "97FBFF")
        contentView.addSubview(greenBackView!)
        greenBackView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(contentView)
        })
        greenBackView?.isHidden = true
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(contentView).inset(UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2))
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
