//
//  LocalNewARViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

class LocalNewARViewModel: BaseViewModel {
    
    fileprivate let dataModel: LocalNewARDataModel = LocalNewARDataModel()
    /// 存档图片URL的临时数组
    fileprivate var arAlbumImageURLs: [URL] = [URL]()
    
    ///存放AR时光相册数据的数组
    var arAblumData: [PostcardModel] = [PostcardModel]()
    /// AR识别图在本地的路径
    var arAlbumPath: [String] = [String]()
    /// 用户的AR识别图
    var userARImageArray: [PostcardModel] = [PostcardModel]()
    
    /// 获取AR相册数据成功
    var requestARAlbumSuccess: ((_ jsonString: String?)->())?
    
    /// 获取单张图片成功
    var getSingleImageInfoSuccess: ((_ indexPath: IndexPath)->())?
    
    /// 获取用户AR是识别图成功
    var requestUserARImageSuccess: (()->())?
    
    /// 没有更多数据了
    var noMoreData: (()->())?
    
    /// 获取用户AR图片的页数
    var arImagesPageNum: Int = 1{
        didSet{
            if arImagesPageNum == 1{
                isNoMoreData = false
            }
        }
    }
    
    /// 是否已经获取过用户数据
    var hasRequestUserData: Bool = false
    
    /// 是否已经没有更多数据了
    var isNoMoreData: Bool = false
    
    /// 已经向后台发送过统计数据的originalId
    fileprivate var computedOriginalIds: [String] = [String]()
    
    override init() {
        super.init()
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
            self?.dismissSVProgress()
        }
        
//        //MARK: 请求完成的闭包
//        dataModel.completionClosure = {[weak self] _ in
//            self?.dismissSVProgress()
//        }
    }
    
    //MARK: 请求时光相册的数据
    /// 请求时光相册的数据
    ///
    /// - Parameter code: 相册密码
    func requestARAlbumData(code: String){
        showSVProgress(title: nil)
        dataModel.requsetARAlbum(code: code, successClosure: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let timeAlbumList: [[String: Any]] = data["timeAlbumList"] as? [[String: Any]], timeAlbumList.isEmpty == false{
                        self?.arAblumData.removeAll()//清空数据
                        self?.arAlbumImageURLs.removeAll()
                        self?.arAlbumPath.removeAll()
                        for dic: [String: Any] in timeAlbumList{
                            let model: PostcardModel = PostcardModel.initFromARAlbum(dic: dic)
                            if model.arThumbnailURL != nil && model.videoURL != nil{
                                self?.arAblumData.append(model)
                                self?.arAlbumImageURLs.append(model.arThumbnailURL!)
                            }
                        }
                        self?.downARImage()
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
            self?.dismissSVProgress()
        })
    }
    
    //MARK: 下载AR识别图
    fileprivate func downARImage(){
        
        let prefetcher: SDWebImagePrefetcher = SDWebImagePrefetcher.shared()
        prefetcher.options = SDWebImageOptions.highPriority
        prefetcher.prefetchURLs(arAlbumImageURLs, progress: {[weak self] (finished: UInt, total: UInt) in
            self?.showProcess(current: Float(finished), andTotal: Float(total), andTitle: NSLocalizedString("正在下载识别资源", comment: ""))
        }, completed: { [weak self] (count: UInt, skipCount: UInt) in
            
            if skipCount == 0{
                if let imageCache: SDImageCache = SDWebImageManager.shared().imageCache{
                    for url in (self?.arAlbumImageURLs)!{
                        if let imagePath: String = imageCache.defaultCachePath(forKey: url.absoluteString){
                            self?.arAlbumPath.append(imagePath)
                            continue
                        }
                        break
                    }
                    DispatchQueue.main.async {
                        self?.dismissSVProgress()
                        self?.requestARAlbumSuccess?(self?.generateDictionaryFromARAlbum())//下载图片完成
                    }
                    return
                }
            }
            //下载图片出错
            let errorDic: [String: Any] = [
                "message": NSLocalizedString("下载AR图片失败，请重试", comment: "")
            ]
            DispatchQueue.main.async {
                self?.dismissSVProgress()
                self?.errorClosure?(nil, errorDic)
            }
        })
    }
    
    //MARK: 请求用户AR识别图的接口
    /// 请求用户AR识别图的接口
    func requestUserARImage() {
        
        if isNoMoreData == true {
            noMoreData?()
            return
        }
        
        dataModel.requestUserARImage(pageNum: arImagesPageNum) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let imageList: [[String: Any]] = data["imageList"] as? [[String: Any]]{
                        if imageList.isEmpty {
                            self?.isNoMoreData = true
                            self?.noMoreData?()
                        }else{
                            if self?.arImagesPageNum == 1{
                                self?.userARImageArray.removeAll()
                            }
                            for dic: [String: Any] in imageList{
                                let model: PostcardModel = PostcardModel.initFromSpaceTimeCircle(dic: dic)
                                self?.userARImageArray.append(model)
                            }
                            self?.requestUserARImageSuccess?()
                        }
                        self?.arImagesPageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 下载单张AR识别图
    /// 下载单张AR识别图
    ///
    /// - Parameter indexPath: IndexPath
    func requestSingleARImage(indexPath: IndexPath){
        showSVProgress(title: NSLocalizedString("正在下载识别资源", comment: ""))
        dataModel.requestSingleImageVideo(originalId: userARImageArray[indexPath.item].originalId) { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let videoList: [String] = data["videoList"] as? [String]{
                        if let videoUrlString: String = videoList.first{
                            self?.userARImageArray[indexPath.item].videoURL = URL(string: videoUrlString)
                            if self?.userARImageArray[indexPath.item].arThumbnailURL != nil {
                                self?.downLoadSingleImage(indexPath: indexPath)
                            }
                        }else{
                            //下载图片出错
                            let errorDic: [String: Any] = [
                                "message": NSLocalizedString("没有对应的AR视频", comment: "")
                            ]
                            self?.dismissSVProgress()
                            self?.errorClosure?(nil, errorDic)
                        }
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 下载并缓存单张图片
    fileprivate func downLoadSingleImage(indexPath: IndexPath){
        let imageUrl: URL = userARImageArray[indexPath.item].arThumbnailURL!
        let prefetcher: SDWebImagePrefetcher = SDWebImagePrefetcher.shared()
        prefetcher.options = SDWebImageOptions.highPriority
        prefetcher.prefetchURLs([imageUrl], progress: nil, completed: { [weak self] (count: UInt, skipCount: UInt) in
                if skipCount == 0{
                    if let imageCache: SDImageCache = SDWebImageManager.shared().imageCache, let imagePath: String = imageCache.defaultCachePath(forKey: imageUrl.absoluteString){
                        DispatchQueue.main.async {
                            self?.dismissSVProgress()
                            self?.userARImageArray[indexPath.item].arImagePath = imagePath
                            self?.getSingleImageInfoSuccess?(indexPath)//下载图片完成
                        }
                        return
                    }
                }
                //下载图片出错
                let errorDic: [String: Any] = [
                    "message": NSLocalizedString("下载AR图片失败，请重试", comment: "")
                ]
                DispatchQueue.main.async {
                    self?.dismissSVProgress()
                    self?.errorClosure?(nil, errorDic)
                }
        })
    }
    
    //MARK: 从AR时光相册生成字典字符串
    fileprivate func generateDictionaryFromARAlbum()->String?{
        var imageInfoDic: [String: Any] = [String: Any]()
        var imageInfoArray: [[String: Any]] = [[String: Any]]()
        for i in 0..<arAblumData.count{
            //取得图片的name
            let stringArray: [String] = arAlbumPath[i].components(separatedBy: "/")
            let imageName: String = stringArray.last!.replacingOccurrences(of: ".png!unity", with: "")
            let dic: [String: Any] = [
                "image": arAlbumPath[i],
                "name": imageName,
                "videoURL": arAblumData[i].videoURL!.absoluteString,
                "originalId": arAblumData[i].originalId
            ]
            imageInfoArray.append(dic)
        }
        imageInfoDic = [
            "images": imageInfoArray
        ]
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: imageInfoDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let string: String = String.init(data: jsonData, encoding: String.Encoding.utf8)!
            return string.replacingOccurrences(of: "\\", with: "")
        }catch{
            return nil
        }
    }
    
    //MARK: 统计AR时光相册的扫描次数
    /// 统计AR时光相册的扫描次数
    ///
    /// - Parameter originalId: 原创ID
    func computeARAlbumScanTimes(originalId: String){
        if computedOriginalIds.contains(originalId) {return}
        computedOriginalIds.append(originalId)
        dataModel.computeARAlbumScanTimes(originalId: originalId)
    }
    
    //MARK: 生成单张图片的JSON字符串
    /// 生成单张图片的JSON字符串
    ///
    /// - Parameters:
    ///   - imagePath: 图片路径
    ///   - videoURLString: 视频URL
    /// - Returns: String
    func generateSingleImageJSON(imagePath: String, andVideoURLString videoURLString: String)->String?{
        var imageInfoDic: [String: Any] = [String: Any]()
        var imageInfoArray: [[String: Any]] = [[String: Any]]()
        //取得图片的Name
        let stringArray: [String] = imagePath.components(separatedBy: "/")
        let imageName: String = stringArray.last!.replacingOccurrences(of: ".png!unity", with: "")
        let dic: [String: Any] = [
            "image": imagePath,
            "name": imageName,
            "videoURL": videoURLString,
            "originalId": ""
        ]
        imageInfoArray.append(dic)
        imageInfoDic = [
            "images": imageInfoArray
        ]
        
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: imageInfoDic, options: JSONSerialization.WritingOptions.prettyPrinted)
            let string: String = String.init(data: jsonData, encoding: String.Encoding.utf8)!
            return string.replacingOccurrences(of: "\\", with: "")
        }catch{
            return nil
        }
    }
}
