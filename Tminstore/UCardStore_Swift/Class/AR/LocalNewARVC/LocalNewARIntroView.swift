//
//  LocalNewARIntroView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class LocalNewARIntroView: UIView {
    
    /// 图片
    fileprivate var imageView: BaseImageView?
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.alpha = 0
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make) in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: -10, left: -10/baseWidth, bottom: -10/baseWidth, right: -10/baseWidth))
        })
        imageView?.sd_setImage(with: appAPIHelp.newARIntroImageURL)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismiss))
        imageView?.addGestureRecognizer(tap)
        imageView?.isUserInteractionEnabled = true
        tap.isEnabled = false
        
        delay(seconds: 2, completion: {_ in
            tap.isEnabled = true
        }, completionBackground: nil)
        
        //渐变和缩放动画
//        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
            self?.alpha = 1
//            self?.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    //MARK: 退出
    func dismiss(){
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
            self?.alpha = 0
//            self?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            }, completion: {[weak self] (_) in
                self?.removeFromSuperview()
                self = nil
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
