//
//  LocalNewARChooseView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class LocalNewARChooseView: UIVisualEffectView {
    
    weak var viewModel: LocalNewARViewModel?{
        didSet{
            choosePictureCollectionView?.viewModel = viewModel
        }
    }
    
    /// header刷新
    var headerRefreshClosure: (()->())?
    /// footer刷新
    var footerRefreshClosure: (()->())?
    
    /// 选中了某个cell
    var cellIsSelected: ((_ indexPath: IndexPath)->())?
    
    /// 输入了AR密码
    var getARCode: ((_ code: String)->())?
    
    /// 中间白色竖线
    fileprivate var whiteLine: BaseView?
    
    /// 输入密码观看的按钮
    fileprivate var passwordButton: BaseButton?
    /// 输入密码观看的label
    fileprivate var passwordLabel: BaseLabel?
    /// 输入密码观看的icon
    fileprivate var passwordIcon: BaseImageView?
    /// 选择图片的按钮
    fileprivate var chooseButton: BaseButton?
    /// 选择图片的label
    fileprivate var chooseLabel: BaseLabel?
    /// 选择图片的icon
    fileprivate var chooseIcon: BaseImageView?
    /// 在密码界面返回的按钮
    fileprivate var passwordBackButton: BaseButton?
    /// 输入密码的textField
    fileprivate var passwordTextField: BaseTextField?
    /// 确认的按钮
    fileprivate var confirmButton: BaseButton?
    
    /// 选择图片返回的按钮
    fileprivate var chooseBackButton: BaseButton?
    /// 选择图片的collectionView
    fileprivate var choosePictureCollectionView: LocalNewARCollectionViewController?
    
    override init(effect: UIVisualEffect?) {
        super.init(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        
        whiteLine = BaseView.colorLine(colorString: "FFFFFF")
        contentView.addSubview(whiteLine!)
        whiteLine?.snp.makeConstraints { (make) in
            make.top.equalTo(contentView).offset(13/baseWidth)
            make.bottom.equalTo(contentView).offset(-63/baseWidth)
            make.width.equalTo(1)
            make.centerX.equalTo(contentView).offset(0)
        }
        
        //输入密码的UI
        passwordButton = BaseButton.normalIconButton()
        contentView.addSubview(passwordButton!)
        passwordButton?.snp.makeConstraints({ (make) in
            make.top.bottom.equalTo(contentView)
            make.right.equalTo(whiteLine!.snp.left)
            make.width.equalTo(screenWidth/2)
        })
        //MARK: 点击输入密码按钮
        passwordButton?.touchButtonClosure = {[weak self] _ in
            self?.changeLayout(offset: screenWidth)
        }
        
        passwordLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("输入密码观看", comment: ""))
        contentView.insertSubview(passwordLabel!, belowSubview: passwordButton!)
        passwordLabel?.snp.makeConstraints({ (make) in
            make.top.equalTo(contentView).offset(16/baseWidth)
            make.right.equalTo(whiteLine!.snp.left)
            make.width.equalTo(screenWidth/2)
        })
        
        passwordIcon = BaseImageView.normalImageView(andImageName: "arPassword")
        contentView.insertSubview(passwordIcon!, belowSubview: passwordButton!)
        passwordIcon?.snp.makeConstraints({ (make) in
            make.top.equalTo(passwordLabel!.snp.bottom).offset(20/baseWidth)
            make.right.equalTo(whiteLine!.snp.left).offset(-82/baseWidth)
            make.size.equalTo(CGSize(width: 30/baseWidth, height: 30/baseWidth))
        })
        
        //选择图片的UI
        chooseButton = BaseButton.normalIconButton()
        contentView.addSubview(chooseButton!)
        chooseButton?.snp.makeConstraints({ (make) in
            make.top.bottom.equalTo(contentView)
            make.left.equalTo(whiteLine!.snp.right)
            make.width.equalTo(passwordButton!)
        })
        //MARK: 点击选择图片按钮的事件
        chooseButton?.touchButtonClosure = {[weak self] _ in
            self?.changeLayout(offset: -screenWidth)
        }
        
        chooseLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "FFFFFF", andTextAlignment: NSTextAlignment.center, andNumberOfLines: 1, andText: NSLocalizedString("选择图片观看", comment: ""))
        contentView.insertSubview(chooseLabel!, belowSubview: chooseButton!)
        chooseLabel?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(passwordLabel!)
            make.left.equalTo(whiteLine!.snp.right)
            make.width.equalTo(passwordLabel!)
        })
        
        chooseIcon = BaseImageView.normalImageView(andImageName: "arImage")
        contentView.insertSubview(chooseIcon!, belowSubview: chooseButton!)
        chooseIcon?.snp.makeConstraints({ (make) in
            make.topMargin.equalTo(passwordIcon!)
            make.left.equalTo(whiteLine!.snp.right).offset(82/baseWidth)
            make.size.equalTo(passwordIcon!)
        })
        
        /// 输入密码的UI
        confirmButton = BaseButton.normalIconButton(iconName: "arCodeConfirm")
        confirmButton?.backgroundColor = UIColor(hexString: "5CE5E5")
        contentView.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(whiteLine!)
            make.right.equalTo(whiteLine!.snp.left).offset(-(22/baseWidth + screenWidth/2))
            make.size.equalTo(CGSize(width: 61/baseWidth, height: 58/baseWidth))
        })
        //MARK: 点击确认密码的按钮
        confirmButton?.touchButtonClosure = {[weak self] _ in
            self?.passwordTextField?.endEditing(true)
        }
        
        passwordTextField = BaseTextField.numberTextField(font: UIFont.systemFont(ofSize: 14), andTextColorHexString: "A4A4A4")
        passwordTextField?.borderStyle = UITextBorderStyle.roundedRect
        contentView.addSubview(passwordTextField!)
        passwordTextField?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(whiteLine!)
            make.right.equalTo(confirmButton!.snp.left).offset(-16/baseWidth)
            make.size.equalTo(CGSize(width: 212/baseWidth, height: 58/baseWidth))
        })
        //MARK: textField停止编辑
        passwordTextField?.didEndEditing = {[weak self] (text: String) in
            self?.getARCode?(text)
            self?.passwordTextField?.text = nil
        }
        
        passwordBackButton = BaseButton.normalIconButton(iconName: "arBack")
        passwordBackButton?.backgroundColor = UIColor(hexString: "3C3C3C")
        contentView.addSubview(passwordBackButton!)
        passwordBackButton?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(whiteLine!)
            make.right.equalTo(passwordTextField!.snp.left).offset(-10/baseWidth)
            make.size.equalTo(CGSize(width: 54/baseWidth, height: 100/baseWidth))
        })
        
        //MARK: 点击返回按钮
        passwordBackButton?.touchButtonClosure = {[weak self] _ in
            self?.passwordTextField?.text = nil
            self?.passwordTextField?.endEditing(true)
            self?.changeLayout(offset: 0)
        }
        
        //MARK: 选择图片的UI
        chooseBackButton = BaseButton.normalIconButton(iconName: "arBack")
        chooseBackButton?.backgroundColor = UIColor(hexString: "3C3C3C")
        contentView.addSubview(chooseBackButton!)
        chooseBackButton?.snp.makeConstraints({ (make) in
            make.centerY.equalTo(whiteLine!)
            make.left.equalTo(whiteLine!.snp.right).offset(screenWidth/2)
            make.size.equalTo(passwordBackButton!)
        })
        //MARK: 点击选择图片返回按钮的事件
        chooseBackButton?.touchButtonClosure = {[weak self] _ in
            self?.changeLayout(offset: 0)
        }
        
        //MARK: 展示图片的collectionView
        choosePictureCollectionView = LocalNewARCollectionViewController()
        contentView.addSubview(choosePictureCollectionView!.view)
        choosePictureCollectionView?.view.snp.makeConstraints({ (make) in
            make.left.equalTo(chooseBackButton!.snp.right)
            make.top.equalTo(contentView)
            make.size.equalTo(CGSize(width: screenWidth - 54/baseWidth, height: 100/baseWidth))
        })
            
        //MARK: 选中了cell
        choosePictureCollectionView?.cellIsSelected = { [weak self] (indexPath: IndexPath) in
            self?.cellIsSelected?(indexPath)
        }
        
        //MAKR: 左拉刷新
        choosePictureCollectionView?.headerRefreshClosure = {[weak self] _ in
            self?.headerRefreshClosure?()
        }
        //MARK: 右拉刷新
        choosePictureCollectionView?.footerRefreshClosure = {[weak self] _ in
            self?.footerRefreshClosure?()
        }
    }
    
    //MARK: 需要刷新UI
    /// 需要刷新UI
    func collectionViewShouldReload(){
        choosePictureCollectionView?.collectionView?.xzm_header.endRefreshing()
        choosePictureCollectionView?.collectionView?.xzm_footer.endRefreshing()
        choosePictureCollectionView?.collectionView?.reloadData()
    }
    
    //MARK: 设置没有更多数据了
    /// 设置没有更多数据了
    func setCollectionViewNoMoreData(){
        choosePictureCollectionView?.collectionView?.xzm_header.endRefreshing()
        choosePictureCollectionView?.collectionView?.xzm_footer.endRefreshing()
    }
    
    //MARK: 改变布局
    fileprivate func changeLayout(offset: CGFloat){
        whiteLine?.snp.updateConstraints({ (make) in
            make.centerX.equalTo(contentView).offset(offset)
        })
        autoLayoutAnimation(view: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
