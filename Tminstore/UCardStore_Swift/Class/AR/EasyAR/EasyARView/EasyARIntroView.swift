//
//  EasyARIntroView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: -------------提醒用户可以点击进行本地扫描的视图-----------------

import UIKit
import SnapKit

class EasyARIntroView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        image = UIImage(named: "arIntroView")
        contentMode = UIViewContentMode.scaleToFill
        
        let label: UILabel = UILabel()
        label.textAlignment = NSTextAlignment.center
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 0
        
        addSubview(label)
        label.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(self)
        }
        label.text = NSLocalizedString("购买图片后扫描\n明信片背面二维\n码或者输入密码\n观看AR视频", comment: "")
        
        isUserInteractionEnabled = true
        
        //点击消失
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToDismiss(tap:)))
        addGestureRecognizer(tap)
        
        //计时器消失
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(dismissTimer(time:)), userInfo: nil, repeats: false)
    }
    
    //MARK: 计时五秒之后消失
    func dismissTimer(time: Timer){
        dismissAniamtion()
    }
    
    //MARK: 点击消失
    func tapToDismiss(tap: UITapGestureRecognizer){
        dismissAniamtion()
    }
    
    //MARK: 消失的动画
    fileprivate func dismissAniamtion(){
        UIView.animate(withDuration: 0.5, animations: { [weak self] _ in
            self?.alpha = 0
            }, completion: {[weak self] (_) in
                self?.removeFromSuperview()
        })
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
