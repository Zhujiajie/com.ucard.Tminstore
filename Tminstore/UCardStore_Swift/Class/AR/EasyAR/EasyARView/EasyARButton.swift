//
//  EasyARButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/9/23.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class EasyARButton: UIButton {

    /// 点击了切换视频按钮
    var touchVideoButton: ((_ tag: Int)->())?
    
    ///点击了退出按钮
    var touchDismissButton: (()->())?
    
    //MARK: 退出的按钮
    /// 退出的按钮
    ///
    /// - returns: EasyARButton
    class func dismissButton()->EasyARButton{
        let button: EasyARButton = EasyARButton()
        button.setImage(UIImage(named: "arCancel"), for: UIControlState.normal)
        button.addTarget(button, action: #selector(button.touchDismissButtonAction(_:)), for: UIControlEvents.touchUpInside)
        return button
    }
    
    //MARK: 点击button的事件
    func touchVideoButton(_ button: UIButton) {
        touchVideoButton?(button.tag)
    }
    
    //MARK: 点击退出按钮事件
    func touchDismissButtonAction(_ button: UIButton) {
        touchDismissButton?()
    }
}
