//
//  EasyARVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/14.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: --------------AR界面-------------------

import UIKit
import SnapKit

let upyunVideoURL: String = "http://ucardstorevideo.b0.upaiyun.com/video/"

class EasyARVC: BaseViewController {
    
    fileprivate let viewModel: EasyARViewModel = EasyARViewModel()
    
    /// 点击了退出按钮
    var dismissClosure: (()->())?
    
    /// 是否是从支付页面转来的
    var comeFromePayView: Bool = false
    /// 点击了扫描二维码按钮
    var touchQRCodeButtonClosure: (()->())?
    /// 成功获取优惠券的闭包
    var getCouponSuccessClosure: (()->())?
    
    /// AR图层
    var glView: OpenGLView?
    
    fileprivate let arFrame: CGRect = UIScreen.main.bounds
    
    /// 展示二维码图片的imageView
    fileprivate var qrCodeImageView: UIImageView?
    /// 点击进入扫描二维码的按钮
    fileprivate var qrCodeButton: UIButton?
    /// 介绍视图
    fileprivate var arCardIntroView: EasyARIntroView?
    /// 退出的按钮
    fileprivate var dismissButton: EasyARButton?
    
    /// 是否需要隐藏状态栏
    fileprivate var isStatusBarHide = true{
        didSet{
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    // MARK: 是否隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return isStatusBarHide
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.black
        
        //退出的按钮
        dismissButton = EasyARButton.dismissButton()
        view.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(30/baseWidth)
            make.left.equalTo(view).offset(20/baseWidth)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        })
        
        qrCodeImageView = UIImageView(image: UIImage(named: "qrCodeEnter"))
        qrCodeImageView?.contentMode = UIViewContentMode.scaleAspectFit
        view.addSubview(qrCodeImageView!)
        qrCodeImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(dismissButton!)
            make.right.equalTo(view).offset(-30/baseWidth)
            make.size.equalTo(CGSize(width: 25/baseWidth, height: 25/baseWidth))
        })
        
        qrCodeButton = UIButton()
        qrCodeButton?.backgroundColor = UIColor.clear
        view.addSubview(qrCodeButton!)
        qrCodeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view)
            make.right.equalTo(view)
            make.size.equalTo(CGSize(width: 80/baseWidth, height: 80/baseWidth))
        })
        qrCodeButton?.addTarget(self, action: #selector(touchQRCodeButton(button:)), for: UIControlEvents.touchUpInside)
        
        arCardIntroView = EasyARIntroView(frame: CGRect.zero)
        view.addSubview(arCardIntroView!)
        arCardIntroView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(56/baseWidth)
            make.right.equalTo(view).offset(-12/baseWidth)
            make.size.equalTo(CGSize(width: 118, height: 113))
        })
        
        closures()
    }
    
    //MARK: 点击二维码按钮，进入扫描二维码界面
    func touchQRCodeButton(button: UIButton){
        let qrVC: QRViewController = QRViewController()
        qrVC.shouldDisplayCodeInputView = true
        present(qrVC, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isStatusBarHide = true//隐藏状态栏
        startAR()//开始AR
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MobClick.beginLogPageView("AR")//友盟页面统计
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        fireAR()//释放AR
        MobClick.endLogPageView("AR")//友盟页面统计
        isStatusBarHide = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        glView?.resize(arFrame, orientation: UIInterfaceOrientation.portrait)
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        glView?.setOrientation(toInterfaceOrientation)
    }
    
    //MARK: 开始AR扫描
    /// 开始AR扫描
    func startAR(){
        initAR()
    }
    
    //MARK: 初始化AR
    fileprivate func initAR(){
        if glView == nil {
            glView = OpenGLView(frame: view.bounds)
            view.insertSubview(glView!, belowSubview: dismissButton!)
            glView?.setOrientation(UIInterfaceOrientation.portrait)
            //MARK: C++的代理
            glView?.foundTarget = {[weak self] (videoID: String?, videoURLString: String?) in
                DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                    //不能两个参数都为空
                    if videoID == nil && videoURLString == nil{ return }
                    self?.viewModel.checkVideo(videoID: videoID, andVideoURLString: videoURLString)
                }
            }
        }
        glView?.active = true
        glView?.start()
    }
    
    //MARK: 停止AR扫描
    /// 停止AR扫描
    func stopAR(){
        glView?.active = false
        glView?.stop()
    }
    
    //MARK: 释放AR
    /// 释放AR
    func fireAR(){
        stopAR()
        glView?.clear()
        glView?.removeFromSuperview()
        glView = nil
    }
    
    //MARK: 各种闭包回调
    func closures(){
        
        //MARK: 点击退出按钮的事件
        dismissButton?.touchDismissButton = {[weak self] _ in
            self?.dismissSelf(animated: true, completion: {
                self?.dismissClosure?()
            })
        }
        
        //MARK: 加载AR视频
        viewModel.loadARVideo = {[weak self] (videoPath: String) in
            DispatchQueue.main.async {
                self?.glView?.loadVideo(videoPath)
            }
        }
        
        //MARK: 加载在线视频
        viewModel.playOnlineARVideo = {[weak self] (videoURL: String) in
            DispatchQueue.main.async {
                self?.glView?.setVideoURL(videoURL)
            }
        }
        
        //MARK: app需要更新
        viewModel.appNeedUpdate = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("需要更新到最新版才能领取优惠券哦", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("更新", comment: ""), leftClosure: {[weak self] _ in
                if self?.glView != nil && self?.glView?.active == false{
                    self?.glView?.start()
                }
            }, rightClosure: {
                let url: URL = URL(string: "https://itunes.apple.com/us/app/ucardstore-send-share-real/id966372400?mt=8")!
                if UIApplication.shared.canOpenURL(url){
                    UIApplication.shared.openURL(url)
                }
            })
        }
        
        //MARK: app的代理
        app.appWillResignActive = {[weak self] _ in
            self?.glView?.stop()
        }
        app.appDidBecomeActive = {[weak self] _ in
            if self?.glView != nil && self?.glView?.active == false{
                self?.glView?.start()
            }
        }
    }
}
