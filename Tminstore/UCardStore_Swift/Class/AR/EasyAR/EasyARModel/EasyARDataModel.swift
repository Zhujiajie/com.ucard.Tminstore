//
//  EasyARDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/9/23.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Alamofire

class EasyARDataModel: BaseDataModel {

    
    /// 下载视频到缓存文件夹
    ///
    /// - Parameters:
    ///   - videID: 视频ID
    ///   - savePath: 保存路径
    ///   - success: 成功的闭包
    ///   - fail: 失败的闭包
    func downLoadVideo(videoURLString: String, andSavePath savePath: String, downLoadSuccess success: (()->())?, downLoadFail fail: ((_ error: Error)->())?){
        
        let cacheDirectory: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!//获取缓存文件夹
        let videoFolder: String = cacheDirectory + "/videoCache"
        if FileManager.default.fileExists(atPath: videoFolder) == false {
            do {
                try FileManager.default.createDirectory(atPath: videoFolder, withIntermediateDirectories: false, attributes: nil)
            }catch{print(error)}
        }//如果文件夹不存在，则创建一个
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (URL(fileURLWithPath: savePath), [.removePreviousFile, .createIntermediateDirectories])
        }
        Alamofire.download(videoURLString, to: destination).response(completionHandler: { (response: DefaultDownloadResponse) in
            if response.error != nil{
                do{
                    try FileManager.default.removeItem(at: appAPIHelp.fontPath())
                }catch{ print(error) }
            }else{
                success?()
            }
        })
    }
    
    //MARK: 获取一张优惠券
    /// 获取一张优惠券
    ///
    /// - Parameters:
    ///   - couponId: 优惠券id
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func addCoupon(couponId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = ["token": getUserToken(), "couponId": couponId] as [String: Any]
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.addCouponAPI, ParametersDictionary: parameters, urlEncoding: URLEncoding.default, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) {
            completion?()
        }
    }
    
    //MARK: 领取优惠券之前，先检查版本更新
    /// 领取优惠券之前，先检查版本更新
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - update: 需要更新的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func iTunesAppUpdate(success: (()->())?, needToUpdate update: (()->())?, failureClosure failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let systemVersion: String? = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.GetITunesAPI(), ParametersDictionary: nil, urlEncoding: URLEncoding.default, successClosure: { (result: [String: Any]) in
            
            if let code: Int = result["resultCount"] as? Int, code == 1{
                if let results: [[String: Any]] = result["results"] as? [[String: Any]]{
                    if let version: String = results[0]["version"] as? String {
                        //本地版本比App Store里的版本低，才会提示更新
                        if version.compare(systemVersion!, options: NSString.CompareOptions.numeric, range: nil, locale: nil) == ComparisonResult.orderedDescending{
                            update?()//需要更新
                            return
                        }else{
                            success?()
                            return
                        }
                    }
                }
                failure?(nil)
            }else{
                failure?(nil)
            }
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
}
