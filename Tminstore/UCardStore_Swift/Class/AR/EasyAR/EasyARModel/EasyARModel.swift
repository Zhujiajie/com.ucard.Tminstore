//
//  EasyARModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/17.
//  Copyright © 2016年 ucard. All rights reserved.
//

//---------------------EasyAR API相关的操作--------------------

import UIKit
import Alamofire

let arHost: String = "http://oi43.easyar.com:8888"
let appKey: String = "4e056078f43dbfe73a150bc5e50548ac98d035330f10d6ec164be8fcd0c70c36"
let appSecret: String = "0e8e9cc8f67675db077c9740557b4abcfc4ae5913b62a2585dc072d1637574c8189f6f519003dc675de1f5d81def542eb1b8d003fb9432ad61feac23d68c23e0"

class EasyARModel: NSObject {
    
    /// Alamofire的manager, 连接超时为20秒
    let manager: SessionManager = {
        let configuraton = URLSessionConfiguration.default
        configuraton.timeoutIntervalForRequest = 20;
        return Alamofire.SessionManager(configuration: configuraton)
    }()
    
    
    /// 当前的网络请求
    var request: Alamofire.Request?
    
    //MARK: 发送检查相似度的请求
    /**
     发送检查相似度的请求
     
     - parameter image:    UIImage
     - parameter success:  图片检测成功
     - parameter failure:  网络请求失败
     - parameter similar:  有相似的图片在服务器上了
     - parameter lowGrade: 图片的易识别度太低
     - parameter complete: 网络请求完成的闭包
     */
    func easyARSimilar(image: UIImage, successClosure success: (()->())?, failureClourse failure: ((_ error: Error?)->())?, similarClosure similar: (()->())?, lowGradeClosure lowGrade: (()->())?, completeClosure complete: (()->())?){
        
        let tuple: (String?, String) = base64ImageAndIso8061Time(image: image)
        let encodedImageStr: String = tuple.0!
        let iso8601String: String = tuple.1
        
        //拼接签名
        let signatureString: String = "appKey\(appKey)date\(iso8601String)image\(encodedImageStr)\(appSecret)"
        let nssignature: NSString = NSString(string: signatureString)
        let signature: String = nssignature.sha1//获得签名哈希值
        
        let para: [String: Any] = ["image": encodedImageStr, "date": iso8601String, "appKey": appKey, "signature": signature] as [String: Any]//参数
        let urlStr: String = arHost + "/similar/"
        
        request = manager.request(urlStr, method: HTTPMethod.post, parameters: para, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { [weak self] (response: DataResponse<Any>) in
                switch response.result{
                case .success(_):
                    
                    let json: [String: Any] = response.result.value as! [String: Any]
                    
                    //判断请求是否成功
                    if let statusCode: Int = json["statusCode"] as? Int, statusCode == 0{
                        
                        let data: [String: Any] = json["result"] as! [String: Any]
                        
                        //如果返回结果为空，则调用判断易识别度的接口
                        if let datas: [[String: Any]] = data["results"] as? [[String: Any]]{
                            
                            if datas.isEmpty {
                                
                                self?.easyARDetection(image: image, successClosure: {
                                    success?()
                                    complete?()
                                }, failureClourse: { (error: Error?) in
                                        failure?(error)
                                        complete?()
                                    }, lowGradeClosure: {
                                        lowGrade?()
                                        complete?()
                                })
                            }
                            
                            //服务器上已经有相似的图片了
                            else{
                                similar?()
                                complete?()
                            }
                        }
                        else {
                            failure?(nil)
                            complete?()
                        }
                        
                    }else{
                        failure?(nil)
                        complete?()
                    }
                case .failure(_):
                    let error: Error? = response.result.error
                    failure?(error)
                    complete?()
                }
        }
    }
    
    //MARK: 检查图片的易识别度
    /**
     检查图片的易识别度
     
     - parameter image:    UIImage
     - parameter success:  图片的易识别度很高
     - parameter failure:  网络请求失败
     - parameter lowGrade: 图片的易识别度很低
     - parameter complete: 完成的闭包
     */
    func easyARDetection(image: UIImage, successClosure success: (()->())?, failureClourse failure: ((_ error: Error?)->())?, lowGradeClosure lowGrade: (()->())?, completeClosure complete: (()->())? = nil) {
        
        let tuple: (String?, String) = base64ImageAndIso8061Time(image: image)
        let encodedImageStr: String = tuple.0!
        let iso8601String: String = tuple.1
        
        let signatureString: String = "appKey\(appKey)date\(iso8601String)image\(encodedImageStr)\(appSecret)"//拼接签名
        let nssignature: NSString = NSString(string: signatureString)
        let signature: String = nssignature.sha1//获得签名哈希值
        
        let para: [String: Any] = ["image": encodedImageStr, "date": iso8601String, "appKey": appKey, "signature": signature] as [String: Any]//参数
        
        let urlStr: String = arHost + "/grade/detection/"
        
        request = manager.request(urlStr, method: HTTPMethod.post, parameters: para, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { (response: DataResponse<Any>) in
                switch response.result{
                case .success(_):
                    let json: [String: Any] = response.result.value as! [String: Any]
                    if let statusCode: Int = json["statusCode"] as? Int, statusCode == 0{
                        let data: [String: Any] = json["result"] as! [String: Any]
                        let grade: Int = data["grade"] as! Int
                        //判断图片的可识别度
                        if grade < 4{
                            success?()
                        }else{
                            lowGrade?()
                        }
                    }else{
                        failure?(nil)
                    }
                case .failure(_):
                    let error: Error? = response.result.error
                    failure?(error)
                }
                complete?()
        }
    }
    
    //MARK: 上传图片到EasyAR服务器的网络请求
    /**
     上传图片到EasyAR服务器的网络请求
     
     - parameter image:   UIImage
     - parameter name:    图片名字
     - parameter success: 成功的闭包，返回targetId
     - parameter failure: 失败的闭包，要用户重试
     */
    func easyARAddTarget(image: UIImage, imageName name: String, successClosure success: ((_ targetId: String)->())?, failCloure failure: (()->())?) {
        
        let tuple: (String?, String) = base64ImageAndIso8061Time(image: image)
        let encodedImageStr: String = tuple.0!
        let iso8601String: String = tuple.1
        
        //拼接视频下载地址
        let videoPath: String = "http://ucardstorevideo.b0.upaiyun.com/video/\(name).mp4"
        let stringData: Data? = videoPath.data(using: String.Encoding.utf8)
        let meta: String = stringData!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        //拼接签名
        let signatureString: String = "appKey\(appKey)date\(iso8601String)image\(encodedImageStr)meta\(meta)name\(name)size15typeImageTarget\(appSecret)"
        let nssignature: NSString = NSString(string: signatureString)
        let signature: String = nssignature.sha1//获得签名哈希值
        
        
        let para: [String: Any] = ["image": encodedImageStr, "name": name, "size": "15", "meta": meta, "type": "ImageTarget", "date": iso8601String, "appKey": appKey, "signature": signature] as [String: Any]//参数
        
        let urlStr: String = arHost + "/targets/"
        
        SVProgressHUD.show(withStatus: NSLocalizedString("上传AR图片中", comment: ""))
        SVProgressHUD.setDefaultMaskType(.clear)

        request = manager.request(urlStr, method: HTTPMethod.post, parameters: para, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { (response: DataResponse<Any>) in
                
                switch response.result{
                case .success(_):
                    
                    let json: [String: Any] = response.result.value as! [String: Any]
                    
                    if let statusCode: Int = json["statusCode"] as? Int, statusCode == 0{
                        let result: [String: Any] = json["result"] as! [String: Any]
                        let targetId: String = result["targetId"] as! String
                        success?(targetId)//成功上传图片获取targetId
                        
                    }else{
                        failure?()
                        SVProgressHUD.dismiss()
                        SVProgressHUD.setDefaultMaskType(.none)
                    }
                    
                case .failure(_):
                    failure?()
                    SVProgressHUD.dismiss()
                    SVProgressHUD.setDefaultMaskType(.none)
                }
        }
    }
    
    //MARK: 删除EasyAR云端的图片
    /**
     订单创建失败时，删除云端的图片
     
     - parameter targetID: EasyAR云端图片的ID
     */
    func deleteARTarget(targetId targetID: String) {
        
        let tuple: (String?, String) = base64ImageAndIso8061Time(image: nil)
        let iso8601String: String = tuple.1
        
        //拼接签名
        let signatureString: String = "appKey\(appKey)date\(iso8601String)\(appSecret)"
        let nssignature: NSString = NSString(string: signatureString)
        let signature: String = nssignature.sha1
        
        let para: [String: Any] = ["date": iso8601String, "appKey": appKey, "signature": signature] as [String: Any]
        
        let urlStr: String = arHost + "/target/\(targetID)"
        
        request = manager.request(urlStr, method: HTTPMethod.delete, parameters: para, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response: DataResponse<Any>) in
                switch response.result{
                case .success( _):
//                    print("Success->>\(json)")
                    break
                case .failure( _):
//                    print("Failure->>\(error)")
                    break
                }
        }
    }
    
    //MARK: 已经上传过AR图片，根据TargetID修改图片
    /**
     已经上传过AR图片，根据TargetID修改图片
     
     - parameter image:    UIImage
     - parameter name:     图片名字
     - parameter targetID: 图片的TargetID
     - parameter success:  成功的闭包
     - parameter failure:  失败的闭包
     */
    func putTarget(image: UIImage, imageName name: String, andTargetID targetID: String, successClosure success: ((_ targetId: String)->())?, failCloure failure: (()->())?) {
        
        let tuple: (String?, String) = base64ImageAndIso8061Time(image: image)
        let encodedImageStr: String = tuple.0!
        let iso8601String: String = tuple.1
        
        //拼接视频下载地址
        let videoPath: String = "http://ucardstorevideo.b0.upaiyun.com/video/\(name).mp4"
        let stringData: Data? = videoPath.data(using: String.Encoding.utf8)
        let meta: String = stringData!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        
        //拼接签名
        let signatureString: String = "appKey\(appKey)date\(iso8601String)image\(encodedImageStr)meta\(meta)name\(name)size15typeImageTarget\(appSecret)"
        let nssignature: NSString = NSString(string: signatureString)
        let signature: String = nssignature.sha1//获得签名哈希值
        
        let para: [String: Any] = ["image": encodedImageStr, "name": name, "size": "15", "meta": meta, "type": "ImageTarget", "date": iso8601String, "appKey": appKey, "signature": signature] as [String: Any]//参数
        
        let urlStr: String = arHost + "/target/\(targetID)"
        
        SVProgressHUD.show(withStatus: NSLocalizedString("上传AR图片中", comment: ""))
        SVProgressHUD.setDefaultMaskType(.clear)
        
        request = manager.request(urlStr, method: HTTPMethod.put, parameters: para, encoding: JSONEncoding.default, headers: nil)
            .responseJSON { [weak self] (response: DataResponse<Any>) in
                
                switch response.result{
                case .success(_):
                    let json: [String: Any] = response.result.value as! [String: Any]
//                    print("Success->>\(json)")
                    
                    if let statusCode: Int = json["statusCode"] as? Int, statusCode == 0{
                        let result: [String: Any] = json["result"] as! [String: Any]
                        let targetId: String = result["targetId"] as! String
                        success?(targetId)//成功上传图片获取targetId
                        
                    }
                    //目标不存在，直接上传
                    else if let statusCode: Int = json["statusCode"] as? Int, statusCode == -1{
                        self?.easyARAddTarget(image: image, imageName: name, successClosure: { (targetId: String) in
                            
                            success?(targetId)
                            
                            }, failCloure: { 
                                failure?()
                                SVProgressHUD.dismiss()
                                SVProgressHUD.setDefaultMaskType(.none)
                        })
                    }
                    //其他错误
                    else{
                        failure?()
                        SVProgressHUD.dismiss()
                        SVProgressHUD.setDefaultMaskType(.none)
                    }
                case .failure(_):
                    failure?()
                    SVProgressHUD.dismiss()
                    SVProgressHUD.setDefaultMaskType(.none)
                }
            }
    }
    
    //MARK: 获取图片信息
    func getTarget(targetID: String) {
        
        let tuple: (String?, String) = base64ImageAndIso8061Time(image: nil)
        let iso8601String: String = tuple.1
        
        let signatureString: String = "appKey\(appKey)date\(iso8601String)\(appSecret)"
        let nssignature: NSString = NSString(string: signatureString)
        let signature: String = nssignature.sha1
        
        let para: [String: Any] = ["date": iso8601String, "appKey": appKey, "signature": signature] as [String: Any]
        
        let urlStr: String = arHost + "/target/\(targetID)"
        
        request = manager.request(urlStr, method: HTTPMethod.get, parameters: para, encoding: URLEncoding.default, headers: nil)
            .validate()
            .responseJSON { (response: DataResponse<Any>) in
                switch response.result{
                case .success(_):
                    
                    if let json: [String: Any] = response.result.value as? [String: Any]{
                        
                        if let statusCode: Int = json["statusCode"] as? Int, statusCode == 0{
                            
                            let result: [String: Any] = json["result"] as! [String: Any]
                            
                            if let meta: String = result["meta"] as? String{
                                let data: Data = Data(base64Encoded: meta, options: [])!
                                if let string: String = String(data: data, encoding: String.Encoding.utf8){
                                    print("meta->>>>>\(string)")
                                }
                            }
                        }
                    }
                case .failure(_):
                    if let error: Error = response.result.error{
                        print("Failure->>\(error)")
                    }
                }
        }
    }
    
    //MARK: 将图片转成base64，并获取时间戳
    /// 将图片转成base64，并获取时间戳
    ///
    /// - Parameter image: 图片为空，则返回空
    /// - Returns: imageString, timeString
    fileprivate func base64ImageAndIso8061Time(image: UIImage?)->(imageString: String?, timeString: String){
        
        var encodedImageStr: String?
        
        if image != nil{
            //将图片转成字符串
            let imageData: Data? = UIImageJPEGRepresentation(image!, 1.0)
            encodedImageStr = imageData!.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        }
        
        //获取ISO8601时间字符串
        let formatter: DateFormatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        let iso8601String: String = formatter.string(from: Date()) + "Z"
        
        return (encodedImageStr, iso8601String)
    }
}
