//
//  EasyARViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/9/23.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class EasyARViewModel: BaseViewModel {

    let dataModel: EasyARDataModel = EasyARDataModel()
    
    /// 网络错误的闭包
//    var errorClosure: ((_ errror: Error?, _ result: [String: Any]?)->())?
    
    /// 成功收件的闭包
    var receivePostcardSuccess: (()->())?
    
    /// 成功获取优惠券，并需要进入优惠券列表界面
    var addCouponSuccess: (()->())?
    
    /// app 需要更新
    var appNeedUpdate: (()->())?
    
    /// 需要加载AR视频
    var loadARVideo: ((_ videoPath: String)->())?
    
    /// 播放在线视频
    var playOnlineARVideo: ((_ videoURL: String)->())?
    
    /// 正在下载的视频的ID
    fileprivate var downLoadingVideoID: String = ""
    /// 正在下载的视频的地址
    fileprivate var downLoadingVideoURLString: String = ""
    
    //MARK: 发送签收明信片的请求
    func receivePostCard(serialNumber: String){
        
//        let url: String = ReceivePostCardAPI + "\(serialNumber)"
//        
//        dataModel.sendGETRequestWithURLString(URLStr: url, ParametersDictionary: nil, successClosure: { [weak self] (result: [String: Any]) in
//            self?.receivePostcardSuccess?()
//            }, failureClourse: { [weak self](error: Error?) in
//                self?.errorClosure?(error, nil)
//            }, completionClosure: nil)
    }
    
    //MARK: 获取一张优惠券
    /// 获取一张优惠券
    ///
    /// - Parameter couponId: 优惠券code
    func addCoupon(couponId: String){
        showSVProgress(title: nil)
        
        dataModel.iTunesAppUpdate(success: { [weak self] _ in
            self?.sendRequest(couponId: couponId)
            }, needToUpdate: { [weak self] _ in
                self?.appNeedUpdate?()
            }, failureClosure: { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 发送请求优惠券的请求
    fileprivate func sendRequest(couponId: String){
        dataModel.addCoupon(couponId: couponId, successClosure: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.addCouponSuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 检查本地是否有视频缓存
    /// 检查本地是否有视频缓存，判断是直接加载本地视频，还是先下载视频到本地
    ///
    /// - Parameter videoID: 视频ID
    func checkVideo(videoID: String?, andVideoURLString videoURLString: String?){
        
        if videoID == downLoadingVideoID || videoURLString == downLoadingVideoURLString{ return }
        
        //获取视频的名字
        var videoName: String
        // 获取视频路径
        var videoURLToPlay: String
        if videoURLString != nil {
            //若视频原路径不为空，则直接去原视频路径
            downLoadingVideoURLString = videoURLString!
            // 截取视频的名字
            videoName = URL(string: downLoadingVideoURLString)!.deletingPathExtension().lastPathComponent
            videoURLToPlay = downLoadingVideoURLString
        }else{
            //如果原视频路径为空，则取默认的路径
            downLoadingVideoID = videoID!
            videoName = downLoadingVideoID
            videoURLToPlay = upyunVideoURL + videoName + ".mp4"
        }
        
        let cacheDirectory: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!//获取缓存文件夹
        let videoFolder: String = cacheDirectory + "/videoCache"
        let videoPath: String = videoFolder + "/" + videoName + ".mp4"//拼接视频路径
        
        if FileManager.default.fileExists(atPath: videoPath) == true {
            loadARVideo?(videoPath)
            downLoadingVideoID = ""
            downLoadingVideoURLString = ""
        }else{
            playOnlineARVideo?(videoURLToPlay)
            dataModel.downLoadVideo(videoURLString: videoURLToPlay, andSavePath: videoPath, downLoadSuccess: { [weak self] _ in
                self?.loadARVideo?(videoPath)//下载完成后，再加载本地视频
                self?.downLoadingVideoID = ""
                self?.downLoadingVideoURLString = ""
                }, downLoadFail: { [weak self] (error: Error?) in
                    self?.downLoadingVideoID = ""
                    self?.downLoadingVideoURLString = ""
            })
        }
    }
}
