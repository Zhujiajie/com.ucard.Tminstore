//
//  VideoCommentPreviewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class VideoCommentPreviewController: BaseViewController {

    fileprivate let viewModel: VideoCommentPreviewViewModel = VideoCommentPreviewViewModel()
    
    var videoPathURL: URL?
    var originalId: String?
    var videoId: String?
    var isComment: Bool = false
    
    ///播放器
    fileprivate var player: ZTPlayer?
    /// 重新播放的按钮
    fileprivate var playAgainButton: UIButton?
    /// 完成的按钮
    fileprivate var finishButton: UIButton?
    fileprivate let playerFrame: CGRect = CGRect(x: 0, y: -navBarHeight, width: screenWidth, height: screenHeight)
    
    // MARK: 是否隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        view.backgroundColor = UIColor.black
        setNavigationBar()
        setPlayer()
        setButtonAndGesture()
        
        //MARK: 上传视频成功，返回时光圈
        viewModel.uploadSuccess = {[weak self] (activityFlag: Bool) in
            
            if activityFlag == true{
                let vc: ActivityVC = ActivityVC()
                vc.backImage = self?.getScreenImage()
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self?.present(vc, animated: true, completion: nil)
                vc.dismissClosure = {[weak self] _ in
                    self?.backToVC()
                }
            }else{
                self?.alert(alertTitle: NSLocalizedString("发布成功", comment: ""), leftClosure: {
                    self?.backToVC()
                })
            }
            self?.deleteVideoFileInBackground()
//            for vc: UIViewController in (self?.navigationController?.viewControllers)!{
//                if vc.isKind(of: LocalARVC.self) == true{
//                    _ = self?.navigationController?.popToViewController(vc, animated: true)
//                }
//            }
        }
        
        //MARK: 出错
        viewModel.netWorkError = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }

    //MARK: 发布成功，返回特定的页面
    fileprivate func backToVC(){
        if isComment == false{
            _ = navigationController?.popToRootViewController(animated: true)
        }else{
            for vc: UIViewController in navigationController!.viewControllers{
                if let controller: VideoReplyController = vc as? VideoReplyController{
                    controller.refreshData()
                    _ = navigationController?.popToViewController(controller, animated: true)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        player?.url = videoPathURL
        player?.playFromBeginning()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("whiteBackArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayer(){
        
        let blackView: UIView = UIView()
        blackView.backgroundColor = UIColor.black
        view.addSubview(blackView)
        blackView.frame = playerFrame
        
        player = ZTPlayer()
        player?.view.backgroundColor = UIColor.black
        player?.view.frame = playerFrame
        player?.playbackLoops = true
        view.addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            self?.playAgainButton?.isHidden = false
        }
        player?.fillMode = FillMode.resizeAspectFill.rawValue

        //重新开始播放的按钮
        playAgainButton = UIButton()
        playAgainButton?.setImage(UIImage(named: "playButton"), for: UIControlState.normal)
        playAgainButton?.contentMode = UIViewContentMode.scaleAspectFit
        playAgainButton?.addTarget(self, action: #selector(playFromCurrentTime(button:)), for: UIControlEvents.touchUpInside)
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 75/baseWidth, height: 75/baseWidth))
        })
        playAgainButton?.isHidden = true
    }
    
    //MARK: 设置底部完成的按钮，并添加手势
    fileprivate func setButtonAndGesture(){
        finishButton = UIButton()
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("完成", comment: ""), attributes: attDic)
        finishButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        finishButton?.backgroundColor = UIColor(hexString: "4CDFED")
        view.addSubview(finishButton!)
        finishButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(53/baseWidth)
        })
        finishButton?.addTarget(self, action: #selector(touchFinishButton(button:)), for: UIControlEvents.touchUpInside)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPlayer(tap:)))
        view.addGestureRecognizer(tap)
    }
    
    //MARK: 重新开始播放视频
    func playFromCurrentTime(button: UIButton){
        playAgainButton?.isHidden = true
        player?.playFromCurrentTime()
    }
    
    //MARK: 点击播放和暂停的手势
    func tapPlayer(tap: UITapGestureRecognizer){
        if player?.playbackState == PlaybackState.playing{
            player?.pause()
            playAgainButton?.isHidden = false
        }else if player?.playbackState == PlaybackState.paused{
            player?.playFromCurrentTime()
            playAgainButton?.isHidden = true
        }
    }
    
    //MARK: 点击完成按钮上传视频
    func touchFinishButton(button: UIButton) {
        player?.pause()
        playAgainButton?.isHidden = false
        viewModel.uploadVideo(videoPathURL: videoPathURL!, andOriginalId: originalId, andVideoId: videoId, andIsComment: isComment)
    }
    
    //MARK: 在后台删除视频
    fileprivate func deleteVideoFileInBackground(){
        doInBackground(inBackground: {[weak self] _ in
            if FileManager.default.fileExists(atPath: (self?.videoPathURL?.path)!) == true{
                do{
                    try FileManager.default.removeItem(at: (self?.videoPathURL)!)
                }catch{print(error)}
            }
        })
    }
    
    //MARK: 点击导航栏左按钮，提示是否退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        deleteAlert(alertTitle: NSLocalizedString("确定退出?\n未保存的视频会被删除", comment: ""), leftButtonTitle: NSLocalizedString("继续", comment: ""), rightButtonTitle: NSLocalizedString("退出", comment: ""), rightClosure: { [weak self] _ in
            self?.deleteVideoFileInBackground()
            _ = self?.navigationController?.popViewController(animated: true)
        })
    }
}
