//
//  VideoCommentController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class VideoCommentController: BaseViewController {
    
    var originalId: String?
    var videoId: String?
    
    /// 是否是发布评论
    var isComment: Bool = false
    
    /// 摄像头预览试图，用户接受手势
    fileprivate var previewView: UIView?
    
    /// 播放摄像头接收到的画面的layer
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer?
    
    /// 录制视频的类
    fileprivate var vision: ZTJVison?
    
    /// 定时器，用于监视录制进度
    fileprivate var timer: Timer?
    
    /// 现实时间的lable
    fileprivate var timeLabel: MULabel?
    
    /// 已录制好的视频的存储路径
    var videoPathURL: URL?
    
    /// 录制的按钮
    fileprivate var recordButton: VideoCommentButton?
    
    /// 聚焦的动画视图
    fileprivate var focusView: PBJFocusView?
    
    /// 重新开始录制的按钮
    fileprivate var deleteButton: VideoCommentButton?
    
    /// 确认视频的按钮
    fileprivate var confirmVideoButton: VideoCommentButton?
    
    /// 闪光灯按钮
    fileprivate var torchBarItem: LocalARBarButtonItem?
    
    /// 录屏的按钮
    fileprivate var switchCameraBarItem: LocalARBarButtonItem?
    
    fileprivate let arFrame: CGRect = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
    /// 是否要展示删除和确认视频的按钮
    fileprivate var shouldShowVideoControl: Bool = false
    
    // MARK: 是否隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        view.backgroundColor = UIColor.black
        
        setNavigationBar()
        setVideoRecorder()
        setPreviewView()
        setTimeLabelAndButtons()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if vision != nil {
            vision?.startPreview()//开启录制界面的预览
            setUpTimer()//设置计时器
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        vision?.stopPreview()
        vision?.cancelVideoCapture()
        timer?.invalidate()
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("whiteBackArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        torchBarItem = LocalARBarButtonItem.torchBarItem()
        torchBarItem?.shouldControllerTorchSelf = true
        switchCameraBarItem = LocalARBarButtonItem.switchCameraBarItem()
        navigationItem.rightBarButtonItems = [switchCameraBarItem!, torchBarItem!]
    }

    //MARK: 设置视频录制
    fileprivate func setVideoRecorder(){
        if vision == nil {
            showSVProgress(title: nil)//初始化控件时，不允许交互
            vision = ZTJVison.ztJVison(device: PBJCameraDevice.front, andMaxDuration: 10.0)
            vision?.delegate = self
        }
        vision?.startPreview()//开始预览
        focusView = PBJFocusView()//设置聚焦视图
    }
    
    //MARK: 设置预览界面
    fileprivate func setPreviewView(){
        
        previewView = UIView(frame: arFrame)
        previewView?.backgroundColor = UIColor.black
        view.addSubview(previewView!)
        previewView?.frame.origin.y = -navBarHeight
        
        //点按重新聚焦
        let focusTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToFocus(_:)))
        previewView?.addGestureRecognizer(focusTap)
        
        previewLayer = vision?.previewLayer
        previewLayer?.frame = (previewView?.bounds)!
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewView?.layer.addSublayer(previewLayer!)
    }
    
    //MARK: 设置播放进度条和时间标签
    fileprivate func setTimeLabelAndButtons(){
        
        //点击录制的按钮
        recordButton = VideoCommentButton.recordButton()
        view.addSubview(recordButton!)
        recordButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(-37/baseHeight)
            make.centerX.equalTo(view.snp.centerX)
            make.size.equalTo(CGSize(width: 83/baseWidth, height: 83/baseWidth))
        })
        
        //删除按钮
        deleteButton = VideoCommentButton.deleteVideoButton()
        view.insertSubview(deleteButton!, belowSubview: recordButton!)
        deleteButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(recordButton!)
            make.centerX.equalTo(view).offset(0)
            make.size.equalTo(CGSize(width: 54/baseWidth, height: 54/baseWidth))
        })
        
        // 确认视频的按钮
        confirmVideoButton = VideoCommentButton.confirmVideoButton()
        view.insertSubview(confirmVideoButton!, belowSubview: recordButton!)
        confirmVideoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(deleteButton!)
            make.centerX.equalTo(view).offset(0)
            make.size.equalTo(deleteButton!)
        })
        
        timeLabel = MULabel.timeLabel()
        timeLabel?.textColor = UIColor.white
        view.addSubview(timeLabel!)
        timeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(recordButton!.snp.top).offset(-11/baseHeight)
            make.centerX.equalTo(view)
            make.height.equalTo(20)
        })
    }
    
    //MARK: 点击导航栏左按钮，提示是否退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        if originalId != nil {
            deleteAlert(alertTitle: NSLocalizedString("确定退出?\n未保存的视频会被删除", comment: ""), leftButtonTitle: NSLocalizedString("继续", comment: ""), rightButtonTitle: NSLocalizedString("退出", comment: ""), rightClosure: { [weak self] _ in
                _ = self?.navigationController?.popViewController(animated: true)
            })
        }else{
            dismissSelf()
        }
    }
}

//MARK: 手势和点击事件
extension VideoCommentController {
    
    //MARK: 点击屏幕，重新聚焦
    func tapToFocus(_ tap: UITapGestureRecognizer){
        
        let tapPoint: CGPoint = tap.location(in: previewView!)
        
        focusView!.frame.origin.x = tapPoint.x - WIDTH(focusView!)/2
        focusView!.frame.origin.y = tapPoint.y - HEIGHT(focusView!)/2
        previewView?.addSubview(focusView!)
        focusView?.startAnimation()
        
        let adjustPoint: CGPoint = PBJVisionUtilities.convertToPointOfInterest(fromViewCoordinates: tapPoint, inFrame: previewView!.frame)
        
        vision?.focusExposeAndAdjustWhiteBalance(atAdjustedPoint: adjustPoint)
    }
    
    //MARK: 点击开始或者停止录制视频
    func touchRecordButton() {
        
        if vision?.isRecording == false {
            vision?.startVideoCapture()
            recordButton?.setImage(UIImage(named: "ARRecording"), for: UIControlState.normal)
        }else{
            if vision?.isPaused == false{
                vision?.pauseVideoCapture()
                recordButton?.setImage(UIImage(named: "ARreadyToRecord"), for: UIControlState.normal)
            }else{
                vision?.resumeVideoCapture()
                recordButton?.setImage(UIImage(named: "ARRecording"), for: UIControlState.normal)
            }
        }
        
        //弹出视频控制按钮，隐藏视频选择的imageView
        if shouldShowVideoControl == false{
            deleteButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.centerX.equalTo(view).offset(-127/baseWidth)
            })
            confirmVideoButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.centerX.equalTo(view).offset(127/baseWidth)
            })
            autoLayoutAnimation(view: view, withDelay: 0.0)
            shouldShowVideoControl = !shouldShowVideoControl
        }
    }
    
    
    //MARK: 点击切换摄像头的按钮，切换前后摄像头
    func touchRotateButton()  {
        guard let vision: ZTJVison = self.vision else { return}
        if vision.cameraDevice == PBJCameraDevice.back {
            vision.cameraDevice = PBJCameraDevice.front
        }else{
            vision.cameraDevice = PBJCameraDevice.back
        }
    }
    
    //MARK: 点击删除按钮，重新开始录制
    func touchDeleteButton(){
        recordButton?.setImage(UIImage(named: "ARreadyToRecord"), for: UIControlState.normal)
        if vision?.isRecording == true{
            vision?.cancelVideoCapture()//取消录制
            setUpTimer()
            if videoPathURL != nil {//将视频删除掉
                do {
                    try FileManager.default.removeItem(at: videoPathURL!)
                }catch {fatalError(error.localizedDescription)}
            }
            videoPathURL = nil
        }
        //隐藏视频控制按钮，出现视频选择的imageView
        if shouldShowVideoControl == true{
            deleteButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.centerX.equalTo(view).offset(0)
            })
            confirmVideoButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.centerX.equalTo(view).offset(0)
            })
            autoLayoutAnimation(view: view, withDelay: 0.0)
            shouldShowVideoControl = !shouldShowVideoControl
        }
    }
    
    //MARK: 设置计时器，监控录制进度
    fileprivate func setUpTimer(){
        timer = Timer(timeInterval: 1.0, target: self, selector: #selector(updateRecordTime(_:)), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    //MARK: 通过计时器获得录制进度
    func updateRecordTime(_ timer: Timer){
        
        var seconds: Double = vision!.capturedVideoSeconds
        switch seconds {
        case -10..<0:
            seconds = 0
            if confirmVideoButton?.isEnabled == true{
                confirmVideoButton?.isEnabled = false
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                    self.confirmVideoButton?.alpha = 0.5
                }, completion: nil)//录制时间不够
            }
        case 5..<videoMaxDuration:
            if confirmVideoButton?.isEnabled == false{
                confirmVideoButton?.isEnabled = true
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                    self.confirmVideoButton?.alpha = 1
                }, completion: nil)//录制时间足够
            }
        case videoMaxDuration..<100:
            seconds = videoMaxDuration
        default:
            break
        }
        timeLabel?.text = String(format: "00:%02.0f", seconds)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK:进入后台之后，停止预览
        app.appDidEnterBackground = {[weak self] _ in
            self?.vision?.stopPreview()
            self?.vision?.pauseVideoCapture()
            self?.recordButton?.setImage(UIImage(named: "ARreadyToRecord"), for: UIControlState.normal)
            self?.timer?.invalidate()
        }
        //MARK:进入前台之后。开始预览
        app.appDidBecomeActive = {[weak self] _ in
            guard let vision: ZTJVison = self?.vision else { return }
            vision.startPreview()
            if self?.timer?.isValid == false{
                self?.setUpTimer()
            }
        }
        
        //MARK: 点击删除视频的按钮
        deleteButton?.touchButtonClosure = {[weak self] _ in
            self?.touchDeleteButton()
        }
        
        //MARK: 点击确认开始录制视频按钮的事件
        recordButton?.touchButtonClosure = {[weak self] _ in
            self?.touchRecordButton()
        }
        
        //MARK: 点击确认视频按钮的事件
        confirmVideoButton?.touchButtonClosure = {[weak self] _ in
            self?.vision?.endVideoCapture()//完成视频录制
        }
        
        //MARK: 点击导航栏右按钮，切换摄像头正反面
        switchCameraBarItem?.touchMoreButtonClosure = {[weak self] _ in
            self?.touchRotateButton()
        }
        
        //MARK: 点击闪光灯按钮，打开或者关闭闪光灯
        torchBarItem?.touchMoreButtonClosure = {[weak self] _ in
            if self?.vision?.isFlashAvailable == true{
                if self?.vision?.flashMode == PBJFlashMode.off{
                    self?.vision?.flashMode = PBJFlashMode.on
                    self?.torchBarItem?.image = UIImage(named: "localTorchOn")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                }else if self?.vision?.flashMode == PBJFlashMode.on{
                    self?.vision?.flashMode = PBJFlashMode.off
                    self?.torchBarItem?.image = UIImage(named: "localTorchOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                }
            }
        }
    }
}

extension VideoCommentController: PBJVisionDelegate{
    
    //MARK: 对焦和曝光的代理
    func visionDidStopFocus(_ vision: PBJVision) {
        if focusView != nil {
            focusView?.stopAnimation()
        }
    }
    
    func visionDidChangeExposure(_ vision: PBJVision) {
        if focusView != nil {
            DispatchQueue.main.async {[weak self] _ in
                self?.focusView?.stopAnimation()
            }
        }
    }
    
    //MARK: 可以录制之后，再绘制录制按钮
    func visionSessionDidStart(_ vision: PBJVision) {
        dismissSVProgress()
    }
    
    //MARK: 拍摄完成
    func vision(_ vision: PBJVision, capturedVideo videoDict: [AnyHashable : Any]?, error: Error?) {
        timer?.invalidate()
        if error != nil  { return }
        
        //先删除原先的视频
        if videoPathURL != nil{
            if FileManager.default.fileExists(atPath: videoPathURL!.path) == true{
                do {
                    try FileManager.default.removeItem(at: videoPathURL!)
                }catch{print(error)}
            }
        }
        
        if let videoPathStr: String = videoDict![PBJVisionVideoPathKey] as? String {
            
            let filePath: String = "file://" + videoPathStr//编辑存储地址
            videoPathURL = URL(string: filePath)
            
//            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(videoPathStr) == true {
//                UISaveVideoAtPathToSavedPhotosAlbum(videoPathStr, nil, nil, nil)
//            }
            
            if videoPathURL != nil{//成功获取视频，进入预览界面
                DispatchQueue.main.async {[weak self] _ in
                    let previewVC: VideoCommentPreviewController = VideoCommentPreviewController()
                    previewVC.umengViewName = "上传视频评论"
                    previewVC.videoPathURL = self?.videoPathURL
                    previewVC.originalId = self?.originalId
                    previewVC.videoId = self?.videoId
                    previewVC.isComment = (self?.isComment)!
                    _ = self?.navigationController?.pushViewController(previewVC, animated: true)
                }
            }
        }
    }
}
