//
//  VideoCommentButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class VideoCommentButton: UIButton {

    /// 点击按钮的事件
    var touchButtonClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
    }

    //MARK: 录制视频的按钮
    /// 录制视频的按钮
    ///
    /// - Returns: MUButton
    class func recordButton()->VideoCommentButton{
        let recordButton: VideoCommentButton = VideoCommentButton()
        recordButton.setImage(UIImage(named: "ARreadyToRecord"), for: UIControlState.normal)
        return recordButton
    }
    
    //MARK: 删除视频的按钮
    /// 删除视频的按钮
    ///
    /// - Returns: MUButton
    class func deleteVideoButton()->VideoCommentButton{
        let deleteButton: VideoCommentButton = VideoCommentButton()
        deleteButton.setImage(UIImage(named: "ARdeleteRecordAR"), for: UIControlState.normal)
        return deleteButton
    }
    
    //MARK: 确认视频的按钮
    /// 确认视频的按钮
    ///
    /// - Returns: MUButton
    class func confirmVideoButton()->VideoCommentButton{
        let confirmViewButton: VideoCommentButton = VideoCommentButton()
        confirmViewButton.setImage(UIImage(named: "ARconfirmVideo"), for: UIControlState.normal)
        return confirmViewButton
    }
    
    //MARK: 拍照的按钮
    /// 拍照的按钮
    ///
    /// - Returns: VideoCommentButton
    class func takePhotoButton()->VideoCommentButton{
        let confirmViewButton: VideoCommentButton = VideoCommentButton()
        confirmViewButton.setImage(UIImage(named: "ARreadyToRecord"), for: UIControlState.normal)
        return confirmViewButton
    }
    
    //MARK: 点击按钮的事件
    func touchButton(button: UIButton){
        touchButtonClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
