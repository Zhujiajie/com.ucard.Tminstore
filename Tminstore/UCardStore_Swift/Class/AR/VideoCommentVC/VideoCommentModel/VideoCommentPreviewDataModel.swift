//
//  VideoCommentPreviewDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class VideoCommentPreviewDataModel: BaseDataModel {

    fileprivate var videoPathURL: URL?
    fileprivate var videoRemotePath: String?
    fileprivate var thumbnailRemotePath: String?
    fileprivate var originalId: String?
    fileprivate var videoId: String?
    fileprivate var exportSession: AVAssetExportSession?
    /// 是否是发布评论
    fileprivate var isComment: Bool = false
    
    ///上传成功的闭包，返回支付信息
    var uploadSuccess: ((_ result: [String: Any])->())?
    
    //MARK: 将卡片投递到时空圈
    func shareToSpaceTime(videoPathURL: URL, andOriginalId originalId: String?, andVideoId videoId: String?, andIsComment isComment: Bool){
        
        self.isComment = isComment
        self.videoPathURL = videoPathURL
        self.originalId = originalId
        self.videoId = videoId
        if videoRemotePath == nil{
            if self.isComment == false{
                videoRemotePath = mapVideoUpun + UUID().uuidString +  ".mp4"
            }else{
                videoRemotePath = mapCommentVideoUpun + UUID().uuidString + ".mp4"
            }
        }
        if thumbnailRemotePath == nil{
            if self.isComment == false{
                thumbnailRemotePath = mapVideoThumbnailUpun + UUID().uuidString + ".png"
            }else{
                thumbnailRemotePath = mapCommentThumbnailUpun + UUID().uuidString + ".png"
            }
        }
        compressVideo()//压缩视频
    }
    
    //MARK: 压缩视频
    fileprivate func compressVideo(){
        
        let asset: AVAsset = AVAsset(url: videoPathURL!)
        
        do{
            let cachesURL: URL = try FileManager.default.url(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: true)//获取缓存文件夹路径
            let videoFolder: String = cachesURL.path + "/videoCache"//获取视频存储的文件夹路径
            if FileManager.default.fileExists(atPath: videoFolder) == false {
                try FileManager.default.createDirectory(atPath: videoFolder, withIntermediateDirectories: false, attributes: nil)
            }//如果文件夹不存在，则创建一个
            let compressVideoOutputURL = URL(fileURLWithPath: videoFolder + "/\(UUID().uuidString).mp4")//设置压缩过的视频存放的路径
            
            let compatiblePresets: [String] = AVAssetExportSession.exportPresets(compatibleWith: asset)
            if compatiblePresets.contains(AVAssetExportPresetMediumQuality) {
                exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetLowQuality)
                
                exportSession?.outputURL = compressVideoOutputURL//输出位置
                exportSession?.outputFileType = AVFileTypeMPEG4//视频格式
                let start: CMTime = CMTimeMakeWithSeconds(0.0, asset.duration.timescale)
                let duration: CMTime = CMTimeMakeWithSeconds(asset.duration.seconds, asset.duration.timescale)
                let range: CMTimeRange = CMTimeRangeMake(start, duration)
                exportSession?.timeRange = range//截取视频的时间范围
                
                exportSession?.exportAsynchronously(completionHandler: { [weak self]_ in
                    switch (self?.exportSession?.status)!{
                    case AVAssetExportSessionStatus.failed, AVAssetExportSessionStatus.cancelled:
                        doInBackground(inBackground: {
                            if FileManager.default.fileExists(atPath: compressVideoOutputURL.path) == true{
                                do {
                                    try FileManager.default.removeItem(at: compressVideoOutputURL)
                                }catch{print(error)}
                            }
                        })//压缩失败，在后台删除视频
                        self?.errorClosure?(self?.exportSession?.error)
                    default:
                        self?.uploadVideo(videoPath: compressVideoOutputURL.path)//裁剪视频成功，进入预览界面
                    }
                })
            }
        }catch{
            errorClosure?(error)
        }
    }
    
    //MARK: 上传视频到又拍云
    fileprivate func uploadVideo(videoPath: String){
        
        do{
            let videoData: Data = try Data(contentsOf: URL(fileURLWithPath: videoPath), options: [])
            
            let parameters: [String: Any] = [
                "name": "naga",
                "type": "video",
                "avopts": "/wmImg/L2lPU0ltYWdlcy93YXRlcm1hcmswMS5wbmc=/wmGravity/northwest/wmDx/20/wmDy/15",
                "save_as": videoRemotePath!
            ]
        
            uploadToUpyun(data: videoData, andSaveKey: videoRemotePath!, andParameters: ["apps": [parameters]], successClosure: { [weak self] (_, _) in
                self?.uploadThumbnail(videoPath: videoPath)
                }, failClosure: { [weak self] (error: Error?, _, _) in
                self?.errorClosure?(error)
                }, progressClosure: { (completeCount: Float, totalCount: Float) in
                SVProgressHUD.showProgress(completeCount/totalCount, status: NSLocalizedString("上传视频中", comment: ""))
            })
        }catch{errorClosure?(error)}
        
    }
    
    //MARK: 上传视频缩略图到又拍云
    fileprivate func uploadThumbnail(videoPath: String){
        
        guard let thumbnailImage: UIImage = getThumbnailImage(videoPath: videoPath), let thumbnailImageData: Data = UIImagePNGRepresentation(thumbnailImage)  else {
            errorClosure?(nil)
            return }
        
        uploadToUpyun(data: thumbnailImageData, andSaveKey: thumbnailRemotePath!, andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.upLoadVideoCommentInfo()
            }, failClosure: { [weak self] (error: Error?, _, _) in
            self?.errorClosure?(error)
            }, progressClosure: {(completeCount: Float, totalCount: Float) in
                SVProgressHUD.showProgress(Float(completeCount/totalCount), status: NSLocalizedString("上传图片中", comment: ""))
        })
    }
    
    
    
    //MARK: 上传视频评论信息
    fileprivate func upLoadVideoCommentInfo(){
        
        SVProgressHUD.show()
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
        
        var urlString: String
        var parameters: [String: Any]
        
        if isComment == false{
            urlString = appAPIHelp.spaceTimeAddVideoAPI
            parameters = [
                "token": getUserToken(),
                "originalId": originalId!,
                "videoUrl": appAPIHelp.upyunBase + videoRemotePath!,
                "videoThumbnail": appAPIHelp.upyunBase + thumbnailRemotePath!
            ]
        }else{
            urlString = appAPIHelp.uploadVideoCommentAPI
            parameters = [
                "token": getUserToken(),
                "videoId": videoId!,
                "contentUrl": appAPIHelp.upyunBase + videoRemotePath!,
                "thumbnailView": appAPIHelp.upyunBase + thumbnailRemotePath!
            ]
        }
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String : Any]) in
            self?.uploadSuccess?(result)
            }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: nil)
    }
    
    //MARK: 获取视频的缩略图
    /// 获取视频的缩略图
    ///
    /// - Parameter videoPath: 视频的路径
    /// - Returns: UIImage?
    fileprivate func getThumbnailImage(videoPath: String)->UIImage?{
        
        let asset: AVAsset = AVAsset(url: URL(fileURLWithPath: videoPath))
        let gen: AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        gen.appliesPreferredTrackTransform = true
        let time: CMTime = CMTime(seconds: 0, preferredTimescale: 600)
        
        var thumnailImage: UIImage?
        
        do {
            let image: CGImage = try gen.copyCGImage(at: time, actualTime: nil)
            thumnailImage = UIImage(cgImage: image)
            return thumnailImage
        }catch{
            errorClosure?(error)
            print(error)
            return nil
        }
    }
}
