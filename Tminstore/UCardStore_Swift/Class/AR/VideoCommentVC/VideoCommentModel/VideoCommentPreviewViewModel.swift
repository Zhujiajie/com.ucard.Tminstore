//
//  VideoCommentPreviewViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class VideoCommentPreviewViewModel: BaseViewModel {

    fileprivate let dataModel: VideoCommentPreviewDataModel = VideoCommentPreviewDataModel()
    /// 发布成功
    var uploadSuccess: ((_ activityFlag: Bool)->())?
    /// 网络错误
    var netWorkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    override init() {
        super.init()
        
        //MARK: 上传成功的闭包
        dataModel.uploadSuccess = {[weak self] (result: [String: Any]) in
            self?.dismissSVProgress()
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    DispatchQueue.main.async {
                        if let activityFlag: Bool = data["activityFlag"] as? Bool{
                            self?.uploadSuccess?(activityFlag)
                        }else{
                            self?.uploadSuccess?(false)
                        }
                    }
                }
                return
            }
            self?.netWorkError?(nil, result)
        }
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            DispatchQueue.main.async {
                self?.netWorkError?(error, nil)
                self?.dismissSVProgress()
            }
        }
    }
    
    //MARK: 发布录制好的视频
    /// 发布录制好的视频
    ///
    /// - Parameters:
    ///   - videoPathURL: 视频在本地的路径
    ///   - originalId: 原创ID
    func uploadVideo(videoPathURL: URL, andOriginalId originalId: String?, andVideoId videoId: String?, andIsComment isComment: Bool){
        showSVProgress(title: nil)
        dataModel.shareToSpaceTime(videoPathURL: videoPathURL, andOriginalId: originalId, andVideoId: videoId, andIsComment: isComment)
    }
}
