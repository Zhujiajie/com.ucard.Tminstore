//
//  VideoSharePlayer.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class VideoSharePlayer: UIView, UITextFieldDelegate {

    /// 成功获得文字
    var getContentTextSuccess: ((_ text: String)->())?
    /// 视频在本地的路径
    var videoURLPath: String?{
        didSet{
            if videoURLPath != nil{
                player?.url = URL(fileURLWithPath: videoURLPath!)
                player?.playFromBeginning()
            }
        }
    }
    
    fileprivate var player: ZTPlayer?
    fileprivate var textField: UITextField?
    fileprivate var playAgainButton: UIButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        setPlayer()
        setTextField()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPlayer(tap:)))
        self.addGestureRecognizer(tap)
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayer(){
        
        player = ZTPlayer()
        player?.view.backgroundColor = UIColor.black
        player?.view.frame = CGRect(x: 8/baseWidth, y: 8/baseWidth, width: 360/baseWidth, height: 242/baseWidth)
        player?.playbackLoops = true
        addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            self?.playAgainButton?.isHidden = false
        }
        player?.fillMode = FillMode.resizeAspectFit.rawValue
        
        //重新开始播放的按钮
        playAgainButton = UIButton()
        playAgainButton?.setImage(UIImage(named: "playButton"), for: UIControlState.normal)
        playAgainButton?.contentMode = UIViewContentMode.scaleAspectFit
        playAgainButton?.addTarget(self, action: #selector(playFromCurrentTime(button:)), for: UIControlEvents.touchUpInside)
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 75/baseWidth, height: 75/baseWidth))
        })
        playAgainButton?.isHidden = true
    }
    
    //MARK: 设置输入备注的textView
    fileprivate func setTextField(){
        textField = UITextField()
        textField?.placeholder = NSLocalizedString("点击添加文字描述...", comment: "")
        textField?.font = UIFont.systemFont(ofSize: 14)
        addSubview(textField!)
        textField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(player!.view.snp.bottom)
            make.left.equalTo(self).offset(8/baseWidth)
            make.right.equalTo(self).offset(-8/baseWidth)
            make.bottom.equalTo(self)
        })
        textField?.delegate = self
    }
    
    //MARK: 点击播放和暂停的手势
    func tapPlayer(tap: UITapGestureRecognizer){
        if player?.playbackState == PlaybackState.playing{
            player?.pause()
            playAgainButton?.isHidden = false
        }else if player?.playbackState == PlaybackState.paused{
            player?.playFromCurrentTime()
            playAgainButton?.isHidden = true
        }
    }
    
    //MARK: 重新开始播放视频
    func playFromCurrentTime(button: UIButton){
        playAgainButton?.isHidden = true
        player?.playFromCurrentTime()
    }
    
    //MARK: 成功获得文字
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text != nil && textField.text != ""{
            getContentTextSuccess?(textField.text!)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
