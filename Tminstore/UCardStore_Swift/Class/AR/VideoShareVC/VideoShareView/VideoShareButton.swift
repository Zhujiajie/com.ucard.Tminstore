//
//  VideoShareButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class VideoShareButton: UIButton {

    /// 点击按钮的事件
    var touchButtonClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
        let attString: NSAttributedString = NSAttributedString(string: NSLocalizedString("完成", comment: ""), attributes: attDic)
        setAttributedTitle(attString, for: UIControlState.normal)
        
        backgroundColor = UIColor(hexString: "4CDFED")
        addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func touchButton(button: UIButton){
        touchButtonClosure?()
    }
}
