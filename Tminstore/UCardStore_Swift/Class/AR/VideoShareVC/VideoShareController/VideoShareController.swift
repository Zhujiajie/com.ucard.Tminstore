//
//  VideoShareController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import IQKeyboardManagerSwift

class VideoShareController: BaseViewController {

    fileprivate let viewModel: VideoShareViewModel = VideoShareViewModel()
    
    var videoURLPath: String?
    
    fileprivate var grayView: UIView?
    fileprivate var playerView: VideoSharePlayer?
    fileprivate var shareView: VideoShareView?
//    fileprivate var finishButton: VideoShareButton?
    
    /// 是否需要弹窗提醒用户还未保存
    fileprivate var isVideoSaved: Bool = false
    
//    /// 是否需要隐藏状态栏
//    fileprivate var isStatusBarHide = false{
//        didSet{
//            setNeedsStatusBarAppearanceUpdate()
//        }
//    }
    // MARK: 是否隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        view.backgroundColor = UIColor.white
        setNavigationBar()
        
        grayView = UIView()
        grayView?.backgroundColor = UIColor(hexString: "#F3F6F8")
        view.addSubview(grayView!)
        grayView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.height.equalTo(8/baseWidth)
        }
        
        setPlayerView()
        setShareView()
        
//        finishButton = VideoShareButton()
//        view.addSubview(finishButton!)
//        finishButton?.snp.makeConstraints({ (make: ConstraintMaker) in
//            make.left.bottom.right.equalTo(view)
//            make.height.equalTo(60/baseWidth)
//        })
        
        closure()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK:设置导航栏
    fileprivate func setNavigationBar() {
        setNavigationBarLeftButtonWithImageName("backArrow")//设置左按钮
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D")] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("分享", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayerView(){
        playerView = VideoSharePlayer()
        playerView?.videoURLPath = videoURLPath
        view.addSubview(playerView!)
        playerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayView!.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(319/baseWidth)
        })
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#F3F6F8")
        playerView?.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(playerView!)
            make.height.equalTo(0.5)
        }
    }
    
    //MARK: 设置分享视图
    fileprivate func setShareView(){
        shareView = VideoShareView()
        view.addSubview(shareView!)
        shareView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(playerView!.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(151/baseWidth)
        })
    }
    
    //MARK: 各种闭包回调
    fileprivate func closure(){
        
        //MARK: 点击分享按钮的事件
        shareView?.touchButtonClosure = {[weak self] (buttonType: ShareButtonType) in
            //判断用户是否登录
            if getUserLoginStatus() {
                self?.viewModel.shareButtonType = buttonType
                self?.viewModel.uploadVideo(videoPathURLString: (self?.videoURLPath)!)
            }else{
                self?.enterLoginView()
            }
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 分享成功的闭包
        viewModel.shareSuccess = {[weak self] _ in
            self?.isVideoSaved = true
            self?.alert(alertTitle: NSLocalizedString("分享成功", comment: ""))
        }
        
        //MARK: 系统自带分享
        viewModel.useSystemShare = {[weak self] (videoRemotePath: String) in
            self?.iOSShare(videoRemotePath: videoRemotePath)
        }
        
        //MARK: 用户写下了备注
        playerView?.getContentTextSuccess = {[weak self] (context: String) in
            self?.viewModel.videoRemark = context
        }
        
//        //MARK: 点击完成按钮的事件
//        finishButton?.touchButtonClosure = {[weak self] _ in
//            self?.popBack()
//        }
    }
    
    //MARK: 系统自带分享
    fileprivate func iOSShare(videoRemotePath: String){
        let ac: UIActivityViewController = UIActivityViewController(activityItems: [URL(string: appAPIHelp.upyunBase + videoRemotePath)!], applicationActivities: nil)
        present(ac, animated: true, completion: nil)
    }
    
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        popBack()
    }
    
    //MARK: 退出
    fileprivate func popBack(){
        //判断一下是否已经保存视频到服务器
        if isVideoSaved == false{
            deleteAlert(alertTitle: NSLocalizedString("视频还未保存确认退出", comment: ""), leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("退出", comment: ""), rightClosure: { [weak self] _ in
                _ = self?.navigationController?.popViewController(animated: true)
            })
        }else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
}
