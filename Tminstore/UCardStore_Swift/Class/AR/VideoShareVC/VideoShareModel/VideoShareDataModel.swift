//
//  VideoShareDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import ImageIO
import SDWebImage

class VideoShareDataModel: BaseDataModel {

    var videoThumbnail: UIImage?
    var shareDescription: String = ""
    
    /// 使用系统自带
    var useSystemShare: ((_ videoRemotePath: String)->())?
    
    fileprivate var videoPathURLString: String?
    fileprivate var videoRemotePath: String?
    fileprivate var thumnailRemotePath: String?
    
    /// 是否已经保存到服务器
    var isVideoSaved: Bool = false
    /// 需要分享的渠道
    var currentShareType: UMSocialPlatformType?
    /// 上传服务器成功的闭包
    var uploadVideoSuccess: ((_ result: [String: Any])->())?
    /// 分享成功的闭包
    var shareSuccess: (()->())?
    
    //MARK: 上传视频到又拍云
    func uploadVideoInfo(videoPathURLString: String){
        
        //如果视频还未上传到服务器，则先上传视频
        if isVideoSaved == false{
            
            self.videoPathURLString = videoPathURLString
            
            if videoRemotePath == nil{
                videoRemotePath = mapShareVideoUpun + NSUUID().uuidString + ".mp4"
            }
            if thumnailRemotePath == nil{
                thumnailRemotePath = mapShareVideoThumbnailUpun + NSUUID().uuidString + ".png"
            }
            uploadThumbnail()
        }else{
            shareToThreePath()
        }
    }
    
    //MARK: 在录屏界面直接分享
    /// 在录屏界面直接分享
    ///
    /// - Parameter recordModel: RecordModel
    func shareFromRecordVC(recordModel: RecordModel){
        
        if currentShareType == nil {
            useSystemShare?(recordModel.videoURL!.absoluteString)
            return
        }
        
        //获取缩略图
        var thumImage: UIImage?
        
        if let cache: SDImageCache = SDWebImageManager.shared().imageCache {
            thumImage = cache.imageFromCache(forKey: recordModel.thumbnailURL?.absoluteString)
        }
        
        let shareObject: UMSocialMessageObject = UMSocialMessageObject()
        
        let shareVideoObject: UMShareVideoObject = UMShareVideoObject.shareObject(withTitle: "来自时记的视频分享", descr: recordModel.remark, thumImage: thumImage)
        shareVideoObject.videoUrl = recordModel.videoURL!.absoluteString
        shareObject.shareObject = shareVideoObject
        
        UMSocialManager.default().share(to: currentShareType!, messageObject: shareObject, currentViewController: nil) { [weak self] (_, error: Error?) in
            if error != nil{
                self?.errorClosure?(error)
            }else{
                self?.shareSuccess?()
            }
        }
    }
    
    //MARK: 上传视频缩略图片
    fileprivate func uploadThumbnail(){
        
        var videoThumbnailData: Data?
        
        videoThumbnail = getThumbnailImage(videoPath: videoPathURLString!)
        if videoThumbnail != nil{
            videoThumbnailData = UIImagePNGRepresentation(videoThumbnail!)
            if videoThumbnailData == nil{
                errorClosure?(nil)
                return
            }
        }
        
        uploadToUpyun(data: videoThumbnailData!, andSaveKey: thumnailRemotePath!, andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.uploadVideo()
            }, failClosure: { [weak self] (error: Error?, _, _) in
            self?.errorClosure?(error)
            }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                self?.uploadImageProgress(percent: completeCount/totalCount)
        })
    }
    
    //MARK: 上传视频到又拍云
    fileprivate func uploadVideo(){
        
        do{
            let videoData: Data = try Data(contentsOf: URL(fileURLWithPath: videoPathURLString!))
            
            let parameters: [String: Any] = [
                "name": "naga",
                "type": "video",
                "avopts": "/wmImg/L2lPU0ltYWdlcy93YXRlcm1hcmswMS5wbmc=/wmGravity/northwest/wmDx/20/wmDy/15",
                "save_as": videoRemotePath!
            ]
            
            uploadToUpyun(data: videoData, andSaveKey: videoRemotePath!, andParameters: ["apps": [parameters]], successClosure: { [weak self] (_, _) in
                self?.saveVideoInfo()
                }, failClosure: { [weak self] (error: Error?, _, _) in
                self?.errorClosure?(error)
                }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                    self?.uploadVideoProgress(percent: completeCount/totalCount)
            })
            
        }catch{errorClosure?(error)}
    }
    
//    //MARK: 上传进度
//    fileprivate func uploadImageProgress(percent: Float){
//        SVProgressHUD.showProgress(percent, status: NSLocalizedString("上传图片中", comment: ""))
//    }
//    
//    //MARK: 上传视频的进度
//    fileprivate func uploadVideoProgress(percent: Float){
//        SVProgressHUD.showProgress(percent, status: NSLocalizedString("上传视频中", comment: ""))
//    }
    
    //MARK: 上传录屏的信息
    fileprivate func saveVideoInfo(){
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "screencapUrl": appAPIHelp.upyunBase + videoRemotePath!,
            "remark": shareDescription,
            "thumbnailView": appAPIHelp.upyunBase + thumnailRemotePath!
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.uploadVideoShareAPI, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String : Any]) in
            self?.uploadVideoSuccess?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
        
    }
    
    //MARK: 获取视频的缩略图
    /// 获取视频的缩略图
    ///
    /// - Parameter videoPath: 视频的路径
    /// - Returns: UIImage?
    fileprivate func getThumbnailImage(videoPath: String)->UIImage?{
        
        let asset: AVAsset = AVAsset(url: URL(fileURLWithPath: videoPath))
        let gen: AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        gen.appliesPreferredTrackTransform = true
        let time: CMTime = CMTime(seconds: 0, preferredTimescale: 600)
        
        var thumnailImage: UIImage?
        
        do {
            let image: CGImage = try gen.copyCGImage(at: time, actualTime: nil)
            thumnailImage = UIImage(cgImage: image)
        }catch{
            errorClosure?(error)
            print(error)
        }
        
        return thumnailImage
    }
    
    //MARK: 分享功能
    /// 分享功能
    func shareToThreePath(){
        
        if currentShareType == nil {
            useSystemShare?(videoRemotePath!)
            return
        }
        
        let shareObject: UMSocialMessageObject = UMSocialMessageObject()
        
        //确保缩略不为空
        videoThumbnail = getThumbnailImage(videoPath: videoPathURLString!)
        if videoThumbnail == nil{
            errorClosure?(nil)
        }
        
        let shareVideoObject: UMShareVideoObject = UMShareVideoObject.shareObject(withTitle: "来自时记的视频分享", descr: shareDescription, thumImage: videoThumbnail!)
        shareVideoObject.videoUrl = appAPIHelp.upyunBase + videoRemotePath!
        shareObject.shareObject = shareVideoObject
        
        UMSocialManager.default().share(to: currentShareType!, messageObject: shareObject, currentViewController: nil) { [weak self] (_, error: Error?) in
            if error != nil{
                self?.errorClosure?(error)
            }else{
                self?.shareSuccess?()
            }
        }
    }
}
