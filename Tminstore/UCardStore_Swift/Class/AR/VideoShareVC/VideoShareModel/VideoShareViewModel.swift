//
//  VideoShareViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/12.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class VideoShareViewModel: BaseViewModel {

    fileprivate let dataModel: VideoShareDataModel = VideoShareDataModel()
    /// 发布成功
    var shareSuccess: (()->())?
    /// 使用系统自带
    var useSystemShare: ((_ videoRemotePath: String)->())?
    
    /// 需要分享的渠道
    var shareButtonType: ShareButtonType?{
        didSet{
            switch shareButtonType! {
            case ShareButtonType.weChatSpace:
                dataModel.currentShareType = UMSocialPlatformType.wechatTimeLine
            case ShareButtonType.weChatFriend:
                dataModel.currentShareType = UMSocialPlatformType.wechatSession
            case ShareButtonType.weiBo:
                dataModel.currentShareType = UMSocialPlatformType.sina
            default:
                dataModel.currentShareType = nil
            }
        }
    }
    
    /// 视频的备注
    var videoRemark: String = "" {
        didSet{
            dataModel.shareDescription = videoRemark
        }
    }
    
    override init() {
        super.init()
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            DispatchQueue.main.async {
                self?.errorClosure?(error, nil)
                self?.dismissSVProgress()
            }
        }
        
        //MARK: 上传成的闭包
        dataModel.uploadVideoSuccess = {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.dismissSVProgress()
                self?.dataModel.shareToThreePath()
                self?.dataModel.isVideoSaved = true
                return
            }
            DispatchQueue.main.async {
                self?.errorClosure?(nil, result)
                self?.dismissSVProgress()
            }
        }
        
        //MARK: 分享成功
        dataModel.shareSuccess = {[weak self] _ in
            DispatchQueue.main.async {
                self?.dismissSVProgress()
                self?.shareSuccess?()
            }
        }
        
        //MARK: 使用系统自带的分享功能
        dataModel.useSystemShare = {[weak self] (videoRemotePath: String) in
            DispatchQueue.main.async {
                self?.dismissSVProgress()
                self?.useSystemShare?(videoRemotePath)
            }
        }
        
        //MARK: 重新返回app
        app.appDidBecomeActive = { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 上传视频
    /// 上传视频
    ///
    /// - Parameter videoPathURLString: 视频本地路径
    func uploadVideo(videoPathURLString: String){
        showSVProgress(title: nil)
        doInBackground(inBackground: {[weak self] _ in
            self?.dataModel.uploadVideoInfo(videoPathURLString: videoPathURLString)
        })
    }
    
    //MARK: 分享到第三方平台
    /// 分享到第三方平台
    ///
    /// - Parameter recordModel: 录屏的模型
    func shareToThreePart(recordModel: RecordModel){
        showSVProgress(title: nil)
        dataModel.shareFromRecordVC(recordModel: recordModel)
    }
}
