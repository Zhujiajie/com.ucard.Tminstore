//
//  VideoReplyViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class VideoReplyViewModel: BaseViewModel {

    fileprivate let dataModel: VideoReplyDataModel = VideoReplyDataModel()
    fileprivate var noMoreDataFlag: Bool = false
    
    // 向服务器端请求第几页数据
    var pageNum: Int = 1{
        didSet{
            if pageNum == 1{
                noMoreDataFlag = false
            }
        }
    }
    /// 没有更多数据了
    var noMoreData: (()->())?
    /// 成功获得数据
    var tableViewShouldReload: (()->())?
    /// 出错的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    /// 视频回复的数组
    var commentModelArray: [CommentModel] = [CommentModel]()
    
    override init() {
        super.init()
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }
    }
    
    //MARK: 请求视频回复数据
    func getVideoCommentData(videoId: String){
        
        if noMoreDataFlag == true{
            noMoreData?()
            return
        }
        
        dataModel.requestVideoComment(pageIndex: pageNum, andVideoId: videoId, andSuccessClosure: {[weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let arComments: [[String: Any]] = data["arComments"] as? [[String: Any]]{
                        if arComments.isEmpty == true{
                            self?.noMoreDataFlag = true
                            self?.noMoreData?()
                        }else{
                            var commentArray: [CommentModel] = [CommentModel]()
                            for dic: [String: Any] in arComments{
                                if dic["contentUrl"] != nil{
                                    let commentModel: CommentModel = CommentModel.initFromDic(dic: dic)
                                    commentArray.append(commentModel)
                                }
                            }
                            if self?.pageNum == 1{
                                self?.commentModelArray.removeAll()
                            }
                            self?.commentModelArray.append(contentsOf: commentArray)
                            self?.tableViewShouldReload?()
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        })
    }
}
