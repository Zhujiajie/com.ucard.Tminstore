//
//  VideoReplyDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import Alamofire

class VideoReplyDataModel: BaseDataModel {

    //MARK: 展示视频回复
    /// 展示视频回复
    ///
    /// - Parameters:
    ///   - pageIndex: 页数
    ///   - commentId: 评论ID
    ///   - success: 成功的闭包
    func requestVideoComment(pageIndex: Int, andVideoId videoId: String, andSuccessClosure success: ((_ result: [String: Any])->())?){
        
        let parameters: [String: Any] = [
            "videoId": videoId,
            "pageIndex": pageIndex
        ]
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.showSpaceTimeCommentAPI, ParametersDictionary: parameters, urlEncoding: URLEncoding.default, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
