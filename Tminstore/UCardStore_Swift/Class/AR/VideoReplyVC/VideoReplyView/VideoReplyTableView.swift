//
//  VideoReplyTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MJRefresh

class VideoReplyTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        backgroundColor = UIColor.white
        
        register(VideoReplyTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.VideoReplyTableViewCellReuseIdentifier)
        separatorStyle = UITableViewCellSeparatorStyle.none
        estimatedRowHeight = 70/baseWidth
        
        mj_header = BaseAnimationHeader()
        mj_footer = BaseRefreshFooter()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
