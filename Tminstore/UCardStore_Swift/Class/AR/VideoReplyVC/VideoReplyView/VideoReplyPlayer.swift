//
//  VideoReplyPlayer.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class VideoReplyPlayer: UIView {
    
    var commentModel: CommentModel?{
        didSet{
            if commentModel != nil{
                setData()
            }
        }
    }
    
    /// 视频的完整路径
    var videoURL: URL?
    
    fileprivate var session: AVCaptureSession?
    
    /// 用户头像
    fileprivate var portraitView: UIImageView?
    /// 昵称
    fileprivate var nickLabel: UILabel?
    /// 时间Label
    fileprivate var createTimeLabel: UILabel?
    /// 展示视频缩略的imageView
    fileprivate var videoThumbnalImageView: UIImageView?
    /// 用来展示摄像头的layer
    fileprivate var cameraLayer: AVCaptureVideoPreviewLayer?
    /// 用来展示摄像头的layer
    fileprivate var player: ZTPlayer?
    /// 重新播放的按钮
    fileprivate var playAgainButton: UIImageView?
    /// 加载动画
    fileprivate var activeIndicator: UIActivityIndicatorView?
    
    /// 播放器的缩放比例
    fileprivate var lastCenter: CGPoint = CGPoint.zero
    
    /// 播放器的原始transform
    fileprivate var originalTransform: CGAffineTransform?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //视频缩略图
        videoThumbnalImageView = UIImageView()
        videoThumbnalImageView?.contentMode = UIViewContentMode.scaleAspectFit
        addSubview(videoThumbnalImageView!)
        videoThumbnalImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        //视频加载指示器
        activeIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        videoThumbnalImageView?.addSubview(activeIndicator!)
        activeIndicator?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(videoThumbnalImageView!)
        })
        activeIndicator?.hidesWhenStopped = true
        
        player = ZTPlayer()
        player?.view.frame = CGRect(x: 50/baseWidth, y: 0, width: screenWidth - 100/baseWidth, height: 400/baseHeight)
        player?.view.center = self.center
        player?.view.frame.origin.y -= 90/baseWidth
        player?.playbackLoops = true
        player?.playerDelegate = self
        player?.playbackDelegate = self
        player?.layerBackgroundColor = UIColor.clear//背景透明
        addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            self?.playAgainButton?.isHidden = false
        }
        
        lastCenter = player!.view.center
        originalTransform = player?.view.transform//保存原始的originalTransform
        
        //添加手势。控制播放暂停
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToControlPlayer(tap:)))
        player?.view.addGestureRecognizer(tap)
        
        //添加手势移动
        let pan: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(moveGesture(pan:)))
        player?.view.addGestureRecognizer(pan)
        
        //添加手势缩放
        let pinch: UIPinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(scaleGesture(pinch:)))
        player?.view.addGestureRecognizer(pinch)
        
        //旋转的手势
        let rotate: UIRotationGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(rotateGesture(rotate:)))
        player?.view.addGestureRecognizer(rotate)
        
        //重新开始播放的按钮
        playAgainButton = UIImageView(image: UIImage(named: "playButton"))
        playAgainButton?.contentMode = UIViewContentMode.scaleAspectFit
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 80/baseWidth, height: 80/baseWidth))
        })
        playAgainButton?.isHidden = true
        
        //视频信息
        portraitView = UIImageView()
        portraitView?.contentMode = UIViewContentMode.scaleAspectFill
        addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self).offset(-63/baseWidth)
            make.left.equalTo(self).offset(109/baseWidth)
            make.size.equalTo(CGSize(width: 46/baseWidth, height: 46/baseWidth))
        })
        portraitView?.layer.cornerRadius = 23/baseWidth
        portraitView?.layer.masksToBounds = true
        
        nickLabel = UILabel()
        nickLabel?.textColor = UIColor(hexString: "2DEAED")
        nickLabel?.font = UIFont.systemFont(ofSize: 14)
        addSubview(nickLabel!)
        nickLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.left.equalTo(portraitView!.snp.right).offset(10/baseWidth)
        })
        
        createTimeLabel = UILabel()
        createTimeLabel?.textColor = UIColor.white
        createTimeLabel?.font = UIFont.systemFont(ofSize: 12)
        addSubview(createTimeLabel!)
        createTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickLabel!.snp.bottom).offset(4/baseWidth)
            make.leftMargin.equalTo(nickLabel!)
        })
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {[weak self] _ in
            self?.getCameraPreview()
        }
    }
    
    //MARK: 重新开始播放播放器
    func replayPlayer(){
        if player?.playbackState == PlaybackState.paused{
            player?.playFromCurrentTime()
        }
    }
    
    //MARK: 停止获取摄像头数据
    /// 停止获取摄像头数据
    func stopSession(){
        if session?.isRunning == true{
            if let input: AVCaptureDeviceInput = session?.inputs.first as? AVCaptureDeviceInput{
                session?.removeInput(input)
                session?.stopRunning()
                session = nil
                cameraLayer?.removeFromSuperlayer()
                cameraLayer = nil
            }
        }
    }
    
    //MARK: 获取摄像头的捕捉到影像
    /**
     获取摄像头的捕捉到影像
     
     - parameter cell: PhotoCollectionViewCell
     */
    func getCameraPreview() {
        
        session = AVCaptureSession()
        session?.sessionPreset = AVCaptureSessionPreset1920x1080
        
        if let device: AVCaptureDevice = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).first as? AVCaptureDevice {
            
            do{
                let input: AVCaptureDeviceInput = try AVCaptureDeviceInput(device: device)
                session?.addInput(input)
                cameraLayer = AVCaptureVideoPreviewLayer(session: session)
                cameraLayer!.frame = self.bounds
                session?.startRunning()
                DispatchQueue.main.async(execute: {[weak self] _ in
                    self?.layer.insertSublayer((self?.cameraLayer)!, below: self?.player?.view.layer)//添加视图
                })
                
            }catch{print(error.localizedDescription) }
        }
    }
    
    //MARK: 暂停播放
    /// 暂停播放
    func pausePlay(){
        if player?.playbackState == PlaybackState.playing{
            player?.pause()
            playAgainButton?.isHidden = false
        }
    }
    
    //MARK: 开始播放
    /// 重新开始播放
    func rePlay(){
        if player?.playbackState == PlaybackState.paused && player?.url != nil{
            player?.playFromCurrentTime()
        }
    }
    
    //MARK: 点击开始播放，或者停止播放
    func tapToControlPlayer(tap: UITapGestureRecognizer){
        if player?.playbackState == PlaybackState.playing{
            player?.pause()
            playAgainButton?.isHidden = false
        }else if player?.playbackState == PlaybackState.paused{
            player?.playFromCurrentTime()
            playAgainButton?.isHidden = true
        }
    }
    
    //MARK: 拖动的手势
    func moveGesture(pan: UIPanGestureRecognizer){
        switch pan.state {
        case UIGestureRecognizerState.began, UIGestureRecognizerState.changed:
            if let view: UIView = pan.view{
                let movePoint: CGPoint = pan.translation(in: view)
                lastCenter = CGPoint(x: lastCenter.x + movePoint.x, y: lastCenter.y + movePoint.y)
                view.transform = view.transform.translatedBy(x: movePoint.x, y: movePoint.y)
            }
        default:
            
            //            //防止播放器移除屏幕范围
            //            if lastCenter.x < 0 || lastCenter.x > screenWidth || lastCenter.y < 0 || lastCenter.y > screenHeight{
            //                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            //                    self?.player?.view.transform = CGAffineTransform.identity
            //                    }, completion: {[weak self] _ in
            //                       self?.lastCenter = (self?.player?.view.center)!
            //                })
            //            }
            break
        }
        pan.setTranslation(CGPoint.zero, in: pan.view)
    }
    
    //MARK: 缩放手势
    func scaleGesture(pinch: UIPinchGestureRecognizer){
        switch pinch.state {
        case UIGestureRecognizerState.began, UIGestureRecognizerState.changed:
            if let view: UIView = pinch.view{
                view.transform = view.transform.scaledBy(x: pinch.scale, y: pinch.scale)
                pinch.scale = 1.0
            }
        default:
            if let view: UIView = pinch.view, let currentScale: CGFloat = view.layer.value(forKeyPath: "transform.scale") as? CGFloat{
                UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
                    if currentScale > 2.0{
                        view.transform = (self?.originalTransform)!.scaledBy(x: 2.0, y: 2.0)
                    }else if currentScale < 0.75{
                        view.transform = (self?.originalTransform)!.scaledBy(x: 0.75, y: 0.75)
                    }
                    }, completion: nil)
            }
        }
    }
    
    //MARK: 旋转的手势
    func rotateGesture(rotate: UIRotationGestureRecognizer){
        if let view: UIView = rotate.view{
            view.transform = view.transform.rotated(by: rotate.rotation)
        }
        rotate.rotation = 0//重置旋转角度
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        
        videoThumbnalImageView?.image = nil
        portraitView?.image = nil
        nickLabel?.text = nil
        createTimeLabel?.text = nil
        
        videoThumbnalImageView?.sd_setImage(with: commentModel?.videoThumbnailURL)
        
        if let url: URL = commentModel?.contentURL{
            playAgainButton?.isHidden = true
            player?.setVideoFullURL(fullVideoURL: url)
            player?.playFromBeginning()
        }
        
        portraitView?.sd_setImage(with: commentModel?.userLogo)
        nickLabel?.text = commentModel!.nickName
        
        if let date: Date = commentModel?.createDate{
            let formatter: DateFormatter = DateFormatter()
            formatter.dateStyle = DateFormatter.Style.medium
            formatter.timeStyle = DateFormatter.Style.none
            createTimeLabel?.text = formatter.string(from: date)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension VideoReplyPlayer: PlayerDelegate, PlayerPlaybackDelegate{
    func playerReady(_ player: Player) {
    }
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    //MARK: 根据缓存情况，展示加载提示器
    func playerBufferingStateDidChange(_ player: Player) {
        switch player.bufferingState {
        case BufferingState.ready:
            activeIndicator?.stopAnimating()
        default:
            activeIndicator?.startAnimating()
        }
    }
    
    func playerCurrentTimeDidChange(_ player: Player){
    }
    func playerPlaybackWillStartFromBeginning(_ player: Player){
    }
    func playerPlaybackDidEnd(_ player: Player){
    }
    func playerPlaybackWillLoop(_ player: Player){
    }
}
