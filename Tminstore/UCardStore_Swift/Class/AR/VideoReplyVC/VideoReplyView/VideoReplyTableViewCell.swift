//
//  VideoReplyTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class VideoReplyTableViewCell: UITableViewCell {

    /// 回复的数据源
    var commentModel: CommentModel?{
        didSet{
            setData()
        }
    }
    
    /// 点击头像的事件
    var tapUserPortraitView: ((_ memberCode: String)->())?
    
    fileprivate var portraitView: BaseImageView?
    fileprivate var nickNameLabel: UILabel?
    fileprivate var timeLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        portraitView = BaseImageView.userPortraitView(cornerRadius: 20/baseWidth)
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.top.equalTo(contentView).offset(14/baseWidth)
            make.bottom.equalTo(contentView).offset(-14/baseWidth)
            make.left.equalTo(contentView).offset(17/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        })
        portraitView?.addTapGesture()
        //MARK: 点击用户头像
        portraitView?.tapImageView = {[weak self] _ in
            self?.tapUserPortraitView?((self?.commentModel?.memberCode)!)
        }
        
        timeLabel = UILabel()
        timeLabel?.textColor = UIColor(hexString: "979797")
        timeLabel?.font = UIFont.systemFont(ofSize: 10)
        timeLabel?.textAlignment = NSTextAlignment.right
        contentView.addSubview(timeLabel!)
        timeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.right.equalTo(contentView).offset(-17/baseWidth)
        })
        
        nickNameLabel = UILabel()
        nickNameLabel?.font = UIFont.systemFont(ofSize: 14)
        nickNameLabel?.textColor = UIColor(hexString: "2DEAED")
        nickNameLabel?.numberOfLines = 1
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.left.equalTo(portraitView!.snp.right).offset(14/baseWidth)
            make.right.equalTo(timeLabel!.snp.left).offset(-5/baseWidth)
        })
        
        let infoLabel: UILabel = UILabel()
        infoLabel.textColor = UIColor(hexString: "737373")
        infoLabel.font = UIFont.systemFont(ofSize: 14)
        infoLabel.text = NSLocalizedString("的评论，点击查看", comment: "")
        contentView.addSubview(infoLabel)
        infoLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(5/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
        }
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "E9E9E9")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.right.equalTo(contentView)
            make.left.equalTo(contentView).offset(20/baseWidth)
            make.height.equalTo(0.5)
        }
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        portraitView?.sd_setImage(with: commentModel?.userLogo)
        nickNameLabel?.text = commentModel?.nickName
        if let date: Date = commentModel?.createDate{
            timeLabel?.text = getDateText(date: date)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        portraitView?.image = nil
        nickNameLabel?.text = nil
        timeLabel?.text = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
