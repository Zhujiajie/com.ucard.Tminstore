//
//  VideoReplyButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class VideoReplyButton: BaseButton {

    //MARK: 视频评论界面的，添加视频评论的按钮
    /// 视频评论界面的，添加视频评论的按钮
    ///
    /// - Returns: VideoReplyButton
    class func videoReplyButton()->VideoReplyButton{
        let button: VideoReplyButton = VideoReplyButton()
        button.backgroundColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        let attStr: NSAttributedString = NSAttributedString(string: "视频评论", attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        return button
    }
    

}
