//
//  VideoReplyController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class VideoReplyController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    fileprivate let userViewModel: PUserViewModel = PUserViewModel()
    fileprivate let viewModel: VideoReplyViewModel = VideoReplyViewModel()
    
    var videoId: String = ""
    
    fileprivate var addVideoReplyButton: VideoReplyButton?
    fileprivate var tableView: VideoReplyTableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexString: "#F2F5F7")
        setNavigationBar()
        
        addVideoReplyButton = VideoReplyButton.videoReplyButton()
        view.addSubview(addVideoReplyButton!)
        addVideoReplyButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(53/baseWidth)
        })
        
        tableView = VideoReplyTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.equalTo(view)
            make.top.equalTo(view).offset(6/baseWidth)
            make.bottom.equalTo(addVideoReplyButton!.snp.top)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
        
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cancelTansparentNavigationBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if viewModel.commentModelArray.isEmpty == true{
            tableView?.mj_header.beginRefreshing()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        cancelTansparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("backArrow")
        setNavigationTitle(NSLocalizedString("评论", comment: ""))//设置导航栏标题
    }
        
    //MARK: 刷新数据
    func refreshData() {
        viewModel.pageNum = 1
        viewModel.getVideoCommentData(videoId: videoId)
        tableView?.mj_footer.resetNoMoreData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.commentModelArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: VideoReplyTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.VideoReplyTableViewCellReuseIdentifier, for: indexPath) as! VideoReplyTableViewCell
        cell.commentModel = viewModel.commentModelArray[indexPath.row]
        
//        //MARK: 点击用户头像
//        cell.tapUserPortraitView = {[weak self] (memberCode: String) in
//            self?.userViewModel.requestOtherUserInfo(memberCode: memberCode)
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc: VideoReplyPlayerViewController = VideoReplyPlayerViewController()
        vc.commentModel = viewModel.commentModelArray[indexPath.row]
        _ = navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 点击添加视频评论的事件
        addVideoReplyButton?.touchButtonClosure = {[weak self] _ in
            let vc: VideoCommentController = VideoCommentController()
            vc.videoId = self?.videoId
            vc.isComment = true
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 下拉刷新
        tableView?.mj_header.refreshingBlock = {[weak self] _ in
            self?.refreshData()
        }
        
        //MARK: 上拉刷新
        tableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.getVideoCommentData(videoId: (self?.videoId)!)
        }
        
        //MARK: 成功获得数据
        viewModel.tableViewShouldReload = {[weak self] _ in
            self?.tableView?.mj_header.endRefreshing()
            self?.tableView?.mj_footer.endRefreshing()
            self?.tableView?.reloadData()
        }
        
        //MARK: 没有更多数据了
        viewModel.noMoreData = {[weak self] _ in
            self?.tableView?.mj_header.endRefreshing()
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.tableView?.mj_header.endRefreshing()
            self?.tableView?.mj_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 成功获得其他用户信息
        userViewModel.getOtherUserInfoSuccess = {[weak self] (userInfoModel: UserInfoModel) in
            let vc: PUserController = PUserController()
            vc.userInfoModel = userInfoModel
            vc.umengViewName = "其他用户信息"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 获取用户信息失败
        userViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
}
