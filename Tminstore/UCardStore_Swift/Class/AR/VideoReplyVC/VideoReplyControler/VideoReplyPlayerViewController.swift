//
//  VideoReplyPlayerViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class VideoReplyPlayerViewController: BaseViewController {

    var commentModel: CommentModel?
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        let player: VideoReplyPlayer = VideoReplyPlayer(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        player.frame.origin.y = -navBarHeight
        view.addSubview(player)
        player.commentModel = commentModel
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("whiteBackArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
    }
}
