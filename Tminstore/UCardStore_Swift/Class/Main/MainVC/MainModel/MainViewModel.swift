//
//  MainViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SDWebImage

class MainViewModel: BaseViewModel {

    fileprivate let dataModel: MainDataModel = MainDataModel()
    
    /// header停止刷新动画
    var headerStopRefresh: (()->())?
    
    /// footer停止刷新
    var footerStopRefresh: (()->())?
    
    /// 没有更多数据了
    var stopRefreshWithNoMoreData: (()->())?
    
    /// tableView需要重新加载数据
    var tableShouldReloadData: ((_ postcardArray: [PostcardModel])->())?
    
    /// 需要更新数据
    var updateData: ((_ indexPath: IndexPath, _ postcardArray: [PostcardModel])->())?
    
    /// tableView需要插入数据
    var tableViewShouleInsetData: ((_ beginIndex: Int, _ totalIndex: Int, _ postcardArray: [PostcardModel])->())?
    
    /// 点赞或取消点赞成功
    var sendLikeOrCancelLikeSuccess: ((_ isLiked: Bool, _ indexPath: IndexPath)->())?
    
    /// 检测是否有权限买图成功
    var checkPurchaseAvailableSuccess: ((_ indexPath: IndexPath)->())?
    
    /// 网络请求错误
    var netWorkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 发现未读消息
    var checkMessageStatusClosure: ((_ number: Int)->())?
    
    /// 向服务器端请求第几页数据
    var pageNum: Int = 1
    
    /// 存放数据的数组
    fileprivate var postcardArray: [PostcardModel] = [PostcardModel]()
    
    /// 是否需要停止上拉刷新
    fileprivate var stopGetMoreData = false
    
    /// 正在加载
    fileprivate var isRequestingData = false
    
    /// 未读消息数量
    fileprivate var unReadMessage: Int = 0
    
    //MARK: 上拉刷新
    /// 上拉刷新
    func pullToRefresh(){
        if stopGetMoreData == false{
            getDate()
        }
    }
    
    //MARK: 下拉刷新
    /// 下拉刷新
    func pushToRefresh() {
        pageNum = 1
        stopGetMoreData = false
        getDate()
    }
    
    //MARK: 获取数据
    /**
     获取社区主页数据
     
     - parameter success: 请求成功的闭包
     - parameter failure: 请求失败的闭包
     */
    fileprivate func getDate(){
        
        if isRequestingData == true{ return }
        isRequestingData = true
        
        dataModel.getMainPageData(pageIndex: pageNum, successClosure: { [weak self](result: [String: Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                
                let data: [String: Any] = result["data"] as! [String: Any]
                let communityList: [[String: Any]] = data["communityList"] as! [[String: Any]]
                // 没有更多数据了
                if communityList.isEmpty == true{
                    self?.stopGetMoreData = true
                    self?.stopRefreshWithNoMoreData?()
                }else{
                    
                    // 获得新数据
                    var newPostcardArray: [PostcardModel] = [PostcardModel]()
                    var placeHolderURLArray: [URL] = [URL]()
                    
                    for dic: [String: Any] in communityList{
                        let postcard: PostcardModel = PostcardModel.initFromDic(dic: dic)
                        newPostcardArray.append(postcard)
                        if let url: URL = postcard.frontPlaceImageURL{
                            placeHolderURLArray.append(url)
                        }//预先下载占位图片
                    }
                    SDWebImagePrefetcher.shared().prefetchURLs(placeHolderURLArray)
                    
                    // 下拉刷新
                    if self?.pageNum == 1{
                        
                        self?.postcardArray.removeAll()
                        
                        self?.postcardArray.append(contentsOf: newPostcardArray)
                        self?.dataModel.saveNewPostCard(postcardArray: (self?.postcardArray)!)//存储新数据
                        
                        self?.headerStopRefresh?()//顶部停止刷新
                    }
                    // 上拉刷新
                    else{
                        //传递新数据，并存储新数据
                        self?.postcardArray.append(contentsOf: newPostcardArray)
                        self?.footerStopRefresh?()//底部停止刷新
                    }
                    self?.tableShouldReloadData?((self?.postcardArray)!)
                    self?.pageNum += 1
                }
            }else{
                self?.netWorkError?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
                self?.netWorkError?(error, nil)
                self?.footerStopRefresh?()
                self?.headerStopRefresh?()
            }) { [weak self] _ in
                self?.dismissSVProgress()
                self?.isRequestingData = false
        }
    }
    
    //MARK: 更新评论数据
    /// 更新评论的数据
    func updateCommentData(indexPath: IndexPath){
        
        let pageIndex: Int = indexPath.row / 20 + 1
        
        dataModel.getMainPageData(pageIndex: pageIndex, successClosure: { [weak self] (result: [String: Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                let arrayIndex: Int = indexPath.row % 20
                let data: [String: Any] = result["data"] as! [String: Any]
                let newArray: [[String: Any]] = data["communityList"] as! [[String: Any]]//解析出数据
                
                let postcard: PostcardModel = PostcardModel.initFromDic(dic: newArray[arrayIndex])
                
                self?.postcardArray[indexPath.row] = postcard
                self?.updateData?(indexPath, (self?.postcardArray)!)
            }
        }, failureClosure: nil, completionClosure: nil)
    }
    
    //MARK: 从CoreData中获取已存储社区数据
    /**
     从CoreData中获取已存储社区数据
     
     - returns: [PostCardModel]?
     */
    func getDataFromCoreData(){
        
        var postcards: [PostcardModel] = [PostcardModel]()
        
        doInBackground(inBackground: { [weak self] _ in
            postcards = (self?.dataModel.getPostcardInfoFromCoreData())!
            }, backToMainQueue: {[weak self] _ in
                self?.postcardArray = postcards
                self?.tableShouldReloadData?(postcards)
                self?.headerStopRefresh?()
                self?.dismissSVProgress()
        })
        
//        doInBackground(inBackground: { [weak self] _ in
//            postcards = (self?.dataModel.getPostcardInfoFromCoreData())!
//        }) { [weak self] _ in
//            if postcards.isEmpty == true{
//                self?.pushToRefresh()
//            }else{
//                self?.postcardArray = postcards
//                self?.tableShouldReloadData?(postcards)
//                self?.headerStopRefresh?()
//                self?.dismissSVProgress()
//            }
//        }
    }
    
    //MARK: 发送点赞请求
    /// 发送点赞请求
    ///
    /// - Parameter originalId: originalId
    func sendLikeRequest(originalId: String, andIndexPath indexPath: IndexPath){
        dataModel.sendLikeRequest(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.sendLikeOrCancelLikeSuccess?(true, indexPath)
            }else{
                self?.netWorkError?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
            self?.netWorkError?(error, nil)
        }, completionClosure: nil)
    }
    
    //MARK: 发送取消点赞的请求
    /// 发送取消点赞的请求
    ///
    /// - Parameter originalId: originalId
    func sendCancelLikeRequest(originalId: String, andIndexPath indexPath: IndexPath){
        dataModel.sendCancelLikeRequest(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.sendLikeOrCancelLikeSuccess?(false, indexPath)
            }else{
                self?.netWorkError?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
            self?.netWorkError?(error, nil)
        }, completionClosure: nil)
    }
    
    //MARK: 检测是否有权限买图
    /// 检测是否有权限买图
    ///
    /// - Parameters:
    ///   - originalid: 原创ID
    ///   - indexPath: 数据来源
    func checkPurchaseAvaliable(originalId: String, andIndexPath indexPath: IndexPath){
        showSVProgress(title: nil)
        dataModel.checkPurchaseAvaliable(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.checkPurchaseAvailableSuccess?(indexPath)
            }else{
                self?.netWorkError?(nil, result)
            }
        }, failureClosure: { [weak self] (error: Error?) in
            self?.netWorkError?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 检查是否有未读消息
    /// 检查是否有未读消息
    func checkMessageStatus(){
        unReadMessage = 0
        //先检查推送
        dataModel.checkUnReadMessage(success: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                if let count: Int = data["count"] as? Int {
                    self?.unReadMessage += count
                    self?.checkSystemMessageStatus()//再检查系统消息
                }
            }
        }, failureClosure: nil)
    }
    
    //MARK: 检查是否有未读的系统消息
    fileprivate func checkSystemMessageStatus(){
        dataModel.checkUnreadSystemMessage(success: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                if let count: Int = data["count"] as? Int {
                    self?.unReadMessage += count
                    self?.checkMessageStatusClosure?((self?.unReadMessage)!)
                }
            }
        }, failureClosure: nil)
    }
}
