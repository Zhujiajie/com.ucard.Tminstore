//
//  MainDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

let PostcardInCommunityEntity: String = "PostcardInCommunity"
let CommentEntity: String = "Comment"

class MainDataModel: BaseDataModel {
    
    //MARK:获取社区主页数据
    /**
     获取社区主页数据
     
     - parameter pageNum:    要获取第几页数据
     - parameter success:    成功的闭包
     - parameter failure:    失败的闭包
     - parameter completion: 请求完成的闭包
     */
    func getMainPageData(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?) {
        
        let urlString: String = appAPIHelp.getCommunityDataAPI + "/\(pageIndex)"
        
        let parameter: [String: Any] = [
            "token": getUserToken()
            ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameter, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) { 
            completion?()
        }
        
//        let parameter: [String: Any] = [
//            "pageIndex": pageIndex
//            ] as [String: Any]
//        
//        sendGETRequestWithURLString(URLStr: getCommunityDataAPI, ParametersDictionary: parameter, urlEncoding: URLEncoding.default, successClosure: { (result: [String : Any]) in
//            success?(result)
//        }, failureClourse: { (error: Error?) in
//            failure?(error)
//        }, completionClosure: {
//            completion?()
//        })
    }
    
    //MARK: 点赞的请求
    /// 点赞的请求
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func sendLikeRequest(originalId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.sendLikeAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 发送取消点赞的请求
    /// 发送取消点赞的请求
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func sendCancelLikeRequest(originalId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId
            ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.sendCancelLikeAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 检测是否有权限买图
    /// 检测是否有权限买图
    ///
    /// - Parameters:
    ///   - originalid: 原创ID
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func checkPurchaseAvaliable(originalId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.purchaseAvaliableCheckAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 判断是否有未读系统消息
    /// 判断是否有未读系统消息
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    func checkUnReadMessage(success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.checkNoticeAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: nil)
    }
    
    //MARK: 判断是否有未读的系统消息
    /// 判断是否有未读的系统消息
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    func checkUnreadSystemMessage(success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.checkSystemNoticeAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: nil)
    }
    
    //MARK: 清除旧数据，并保存新数据
    /**
     清除旧数据，并保存新数据，下拉刷新时，调用该方法
     
     - parameter postcardArray: [PostCardModel]
     */
    func saveNewPostCard(postcardArray: [PostcardModel]) {
        self.deletePostcardDataInCoreData()
        delay(seconds: 2, completion: { [weak self] _ in
            self?.savePostcardModelsToCoreData(postcardArray: postcardArray)
        })
    }
    
    //MARK: 将新数据添加到Core Data中
    /**
     将新数据添加到Core Data中, 上拉刷新调用此方法
     
     - parameter postcardArray: [PostCardModel]
     */
    func addNewPostcardToCoreData(postcardArray: [PostcardModel]) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            self.savePostcardModelsToCoreData(postcardArray: postcardArray)
            }
    }
    
    //MARK: 将社区的明信片信息保存到Core Data中
    /**
     将社区的明信片信息保存到Core Data中
     
     - parameter postcardArray: [PostCardModel]
     */
    fileprivate func savePostcardModelsToCoreData(postcardArray: [PostcardModel]) {
        for postcard: PostcardModel in postcardArray {
            savePostcard(postcard: postcard)
        }
        app.saveContext()
    }
    
    //MARK: 存储postcard
    /**
     存储postcard
     
     - parameter postcard: PostCardModel
     */
    fileprivate func savePostcard(postcard: PostcardModel){
        
        let postcardData: PostcardInCommunity = NSEntityDescription.insertNewObject(forEntityName: PostcardInCommunityEntity, into: app.managedObjectContext) as! PostcardInCommunity
        
        postcardData.originalId = postcard.originalId
        postcardData.videoId = postcard.videoId
        postcardData.memberCode = postcard.memberCode
        postcardData.nickName = postcard.nickName
        postcardData.remark = postcard.remark
        postcardData.reviewStatus = postcard.reviewStatus.rawValue
        postcardData.isOnMarket = NSNumber(booleanLiteral: postcard.isOnMarket)
        postcardData.isPurchased = NSNumber(booleanLiteral: postcard.isPurchased)
        postcardData.pointPrice = Int32(postcard.pointPrice)
        postcardData.commentTotal = Int32(postcard.commentTotal)
        postcardData.createDate = postcard.createDate as NSDate?
        postcardData.updateDate = postcard.createDate as NSDate?
        postcardData.isSale = NSNumber(booleanLiteral: postcard.isSale)
        postcardData.frontImageURLStr = postcard.frontImageURL?.absoluteString
        postcardData.backImageURLStr = postcard.backImageURL?.absoluteString
        postcardData.targetId = postcard.targetId
        postcardData.photoLocation = postcard.photoLocation
        postcardData.country = postcard.country
        postcardData.province = postcard.province
        postcardData.city = postcard.city
        postcardData.district = postcard.district
        postcardData.detailedAddress = postcard.detailedAddress
        postcardData.isAR = NSNumber(booleanLiteral: postcard.isAR)
        postcardData.customerType = postcard.customerType.rawValue
        postcardData.userLogoURLStr = postcard.userLogoURL?.absoluteString
        postcardData.isLiked = NSNumber(booleanLiteral: postcard.isLiked)
        postcardData.likeCount = Int32(postcard.likeCount)
        
        var commentList: [Comment] = [Comment]()
        for comment: CommentModel in postcard.commentList {
            commentList.append(saveComment(comment: comment))
        }
        postcardData.commentList = NSSet(array: commentList)
    }
    
    //MARK: 存储评论
    /**
     存储评论
     
     - parameter commnet: CommentModel
     
     - returns: Comment
     */
    fileprivate func saveComment(comment: CommentModel) -> Comment{
        
        let commentToSave = NSEntityDescription.insertNewObject(forEntityName: CommentEntity, into: app.managedObjectContext) as! Comment
        
        commentToSave.commentId   = comment.commentId
        commentToSave.originalId  = comment.originalId
        commentToSave.memberCode  = comment.memberCode
        commentToSave.userLogoStr = comment.userLogo?.absoluteString
        commentToSave.nickName    = comment.nickName
        commentToSave.content     = comment.content
        commentToSave.status      = comment.status
        commentToSave.createDate  = comment.createDate as NSDate?
        commentToSave.updateDate  = comment.updateDate as NSDate?
        commentToSave.replyTotal  = Int32(comment.replyTotal)
        
        return commentToSave
    }
    
    //MARK: 删除已保存的CoreData数据
    /**
     删除已保存的CoreData数据
     */
    fileprivate func deletePostcardDataInCoreData() {
        
        do {
            let fetch: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: PostcardInCommunityEntity)
          
            if #available(iOS 9.0, *) {
                //批量删除
                let batchDeleteRequest: NSBatchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetch)
                batchDeleteRequest.resultType = NSBatchDeleteRequestResultType.resultTypeCount
                try app.managedObjectContext.execute(batchDeleteRequest)
                app.managedObjectContext.reset()
            } else {
                
                let fetchObjects: [NSManagedObject] = try app.managedObjectContext.fetch(fetch) as! [NSManagedObject]
                
                for item: NSManagedObject in fetchObjects {
                    app.managedObjectContext.delete(item)
                }
                
                app.saveContext()
            }
        }catch{ print(error.localizedDescription) }
    }
    
    //MARK: 从Core Data中取出社区明信片数据
    /**
     从Core Data中取出社区明信片数据
     
     - returns: [PostCardModel]
     */
    func getPostcardInfoFromCoreData() -> [PostcardModel] {
        
        var postcardArray: [PostcardModel] = [PostcardModel]()
        
        let fetch: NSFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: PostcardInCommunityEntity)
        
        let sortDescription: NSSortDescriptor = NSSortDescriptor(key: "createDate", ascending: false)//已ID倒序排列
        fetch.sortDescriptors = [sortDescription]
        
        do {
            let fetchObjects: [PostcardInCommunity] = try app.managedObjectContext.fetch(fetch) as! [PostcardInCommunity]
            
            for item: PostcardInCommunity in fetchObjects {
                let postcard: PostcardModel = PostcardModel.initFromCoreData(postcardData: item)
                if item.commentList != nil{
                    postcard.commentList = getCommentFromCoreData(commentDatas: item.commentList!)
                }
                postcardArray.append(postcard)
            }
        }catch{ print(error.localizedDescription)}
        
        return postcardArray
    }
    
    //MARK: 从Core Data中取出评论数据
    /**
     从Core Data中取出评论数据
     
     - parameter commentDatas: [Comment]
     
     - returns: [CommentModel]
     */
    fileprivate func getCommentFromCoreData(commentDatas: NSSet)->[CommentModel]{
        var commentArray: [CommentModel] = [CommentModel]()
        for commentData: NSFastEnumerationIterator.Element in commentDatas {
            let comment: CommentModel = CommentModel.initFromCoreData(commentObject: commentData as! Comment)
            commentArray.append(comment)
        }
        return commentArray
    }
}
