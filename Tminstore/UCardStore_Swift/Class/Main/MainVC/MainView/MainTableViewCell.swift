//
//  MainTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

class MainTableViewCell: UITableViewCell {
    
    var postcard: PostcardModel?{
        didSet{
            setData()//得到数据，开始设置UI
        }
    }
    
    /// 点击更多按钮的事件
    var touchMoreButtonClosure: ((_ postcard: PostcardModel)->())?
    
    /// 点击评论按钮，转到明信片详情界面
    var imageViewOrCommentClosure: ((_ originalId: String)->())?
    
    /// 点击拍图按钮的事件
    var touchPurchaseButtonClosure: (()->())?
    
    /// 点击点赞按钮的事件
    var touchLikeButtonClosure: (()->())?
    
    /// 用户头像
    fileprivate var portraitView: UIImageView?
    /// 用户昵称
    fileprivate var userNameLabel: UILabel?
    /// 明信片制作完成的时间
    fileprivate var createdTimeLabel: UILabel?
    
    /// 点击选择更多的按钮
    fileprivate var moreButton: UIButton?

    /// 图片标题的label
    fileprivate var frontImageRemarkLabel: UILabel?
    /// 承载图片和视频的collectionView
    fileprivate var collectionView: UICollectionView?
    /// 点赞按钮
    fileprivate var likeButton: UIButton?
    /// 买图按钮
    fileprivate var buyPhotoButton: UIButton?
    /// 评论按钮
    fileprivate var commentButton: UIButton?
    /// 展示点赞数的label
    fileprivate var likeCountLabel: UILabel?
    /// 展示页数的指示器
    fileprivate var pageControl: UIPageControl?
    /// 第一条评论
    fileprivate var commentLabel1: UILabel?
    /// 第二条评论
    fileprivate var commentLabel2: UILabel?
    /// 第三条评论
    fileprivate var commentLabel3: UILabel?
    /// 用于存放commentLabel的数组
    fileprivate var commentLabelArray: [UILabel]?
    /// 用户地理位置
    fileprivate var locationLabel: UILabel?
    /// 提示用户可以买图的视图
    fileprivate var purchaseInfoView: MainView?
    /// 将经纬度转换为详细地址
    fileprivate var geocoder: CLGeocoder?
    /// 是否要不响应点击点赞按钮的事件
    fileprivate var ignoreLikeAction: Bool = false
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        configCell()
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
    }

    //MARK:设置cell
    fileprivate func configCell() {

        //增加灰色分隔区间
        let grayLine1: UIView = UIView()
        grayLine1.backgroundColor = UIColor(hexString: "#F2F6F8")
        contentView.addSubview(grayLine1)
        grayLine1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.right.equalTo(contentView)
            make.height.equalTo(4/baseWidth)
        }
        
        //头像
        portraitView                     = UIImageView()
        portraitView?.contentMode         = UIViewContentMode.scaleAspectFill
        portraitView?.layer.cornerRadius  = 43/baseWidth/2
        portraitView?.layer.masksToBounds = true
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayLine1).offset(15/baseWidth)
            make.left.equalTo(contentView).offset(14/baseWidth)
            make.size.equalTo(CGSize(width: 43/baseWidth, height: 43/baseWidth))
        })
        
        // 更多按钮
        let moreButtonImageView: UIImageView = UIImageView(image: UIImage(named: "moreButton"))
        contentView.addSubview(moreButtonImageView)
        moreButtonImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(grayLine1).offset(29/baseWidth)
            make.right.equalTo(contentView).offset(-14/baseWidth)
        }
        moreButton = UIButton()
        moreButton?.backgroundColor = UIColor.clear
        contentView.addSubview(moreButton!)
        moreButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(11/baseWidth)
            make.right.equalTo(contentView).offset(-11/baseWidth)
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 60/baseWidth))
        })
        moreButton?.addTarget(self, action: #selector(touchMoreButton(button:)), for: UIControlEvents.touchUpInside)
        
        //设置昵称
        userNameLabel                = UILabel()
        userNameLabel?.textColor     = UIColor(hexString: "6B6B6B")
        userNameLabel?.font          = UIFont.systemFont(ofSize: 20)
        userNameLabel?.textAlignment = NSTextAlignment.left
        contentView.addSubview(userNameLabel!)
        userNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(11/baseWidth)
            make.left.equalTo(portraitView!.snp.right).offset(8/baseWidth)
            make.right.equalTo(moreButton!.snp.left)
            make.height.equalTo(23/baseWidth)
        })
        
        //明信片制作完成的时间
        createdTimeLabel            = UILabel()
        createdTimeLabel?.textColor = UIColor(hexString: "878686")
        createdTimeLabel?.font      = UIFont.systemFont(ofSize: 11)
        contentView.addSubview(createdTimeLabel!)
        createdTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(userNameLabel!)
            make.bottomMargin.equalTo(portraitView!)
            make.height.equalTo(13/baseWidth)
        })
        
        // 正面图片的标题
        frontImageRemarkLabel = UILabel()
        frontImageRemarkLabel?.textColor = UIColor(hexString: "242424")
        frontImageRemarkLabel?.numberOfLines = 1
        if #available(iOS 8.2, *) {
            frontImageRemarkLabel?.font = UIFont.systemFont(ofSize: 13, weight: UIFontWeightLight)
        } else {
            frontImageRemarkLabel?.font = UIFont.systemFont(ofSize: 13)
        }
        contentView.addSubview(frontImageRemarkLabel!)
        frontImageRemarkLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(portraitView!.snp.bottom).offset(4/baseHeight)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
        })

        collectionView = MainCollectionView()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(frontImageRemarkLabel!.snp.bottom).offset(8/baseWidth)
            make.left.right.equalTo(contentView)
            make.height.equalTo(266/baseWidth)
        }
        
        //买图的按钮
        buyPhotoButton = UIButton()
        buyPhotoButton!.setImage(UIImage(named: "buyPhoto"), for: UIControlState.normal)
        contentView.addSubview(buyPhotoButton!)
        buyPhotoButton!.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom).offset(15/baseWidth)
            make.left.equalTo(contentView).offset(14/baseWidth)
            make.height.equalTo(22/baseWidth)
            make.width.equalTo(22/baseWidth)
        })
        buyPhotoButton?.addTarget(self, action: #selector(touchPurchaseButton(button:)), for: UIControlEvents.touchUpInside)
        
        //评论按钮
        commentButton = UIButton()
        commentButton!.setImage(UIImage(named: "comment"), for: UIControlState.normal)
        contentView.addSubview(commentButton!)
        commentButton!.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(buyPhotoButton!)
            make.left.equalTo(buyPhotoButton!.snp.right).offset(20/baseWidth)
            make.size.equalTo(buyPhotoButton!)
        })
        commentButton?.addTarget(self, action: #selector(touchCommentButton(_:)), for: UIControlEvents.touchUpInside)
        
        //点赞按钮
        likeButton = UIButton()
        likeButton?.imageView?.contentMode = UIViewContentMode.scaleAspectFill
        likeButton?.imageView?.clipsToBounds = false
        setLikeButtonStatus(isLiked: false)
        contentView.addSubview(likeButton!)
        likeButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(buyPhotoButton!)
            make.left.equalTo(commentButton!.snp.right).offset(20/baseWidth)
            make.size.equalTo(buyPhotoButton!)
        })
        likeButton?.addTarget(self, action: #selector(touchLikeButton(button:)), for: UIControlEvents.touchUpInside)
        
        
        // 页数指示器
        pageControl = UIPageControl()
        pageControl?.numberOfPages = 2
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "60DBE8")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D6D6D6")
        contentView.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(buyPhotoButton!)
            make.centerX.equalTo(contentView)
        })
        
        //地理位置
        locationLabel           = UILabel()
        locationLabel!.textColor     = UIColor(hexString: "8C8A8A")
        locationLabel!.font          = UIFont.systemFont(ofSize: 14)
        locationLabel!.numberOfLines = 1
        locationLabel!.textAlignment = NSTextAlignment.right
        contentView.addSubview(locationLabel!)
        locationLabel!.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(commentButton!)
            make.right.equalTo(contentView).offset(-13/baseWidth)
            make.height.equalTo(20)
        })
        
        likeCountLabel = UILabel()
        likeCountLabel?.textColor = UIColor(hexString: "838383")
        likeCountLabel?.font = UIFont.systemFont(ofSize: 12)
        contentView.addSubview(likeCountLabel!)
        likeCountLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(buyPhotoButton!.snp.bottom).offset(6/baseWidth)
            make.left.equalTo(contentView).offset(15/baseWidth)
            make.height.equalTo(17/baseWidth)
        })
        
        //第一条评论Label
        commentLabel1                = commmentLabel()
        contentView.addSubview(commentLabel1!)
        commentLabel1!.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(likeCountLabel!.snp.bottom).offset(6/baseHeight)
            make.left.equalTo(contentView).offset(16/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
        })
        
        //第二条评论Label
        commentLabel2                = commmentLabel()
        contentView.addSubview(commentLabel2!)
        commentLabel2!.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(commentLabel1!.snp.bottom).offset(6/baseHeight)
            make.left.equalTo(contentView).offset(16/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
            
        })
        
        //第三条评论Label
        commentLabel3                = commmentLabel()
        contentView.addSubview(commentLabel3!)
        commentLabel3!.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(commentLabel2!.snp.bottom).offset(6/baseHeight)
            make.left.equalTo(contentView).offset(16/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
        })
            
        commentLabelArray = [commentLabel1!, commentLabel2!, commentLabel3!]
        
        //增加灰色分隔区间
        let grayLine2: UIView = UIView()
        grayLine2.backgroundColor = UIColor(hexString: "#F2F6F8")
        contentView.addSubview(grayLine2)
        grayLine2.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(commentLabel3!.snp.bottom).offset(2/baseWidth)
            make.left.right.equalTo(contentView)
            make.height.equalTo(4/baseWidth)
            make.bottom.equalTo(contentView)
        }
        
        //MARK: 第一次打开APP提示用户，可以买图
        if hasUserLoadAPP() == false{
            setUserHasLoadAPP()//设置用户已经登陆过APP了
            purchaseInfoView = MainView.purchaseIntroView()
            contentView.addSubview(purchaseInfoView!)
            purchaseInfoView?.snp.makeConstraints { (make: ConstraintMaker) in
                make.bottom.equalTo(buyPhotoButton!.snp.top)
                make.left.equalTo(contentView).offset(12/baseWidth)
            }
        }
    }
    
    //设置数据
    fileprivate func setData() {
        
        collectionView?.reloadData()//重新载入数据
        
        //设置头像
        if postcard!.userLogoURL != nil {
            portraitView?.sd_setImage(with: postcard!.userLogoURL!, placeholderImage: UIImage(named: "defaultLogo"))//设置头像
        }
        // 设置昵称
        userNameLabel?.text = postcard!.nickName
        // 设置发布时间
        if postcard?.createDate != nil{
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.none
            let dateString = dateFormatter.string(from: postcard!.createDate!)
            createdTimeLabel?.text = dateString
        }
        
        // 图片标题
        frontImageRemarkLabel?.text = postcard?.remark
        
        //设置城市
        if postcard?.city != ""{
            locationLabel?.text = postcard?.city
        }else{
            doInBackground(inBackground: {[weak self] _ in
                // 将经纬度转换为详细的地址
                if self?.geocoder == nil{
                    self?.geocoder = CLGeocoder()
                }
                if let coordinate: CLLocation = self?.postcard?.photoLocation{
                    self?.geocoder?.reverseGeocodeLocation(coordinate, completionHandler: { [weak self] ( results:[CLPlacemark]?, error: Error?) in
                        if let result: CLPlacemark = results?.first, error == nil {
                            //                    print(result)
                            if let subLocation: String = result.locality{
                                DispatchQueue.main.async {
                                    self?.locationLabel?.text = subLocation
                                    self?.postcard?.city = subLocation
                                }
                            }
                        }
                    })
                }
            })
        }
        
        setLikeButtonStatus(isLiked: postcard!.isLiked)//是否已点赞
        
        likeCountLabel?.text = "\(postcard!.orderTotal)" + NSLocalizedString("次购买", comment: "") + "     \(postcard!.likeCount)" + NSLocalizedString("次赞", comment: "")//点赞数
        
        //设置评论内容
        if postcard?.commentList.isEmpty == false {
            
            var i: Int = 0
            
            for comment: CommentModel in postcard!.commentList {
                
                let commentName: NSAttributedString = NSAttributedString(string: comment.nickName)
                let commentContent: NSAttributedString = NSAttributedString(string: comment.content)
                let range1: NSRange = NSRange(location: 0, length: commentName.length)// 评论人的昵称
                let range2: NSRange = NSRange(location: commentName.length + 2, length: commentContent.length)// 评论内容
                
                
                let str: NSMutableAttributedString = NSMutableAttributedString(string: comment.nickName + "  \(comment.content)")
                str.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 14)] as [String: Any], range: range1)
                str.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 13)] as [String: Any], range: range2)
                str.addAttributes([NSForegroundColorAttributeName: UIColor(hexString: "#353535")] as [String: Any], range: range1)
                str.addAttributes([NSForegroundColorAttributeName: UIColor(hexString: "#828080")] as [String: Any], range: range2)
                
                commentLabelArray![i].attributedText = str
                
                i += 1
            }
        }
    }
    
    //MARK: 点击评论按钮的事件
    func touchCommentButton(_ button: UIButton) {
        imageViewOrCommentClosure?(postcard!.originalId)
    }
    
    //MARK: 点击买图按钮的事件
    func touchPurchaseButton(button: UIButton){
        touchPurchaseButtonClosure?()
        if purchaseInfoView != nil{
            purchaseInfoView?.dismissAnimation()
        }
    }
    
    //MARK: 点击点赞按钮的事件
    func touchLikeButton(button: UIButton){
        if ignoreLikeAction == false{
            ignoreLikeAction = true
            touchLikeButtonClosure?()
            //一秒钟之后，才能重新点击该按钮
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(enableLikeButton(timer:)), userInfo: nil, repeats: false)
        }
    }
    
    //MARK: 重新启用点赞
    func enableLikeButton(timer: Timer){
        ignoreLikeAction = false
    }
    
    //MARK: 点击更多按钮的事件
    func touchMoreButton(button: UIButton){
        touchMoreButtonClosure?(postcard!)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        portraitView?.image         = nil
        userNameLabel?.text         = nil
        createdTimeLabel?.text      = nil
        frontImageRemarkLabel?.text = nil
        locationLabel?.text         = nil
        commentLabel1?.text         = nil
        commentLabel2?.text         = nil
        commentLabel3?.text         = nil
        collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: false)
        pageControl?.currentPage = 0
    }
    
    //MARK: 展示评论内容的Label
    fileprivate func commmentLabel()->UILabel{
        let commentLabel: UILabel = UILabel()
        commentLabel.textAlignment = NSTextAlignment.left
        commentLabel.numberOfLines = 3
        commentLabel.isUserInteractionEnabled = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapCommentLabel(tap:)))
        commentLabel.addGestureRecognizer(tap)
        return commentLabel
    }
    
    //MARK: 点击评论Label
    func tapCommentLabel(tap: UITapGestureRecognizer){
        imageViewOrCommentClosure?(postcard!.originalId)
    }
    
    //MARK: 暂停视频播放
    func pauseVideo() {
        if let cell: MainCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? MainCollectionViewCell{
            cell.pausePlayer()
        }
    }
    
    //MARK: 设置点赞按钮的状态
    /// 设置点赞按钮的状态
    ///
    /// - Parameters:
    ///   - isLiked: 点赞或取消点赞
    ///   - should: 是否改变点赞数，默认不改变
    func setLikeButtonStatus(isLiked: Bool, shouldChangeLikeCount should: Bool = false){
        if isLiked == true{
            likeButton?.setImage(UIImage(named: "liked"), for: UIControlState.normal)
            if should == true{
                postcard?.likeCount += 1
                likeCountLabel?.text = "\(postcard!.orderTotal)" + NSLocalizedString("次购买", comment: "") + "     \(postcard!.likeCount)" + NSLocalizedString("次赞", comment: "")//点赞数
                
                //向下位移，并缩小
                var transform1: CGAffineTransform = likeButton!.transform
                transform1 = transform1.scaledBy(x: 0.5, y: 0.5)
                transform1 = transform1.translatedBy(x: 0, y: 10)
                
                //向上位移，并放大
                var transform2: CGAffineTransform = likeButton!.transform
                transform2 = transform2.scaledBy(x: 1.5, y: 1.5)
                
                UIView.animateKeyframes(withDuration: 0.6, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModePaced, animations: { [weak self] _ in
                    
                    //向下移，并缩小
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.3, animations: { 
                        self?.likeButton?.transform = transform1
                    })
                    //上移并放大
                    UIView.addKeyframe(withRelativeStartTime: 0.4, relativeDuration: 0.3, animations: { 
                        self?.likeButton?.transform = transform2
                    })
                    //回复原始
                    UIView.addKeyframe(withRelativeStartTime: 0.7, relativeDuration: 0.4, animations: {
                        self?.likeButton?.transform = CGAffineTransform.identity
                    })
                    
                }, completion: nil)
            }
        }else{
            likeButton?.setImage(UIImage(named: "likeButton"), for: UIControlState.normal)
            if should == true{
                postcard?.likeCount -= 1
                likeCountLabel?.text = "\(postcard!.orderTotal)" + NSLocalizedString("次购买", comment: "") + "     \(postcard!.likeCount)" + NSLocalizedString("次赞", comment: "")//点赞数
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MainTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    //MARK: 设置cell的内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.row {
        case 0:
            let cell: MainImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MainImageCollectionViewCellReuseIdentifier, for: indexPath) as! MainImageCollectionViewCell
            cell.placeholerImageURL = postcard?.frontPlaceImageURL
            cell.imageURL = postcard?.frontImageURL
            return cell
        default:
            let cell: MainCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MainCollectionViewCellReuseIdentifier, for: indexPath) as! MainCollectionViewCell
            cell.videoId = postcard!.videoId
            return cell
        }
    }
    
    //MARK: 不显示Cell时。需要暂停播放视频
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell: MainCollectionViewCell = cell as? MainCollectionViewCell{
            cell.pausePlayer(hidePlayButton: true)
        }
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == screenWidth{
            if let cell: MainCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? MainCollectionViewCell{
                cell.playVideo()
            }
        }
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
    }
}
