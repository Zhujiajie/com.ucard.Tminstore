//
//  MainTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/13.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import MJRefresh

class MainTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        backgroundColor = UIColor(hexString: "#F2F6F8")
        
        register(MainTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MainTableViewCellReuseIdentifier)
        
        separatorStyle = UITableViewCellSeparatorStyle.none//除去分割线
        
//        contentInset = UIEdgeInsets(top: 4.5, left: 0, bottom: 0, right: 0)
        
        alwaysBounceVertical = false
        estimatedRowHeight = 600/baseWidth
        
        //设置上拉刷新
        self.mj_footer                          = BaseRefreshFooter()
        
        //设置下拉刷新
        self.mj_header = BaseAnimationHeader()
        
//        //设置footerView
//        let view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: tabbarHeight + 20/baseHeight))
//        view.backgroundColor = UIColor.clear
//        self.tableFooterView = view
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
