//
//  MainCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/2.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MainCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: screenWidth, height: 266/baseWidth)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        super.init(frame: frame, collectionViewLayout: layout)
        
        backgroundColor = UIColor.white
        isPagingEnabled = true
        register(MainCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.MainCollectionViewCellReuseIdentifier)
        register(MainImageCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.MainImageCollectionViewCellReuseIdentifier)
        
        //MARK: 第一次打开APP提示用户，滑动图片
        if hasUserLoadAPP() == false{
            // 提示用户滑动查看更多的视图
            let infoView: MainView = MainView.swipeInfoView()
            addSubview(infoView)
            infoView.snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(self).offset(220/baseWidth)
                make.left.equalTo(self).offset(200/baseWidth)
            })
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
