//
//  MainView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/13.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MainView: UIView {
    
    /// 底部的黑色线条
    var blackLine: UIView?
    
    /// 标题
    var titleImageView: UIImageView?
    
    /// 点击了购买按钮
    var touchPurchaseButtonClosure: (()->())?
    
    /// 顶部的为导航栏
    ///
    /// - returns: MainView
    class func topHeaderView()->MainView{
        
        let topHeaderView: MainView = MainView()
        topHeaderView.backgroundColor = UIColor.white
        
        //标题
        topHeaderView.titleImageView = UIImageView()
        topHeaderView.titleImageView?.contentMode = UIViewContentMode.scaleAspectFit
        topHeaderView.titleImageView?.image = UIImage(named: "TimeoryTitle")
        topHeaderView.addSubview(topHeaderView.titleImageView!)
        topHeaderView.titleImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(topHeaderView).offset(-9)
            make.centerX.equalTo(topHeaderView)
            make.size.equalTo(CGSize(width: 74, height: 21))
        })
//        topHeaderView.titleLable = UILabel()
//        topHeaderView.titleLable?.textAlignment = NSTextAlignment.center
//        topHeaderView.addSubview(topHeaderView.titleLable!)
//        topHeaderView.titleLable?.snp.makeConstraints { (make: ConstraintMaker) in
//            make.centerX.equalTo(topHeaderView)
//            make.centerY.equalTo(topHeaderView).offset(9.5)
//        }
//        let attDic: [String: Any] = [NSFontAttributeName: UIFont.init(name: LobsterTwo_Bold, size: 26)!, NSForegroundColorAttributeName: appThemeColor()] as [String: Any]
//        topHeaderView.titleLable?.attributedText = NSAttributedString(string: "Timeory", attributes: attDic)
        
        //底部黑色线条
        topHeaderView.blackLine = UIView()
        topHeaderView.blackLine?.backgroundColor = UIColor.lightGray
        topHeaderView.addSubview(topHeaderView.blackLine!)
        topHeaderView.blackLine?.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.equalTo(topHeaderView)
            make.right.equalTo(topHeaderView)
            make.bottom.equalTo(topHeaderView)
            make.height.equalTo(0.4)
        }
        
        return topHeaderView
    }

    
    /// 点击图片看视频的提示
    ///
    /// - returns: MainView
    class func setTouchIntroView()->MainView{
        
        let introView: MainView = MainView()
        introView.backgroundColor = UIColor.clear//view颜色为透明
        
        let blur: UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let visualView: UIVisualEffectView = UIVisualEffectView(effect: blur)
        introView.addSubview(visualView)
        visualView.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(introView)
        }
        visualView.layer.cornerRadius = 15
        visualView.layer.masksToBounds = true
        
        let introLabel: UILabel = UILabel()
        introLabel.textColor = UIColor.white
        if #available(iOS 8.2, *) {
            introLabel.font = UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium)
        } else {
            introLabel.font = UIFont.systemFont(ofSize: 18)
        }
        introLabel.numberOfLines = 2
        introLabel.textAlignment = NSTextAlignment.center
        introLabel.text = NSLocalizedString("长按图片2秒钟\n让时光记忆动起来", comment: "")
        visualView.contentView.addSubview(introLabel)
        introLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(visualView).offset(25)
            make.width.equalTo(visualView)
            make.height.equalTo(45)
            make.centerX.equalTo(visualView)
        }
        
        let ringView: UIImageView = UIImageView(image: UIImage(named: "touchRing"))
        visualView.contentView.addSubview(ringView)
        ringView.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerX.equalTo(visualView)
            make.top.equalTo(visualView).offset(83)
            make.size.equalTo(CGSize(width: 42, height: 42))
        }
        
        let fingerView: UIImageView = UIImageView(image: UIImage(named: "finger"))
        introView.addSubview(fingerView)
        fingerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(introView).offset(105)
            make.left.equalTo(introView).offset(73)
            make.size.equalTo(CGSize(width: 68, height: 90))
        }
        
        introView.add3DAnimation(layer: fingerView.layer)
        
        //点击消失
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: introView, action: #selector(introView.tapIntroView(_:)))
        tap.numberOfTapsRequired = 1
        introView.addGestureRecognizer(tap)
        
        //动画显示3秒
        Timer.scheduledTimer(timeInterval: 5.0, target: introView, selector: #selector(introView.dismissIntroView(_:)), userInfo: nil, repeats: false)
        
        return introView
    }
    
    /// 给提示框增加3D动画效果
    ///
    /// - parameter layer: CALayer
    fileprivate func add3DAnimation(layer: CALayer){
        
        var imageTransform: CATransform3D = CATransform3DIdentity
        
        imageTransform = CATransform3DTranslate(imageTransform, 0, 0, 50)
        imageTransform = CATransform3DScale(imageTransform, 1.2, 1.2, 1.2)
        
        let animation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "transform")
        animation.duration = 1.5
        animation.repeatCount = Float.infinity
        animation.values = [NSValue(caTransform3D: imageTransform), NSValue(caTransform3D: layer.transform), NSValue(caTransform3D: layer.transform), NSValue(caTransform3D: imageTransform)]
        animation.keyTimes = [0.0, 0.25, 0.75, 1.0]
        
        layer.add(animation, forKey: nil)
    }
    
    //MARK: 点击消失
    func tapIntroView(_ tap: UITapGestureRecognizer){
        dismissAnimation()
    }
    
    //MARK: 计时器，消失，并移除
    func dismissIntroView(_ timer: Timer) {
        dismissAnimation()
    }
    
    //MARK: 消失的动画
    /// 消失的动画
    func dismissAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            self.alpha = 0
        }) { (_) in
            self.layer.removeAllAnimations()
            self.removeFromSuperview()
        }
    }
    
    //MARK: 提示用户滑动查看更多的View
    /// 提示用户滑动查看更多的View
    ///
    /// - Returns: MainView
    class func swipeInfoView()->MainView{
        let mainView: MainView = MainView()
        mainView.backgroundColor = UIColor(white: 0.0, alpha: 0.57)
        mainView.layer.cornerRadius = 2
        mainView.layer.masksToBounds = true
        
        let label: UILabel = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont.systemFont(ofSize: 12)
        label.text = NSLocalizedString("滑动查看更多内容", comment: "")
        mainView.addSubview(label)
        label.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(mainView).inset(UIEdgeInsets(top: 4, left: 6, bottom: 4, right: 6))
        }
        
        //动画显示3秒
        Timer.scheduledTimer(timeInterval: 5.0, target: mainView, selector: #selector(mainView.dismissIntroView(_:)), userInfo: nil, repeats: false)
        
        return mainView
    }
    
    //MARK: 提示用户买图的视图
    /// 提示用户买图的视图
    ///
    /// - Returns: MainView
    class func purchaseIntroView()->MainView{
        
        let mainView: MainView = MainView()
        mainView.backgroundColor = UIColor.clear
        
        let imageView: UIImageView = UIImageView(image: UIImage(named: "purchaseIntro"))
        imageView.contentMode = UIViewContentMode.scaleToFill
        mainView.addSubview(imageView)
        imageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(mainView)
        }
        
        let infoLabel: UILabel = UILabel()
        infoLabel.textColor = UIColor(hexString: "8A8A8A")
        infoLabel.font = UIFont.systemFont(ofSize: 10)
        infoLabel.text = NSLocalizedString("点击购买图片\n制作实体AR卡", comment: "")
        infoLabel.textAlignment = NSTextAlignment.center
        infoLabel.numberOfLines = 2
        mainView.addSubview(infoLabel)
        infoLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(mainView).inset(UIEdgeInsets(top: 3, left: 12, bottom: 10, right: 12))
        }
        
        //动画显示3秒
//        Timer.scheduledTimer(timeInterval: 5.0, target: mainView, selector: #selector(mainView.dismissIntroView(_:)), userInfo: nil, repeats: false)
        
        return mainView
    }
    
    //MARK: 买图的确认视图
    /// 买图的确认视图
    ///
    /// - Parameters:
    ///   - portraitURL: 头像的路径
    ///   - frame: 大小
    /// - Returns: MainView
    class func purchaseConfirmView(portraitURL: URL?, andFrame frame: CGRect)->MainView{
        
        let mainView: MainView = MainView(frame: frame)
        mainView.backgroundColor = UIColor(white: 0.0, alpha: 0.35)
        
        mainView.alpha = 0
        
        //点击退出
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: mainView, action: #selector(mainView.tapIntroView(_:)))
        mainView.addGestureRecognizer(tap)
        
        //底部白色视图
        let containerView: UIView = UIView()
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 8
        containerView.layer.masksToBounds = true
        mainView.addSubview(containerView)
        containerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerX.equalTo(mainView)
            make.top.equalTo(mainView).offset(180/baseWidth)
            make.size.equalTo(CGSize(width: 274/baseWidth, height: 252/baseWidth))
        }
        
        //头像
        let portraitView: UIImageView = UIImageView()
        portraitView.contentMode = UIViewContentMode.scaleToFill
        portraitView.sd_setImage(with: portraitURL)
        containerView.addSubview(portraitView)
        portraitView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(containerView).offset(20/baseWidth)
            make.centerX.equalTo(containerView)
            make.size.equalTo(CGSize(width: 43/baseWidth, height: 43/baseWidth))
        }
        portraitView.layer.cornerRadius = 43/baseWidth/2
        portraitView.layer.masksToBounds = true
        
        //文案
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "7A7979")
        label.font = UIFont.systemFont(ofSize: 18)
        label.textAlignment = NSTextAlignment.center
        label.numberOfLines = 0
        label.text = NSLocalizedString("是否将他（她）的时光记忆做成一张AR实体卡片？\n时记会为您打印寄送哦！", comment: "")
        containerView.addSubview(label)
        label.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.equalTo(containerView).offset(20/baseWidth)
            make.right.equalTo(containerView).offset(-20/baseWidth)
            make.centerY.equalTo(containerView)
        }
        
        //确认的按钮
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor(hexString: "77E0EC")
        button.layer.cornerRadius = 3
        button.layer.masksToBounds = true
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 16), NSForegroundColorAttributeName: UIColor.white]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("购买", comment: ""), attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        containerView.addSubview(button)
        button.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerX.equalTo(containerView)
            make.bottom.equalTo(containerView).offset(-22/baseWidth)
            make.size.equalTo(CGSize(width: 228/baseWidth, height: 36/baseWidth))
        }
        button.addTarget(mainView, action: #selector(mainView.touchPurchaseButton(button:)), for: UIControlEvents.touchUpInside)
        
        //退出按钮
        let dismissImageView: UIImageView = UIImageView(image: UIImage(named: "purchaseDismiss"))
        mainView.addSubview(dismissImageView)
        dismissImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(containerView.snp.bottom).offset(19/baseWidth)
            make.centerX.equalTo(containerView)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        }
        dismissImageView.layer.cornerRadius = 36/baseWidth/2
        dismissImageView.layer.masksToBounds = true
        
        //渐变和缩放动画
        containerView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            mainView.alpha = 1
            containerView.transform = CGAffineTransform.identity
        }, completion: nil)
        
        return mainView
    }
    
    //MARK: 发布到地图成功之后的弹窗
    class func releareToMapSuccessAlert(frame: CGRect)->MainView{
        
        let mainView: MainView = MainView(frame: frame)
        mainView.backgroundColor = UIColor(white: 0.0, alpha: 0.35)
        
        mainView.alpha = 0
        
//        //点击退出
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: mainView, action: #selector(mainView.tapIntroView(_:)))
//        mainView.addGestureRecognizer(tap)
        
        //底部白色视图
        let containerView: UIView = UIView()
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 5
        containerView.layer.masksToBounds = true
        mainView.addSubview(containerView)
        containerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerX.equalTo(mainView)
            make.top.equalTo(mainView).offset(180/baseWidth)
            make.size.equalTo(CGSize(width: 271/baseWidth, height: 259/baseWidth))
        }
        
        //成功的图片
        let imageView: UIImageView = UIImageView()
        imageView.sd_setImage(with: appAPIHelp.releaseSuccessImage())
        imageView.contentMode = UIViewContentMode.scaleToFill
        containerView.addSubview(imageView)
        imageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(containerView)
            make.height.equalTo(210/baseWidth)
        }
        
        //确认的按钮
        let button: UIButton = UIButton()
        button.backgroundColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor.white]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("立即体验", comment: ""), attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        containerView.addSubview(button)
        button.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(imageView.snp.bottom)
            make.bottom.left.right.equalTo(containerView)
        }
        button.addTarget(mainView, action: #selector(mainView.touchPurchaseButton(button:)), for: UIControlEvents.touchUpInside)
        
        //渐变和缩放动画
        containerView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
            mainView.alpha = 1
            containerView.transform = CGAffineTransform.identity
        }, completion: nil)
        
        return mainView
    }
    
    //MARK: 点击购买按钮的事件
    func touchPurchaseButton(button: UIButton) {
        dismissAnimation()
        touchPurchaseButtonClosure?()
    }
}
