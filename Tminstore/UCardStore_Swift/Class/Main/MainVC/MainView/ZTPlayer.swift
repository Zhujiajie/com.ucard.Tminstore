//
//  ZTPlayer.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/11/2.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class ZTPlayer: Player {

    /// 检查视频成功，开始播放
    var checkVideoSuccess: (()->())?
    
    /// 进入后台的闭包
    var playerEnterBackground: (()->())?
    
    /// app不活跃是，是否停止播放视频。默认停止
    var shouldStopWhenEnterBackground: Bool = true
    
    fileprivate var currentVideoID: String = ""
    fileprivate var videoURL: URL?
    fileprivate var session: URLSession?
    fileprivate var connectionTask: URLSessionDataTask?
    
    /// 监测是否可以缓存视频
    fileprivate var timer: Timer?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        playbackPausesWhenBackgrounded = true//进入后台停止播放
        playbackResumesWhenEnteringForeground = false//进入前台不播放
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// 判断视频是否已经缓存过了
    ///
    /// - Parameter videoID: 视频的ID
    /// - Parameter isCommunityVideo: 是否是社区的视频
    func setVideoWithVideoID(videoID: String, shouldSaveVideo save: Bool = true) {
        
        currentVideoID = videoID
        
        let cachePath: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!
        let videoCachePath: String = cachePath + ("/videoCache/" + currentVideoID + ".mp4")
        if FileManager.default.fileExists(atPath: videoCachePath) == true{
            videoURL = URL(fileURLWithPath: videoCachePath)
            self.url = videoURL
        }else{
            
            videoURL = URL(string: "http://ucardstorevideo.b0.upaiyun.com" + videoUpun + currentVideoID + ".mp4")
            self.url = videoURL
            
            if save == true{
                saveVideoInBackground()
            }
//            //先检测视频是否存在
//            let request: URLRequest = URLRequest(url: videoURL!)
//            session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
//            connectionTask = session?.dataTask(with: request)
//            connectionTask!.resume()//开始请求
        }
    }
    
    
    
    
    //MARK: 设置URL，并保存视频
    /// 设置URL，并保存视频
    ///
    /// - Parameters:
    ///   - fullVideoURL: 视频的完整路径
    ///   - save: 是否需要缓冲，默认开启缓冲
    func setVideoFullURL(fullVideoURL: URL, shouldSaveVideo save: Bool = true ){
        
        currentVideoID = fullVideoURL.deletingPathExtension().lastPathComponent
        
        let cachePath: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!
        let videoCachePath: String = cachePath + ("/videoCache/" + currentVideoID + ".mp4")
        if FileManager.default.fileExists(atPath: videoCachePath) == true{
            videoURL = URL(fileURLWithPath: videoCachePath)
            self.url = videoURL
        }else{
            videoURL = fullVideoURL
            self.url = videoURL
            
            if save == true{
                saveVideoInBackground()
            }
            
            //            //先检测视频是否存在
            //            let request: URLRequest = URLRequest(url: videoURL!)
            //            session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: nil)
            //            connectionTask = session?.dataTask(with: request)
            //            connectionTask!.resume()//开始请求
        }
    }
    
    //MARK: 缓存视频
    fileprivate func saveVideoInBackground(){
        if timer != nil {timer?.invalidate()}
        timer = Timer(timeInterval: 1, target: self, selector: #selector(checkVideoSize(timer:)), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    //MARK: 检测是否获得了视频的长宽
    func checkVideoSize(timer: Timer){
        if naturalSize.width != 0{
            timer.invalidate()
            doInBackground(inBackground: {[weak self] _ in
                self?.saveVideo(videoName: (self?.currentVideoID)!)//保存视频
            })
        }
    }
    
    //MARK: 开启下载服务
    fileprivate func saveVideo(videoName: String){
        
        let cacheDirectory: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!//获取缓存文件夹
        let videoFolder: String = cacheDirectory + "/videoCache"
        let videoOutPutPath: String = videoFolder + "/\(videoName).mp4"//拼接视频路径
        
        if FileManager.default.fileExists(atPath: videoFolder) == false {
            do {
                try FileManager.default.createDirectory(atPath: videoFolder, withIntermediateDirectories: false, attributes: nil)
            }catch{print(error)}
        }//如果文件夹不存在，则创建一个
        
        let fileURL: URL = URL(fileURLWithPath: videoOutPutPath)
        if _asset != nil {
            let mixComposition: AVMutableComposition = AVMutableComposition()
            let videoTrack: AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaTypeVideo, preferredTrackID: kCMPersistentTrackID_Invalid)
            //当视频的高大于宽时，将视频旋转90度，然后缓存
            if shouldRotate() == true{
                videoTrack.preferredTransform = CGAffineTransform(rotationAngle: CGFloat(Double.pi/2))
            }
            let audioTrack: AVMutableCompositionTrack = mixComposition.addMutableTrack(withMediaType: AVMediaTypeAudio, preferredTrackID: kCMPersistentTrackID_Invalid)
            do {
                if let videoOfTrack: AVAssetTrack = _asset.tracks(withMediaType: AVMediaTypeVideo).first{
                    try videoTrack.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: _asset.duration), of: videoOfTrack, at: kCMTimeZero)
                }
                if let audioOfTrack: AVAssetTrack = _asset.tracks(withMediaType: AVMediaTypeAudio).first {
                    try audioTrack.insertTimeRange(CMTimeRange(start: kCMTimeZero, duration: _asset.duration), of: audioOfTrack, at: kCMTimeZero)
                }
            }catch{print(error)}
            
            let export: AVAssetExportSession? = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetPassthrough)
            export?.outputURL = fileURL
            export?.outputFileType = AVFileTypeMPEG4
            export?.exportAsynchronously(completionHandler: {
                
                switch export!.status{
                case AVAssetExportSessionStatus.completed:
//                    self?.switchPlayerItemToLocalItem(videoURL: fileURL)//下载完成后，将视频播放地址切换到本地
                    break
                default:
                    if let error: Error = export?.error{
                        print(error)
                    }
                    do{
                        try FileManager.default.removeItem(at: fileURL)
                    }catch{print(error)}
                }
            })
        }
    }
    
    //MARK: 视频下载完成之后，将播放器的播放地址切换成本地的
    fileprivate func switchPlayerItemToLocalItem(videoURL: URL){
        //如果视频正在播放，则切换到本地视频
        if self.playbackState == PlaybackState.playing{
            if let videoTime: CMTime = _playerItem?.currentTime(){
                self.url = videoURL
                seekToTime(to: videoTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
                playFromCurrentTime()
            }
        }
    }
    
    
    //MARK: 判断视频是否需要旋转
    fileprivate func shouldRotate()->Bool{
        let track: AVAssetTrack = _asset.tracks(withMediaType: AVMediaTypeVideo)[0]
        let txf: CGAffineTransform = track.preferredTransform
        let videoAngleInDegree: CGFloat = atan2(txf.b, txf.a) * 180 / CGFloat(Double.pi)
        return videoAngleInDegree == 90
    }
//    //MARK: 通过代理判断视频是否存在
//    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
//        dataTask.cancel()
//        if let httpResponse: HTTPURLResponse = response as? HTTPURLResponse{
//            if httpResponse.statusCode != 200{//视频不存在，切换到新路径
//                videoURL = URL(string: "http://ucardstorevideo.b0.upaiyun.com" + communityVideoUpun + currentVideoID + ".mp4")
//            }
//        }
//        setUrl(videoURL!)
//        saveVideo(videoName: currentVideoID)//保存视频
//        checkVideoSuccess?()
//    }
}
