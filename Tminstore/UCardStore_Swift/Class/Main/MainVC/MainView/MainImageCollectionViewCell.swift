//
//  MainImageCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/8.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: ---------------------展示图片的collectionViewCell--------------------

import UIKit
import SnapKit
import SDWebImage

class MainImageCollectionViewCell: UICollectionViewCell {
    
    /// 占位图片的路径
    var placeholerImageURL: URL?
    
    /// 图片路径
    var imageURL: URL?{
        didSet{
            if let url: URL = imageURL{
                //先判断是否已换成图片
                SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                    
                    if result == true{
                        self?.imageView?.sd_setImage(with: url)
                    }else{
                        if let placeHolderURL: URL = self?.placeholerImageURL{
                            self?.imageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                                DispatchQueue.main.async {
                                    if image != nil {
                                        self?.imageView?.sd_setImage(with: url, placeholderImage: image!)
                                    }else{
                                        self?.imageView?.sd_setImage(with: url)
                                    }
                                }
                            })
                        }else{
                            self?.imageView?.sd_setImage(with: url)
                        }
                    }
                })
            }
        }
    }
    
    fileprivate var imageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        //图片
        imageView = UIImageView()
        imageView?.backgroundColor = UIColor.white
        imageView?.contentMode = UIViewContentMode.scaleToFill
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
        imageURL = nil
        placeholerImageURL = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
