//
//  MainCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/28.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

let stopVideoPlayNotificationName: String = "stopVideoPlayNotificationName"

class MainCollectionViewCell: UICollectionViewCell {
    
    /// 视频ID
    var videoId: String?{
        didSet{
            if videoId != "" && videoId != nil{
                player?.stop()
                player?.setVideoWithVideoID(videoID: videoId!, shouldSaveVideo: true)
                player?.view.isHidden = false
                activeIndicator?.startAnimating()
            }
        }
    }
    
    fileprivate var player: ZTPlayer?
    fileprivate var playAgainButton: UIButton?
    fileprivate var activeIndicator: UIActivityIndicatorView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        player = ZTPlayer()
        player?.view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 266/baseWidth)
        player?.playbackLoops = true
        player?.playerDelegate = self
        player?.playbackDelegate = self
        contentView.addSubview(player!.view)
        //MARK: app进入后台，停止播放，并展示播放按钮
        player?.enterBackgroundClosure = {[weak self] _ in
            if self?.videoId != nil{
                self?.playAgainButton?.isHidden = false
            }
        }
        
        //重新开始播放的按钮
        playAgainButton = UIButton()
        playAgainButton?.setImage(UIImage(named: "playButton"), for: UIControlState.normal)
        playAgainButton?.contentMode = UIViewContentMode.scaleAspectFit
        playAgainButton?.addTarget(self, action: #selector(playFromCurrentTime(button:)), for: UIControlEvents.touchUpInside)
        player?.view.addSubview(playAgainButton!)
        playAgainButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view!)
            make.size.equalTo(CGSize(width: 80/baseWidth, height: 80/baseWidth))
        })
        playAgainButton?.isHidden = true
        
        activeIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        contentView.addSubview(activeIndicator!)
        activeIndicator?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(contentView)
        })
        activeIndicator?.hidesWhenStopped = true
        
        //注册通知，暂停视频播放
        NotificationCenter.default.addObserver(self, selector: #selector(stopVideoPlayNotification(info:)), name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)
    }
    
    //MARK: 播放视频
    /// 播放视频
    func playVideo(){
        player?.playFromCurrentTime()
    }
    
    //MARK: 暂停视频
    /// 暂停视频
    ///
    /// - Parameter hidePlayButton: 是否需要显示播放按钮
    func pausePlayer(hidePlayButton: Bool = false){
        playAgainButton?.isHidden = hidePlayButton
        player?.pause()
    }
    
    //MARK: 重新开始播放视频
    func playFromCurrentTime(button: UIButton){
        playAgainButton?.isHidden = true
        player?.playFromCurrentTime()
    }
    
    //MARK: 接受通知，暂停视频播放
    func stopVideoPlayNotification(info: Notification){
        if player?.playbackState == PlaybackState.playing{
            pausePlayer()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        videoId = nil
        playAgainButton?.isHidden = true
        player?.stop()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
extension MainCollectionViewCell: PlayerDelegate, PlayerPlaybackDelegate{
    func playerReady(_ player: Player) {
    }
    func playerPlaybackStateDidChange(_ player: Player) {
    }
    //MARK: 根据缓存情况，展示加载提示器
    func playerBufferingStateDidChange(_ player: Player) {
        switch player.bufferingState {
        case BufferingState.ready:
            activeIndicator?.stopAnimating()
        default:
            activeIndicator?.startAnimating()
        }
    }
    
    func playerCurrentTimeDidChange(_ player: Player){
    }
    func playerPlaybackWillStartFromBeginning(_ player: Player){
    }
    func playerPlaybackDidEnd(_ player: Player){
    }
    func playerPlaybackWillLoop(_ player: Player){
    }
}
