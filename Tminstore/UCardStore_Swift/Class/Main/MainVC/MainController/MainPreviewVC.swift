//
//  MainPreviewVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MainPreviewVC: BaseViewController {
    
    /// 订单模型
    var order: OrderPostcardModel?
    
    /// 支付
    var purchaseClosure: (()->())?
    
    /// 分享
    var shareClosure: (()->())?
    
    /// 取消分享
    var cancelClosure: (()->())?
    
    /// 买图
    var buyPhoto: (()->())?
    
    /// 评论
    var commit: (()->())?
    
    /// 举报
    var report: (()->())?
    
    /// 视频的路径
    var videoId: String = ""
    
    /// 是否要展示举报按钮
    var shouldShowReportAction: Bool = false
    
    /// 视频的播放时间
    var currentVideoTime: CMTime?{
        get {
            if videoPlayer != nil && videoPlayer?._playerItem != nil{
                return videoPlayer!._playerItem!.currentTime()
            }else{
                return nil
            }
        }
    }
    
    /// 播放器
    var videoPlayer: ZTPlayer?{
        didSet{
            setPlayer()
        }
    }
    
    /// 加载动画
    fileprivate var loadingView: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.clear
        check3DTouch()
        setPlayer()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if videoPlayer != nil{
            videoPlayer?.playFromBeginning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayer(){
        
        setLoadingView()//先设置加载动画
        
        if videoPlayer != nil {
            view.addSubview(videoPlayer!.view)
            videoPlayer?.view.snp.makeConstraints({ (make: ConstraintMaker) in
                make.center.equalTo(view)
                make.size.equalTo(CGSize(width: screenWidth, height: screenWidth * 0.66))
            })
            //添加圆角
            videoPlayer?.view.layer.cornerRadius = 20/baseWidth
            videoPlayer?.view.layer.masksToBounds = true
        }
    }
    
    //MARK: 设置加载动画
    fileprivate func setLoadingView(){
        loadingView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        view.addSubview(loadingView!)
        loadingView?.center = view.center
        loadingView?.startAnimating()
        loadingView?.hidesWhenStopped = true
    }
    
    //MARK: 增加点击退出
    fileprivate func check3DTouch(){
        if #available(iOS 9.0, *) {
            if traitCollection.forceTouchCapability == UIForceTouchCapability.available{
                let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissMe(tap:)))
                view.addGestureRecognizer(tap)
            }
        } else {
            // Fallback on earlier versions
        }
    }

    func dismissMe(tap: UITapGestureRecognizer){
        dismissSelf()
    }
    
    //MARK: 停止播放
    func stopPlay(){
        loadingView?.stopAnimating()
        videoPlayer?.stop()
    }
    
    @available(iOS 9.0, *)
    override var previewActionItems: [UIPreviewActionItem]{
        get{
            return setPreviewActionItemsFunc()
        }
    }
    
    //MARK: 设置3D touch 预览视图的按钮
    @available(iOS 9.0, *)
    fileprivate func setPreviewActionItemsFunc()->[UIPreviewActionItem]{
        
        var actionItems: [UIPreviewActionItem] = [UIPreviewActionItem]()
        
        if order == nil {
            // 买图
            let buyAction: UIPreviewAction = UIPreviewAction(title: NSLocalizedString("买图", comment: ""), style: UIPreviewActionStyle.default) { [weak self](_, _) in
                self?.buyPhoto?()
            }
            actionItems.append(buyAction)
            
            // 评论
            let commitAction: UIPreviewAction = UIPreviewAction(title: NSLocalizedString("评论", comment: ""), style: UIPreviewActionStyle.default) { [weak self](_, _) in
                self?.commit?()
            }
            actionItems.append(commitAction)
            
            // 只在社区主页展示举报按钮
            if shouldShowReportAction == true{
                //举报
                let reportAction: UIPreviewAction = UIPreviewAction(title: NSLocalizedString("举报", comment: ""), style: UIPreviewActionStyle.default) { [weak self] (_, _) in
                    self?.report?()
                }
                actionItems.append(reportAction)
            }
        }else{
           
            if order!.status == OrderStaus.unPaied{
                // 支付
                let purchaseAction: UIPreviewAction = UIPreviewAction(title: NSLocalizedString("去支付", comment: ""), style: UIPreviewActionStyle.default) { [weak self](_, _) in
                    self?.purchaseClosure?()
                }
                actionItems.append(purchaseAction)
            }
            
            if order!.isShared == false{
                // 分享
                let shareAction: UIPreviewAction = UIPreviewAction(title: NSLocalizedString("发布", comment: ""), style: UIPreviewActionStyle.default) { [weak self](_, _) in
                    self?.shareClosure?()
                }
                actionItems.append(shareAction)
            }else{
                // 取消分享
                let cancelShareAction: UIPreviewAction = UIPreviewAction(title: NSLocalizedString("取消发布", comment: ""), style: UIPreviewActionStyle.default) { [weak self](_, _) in
                    self?.cancelClosure?()
                }
                actionItems.append(cancelShareAction)
            }
        }
        
        
        return actionItems
    }
}
