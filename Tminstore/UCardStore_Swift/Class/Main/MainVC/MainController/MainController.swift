//
//  MainController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import MJRefresh
import SnapKit

//-------------------------主界面，社区界面----------------------------

class MainController: BaseViewController {

    fileprivate let viewModel: MainViewModel = MainViewModel()
    
    /// 存放数据的数组
    var postcardArray: [PostcardModel] = [PostcardModel]()

    fileprivate var tableView: MainTableView?
    
    /// 提示框，提示用户点击
    fileprivate var introView: MainView?
    /// 举报的提示框
    fileprivate var reportAlert: MainReportAlertVC?
    /// 提示买图的视图
    fileprivate var purchaseConfirmView: MainView?
    
    /// 导航栏的右按钮
    fileprivate var navigationRightButton: UIBarButtonItem?
    
    //MARK:----------------生命周期--------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hidesBottomBarWhenPushed = false
        
        setNavigationBar()
        setTableView()//设置tableView
        viewModelClosure()//闭包回调
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)//发送通知，暂停播放视频
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkMessageStatus()//检查是否有未读消息
        //如果数据为空，则向后台请求数据
        if postcardArray.isEmpty == true{
           loadData()
        }
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        
        let titleView: UIImageView = UIImageView(image: UIImage(named: "TimeoryTitle"))
        navigationItem.titleView = titleView

        // 自定义导航栏右按钮
        navigationRightButton = UIBarButtonItem(title: nil, style: UIBarButtonItemStyle.plain, target: self, action: #selector(navigationBarRightBarButtonAction(_:)))
        navigationRightButton?.image = UIImage(named: "Message")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        navigationItem.rightBarButtonItem = navigationRightButton
    }
    
    //MARK:设置tableView
    fileprivate func setTableView() {
        
        tableView = MainTableView()
        tableView?.delegate   = self
        tableView?.dataSource = self
        
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view)
            make.left.equalTo(view)
            make.bottom.equalTo(view)
            make.right.equalTo(view)
        }
    }
    
    //MARK: 买图行为
    fileprivate func buyPhotot(indexPath: IndexPath){
//        MobClick.event("buy_touchButton")
//        purchaseConfirmView = MainView.purchaseConfirmView(portraitURL: postcardArray[indexPath.row].userLogoURL, andFrame: UIScreen.main.bounds)
//        app.tabBarVC?.view.addSubview(purchaseConfirmView!)
//        purchaseConfirmView?.touchPurchaseButtonClosure = {[weak self] _ in
//            self?.viewModel.checkPurchaseAvaliable(originalId: (self?.postcardArray[indexPath.row].originalId)!, andIndexPath: indexPath)
//        }
    }

    //MARK: 评论行为
    fileprivate func commit(originalId: String, andIndexPath indexPath: IndexPath){
        let detailVC: MainDetialController = MainDetialController()
        detailVC.originalId = originalId
        detailVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(detailVC, animated: true)
        
        //MARK: 评论成功之后，更新数据
        detailVC.postCommentSuccess = {[weak self] in
            self?.viewModel.updateCommentData(indexPath: indexPath)
        }
    }
    
    //MARK: 加载主页数据
    /// 加载主页数据
    func loadData(){
//        showSVProgress(title: nil)
//        viewModel.pushToRefresh()//先请求服务器端的数据
        tableView?.mj_header.beginRefreshing()//先请求服务器端的数据
    }
    
    //MARK: 检查消息状态
    func checkMessageStatus(){
        viewModel.checkMessageStatus()//检查是否有未读消息
    }
    
    //MARK: viewModel的闭包回调
    fileprivate func viewModelClosure(){
        
        //设置上拉刷新
        tableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.pullToRefresh()
        }
        
        //设置下拉刷新
        tableView?.mj_header.refreshingBlock = { [weak self]  _ in
            self?.viewModel.pushToRefresh()
            self?.tableView?.mj_footer.resetNoMoreData()
        }
        
        //MARK: 顶部刷新动画需要停止
        viewModel.headerStopRefresh = {[weak self] _ in
            self?.tableView?.mj_header.endRefreshing()
        }
        
        //MARK: 底部刷新动画需要停止
        viewModel.footerStopRefresh = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshing()
        }
        
        //MARK: 没有更多数据了
        viewModel.stopRefreshWithNoMoreData = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: tableView需要重新加载
        viewModel.tableShouldReloadData = {[weak self] (postcardArray: [PostcardModel]) in
            self?.postcardArray = postcardArray
            self?.tableView?.reloadData()
        }
        
        //MARK: 评论之后需要更新数据
        viewModel.updateData = {[weak self] (indexPath: IndexPath, postcardArray: [PostcardModel]) in
            self?.postcardArray = postcardArray
            self?.tableView?.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
        
        //MARK: 检查买图权限成功，进图买图流程
        viewModel.checkPurchaseAvailableSuccess = {[weak self] (indexPath: IndexPath) in
            self?.enterBackEditView(indexPath: indexPath)
        }
        
        //MARK: 网络请求错误
        viewModel.netWorkError = {[weak self] (error, result) in
            //如果获取数据出错，则从CoreData中获取数据
            if self?.postcardArray.isEmpty == true{
                self?.viewModel.getDataFromCoreData()
            }
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 发现有未读消息，或没有未读消息
        viewModel.checkMessageStatusClosure = {[weak self] (count: Int) in
            if count != 0{
                self?.navigationRightButton?.image = UIImage(named: "NewMessage")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                self?.navigationController?.tabBarItem.badgeValue = "\(count)"
            }else{
                self?.navigationRightButton?.image = UIImage(named: "Message")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                self?.navigationController?.tabBarItem.badgeValue = nil
            }
        }
    }
    
    //MARK: 举报的sheet
    fileprivate func showReportAlert(postcardModel: PostcardModel){
        reportAlert = MainReportAlertVC(title: NSLocalizedString("举报", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        reportAlert?.postcard = postcardModel
        present(reportAlert!, animated: true, completion: nil)
        reportAlert?.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }//举报的请求出错
        reportAlert?.reportSuccess = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("感谢您的举报", comment: ""))
        }//举报成功
    }
    
    //MARK: 进入明信片背面编辑，买图
    fileprivate func enterBackEditView(indexPath: IndexPath){
        MobClick.event("buy_touchConfirmButton")
        let postcard: PostcardModel = postcardArray[indexPath.row]
        let postcardToMake: MUFinalPostcardModel = MUFinalPostcardModel()
        if postcard.photoLocation?.coordinate.latitude == 0 || postcard.photoLocation == nil{
            postcardToMake.photoLocation = currentUserLocation
        }else{
            postcardToMake.photoLocation = postcard.photoLocation
        }
        postcardToMake.buyFrontImageURL = postcard.frontImageURL
        postcardToMake.isComeFromPurchase = true
        postcardToMake.originalId = postcard.originalId
        //进入明信片背面编辑界面
        let backVC: MUBackVC = MUBackVC()
        backVC.hidesBottomBarWhenPushed = true
        backVC.umengViewName = "编辑背面"
        backVC.postcard = postcardToMake
        _ = navigationController?.pushViewController(backVC, animated: true)
    }
    
    //MARK: 点击导航栏右按钮的事件
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        let messageVC: MessageController = MessageController()
        messageVC.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(messageVC, animated: true)
    }
}
//MARK:----------------UITableViewDelegate, UITableViewDataSource-------
extension MainController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postcardArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //MARK:设置cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: MainTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MainTableViewCellReuseIdentifier, for: indexPath) as! MainTableViewCell
        cell.postcard = postcardArray[indexPath.row]
        
        //MARK: 点击了cell的评论按钮，跳转到明信片详情界面
        cell.imageViewOrCommentClosure = {[weak self] (originalId: String) in
            self?.commit(originalId: originalId, andIndexPath: indexPath)
        }
        
        //MARK: 点击买图按钮的事件
        cell.touchPurchaseButtonClosure = {[weak self] _ in
            self?.buyPhotot(indexPath: indexPath)
        }
        
        //MARK: 点击更多按钮， 去举报
        cell.touchMoreButtonClosure = {[weak self] (postcard: PostcardModel) in
            self?.showReportAlert(postcardModel: (self?.postcardArray[indexPath.row])!)
        }
        
        //MARK: 点击点赞按钮，发送点赞或取消点赞的请求
        cell.touchLikeButtonClosure = {[weak self] _ in
            if let postcard: PostcardModel = self?.postcardArray[indexPath.row]{
                if postcard.isLiked == false{
                    self?.viewModel.sendLikeRequest(originalId: postcard.originalId, andIndexPath: indexPath)
                }else{
                    self?.viewModel.sendCancelLikeRequest(originalId: postcard.originalId, andIndexPath: indexPath)
                }
            }
        }
        
        //MARK: 点赞的结果
        viewModel.sendLikeOrCancelLikeSuccess = {[weak self] (isLiked: Bool, indexPath: IndexPath) in
            if let postcard: PostcardModel = self?.postcardArray[indexPath.row]{
                if let cell: MainTableViewCell = self?.tableView?.cellForRow(at: indexPath) as? MainTableViewCell{
                    cell.setLikeButtonStatus(isLiked: isLiked, shouldChangeLikeCount: true)
                    postcard.isLiked = isLiked
                }
            }
        }
        
        return cell
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if let cells: [MainTableViewCell] = tableView?.visibleCells as? [MainTableViewCell]{
            for cell: MainTableViewCell in cells{
                cell.pauseVideo()
            }
        }
    }
}
