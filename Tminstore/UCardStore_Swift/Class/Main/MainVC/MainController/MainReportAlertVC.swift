//
//  MainReportAlertVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/5.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: -----------------举报的提示框----------------------
import UIKit

class MainReportAlertVC: UIAlertController {

    var postcard: PostcardModel?
    
    var commentModel: CommentModel?
    
    var replyModel: ReplyModel?
    
    /// 举报成功
    var reportSuccess: (()->())?
    
    /// 举报接口出错的闭包
    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    fileprivate let dataModel: BaseDataModel = BaseDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let action1 = UIAlertAction(title: NSLocalizedString("广告", comment: ""), style: .default) { [weak self] (action: UIAlertAction) in
            self?.reportIssue(content: action.title!)
        }
        addAction(action1)
        
        let action2 = UIAlertAction(title: NSLocalizedString("色情", comment: ""), style: .default) { [weak self] (action: UIAlertAction) in
            self?.reportIssue(content: action.title!)
        }
        addAction(action2)
        
        let action3 = UIAlertAction(title: NSLocalizedString("政治敏感信息", comment: ""), style: .default) { [weak self] (action: UIAlertAction) in
            self?.reportIssue(content: action.title!)
        }
        addAction(action3)
        
        let action4 = UIAlertAction(title: NSLocalizedString("其他", comment: ""), style: .default) { [weak self] (action: UIAlertAction) in
            self?.reportIssue(content: action.title!)
        }
        addAction(action4)
        
        let cancel = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: .cancel, handler: nil)
        addAction(cancel)
    }
    
    //MARK: 举报功能图片
    fileprivate func reportIssue(content: String){
        
        if postcard != nil{
            reportPhoto(content: content)
        }else if commentModel != nil{
            reportComment(content: content)
        }else if replyModel != nil{
            reportReply(content: content)
        }
    }
    
    //MARK: 举报图片
    fileprivate func reportPhoto(content: String){
        let parameters: [String: Any] = [
            "fromToken": getUserToken(),
            "toMemberCode": postcard!.memberCode,
            "content": content,
            "origianlId": postcard!.originalId
            ] as [String: Any]
        
        dataModel.sendPOSTRequestWithURLString(URLStr: appAPIHelp.reportAPI, ParametersDictionary: parameters, successClosure: { [weak self](result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.reportSuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
            }, completionClosure: nil)
    }
    
    //MARK: 举报评论
    fileprivate func reportComment(content: String){
        
        let urlString: String = appAPIHelp.reportCommentAPI + "\(commentModel!.commentId)/report"
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "content": content
        ] as [String: Any]
        
        dataModel.sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.reportSuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: {[weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
        })
    }
    
    //MARK: 举报回复
    fileprivate func reportReply(content: String){
        let urlString: String = appAPIHelp.reportReplyAPI + "\(replyModel!.id)/report"
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "content": content
            ] as [String: Any]
        
        dataModel.sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.reportSuccess?()
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: {[weak self] (error: Error?) in
                self?.errorClosure?(error, nil)
        })
    }
}
