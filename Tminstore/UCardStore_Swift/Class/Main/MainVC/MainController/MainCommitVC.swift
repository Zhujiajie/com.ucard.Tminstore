//
//  MainCommitVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MainCommitVC: BaseViewController {

    /// 用来执行动画的frame
    var cellFrame: CGRect = CGRect.zero
    
    /// 退出的闭包
    var dismissClosure: (()->())?
    
    var videoId: String = ""
    
    /// 上一个视图额播放进度
    var videoCurrentTime: CMTime?
    
    /// 背景图片
    var backgroundImage: UIImage?
    
    /// 高斯模糊
    fileprivate var visualView: UIVisualEffectView?
    /// 背景图片
    fileprivate var backgroundImageView: UIImageView?
    
    /// 播放器
    var videoPlayer: ZTPlayer?
    
    /// 加载动画
    fileprivate var loadingView: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setBackgroundImageView()
        setVisualView()
        setPlayer()
        
        // 添加点击退出的手势
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToDismiss(tap:)))
        visualView?.contentView.addGestureRecognizer(tap)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        loadingView?.stopAnimating()
    }
    
    //MARK :设置背景视图
    fileprivate func setBackgroundImageView(){
        backgroundImageView = UIImageView()
        backgroundImageView?.contentMode = UIViewContentMode.scaleAspectFill
        backgroundImageView?.image = backgroundImage
        view.addSubview(backgroundImageView!)
        backgroundImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
    }
    
    //MARK: 设置背景视图上方的高斯模糊视图
    fileprivate func setVisualView(){
        let blur: UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
        visualView = UIVisualEffectView(effect: blur)
        view.addSubview(visualView!)
        visualView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(view)
        }
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayer(){
        setLoadingView()//先设置加载动画
        if videoPlayer != nil {
            view.addSubview(videoPlayer!.view)
            videoPlayer?.view.snp.makeConstraints({ (make: ConstraintMaker) in
                make.center.equalTo(view)
                make.size.equalTo(CGSize(width: screenWidth, height: screenWidth * 0.66))
            })
            // 去除圆角
            videoPlayer?.view.layer.cornerRadius = 0
            videoPlayer?.view.layer.masksToBounds = false
            if videoPlayer?.playbackState != PlaybackState.playing{
                videoPlayer?.playFromBeginning()
            }//如果不在播放，则重头播放
        }
    }
    
    //MARK: 设置加载动画
    fileprivate func setLoadingView(){
        loadingView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        view.addSubview(loadingView!)
        loadingView?.center = view.center
        loadingView?.startAnimating()
        loadingView?.hidesWhenStopped = true
    }
    
    //MARK: 点击退出
    func tapToDismiss(tap: UITapGestureRecognizer){
        dismiss(animated: true, completion: {[weak self] _ in
            self?.dismissClosure?()
        })
    }
}
