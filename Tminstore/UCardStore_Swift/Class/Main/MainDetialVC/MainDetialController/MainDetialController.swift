//
//  MainDetialController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

//-------------------------详情界面---------------------

import UIKit
import SnapKit

class MainDetialController: BaseCommentViewController {

    fileprivate let viewModel: MainDetialViewModel = MainDetialViewModel()
    fileprivate let arViewModel: QRViewModel = QRViewModel()
    
    /// 是否需要等待加载数据
    var shouldWaitData: Bool = false
    
    /// 是否是显示发布详情的页面
    var isReleaseVC: Bool = false
    
    /// 是否使用dismiss方法
    var shouldUseDismiss: Bool = false
    
    /// 明信片的模型
    var postcard: PostcardModel?
    
    /// tableHeaderView
    fileprivate var tableHeaderView: PDetialHeaderView?
    
    /// 该明信片的id
    var originalId: String = ""
    
    /// 发表评论成功的闭包
    var postCommentSuccess: (()->())?
    
    /// 取消分享成功
    var cancelOrReleaseOrDeleteSuccess: (()->())?
    
    /// 正在回复的评论
    fileprivate var replayComment: CommentModel?
    /// 积分提示框
    fileprivate var creditAlerVC: MUPublishAlertVC?
    /// 举报的提示框
    fileprivate var reportAlert: MainReportAlertVC?
    /// 导航栏返回的按钮
    fileprivate var backBarItem: MDetailBarItem?
    /// 导航栏进入AR扫描的按钮
    fileprivate var enterARBarItem: MDetailBarItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()//设置导航栏
        //从推送进入本界面，需要等待数据载入
        if shouldWaitData == true{
            viewModel.getPurchasedCardInfo(originalId: originalId)
            viewModel.getPurchasedCardInfoSuccess = {[weak self] (postcard: PostcardModel) in
                self?.postcard = postcard
                self?.shouldWaitData = false
                self?.setUI()
                self?.requestCommentData()
            }
        }else{
            setUI()//设置UI和闭包回调
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if shouldWaitData == true {return}
        requestCommentData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //取消导航栏透明
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.isTranslucent = false
        
        super.viewWillAppear(animated)
        
        if isReleaseVC == true{
            MobClick.beginLogPageView("发布")
            
        }else{
            MobClick.beginLogPageView("评论")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: stopVideoPlayNotificationName), object: nil)//发送通知，暂停播放视频
        if isReleaseVC == true{
            MobClick.endLogPageView("发布")
        }else{
            MobClick.endLogPageView("评论")
        }
    }

    //MARK:设置导航栏
    fileprivate func setNavigationBar() {

        //设置左按钮
        backBarItem = MDetailBarItem.backBarItem()
        if isReleaseVC == true && postcard?.arCode != nil {
            enterARBarItem = MDetailBarItem.enterARBarItem()
            navigationItem.leftBarButtonItems = [backBarItem!, enterARBarItem!]
        }else{
            navigationItem.leftBarButtonItem = backBarItem!
        }

        // 设置标题
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D")] as [String: Any]
        
        if isReleaseVC == true{
            navigationItem.title = NSLocalizedString("详情", comment: "")
            if postcard != nil{
                //设置右按钮
                let rightButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "dot"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(navigationBarRightBarButtonAction(_:)))
                navigationItem.rightBarButtonItem = rightButton
                rightButton.tintColor = UIColor(hexString: "#949494")
            }
        }else{
            navigationItem.title = NSLocalizedString("评论", comment: "")
        }
        
        navigationController?.navigationBar.titleTextAttributes = attDic
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        self.tableView?.register(MDTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MDTableViewCellReuseIdentifier)
        
        if postcard != nil{
            tableHeaderView = PDetialHeaderView()
            tableHeaderView?.shouldShowBackImage = false
            tableHeaderView?.isOrder = false
            tableHeaderView?.postcard = postcard
            sizeHeaderToFit(tableHeaderView: tableHeaderView!)//计算tableHeaderView的高度
            tableView?.tableHeaderView = tableHeaderView
            viewModel.originalId = postcard!.originalId//发布详情
        }else{
            viewModel.originalId = originalId//订单详情
        }
        
        self.textView.placeholder = NSLocalizedString("说点什么吧", comment: "")
        
        closures()//闭包回调
    }
    
    //MARK: 请求评论数据
    fileprivate func requestCommentData(){
        if viewModel.commentArray.isEmpty == true{
            // 进入该页面时，刷新数据
            viewModel.pageIndex = 1
            viewModel.getCommentList()//请求数据
        }
    }
    
    //MARK:设置导航栏右按钮的点击事件
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        let sheet: MainDetialSheetController = MainDetialSheetController.releaseDetialSheet(postcard: postcard!)
        present(sheet, animated: true, completion: nil)
        
        //MARK: 取消分享
        sheet.cancelReleaseClosure = {[weak self] _ in
            self?.viewModel.cancelRelease(originalId: (self?.postcard?.originalId)!)
        }
        
        //MARK: 删除发布
        sheet.deleteClosure = {[weak self] _ in
            self?.deleteAlert(alertTitle: NSLocalizedString("确认删除", comment: ""), messageString: nil, leftClosure: nil, rightClosure: { 
                self?.viewModel.deleteRelease(originalId: (self?.postcard?.originalId)!)
            })
        }
        
        //MARK: 发布按钮
        sheet.releaseClosure = {[weak self] _ in
            if self?.postcard == nil {return}
            self?.showCreditView()
        }
    }
    
    //MARK: 设置积分视图
    fileprivate func showCreditView(){
        
        creditAlerVC = MUPublishAlertVC()
        creditAlerVC?.isShareToCommunityAgain = true
        let payModel: PayModel = PayModel()
        payModel.originalId = postcard!.originalId
        creditAlerVC?.payModel = payModel
        
        addChildViewController(creditAlerVC!)
        view.addSubview(creditAlerVC!.view)
        creditAlerVC?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        //MARK: 成功分享到时空圈
        creditAlerVC?.shareToCommunitySuccess = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("发布成功", comment: ""),leftClosure: {
                self?.cancelOrReleaseOrDeleteSuccess?()
                _ = self?.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    
    //MARK: 各种闭包回调
    fileprivate func closures(){
        
        //MARK: 点击导航栏返回按钮的事件
        backBarItem?.touchBarItemClosure = {[weak self] _ in
            //选择使用dismiss，还是pop
            if self?.shouldUseDismiss == true{
                self?.dismissSelf()
            }else{
                _ = self?.navigationController?.popViewController(animated: true)
            }
        }
        
        //MARK: 点击导航栏AR按钮的事件
        enterARBarItem?.touchBarItemClosure = {[weak self] _ in
            if self?.postcard?.arCode != nil{
                self?.arViewModel.requestARImageInfo(code: (self?.postcard?.arCode)!)
            }
        }
        
        //MARK: 成功下载AR识别图的闭包
        arViewModel.requestARImageSuccess = {[weak self] (imagePath: String, videoURLString: String) in
            //进入本地识别
            let arVC: LocalARCardViewController = LocalARCardViewController()
            arVC.arImagePath = imagePath
            arVC.videoURLString = videoURLString
            self?.present(arVC, animated: true, completion: nil)
        }
        
        //MARK: 下载AR识别图出错
        arViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 上拉获取更多数据
        self.tableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.viewModel.getCommentList()
        }
        
        //MARK: 成功获取到评论列表，tableView需要重新载入
        viewModel.getCommentSuccess = {[weak self] (totalComment: Int) in
            self?.postcard?.commentTotal = totalComment
            self?.tableHeaderView?.commentNumber = totalComment
            self?.tableView?.reloadData()
        }
        
        //MARK: 网络错误
        viewModel.networkError = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
            self?.tableView?.mj_footer.endRefreshing()
        }
        
        //MARK: 发表评论或回复成功
        viewModel.postCommentSuccess = {[weak self] _ in
            self?.postCommentSuccess?()
            self?.emptyReply()
        }
        
        //MARK: 没有更多数据了
        viewModel.noMoreData = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 获得了更多的数据
        viewModel.getMoreData = {[weak self] (indexPathArray: [IndexPath], totalComment: Int) in
            self?.tableHeaderView?.commentNumber = totalComment
            self?.tableView?.mj_footer.endRefreshing()
            self?.tableView?.beginUpdates()
            self?.tableView?.insertRows(at: indexPathArray, with: UITableViewRowAnimation.automatic)
            self?.tableView?.endUpdates()
        }
        
        //MARK: 删除评论成功
        viewModel.deleteCommmentSuccess = {[weak self] (indexPath: IndexPath) in
            self?.viewModel.commentArray.remove(at: indexPath.row)
            self?.tableView?.beginUpdates()
            self?.tableView?.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            self?.tableView?.endUpdates()
            self?.postCommentSuccess?()
        }
        
        //MARK: 取消分享、分享或删除成功
        viewModel.cancelReleaseOrDeleteSuccess = {[weak self] _ in
            self?.cancelOrReleaseOrDeleteSuccess?()
            _ = self?.navigationController?.popViewController(animated: true)
        }
        
        //MARK: 点赞的结果
        viewModel.sendLikeOrCancelLikeSuccess = {[weak self] (result: Bool) in
            self?.postcard?.isLiked = result
            if result == true{
                self?.postcard?.likeCount += 1
            }else{
                self?.postcard?.likeCount -= 1
            }
            self?.tableHeaderView?.likeCount = (self?.postcard?.likeCount)!
        }
        
        //MARK: 点击评论按钮的事件
        tableHeaderView?.touchCommentButtonClosure = {[weak self] _ in
            self?.textView.becomeFirstResponder()
        }
        
        //MARK: 点击点赞按钮的事件
        tableHeaderView?.touchLikeButtonClosure = {[weak self] _ in
            self?.likeOrCancelLike()
        }
    }
    
    //MARK: 发送点赞或者取消点赞的请求
    fileprivate func likeOrCancelLike(){
        if postcard?.isLiked == false{
            viewModel.sendLikeRequest(originalId: postcard!.originalId)
        }else{
            viewModel.sendCancelLikeRequest(originalId: postcard!.originalId)
        }
    }
    
    //MARK: 清空回复状态
    fileprivate func emptyReply(){
        self.textView.placeholder = NSLocalizedString("说点什么吧", comment: "")
        replayComment = nil
    }
    
    //MARK: 停止编辑时，改回原来的占位符
    override func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            emptyReply()
        }
    }
    
    //MARK: 点击右按钮，发布评论
    override func didPressRightButton(_ sender: Any?) {
        self.textView.refreshFirstResponder()
        self.textView.resignFirstResponder()
        viewModel.pageIndex = 1
        if replayComment != nil{
            viewModel.postReply(replyContent: self.textView.text, commentModel: replayComment!)//回复某人
        }else{
            viewModel.postComment(content: self.textView.text)//发表评论
        }
        super.didPressRightButton(sender)
    }
    
    //MARK: 买图行为
    fileprivate func buyPhotot(){
        alert(alertTitle: NSLocalizedString("即将上线，敬请期待", comment: ""))
    }
    
    //MARK: 评论行为
    fileprivate func commit(){
        emptyReply()
        self.textView.becomeFirstResponder()
    }
    
    //MARK: 举报的sheet
    fileprivate func showReportAlert(commentModel: CommentModel){
        reportAlert = MainReportAlertVC(title: NSLocalizedString("举报", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        reportAlert?.commentModel = commentModel
        present(reportAlert!, animated: true, completion: nil)
        reportAlert?.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }//举报的请求出错
        reportAlert?.reportSuccess = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("感谢您的举报", comment: ""))
        }//举报成功
    }
}

//MARK: tableViewDelegate and dateSource
extension MainDetialController{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        self.tableView?.mj_footer.isHidden = viewModel.commentArray.isEmpty
        return viewModel.commentArray.count
    }
    //MARK: 设置cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell: MDTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MDTableViewCellReuseIdentifier, for: indexPath) as! MDTableViewCell
        cell.postcardComment = viewModel.commentArray[indexPath.row]//设置cell的数据源
        //MARK: 进入回复界面
        cell.tapToReply = {[weak self] _ in
            let replyVC: MainReplyController = MainReplyController()
            replyVC.commentModel = cell.postcardComment
            _ = self?.navigationController?.pushViewController(replyVC, animated: true)
            //MARK: 回复成功之后，刷新数据
            replyVC.postReplySuccess = {
                self?.viewModel.pageIndex = 1
                self?.viewModel.getCommentList()
            }
        }
        return cell
    }
    // MARK: 点击cell，回复某人
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let comment: CommentModel = viewModel.commentArray[indexPath.row]
        
        replyOrDeleteSheet(commentModel: comment, deleteClosure: { [weak self] _ in
            self?.viewModel.deleteComment(commentModel: comment, andIndexPath: indexPath)
        }, replyClosure: { [weak self] _ in
            self?.textView.placeholder = "@\(comment.nickName)"
            self?.textView.text = ""//清空原先的内容
            self?.textView.becomeFirstResponder()//输入回复的内容
            self?.replayComment = comment//正在回复的评论
            }, reportClosure: {[weak self] _ in
                self?.showReportAlert(commentModel: comment)//举报
        })
        
//        let title: String = "\(comment.nickName): \(comment.content)"
//        replySheet(title: title, replyClosure: { [weak self] _ in
//            self?.textView.placeholder = "@\(comment.nickName)"
//            self?.textView.text = ""//清空原先的内容
//            self?.textView.becomeFirstResponder()//输入回复的内容
//            self?.replayComment = comment//正在回复的评论
//        }) {
//            UIPasteboard.general.string = comment.content//复制
//        }
    }
}
