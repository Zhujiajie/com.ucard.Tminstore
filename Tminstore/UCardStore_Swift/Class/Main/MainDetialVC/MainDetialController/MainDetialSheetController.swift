//
//  MainDetialSheetController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: ----------------发布详情界面，点击右上方三点按钮的弹窗--------------------
import UIKit

class MainDetialSheetController: UIAlertController {
    
    fileprivate let dataModel: BaseDataModel = BaseDataModel()
    
    var postcard: PostcardModel?
    
    /// 发布到时空圈的闭包
    var releaseClosure: (()->())?
    /// 取消发布的闭包
    var cancelReleaseClosure: (()->())?
    /// 删除的闭包
    var deleteClosure: (()->())?
    
    /// 带分享功能的弹窗，可以选择发布、取消发布和删除
    ///
    /// - Parameter postcard: 明信片模型
    /// - Returns: MainDetialSheetController
    class func releaseDetialSheet(postcard: PostcardModel)->MainDetialSheetController{
        
        let sheet: MainDetialSheetController = MainDetialSheetController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        sheet.postcard = postcard
        
        // 发布或取消发布的按钮
        if sheet.postcard?.isOnMarket == false{
            let releaseAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("发布到时空圈", comment: ""), style: UIAlertActionStyle.default, handler: {(_) in
                sheet.releaseClosure?()
            })
            sheet.addAction(releaseAction)
        }else{
            let cancelReleaseAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消发布", comment: ""), style: UIAlertActionStyle.default, handler: {(_) in
                sheet.cancelReleaseClosure?()
            })
            sheet.addAction(cancelReleaseAction)
        }
        
        //删除按钮
        let deleteAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("删除", comment: ""), style: UIAlertActionStyle.destructive, handler: { (_) in
            sheet.deleteClosure?()
        })
        sheet.addAction(deleteAction)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        sheet.addAction(cancelAction)
        
        return sheet
    }
}
