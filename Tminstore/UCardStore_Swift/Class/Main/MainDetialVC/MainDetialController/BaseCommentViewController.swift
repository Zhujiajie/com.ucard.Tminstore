//
//  BaseCommentViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MJRefresh
import SlackTextViewController

class BaseCommentViewController: SLKTextViewController {
    
    /// 动画
    fileprivate let transition: PopAnimator = PopAnimator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        self.isInverted = false//tableView是否颠倒展示
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium)
        } else {
            font = UIFont.systemFont(ofSize: 18)
        }
        let str: String = NSLocalizedString("发送", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: font, NSForegroundColorAttributeName: UIColor(hexString: "4CDFED")]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        self.rightButton.setAttributedTitle(attStr, for: UIControlState.normal)//发送按钮的标题
        
        self.textInputbar.maxCharCount = 200
        self.textInputbar.counterStyle = SLKCounterStyle.countdown
        self.textInputbar.counterPosition = SLKCounterPosition.top
        
        self.tableView?.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView?.estimatedRowHeight = 60
        
        self.tableView?.mj_footer = BaseRefreshFooter()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dismissSVProgress()
    }
    
    //MARK:设置导航栏的左击事件
    func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: 导航栏右按钮点击事件
    /**
     导航栏右按钮点击事件
     
     - parameter button:
     */
    func navigationBarRightBarButtonAction(_ button: UIBarButtonItem){
        
    }
    
    //MARK: 计算tableHeaderView的高度
    /**
     计算tableHeaderView的高度
     */
    func sizeHeaderToFit(tableHeaderView: UIView) {
        
        tableHeaderView.setNeedsLayout()
        tableHeaderView.layoutIfNeeded()
        
        let height: CGFloat? = tableHeaderView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        
        tableHeaderView.frame.size.height = height!
    }
    
    //MARK: 成功的提示框
    /// 展示复制成功的提示框，时长1秒钟
    func showCopySuccessProgress(){
        SVProgressHUD.showSuccess(withStatus: NSLocalizedString("复制成功", comment: ""))
        delay(seconds: 1.0, completion: { [weak self] _ in
            self?.dismissSVProgress()
        })
    }
    
    //MARK: 展示加载器
    /**
     展示加载器
     
     - parameter title:    加载器上的标题
     - parameter maskType: mask类型，默认为.Clear
     */
    func showSVProgress(title: String?, andMaskType maskType :SVProgressHUDMaskType = .clear){
        if title == nil {
            SVProgressHUD.show()
        }else{
            SVProgressHUD.show(withStatus: NSLocalizedString(title!, comment: ""))
        }
        SVProgressHUD.setDefaultMaskType(maskType)
        SVProgressHUD.setDefaultStyle(.dark)
    }
    
    //MARK: 隐藏加载指示器
    /**
     隐藏加载指示器
     */
    func dismissSVProgress(){
        SVProgressHUD.dismiss()
        SVProgressHUD.setDefaultMaskType(.none)
    }
    
    //MARK: 退出
    /// dismissViewController
    ///
    /// - parameter animated:          是否需要动画，默认true
    /// - parameter completionClosure: dismiss完成后的闭包
    func dismissSelf(animated: Bool = true, completion completionClosure: (()->())? = nil){
        let rootVC: UIViewController? = self.presentingViewController
        rootVC?.dismiss(animated: animated, completion: nil)
        completionClosure?()
    }
    
    //MARK:------------处理网络请求失败-----------------
    /**
     处理网络请求失败，token过期则让用户重新登陆。其他错误则弹窗
     
     - parameter error:        alamofire返回的错误
     - parameter loginSuccess: 重新登录成功的闭包
     */
    func handelNetWorkError(_ error: Error? = nil, withResult result: [String: Any]? = nil, loginSuccessClosure loginSuccess: (()->())? = nil){
        //提醒用户出错
        if #available(iOS 10.0, *) {
            let notificationGenerator: UINotificationFeedbackGenerator = UINotificationFeedbackGenerator()
            notificationGenerator.prepare()
            notificationGenerator.notificationOccurred(UINotificationFeedbackType.error)
        }
        if let message: String = result?["message"] as? String{// 打印Java后台返回的错误
            alert(alertTitle: message)
            return
        }else{
            //网络错误
            if error != nil{
                alert(alertTitle: error?.localizedDescription)
            }else{
                alert(alertTitle: NSLocalizedString("网络出错，请稍后再试", comment: ""))
            }
        }
    }
    
    //MARK:------------------------创建提示框-----------------------------
    //MARK: 创建提示框
    /**
     创建提示框
     
     - parameter title:      标题
     - parameter message:    信息
     - parameter leftTitle:  左按钮标题 默认 字符串 “OK” or “好的”
     - parameter rightTitle: 右按钮标题
     - parameter left:       左按钮点击事件
     - parameter right:      右按钮点击事件
     - parameter complition: 提示框弹出完成之后的闭包
     */
    func alert(alertTitle title: String?, messageString message: String? = nil, leftButtonTitle leftTitle: String = NSLocalizedString("好的", comment: ""), rightButtonTitle rightTitle: String? = nil, leftClosure left: (()->())? = nil, rightClosure right: (()->())? = nil, presentComplition complition: (()->())? = nil){
        
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        
        let leftAc: UIAlertAction = UIAlertAction(title: leftTitle, style: .cancel) { (_) in
            left?()
        }// 左按钮
        ac.addAction(leftAc)
        
        if (rightTitle != nil) {
            let rightAc: UIAlertAction = UIAlertAction(title: rightTitle, style: .default, handler: { (_) in
                right?()
            })//右按钮
            ac.addAction(rightAc)
        }
        
        present(ac, animated: true) {
            complition?()
        }//提示框弹出结束后的事件
    }
    
    //MARK: 创建删除提示框
    /**
     创建删除提示框
     
     - parameter title:      标题
     - parameter message:    信息
     - parameter leftTitle:  左按钮标题
     - parameter rightTitle: 右按钮标题
     - parameter left:       左按钮点击事件
     - parameter right:      右按钮点击事件
     - parameter complition: 提示框弹出完成之后的闭包
     */
    func deleteAlert(alertTitle title: String?, messageString message: String?, leftClosure left: (()->())?, rightClosure right: (()->())?, presentCompletion completion: (()->())? = nil){
        
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let leftAc: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: .cancel) { (_) in
            left?()
        }// 左按钮
        ac.addAction(leftAc)
        
        let rightAc: UIAlertAction = UIAlertAction(title: NSLocalizedString("删除", comment: ""), style: .destructive, handler: { (_) in
            right?()
        })//删除按钮
        ac.addAction(rightAc)
        
        present(ac, animated: true) {
            completion?()
        }//提示框弹出结束后的事件
    }
    
    //MARK: 回复的sheet
    /// 回复的sheet
    ///
    /// - Parameters:
    ///   - title: 标题
    ///   - reply: 回复按钮
    ///   - copy: 复制按钮
    func replySheet(title: String, replyClosure reply: (()->())?, copyClosure copy: (()->())?){
        let sheet: UIAlertController = UIAlertController(title: title, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let replyAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("回复", comment: ""), style: UIAlertActionStyle.default) { (_) in
            reply?()
        }
        sheet.addAction(replyAction)
        let copyAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("复制", comment: ""), style: UIAlertActionStyle.default) { [weak self] (_) in
            copy?()//复制闭包
            //提醒用户复制成功
            if #available(iOS 10.0, *) {
                let notificationGenerator: UINotificationFeedbackGenerator = UINotificationFeedbackGenerator()
                notificationGenerator.prepare()
                notificationGenerator.notificationOccurred(UINotificationFeedbackType.success)
                self?.showCopySuccessProgress()//复制成功的提示框
            }
        }
        sheet.addAction(copyAction)
        let cancenAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        sheet.addAction(cancenAction)
        present(sheet, animated: true, completion: nil)
        CFRunLoopWakeUp(CFRunLoopGetCurrent())
    }
    
    //MARK: 回复或者删除的alertSheet
    /// 根据memberCode判断是否是自己的回复或评论。如果是自己的则提供删除功能。若为他人的，则提供回复和举报功能
    ///
    /// - Parameters:
    ///   - commentModel: 评论模型
    ///   - delete: 删除的闭包
    ///   - reply: 回复的闭包
    ///   - report: 举报的闭包
    func replyOrDeleteSheet(commentModel: CommentModel? = nil, orReplyModel replyModel: ReplyModel? = nil, deleteClosure delete: (()->())? = nil, replyClosure reply: (()->())? = nil, reportClosure report: (()->())? = nil){
        
        var nickName: String
        var content: String
        var memberCode: String
        
        if let comment: CommentModel = commentModel{
            nickName = comment.nickName
            content = comment.content
            memberCode = comment.memberCode
        }else if let reply: ReplyModel = replyModel{
            nickName = reply.replyName
            content = reply.content
            memberCode = reply.replyMemberCode
        }else{
            return
        }
        
        let sheet: UIAlertController = UIAlertController(title: "\(nickName): \(content)", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        if memberCode == getUserID(){
            //删除
            let deleteAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("删除", comment: ""), style: UIAlertActionStyle.destructive, handler: { (_) in
                delete?()
            })
            sheet.addAction(deleteAction)
            
        }else{
            //回复
            let replyAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("回复", comment: ""), style: UIAlertActionStyle.default) { (_) in
                reply?()
            }
            sheet.addAction(replyAction)
            
            //如果不是自己发表的。可以举报
            if memberCode != getUserID(){
                //举报
                let reportAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("举报", comment: ""), style: UIAlertActionStyle.default, handler: { (_) in
                    report?()
                })
                sheet.addAction(reportAction)
            }
        }
        
        // 复制
        let copyAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("复制", comment: ""), style: UIAlertActionStyle.default) { [weak self] (_) in
            UIPasteboard.general.string = content//复制
            //提醒用户复制成功
            if #available(iOS 10.0, *) {
                let notificationGenerator: UINotificationFeedbackGenerator = UINotificationFeedbackGenerator()
                notificationGenerator.prepare()
                notificationGenerator.notificationOccurred(UINotificationFeedbackType.success)
                self?.showCopySuccessProgress()//复制成功的提示框
            }
        }
        sheet.addAction(copyAction)
        let cancenAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
        sheet.addAction(cancenAction)
        present(sheet, animated: true, completion: nil)
        CFRunLoopWakeUp(CFRunLoopGetCurrent())
    }
}

//MARK: 动画相关
extension BaseCommentViewController: UIViewControllerTransitioningDelegate{
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        guard let vc: MainCommitVC = presented as? MainCommitVC else { return nil }
        transition.originFrame = vc.cellFrame//获取cell当前的frame
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
}
