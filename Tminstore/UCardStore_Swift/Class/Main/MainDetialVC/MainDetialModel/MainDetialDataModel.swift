//
//  MainDetialDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Alamofire

class MainDetialDataModel: BaseDataModel {
    
    //MARK:获取评论
    /**
     获取评论列表
     
     - parameter parameter: 参数
     - parameter success:    成功的闭包
     - parameter failsure:   失败的闭包
     */
    func getCommentData(originalId: String, AndPageIndex pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        let parameters: [String: Any] = [
            "originalId": originalId,
            "pageIndex": pageIndex
        ]
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.checkCommentAPI, ParametersDictionary: parameters, urlEncoding: URLEncoding.default, successClosure: { (result) in
            success?(result)//成功的闭包
            }, failureClourse: { (error) in
                failsure?(error)//失败的闭包
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK:发送评论
    /**
     发送评论
     
     - parameter parameter: 参数
     - parameter success:    成功的闭包
     - parameter failsure:   失败的闭包
     */
    func postComment(parameter: [String: Any], successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.postCommentApi, ParametersDictionary: parameter, successClosure: {(result) in
            success?(result)
        }, failureClourse: {(error) in
            failsure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 发布回复
    /// 发布回复
    ///
    /// - Parameters:
    ///   - parameter: 参数
    ///   - success: 成功的闭包
    ///   - failsure: 失败的闭包
    ///   - completion: 完成的闭包
    func postReply(parameter: [String: Any], successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.postReplyAPI, ParametersDictionary: parameter, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 查看回复
    /// 查看回复
    ///
    /// - Parameters:
    ///   - pageIndex: 回复页数
    ///   - commentId:
    ///   - success: 成功的闭包
    ///   - failsure: 失败的闭包
    ///   - completion: 完成的闭包
    func getReplyList(pageIndex: Int, andCommentId commentId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "pageIndex": pageIndex,
            "commentId": commentId
            ] as [String: Any]
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.checkReplyAPI, ParametersDictionary: parameters, urlEncoding: URLEncoding.default, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }) {
            completion?()
        }
    }
    
    //MARK: 删除评论的请求
    /// 删除评论的请求
    ///
    /// - Parameters:
    ///   - commentModel: 评论模型
    ///   - success: 成功的闭包
    ///   - failsure: 失败的闭包
    ///   - completion: 完成的闭包
    func deleteComment(commentModel: CommentModel, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "commentId": commentModel.commentId,
            "memberCode": commentModel.memberCode,
            "originalId": commentModel.originalId
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.deleteCommentAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 删除回复的请求
    /// 删除回复的请求
    ///
    /// - Parameters:
    ///   - replyModel: 回复模型
    ///   - originalId: 卡片原始id
    ///   - success: 成功的闭包
    ///   - failsure: 失败的闭包
    ///   - completion: 完成的闭包
    func deleteReply(replyModel: ReplyModel, andOriginalId originalId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "id": replyModel.id,
            "token": getUserToken(),
            "replyMemberCode": replyModel.replyMemberCode,
            "originalId": originalId
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.deleteReplyAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 取消分享的请求
    /// 取消分享的请求
    ///
    /// - Parameters:
    ///   - originalId: 明信片的ID
    ///   - success: 成功
    ///   - failsure: 失败
    ///   - completion: 完成
    func cancelRelease(originalId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId
        ] as [String: Any]
        
        sendPUTRequestWithURLString(URLStr: appAPIHelp.cancelReleaseAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 删除发布的请求
    /// 删除发布的请求
    ///
    /// - Parameters:
    ///   - originalId: 明信片的ID
    ///   - success: 成功
    ///   - failsure: 失败
    ///   - completion: 完成    
    func deleteRelease(originalId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let urlStr: String = appAPIHelp.deleteReleaseAPI + originalId
        
        let parameters: [String: Any] = [
            "token": getUserToken()
            ] as [String: Any]
        
        sendDELETERequestWithURLString(URLStr: urlStr, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 获取被买图的信息
    /// 获取被买图的信息
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    ///   - failsure: 失败的闭包
    ///   - completion: 完成的闭包
    func getInfoOfPurchasedCard(originalId: String, successClosure success: ((_ postCardArray: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        let urlString: String = appAPIHelp.checkPurchasedCardAPI + originalId
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: {(error: Error?) in
            failsure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 点赞的请求
    /// 点赞的请求
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func sendLikeRequest(originalId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId
            ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.sendLikeAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 发送取消点赞的请求
    /// 发送取消点赞的请求
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func sendCancelLikeRequest(originalId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId
            ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.sendCancelLikeAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
}
