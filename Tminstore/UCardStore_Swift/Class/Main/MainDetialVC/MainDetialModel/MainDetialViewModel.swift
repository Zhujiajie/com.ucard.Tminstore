//
//  MainDetialViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import MJRefresh
import IQKeyboardManagerSwift
import Alamofire

class MainDetialViewModel: BaseViewModel {

    fileprivate let dataModel: MainDetialDataModel = MainDetialDataModel()
    
    /// 卡片的ID
    var originalId: String = ""
    
    /// 评论数组
    var commentArray: [CommentModel] = [CommentModel]()
    
    /// 要获取第几页评论
    var pageIndex: Int = 1
    
    /// 成功获取到评论列表
    var getCommentSuccess: ((_ totalComment: Int)->())?
    
    /// 没有更多数据了
    var noMoreData: (()->())?
    
    /// 得到了更多的数据
    var getMoreData: ((_ indexPathArry: [IndexPath], _ totalComment: Int)->())?
    
    /// 网络错误
    var networkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 评论成功
    var postCommentSuccess: (()->())?
    
    /// 删除评论成功
    var deleteCommmentSuccess: ((_ indexPath: IndexPath)->())?
    
    /// 删除回复成功
    var deleteReplySuccess: ((_ indexPath: IndexPath)->())?
    
    /// 不需要刷新评论数据
    var shouldNotRefreshCommentData: Bool = false
    
    /// 暂时不请求数据
    fileprivate var shouldStopRequestData: Bool = false
    
    /// 取消发布或删除成功
    var cancelReleaseOrDeleteSuccess: (()->())?
    
    /// 获取被买图片的信息成功
    var getPurchasedCardInfoSuccess: ((_ postcard: PostcardModel)->())?
    
    /// 点赞或取消点赞成功
    var sendLikeOrCancelLikeSuccess: ((_ isLiked: Bool)->())?
    
    //MARK: 获取评论列表
    /**
     获取评论列表
     */
    func getCommentList() {
        
        if shouldStopRequestData == true{ return }
        shouldStopRequestData = true
        
        if pageIndex == 1{
            commentArray.removeAll()
            showSVProgress(title: nil)
        }
        
        dataModel.getCommentData(originalId: originalId, AndPageIndex: pageIndex, successClosure: { [weak self] (result: [String: Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                
                let data: [String: Any] = result["data"] as! [String: Any]
                let totalComment: Int = data["total"] as! Int//总评论数
                let commentDics: [[String: Any]] = data["commentRepliyList"] as! [[String: Any]]
                
                if commentDics.isEmpty == true{//没有数据了
                    self?.noMoreData?()
                }else{
                    
                    if self?.pageIndex == 1 {
                        for dic: [String: Any] in commentDics{
                            let comment: CommentModel = CommentModel.initFromDic(dic: dic)
                            self?.commentArray.append(comment)
                        }
                        self?.getCommentSuccess?(totalComment)
                        
                    }else{//获得了更多的数据
                        
                        var newCommentArray: [CommentModel] = [CommentModel]()
                        
                        for dic: [String: Any] in commentDics{
                            let comment: CommentModel = CommentModel.initFromDic(dic: dic)
                            newCommentArray.append(comment)
                        }
                        
                        let currentRow: Int = (self?.commentArray.count)!
                        let newCommentCount: Int = newCommentArray.count
                        
                        var indexPathArray: [IndexPath] = [IndexPath]()
                        for i: Int in 0..<newCommentCount{
                            let newIndexPath: IndexPath = IndexPath(row: currentRow + i, section: 0)
                            indexPathArray.append(newIndexPath)
                        }
                        self?.commentArray.append(contentsOf: newCommentArray)
                        self?.getMoreData?(indexPathArray, totalComment)
                    }
                    
                    self?.pageIndex += 1
                }
            }else{
                self?.networkError?(nil, result)
            }
            
        }, failureClosure: { [weak self](error: Error?) in
            self?.networkError?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
            self?.shouldStopRequestData = false
        }
    }
    
    
    //MARK:发送评论
    /**
     发送评论
     
     - parameter Comment: 评论内容
     - parameter success: 成功的闭包
     - parameter failure: 失败的闭包
     */
    func postComment(content: String) {
        
        showSVProgress(title: nil)
        
        let parameters: [String: Any] = [
            "originalId": originalId,
            "content": content,
            "token": getUserToken()
            ] as [String : Any]
        
        dataModel.postComment(parameter: parameters, successClosure: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.postCommentSuccess?()
                self?.pageIndex = 1
                self?.getCommentList()
            }else{
                self?.networkError?(nil, result)
            }
            }, failureClosure: { [weak self](error: Error?) in
            self?.networkError?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 发布回复
    /// 发布回复
    ///
    /// - Parameters:
    ///   - replyContent: 回复内容
    ///   - comment: 评论模型
    func postReply(replyContent: String, commentModel comment: CommentModel, andToMemberCode toMemberCode: String? = nil){
        
        showSVProgress(title: nil)
        
        var code: String
        if toMemberCode != nil{
            code = toMemberCode!
        }else{
            code = comment.memberCode
        }
        
        let parameters: [String: Any] = [
            "originalId": comment.originalId,
            "commentId": comment.commentId,
            "replyContent": replyContent,
            "token": getUserToken(),
            "toMemberCode": code
        ] as [String: Any]
        
        dataModel.postReply(parameter: parameters, successClosure: { [weak self](result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.postCommentSuccess?()
                if self?.shouldNotRefreshCommentData == false{
                    self?.pageIndex = 1
                    self?.getCommentList()
                }
            }else{
                self?.networkError?(nil, result)
            }
            }, failureClosure: { [weak self](error: Error?) in
            self?.networkError?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 删除评论的请求
    /// 删除评论的请求
    ///
    /// - Parameters:
    ///   - commentModel: 评论模型
    ///   - indexPath: IndexPath
    func deleteComment(commentModel: CommentModel, andIndexPath indexPath: IndexPath){
        dataModel.deleteComment(commentModel: commentModel, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.deleteCommmentSuccess?(indexPath)
            }else{
                self?.networkError?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
            self?.networkError?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 删除回复的请求
    /// 删除回复的请求
    ///
    /// - Parameters:
    ///   - replyModel: 回复模型
    ///   - originalId: 原始id
    ///   - indexPath: IndexPath
    func deleteReply(replyModel: ReplyModel, andOriginalId originalId: String, andIndexPath indexPath: IndexPath){
        dataModel.deleteReply(replyModel: replyModel, andOriginalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.deleteReplySuccess?(indexPath)
            }else{
                self?.networkError?(nil, result)
            }
            }, failureClosure: { [weak self](error: Error?) in
            self?.networkError?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 取消分享
    /// 取消分享
    ///
    /// - Parameter originalId: 原创ID
    func cancelRelease(originalId: String) {
        
        showSVProgress(title: nil)
        
        dataModel.cancelRelease(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.cancelReleaseOrDeleteSuccess?()
            }else{
                self?.networkError?(nil, result)
            }
            
            }, failureClosure: { [weak self] (error: Error?) in
            self?.networkError?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 删除发布的请求
    /// 删除发布的请求
    ///
    /// - Parameter originalId: 原创ID
    func deleteRelease(originalId: String){
        showSVProgress(title: nil)
        dataModel.deleteRelease(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.cancelReleaseOrDeleteSuccess?()
            }else{
                self?.networkError?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
                self?.networkError?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 获取被买图的信息
    /// 获取被买图的信息
    ///
    /// - Parameter originalId: 原创ID
    func getPurchasedCardInfo(originalId: String){
        showSVProgress(title: nil)
        dataModel.getInfoOfPurchasedCard(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let dic: [String: Any] = data["community"] as! [String: Any]
                let postcard: PostcardModel = PostcardModel.initFromDic(dic: dic)
                self?.getPurchasedCardInfoSuccess?(postcard)
            }else{
                self?.networkError?(nil, result)
            }
        }, failureClosure: { [weak self] (error: Error?) in
            self?.networkError?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 发送点赞请求
    /// 发送点赞请求
    ///
    /// - Parameter originalId: originalId
    func sendLikeRequest(originalId: String){
        dataModel.sendLikeRequest(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.sendLikeOrCancelLikeSuccess?(true)
            }else{
                self?.networkError?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
                self?.networkError?(error, nil)
            }, completionClosure: nil)
    }
    
    //MARK: 发送取消点赞的请求
    /// 发送取消点赞的请求
    ///
    /// - Parameter originalId: originalId
    func sendCancelLikeRequest(originalId: String){
        dataModel.sendCancelLikeRequest(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.sendLikeOrCancelLikeSuccess?(false)
            }else{
                self?.networkError?(nil, result)
            }
            }, failureClosure: { [weak self] (error: Error?) in
                self?.networkError?(error, nil)
            }, completionClosure: nil)
    }
}
