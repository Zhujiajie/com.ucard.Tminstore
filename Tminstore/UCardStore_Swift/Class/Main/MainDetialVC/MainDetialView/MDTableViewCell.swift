//
//  MDTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/5.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MDTableViewCell: UITableViewCell {

    /// 评论内容
    var postcardComment: CommentModel?{
        didSet{
            setData()//设置cell的数据
        }
    }
    
    /// 点击了回复区域
    var tapToReply: (()->())?
    
    /// 昵称
    fileprivate var nameLabel: UILabel?
    /// 评论
    fileprivate var commentLabel: UILabel?
    /// 头像
    fileprivate var portraitView: UIImageView?
    /// 评论时间
    fileprivate var createdAtLable: UILabel?
    /// 展示回复的view
    fileprivate var grayContainerView: UIView?
    /// 回复者的头像
    fileprivate let replyUserPortraitViewArray: [UIImageView] = [UIImageView(), UIImageView(), UIImageView(), UIImageView(), UIImageView()]
    /// 回复者的昵称
    fileprivate let replyUserNameLabelArray: [UILabel] = [UILabel(), UILabel(), UILabel(), UILabel(), UILabel()]
    /// 回复时间
    fileprivate let replyDateLabelArray: [UILabel] = [UILabel(), UILabel(), UILabel(), UILabel(), UILabel()]
    /// 回复者的回复内容
    fileprivate let replyContentLabelArray: [UILabel] = [UILabel(), UILabel(), UILabel(), UILabel(), UILabel()]
    
    /// 当回复数超过5条时，显示的label
    fileprivate var totalReplyLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setCell()
        
        // 设置回复UI
        for i: Int in 0..<replyUserPortraitViewArray.count {
            replyUserPortraitViewArray[i].contentMode = UIViewContentMode.scaleAspectFill
            replyUserPortraitViewArray[i].layer.cornerRadius = 26/baseWidth/2
            replyUserPortraitViewArray[i].layer.masksToBounds = true
            
            replyUserNameLabelArray[i].textColor = UIColor(hexString: "5AB8CD")
            replyUserNameLabelArray[i].font = UIFont.systemFont(ofSize: 12)
            replyUserNameLabelArray[i].numberOfLines = 1
            
            replyDateLabelArray[i].textColor = UIColor(hexString: "ADADAD")
            replyDateLabelArray[i].font = UIFont.systemFont(ofSize: 8)
            replyDateLabelArray[i].numberOfLines = 1
            replyDateLabelArray[i].textAlignment = NSTextAlignment.right
            
            replyContentLabelArray[i].numberOfLines = 2
            replyContentLabelArray[i].font = UIFont.systemFont(ofSize: 10)
        }
        
        totalReplyLabel = UILabel()
        totalReplyLabel?.textColor = UIColor(hexString: "4CDFED")
        totalReplyLabel?.font = UIFont.systemFont(ofSize: 10)
    }
    
    /**
     设置cell的UI
     */
    fileprivate func setCell() {
        
        //头像
        portraitView = UIImageView()
        portraitView?.contentMode = UIViewContentMode.scaleAspectFill
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView.snp.top).offset(10/baseHeight)
            make.left.equalTo(contentView.snp.left).offset(10/baseWidth)
            make.size.equalTo(CGSize(width: 37, height: 37))
        })
        portraitView?.layer.cornerRadius = 18.5//添加圆角
        portraitView?.layer.masksToBounds = true

        //评论时间的label
        createdAtLable = UILabel()
        createdAtLable?.textColor = UIColor(hexString: "ADADAD")
        if #available(iOS 8.2, *) {
            createdAtLable?.font = UIFont.systemFont(ofSize: 11, weight: UIFontWeightSemibold)
        } else {
            createdAtLable?.font = UIFont.systemFont(ofSize: 11)
        }
        contentView.addSubview(createdAtLable!)
        createdAtLable?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(12/baseHeight)
            make.right.equalTo(contentView).offset(-7/baseWidth)
            make.height.equalTo(16)
        })
        
        //用户昵称
        nameLabel = UILabel()
        nameLabel?.textColor = UIColor(hexString: "545454")
        if #available(iOS 8.2, *) {
            nameLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
        } else {
            nameLabel?.font = UIFont.systemFont(ofSize: 14)
        }
        nameLabel?.textAlignment = NSTextAlignment.left
        contentView.addSubview(nameLabel!)
        nameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(10/baseHeight)
            make.left.equalTo(portraitView!.snp.right).offset(5/baseWidth)
            make.right.equalTo(contentView).offset(-5/baseWidth)
            make.height.equalTo(17)
        })
        
        //用户评论
        commentLabel = UILabel()
        commentLabel?.textColor = UIColor(hexString: "676767")
        if #available(iOS 8.2, *) {
            commentLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightLight)
        } else {
            commentLabel?.font = UIFont.systemFont(ofSize: 12)
        }
        commentLabel?.textAlignment = NSTextAlignment.left
        commentLabel?.numberOfLines = 40
        contentView.addSubview(commentLabel!)
        commentLabel?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(nameLabel!.snp.bottom).offset(7/baseHeight)
            make.leftMargin.equalTo(nameLabel!)
            make.bottom.equalTo(contentView.snp.bottom).offset(-10/baseHeight)
            make.rightMargin.equalTo(createdAtLable!)
        }
        
        //底部分割线
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "E0E0E0")
        grayLine.alpha = 0.5
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(contentView)
            make.leftMargin.equalTo(nameLabel!)
            make.right.equalTo(contentView)
            make.height.equalTo(1)
        }
    }
    
    //MARK:设置cell的数据
    /**
     设置cell的数据
     */
    fileprivate func setData(){
        // 获取评论时间
        if let commentDate: Date = postcardComment?.createDate{
            createdAtLable?.text = getDateText(date: commentDate)
        }
        
        //获取头像
        if postcardComment?.userLogo != nil {
            portraitView?.sd_setImage(with: postcardComment!.userLogo!, placeholderImage: #imageLiteral(resourceName: "defaultLogo"))
        }else{
            portraitView?.image = #imageLiteral(resourceName: "defaultLogo")
        }
        
        //获取昵称
        nameLabel?.text = postcardComment?.nickName
        
        //获取评论
        let commentStr = postcardComment?.content
        let attStr = NSMutableAttributedString(string: commentStr!)

        //如果评论中包含@...字段，则将这段字段加粗
        if commentStr?.characters.first == "@" {
            let index = commentStr?.range(of: " ")
            let intValue: Int = (commentStr?.characters.distance(from: (commentStr?.startIndex)!, to: (index?.lowerBound)!))!
            let attDic = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor.black] as [String: Any]
            let range = NSMakeRange(0, intValue)
            attStr.addAttributes(attDic, range: range)
        }

        commentLabel?.attributedText = attStr
        
        // 回复内容不为空
        if postcardComment?.replyArray.isEmpty == false{
            // 上方的评论
            commentLabel?.snp.remakeConstraints({ (make: ConstraintMaker) in
                commentLabel?.snp.remakeConstraints { (make: ConstraintMaker) in
                    make.top.equalTo(nameLabel!.snp.bottom).offset(7/baseHeight)
                    make.leftMargin.equalTo(nameLabel!)
                    make.rightMargin.equalTo(createdAtLable!)
                }
            })
            serReplyUIAndData()
        }else{
            commentLabel?.snp.remakeConstraints { (make: ConstraintMaker) in
                make.top.equalTo(nameLabel!.snp.bottom).offset(7/baseHeight)
                make.leftMargin.equalTo(nameLabel!)
                make.bottom.equalTo(contentView.snp.bottom).offset(-10/baseHeight)
                make.rightMargin.equalTo(createdAtLable!)
            }
        }
    }
    
    //MARK: 设置回复数据
    fileprivate func getReplyConent(reply: ReplyModel)->NSMutableAttributedString{
        
        var str: NSMutableAttributedString//评论内容
        var replyString: String//评论开头
        if languageCode() == "CN"{
            replyString = "回复"
        }else{
            replyString = "Reply"
        }
        str = NSMutableAttributedString(string: replyString + "@" + reply.toReplyName + reply.content)
        let replyName: NSAttributedString = NSAttributedString(string: reply.toReplyName)
        let replyContent: NSAttributedString = NSAttributedString(string: reply.content)
        let range1: NSRange = NSRange(location: 0, length: replyString.characters.count)// 回复
        let range2: NSRange = NSRange(location: replyString.characters.count, length: replyName.length + 1)// 回复某人的名字
        let range3: NSRange = NSRange(location: replyString.characters.count + 1 + replyName.length, length: replyContent.length)//评论详细内容
        
        str.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 10), NSForegroundColorAttributeName: UIColor(hexString: "808080")] as [String: Any], range: range1)
        str.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 10), NSForegroundColorAttributeName: UIColor(hexString: "6AC4D7")] as [String: Any], range: range2)
        str.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 10), NSForegroundColorAttributeName: UIColor(hexString: "808080")] as [String: Any], range: range3)
       
        return str
    }
    
    //MARK: 设置回复的UI
    fileprivate func serReplyUIAndData(){
        
        grayContainerView = UIView()
        grayContainerView?.backgroundColor = UIColor(hexString: "F4F4F4")
        contentView.addSubview(grayContainerView!)
        grayContainerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(commentLabel!.snp.bottom).offset(10/baseHeight)
            make.left.equalTo(contentView).offset(10/baseWidth)
            make.bottom.equalTo(contentView).offset(-12/baseHeight)
            make.right.equalTo(contentView).offset(-10/baseWidth)
        })
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapReplyView(tap:)))
        grayContainerView?.addGestureRecognizer(tap)
        
        for i: Int in 0..<postcardComment!.replyArray.count{
            
            grayContainerView?.addSubview(replyUserPortraitViewArray[i])
            grayContainerView?.addSubview(replyUserNameLabelArray[i])
            grayContainerView?.addSubview(replyDateLabelArray[i])
            grayContainerView?.addSubview(replyContentLabelArray[i])
            
            replyUserPortraitViewArray[i].snp.makeConstraints({ (make: ConstraintMaker) in
                if i == 0{
                    make.top.equalTo(grayContainerView!).offset(4/baseHeight)
                }else{
                    make.top.equalTo(replyContentLabelArray[i - 1].snp.bottom).offset(9/baseHeight)
                }
                make.left.equalTo(grayContainerView!).offset(6/baseWidth)
                make.size.equalTo(CGSize(width: 26/baseWidth, height: 26/baseWidth))
            })
            replyDateLabelArray[i].snp.makeConstraints({ (make: ConstraintMaker) in
                make.topMargin.equalTo(replyUserPortraitViewArray[i]).offset(3/baseHeight)
                make.right.equalTo(grayContainerView!).offset(-10/baseWidth)
            })
            // 设置回复时间
            if let replyDate: Date = postcardComment?.replyArray[i].createDate{
                replyDateLabelArray[i].text = getDateText(date: replyDate)
            }
            replyUserNameLabelArray[i].snp.makeConstraints({ (make: ConstraintMaker) in
                make.topMargin.equalTo(replyUserPortraitViewArray[i]).offset(2/baseWidth)
                make.left.equalTo(replyUserPortraitViewArray[i].snp.right).offset(8/baseWidth)
                make.right.equalTo(replyDateLabelArray[i].snp.left).offset(-10/baseWidth)
            })
            
            replyContentLabelArray[i].snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(replyUserNameLabelArray[i].snp.bottom).offset(4/baseHeight)
                make.leftMargin.equalTo(replyUserNameLabelArray[i])
                make.rightMargin.equalTo(replyUserNameLabelArray[i])
                if i == postcardComment!.replyArray.count - 1 && postcardComment!.replyTotal < 6{
                    make.bottom.equalTo(grayContainerView!).offset(-14/baseHeight)
                }
            })
            
            if postcardComment!.replyTotal > 5 && i == 4{
                grayContainerView?.addSubview(totalReplyLabel!)
                totalReplyLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
                    make.top.equalTo(replyContentLabelArray[4].snp.bottom).offset(10/baseHeight)
                    make.leftMargin.equalTo(replyContentLabelArray[4])
                    make.bottom.equalTo(grayContainerView!).offset(-12/baseHeight)
                })
                
                if languageCode() == "CN"{
                    totalReplyLabel?.text = "共\(postcardComment!.replyTotal)条回复>"
                }else{
                    totalReplyLabel?.text = "\(postcardComment!.replyTotal) replies total>"
                }
            }
            
            // 设置头像等数据
            if let url: URL = postcardComment?.replyArray[i].userLogoURL{
                replyUserPortraitViewArray[i].sd_setImage(with: url, placeholderImage: UIImage(named: "defaultLogo"))
            }
            replyUserNameLabelArray[i].text = postcardComment?.replyArray[i].replyName
            replyContentLabelArray[i].attributedText = getReplyConent(reply: postcardComment!.replyArray[i])
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        portraitView?.image = nil
        nameLabel?.text = nil
        commentLabel?.text = nil
        createdAtLable?.text = nil
        
        grayContainerView?.removeFromSuperview()
        
        for i: Int in 0..<replyUserNameLabelArray.count{
            replyUserNameLabelArray[i].text = nil
            replyUserPortraitViewArray[i].image = nil
            replyDateLabelArray[i].text = nil
            replyContentLabelArray[i].text = nil
        }
        totalReplyLabel?.removeFromSuperview()
    }
    
    //MARK: 点击灰色视图，跳转到回复界面
    func tapReplyView(tap: UITapGestureRecognizer){
        tapToReply?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
