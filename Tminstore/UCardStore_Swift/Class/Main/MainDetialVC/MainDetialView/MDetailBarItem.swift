//
//  PDetailBarItem.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/10.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MDetailBarItem: UIBarButtonItem {

    ///点击按钮的事件
    var touchBarItemClosure: (()->())?
    
    //MARK: 返回的按钮
    /// 返回的按钮
    ///
    /// - Returns: PDetailBarItem
    class func backBarItem()->MDetailBarItem{
        
        let barItem: MDetailBarItem = MDetailBarItem()
        barItem.image = UIImage(named: "backArrow")
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchBarItem(barItem:))
        barItem.tintColor = UIColor(hexString: "4CDFED")
        
        return barItem
    }
    
    //MARK: 点击进入AR扫描界面的按钮
    /// 点击进入AR扫描界面的按钮
    ///
    /// - Returns: PDetailBarItem
    class func enterARBarItem()->MDetailBarItem{
        let barItem: MDetailBarItem = MDetailBarItem()
        barItem.image = UIImage(named: "ARInDetail")
        barItem.style = UIBarButtonItemStyle.plain
        barItem.target = barItem
        barItem.action = #selector(barItem.touchBarItem(barItem:))
        barItem.tintColor = UIColor(hexString: "4CDFED")
        return barItem
    }
    
    //MARK: 点击按钮的事件
    func touchBarItem(barItem: UIBarButtonItem) {
        touchBarItemClosure?()
    }
}
