//
//  MainReplyController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/19.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MainReplyController: BaseCommentViewController {

    fileprivate let getReplyViewModel: MainReplyViewModel = MainReplyViewModel()
    fileprivate let postReplyViewModel: MainDetialViewModel = MainDetialViewModel()

    /// 成功发表回复
    var postReplySuccess: (()->())?
    
    /// 评论模型
    var commentModel: CommentModel?
    /// 被回复的人的memberCode
    var toMemberCode: String = ""

    /// 原创ID
    var originalId: String = ""
    /// 评论ID
    var commentId: String = ""
    /// 是否需要使用dismissSelf方法
    var shouldUseDismiss: Bool = false
    
    /// 展示评论的视图
    fileprivate var tableHeaderView: MainReplyView?
    /// 举报的提示框
    fileprivate var reportAlert: MainReportAlertVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()//设置导航栏
        
        self.tableView?.register(MainReplyTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MainReplyTableViewCellReuseIdentifier)
        
        if commentModel != nil{
            setUIOfTableView()
            toMemberCode = commentModel!.memberCode
        }
        closures()//闭包回调
    }
    
    //MARK: tableView的UI
    fileprivate func setUIOfTableView(){
        self.textView.placeholder = "@\(commentModel!.nickName)"
        tableHeaderView = MainReplyView.setCommentView(commentModel: commentModel!)
        sizeHeaderToFit(tableHeaderView: tableHeaderView!)//计算tableHeaderView的高度
        tableView?.tableHeaderView = tableHeaderView
        
        //MARK: 点击commentView去回复评论
        tableHeaderView?.tapToReply = {[weak self] _ in
            self?.emptyReply()
            self?.textView.becomeFirstResponder()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCommentData()//请求数据
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK:设置导航栏
    fileprivate func setNavigationBar() {
        navigationController?.navigationBar.tintColor = appThemeColor()
        //设置左按钮
        let button: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "backArrow"), style: .plain, target: self, action: #selector(navigationBarLeftBarButtonAction(_:)))
        button.tintColor = UIColor(hexString: "4D4D4D")
        navigationItem.leftBarButtonItem = button
    }
    
    //MARK: 点击导航栏做按钮，退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        if shouldUseDismiss == true{
            dismissSelf()
        }else{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: 设置导航栏的标题
    fileprivate func setNavigationBarTitle(){
        // 设置标题
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D")] as [String: Any]
        
        var title: String
        if languageCode() == "CN"{
            title = "\(getReplyViewModel.replyArray.count)条回复"
        }else{
            title = "\(getReplyViewModel.replyArray.count)Replies"
        }
        navigationItem.title = title
        navigationController?.navigationBar.titleTextAttributes = attDic
    }
    
    //MARK: 各种闭包回调
    fileprivate func closures(){
        
        /// 无需更新评论数据
        postReplyViewModel.shouldNotRefreshCommentData = true
        
        //MARK: 上拉获取更多数据
        self.tableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.getReplyViewModel.getReplyList()
        }
        
        //MARK: 成功获取到评论列表，tableView需要重新载入
        getReplyViewModel.getReplySuccess = {[weak self] _ in
            if self?.commentModel == nil{
                self?.commentModel = self?.getReplyViewModel.commentModel
                self?.toMemberCode = (self?.commentModel?.memberCode)!
                self?.setUIOfTableView()
            }
            self?.tableView?.reloadData()
            self?.setNavigationBarTitle()
        }
        
        //MARK: 没有更多数据了
        getReplyViewModel.noMoreData = {[weak self] _ in
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 获得了更多的数据
        getReplyViewModel.getMoreData = {[weak self] (indexPathArray: [IndexPath]) in
            self?.tableView?.mj_footer.endRefreshing()
            self?.tableView?.beginUpdates()
            self?.tableView?.insertRows(at: indexPathArray, with: UITableViewRowAnimation.automatic)
            self?.tableView?.endUpdates()
            self?.setNavigationBarTitle()
        }
        
        //MARK: 网络错误
        getReplyViewModel.networkError = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.tableView?.mj_footer.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        //MARK: 发表评论或回复成功
        postReplyViewModel.postCommentSuccess = {[weak self] _ in
            self?.postReplySuccess?()
            self?.emptyReply()
            self?.getReplyViewModel.pageIndex = 1
            self?.getReplyViewModel.getReplyList()
        }
        
        //MARK: 删除回复成功
        postReplyViewModel.deleteReplySuccess = {[weak self] (indexPath: IndexPath) in
            self?.getReplyViewModel.replyArray.remove(at: indexPath.row)
            self?.tableView?.beginUpdates()
            self?.tableView?.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            self?.tableView?.endUpdates()
        }
        
//        //MARK: 获得评论成功
//        postReplyViewModel.getCommentSuccess = {[weak self] (_) in
//            for comment: CommentModel in (self?.postReplyViewModel.commentArray)!{
//                if comment.commentId == (self?.commentId)!{
//                    self?.commentModel = comment
//                    self?.getCommentData()//获取评论数据
//                }
//            }
//        }
    }
    
    //MARK: 获取回复数据
    fileprivate func getCommentData(){
        if commentModel != nil{
            // 进入该页面时，刷新数据
            getReplyViewModel.commentId = commentModel!.commentId
        }else{
            getReplyViewModel.commentId = commentId
        }
        getReplyViewModel.pageIndex = 1
        getReplyViewModel.getReplyList()//请求数据
    }
    
    //MARK: 清空回复状态
    fileprivate func emptyReply(){
        self.textView.placeholder = "@\(commentModel!.nickName)"
        toMemberCode = commentModel!.memberCode
    }
    
    //MARK: 停止编辑时，改回原来的占位符
    override func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            emptyReply()
        }
    }
    
    //MARK: 点击右按钮，发布评论
    override func didPressRightButton(_ sender: Any?) {
        self.textView.refreshFirstResponder()
        self.textView.resignFirstResponder()
        postReplyViewModel.postReply(replyContent: self.textView.text, commentModel: commentModel!, andToMemberCode: toMemberCode)
        toMemberCode = commentModel!.memberCode
        super.didPressRightButton(sender)
    }
    
    //MARK: 举报的sheet
    fileprivate func showReportAlert(replyModel: ReplyModel){
        reportAlert = MainReportAlertVC(title: NSLocalizedString("举报", comment: ""), message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        reportAlert?.replyModel = replyModel
        present(reportAlert!, animated: true, completion: nil)
        reportAlert?.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }//举报的请求出错
        reportAlert?.reportSuccess = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("感谢您的举报", comment: ""))
        }//举报成功
    }
}

extension MainReplyController{
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        self.tableView?.mj_footer.isHidden = getReplyViewModel.replyArray.isEmpty
        return getReplyViewModel.replyArray.count
    }
    //MARK: 设置cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell: MainReplyTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MainReplyTableViewCellReuseIdentifier, for: indexPath) as! MainReplyTableViewCell
        cell.reply = getReplyViewModel.replyArray[indexPath.row]//设置cell的数据源
        return cell
    }
    // MARK: 点击cell，回复某人
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let reply: ReplyModel = getReplyViewModel.replyArray[indexPath.row]
        
        replyOrDeleteSheet(orReplyModel: reply, deleteClosure: { [weak self] _ in
            self?.postReplyViewModel.deleteReply(replyModel: reply, andOriginalId: (self?.commentModel?.originalId)!, andIndexPath: indexPath)
        }, replyClosure: { [weak self] _ in
            self?.textView.placeholder = "@\(reply.replyName)"
            self?.textView.text = ""//清空原先的内容
            self?.textView.becomeFirstResponder()//输入回复的内容
            self?.toMemberCode = reply.replyMemberCode//正在回复的评论
            }, reportClosure: {[weak self] _ in
                self?.showReportAlert(replyModel: reply)
        })
        
        
//        let title: String = "\(reply.replyName): \(reply.content)"
//        replySheet(title: title, replyClosure: { [weak self] _ in
//            self?.textView.placeholder = "@\(reply.replyName)"
//            self?.textView.text = ""//清空原先的内容
//            self?.textView.becomeFirstResponder()//输入回复的内容
//            self?.toMemberCode = reply.replyMemberCode//正在回复的评论
//        }) {
//            UIPasteboard.general.string = reply.content//复制
//        }
    }
}
