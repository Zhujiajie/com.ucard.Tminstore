//
//  MainReplyView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/19.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MainReplyView: UIView {

    /// 点击回复
    var tapToReply: (()->())?
    
    fileprivate var portraitView: UIImageView?
    fileprivate var createdAtLable: UILabel?
    fileprivate var nameLabel: UILabel?
    fileprivate var commentLabel: UILabel?
    
    /// 展示评论数据的视图
    ///
    /// - Parameter commentModel: 评论模型
    /// - Returns: MainReplyView
    class func setCommentView(commentModel: CommentModel)->MainReplyView{
        
        let commentView: MainReplyView = MainReplyView()
        
        //头像
        commentView.portraitView = UIImageView()
        commentView.portraitView?.contentMode = UIViewContentMode.scaleAspectFill
        commentView.addSubview(commentView.portraitView!)
        commentView.portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(commentView).offset(10/baseHeight)
            make.left.equalTo(commentView).offset(10/baseWidth)
            make.size.equalTo(CGSize(width: 37, height: 37))
        })
        commentView.portraitView?.layer.cornerRadius = 18.5//添加圆角
        commentView.portraitView?.layer.masksToBounds = true
        
        //评论时间的label
        commentView.createdAtLable = UILabel()
        commentView.createdAtLable?.textAlignment = NSTextAlignment.right
        commentView.createdAtLable?.textColor = UIColor(hexString: "ADADAD")
        if #available(iOS 8.2, *) {
            commentView.createdAtLable?.font = UIFont.systemFont(ofSize: 11, weight: UIFontWeightSemibold)
        } else {
            commentView.createdAtLable?.font = UIFont.systemFont(ofSize: 11)
        }
        commentView.addSubview(commentView.createdAtLable!)
        commentView.createdAtLable?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(commentView).offset(12/baseHeight)
            make.right.equalTo(commentView).offset(-7/baseWidth)
            make.height.equalTo(16)
        })
        
        //用户昵称
        commentView.nameLabel = UILabel()
        commentView.nameLabel?.textColor = UIColor(hexString: "545454")
        if #available(iOS 8.2, *) {
            commentView.nameLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
        } else {
            commentView.nameLabel?.font = UIFont.systemFont(ofSize: 14)
        }
        commentView.nameLabel?.textAlignment = NSTextAlignment.left
        commentView.addSubview(commentView.nameLabel!)
        commentView.nameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(commentView).offset(10/baseHeight)
            make.left.equalTo(commentView.portraitView!.snp.right).offset(5/baseWidth)
            make.right.equalTo(commentView.createdAtLable!).offset(-5/baseWidth)
            make.height.equalTo(17)
        })
        
        //用户评论
        commentView.commentLabel = UILabel()
        commentView.commentLabel?.textColor = UIColor(hexString: "676767")
        if #available(iOS 8.2, *) {
            commentView.commentLabel?.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightLight)
        } else {
            commentView.commentLabel?.font = UIFont.systemFont(ofSize: 12)
        }
        commentView.commentLabel?.textAlignment = NSTextAlignment.left
        commentView.commentLabel?.numberOfLines = 40
        commentView.addSubview(commentView.commentLabel!)
        commentView.commentLabel?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(commentView.nameLabel!.snp.bottom).offset(7/baseHeight)
            make.leftMargin.equalTo(commentView.nameLabel!)
            make.bottom.equalTo(commentView).offset(-10/baseHeight)
            make.rightMargin.equalTo(commentView.createdAtLable!)
        }
        
        commentView.setDate(commentModel: commentModel)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: commentView, action: #selector(commentView.tapCommentView(tap:)))
        commentView.addGestureRecognizer(tap)
        
        return commentView
    }
    
    //MARK: 设置数据
    fileprivate func setDate(commentModel: CommentModel){
        // 获取评论时间
        if let date: Date = commentModel.createDate{
            createdAtLable?.text = getDateText(date: date)
        }
        
        //获取头像
        if let url: URL = commentModel.userLogo {
            portraitView?.sd_setImage(with: url, placeholderImage: UIImage(named: "defaultLogo"))
        }else{
            portraitView?.image = UIImage(named: "defaultLogo")
        }
        
        //获取昵称
        nameLabel?.text = commentModel.nickName
        
        //获取评论
        let commentStr = commentModel.content
        let attStr = NSMutableAttributedString(string: commentStr)
        
        //如果评论中包含@...字段，则将这段字段加粗
        if commentStr.characters.first == "@" {
            let index = commentStr.range(of: " ")
            let intValue: Int = (commentStr.characters.distance(from: (commentStr.startIndex), to: (index?.lowerBound)!))
            let attDic = [NSFontAttributeName: UIFont.boldSystemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor.black] as [String: Any]
            let range = NSMakeRange(0, intValue)
            attStr.addAttributes(attDic, range: range)
        }
        
        commentLabel?.attributedText = attStr
    }
    
    //MARK: 设置时间
    fileprivate func getDateText(date: Date) ->String{
        
        var timeText: String
        let todayDate: Date = Date()//今天的日期
        let timeInterval: TimeInterval = todayDate.timeIntervalSince(date)//评论的时间间隔
        
        switch timeInterval {
        case 0..<60://一分钟内
            timeText = "\(Int(timeInterval))" + NSLocalizedString("秒", comment: "")
        case 60..<3600://一小时之内
            timeText = "\(Int(timeInterval/60))" + NSLocalizedString("分钟", comment: "")
        case 3600..<86400://二十四小时之内
            timeText = "\(Int(timeInterval/3600))" + NSLocalizedString("小时", comment: "")
        case 86400..<604800://七天之内
            timeText = "\(Int(timeInterval/86400))" + NSLocalizedString("天", comment: "")
        default://超过七天
            let timeFomatter: DateFormatter = DateFormatter()
            timeFomatter.timeStyle = DateFormatter.Style.none
            timeFomatter.dateStyle = DateFormatter.Style.medium
            timeText = timeFomatter.string(from: date)
        }
        return timeText
    }
    
    //MARK: 点击回复
    func tapCommentView(tap: UITapGestureRecognizer){
        tapToReply?()
    }
}
