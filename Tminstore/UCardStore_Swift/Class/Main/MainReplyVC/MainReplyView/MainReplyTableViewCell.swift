//
//  MainReplyTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/19.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MainReplyTableViewCell: UITableViewCell {

    var reply: ReplyModel?{
        didSet{
            if reply != nil{
                setData()
            }
        }
    }
    
    /// 展示回复内容的灰色视图
    fileprivate var grayContainerView: UIView?
    /// 回复人的头像
    fileprivate var userPortraitView: UIImageView?
    /// 昵称Label
    fileprivate var nickNameLabel: UILabel?
    /// 回复时间
    fileprivate var replyDatelabel: UILabel?
    /// 回复的内容
    fileprivate var replyContentLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        grayContainerView = UIView()
        grayContainerView?.backgroundColor = UIColor(hexString: "#F8F8F8")
        contentView.addSubview(grayContainerView!)
        grayContainerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.bottom.equalTo(contentView)
            make.left.equalTo(contentView).offset(10/baseWidth)
            make.right.equalTo(contentView).offset(-10/baseWidth)
        })
        
        userPortraitView = UIImageView()
        userPortraitView?.contentMode = UIViewContentMode.scaleAspectFill
        userPortraitView?.layer.cornerRadius = 26/baseWidth/2
        userPortraitView?.layer.masksToBounds = true
        grayContainerView?.addSubview(userPortraitView!)
        userPortraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayContainerView!).offset(4/baseHeight)
            make.left.equalTo(grayContainerView!).offset(6/baseWidth)
            make.size.equalTo(CGSize(width: 26/baseWidth, height: 26/baseWidth))
        })
        
        replyDatelabel = UILabel()
        replyDatelabel?.textColor = UIColor(hexString: "ADADAD")
        replyDatelabel?.font = UIFont.systemFont(ofSize: 8)
        replyDatelabel?.numberOfLines = 1
        replyDatelabel?.textAlignment = NSTextAlignment.right
        grayContainerView?.addSubview(replyDatelabel!)
        replyDatelabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(userPortraitView!).offset(3/baseHeight)
            make.right.equalTo(grayContainerView!).offset(-10/baseWidth)
        })
        
        nickNameLabel = UILabel()
        nickNameLabel?.textColor = UIColor(hexString: "5AB8CD")
        nickNameLabel?.font = UIFont.systemFont(ofSize: 12)
        nickNameLabel?.numberOfLines = 1
        grayContainerView?.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(userPortraitView!).offset(2/baseWidth)
            make.left.equalTo(userPortraitView!.snp.right).offset(8/baseWidth)
            make.right.equalTo(replyDatelabel!.snp.left).offset(-10/baseWidth)
        })
        
        replyContentLabel = UILabel()
        replyContentLabel?.numberOfLines = 2
        replyContentLabel?.font = UIFont.systemFont(ofSize: 10)
        grayContainerView?.addSubview(replyContentLabel!)
        replyContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(4/baseHeight)
            make.leftMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
            make.bottom.equalTo(grayContainerView!).offset(-14/baseHeight)
        })
    }
    
    //MARK: 设置数据源
    fileprivate func setData(){
        if let url: URL = reply?.userLogoURL{
            userPortraitView?.sd_setImage(with: url, placeholderImage: UIImage(named: "defaultLogo"))
        }
        if let date: Date = reply?.createDate{
            replyDatelabel?.text = getDateText(date: date)
        }
        nickNameLabel?.text = reply?.replyName
        replyContentLabel?.attributedText = getReplyConent(reply: reply!)
    }
    
    //MARK: 设置时间
    fileprivate func getDateText(date: Date) ->String{
        
        var timeText: String
        let todayDate: Date = Date()//今天的日期
        var timeInterval: TimeInterval = todayDate.timeIntervalSince(date)//评论的时间间隔
        if timeInterval < 0 {
            timeInterval = 0
        }
        switch timeInterval {
        case 0..<60://一分钟内
            timeText = "\(Int(timeInterval))" + NSLocalizedString("秒", comment: "")
        case 60..<3600://一小时之内
            timeText = "\(Int(timeInterval/60))" + NSLocalizedString("分钟", comment: "")
        case 3600..<86400://二十四小时之内
            timeText = "\(Int(timeInterval/3600))" + NSLocalizedString("小时", comment: "")
        case 86400..<604800://七天之内
            timeText = "\(Int(timeInterval/86400))" + NSLocalizedString("天", comment: "")
        default://超过七天
            let timeFomatter: DateFormatter = DateFormatter()
            timeFomatter.timeStyle = DateFormatter.Style.none
            timeFomatter.dateStyle = DateFormatter.Style.medium
            timeText = timeFomatter.string(from: date)
        }
        return timeText
    }
    
    //MARK: 设置回复数据
    fileprivate func getReplyConent(reply: ReplyModel)->NSMutableAttributedString{
        
        var str: NSMutableAttributedString//评论内容
        var replyString: String//评论开头
        if languageCode() == "CN"{
            replyString = "回复"
        }else{
            replyString = "Reply"
        }
        str = NSMutableAttributedString(string: replyString + "@" + reply.toReplyName + reply.content)
        
        let replyName: NSAttributedString = NSAttributedString(string: reply.toReplyName)
        let replyContent: NSAttributedString = NSAttributedString(string: reply.content)
        let range1: NSRange = NSRange(location: 0, length: replyString.characters.count)// 回复
        let range2: NSRange = NSRange(location: replyString.characters.count, length: replyName.length + 1)// 回复某人的名字
        let range3: NSRange = NSRange(location: replyString.characters.count + 1 + replyName.length, length: replyContent.length)//评论详细内容
        
        str.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 10), NSForegroundColorAttributeName: UIColor(hexString: "808080")] as [String: Any], range: range1)
        str.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 10), NSForegroundColorAttributeName: UIColor(hexString: "6AC4D7")] as [String: Any], range: range2)
        str.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 10), NSForegroundColorAttributeName: UIColor(hexString: "808080")] as [String: Any], range: range3)
        
        return str
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userPortraitView?.image = nil
        nickNameLabel?.text     = nil
        replyDatelabel?.text    = nil
        replyContentLabel?.text = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
