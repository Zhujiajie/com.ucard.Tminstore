//
//  MainReplyViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/19.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MainReplyViewModel: BaseViewModel {

    fileprivate let dataModel: MainDetialDataModel = MainDetialDataModel()
    
    /// 卡片的ID
    var commentId: String = ""
    
    /// 评论模型
    var commentModel: CommentModel?
    
    /// 评论数组
    var replyArray: [ReplyModel] = [ReplyModel]()
    
    /// 要获取第几页评论
    var pageIndex: Int = 1
    
    /// 成功获取到评论列表
    var getReplySuccess: (()->())?
    
    /// 没有更多数据了
    var noMoreData: (()->())?
    
    /// 得到了更多的数据
    var getMoreData: ((_ indexPathArry: [IndexPath])->())?
    
    /// 网络错误
    var networkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 暂时不请求数据
    fileprivate var shouldStopRequestData: Bool = false
    
    
    //MARK: 获取评论列表
    /**
     获取评论列表
     */
    func getReplyList() {
        
        if shouldStopRequestData == true{ return }
        shouldStopRequestData = true
        
        if pageIndex == 1{
            replyArray.removeAll()
            showSVProgress(title: nil)
        }
        
        dataModel.getReplyList(pageIndex: pageIndex, andCommentId: commentId, successClosure: { [weak self] (result: [String: Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                
                let data: [String: Any] = result["data"] as! [String: Any]
                let replyDics: [[String: Any]] = data["replyInfoList"] as! [[String: Any]]
                let commentDic: [String: Any] = data["comment"] as! [String: Any]
                self?.commentModel = CommentModel.initFromDic(dic: commentDic)
                
                if replyDics.isEmpty == true{//没有数据了
                    self?.noMoreData?()
                }else{
                    
                    if self?.pageIndex == 1 {
                        for dic: [String: Any] in replyDics{
                            let reply: ReplyModel = ReplyModel.initFromDic(dic: dic)
                            self?.replyArray.append(reply)
                        }
                        self?.getReplySuccess?()
                        
                    }else{//获得了更多的数据
                        
                        var newReplyArray: [ReplyModel] = [ReplyModel]()
                        
                        for dic: [String: Any] in replyDics{
                            let reply: ReplyModel = ReplyModel.initFromDic(dic: dic)
                            newReplyArray.append(reply)
                        }
                        
                        let currentRow: Int = (self?.replyArray.count)!
                        let newCommentCount: Int = newReplyArray.count
                        
                        var indexPathArray: [IndexPath] = [IndexPath]()
                        for i: Int in 0..<newCommentCount{
                            let newIndexPath: IndexPath = IndexPath(row: currentRow + i, section: 0)
                            indexPathArray.append(newIndexPath)
                        }
                        self?.replyArray.append(contentsOf: newReplyArray)
                        self?.getMoreData?(indexPathArray)
                    }
                    
                    self?.pageIndex += 1
                }
            }else{
                self?.networkError?(nil, result)
            }
            
            }, failureClosure: { [weak self](error: Error?) in
                self?.networkError?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
            self?.shouldStopRequestData = false
        }
    }
}
