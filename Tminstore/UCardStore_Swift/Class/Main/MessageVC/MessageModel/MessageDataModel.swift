//
//  MessageDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MessageDataModel: BaseDataModel {
    
    //MARK: 获取消息列表
    /**
     获取消息列表
     
     - parameter pageNum:    要获取第几页数据
     - parameter success:    成功的闭包
     - parameter failure:    失败的闭包
     - parameter completion: 请求完成的闭包
     */
    func getMessageList(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?){
        
        let urlString: String = appAPIHelp.getMessageListAPI + "\(pageIndex)"
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: {(error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 将某条消息状态改成已读
    /// 将某条消息状态改成已读
    ///
    /// - Parameter noticeId: 消息的ID
    func readMessage(noticeId: String){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "noticeId": noticeId
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.readMessageAPI, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil)
    }
    
    //MARK: 删除某条消息
    /// 删除某条消息
    ///
    /// - Parameter noticeId: 消息的ID
    func deleteMessage(noticeId: String){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "noticeId": noticeId
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.deleteMessageAPI, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil)
    }
    
    //MARK: 将所有推送消息标为已读
    /// 将所有推送消息标为已读
    ///
    /// - Parameter completion: 完成的闭包
    func readAllMessage(completion: (()->())?){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.readAllMessageAPI, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 将所有系统消息标为已读
    /// 将所有系统消息标为已读
    func readAllSystemMessage(){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.readAllSystemMessageAPI, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil, completionClosure: nil)
    }
    
    //MARK: 删除所有推送消息
    /// 删除所有推送消息
    ///
    /// - Parameter completion: 完成的闭包
    func deleteAllMessage(completion: (()->())?){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.deleteAllMessageAPI, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 获取系统消息列表
    /// 获取系统消息列表
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - completion: 完成的闭包
    func getSystemMessage(pageNum: Int, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?){
        
        let urlString: String = appAPIHelp.getSystemMessageListAPI + "\(pageNum)"
        
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken()
        ]
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: {(error: Error?) in
            failure?(error)
        }, completionClosure: {
            completion?()
        })
    }
    
    //MARK: 读取系统消息
    /// 读取系统消息
    ///
    /// - Parameter messageId: 消息的ID
    func readSystemMessage(messageId: String){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "messageId": messageId
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.readSystemMessageAPI, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil)
    }
    
    //MARK: 删除系统消息
    /// 读取系统消息
    ///
    /// - Parameter messageId: 消息的ID
    func deleteSystemMessage(messageId: String){
        let parameters: [String: Any] = [
            "tminstoreToken": getUserToken(),
            "messageId": messageId
        ]
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.deleteSystemMessageAPI, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil)
    }
}
