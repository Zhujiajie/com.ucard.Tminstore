//
//  MessageViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/8.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MessageViewModel: BaseViewModel {

    fileprivate let dataModel: MessageDataModel = MessageDataModel()
    
    /// footer需要重置
    var footerShouldReset: (()->())?
    
    /// header停止刷新动画
    var headerStopRefresh: (()->())?
    
    /// footer停止刷新
    var footerStopRefresh: (()->())?
    
    /// 没有更多数据了
    var stopRefreshWithNoMoreData: (()->())?
    
    /// tableView需要重新加载数据
    var tableShouldReloadData: (()->())?
    
    /// 网络请求错误
    var netWorkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 向服务器端请求第几页数据
    var pageNum: Int = 1
    
    /// 存放系统消息的数组
    var systemMessageArray: [SystemMessageModel] = [SystemMessageModel]()
    
    /// 存放消息的数组
    var messageArray: [MessageModel] = [MessageModel]()
    
    /// 是否需要停止上拉刷新
    fileprivate var stopGetMoreData = false
    
    /// 正在加载
    fileprivate var isRequestingData = false
    
    //MARK: 上拉刷新
    /// 上拉刷新
    func pullToRefresh(){
        //防止重复获取数据
        if isRequestingData == true{ return }
        isRequestingData = true
    }
    
    //MARK: 下拉刷新
    /// 下拉刷新
    func pushToRefresh() {
        pageNum = 1
        stopGetMoreData = false
        getSystemMessage()//先获取系统消息
        footerShouldReset?()//重置footer
    }
    
    //MARK: 获取系统消息，
    /// 获取系统消息
    func getSystemMessage(){

        dataModel.getSystemMessage(pageNum: pageNum, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if list.isEmpty {
                            self?.stopRefreshWithNoMoreData?()
                        }else{
                            if self?.pageNum == 1{
                                self?.systemMessageArray.removeAll()
                            }
                            for dic: [String: Any] in list{
                                let systemMessage: SystemMessageModel = SystemMessageModel.initFromDic(dic: dic)
                                self?.systemMessageArray.append(systemMessage)
                            }
                            self?.tableShouldReloadData?()
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.netWorkError?(nil, result)
            }, failureClosure: { [weak self] (error: Error?) in
            self?.netWorkError?(error, nil)
        }, completionClosure: nil)
    }

    //MARK: 获取消息列表数据
    func getUserDate(){
        
        dataModel.getMessageList(pageIndex: pageNum, successClosure: { [weak self](result: [String : Any]) in
            
//            print(result)
            
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let messageList: [[String: Any]] = data["list"] as? [[String: Any]]{
                        // 没有更多数据了
                        if messageList.isEmpty == true{
                            self?.stopRefreshWithNoMoreData?()
                        }else{
                            if self?.pageNum == 1{
                                self?.messageArray.removeAll()
                            }
                            // 获得新数据
                            var newMessageArray: [MessageModel] = [MessageModel]()
                            for dic: [String: Any] in messageList{
                                let message: MessageModel = MessageModel.initFromDic(dic: dic)
                                newMessageArray.append(message)
                            }
                            //传递新数据，并存储新数据
                            self?.messageArray.append(contentsOf: newMessageArray)
                            self?.tableShouldReloadData?()
                        }
                        self?.pageNum += 1
                        return
                    }
                }
            }
            self?.netWorkError?(nil, result)
            }, failureClosure: { [weak self] (error: Error?) in
                self?.netWorkError?(error, nil)
            }, completionClosure: nil)
    }
    
    //MARK: 将消息的状态改成已读
    /// 将消息的状态改成已读
    ///
    /// - Parameter noticeId: 消息的ID
    func readMessage(noticeId: String){
        dataModel.readMessage(noticeId: noticeId)
    }
    
    //MARK: 删除消息
    /// 删除消息
    ///
    /// - Parameter noticeId: 消息的ID
    func deleteMessage(noticeId: String){
        dataModel.deleteMessage(noticeId: noticeId)
    }
    
    //MARK: 读取系统消息
    /// 读取系统消息
    ///
    /// - Parameter messageId: 消息的ID
    func readSystemMessage(messageId: String){
        dataModel.readSystemMessage(messageId: messageId)
    }
    
    //MARK: 删除系统消息
    /// 读取系统消息
    ///
    /// - Parameter messageId: 消息的ID
    func deleteSystemMessage(messageId: String){
        dataModel.deleteSystemMessage(messageId: messageId)
    }
    
    //MARK: 将所有推送消息标为已读
    /// 将所有推送消息标为已读
    func readAllMessage(){
        dataModel.readAllMessage(completion: {[weak self] _ in
            self?.pushToRefresh()//请求完成后，刷新界面
        })
    }
    
    //MARK: 将所有系统消息标为已读
    /// 将所有系统消息标为已读
    func readAllSystemMessage(){
        dataModel.readAllSystemMessage()
    }
    
    //MARK: 删除所有推送消息
    /// 删除所有推送消息
    func deleteAllMessage(){
        messageArray.removeAll()
        dataModel.deleteAllMessage(completion: {[weak self] _ in
            self?.pushToRefresh()//请求完成后，刷新界面
        })
    }
}
