//
//  MessageController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/7.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: ------------------展示消息列表的Controller--------------

import UIKit
import SnapKit

class MessageController: BaseViewController {
    
    fileprivate let mapViewModel: PMapViewModel = PMapViewModel()
    fileprivate let viewModel: MessageViewModel = MessageViewModel()
    
    /// 上部分切换消息类型的视图
    fileprivate var messageHeaderView: MessageHeaderView?
    /// 承载两个tableView的collectionView
    fileprivate var collectionViewController: MessageCollectionViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexString: "#F5F5F5")
        setNavigationBar()
        setUI()
        closures()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        interactive.pan?.isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        interactive.pan?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D")]
        setNavigationAttributeTitle(NSLocalizedString("通知中心", comment: ""), attributeDic: attDic)
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        
//        setNavigationBarRightButtonWithImageName("dot")
//        navigationItem.rightBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
    }
    
    //MARK: 设置tableView
    fileprivate func setUI(){
        
        messageHeaderView = MessageHeaderView()
        view.addSubview(messageHeaderView!)
        messageHeaderView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(5)
            make.left.equalTo(view).offset(7/baseWidth)
            make.right.equalTo(view).offset(-7/baseWidth)
            make.height.equalTo(45/baseWidth)
        })
        
        collectionViewController = MessageCollectionViewController()
        addChildViewController(collectionViewController!)
        view.addSubview(collectionViewController!.view)
        collectionViewController?.didMove(toParentViewController: self)
        
        collectionViewController?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(messageHeaderView!.snp.bottom)
            make.left.bottom.right.equalTo(view)
        })
        
        collectionViewController?.netWorkError = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 点击导航栏左按钮，退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
    }
    
//    //MARK: 点击导航栏右按钮，清空消息，或者将所有消息删除
//    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
//        let sheet: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
//        // 将所有消息标为已读
//        let readAllAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("全部标为已读", comment: ""), style: UIAlertActionStyle.default) { [weak self](_) in
//            self?.viewModel.readAllMessage()
//            self?.viewModel.readAllSystemMessage()
//        }
//        sheet.addAction(readAllAction)
//        //删除所有消息
//        let deleteAllAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("删除所有消息", comment: ""), style: UIAlertActionStyle.destructive) { [weak self] (_) in
//            self?.viewModel.deleteAllMessage()
//        }
//        sheet.addAction(deleteAllAction)
//        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("取消", comment: ""), style: UIAlertActionStyle.cancel, handler: nil)
//        sheet.addAction(cancelAction)
//        present(sheet, animated: true, completion: nil)
//    }
    
    //MARK: 各种闭包回调
    fileprivate func closures(){
        
        //MARK: collectionView滚动了
        collectionViewController?.collectionViewScrolled = {[weak self] (indexPath: IndexPath) in
            self?.messageHeaderView?.isUserMessageButtonSelected = indexPath.row == 0
        }
        
        //MARK: 成功获得图片数据
        collectionViewController?.getPictureInfoSuccess = {[weak self] (mapData: SpaceTimeDataModel) in
            let vc: PNewMapDetailController = PNewMapDetailController()
            vc.mapData = mapData
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 出错的闭包
        collectionViewController?.netWorkError = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 切换了消息类型
        messageHeaderView?.messageButtonChanged = {[weak self] (isUserMessageButtonSelected: Bool) in
            var indexPath: IndexPath
            if isUserMessageButtonSelected == true{
                indexPath = IndexPath(item: 0, section: 0)
            }else{
                indexPath = IndexPath(item: 1, section: 0)
            }
            self?.collectionViewController?.collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
            delay(seconds: 0.5, completion: {
                self?.collectionViewController?.checkSystemMessage()
            }, completionBackground: nil)
        }
    }
}
