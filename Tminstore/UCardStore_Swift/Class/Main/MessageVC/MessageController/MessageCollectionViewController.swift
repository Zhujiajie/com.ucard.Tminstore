//
//  MessageCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/13.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import MJRefresh

class MessageCollectionViewController: UICollectionViewController {
    
    /// 获取某一张图片所有信息成功
    var getPictureInfoSuccess: ((_ mapData: SpaceTimeDataModel)->())?
    
    /// 网络请求错误
    var netWorkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 监测collectionView的滚动
    var collectionViewScrolled: ((_ indexPath: IndexPath)->())?
    
    /// 展示用户消息的tableView
    fileprivate var userMessageTableViewController: MessageTableViewController?
    /// 展示系统消息的tableView
    fileprivate var systemMessageTableViewController: MessageTableViewController?
    
    init() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.itemSize = CGSize(width: screenWidth, height: 550/baseWidth)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.clear
        self.collectionView?.backgroundColor = UIColor.clear
        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "0")
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "1")
        self.collectionView?.isScrollEnabled = false
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        userMessageTableViewController?.tableView.mj_header.beginRefreshing()//获取用户消息数据
    }
    
    //MARK: 检查是否需要获取系统消息
    /// 检查是否需要获取系统消息
    func checkSystemMessage(){
        if systemMessageTableViewController != nil && systemMessageTableViewController?.systemMessageModelArray.isEmpty == true{
            systemMessageTableViewController?.tableView.mj_header.beginRefreshing()
        }
    }
    
    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(indexPath.row)", for: indexPath)
    
        if indexPath.row == 0{
            userMessageTableViewController = MessageTableViewController()
            cell.contentView.addSubview(userMessageTableViewController!.view)
            userMessageTableViewController!.view.snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(cell.contentView).offset(8)
                make.left.equalTo(cell.contentView).offset(7/baseWidth)
                make.right.equalTo(cell.contentView).offset(-7/baseWidth)
                make.bottom.equalTo(cell.contentView)
            })
            
            //MARK: 出错的闭包
            userMessageTableViewController?.netWorkError = {[weak self] (error: Error?, result: [String: Any]?) in
                self?.netWorkError?(error, result)
            }
            
            //MARK: 成功获得图片数据
            userMessageTableViewController?.getPictureInfoSuccess = {[weak self] (mapData: SpaceTimeDataModel) in
                self?.getPictureInfoSuccess?(mapData)
            }
            
        }else {
            systemMessageTableViewController = MessageTableViewController()
            systemMessageTableViewController?.isShowingUserMessage = false
            cell.contentView.addSubview(systemMessageTableViewController!.view)
            systemMessageTableViewController!.view.snp.makeConstraints({ (make: ConstraintMaker) in
                make.top.equalTo(cell.contentView).offset(8)
                make.left.equalTo(cell.contentView).offset(7/baseWidth)
                make.right.equalTo(cell.contentView).offset(-7/baseWidth)
                make.bottom.equalTo(cell.contentView)
            })
            
            //MARK: 出错的闭包
            systemMessageTableViewController?.netWorkError = {[weak self] (error: Error?, result: [String: Any]?) in
                self?.netWorkError?(error, result)
            }
        }
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    //MARK: 滚动cell
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexPath: IndexPath = IndexPath(item: Int(scrollView.contentOffset.x / screenWidth), section: 0)
        collectionViewScrolled?(indexPath)
        //若数据为空，则刷新
        if indexPath.row == 0 && userMessageTableViewController?.messageModelArray.isEmpty == true{
            userMessageTableViewController?.tableView.mj_header.beginRefreshing()
        }else if indexPath.row == 1 && systemMessageTableViewController?.systemMessageModelArray.isEmpty == true {
            systemMessageTableViewController?.tableView.mj_header.beginRefreshing()
        }
    }
}

//MARK: 展示消息详情的cell
class MessageTableViewController: UITableViewController {
    
    /// 网络请求错误
    var netWorkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 获取某一张图片所有信息成功
    var getPictureInfoSuccess: ((_ mapData: SpaceTimeDataModel)->())?
    
    fileprivate let messageViewModel: MessageViewModel = MessageViewModel()
    fileprivate let mapViewModel: PMapViewModel = PMapViewModel()
    
    /// 数据源
    var messageModelArray: [MessageModel] = [MessageModel]()
    var systemMessageModelArray: [SystemMessageModel] = [SystemMessageModel]()
    
    /// 是否是展示用户消息的tableView
    var isShowingUserMessage: Bool = true
    
    override func viewDidLoad() {
        
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.register(MessageTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MessageTableViewCellReuseIdentifier)
        
        tableView.mj_header = BaseAnimationHeader()
        tableView.mj_footer = BaseRefreshFooter()
        
        closures()
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 下拉刷新
        tableView.mj_header.refreshingBlock = {[weak self] _ in
            self?.messageViewModel.pageNum = 1
            if self?.isShowingUserMessage == true{
                self?.messageViewModel.getUserDate()
            }else{
                self?.messageViewModel.getSystemMessage()
            }
        }
        
        //MARK: 上拉刷新
        tableView.mj_footer.refreshingBlock = {[weak self] _ in
            if self?.isShowingUserMessage == true{
                self?.messageViewModel.getUserDate()
            }else{
                self?.messageViewModel.getSystemMessage()
            }
        }
        
        //MARK: 成功获得数据
        messageViewModel.tableShouldReloadData = {[weak self] _ in
            self?.tableView.mj_header.endRefreshing()
            self?.tableView.mj_footer.endRefreshing()
            if self?.isShowingUserMessage == true {
                self?.messageModelArray = (self?.messageViewModel.messageArray)!
            }else{
                self?.systemMessageModelArray = (self?.messageViewModel.systemMessageArray)!
            }
            self?.tableView.reloadData()
        }
        
        //MARK: 没有更多数据了
        messageViewModel.stopRefreshWithNoMoreData = {[weak self] _ in
            self?.tableView.mj_header.endRefreshing()
            self?.tableView.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 出错的闭包
        messageViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.netWorkError?(error, result)
        }
        
        //MARK: 获得图片信息成功
        mapViewModel.getPictureInfoSuccess = {[weak self] (mapData: SpaceTimeDataModel) in
            self?.getPictureInfoSuccess?(mapData)
        }
        
        //MARK: 出错
        mapViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.netWorkError?(error, result)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isShowingUserMessage {
            return messageModelArray.count
        }else{
            return systemMessageModelArray.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MessageTableViewCellReuseIdentifier, for: indexPath) as! MessageTableViewCell
        if isShowingUserMessage == true{
            cell.messageModel = messageModelArray[indexPath.row]
        }else{
            cell.systemMessageModel = systemMessageModelArray[indexPath.row]
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84/baseWidth
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell: MessageTableViewCell = tableView.cellForRow(at: indexPath) as? MessageTableViewCell{
            cell.isRead = true
            if isShowingUserMessage {
                messageModelArray[indexPath.row].status = true
                messageViewModel.readMessage(noticeId: messageModelArray[indexPath.row].noticeId)
                //获取videoID
                mapViewModel.getPictureInfo(videoId: cell.messageModel!.videoId)
            }else{
                systemMessageModelArray[indexPath.row].readStatus = true
                messageViewModel.readSystemMessage(messageId: systemMessageModelArray[indexPath.row].messageId)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //MARK: 删除cell
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete{
            if isShowingUserMessage {
                messageViewModel.deleteMessage(noticeId: messageModelArray[indexPath.row].noticeId)
                messageModelArray.remove(at: indexPath.row)
                messageViewModel.messageArray.remove(at: indexPath.row)
            }else{
                messageViewModel.deleteSystemMessage(messageId: systemMessageModelArray[indexPath.row].messageId)
                systemMessageModelArray.remove(at: indexPath.row)
                messageViewModel.systemMessageArray.remove(at: indexPath.row)
            }
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
}

class MessageTableViewCell: UITableViewCell{
    
    var messageModel: MessageModel?{
        didSet{
            setData()
        }
    }
    
    var systemMessageModel: SystemMessageModel?{
        didSet{
            setData()
        }
    }
    
    /// 是否已读，更改Label的alpha值
    var isRead: Bool = false{
        didSet{
            if isRead == true{
                nickNameLabel?.alpha = 0.4
                contentLabel?.alpha = 0.4
            }else{
                nickNameLabel?.alpha = 1
                contentLabel?.alpha = 1
            }
        }
    }
    
    /// 用户头像
    fileprivate var portraitView: BaseImageView?
    /// 昵称
    fileprivate var nickNameLabel: BaseLabel?
    /// 时间Label
    fileprivate var createTimeLabel: BaseLabel?
    /// 消息时间
    fileprivate var contentLabel: BaseLabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        portraitView = BaseImageView.userPortraitView(cornerRadius: 20/baseWidth)
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(11/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        }
        
        createTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "B6B6B6", andTextAlignment: NSTextAlignment.right)
        contentView.addSubview(createTimeLabel!)
        createTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(27/baseWidth)
            make.right.equalTo(contentView).offset(-17/baseWidth)
        })
        
        nickNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "666666", andNumberOfLines: 1)
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(portraitView!)
            make.left.equalTo(portraitView!.snp.right).offset(10/baseWidth)
            make.right.equalTo(createTimeLabel!.snp.left).offset(-10/baseWidth)
        })
        
        contentLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "919191", andNumberOfLines: 2)
        contentView.addSubview(contentLabel!)
        contentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(8/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
            make.bottom.equalTo(contentView).offset(-5/baseWidth)
        })
        
        let grayLine: BaseView = BaseView.colorLine(colorString: "#F5F5F5")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(2)
        }
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        if messageModel != nil {
            portraitView?.sd_setImage(with: messageModel?.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
            if let date: Date = messageModel?.createDate{
                createTimeLabel?.text = getDateText(date: date, andTimeStyle: DateFormatter.Style.none, andDateStyle: DateFormatter.Style.short)
            }
            nickNameLabel?.text = messageModel?.noticeTitle
            contentLabel?.text = messageModel?.noticeContent
            isRead = messageModel!.status
        }else if systemMessageModel != nil{
            portraitView?.image = UIImage(named: "systemMessageLogo")
            if let date: Date = systemMessageModel?.createDate{
                createTimeLabel?.text = getDateText(date: date, andTimeStyle: DateFormatter.Style.none, andDateStyle: DateFormatter.Style.short)
            }
            nickNameLabel?.text = systemMessageModel?.subject
            contentLabel?.text = systemMessageModel?.content
            isRead = systemMessageModel!.readStatus
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

