//
//  SystemMessageController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/9.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class SystemMessageController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    var systemMessageArray: [SystemMessageModel] = [SystemMessageModel]()
    
    fileprivate let viewModel: MessageViewModel = MessageViewModel()
    
    fileprivate var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        systemMessageArray = systemMessageArray.reversed()
        
        setNavigationBar()
        
        tableView = UITableView()
        tableView?.backgroundColor = UIColor(hexString: "#FBFBFB")
        tableView?.register(SystemMessageTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.SystemMessageTableViewCellReuseIdentifier)
        tableView?.separatorStyle = UITableViewCellSeparatorStyle.none//除去分割线
        tableView?.estimatedRowHeight = 30/baseWidth
        tableView?.delegate = self
        tableView?.dataSource = self
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView?.scrollToRow(at: IndexPath(row: systemMessageArray.count - 1, section: 0), at: UITableViewScrollPosition.bottom, animated: true)
        viewModel.readAllSystemMessage()//将所有系统消息设置为已读
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D")]
        setNavigationAttributeTitle(NSLocalizedString("系统消息", comment: ""), attributeDic: attDic)
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
    }

    //MARK: 点击导航栏左按钮，退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return systemMessageArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SystemMessageTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.SystemMessageTableViewCellReuseIdentifier, for: indexPath) as! SystemMessageTableViewCell
        
        cell.systemMessageModel = systemMessageArray[indexPath.row]
        
        return cell
    }
}
