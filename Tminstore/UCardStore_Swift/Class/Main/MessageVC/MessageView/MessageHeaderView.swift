//
//  MessageHeaderView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/13.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MessageHeaderView: BaseView {

    /// 切换了消息类型
    var messageButtonChanged: ((_ isUserMessageButtonSelected: Bool)->())?
    
    /// 当前选中的消息类型
    var isUserMessageButtonSelected: Bool = true{
        didSet{
            if isUserMessageButtonSelected == oldValue{return}
            touchMessageButton()
        }
    }
    
    /// 用户消息按钮
    fileprivate var userMessageButton: BaseButton?
    /// 系统消息按钮
    fileprivate var systemMessageButton: BaseButton?
    /// 底部绿色线条
    fileprivate var greenLine: BaseView?
    /// 选中的标题格式
    fileprivate let selectedAttribute: [String: Any] = [
        NSFontAttributeName: UIFont.systemFont(ofSize: 14),
        NSForegroundColorAttributeName: UIColor(hexString: "81F5FF")
    ]
    /// 未选中的标题格式
    fileprivate let deSelecteAttribute: [String: Any] = [
        NSFontAttributeName: UIFont.systemFont(ofSize: 14),
        NSForegroundColorAttributeName: UIColor(hexString: "B7B7B7")
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        userMessageButton = BaseButton.normalTitleButton(title: NSLocalizedString("用户消息", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "81F5FF", andBackgroundColorHexString: "FFFFFF")
        addSubview(userMessageButton!)
        userMessageButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.bottom.equalTo(self)
            make.width.equalTo(180.5/baseWidth)
        })
        userMessageButton?.touchButtonClosure = {[weak self] _ in
            self?.isUserMessageButtonSelected = true
            self?.messageButtonChanged?(true)
        }
        
        systemMessageButton = BaseButton.normalTitleButton(title: NSLocalizedString("系统通知", comment: ""), andTextFont: UIFont.systemFont(ofSize: 14), andTextColorHexString: "B7B7B7", andBackgroundColorHexString: "FFFFFF")
        addSubview(systemMessageButton!)
        systemMessageButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.right.bottom.equalTo(self)
            make.left.equalTo(userMessageButton!.snp.right)
        })
        systemMessageButton?.touchButtonClosure = {[weak self] _ in
            self?.isUserMessageButtonSelected = false
            self?.messageButtonChanged?(false)
        }
        
        greenLine = BaseView.colorLine(colorString: "81F5FF")
        addSubview(greenLine!)
        greenLine?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(self)
            make.centerX.equalTo(userMessageButton!)
            make.width.equalTo(userMessageButton!)
            make.height.equalTo(2)
        })
    }
    
    //MARK: 点击用户消息按钮的事件
    fileprivate func touchMessageButton(){
        if isUserMessageButtonSelected {
            userMessageButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("用户消息", comment: ""), attributes: selectedAttribute), for: UIControlState.normal)
            systemMessageButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("系统通知", comment: ""), attributes: deSelecteAttribute), for: UIControlState.normal)
            greenLine?.snp.remakeConstraints({ (make: ConstraintMaker) in
                make.bottom.equalTo(self)
                make.centerX.equalTo(userMessageButton!)
                make.width.equalTo(userMessageButton!)
                make.height.equalTo(2)
            })
        }else{
            userMessageButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("用户消息", comment: ""), attributes: deSelecteAttribute), for: UIControlState.normal)
            systemMessageButton?.setAttributedTitle(NSAttributedString(string: NSLocalizedString("系统通知", comment: ""), attributes: selectedAttribute), for: UIControlState.normal)
            greenLine?.snp.remakeConstraints({ (make: ConstraintMaker) in
                make.bottom.equalTo(self)
                make.centerX.equalTo(systemMessageButton!)
                make.width.equalTo(userMessageButton!)
                make.height.equalTo(2)
            })
        }
        autoLayoutAnimation(view: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
