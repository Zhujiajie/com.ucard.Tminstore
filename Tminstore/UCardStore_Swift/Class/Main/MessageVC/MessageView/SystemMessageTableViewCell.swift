//
//  SystemMessageTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/9.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class SystemMessageTableViewCell: UITableViewCell {

    //MARK: 存放系统消息的模型
    var systemMessageModel: SystemMessageModel?{
        didSet{
            setMessageData()
        }
    }
    
    fileprivate var logoImageView: UIImageView?
    
    fileprivate var messageBackgroundImageView: UIImageView?
    
    fileprivate var messageContentLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        selectionStyle = UITableViewCellSelectionStyle.none
        
        let grayView: UIView = UIView()
        grayView.backgroundColor = UIColor.clear
        contentView.addSubview(grayView)
        grayView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(14/baseWidth)
        }
        
        logoImageView = UIImageView(image: UIImage(named: "systemMessageLogo"))
        logoImageView?.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(logoImageView!)
        logoImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(23/baseWidth)
            make.left.equalTo(contentView).offset(16/baseWidth)
            make.size.equalTo(CGSize(width: 40/baseWidth, height: 40/baseWidth))
        })
        
        messageContentLabel = UILabel()
        messageContentLabel?.textColor = UIColor(hexString: "7C7C7C")
        messageContentLabel?.font = UIFont.systemFont(ofSize: 12)
        messageContentLabel?.numberOfLines = 0
        contentView.addSubview(messageContentLabel!)
        messageContentLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(27/baseWidth)
            make.left.equalTo(logoImageView!.snp.right).offset(50/baseWidth)
            make.bottom.equalTo(contentView).offset(-15/baseWidth)
            make.width.lessThanOrEqualTo(200/baseWidth)
        })
        
        let backgroundImage: UIImage = UIImage(named: "contentBackground")!.resizableImage(withCapInsets: UIEdgeInsets(top: 45, left: 35, bottom: 13, right: 13), resizingMode: UIImageResizingMode.stretch)
        messageBackgroundImageView = UIImageView(image: backgroundImage)
        messageBackgroundImageView?.contentMode = UIViewContentMode.scaleToFill
        contentView.insertSubview(messageBackgroundImageView!, belowSubview: messageContentLabel!)
    }
    
    //MARK: 设置系统消息
    fileprivate func setMessageData(){
        
        messageContentLabel?.text = systemMessageModel!.subject + "\n" + systemMessageModel!.content
        
        //计算气泡的大小
        var string: NSString
        if let str: String = messageContentLabel?.text{
            string = str as NSString
        }else{
            string = " "
        }
        //先计算高度
        let size1 = string.boundingRect(with: CGSize(width: 200/baseWidth, height: CGFloat.infinity), options: [NSStringDrawingOptions.truncatesLastVisibleLine, NSStringDrawingOptions.usesFontLeading, NSStringDrawingOptions.usesLineFragmentOrigin], attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 12)], context: nil)
        //在计算高度和宽度
        let size2 = string.boundingRect(with: CGSize(width: 200/baseWidth, height: size1.height), options: [NSStringDrawingOptions.truncatesLastVisibleLine, NSStringDrawingOptions.usesFontLeading, NSStringDrawingOptions.usesLineFragmentOrigin], attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 12)], context: nil)
        //设置气泡的frame
        messageBackgroundImageView?.frame = CGRect(x: 66/baseWidth, y: 14/baseWidth, width: size2.width + 57/baseWidth, height: size2.height + 30/baseWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
