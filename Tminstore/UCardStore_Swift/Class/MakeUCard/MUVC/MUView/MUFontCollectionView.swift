//
//  MUFontCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/27.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUFontCollectionView: UIView {

    /// 用于存储字体的数组
    var fontArray: [Any] = [Any]()
    /// 选择字体的闭包
    var chooseFontClosure: ((_ fontName: String)->())?
    /// 选择字体的collectionView
    fileprivate var fontCollectionView: UICollectionView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        fontArray = [[NSLocalizedString("默认", comment: ""): NSLocalizedString("默认", comment: "")], [LobsterTwo_Bold: LobsterTwo_Bold], ["Arial-BoldItalicMT": "Arial-BoldItalicMT"], ["Baskerville-Bold": "Baskerville-Bold"], ["Baskerville-BoldItalic": "Baskerville-BoldItalic"], ["Baskerville-Italic": "Baskerville-Italic"], ["BodoniSvtyTwoOSITCTT-Book": "BodoniSvtyTwoOSITCTT-Book"], ["Cochin-Italic": "Cochin-Italic"], ["Baskerville-SemiBoldItalic": "Baskerville-SemiBoldItalic"], ["Didot-Italic": "Didot-Italic"], ["Georgia-Italic": "Georgia-Italic"], ["Zapfino": "Zapfino"], ["TimesNewRomanPSMT": "TimesNewRomanPSMT"], ["SnellRoundhand": "SnellRoundhand"], ["SnellRoundhand-Bold": "SnellRoundhand-Bold"], ["SnellRoundhand-Black": "SnellRoundhand-Black"], ["Menlo-Regular": "Menlo-Regular"], ["IowanOldStyle-BoldItalic": "IowanOldStyle-BoldItalic"], ["Futura-CondensedMedium": "Futura-CondensedMedium"]]
        if fontHasLoaded == true{
            fontArray.insert([FZYBXSJW_GB1_0: FZYBXSJW_GB1_0], at: 1)
        }
        
        self.backgroundColor = UIColor(hexString: "6A6C71")
        setCollectionView()
    }
    
    //MARK: 设置collectionView
    fileprivate func setCollectionView(){
        
        //layout
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: screenWidth/2 - 10, height: 33/baseHeight)
        layout.minimumInteritemSpacing = 10/baseWidth
        layout.minimumLineSpacing = 9/baseHeight
        layout.scrollDirection = UICollectionViewScrollDirection.vertical
        
        fontCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        
        fontCollectionView?.register(FontCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.FontCollectionViewCellReuseIdentifier)
        fontCollectionView?.backgroundColor = UIColor.clear
        self.addSubview(fontCollectionView!)
        fontCollectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        
        fontCollectionView?.delegate = self
        fontCollectionView?.dataSource = self
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: ------------
extension MUFontCollectionView: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return fontArray.count
    }
    
    //MARK: 设置cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell: FontCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.FontCollectionViewCellReuseIdentifier, for: indexPath) as! FontCollectionViewCell
        
        let dic: [String: Any]? = fontArray[indexPath.item] as? [String: Any]
//        print(dic)
        cell.cellFontName = dic?.values.first as? String
        
        return cell
    }
    
    //MARK: 点击cell，改变greetingTextView的字体
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell: FontCollectionViewCell = collectionView.cellForItem(at: indexPath) as! FontCollectionViewCell
        
        chooseFontClosure?(cell.cellFontName!)//通过闭包传递字体
    }
}


//MARK:----------fontCollectionViewCell------------
class FontCollectionViewCell: UICollectionViewCell{
    
    var cellFontName: String?{
        didSet{
            cellLabel?.font = UIFont(name: cellFontName!, size: 20)
            if cellFontName == "FZYBXSJW--GB1-0" {
                cellLabel?.text = "方正硬笔行书简体"
            }else{
                cellLabel?.text = cellFontName
            }
        }
    }
    
    /// 字体label
    fileprivate var cellLabel: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setCellLabel()//设置cellLabel
    }
    
    fileprivate func setCellLabel(){
        
        cellLabel = UILabel()
        cellLabel?.backgroundColor = UIColor.clear
        cellLabel?.textColor = UIColor.white
        cellLabel?.text = "Ucardstore"
        cellLabel?.textAlignment = NSTextAlignment.center
        
        contentView.addSubview(cellLabel!)
        cellLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
