//
//  MUVisualEffectView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/9/27.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUVisualEffectView: UIVisualEffectView {
    
    /// 相册tableView
    var albumTableView: MUTableView?
    
    //MARK: 高斯模糊视图
    /// 高斯模糊视图
    ///
    /// - Returns: MUVisualEffectView
    class func albumVisualView()->MUVisualEffectView{
        let blur: UIBlurEffect = UIBlurEffect(style: .extraLight)
        let visualView: MUVisualEffectView = MUVisualEffectView(effect: blur)
        
        visualView.albumTableView = MUTableView.photoTableView()
        visualView.contentView.addSubview(visualView.albumTableView!)
        visualView.albumTableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(visualView.contentView)
        })
        
//        visualView.albumTableView?.contentInset = UIEdgeInsets(top: 70, left: 0, bottom: 10, right: 0)
        
        return visualView
    }
}
