//
//  MUBackEditView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/11.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Alamofire
import SnapKit
import CoreLocation

let greetingTextViewTag: Int = 123
let greetingTextView2Tag: Int = 321
let defualTextSize: Int = 14
let minTextSize: Int = 5

class MUBackEditView: UIView {

    /// 背面模板的index
    var templateIndex: Int = 0{
        didSet{
            swithTamplate()
        }
    }
    
    /// 点击了签名视图的闭包，用于弹出签名编辑界面
    var touchSignatureViewClosure: (()->())?
    /// textView结束编辑的闭包
    var textViewEndEditingClosure: ((_ textView: ZTTextView)->())?
    /// 书写祝福语
    var greetingTextView: ZTTextView?
    
    /// greetingTextView的inputView是否为键盘
    fileprivate var isKeyBoardShow: Bool = true
    /// textView的占位字符串
    var placeHolder: String = NSLocalizedString("写下我的祝福", comment: "")
    /// 用于判断greetingTextView是处于输入文字还是删除文字
    fileprivate var originalTextLength: Int = 0
    
    /// greetingTextView的偏移量
    var greetingTextViewContentOffset: CGPoint = CGPoint.zero{
        didSet{
            greetingTextView?.contentOffset = greetingTextViewContentOffset
        }
    }
    
    /// 是否要显示占位字符
    var shouldPlaceHolderShow: Bool = true{
        didSet{
            if shouldPlaceHolderShow {
                greetingTextView?.contentInset = UIEdgeInsets.zero//取消textView的文字居中
            }else{
                greetingTextView?.textColor = UIColor.black
            }
        }
    }
    
    /// 尤卡成logo的imageView
    fileprivate var ucardLogoImageView: UIImageView?
    /// 邮票图片
    fileprivate var stampImageView: UIImageView?
    
    /// backEditView原始的transform
    fileprivate var originalTransform: CGAffineTransform?
    /// 签名图片
    var signatureImage: UIImage?{
        didSet{
            if signatureImage == nil {
                signaturelabel?.isHidden = false
            }else{
                signaturelabel?.isHidden = true
            }
            signatureView?.image = signatureImage
        }
    }
    /// 从签名转成的图片
    var signatureView: UIImageView?
    /// 显示“签名...”的label
    fileprivate var signaturelabel: UILabel?
    /// 选择字体的视图
    fileprivate var fontCollectionView: MUFontCollectionView?
    
    /// 地理位置icon
    fileprivate var locationImageView: UIImageView?
    /// 地理位置Label
    fileprivate var locationLabel: UILabel?
    
    /// caption
    fileprivate var captionImageView: UIImageView?
    
    /// 当前城市的中文名称
    var cityInCN: String = ""
    var stateInCN: String = ""
    var countryInCn: String = ""
    /// 当前城市的英文名称
    var cityInEn: String = ""
    var stateInEn: String = ""
    var countryInEn: String = ""
    
    /// 地理位置的文字
    var locationText: String = ""{
        didSet{
            locationLabel?.text = locationText
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        self.layer.masksToBounds = true
        setUI()//设置UI
        originalTransform = self.transform
    }
    
    //MARK: 设置UI
    fileprivate func setUI(){
        
        // 展示logo的imageView
        ucardLogoImageView = UIImageView()
        ucardLogoImageView?.sd_setImage(with: appAPIHelp.timeoryLogoURL(), placeholderImage: #imageLiteral(resourceName: "timeoryLogo"))//远程加载标题
        ucardLogoImageView?.contentMode = UIViewContentMode.scaleToFill
        ucardLogoImageView?.clipsToBounds = true
        self.addSubview(ucardLogoImageView!)
        ucardLogoImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(20/baseWidth)
            make.left.equalTo(self).offset(20/baseWidth)
            make.size.equalTo(CGSize(width: 68/baseWidth, height: 11/baseWidth))
        })
        
        //地理位置图片
        locationImageView = UIImageView(image: UIImage(named: "MUlocation"))
        locationImageView?.contentMode = UIViewContentMode.scaleToFill
        locationImageView?.clipsToBounds = true
        self.addSubview(locationImageView!)
        locationImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(self).offset(20/baseWidth)
            make.bottom.equalTo(self).offset(-20/baseWidth)
            make.size.equalTo(CGSize(width: 7/baseWidth, height: 10/baseWidth))
        })
        
        //地理位置标签
        locationLabel           = UILabel()
        locationLabel?.font          = UIFont.systemFont(ofSize: 8)
        locationLabel?.textColor     = UIColor(hexString: "B9BCBB")
        locationLabel?.textAlignment = NSTextAlignment.left
        self.addSubview(locationLabel!)
        locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(locationImageView!.snp.right).offset(5/baseWidth)
            make.centerY.equalTo(locationImageView!)
            make.height.equalTo(13/baseWidth)
        })
        
        //设置邮戳视图
        stampImageView = UIImageView()
        stampImageView?.sd_setImage(with: appAPIHelp.stampURL(), placeholderImage: #imageLiteral(resourceName: "stamp"))
        stampImageView?.clipsToBounds = true
        stampImageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.addSubview(stampImageView!)
        stampImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(20/baseWidth)
            make.right.equalTo(self).offset(-20/baseWidth)
            make.size.equalTo(CGSize(width: 73/baseWidth, height: 46/baseWidth))
        })
        
        //底部caption
        captionImageView = UIImageView()
        captionImageView?.sd_setImage(with: appAPIHelp.captionURL(), placeholderImage: #imageLiteral(resourceName: "timeoryCaption"))
        captionImageView?.clipsToBounds = true
        captionImageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.addSubview(captionImageView!)
        captionImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.bottom.equalTo(self).offset(-9/baseWidth)
            make.size.equalTo(CGSize(width: 164/baseWidth, height: 12/baseWidth))
        })
        
        setGreetingView()//设置填写祝福的视图
        setFontCollectionView()//设置选择字体的视图
        setSignatureView()//设置签名视图
    }
    
    //MARK: 设置填写祝福的视图
    fileprivate func setGreetingView(){
        
        greetingTextView = ZTTextView()
        self.addSubview(greetingTextView!)
        greetingTextView?.delegate = self
        greetingTextView?.tag = greetingTextViewTag
        
        //MARK: 文字超出范围时，退出编辑
        greetingTextView?.addTextLengthDidMaxHandler({ (textView: FSTextView?) in
            textView?.resignFirstResponder()
            textView?.endEditing(true)
        })
        
        greetingTextView?.font = UIFont.systemFont(ofSize: CGFloat(defualTextSize))
        greetingTextView?.textColor = UIColor(hexString: "B9BCBB")
        
        setKeyBoardToolBar()//设置键盘的UIToolBar
    }
    
    //MARK: 设置选择字体的视图
    fileprivate func setFontCollectionView(){
        
        fontCollectionView = MUFontCollectionView()
        fontCollectionView?.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 300/baseHeight)
        
        fontCollectionView?.chooseFontClosure = {[weak self] (fontName: String) in
            
            if self?.greetingTextView?.isFirstResponder == true {
                self?.greetingTextView?.fontName = fontName
                self?.resizeTextView((self?.greetingTextView)!)//重新排版文字
            }
        }//通过闭包，传递字体
    }
    
    //MARK: 设置键盘的UIToolBar
    fileprivate func setKeyBoardToolBar(){
        
        let fontButton: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("字体", comment: ""), style: UIBarButtonItemStyle.plain, target: self, action: #selector(switchFontOrKeyBoard(_:)))
        let fixSpaceButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(textViewEndEditing(_:)))
        
        let toolBar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 40))
        toolBar.items = [fontButton, fixSpaceButton, doneButton]
        toolBar.barStyle = UIBarStyle.default
        toolBar.tintColor = UIColor.black
        
        greetingTextView?.inputAccessoryView  = toolBar
    }
    
    //MARK: 设置签名视图
    fileprivate func setSignatureView(){
        
        signatureView                         = UIImageView()
        signatureView?.contentMode            = UIViewContentMode.scaleAspectFit
        signatureView?.isUserInteractionEnabled = true
        signatureView?.clipsToBounds          = true
        self.addSubview(signatureView!)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(touchSignatureView(_:)))
        tap.numberOfTapsRequired = 1
        signatureView?.addGestureRecognizer(tap)//增加手势，点击这里就进入签名编辑页面
        
        signaturelabel                 = UILabel()
        
        let str: String    = NSLocalizedString("签名", comment: "")
        var font: UIFont?
        if languageCode() == "CN"{
            if fontHasLoaded == true{
                font = UIFont(name: "FZYBXSJW--GB1-0", size: 14)
            }else{
                font = UIFont.systemFont(ofSize: 14)
            }
        }else{
            font = UIFont.systemFont(ofSize: 14)
        }
        let attDic: [String: Any] = [NSFontAttributeName: font!, NSForegroundColorAttributeName: UIColor(hexString: "6E6E6E")] as [String : Any]
        signaturelabel?.attributedText = NSAttributedString(string: str, attributes: attDic)
        
        self.addSubview(signaturelabel!)
        
        //自动布局
        signatureView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(self).offset(-97/baseWidth)
            make.width.equalTo(49/baseWidth)
            make.bottom.equalTo(captionImageView!.snp.top).offset(-6/baseWidth)
            make.height.equalTo(33/baseWidth)
        })
        
        signaturelabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(signatureView!.snp.right)
            make.bottomMargin.equalTo(signatureView!)
        })
    }
    
    //MARK: 将明信片背面保存为图片
    /**
     将明信片背面保存为图片
     
     - returns: UIImage
     */
    func getBackImage()->UIImage{
        
        //绘制之前，隐藏UI
        ucardLogoImageView?.isHidden = true
        signaturelabel?.isHidden     = true
        greetingTextView?.isHidden   = true
        signatureView?.isHidden      = true
        stampImageView?.isHidden     = true
        captionImageView?.isHidden   = true
        
        let scale: CGFloat = CGFloat(1750.0 / 296.0 * 1.0)
        
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, scale)
        
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        let greetingLabel: ZTTLabel = ZTTLabel()
        
        //用户输入了祝福语之后，才绘制greetingTextView
        if greetingTextView?.text != placeHolder {
            var greetingTextViewRect: CGRect = self.convert(greetingTextView!.frame, from: self)
            greetingTextViewRect.origin.y -= greetingTextViewContentOffset.y//获取textView在Y轴的偏移量
            
            //绘制祝福语1
            let fontName: String? = greetingTextView?.fontName
            let fontSize: CGFloat = CGFloat(greetingTextView!.textFontSize)
            let attDic: [String: Any] = [NSFontAttributeName: UIFont(name: fontName!, size: fontSize)!, NSForegroundColorAttributeName: UIColor.black] as [String: Any]
            
            greetingLabel.attributedText = NSAttributedString(string: greetingTextView!.text, attributes: attDic)
            greetingLabel.numberOfLines = (greetingTextView?.textContainer.maximumNumberOfLines)!
            greetingLabel.frame = greetingTextViewRect
            self.addSubview(greetingLabel)
            
            greetingLabel.drawText(in: greetingTextViewRect)
        }
        
        stampImageView?.image?.draw(in: stampImageView!.frame)//邮票
        ucardLogoImageView?.image?.draw(in: ucardLogoImageView!.frame)//logo
        captionImageView?.image?.draw(in: captionImageView!.frame)//caption
        
        //绘制签名
        let signatureViewRect: CGRect = self.convert(signatureView!.frame, from: self)
        
        signatureView?.image?.draw(in: signatureViewRect)
        
        locationImageView?.image?.draw(in: locationImageView!.frame)
        locationLabel?.drawText(in: locationLabel!.frame)
        
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        //绘制结束，取消隐藏
        ucardLogoImageView?.isHidden = false
        greetingTextView?.isHidden   = false
        signatureView?.isHidden      = false
        stampImageView?.isHidden     = false
        captionImageView?.isHidden   = false
        
        if signatureView?.image == nil {
            signaturelabel?.isHidden = false
        }//如果用户未签名，则不隐藏签名label
        
        resizeTextViewAndFont()//重新排版文字
        
        greetingLabel.removeFromSuperview()
        
//        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
        return image!
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: ----------各种模板----------------------
extension MUBackEditView{
    
    //MARK: 切换模板
    /**
     切换模板
     */
    func swithTamplate() {
        
//        var topOffset: CGFloat = 0
        
        switch templateIndex {
        case 0:
            
            template0()
//            topOffset = 8/baseWidth
            
        case 1:
            
            template1()
//            topOffset = 12/baseWidth
            
        default:
            break
        }
        confirmAutoLayoutAnimation()
    }
    
    //MARK: 第一种模板
    fileprivate func template0(){
        
        greetingTextView?.snp.remakeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self.snp.top).offset(38/baseWidth)
            make.left.equalTo(self.snp.left).offset(21/baseWidth)
            make.bottom.equalTo(self.snp.bottom).offset(-47/baseWidth)
            make.right.equalTo(stampImageView!.snp.left)
        })
    }
    
    //MARK: 第二种模板
    fileprivate func template1(){
    }
    
    //MARK: 根据模板index，放大textView
    /**
     根据模板index，放大textView
     */
    fileprivate func zoomInTextView(_ textView: UITextView){
        
//        if textView.tag == greetingTextViewTag {
//            blessingImageView?.hidden = true//隐藏提示书写祝福的图片
//        }
        
        switch templateIndex {
            
        case 0:
            UIView.animate(withDuration: 0.4, delay: 0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                self.transform = self.originalTransform!.scaledBy(x: 1.5, y: 1.5)
                self.transform = self.transform.translatedBy(x: 30/baseWidth, y: 0)
                }, completion: nil)
            
        case 1:
            
            //对greetingTextView进行放大
            if textView.tag == greetingTextViewTag {
                
                UIView.animate(withDuration: 0.4, delay: 0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                    self.transform = self.originalTransform!.scaledBy(x: 2.2, y: 2.2)
                    self.transform = self.transform.translatedBy(x: 80/baseWidth, y: -10/baseWidth)
                    }, completion: nil)
            }
            //对greetingTextView2进行放大
            else{
                UIView.animate(withDuration: 0.4, delay: 0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                    self.transform = self.originalTransform!.scaledBy(x: 2.2, y: 2.0)
                    self.transform = self.transform.translatedBy(x: -80/baseWidth, y: -20/baseWidth)
                    }, completion: nil)
            }
            
            
        default:
            break
        }
    }
    
    //MARK: 根据模板index，缩小textView
    /**
     根据模板index，缩小textView
     */
    fileprivate func zoomOutTextView(){
        UIView.animate(withDuration: 0.3, delay: 0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            self.transform = self.originalTransform!
            }, completion: nil)
    }
    
    //MARK:autoLayout的动画
    /**
     autoLayout的动画
     */
    fileprivate func confirmAutoLayoutAnimation(){
        
        self.setNeedsUpdateConstraints()//约束需要更新
        self.updateConstraintsIfNeeded()//检测需要更新的约束
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.layoutIfNeeded()//强制更新约束
            }, completion: nil)
    }
}

//MARK:--------------UITextViewDelegate------------
extension MUBackEditView: UITextViewDelegate{
    
    //MARK: 开始编辑greetingTextView
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        zoomInTextView(textView)//方法视图
        
        originalTextLength = textView.text.characters.count//取得当前textView的字符数
        
        if textView.tag == greetingTextViewTag {
            
            if shouldPlaceHolderShow == false {
                return
            }
            
            let textView: ZTTextView = textView as! ZTTextView
            
            textView.text = ""
            textView.textFontSize = defualTextSize
            textView.textColor = UIColor.black
            
            textView.spellCheckingType = UITextSpellCheckingType.default//开启拼写检查
            
            greetingTextViewContentOffset = CGPoint(x: 0, y: -(HEIGHT(textView)-textView.contentSize.height)/2)//将textView的文字居中
        }
    }
    
    //MARK: 结束编辑greetingTextView
    func textViewDidEndEditing(_ textView: UITextView) {
        
        zoomOutTextView()//将视图缩小到原始大小
        
        if textView.tag == greetingTextViewTag {
            
            if greetingTextView?.text == "" {//文字为空时，设置占位自负
                //设置占位符
//                greetingTextView?.text      = placeHolder
                greetingTextView?.font      = UIFont.systemFont(ofSize: 9)
                greetingTextView?.textColor = UIColor(hexString: "B9BCBB")
                shouldPlaceHolderShow       = true
                
            }else{
                textView.spellCheckingType = UITextSpellCheckingType.no//关闭拼写检查
                
                let text: String      = textView.text//去除因为拼写检查而出现的红色下滑虚线
                textView.text = ""
                textView.text = text
                shouldPlaceHolderShow  = false
            }
        }else{
            
            textView.spellCheckingType = UITextSpellCheckingType.no//关闭拼写检查
            
            let text: String      = textView.text//去除因为拼写检查而出现的红色下滑虚线
            textView.text = ""
            textView.text = text
            
        }
        //重新计算文字在垂直的偏移量
        resizeTextView(textView)
        textViewEndEditingClosure?(textView as! ZTTextView)
    }
    
    //MARK: 文字正在编辑中，文字大小随文字多少而改变
    func textViewDidChange(_ textView: UITextView) {
        
        let textView: ZTTextView = textView as! ZTTextView
        
        //没有文字时，不缩放字体
        if textView.text.characters.count == 0 {
            textView.textFontSize = defualTextSize
            return
        }
        textView.sizeFont(toFit: textView.text, minSize: 5, maxSize: Float(defualTextSize))
        
        //重新计算文字在垂直的偏移量
        resizeTextView(textView)
    }
    
    //MARK: 使greetingView的text垂直居中
    /**
     使greetingView的text垂直居中
     */
    func resizeTextView(_ textView: UITextView) {
        greetingTextViewContentOffset = CGPoint(x: 0, y: -(HEIGHT(textView)-textView.contentSize.height)/2)//将textView的文字居中
        originalTextLength = textView.text.characters.count
    }
    
    //MARK: 从草稿获取文字时，需要重新排版文字
    /// 从草稿获取文字时，需要重新排版文字
    func resizeTextViewAndFont(){
        shouldPlaceHolderShow = false
        greetingTextView!.sizeFont(toFit: greetingTextView!.text, minSize: 5, maxSize: Float(defualTextSize))
        //重新计算文字在垂直的偏移量
        resizeTextView(greetingTextView!)
    }
}

//MARK: ------------手势和点击事件-----------------
extension MUBackEditView{
    
    //MARK: 点击signatureView 的事件，出现编辑签名的界面
    func touchSignatureView(_ tap: UITapGestureRecognizer){
        touchSignatureViewClosure?()
    }
    
    //MARK: 点击了选择字体或键盘的按钮
    /**
     点击了选择字体或键盘的按钮
     
     - parameter button: UIBarButtonItem
     */
    func switchFontOrKeyBoard(_ button: UIBarButtonItem){
        
        if isKeyBoardShow {
            
            button.title = NSLocalizedString("键盘", comment: "")
            
            if greetingTextView!.isFirstResponder {
                greetingTextView?.inputView  = fontCollectionView
            }
        }else{
            button.title = NSLocalizedString("字体", comment: "")
            greetingTextView?.inputView  = nil
        }
        
        if greetingTextView!.isFirstResponder {
            greetingTextView?.resignFirstResponder()
            greetingTextView?.becomeFirstResponder()
        }
        
        isKeyBoardShow = !isKeyBoardShow
    }
    
    //MARK: 点击了键盘上完成按钮
    /**
     点击了键盘上完成按钮
     
     - parameter button: UIBarButtonItem
     */
    func textViewEndEditing(_ button: UIBarButtonItem) {
        if greetingTextView!.isFirstResponder == true {
            greetingTextView?.endEditing(true)
        }
    }
}

//MARK:---------------谷歌地址反编译API---------------------------
extension MUBackEditView {
    
    //MARK: 开始反编译地理信息
    /**
     开始反编译地理信息
     */
    func requestLocationInfo(location: CLLocation?) {
        //通过谷歌地图API反编译地址，分别获取中文和英文的字段
        var language: String
        if getPreferredLanguage()?.contains("zh") == true {
            language = "zh-CN"
        }else{
            language = "en-GB"
        }
        requestCityName(language: language, withLocation: location)
    }
    
    //MARK: 谷歌API反编译地址
    /**
     谷歌API反编译地址
     
     - parameter location: 地理位置 CLLocation
     - parameter language: 英文字段或中文字段
     */
    fileprivate func requestCityName(language: String, withLocation location: CLLocation?) {
        
        /**
         谷歌地址反编译
         GET https://maps.google.cn/maps/api/geocode/json 国内服务器
         GET https://maps.googleapis.com/maps/api/geocode/json 国外服务器
         */
        
        // Add Headers
        let headers: [String: String] = [
            "Content-Type":"application/x-www-form-urlencoded; charset=utf-8",
            ]
        
        //解析出经纬度
        var latlng: String

        if let coordinate: CLLocationCoordinate2D = location?.coordinate{
            latlng = "\(coordinate.latitude),\(coordinate.longitude)"
        }else{
            latlng = getLocationString()
        }
        
        // Add URL parameters
        let urlParams: [String: Any] = [
            "latlng": latlng,
            "key": appAPIHelp.googleAPIKey,
            "result_type": "locality",
            "location_type": "APPROXIMATE",
            "language": language,
            ]
        
        //分别尝试中国和国外的服务器
        alamofireRequest(url: "https://maps.google.cn/maps/api/geocode/json", withUrlParams: urlParams, withHeaders: headers, withLanguage: language)
//        alamofireRequest(url: "https://maps.googleapis.com/maps/api/geocode/json", withUrlParams: urlParams, withHeaders: headers, withLanguage: language)
    }
    
    //MARK: 反编译地址的网络请求
    /**
     反编译地址的网络请求
     
     - parameter url:       服务器地址
     - parameter urlParams: 参数
     - parameter headers:   请求头
     - parameter language:  字段语言
     */
    fileprivate func alamofireRequest(url: String, withUrlParams urlParams: [String: Any], withHeaders headers: [String: String], withLanguage language: String){
        
        // Fetch Request
        Alamofire.request(url, method: HTTPMethod.get, parameters: urlParams, encoding: URLEncoding.default, headers: headers)
            .validate()
            .responseJSON {[weak self ] (response: DataResponse<Any>) in
//                if let value = response.result.value{
//                    print(value)
//                }
                switch response.result{
                case .success(let json as [String: Any]):
                    
                    if let resultArray: [[String: Any]] = json["results"] as? [[String: Any]]{
                        
                        if let address_components: [[String: AnyObject]] = resultArray.first?["address_components"] as? [[String: AnyObject]] {
                            
                            switch language{
                                
                            case "zh-CN":
                                
                                for dic: [String: Any] in address_components {
                                    
                                    let type: [String] = dic["types"] as! [String]
                                    
                                    switch type[0]{
                                        
                                    case "locality":
                                        self?.cityInCN = dic["long_name"] as! String
                                    case "administrative_area_level_1":
                                        self?.stateInCN = dic["long_name"] as! String
                                    case "country":
                                        self?.countryInCn = dic["long_name"] as! String
                                    default: break
                                    }
                                }
                                
                            case "en-GB":
                                
                                for dic: [String: Any] in address_components {
                                    
                                    let type: [String] = dic["types"] as! [String]
                                    
                                    switch type[0]{
                                        
                                    case "locality":
                                        self?.cityInEn = dic["long_name"] as! String
                                    case "administrative_area_level_1":
                                        self?.stateInEn = dic["long_name"] as! String
                                    case "country":
                                        self?.countryInEn = dic["long_name"] as! String
                                    default: break
                                    }
                                }
                                
                            default: break
                            }
                            DispatchQueue.main.async {
                                self?.setLocationLabelText()//通过地址字段，拼接完整的地址
                            }
                        }
                    }
                case .failure(_):
                    break
                default:
                    break
                }
        }
    }
    
    //MARK: 拼接地址
    /**
     拼接地址
     */
    fileprivate func setLocationLabelText(){
        
        let systemLan: String? = getPreferredLanguage()//获取系统语言
        
        if systemLan == nil {
            
            locationText = "\(cityInEn) \(stateInEn) \(countryInEn)"
            
        }else if systemLan!.contains("zh"){//中文
            
            locationText = "\(countryInCn) \(stateInCN) \(cityInCN)"
            
        }else{//其它语言
            
            locationText = "\(cityInEn) \(stateInEn) \(countryInEn)"
        }
    }
}

