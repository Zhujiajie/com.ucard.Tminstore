//
//  MULabel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/19.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MULabel: UILabel {

    //MARK: 录制界面的时间Label
    /// 录制界面的时间Label
    ///
    /// - Returns: MULabel
    class func timeLabel()->MULabel{
        let timeLabel: MULabel = MULabel()
        timeLabel.textColor = UIColor(hexString: "8B8B8B")
        timeLabel.font = UIFont.systemFont(ofSize: 13)
        timeLabel.textAlignment = NSTextAlignment.center
        return timeLabel
    }
    
    //MARK: 提示用户拖动来编辑视频的label
    class func introLabel()->MULabel{
        
        let introLabel: MULabel = MULabel()
        introLabel.textColor = UIColor(hexString: "999999")
        introLabel.font = UIFont.systemFont(ofSize: 12)
        introLabel.text = NSLocalizedString("↓拖动选择你要裁剪的片段", comment: "")
        
        return introLabel
    }
    
    //MARK: 编辑视频界面的时间Label
    /// 编辑视频界面的时间Label
    ///
    /// - Returns: MULabel
    class func editVideoTimeLabel()->MULabel{
        let timeLable: MULabel = MULabel()
        timeLable.textColor = UIColor(hexString: "999999")
        timeLable.font = UIFont.systemFont(ofSize: 18)
        return timeLable
    }
}
