//
//  MUSignatureView.h
//  UcardStore
//
//  Created by Wenslow on 16/6/1.
//  Copyright © 2016年 ucard. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SmoothLineView;

@interface MUSignatureView : UIView

@property (nonatomic, strong) SmoothLineView *smoothView;
@property (nonatomic, strong) UIButton *commitButton;
@property (nonatomic, strong) UIButton *clearButton;

@property (nonatomic, assign) BOOL empty;
@property (nonatomic, strong) void (^commitBlock) ();
//@property (nonatomic, strong) void (^dismissBlock) ();

#pragma mark--获取已保存的路径
- (void)getSignaturePath;

#pragma mark--获取签名图片
- (UIImage *)getSignatureImage;

#pragma mark--清除按钮
- (void)touchClearButton: (UIButton *)button;
#pragma mark--提交按钮事件
- (void)touchCommitButton: (UIButton *)button;

@end
