//
//  ZTTLabel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/6.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class ZTTLabel: UILabel {

    override func drawText(in rect: CGRect) {
        
        if let stringText: String = text {
            
            let stringTextAsNSString: NSString = stringText as NSString
            
            let labelStringSize: CGSize = stringTextAsNSString.boundingRect(with: CGSize(width: WIDTH(self), height: CGFloat.greatestFiniteMagnitude), options: [.usesLineFragmentOrigin], attributes: [NSFontAttributeName: font] as [String: Any], context: nil).size
            
            super.drawText(in: CGRect(x: rect.origin.x, y: rect.origin.y, width: WIDTH(self), height: ceil(labelStringSize.height)))
            
        } else {
            super.drawText(in: rect)
        }
    }

}
