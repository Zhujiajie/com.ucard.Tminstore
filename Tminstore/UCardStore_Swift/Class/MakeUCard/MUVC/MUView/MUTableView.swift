//
//  MUTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/20.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Photos
import SnapKit



class MUTableView: UITableView {

    //MARK: 展示相片分类的tableView
    class func photoTableView()->MUTableView{
        
        let photoTableView: MUTableView = MUTableView(frame: CGRect.zero, style: UITableViewStyle.plain)
        photoTableView.register(MUAlbumTableViewCell.self, forCellReuseIdentifier: CellIdentifier.allPhotos.rawValue)
        photoTableView.register(MUAlbumTableViewCell.self, forCellReuseIdentifier: CellIdentifier.smartAlbums.rawValue)
        photoTableView.register(MUAlbumTableViewCell.self, forCellReuseIdentifier: CellIdentifier.userCollections.rawValue)
        photoTableView.backgroundColor = UIColor.clear
        photoTableView.estimatedRowHeight = 82
        
        return photoTableView
    }

}

//MARK: 展示相册分类的cell
class MUAlbumTableViewCell: UITableViewCell{
    
    /// 相片资源
    var asset: PHAsset?{
        didSet{
            setAsset()
        }
    }
    
    var title: String?{
        didSet{
            albumTitleLabel?.text = title!
        }
    }
    
    var numberOfPhotos: Int?{
        didSet{
            photoNumberLabel?.text = "(\(numberOfPhotos!))"
        }
    }
    
    fileprivate var imageManager: PHImageManager?
    
    /// 展示缩略图
    fileprivate var albumImageView: UIImageView?
    /// 展示相册名称
    fileprivate var albumTitleLabel: UILabel?
    /// 展示相片数量
    fileprivate var photoNumberLabel: UILabel?
    /// 缩略图的大小
    fileprivate let thumbnailSize = CGSize(width: 144, height: 144)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = UIColor.clear
        
        albumImageView = UIImageView()
        albumImageView?.contentMode = UIViewContentMode.scaleAspectFill
        albumImageView?.backgroundColor = UIColor.white
        albumImageView?.clipsToBounds = true
        contentView.addSubview(albumImageView!)
        albumImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.equalTo(contentView).offset(5)
            make.bottom.equalTo(contentView).offset(-5)
            make.size.equalTo(CGSize(width: 72, height: 72))
        })
        albumImageView?.image = #imageLiteral(resourceName: "photoPlaceHolder")
        
        albumTitleLabel = UILabel()
        albumTitleLabel?.textColor = UIColor(hexString: "4D4D4D")
        albumTitleLabel?.font = UIFont.systemFont(ofSize: 16)
        albumTitleLabel?.textAlignment = NSTextAlignment.left
        contentView.addSubview(albumTitleLabel!)
        albumTitleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(albumImageView!.snp.right).offset(19/baseWidth)
            make.height.equalTo(25)
        })
        
        photoNumberLabel = UILabel()
        photoNumberLabel?.textColor = UIColor(hexString: "8D8D8D")
        photoNumberLabel?.font = UIFont.systemFont(ofSize: 14)
        photoNumberLabel?.textAlignment = NSTextAlignment.left
        contentView.addSubview(photoNumberLabel!)
        photoNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(albumTitleLabel!.snp.right).offset(6/baseWidth)
            make.height.equalTo(25)
        })
        
        imageManager = PHImageManager()
    }
    
    //MARK: 往cell里填充图片
    fileprivate func setAsset(){
        if asset != nil {
            imageManager?.requestImage(for: asset!, targetSize: thumbnailSize, contentMode: PHImageContentMode.aspectFit, options: nil, resultHandler: { [weak self] (image: UIImage?, _) in
                if image != nil {
                    self?.albumImageView?.image = image!
                }
            })
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        albumImageView?.image = #imageLiteral(resourceName: "photoPlaceHolder")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
