//
//  MUScrollView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/21.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

let newContainerWidth: CGFloat = 364/baseWidth
let newContainerHeight: CGFloat = 259/baseWidth

class MUScrollView: UIView {
    
    /// 照片所对应的photoCollectionView的indexPath
    var photoIndexPath: IndexPath?
    
    /// 是否添加过图片，若没添加过图片，则无法添加滤镜
    var isImageAdd: Bool = false{
        didSet{
            addButton?.isHidden = isImageAdd
        }
    }
    
    /// 对外部开放的image
    var image: UIImage?{
        didSet{
            if image == nil {return}
            scrollView?.setZoomScale(1.0, animated: true)
            isImageAdd = true
            imageWasSet()
        }
    }
    
    /// 展示照片的UIImageView
    var imageView: UIImageView?

    /// 成功获取照片的闭包
    var getImageSuccessful: (()->())?
    
    /// 图片更宽
    var widerImage: (()->())?
    /// 图片更高
    var heigherImage: (()->())?
    
    /// 将scroll的缩放功能加在这个容器视图上，旋转手势则加在imageView上
    var containerView: UIView?
    
    //用于实现缩放的scrollView
    var scrollView: ZTScrollView?
    /// 原始的照片
    var originalImage: UIImage?
    /// 加了滤镜的image
    fileprivate var filterImage: UIImage?
    
    /// 添加图片的按钮，添加图片之后，该按钮要隐藏
    fileprivate var addButton: UIButton?
    /// 原始original
    fileprivate var originalRotateTranform: CGAffineTransform?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        setScrollView()
    }
    
    //MARK: 设置边框
    /**
     设置边框
     */
    func setLayerBoard() {
        if isImageAdd == false {
            scrollView?.layer.borderColor   = UIColor.flatGray().cgColor
            scrollView?.layer.borderWidth   = 1
        }
    }
    
    //MARK: 取消边框
    /**
     取消边框
     */
    func cancelLayerBoard() {
        scrollView?.layer.borderColor   = UIColor.clear.cgColor
        scrollView?.layer.borderWidth   = 0
    }
    
    //MARK: 截取当前的图片
    /**
     截取当前的图片
     
     - returns: UIImage
     */
    func getScrollViewImage()->UIImage{
        
        let scale: CGFloat = 1750.0 / self.bounds.size.width * 1.0
        
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, scale)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    
    //MARK: 设置UI
    /**
     设置UI
     */
    fileprivate func setScrollView(){
        
        self.backgroundColor     = UIColor.clear
        
        scrollView                      = ZTScrollView()
        scrollView?.minimumZoomScale    = 1.0//设置缩放程度
        scrollView?.maximumZoomScale    = 2.0
        scrollView?.zoomScale           = 1.0
        scrollView?.backgroundColor = UIColor.clear
        
        self.addSubview(scrollView!)
        scrollView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        
        //缩放功能要加在这个容器视图上
        containerView = UIView()
        containerView?.backgroundColor = UIColor.clear
        containerView?.frame = CGRect(x: 0, y: 0, width: newContainerWidth, height: newContainerHeight)
        scrollView?.addSubview(containerView!)
        
        imageView = UIImageView()
        imageView?.contentMode = UIViewContentMode.scaleAspectFit
        imageView?.frame = CGRect(x: 0, y: 0, width: 30/baseWidth, height: 30/baseWidth)
        containerView?.addSubview(imageView!)
        imageView?.center = (containerView?.center)!
        imageView?.layer.anchorPoint = CGPoint(x: 0.5, y:0.5)//围绕中心旋转
        //抗锯齿
        imageView?.layer.shouldRasterize = true
        imageView?.layer.rasterizationScale = UIScreen.main.scale
        originalRotateTranform = imageView?.transform//保存原始transform
        
        layer.masksToBounds = false
        containerView?.layer.masksToBounds = false
        imageView?.layer.masksToBounds = false
        imageView?.clipsToBounds = false
    }
    
    //MARK: 设置了image，开始设置imageView
    /**
     设置了image，开始设置imageView
     */
    fileprivate func imageWasSet(){
        
        //先将imageView旋转到原始角度
        UIView.animate(withDuration: 0.3, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            self.imageView?.transform = self.originalRotateTranform!
            }) { (_) in
                self.resizeImage(self.image!)
        }
    }
    
    //MARK:缩放图片
    /**
     缩放图片
     
     - parameter image: UIImage
     */
    fileprivate func resizeImage(_ image: UIImage) {
        
        originalImage = getNewImage(image)
        
        let size: CGSize = originalImage!.size
        
        containerView?.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        
        imageView?.image = originalImage
        imageView?.frame = (containerView?.frame)!
        
        scrollView?.contentSize = size
        
        if size.width > size.height{
            let offset: CGFloat = size.width - (scrollView?.frame.width)!
            scrollView?.contentOffset = CGPoint(x: offset/2, y: CGFloat(0))
        }else{
            let offset:CGFloat = size.height - (scrollView?.frame.height)!
            scrollView?.contentOffset = CGPoint(x: CGFloat(0), y: offset/2)
        }
        
        getImageSuccessful?()//成功获取图片的闭包
    }
    
    //MARK:重新缩放图片
    /**
     重新缩放图片
     
     - parameter photo: 图片
     
     - returns: 返回缩放过的照片
     */
    fileprivate func getNewImage(_ photo: UIImage) -> UIImage{
        
        var targetSize: CGSize = CGSize.zero
        
        //根据图片大小和比例重新计算新图片的大小
        if photo.size.width > photo.size.height {
            
            targetSize = CGSize(width: photo.size.width * (newContainerHeight / photo.size.height), height: newContainerHeight)
            
            if targetSize.width < newContainerWidth {
                targetSize = CGSize(width:newContainerWidth, height: photo.size.height * newContainerWidth / photo.size.width)
            }
            
        }else{
            
            targetSize = CGSize(width:newContainerWidth, height: photo.size.height * newContainerWidth / photo.size.width)
            
            if targetSize.width < newContainerWidth {
                targetSize = CGSize(width: photo.size.width * newContainerHeight / photo.size.height, height: newContainerHeight)
            }
        }
        //获得需要缩放的比例
        let imageScale: CGFloat = photo.size.width / targetSize.width
        return UIImage(cgImage: photo.cgImage!, scale: imageScale, orientation: .up)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class ZTScrollView: UIScrollView{
    
    override func touchesShouldBegin(_ touches: Set<UITouch>, with event: UIEvent?, in view: UIView) -> Bool {
        
        return true
    }
}
