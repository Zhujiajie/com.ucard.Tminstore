//
//  ZTTextView.h
//  UcardStore
//
//  Created by Wenslow on 16/6/1.
//  Copyright © 2016年 ucard. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSTextView.h"

@interface ZTTextView : FSTextView

@property (nonatomic, assign) NSInteger textFontSize;
@property (nonatomic, strong) NSString *fontName;
@property (nonatomic, assign) NSInteger origialTextLength;
@property (nonatomic, strong, readonly) NSString *placeHolderFontName;

- (BOOL)sizeFontToFit: (NSString *)aString minSize:(float)aMinFontSize maxSize:(float)aMaxFontSize;

@end
