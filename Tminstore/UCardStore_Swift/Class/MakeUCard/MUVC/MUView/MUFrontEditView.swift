//
//  MUFrontEditView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/4.
//  Copyright © 2016年 ucard. All rights reserved.
//

//----------------明星片正面的编辑界面----------------
import UIKit

let addImageButtonTag = 123

let setTemplateNotification: String = "setTemplateNotification"
let isHorizonKey: String = "isHorizonKey"
let templateIndexKey: String = "templateIndexKey"

class MUFrontEditView: UIView {
    
    /// 排版的样式
    var templateIndex: Int = 0
    
    /// 是否适用横版模板
    var isHorizon: Bool = true
    
    //scrollView
    var scrollView0: MUScrollView = MUScrollView()
//    var scrollView1 = MUScrollView()
//    var scrollView2 = MUScrollView()
//    var scrollView3 = MUScrollView()

    var scrollViewArray: [MUScrollView] = [MUScrollView]()
//    var imageViewArray = [UIImageView]()
    
    var totalRotate: Double = 0
    
    /// addImageAndChooseFilterView是否隐藏了
    var isAddImageAndChooseFilterViewHide: Bool = true
    /// 被选中的scrollView的index
    var indexOfSelectedScrollView: Int = 0
//    /// 点击了scrollView的闭包
//    var touchScrollViewClosure: ((_ alpha: CGFloat, _ isImageAdded: Bool, _ isScrollViewSelected: Bool)->())?
    
    /// 当添加图片时，是否需要根据图片的长宽高自动更改横竖版
    var shouldChangeTemplate: ((_ isHorizon: Bool)->())?
    
    /// 教程图片
    var tutorialImageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUI()
        self.backgroundColor = UIColor.clear
        
//        isHorizon = true//设置模板
        templateIndex = 0
        
        self.layer.masksToBounds = false
    }
    
    //MARK: 设置UI
    /**
     设置UI
     */
    fileprivate func setUI(){
        
        scrollViewArray = [scrollView0]
        
        var i = 0
        
        for scrollView in scrollViewArray {
            
            scrollView.tag = i
            
            self.addSubview(scrollView)
            scrollView.scrollView!.delegate = self
            
            //给scrollView增加旋转手势
            let rotate: UIRotationGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(handelRotation(_:)))
            scrollView.addGestureRecognizer(rotate)
            rotate.delegate = self
            
            i += 1
        }
        
        if isHorizon {
            chooseHorizonTemplate()//排版
        }
    }
    
    
    //MARK: 旋转手势
    func handelRotation(_ recognizer: UIRotationGestureRecognizer) {
        
        switch recognizer.state {
            
        case UIGestureRecognizerState.began, UIGestureRecognizerState.changed:
            
            let scrollView: MUScrollView = recognizer.view as! MUScrollView
            let imageView: UIImageView? = scrollView.imageView
            
            //开始旋转
            totalRotate += Double(recognizer.rotation)
            imageView!.transform = imageView!.transform.rotated(by: recognizer.rotation)
            recognizer.rotation = 0
            
        case UIGestureRecognizerState.cancelled, UIGestureRecognizerState.ended:

            totalRotate = 0//清零

            break
        default:
            break
        }
    }
    
    //MARK: 将明信片正面保存为图片
    /**
     将明信片正面保存为图片
     
     - returns: UIImage
     */
    func getFrontImage() -> UIImage{
        
//        let scale = CGFloat(1750.0 / 296 * 1.0)
        let scale = CGFloat(1750.0 / 296.0 * 1.0)//去除图片底部的黑边
        
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, scale);
        //背景
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        //绘制相片
        for scrollView: MUScrollView in scrollViewArray {
            
            if scrollView.isImageAdd {
                let scrollViewRect: CGRect = self.convert(scrollView.frame, from: self)
                scrollView.getScrollViewImage().draw(in: scrollViewRect)
            }
        }
        
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    
//    //MARK: 第一次添加完照片之后，出现提示
//    /**
//     第一次添加完照片之后，出现提示
//     */
//    func setTutorial() {
//        
//        if getTutorialStatus() == true { return }//已经看过教程，则直接跳过
//        
//        tutorialImageView = UIImageView(image: UIImage(named: "clickImage"))
//        self.addSubview(tutorialImageView!)
//        self.bringSubview(toFront: tutorialImageView!)
//        tutorialImageView?.snp.makeConstraints { (make) in
//            make.right.equalTo(self.snp.right).offset(-96/baseWidth)
//            make.bottom.equalTo(self.snp.bottom).offset(-95/baseWidth)
//            make.width.equalTo(39/baseWidth)
//            make.height.equalTo(74/baseWidth)
//        }
//        
//        let fade = CABasicAnimation(keyPath: "opacity")
//        fade.fromValue = 1.0
//        fade.toValue = 0.25
//        fade.autoreverses = true
//        fade.repeatCount = Float.infinity
//        fade.duration = 1
//        tutorialImageView?.layer.add(fade, forKey: "fadein")
//        
//        setTutorialStatus()//不重复加载该动画
//    }
    
//    //MARK: 去除教程图片的动画
//    /**
//     去除教程图片的动画
//     */
//    func removeAllAnimation() {
//        
//        if tutorialImageView != nil {
//            tutorialImageView?.layer.removeAllAnimations()
//            tutorialImageView?.layer.opacity = 0.0
//        }
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK:------------UIScrollViewDelegate, UIGestureRecognizerDelegate-----------------
extension MUFrontEditView: UIScrollViewDelegate, UIGestureRecognizerDelegate{
    
    //MARK:允许多手势
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //MARK: scrollView的缩放
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        let sv: MUScrollView = scrollView.superview as! MUScrollView
        return sv.containerView
    }
}

//MARK:------autoLayout实现排版-------------
extension MUFrontEditView{
    
    //MARK: 选择横版排版
    /**
     选择横版排版
     
     - parameter index: 排版编号
     */
    func chooseHorizonTemplate(){
        
        switch templateIndex {
        case 0: horizontalTemplate0()
//        case 1: horizontalTemplate1()
        default:
            break
        }
        confirmAutoLayoutAnimation()
//        let _ = scrollViewArray.map { (scrollView) in scrollView.isHorizon = true }
    }
    
    //MARK: 选择竖版排版
    /**
     选择竖版排版
     
     - parameter index: 排版编号
     */
    func chooseVerticalTemplate(){
        
//        switch templateIndex {
//        case 0: verticalTemplate0()
//        case 1: verticalTemplate1()
//        default:
//            break
//        }
        confirmAutoLayoutAnimation()
//        let _ = scrollViewArray.map { (scrollView) in scrollView.isHorizon = false}
    }
    
    //MARK:第一种横版拍版
    /**
     第一种横版拍版
     */
    fileprivate func horizontalTemplate0(){
        
        scrollView0.snp.remakeConstraints { (make) in
            make.edges.equalTo(self)
        }
    }
    
    //MARK:autoLayout的动画
    /**
     autoLayout的动画
     */
    fileprivate func confirmAutoLayoutAnimation(){
        
        self.setNeedsUpdateConstraints()//约束需要更新
        self.updateConstraintsIfNeeded()//检测需要更新的约束
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.curveEaseOut, animations: {
            self.layoutIfNeeded()//强制更新约束
            }, completion: nil)
    }
}
