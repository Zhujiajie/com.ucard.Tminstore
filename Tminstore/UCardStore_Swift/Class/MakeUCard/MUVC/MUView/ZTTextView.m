//
//  ZTTextView.m
//  UcardStore
//
//  Created by Wenslow on 16/6/1.
//  Copyright © 2016年 ucard. All rights reserved.
//

#import "ZTTextView.h"

@interface ZTTextView ()<UITextViewDelegate>

@end

@implementation ZTTextView

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        //默认字体和大小
        _textFontSize = 14;
        
        UIFont *sysFont = [UIFont systemFontOfSize:9];
        _placeHolderFontName = [sysFont fontName];
        _fontName = [sysFont fontName];
        
        self.backgroundColor = [UIColor clearColor];
        
        //允许文字滚出界面外
        self.clipsToBounds = NO;
        self.textContainer.maximumNumberOfLines = 25;
        self.maxLength = 400;
        
        //关闭自动更正
        //self.autocorrectionType = UITextAutocorrectionTypeNo;
        
        //关闭滚动指示器
        self.showsVerticalScrollIndicator = NO;
        
        self.placeholder = NSLocalizedString(@"写下我的祝福", @"");
    }
    return self;
}


- (void)setTextFontSize:(NSInteger)textFontSize{
    _textFontSize = textFontSize;
    self.font = [UIFont fontWithName:_fontName size:_textFontSize];
    
}

- (void)setFontName:(NSString *)fontName{
    _fontName = fontName;
    if (_fontName == NSLocalizedString(@"默认", comment: @"")) {
        self.font = [UIFont systemFontOfSize:_textFontSize];
    }else{
        self.font = [UIFont fontWithName:_fontName size:_textFontSize];
    }
}

- (BOOL)sizeFontToFit: (NSString *)aString minSize:(float)aMinFontSize maxSize:(float)aMaxFontSize{
    float fudgeFactor = 16.0;
    float fontSize = aMaxFontSize;
    
    self.font = [self.font fontWithSize:fontSize];
    
    CGSize tallerSize = CGSizeMake(self.frame.size.width - fudgeFactor, MAXFLOAT);
    CGSize stringSize = [aString boundingRectWithSize:tallerSize options: NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.font} context:nil].size;
    
    while (stringSize.height >= self.frame.size.height) {
        if (fontSize <= aMinFontSize) {
            self.font = [self.font fontWithSize:aMinFontSize];
            self.textFontSize = (NSInteger)aMinFontSize;
            return NO;
        }
        fontSize -= 1.0;
        self.font = [self.font fontWithSize:fontSize];
        tallerSize = CGSizeMake(self.frame.size.width - fudgeFactor, MAXFLOAT);
        stringSize = [aString boundingRectWithSize:tallerSize options: NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.font} context:nil].size;
    }
    
    self.font = [self.font fontWithSize:fontSize];
    self.textFontSize = (NSInteger)fontSize;
    return YES;
}

@end
