//
//  MUSignatureView.m
//  UcardStore
//
//  Created by Wenslow on 16/6/1.
//  Copyright © 2016年 ucard. All rights reserved.
//
#import "MUSignatureView.h"
#import "SmoothLineView.h"

#define SignaturePath @"SignaturePath"
//屏幕高度和宽度
#define screenWidth [UIScreen mainScreen].bounds.size.width
#define screenHeight [UIScreen mainScreen].bounds.size.height

//用于按比例计算autoLayout
#define baseWidth 375.0*screenWidth
#define baseHeight 667.0*screenHeight

static void saveApplier(void *info, const CGPathElement *element)
{
    NSMutableArray *array = (__bridge NSMutableArray *)info;
    
    int nPoints;
    switch (element->type) {
        case kCGPathElementMoveToPoint:
            nPoints = 1;
            break;
        case kCGPathElementAddLineToPoint:
            nPoints = 1;
            break;
        case kCGPathElementAddQuadCurveToPoint:
            nPoints = 2;
            break;
        case kCGPathElementAddCurveToPoint:
            nPoints = 3;
            break;
        case kCGPathElementCloseSubpath:
            nPoints = 0;
            break;
        default:
            [array replaceObjectAtIndex:0 withObject:[NSNumber numberWithBool:NO]];
            return;
    }
    
    NSNumber *type = [NSNumber numberWithInt:element->type];
    NSData *points = [NSData dataWithBytes:element->points length:nPoints * sizeof(CGPoint)];
    [array addObject:[NSDictionary dictionaryWithObjectsAndKeys:type, @"type", points, @"points", nil]];
}

@implementation MUSignatureView

//- (instancetype)init
//{
//    self = [super init];
//    if (self) {
//     
//        [self setSignatureView];
//        
//    }
//    return self;
//}
//
//#pragma mark--设置
//- (void)setSignatureView{
//    
//    self.backgroundColor = [UIColor whiteColor];
//    //设置绘制签名区域
//    [self setSignatureField];
//    [self setButtons];
//    
//}
//
//#pragma mark--设置绘制签名区域
//- (void)setSignatureField{
//    _smoothView = [SmoothLineView new];
//    
//    [self addSubview:_smoothView];
//    [_smoothView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(self.mas_top).mas_offset(10/baseHeight);
//        make.left.mas_equalTo(self.mas_left).mas_offset(10/baseWidth);
//        make.right.mas_equalTo(self.mas_right).mas_offset(-10/baseWidth);
//        make.bottom.mas_equalTo(self.mas_bottom).mas_offset(-10/baseHeight);
//    }];
//}
//
//#pragma mark--设置各种按钮
//- (void)setButtons{
//    
//    //提交按钮
//    _commitButton = [UIButton new];
//    [self addSubview:_commitButton];
//    [_commitButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.right.mas_equalTo(_smoothView.mas_right).mas_offset(-14/baseWidth);
//        make.bottom.mas_equalTo(_smoothView.mas_bottom).mas_offset(-14/baseHeight);
//    }];
//    [_commitButton setImage:[UIImage imageNamed:@"commitSignature"] forState:UIControlStateNormal];
//    [_commitButton addTarget:self action:@selector(touchCommitButton:) forControlEvents:UIControlEventTouchUpInside];
//    
//    //清除按钮
//    _clearButton = [UIButton new];
//    [self addSubview:_clearButton];
//    [_clearButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.mas_equalTo(_smoothView.mas_top).mas_offset(21/baseHeight);
//        make.right.mas_equalTo(_smoothView.mas_right).mas_offset(-18/baseWidth);
//    }];
//    [_clearButton setImage:[UIImage imageNamed:@"clearSignature"] forState:UIControlStateNormal];
//    [_clearButton addTarget:self action:@selector(touchClearButton:) forControlEvents:UIControlEventTouchUpInside];
//}

#pragma mark--提交按钮事件
- (void)touchCommitButton: (UIButton *)button{
    
    //保存签名
    [self saveSignature];
    if (_commitBlock) {
        _commitBlock();
    }
}

#pragma mark--清除按钮
- (void)touchClearButton: (UIButton *)button{
    [_smoothView clear];
}

#pragma mark--保存签名
- (void)saveSignature{
    
    if (!_smoothView.path){
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:SignaturePath];
    }
    // Convert path to an array
    NSMutableArray *a = [NSMutableArray arrayWithObject:[NSNumber numberWithBool:YES]];
    
    CGPathApply(_smoothView.path, (__bridge void *)(a), saveApplier);
    if (![[a objectAtIndex:0] boolValue]){return;}
    
    [[NSUserDefaults standardUserDefaults] setObject:a forKey:SignaturePath];
    
}

#pragma mark--获取已保存的路径
- (void)getSignaturePath{
    
    [_smoothView clear];
    
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:SignaturePath];
    
//    if (!array){
//        _smoothView.path = nil;
//        return;
//    }
    
    CGMutablePathRef p = CGPathCreateMutable();
    for (NSInteger i = 1, l = [array count]; i < l; i++)
    {
        NSDictionary *d = [array objectAtIndex:i];
        
        CGPoint *points = (CGPoint *) [[d objectForKey:@"points"] bytes];
        switch ([[d objectForKey:@"type"] intValue])
        {
            case kCGPathElementMoveToPoint:
                CGPathMoveToPoint(p, NULL, points[0].x, points[0].y);
                break;
            case kCGPathElementAddLineToPoint:
                CGPathAddLineToPoint(p, NULL, points[0].x, points[0].y);
                break;
            case kCGPathElementAddQuadCurveToPoint:
                CGPathAddQuadCurveToPoint(p, NULL, points[0].x, points[0].y, points[1].x, points[1].y);
                break;
            case kCGPathElementAddCurveToPoint:
                CGPathAddCurveToPoint(p, NULL, points[0].x, points[0].y, points[1].x, points[1].y, points[2].x, points[2].y);
                break;
            case kCGPathElementCloseSubpath:
                CGPathCloseSubpath(p);
                break;
            default:
                CGPathRelease(p);
                break;
        }
    }
    _smoothView.path = p;
    
    if (_smoothView.path == nil) {
        _empty = true;
    }else{
        _empty = false;
    }
}

#pragma mark--获取签名图片
- (UIImage *)getSignatureImage{
    UIGraphicsBeginImageContextWithOptions(_smoothView.frame.size, NO, 0.5);
    CGContextRef context = UIGraphicsGetCurrentContext();

    [_smoothView.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    //UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    return image;
}
@end
