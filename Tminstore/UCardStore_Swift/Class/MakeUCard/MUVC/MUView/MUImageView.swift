//
//  MUImageView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/9/28.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Photos
import SDWebImage

class MUImageView: UIImageView {

    /// 点击图片的事件
    var tapImageViewClosure: (()->())?
    
    /// 成功获取到用户的视频信息
    var fetchImageSuccess: (()->())?
    
    override init(image: UIImage?) {
        super.init(image: image)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapImageView(tap:)))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
    }
    
    //MARK: 展示用户第一个视频的imageView
    /// 展示用户第一个视频的imageView，若成功获取到图片，则展示该视图，否则需要隐藏
    ///
    /// - Returns: MUImageView
    class func videoImageView()->MUImageView{
        
        let videoImageView: MUImageView = MUImageView(image: nil)//先设置占位图片
        videoImageView.alpha = 0
        videoImageView.contentMode = UIViewContentMode.scaleAspectFill
        videoImageView.layer.masksToBounds = true
        videoImageView.isUserInteractionEnabled = true
        
        // 请求照片权限
        PHPhotoLibrary.requestAuthorization { (status: PHAuthorizationStatus) in
            if status == PHAuthorizationStatus.authorized{
                DispatchQueue.main.async {
                    
                    let imageManager: PHCachingImageManager = PHCachingImageManager()
                    
                    let scale: CGFloat = UIScreen.main.scale
                    let thumbnailSize: CGSize = CGSize(width: 120/baseWidth * scale, height: 120/baseWidth * scale)//缩略图大小
                    
                    let fetchOptions: PHFetchOptions = PHFetchOptions()
                    fetchOptions.sortDescriptors = [NSSortDescriptor(key: "modificationDate", ascending: false)]
                    fetchOptions.includeHiddenAssets = true//包含隐藏的照片
                    
                    let videoAlbum: PHFetchResult<PHAssetCollection> = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.smartAlbum, subtype: PHAssetCollectionSubtype.smartAlbumVideos, options: nil)//只获取视频
                    
                    if let collection: PHAssetCollection = videoAlbum.firstObject{
                        if let asset: PHAsset = PHAsset.fetchAssets(in: collection, options: fetchOptions).firstObject{
                            imageManager.requestImage(for: asset, targetSize: thumbnailSize, contentMode: PHImageContentMode.aspectFit, options: nil, resultHandler: { (image: UIImage?, info: [AnyHashable: Any]?) in
//                                print(info ?? "")
                                if image != nil{//请求完成
                                    DispatchQueue.main.async {
                                        videoImageView.image = image
                                        videoImageView.fetchImageSuccess?()//成功获取到视频缩略图
                                    }
                                }
                            })
                        }
                    }
                }
            }
        }
        return videoImageView
    }
    
    //MARK: 待添加图片的预览界面
    /// 待添加图片的预览界面
    ///
    /// - Parameter image: 从coredata中获取的图片
    /// - Returns: MUImageView
    class func previewImageView(image: UIImage?)-> MUImageView{
        
        let previewImageView: MUImageView = MUImageView(image: nil)
        previewImageView.contentMode = UIViewContentMode.scaleToFill
        
        if image == nil {
            
            previewImageView.alpha = 0
            
            previewImageView.sd_setImage(with: appAPIHelp.sampleImageURL(), completed: { (image, _, _, _) in
                previewImageView.image = image
                UIView.animate(withDuration: 0.5, delay: 1.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                    previewImageView.alpha = 1
                }, completion: nil)
            })
        }
            
//            previewImageView.sd_setImage(with: sampleImageURL(), placeholderImage: #imageLiteral(resourceName: "photoPlaceHolder"), options: SDWebImageOptions.refreshCached, completed: { (image, _, _, _) in
//                previewImageView.image = image
//                UIView.animate(withDuration: 0.5, delay: 1.0, options: [.curveEaseOut], animations: {
//                    previewImageView.alpha = 1
//                }, completion: nil)
//            })
        
        return previewImageView
    }

    //MARK: 绘制图片
    /// 绘制图片
    ///
    /// - returns: UIImage
    func getFrontImage() -> UIImage {
        
        let scale: CGFloat = 1750.0 / self.bounds.size.width * 1.0
        
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, scale)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    //MARK: 点击图片的事件
    func tapImageView(tap: UITapGestureRecognizer){
        tapImageViewClosure?()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
