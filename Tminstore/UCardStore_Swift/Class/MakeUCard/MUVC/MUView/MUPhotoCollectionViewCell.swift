//
//  MUPhotoCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/9.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

//MARK:---------------PhotoCollectionViewCell-------------

let DeselectedPhotoCellNotification: String = "DeselectedPhotoCellNotification"

class MUPhotoCollectionViewCell: UICollectionViewCell{
    
    var image: UIImage?{
        didSet{
            imageView?.image = image
        }
    }
    
    var isCellSelected: Bool = false{
        didSet{
            setBoard()//添加或者去除边框
        }
    }
    
    /// 用来获取image的identifier
    var representedAssetIdentifier: String = ""
    /// 用来展示摄像头的layer
    fileprivate var cameraLayer: AVCaptureVideoPreviewLayer?
    
    fileprivate var imageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = UIColor.clear
        self.layer.masksToBounds = true
        setImageView()//设置imageView
        
        NotificationCenter.default.addObserver(self, selector: #selector(deselectedPhotoCell(_:)), name: NSNotification.Name(rawValue: DeselectedPhotoCellNotification), object: nil)//注册通知中心，用来去除cell的边框
    }
    
    //MARK:设置imageView
    fileprivate func setImageView(){
        
        imageView                  = UIImageView()
        imageView?.contentMode     = UIViewContentMode.scaleAspectFill
        imageView?.backgroundColor = UIColor.clear
        imageView?.clipsToBounds   = true
        
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
    }
    
    //MARK: 获取摄像头的捕捉到影像
    /**
     获取摄像头的捕捉到影像
     
     - parameter cell: PhotoCollectionViewCell
     */
    func getCameraPreview() {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) == false { return }
        
        let session: AVCaptureSession = AVCaptureSession()
        session.sessionPreset = AVCaptureSessionPreset352x288
        
        if let device: AVCaptureDevice = AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo).first as? AVCaptureDevice {
            
            do{
                let input: AVCaptureDeviceInput = try AVCaptureDeviceInput(device: device)
                
                DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async(execute: {[weak self] _ in
                    
                    session.addInput(input)
                    
                    self?.frame.size = firstCellSize
                    self?.cameraLayer = AVCaptureVideoPreviewLayer(session: session)
                    self?.cameraLayer!.frame = (self?.bounds)!
                    
                    session.startRunning()
                    
                    DispatchQueue.main.async(execute: {
                        
                        
                        self?.contentView.layer.addSublayer((self?.cameraLayer)!)
                        self?.cameraLayer?.transform = CATransform3DMakeScale(1.5, 1.5, 1.0)
                    })
                })
            }catch{print(error.localizedDescription) }
        }
    }
    
    //MARK: 给cell添加边框或者去除边框
    /**
     给cell添加边框或者去除边框
     */
    fileprivate func setBoard(){
        
        if isCellSelected {
            self.layer.borderColor = UIColor(hexString: "68cfd6").cgColor
            self.layer.borderWidth = 2
            self.layer.cornerRadius = 2
        }else{
            self.layer.borderColor = UIColor.clear.cgColor
            self.layer.cornerRadius = 0
        }
    }
    
    //MARK: 将cell设置为未被选中
    /**
     将cell设置为未被选中
     
     - parameter info: NSNotification
     */
    func deselectedPhotoCell(_ info: Notification) {
        isCellSelected = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
        cameraLayer?.removeFromSuperlayer()
        isCellSelected = false
    }
    
    deinit{
        NotificationCenter.default.removeObserver(DeselectedPhotoCellNotification)//移除通知
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: ----------------photoCollectionView的layout----------------

let YOffset = photoCollectionViewCellSize.width + 3

class PhotoCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        
        self.itemSize = photoCollectionViewCellSize
        self.minimumInteritemSpacing = 3
        self.minimumLineSpacing = 3
        self.scrollDirection = .vertical
    }
    
    override var collectionViewContentSize : CGSize {
        
        var size = super.collectionViewContentSize
        
        size.height += YOffset + 3
        
        return size
    }
    
    
    //MARK: 重写itemSize
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var attArray = [UICollectionViewLayoutAttributes]()
        let cellCount = collectionView!.numberOfItems(inSection: 0)
        
        
        for i in 0..<cellCount {
            
            let indexPath = IndexPath(item: i, section: 0)
            
            let att = layoutAttributesForItem(at: indexPath)
            
            attArray.append(att!)
        }
        
        return attArray
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        let att = super.layoutAttributesForItem(at: indexPath)?.copy() as? UICollectionViewLayoutAttributes
        
        switch indexPath.item {
            
        case 0:
            
            att?.frame.size = firstCellSize
            return att
            
        case 1...2:
            
            att?.frame.origin.x += YOffset + 1.5/baseWidth
            
        case 3:
            
            att?.frame.origin.x -= YOffset
            att?.frame.origin.y += YOffset
            
            
        default:
            
            if indexPath.item % 4 == 0 {
                
                att?.frame.origin.x += 3 * YOffset + 3
                
            }else {
                att?.frame.origin.x -= YOffset
                att?.frame.origin.y += YOffset
            }
        }
        
        att!.frame.size = photoCollectionViewCellSize
        
        return att
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
