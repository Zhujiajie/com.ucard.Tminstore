//
//  MUCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/20.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUCollectionView: UICollectionView {

    //MARK: 展示相片的collectionView
    /// 展示相片的collectionView
    ///
    /// - Returns: MUCollectionView
    class func photoCollectionView()->MUCollectionView{
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = photoCollectionViewCellSize
        layout.minimumInteritemSpacing = 3
        layout.minimumLineSpacing = 3
        layout.scrollDirection = UICollectionViewScrollDirection.vertical
        
//        let layout: PhotoCollectionViewFlowLayout = PhotoCollectionViewFlowLayout()
        
        let photoCollectionView: MUCollectionView = MUCollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        photoCollectionView.register(MUPhotoCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.MUPhotoCollectionViewCellReuseIdentifier)
        photoCollectionView.backgroundColor = UIColor.clear
        
        return photoCollectionView
    }

}
