//
//  ZTJVison.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/21.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class ZTJVison: PBJVision {

    //MARK: 自定义的录制界面
    /// 自定义的录制界面
    ///
    /// - Parameters:
    ///   - device: 默认背面摄像头
    ///   - duration: 默然为30秒
    /// - Returns: ZTJVison
    class func ztJVison(device: PBJCameraDevice = PBJCameraDevice.back, andMaxDuration duration: Double = videoMaxDuration, andCameraMode cameraMode: PBJCameraMode = PBJCameraMode.video)->ZTJVison{
        
        let vision: ZTJVison = ZTJVison()
        vision.cameraMode = cameraMode//录制视频或者拍照
        vision.cameraOrientation = PBJCameraOrientation.portrait//竖屏录制视频
        vision.focusMode = PBJFocusMode.autoFocus//自动对焦
//        vision.captureSessionPreset = AVCaptureSessionPresetMedium//视频质量
//        vision.videoBitRate = PBJVideoBitRate640x480//视频压缩质量
        vision.outputFormat = PBJOutputFormat.preset//输出宽高比
        vision.cameraDevice = device//摄像头正面还是反面
        vision.maximumCaptureDuration = CMTime(seconds: duration, preferredTimescale: 600)
        
        do{
            let cachesPath: URL = try FileManager.default.url(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: true)//获取缓存文件夹路径
            vision.captureDirectory = cachesPath.path//设置视频存储路径
        }catch{fatalError(error.localizedDescription)}
        
        return vision
    }
}
