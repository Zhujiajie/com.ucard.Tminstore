//
//  MUView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/23.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

let backStepButtonTag: Int = 1234
let nextStepButtonTag: Int = 4321
let playButtonTag: Int = 123456

class MUView: UIView {

    /// 返回到正面按钮
    var backStepButton: UIButton?
    /// 前进到背面的按钮
    var nextStepButton: UIButton?

    /// 底部黑色线
    var blackLine: UIView?
    
    /// 播放按钮
    var playButton: UIButton?
    
    /// 点击取消按钮
    var touchBackButton: (()->())?
    
    /// 点击继续按钮
    var touchNextButton: (()->())?
    
    /// 点击播放按钮
    var touchPlayButton: (()->())?
    
    /// 标题
    var titleLabel: UILabel?
    
    /// 箭头
    var arrowIconImageView: UIImageView?
    
    /// 点击titleView的事件
    var touchTitleViewClosure: (()->())?
    
    /// 展示视频时间的Label
    var timeLable: MULabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //MARK: 设置containerView
    /**
     设置containerView
     
     - returns: MUView
     */
    class func containerView()->MUView{
        
        let containerView: MUView = MUView()
        
        containerView.backgroundColor = UIColor.white
        
        //添加阴影
        containerView.layer.shadowColor = UIColor.black.cgColor
        containerView.layer.shadowOffset = CGSize(width: 2, height: 0)
        containerView.layer.shadowOpacity = 0.26
        containerView.layer.shadowRadius = 4
        
        return containerView
    }
    
    //MARK: 灰色半透明视图
    /// 灰色半透明视图
    ///
    /// - Returns: MUView
    class func grayView()->MUView{
        let grayView: MUView = MUView()
        grayView.backgroundColor = UIColor(hexString: "D8D8D8")
        grayView.alpha = 0.82
        return grayView
    }
    
    //MARK: 导航栏上可以点击的自定义View
    /// 导航栏上可以点击的自定义View
    ///
    /// - Returns: MUView
    class func customTitleView(isVideo: Bool = false)->MUView{
        
        let titleView: MUView             = MUView(frame: CGRect(x: 0, y: 0, width: 261/baseWidth, height: 22))
        titleView.backgroundColor = UIColor.clear
        
        titleView.titleLabel                  = UILabel()
        titleView.titleLabel?.backgroundColor = UIColor.clear
        titleView.titleLabel?.textColor       = UIColor(hexString: "4D4D4D")
        titleView.titleLabel?.font            = UIFont.systemFont(ofSize: 16)
        if isVideo == true{
           titleView.titleLabel?.text            = NSLocalizedString("所有视频", comment: "")
        }else{
            titleView.titleLabel?.text            = NSLocalizedString("所有照片", comment: "")
        }
        
        titleView.addSubview(titleView.titleLabel!)
        titleView.titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.bottom.equalTo(titleView)
            make.centerX.equalTo(titleView).offset(-5)
        })
        
        titleView.arrowIconImageView = UIImageView(image: #imageLiteral(resourceName: "arrowIcon"))
        titleView.addSubview(titleView.arrowIconImageView!)
        titleView.arrowIconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(titleView)
            make.left.equalTo(titleView.titleLabel!.snp.right)
            make.size.equalTo(CGSize(width: 16, height: 9))
        })
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: titleView, action: #selector(titleView.touchTitleView(tap:)))
        tap.numberOfTapsRequired = 1
        titleView.addGestureRecognizer(tap)
        
        return titleView
    }
    
    //MARK: 设置剪切视频界面下方的半透明视图
    class func setCropView()->MUView{
        
        let cropView: MUView = MUView()
        cropView.backgroundColor = UIColor.white
        
        // 展示时间的Label
        cropView.timeLable = MULabel.timeLabel()
        cropView.addSubview(cropView.timeLable!)
        cropView.timeLable?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(cropView)
            make.left.equalTo(cropView).offset(29/baseWidth)
            make.height.equalTo(20)
        })
        
        // 播放按钮
        cropView.playButton = UIButton()
        cropView.playButton?.setImage(UIImage(named: "playButton2"), for: UIControlState.normal)
        cropView.playButton?.tag = playButtonTag
        cropView.addSubview(cropView.playButton!)
        cropView.playButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(cropView)
            make.size.equalTo(CGSize(width: 45/baseWidth, height: 41/baseWidth))
        })
        cropView.playButton?.addTarget(cropView, action: #selector(cropView.touchButton(_:)), for: UIControlEvents.touchUpInside)
        
        return cropView
    }
    
    //MARK: 按钮的点击事件
    /// 按钮的点击事件
    ///
    /// - parameter button: UIButton
    func touchButton(_ button: UIButton){
        switch button.tag {
        case backStepButtonTag:
            touchBackButton?()
        case nextStepButtonTag:
            touchNextButton?()
        default:
            touchPlayButton?()
        }
    }
    
    //MARK: 设置右按钮的标题
    /// 设置右按钮的标题
    ///
    /// - parameter string: String
    func setNextButtonTitle(string: String){
        var dic2: [String: Any]?
        if #available(iOS 8.2, *) {
            dic2 = [NSFontAttributeName: UIFont.systemFont(ofSize: 18, weight: UIFontWeightRegular), NSForegroundColorAttributeName: UIColor.init(hexString: "72D7D8")]
        } else {
            dic2 = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.init(hexString: "72D7D8")]
        }
        
        let attStr2: NSAttributedString = NSAttributedString(string: string, attributes: dic2)
        nextStepButton?.setAttributedTitle(attStr2, for: UIControlState())
    }
    
    //MARK: 点击titleView的事件
    func touchTitleView(tap: UITapGestureRecognizer){
        touchTitleViewClosure?()
    }

    //MARK: 选择icon的动画
    func rotateArrowIconAnimation(){
        if let imageView: UIImageView = arrowIconImageView{
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                imageView.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi))
            }, completion: nil)
        }
    }
    
    //MARK: 改变titleView的标题
    func changeTitleViewTitle(title: String? = nil){
        if let lable: UILabel = titleLabel{
            if let str: String = title{
                lable.text = str
            }
            UIView.animate(withDuration: 0.3, delay: 0.3, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                self.arrowIconImageView!.transform = CGAffineTransform(rotationAngle: 0.0)
            }, completion: nil)
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
