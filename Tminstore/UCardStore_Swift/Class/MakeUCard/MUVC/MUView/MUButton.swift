//
//  MUButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/19.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUButton: UIButton {

    /// 点击按钮的事件
    var touchButtonClosure: (()->())?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 点击添加图片的按钮
    /// 点击添加图片的按钮
    ///
    /// - Returns: MUButton
    class func addImageButton()->MUButton{
        let addImageButton: MUButton = MUButton()
        addImageButton.backgroundColor = UIColor.clear
        addImageButton.setImage(#imageLiteral(resourceName: "addImageButton"), for: UIControlState.normal)
        return addImageButton
    }
    
    //MARK: 点击添加AR视频的按钮
    /// 点击添加AR视频的按钮
    ///
    /// - Returns: MUButton
    class func addARVideoButton()->MUButton{
        
        let addARVideoButton: MUButton = MUButton()
        addARVideoButton.backgroundColor = UIColor.white
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "4CDFED")]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("更换AR视频", comment: ""), attributes: attDic)
        addARVideoButton.setAttributedTitle(attStr, for: UIControlState.normal)
        
        addARVideoButton.layer.borderColor = UIColor(hexString: "9BFFEB").cgColor
        addARVideoButton.layer.borderWidth = 2
        addARVideoButton.layer.masksToBounds = true
        
        return addARVideoButton
    }
    
    //MARK: 更换标题
    /// 成功获取到AR视频，改变按钮的标题
    func changeAddARVideoButtonTitle(){
        var attDic: [String: Any] = [String: Any]()
        
        if #available(iOS 8.2, *) {
            attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium), NSForegroundColorAttributeName: UIColor.white]
        } else {
            attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
        }
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("更改AR视频", comment: ""), attributes: attDic)
        self.setAttributedTitle(attStr, for: UIControlState.normal)
    }
    
    //MARK: 裁切图片的两个按钮
    /// 裁切图片的两个按钮
    ///
    /// - Returns: MUButton
    class func editButton(title: String)->MUButton{
        
        let editButton: MUButton = MUButton()
        editButton.backgroundColor = UIColor(hexString: "#FFFFFF")
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor(hexString: "727272")] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: title, attributes: attDic)
        editButton.setAttributedTitle(attStr, for: UIControlState.normal)
        
        editButton.layer.cornerRadius = 3
        editButton.layer.masksToBounds = true
        
        return editButton
    }
    
    //MARK: 录制视频的按钮
    /// 录制视频的按钮
    ///
    /// - Returns: MUButton
    class func recordButton()->MUButton{
        let recordButton: MUButton = MUButton()
        recordButton.setImage(#imageLiteral(resourceName: "readyToRecord"), for: UIControlState.normal)
        return recordButton
    }
    
    //MARK: 删除视频的按钮
    /// 删除视频的按钮
    ///
    /// - Returns: MUButton
    class func deleteVideoButton()->MUButton{
        let deleteButton: MUButton = MUButton()
        deleteButton.setImage(#imageLiteral(resourceName: "deleteRecord"), for: UIControlState.normal)
        return deleteButton
    }
    
    //MARK: 确认视频的按钮
    /// 确认视频的按钮
    ///
    /// - Returns: MUButton
    class func confirmVideoButton()->MUButton{
        let confirmViewButton: MUButton = MUButton()
        confirmViewButton.setImage(#imageLiteral(resourceName: "confirmVideo"), for: UIControlState.normal)
        confirmViewButton.alpha = 0.5
        confirmViewButton.isEnabled = false
        return confirmViewButton
    }
    
    //MARK: 进入下一步的按钮
    /// 进入下一步的按钮
    ///
    /// - Returns: MUButton
    class func nextButton()->MUButton{
        
        let button: MUButton = MUButton()
        button.backgroundColor = UIColor(hexString: "60DBE8")
        
        var attDic: [String: Any] = [String: Any]()
        
        if #available(iOS 8.2, *) {
            attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 18, weight: UIFontWeightMedium), NSForegroundColorAttributeName: UIColor.white]
        } else {
            attDic = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
        }
        let title: String = NSLocalizedString("下一步", comment: "")
        let attStr: NSAttributedString = NSAttributedString(string: title, attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        
        button.layer.cornerRadius = 3
        button.layer.masksToBounds = true
        
        return button
    }
    
    //MARK: 展示照片选择器的按钮
    /// 展示照片选择器的按钮
    ///
    /// - Returns: MUButton
    class func showPhotoButton()->MUButton{
        let button: MUButton = MUButton()
        button.setImage(UIImage(named: "showPhotoIcon"), for: UIControlState.normal)
        return button
    }
    
    //MARK: 隐藏照片选择器的按钮
    /// 隐藏照片选择器的按钮
    ///
    /// - Returns: MUButton
    class func hidePhotoButton()->MUButton{
        let button: MUButton = MUButton()
        button.setImage(UIImage(named: "hidePhotoIcon"), for: UIControlState.normal)
        return button
    }
    
    //MARK: 继续的按钮
    /// 继续的按钮
    ///
    /// - Returns: MUButton
    class func continueButton()->MUButton{
        let button: MUButton = MUButton()
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("继续", comment: ""), attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        button.backgroundColor = UIColor(hexString: "61E3F0")
        return button
    }
    
    //MARK: 点击按钮的事件
    func touchButton(button: UIButton){
        touchButtonClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
