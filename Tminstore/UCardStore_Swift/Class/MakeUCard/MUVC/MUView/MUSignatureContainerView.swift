//
//  MUSignatureContainerView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/23.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUSignatureContainerView: UIImageView {

    var signatureView: ZTSignatureView?
    
    override init(image: UIImage?) {
        
        super.init(image: image)
        
        self.clipsToBounds          = true
        self.contentMode            = UIViewContentMode.scaleToFill
        self.isUserInteractionEnabled = true
        
        signatureView = ZTSignatureView()
        self.addSubview(signatureView!)
        signatureView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(35/baseHeight)
            make.left.equalTo(self).offset(30/baseWidth)
            make.bottom.equalTo(self).offset(-28/baseHeight)
            make.right.equalTo(self).offset(-30/baseWidth)
        })
        
        self.alpha = 0//隐藏该视图
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

