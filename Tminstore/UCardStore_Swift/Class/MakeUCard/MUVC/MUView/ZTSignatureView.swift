//
//  ZTSignatureView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/24.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class ZTSignatureView: MUSignatureView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        setSignatureView()
    }
    
    fileprivate func setSignatureView(){
        setSignatureField()
        setButtons()
    }

    //MARK: 设置绘制签名区域
    fileprivate func setSignatureField() {
        self.smoothView = SmoothLineView()
        self.addSubview(self.smoothView)
        self.smoothView.snp.makeConstraints { (make: ConstraintMaker) in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 10/baseHeight, left: 10/baseWidth, bottom: 10/baseHeight, right: 10/baseWidth))
        }
    }
    
    //MARK: 设置按钮
    fileprivate func setButtons(){
        //提交按钮
        self.commitButton = UIButton()
        self.addSubview(self.commitButton)
        self.commitButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.right.equalTo(self.smoothView).offset(-14/baseWidth)
            make.bottom.equalTo(self.smoothView).offset(-14/baseHeight)
        }
        self.commitButton.setImage(#imageLiteral(resourceName: "commitSignature"), for: UIControlState.normal)
        self.commitButton.addTarget(self, action: #selector(self.touchCommit(_:)), for: UIControlEvents.touchUpInside)
        
        //清除按钮
        self.clearButton = UIButton()
        self.addSubview(self.clearButton)
        self.clearButton.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(self.smoothView).offset(21/baseHeight)
            make.right.equalTo(self.smoothView).offset(-18/baseWidth)
        }
        self.clearButton.setImage(#imageLiteral(resourceName: "clearSignature"), for: UIControlState.normal)
        self.clearButton.addTarget(self, action: #selector(touchClear(_:)), for: UIControlEvents.touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
