//
//  MUViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/19.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: ------------------- 制作明信片的界面 ----------------------
import UIKit
import SnapKit
import CoreLocation
import SDWebImage

let containerWidth: CGFloat = 364/baseWidth
let containerHeight: CGFloat = 259/baseWidth

/// 照片的建议宽度
let kCardWidth: CGFloat = 1750.0
/// 照片的建议高度
let kCardHeight: CGFloat = 1240.0

class MUViewController: BaseViewController {

    /// Core Data
    var postcardData: Postcard?
    
    fileprivate var postcard: MUFinalPostcardModel?
    
    fileprivate let viewModel: MUViewModel = MUViewModel()
    
    fileprivate var animationModel: MUPreviewAnimationModel?
    
    /// 容器视图
    fileprivate var containerView: MUView?
    /// 图片视图
    fileprivate var arImageView: MUImageView?
    /// 在arImageView上方的灰色视图
    fileprivate var grayView: MUView?
    /// 点击添加图片的按钮
    fileprivate var addImageButton: MUButton?
    /// 继续的按钮
    fileprivate var continueButton: MUButton?
    /// 更换AR视频的按钮
    fileprivate var addARVideoButton: MUButton?
    
    /// 点击打开播放界面的按钮
    fileprivate var playButton: MUPreviewButton?
    
    /// 承载播放器的视图
    fileprivate var playerContainerView: MUPreviewPlayView?
    /// 播放器
    fileprivate var player: ZTPlayer?
    
    /// 是否需要检测AR
    fileprivate var shouldCheckARImage: Bool = true
    
    var backgroundImage: UIImage?
    fileprivate var backgroundImageView: UIImageView?
    fileprivate var photoVC: MUPhotoController?
    fileprivate var photoVCNav: UINavigationController?
    fileprivate var videoVC: MUViewRecordVC?
    fileprivate var videoNav: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if postcardData != nil {
            postcard = MUFinalPostcardModel.initFromCoreData(postcardData: postcardData!)
        }
        
        setNavigationBar()
        setImageView()
        setAddARVideoButton()
        setAnimationModel()//设置动画模型
        
        if postcardData == nil{
            postcard = MUFinalPostcardModel()
            
        }else{
            loadCoreDate()// 要在UI绘制完毕之后，再加载CoreData 中的数据
        }
        
        backgroundImageView = UIImageView(image: backgroundImage)
        backgroundImageView?.contentMode = UIViewContentMode.scaleToFill
        navigationController?.view.addSubview(backgroundImageView!)
        backgroundImageView?.frame = UIScreen.main.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if photoVC == nil{
            photoVC = MUPhotoController()
            photoVC?.umengViewName = "拍照"
            photoVC?.postcardModel = postcard
            photoVCNav = UINavigationController(rootViewController: photoVC!)
            if postcard?.frontImage == nil{
                present(photoVCNav!, animated: true, completion: nil)
            }
            closures()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("预览", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置imageView
    fileprivate func setImageView(){

        containerView = MUView.containerView()
        view.addSubview(containerView!)
        containerView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view).offset(42/baseWidth)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 363/baseWidth, height: 261/baseWidth))
        }
        
        arImageView = MUImageView.previewImageView(image: postcard?.frontImage)
        containerView?.addSubview(arImageView!)
        arImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(containerView!).inset(UIEdgeInsets(top: 6/baseWidth, left: 6/baseWidth, bottom: 6/baseWidth, right: 6/baseWidth))
        })
        
        grayView = MUView.grayView()
        arImageView?.addSubview(grayView!)
        grayView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(arImageView!)
        })
        
        addImageButton = MUButton.addImageButton()
        containerView?.addSubview(addImageButton!)
        addImageButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(containerView!)
        })
    }
    
    //MARK: 设置底部添加AR视频的按钮
    fileprivate func setAddARVideoButton(){
        
        continueButton = MUButton.continueButton()
        view.addSubview(continueButton!)
        continueButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(-105/baseWidth)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 335/baseWidth, height: 51/baseWidth))
        })
        
        addARVideoButton = MUButton.addARVideoButton()
        view.addSubview(addARVideoButton!)
        addARVideoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(-35/baseHeight)
            make.centerX.equalTo(continueButton!)
            make.size.equalTo(continueButton!)
        })
    }
    
    //MARK: 设置打开播放器的按钮
    fileprivate func setPlayButton(){
        playButton = MUPreviewButton.playButton()
        view.addSubview(playButton!)
        playButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(arImageView!).offset(10/baseWidth)
            make.bottom.equalTo(arImageView!).offset(-10/baseWidth)
            make.size.equalTo(CGSize(width: 33/baseWidth, height: 33/baseWidth))
        })
        //MARK: 点击播放按钮，开始播放视频
        playButton?.touchPlayButton = {[weak self] _ in
            self?.setPlayer()
        }
    }
    
    //MARK: 设置动画模型
    fileprivate func setAnimationModel(){
        animationModel = MUPreviewAnimationModel()
        animationModel?.superView = view
    }
    
    //MARK: 设置播放器相关
    fileprivate func setPlayer(){
        
        playerContainerView = MUPreviewPlayView.playerView(videoPath: postcard!.videoPath!)
        
        view.insertSubview(playerContainerView!, aboveSubview: playButton!)
        playerContainerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
            make.size.equalTo(CGSize(width: 1, height: 1))
        })
        view.setNeedsUpdateConstraints()//约束需要更新
        view.updateConstraintsIfNeeded()//检测需要更新的约束
        view.layoutIfNeeded()//强制更新约束
        
        navigationController?.navigationBar.layer.zPosition = -1//将视频播放界面推到最前端
        
        player = playerContainerView?.player
        self.addChildViewController(player!)
        player?.didMove(toParentViewController: self)
        
        animationModel?.playerContainerView = playerContainerView
        animationModel?.playerAnimationAppear()//进入播放界面
        playerClousre()//必须在这里调用
    }

    
    //MARK: 成功获取到图片
    fileprivate func getImageSuccess(image: UIImage){
        
        arImageView?.image = image
        grayView?.removeFromSuperview()
        addImageButton?.removeFromSuperview()
        
        shouldCheckARImage = true//需要检测AR
        
        //添加手势，点击重新选择图片
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapARImageView(tap:)))
        tap.numberOfTapsRequired = 1
        arImageView?.addGestureRecognizer(tap)
        arImageView?.isUserInteractionEnabled = true
    }
    
    //MARK: 点击导航栏左按钮的事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        warmAlert()
    }
    
    //MARK: 进入新发布界面
    fileprivate func enterRepleaseView(){
        if viewModel.checkPostcard(postcard: postcard!) == true{
            let vc: MUNewPublishController = MUNewPublishController()
            vc.postcard = postcard
            vc.postcardData = postcardData
            vc.umengViewName = "发布界面"
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: 点击图片，重新选择图片
    func tapARImageView(tap: UITapGestureRecognizer){
        enterPhotoVC()
    }
    
    //MARK: 进入相片选择界面
    fileprivate func enterPhotoVC(){
        if photoVC == nil {
            photoVC = MUPhotoController()
            photoVC?.umengViewName = "拍照"
            photoVC?.postcardModel = postcard
            photoVCNav = UINavigationController(rootViewController: photoVC!)
        }
        present(photoVCNav!, animated: true, completion: nil)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 点击添加图片按钮的事件
        addImageButton?.touchButtonClosure = {[weak self] _ in
            self?.enterPhotoVC()
        }
        
        //MARK: 点击继续按钮的事件
        continueButton?.touchButtonClosure = {[weak self] _ in
            if let image: UIImage = self?.arImageView?.image{
                if self?.shouldCheckARImage == true {//需要检测AR图片
                    let resizeImage: UIImage = UIImage(image: image, scaledToWidth: 200)
                    self?.viewModel.checkARImage(image: resizeImage)
                }else{
                    self?.enterRepleaseView()
                }
            }
        }
        
        //MARK: 点击更换AR视频按钮的事件
        addARVideoButton?.touchButtonClosure = {[weak self] _ in
            self?.enterVideoRecordVC()
        }
        
        //MARK: AR图片检测成功，进入背面编辑
        viewModel.checkFrontViewSuccessClosure = {[weak self] _ in
            self?.shouldCheckARImage = false
            self?.enterRepleaseView()
        }
        
        //MARK: AR图片已经存在
        viewModel.arImageAlreadyExit = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("照片已被他人占用，请重新选择照片", comment: ""), leftButtonTitle: NSLocalizedString("好的", comment: ""))
        }
        
        //MARK: AR图片的可识别度不够
        viewModel.arImageIsBad = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("AR照片识别度太低，请重新选择照片", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("好的", comment: ""))
        }
        
        //MARK: 网络请求失败的闭包
        viewModel.networkErrorClosure = {[weak self] (error: Error?) in
            self?.handelNetWorkError(error)
        }
        
        //MARK: 需要添加相片的闭包
        viewModel.shouldAddPhoto = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("请添加照片", comment: ""))
        }
        
        //MARK: 需要添加视频
        viewModel.shouldAddVideo = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("请添加视频", comment: ""))
        }
        
        //MARK: 成功获取照片
        photoVC?.getPhotoSuccess = {[weak self] _ in
            self?.getImageSuccess(image: (self?.postcard?.frontImage)!)
            self?.shouldCheckARImage = true
        }
        
        //MARK: 从选择照片的界面，退出时
        photoVC?.dismissClosure = {[weak self] (shouldSetPlayer: Bool) in
            if self?.postcard?.frontImage == nil || self?.postcard?.videoPath == nil{
                self?.warmAlert()
            }else{
                self?.backgroundImageView?.removeFromSuperview()
                self?.backgroundImageView = nil
                if shouldSetPlayer == true{
                    self?.getVideoSuccess(videoURL: (self?.postcard?.videoPath)!)
                }
            }
        }
    }
    
    //MARK: 播放器相关闭包
    func playerClousre() {
        //MARK: 在播放界面点击退出，然后销毁播放器
        playerContainerView?.tapToDismiss = {[weak self] _ in
            self?.animationModel?.dismissPlayer(complete: {
                self?.navigationController?.navigationBar.layer.zPosition = 0//恢复导航栏的位置
                self?.playerContainerView?.firePlayer()
                self?.playButton?.isHidden = false
                self?.player = nil
            })
        }
    }
    
    //MARK: 进入添加视频的页面
    fileprivate func enterVideoRecordVC(){
        if videoVC == nil{
            videoVC = MUViewRecordVC()
            videoVC?.postcardModel = postcard
            videoNav = UINavigationController(rootViewController: videoVC!)
            videoVC?.getVideoSuccess = {[weak self] (videoPath: URL) in
                self?.getVideoSuccess(videoURL: videoPath)
                self?.dismiss(animated: true, completion: nil)
            }
        }
        present(videoNav!, animated: true, completion: nil)
    }
    
    //MARK: 成功获取视频
    fileprivate func getVideoSuccess(videoURL: URL){
        postcard!.videoPath = videoURL
        setPlayButton()//设置播放按钮
        navigationItem.rightBarButtonItem?.isEnabled = true//可以进入下一个页面
    }
    
    //MARK: 加载草稿箱的数据
    fileprivate func loadCoreDate(){
        if let image: UIImage = postcard?.frontImage{
            getImageSuccess(image: image)
        }
        if let url: URL = postcard?.videoPath{
            if FileManager.default.fileExists(atPath: url.path) == true{
                getVideoSuccess(videoURL: url)
            }
        }
    }
    
    //MARK: 询问用户是否继续
    fileprivate func warmAlert(){
        dismissSelf()
//        deleteAlert(alertTitle: NSLocalizedString("确定取消发布？", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("继续", comment: ""), rightButtonTitle: NSLocalizedString("取消", comment: ""), leftClosure: {[weak self] _ in
//            
//            if self?.postcard?.frontImage == nil{
//                self?.present((self?.photoVCNav)!, animated: true, completion: nil)
//            }else if self?.postcard?.videoPath == nil{
//                
//            }
//            
//            }, rightClosure: {[weak self] _ in
//                self?.dismissSelf(animated: false, completion: nil)
//            }, presentCompletion: nil)
    }
}
