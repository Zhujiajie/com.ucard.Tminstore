//
//  MUViewRecordVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/9.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:---------------------录制视频的控制器----------------------------

import UIKit
import SnapKit
import MobileCoreServices

let videoMaxDuration: Double = 30

class MUViewRecordVC: BaseViewController {
    
    var postcardModel: MUFinalPostcardModel?
    ///成功获得视频
    var getVideoSuccess: ((_ videPathURL: URL)->())?
    ///退出的闭包回调
    var dismissClosure: (()->())?
    /// 已录制好的视频的存储路径
    var videoPathURL: URL?
    /// 是否使用dismiss
    var useDismiss: Bool = true
    
    /// 摄像头预览试图，用户接受手势
    fileprivate var previewView: UIView?
    
    /// 播放摄像头接收到的画面的layer
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer?
    
    /// 录制视频的类
    fileprivate var vision: ZTJVison?
    /// 聚焦的动画视图
    fileprivate var focusView: PBJFocusView?
    /// 定时器，用于监视录制进度
    fileprivate var timer: Timer?
    /// 弹出展示照片的视图
    fileprivate var showPhotoButton: MUButton?
    /// 隐藏选择照片视图的按钮
    fileprivate var hidePhotoButton: MUButton?
    /// 闪光灯按钮
    fileprivate var torchBarItem: LocalARBarButtonItem?
    
    /// 录屏的按钮
    fileprivate var switchCameraBarItem: LocalARBarButtonItem?
    /// 选择照片的控制器
    fileprivate var chooseVideoVC: MUVideoController?
    /// 当前照片视图离顶部的距离
    fileprivate var currentPhotoControllerOffset: CGFloat = 160/baseWidth
    // MARK: 是否隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return true
    }
    /// 录制的按钮
    fileprivate var recordButton: VideoCommentButton?
    /// 重新开始录制的按钮
    fileprivate var deleteButton: VideoCommentButton?
    /// 确认视频的按钮
    fileprivate var confirmVideoButton: VideoCommentButton?
    /// 现实时间的lable
    fileprivate var timeLabel: MULabel?
    /// 是否要展示删除和确认视频的按钮
    fileprivate var shouldShowVideoControl: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        setVideoRecorder()
        setPreviewView()
        setButtons()
        setChooseVideoController()
        closures()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        transparentNavigationBar()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if vision != nil {
            vision?.startPreview()//开启录制界面的预览
            setUpTimer()//设置计时器
        }
        if navigationController != nil{
            showPhotoController()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hidePhotoController()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        vision?.stopPreview()
        timer?.invalidate()
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("whiteBackArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        torchBarItem = LocalARBarButtonItem.torchBarItem()
        torchBarItem?.shouldControllerTorchSelf = true
        switchCameraBarItem = LocalARBarButtonItem.switchCameraBarItem()
        navigationItem.rightBarButtonItems = [switchCameraBarItem!, torchBarItem!]
    }
    
    //MARK: 设置视频录制
    fileprivate func setVideoRecorder(){
        if vision == nil {
            showSVProgress(title: nil)//初始化控件时，不允许交互
            vision = ZTJVison.ztJVison(device: PBJCameraDevice.back, andMaxDuration: videoMaxDuration)
            vision?.delegate = self
        }
        vision?.startPreview()//开始预览
        focusView = PBJFocusView()//设置聚焦视图
    }
    
    //MARK: 设置预览界面
    fileprivate func setPreviewView(){
        
        previewView = UIView(frame: UIScreen.main.bounds)
        previewView?.backgroundColor = UIColor.black
        view.addSubview(previewView!)
        previewView?.frame.origin.y = -nav_statusHeight
        
        //点按重新聚焦
        let focusTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToFocus(_:)))
        previewView?.addGestureRecognizer(focusTap)
        
        previewLayer = vision?.previewLayer
        previewLayer?.frame = (previewView?.bounds)!
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewView?.layer.addSublayer(previewLayer!)
    }
    
    //MARK: 点击屏幕，重新聚焦
    func tapToFocus(_ tap: UITapGestureRecognizer){
        
        let tapPoint: CGPoint = tap.location(in: previewView!)
        
        focusView!.frame.origin.x = tapPoint.x - WIDTH(focusView!)/2
        focusView!.frame.origin.y = tapPoint.y - HEIGHT(focusView!)/2
        previewView?.addSubview(focusView!)
        focusView?.startAnimation()
        
        let adjustPoint: CGPoint = PBJVisionUtilities.convertToPointOfInterest(fromViewCoordinates: tapPoint, inFrame: previewView!.frame)
        
        vision?.focusExposeAndAdjustWhiteBalance(atAdjustedPoint: adjustPoint)
    }
    
    //MARK: 设置按钮
    fileprivate func setButtons(){
        
        //点击录制的按钮
        recordButton = VideoCommentButton.recordButton()
        view.addSubview(recordButton!)
        recordButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(-37/baseHeight)
            make.centerX.equalTo(view.snp.centerX)
            make.size.equalTo(CGSize(width: 83/baseWidth, height: 83/baseWidth))
        })
        
        //展示选择视频的按钮
        showPhotoButton = MUButton.showPhotoButton()
        view.addSubview(showPhotoButton!)
        showPhotoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(recordButton!)
            make.left.equalTo(recordButton!.snp.right).offset(50/baseWidth)
            make.size.equalTo(CGSize(width: 25.6/baseWidth, height: 45.5/baseWidth))
        })
        
        //删除按钮
        deleteButton = VideoCommentButton.deleteVideoButton()
        view.insertSubview(deleteButton!, belowSubview: recordButton!)
        deleteButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(recordButton!)
            make.centerX.equalTo(view).offset(0)
            make.size.equalTo(CGSize(width: 54/baseWidth, height: 54/baseWidth))
        })
        
        // 确认视频的按钮
        confirmVideoButton = VideoCommentButton.confirmVideoButton()
        view.insertSubview(confirmVideoButton!, belowSubview: recordButton!)
        confirmVideoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(deleteButton!)
            make.centerX.equalTo(view).offset(0)
            make.size.equalTo(deleteButton!)
        })
        confirmVideoButton?.alpha = 0.5
        confirmVideoButton?.isEnabled = false
        
        timeLabel = MULabel.timeLabel()
        timeLabel?.textColor = UIColor.white
        view.addSubview(timeLabel!)
        timeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(recordButton!.snp.top).offset(-11/baseHeight)
            make.centerX.equalTo(view)
            make.height.equalTo(20)
        })
        
    }
    
    //MARK: 设置选择照片的控制器
    fileprivate func setChooseVideoController(){
        chooseVideoVC = MUVideoController()
        navigationController?.view.addSubview(chooseVideoVC!.view)
        chooseVideoVC?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(navigationController!.view).offset(currentPhotoControllerOffset)
            make.left.right.equalTo(navigationController!.view)
            make.height.equalTo(screenHeight)
        })
        
        let pan: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panToMovePhotoController(pan:)))
        chooseVideoVC?.titleView?.addGestureRecognizer(pan)
        
        hidePhotoButton = MUButton.hidePhotoButton()
        navigationController?.view.addSubview(hidePhotoButton!)
        hidePhotoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(chooseVideoVC!.view)
            make.bottom.equalTo(chooseVideoVC!.view.snp.top).offset(-25/baseHeight)
            make.size.equalTo(CGSize(width: 25.6/baseWidth, height: 45.5/baseWidth))
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK:进入后台之后，停止预览
        app.appDidEnterBackground = {[weak self] _ in
            self?.vision?.stopPreview()
        }
        //MARK:进入前台之后。开始预览
        app.appDidBecomeActive = {[weak self] _ in
            guard let vision: ZTJVison = self?.vision else { return }
            vision.startPreview()
        }
        
        //MARK: 点击导航栏右按钮，切换摄像头正反面
        switchCameraBarItem?.touchMoreButtonClosure = {[weak self] _ in
            self?.touchRotateButton()
        }
        
        //MARK: 点击闪光灯按钮，打开或者关闭闪光灯
        torchBarItem?.touchMoreButtonClosure = {[weak self] _ in
            if self?.vision?.isFlashAvailable == true{
                if self?.vision?.flashMode == PBJFlashMode.off{
                    self?.vision?.flashMode = PBJFlashMode.on
                    self?.torchBarItem?.image = UIImage(named: "localTorchOn")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                }else if self?.vision?.flashMode == PBJFlashMode.on{
                    self?.vision?.flashMode = PBJFlashMode.off
                    self?.torchBarItem?.image = UIImage(named: "localTorchOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                }
            }
        }
        
        //MARK: 点击隐藏照片选择器按钮的事件
        hidePhotoButton?.touchButtonClosure = {[weak self] _ in
            self?.hidePhotoController()
        }
        
        //MARK: 点击展示照片选择器按钮的事件
        showPhotoButton?.touchButtonClosure = {[weak self] _ in
            self?.showPhotoController()
        }
        
        //MARK: 成功获得视频
        chooseVideoVC?.getVideoClosure = {[weak self] (asset: AVAsset) in
            self?.enterViewCropVC(asset: asset)
        }
        
        //MARK: 选择照片的视图，滚动到底部了
        chooseVideoVC?.scrollToBottom = {[weak self] _ in
            self?.fullScreenToShowPhotoController()
        }
        
        //MARK: 点击删除视频的按钮
        deleteButton?.touchButtonClosure = {[weak self] _ in
            self?.touchDeleteButton()
        }
        
        //MARK: 点击确认开始录制视频按钮的事件
        recordButton?.touchButtonClosure = {[weak self] _ in
            self?.touchRecordButton()
        }
        
        //MARK: 点击确认视频按钮的事件
        confirmVideoButton?.touchButtonClosure = {[weak self] _ in
            self?.vision?.endVideoCapture()//完成视频录制
        }
    }
    
    //MARK: 进入视频裁切界面
    fileprivate func enterViewCropVC(asset: AVAsset){
        let cropView: MUCropVideoVC = MUCropVideoVC()
        cropView.asset = asset
        navigationController?.pushViewController(cropView, animated: true)
        //MARK: 裁切视频成功
        cropView.getVideoSuccess = {[weak self] (videoURL: URL) in
            self?.videoPathURL = videoURL
            self?.backToPreview()
        }
    }
    
    //MARK: 点击切换摄像头的按钮，切换前后摄像头
    func touchRotateButton()  {
        guard let vision: ZTJVison = self.vision else { return}
        if vision.cameraDevice == PBJCameraDevice.back {
            vision.cameraDevice = PBJCameraDevice.front
        }else{
            vision.cameraDevice = PBJCameraDevice.back
        }
    }
    
    //MARK: 点击删除按钮，重新开始录制
    func touchDeleteButton(){
        if vision?.isRecording == true{
            vision?.cancelVideoCapture()//取消录制
            setUpTimer()
            recordButton?.setImage(UIImage(named: "readyToRecord"), for: UIControlState.normal)
            if videoPathURL != nil {//将视频删除掉
                do {
                    try FileManager.default.removeItem(at: videoPathURL!)
                }catch {fatalError(error.localizedDescription)}
            }
            videoPathURL = nil
        }
        //隐藏视频控制按钮，出现视频选择的imageView
        if shouldShowVideoControl == true{
            deleteButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.centerX.equalTo(view).offset(0)
            })
            confirmVideoButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.centerX.equalTo(view).offset(0)
            })
            autoLayoutAnimation(view: view, withDelay: 0.0)
            shouldShowVideoControl = !shouldShowVideoControl
            showPhotoButton?.isHidden = false
        }
    }
    
    //MARK: 点击开始或者停止录制视频
    func touchRecordButton() {
        
        if vision?.isRecording == false {
            vision?.startVideoCapture()
            recordButton?.setImage(UIImage(named: "ARRecording"), for: UIControlState.normal)
        }else{
            if vision?.isPaused == false{
                vision?.pauseVideoCapture()
                recordButton?.setImage(UIImage(named: "ARreadyToRecord"), for: UIControlState.normal)
            }else{
                vision?.resumeVideoCapture()
                recordButton?.setImage(UIImage(named: "ARRecording"), for: UIControlState.normal)
            }
        }
        //弹出视频控制按钮，隐藏视频选择的imageView
        if shouldShowVideoControl == false{
            deleteButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.centerX.equalTo(view).offset(-127/baseWidth)
            })
            confirmVideoButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                make.centerX.equalTo(view).offset(127/baseWidth)
            })
            autoLayoutAnimation(view: view, withDelay: 0.0)
            shouldShowVideoControl = !shouldShowVideoControl
            showPhotoButton?.isHidden = true
        }
    }
    
    //MARK: 点击导航栏左按钮
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        deleteAlert(alertTitle: NSLocalizedString("确定退出?\n未保存的视频会被删除", comment: ""), leftButtonTitle: NSLocalizedString("继续", comment: ""), rightButtonTitle: NSLocalizedString("退出", comment: ""), rightClosure: { [weak self] _ in
            if self?.useDismiss == false{
                _ = self?.navigationController?.popViewController(animated: true)
            }else{
                self?.dismissSelf()
            }
        })
    }
    
    //MARK: 全屏展示选择照片的视图
    fileprivate func fullScreenToShowPhotoController(){
        chooseVideoVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(navigationController!.view).offset(0)
        })
        autoLayoutAnimation(view: navigationController!.view)
        currentPhotoControllerOffset = 0
    }
    
    //MARK: 隐藏选择照片的视图
    fileprivate func hidePhotoController(){
        chooseVideoVC?.cancelRequestPhoto()//先取消获取相片
        chooseVideoVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(navigationController!.view).offset(screenHeight + 80)
        })
        autoLayoutAnimation(view: navigationController!.view)
        currentPhotoControllerOffset = 160/baseWidth
    }
    
    //MARK: 展示选择照片的控制器
    fileprivate func showPhotoController(){
        chooseVideoVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(navigationController!.view).offset(160/baseWidth)
        })
        autoLayoutAnimation(view: navigationController!.view)
        currentPhotoControllerOffset = 160/baseWidth
    }
    
    //MARK: 滑动，移动选择照片的控制器
    func panToMovePhotoController(pan: UIPanGestureRecognizer){
        if let view: UIView = pan.view{
            let movePoint: CGPoint = pan.translation(in: view)
            switch pan.state {
            case UIGestureRecognizerState.began, UIGestureRecognizerState.changed:
                chooseVideoVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
                    make.top.equalTo(navigationController!.view).offset(currentPhotoControllerOffset + movePoint.y)
                })
            default:
                if movePoint.y < 0{
                    fullScreenToShowPhotoController()
                }else if movePoint.y > screenHeight/2 - 160/baseHeight{
                    hidePhotoController()
                }else{
                    showPhotoController()
                }
                pan.setTranslation(CGPoint.zero, in: view)
            }
        }
    }
    
    //MARK: 设置计时器，监控录制进度
    fileprivate func setUpTimer(){
        timer = Timer(timeInterval: 0.1, target: self, selector: #selector(updateRecordTime(_:)), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    //MARK: 通过计时器获得录制进度
    func updateRecordTime(_ timer: Timer){
        
        var seconds: Double = vision!.capturedVideoSeconds
        switch seconds {
        case -10..<0:
            seconds = 0
            if confirmVideoButton?.isEnabled == true{
                confirmVideoButton?.isEnabled = false
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                    self.confirmVideoButton?.alpha = 0.5
                }, completion: nil)//录制时间不够
            }
        case 5..<videoMaxDuration:
            if confirmVideoButton?.isEnabled == false{
                confirmVideoButton?.isEnabled = true
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                    self.confirmVideoButton?.alpha = 1
                }, completion: nil)//录制时间足够
            }
        case videoMaxDuration..<100:
            seconds = videoMaxDuration
        default:
            break
        }
        timeLabel?.text = String(format: "00:%02.0f", seconds)
    }
    
    //MARK: 成功获得视频
    fileprivate func backToPreview(){
        postcardModel?.videoPath = videoPathURL
        getVideoSuccess?(videoPathURL!)//成功获取视频，返回
        if let _: MUPhotoController = navigationController?.viewControllers.first as? MUPhotoController{
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
}
extension MUViewRecordVC: PBJVisionDelegate{
    
    //MARK: 对焦和曝光的代理
    func visionDidStopFocus(_ vision: PBJVision) {
        if focusView != nil {
            focusView?.stopAnimation()
        }
    }
    
    func visionDidChangeExposure(_ vision: PBJVision) {
        if focusView != nil {
            DispatchQueue.main.async {[weak self] _ in
                self?.focusView?.stopAnimation()
            }
        }
    }
    
    //MARK: 可以录制之后，再绘制录制按钮
    func visionSessionDidStart(_ vision: PBJVision) {
        dismissSVProgress()
    }
    
    //MARK: 拍摄完成
    func vision(_ vision: PBJVision, capturedVideo videoDict: [AnyHashable : Any]?, error: Error?) {
        
        timer?.invalidate()
        
        if error != nil  {return}
        
        if let videoPathStr: String = videoDict![PBJVisionVideoPathKey] as? String {
            
            let filePath: String = "file://" + videoPathStr//编辑存储地址
            videoPathURL = URL(string: filePath)
            
//            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(videoPathStr) == true {
//                UISaveVideoAtPathToSavedPhotosAlbum(videoPathStr, nil, nil, nil)
//            }
            
            if videoPathURL != nil{
                backToPreview()
            }
        }
    }
}
