//
//  MUBackVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/9/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: -----------编辑明信片背面的控制器-----------------

import UIKit
import SnapKit

class MUBackVC: BaseViewController {
    
    /// Core Data
    var postcardData: Postcard?
    
    var postcard: MUFinalPostcardModel?
    
    fileprivate let viewModel: MUViewModel = MUViewModel()
    
    /// 明信片正方面的容器视图
    fileprivate var containerView: MUView?
    
    /// 明信片背面编辑视图
    fileprivate var backEditView: MUBackEditView?
    
    /// 将signatureView添加到这个容器视图上
    fileprivate var signatureContainerView: MUSignatureContainerView?
    
    /// 用于编辑签名的视图
    fileprivate var signatureView: ZTSignatureView?
    
    /// 进入到预览界面的按钮
    fileprivate var nextButton: MUButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        setContainerView()
        setBackEditView()
        setSignatureView()
        setNextButton()
        closure()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getTextAndFont()//获取已经填写的文字和字体
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("背面", comment: ""), attributeDic: attDic)
    }
    
    //MARK:设置containerView
    fileprivate func setContainerView(){
        containerView = MUView.containerView()
        view.addSubview(containerView!)
        containerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(43/baseHeight)
            make.size.equalTo(CGSize(width: 352/baseWidth, height: 249/baseWidth))
            make.centerX.equalTo(view)
        })
    }
    
    //MARK: 设置明信片反面编辑界面
    fileprivate func setBackEditView(){
        backEditView = MUBackEditView()
        containerView?.addSubview(backEditView!)
        backEditView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(containerView!)
        })
        backEditView?.templateIndex = 0
        backEditView?.requestLocationInfo(location: postcard?.photoLocation)
    }
    
    //MARK: 设置signatureView，编辑签名的视图
    fileprivate func setSignatureView(){
        signatureContainerView = MUSignatureContainerView(image: UIImage(named: "signatureContainer"))
        view.addSubview(signatureContainerView!)
        signatureView = signatureContainerView?.signatureView
        signatureContainerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
    }
    
    //MARK: 设置进入预览界面的按钮
    fileprivate func setNextButton(){
        nextButton = MUButton.nextButton()
        view.addSubview(nextButton!)
        nextButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-70/baseHeight)
            make.size.equalTo(CGSize(width: 335/baseWidth, height: 51/baseWidth))
        })
    }
    //MARK: 闭包回调
    fileprivate func closure(){
        
        //MARK: 点击签名进入编辑签名的视图
        backEditView?.touchSignatureViewClosure = {[weak self] _ in
            self?.signatureView?.getSignaturePath()//取出已保存的签名
            self?.view.bringSubview(toFront: (self?.signatureContainerView)!)//将该视图移到最前
            UIView.animate(withDuration: 0.5, animations: {
                self?.signatureContainerView?.alpha = 1
            })
        }
        
        //MARK: 成功获得签名
        signatureView?.commitBlock = {[weak self] _ in
            UIView.animate(withDuration: 0.5, animations: {[weak self] _ in
                self?.signatureContainerView?.alpha = 0
                })//隐藏视图
            //判断一下。返回的签名是否为空
            if self?.signatureView?.empty == true{
                self?.backEditView?.signatureImage = nil
            }else{
                if let image: UIImage = self?.signatureView?.getSignatureImage() {
                    self?.backEditView?.signatureImage = UIImage(cgImage: image.cgImage!, scale: image.scale, orientation: .left)
                }
            }
        }
        
        //MARK: 背面结束编辑获取背面文字
        backEditView?.textViewEndEditingClosure = {[weak self] (textView: ZTTextView) in
            doInBackground(inBackground: {
                self?.postcard?.message = textView.text
                self?.postcard?.messageFontName = textView.fontName
                self?.postcard?.messageFontSize = textView.textFontSize
                }, backToMainQueue: nil)
        }
        
        //MARK: 点击添加祝福语的按钮，开始编辑背面文字
        nextButton?.touchButtonClosure = {[weak self] _ in
            self?.viewModel.checkBackView(postcard: (self?.postcard)!)
        }
        
        //MARK: 用户还未添加文字
        viewModel.shouldAddText = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("是否放弃在卡片上写祝福语？", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("是", comment: ""), rightButtonTitle: NSLocalizedString("否", comment: ""), leftClosure: {
                self?.enterPreviewVC()
                })
        }
        
        //MARK: 检查成功，进入预览界面
        viewModel.backCheckSuccess = {[weak self] _ in
            self?.enterPreviewVC()
        }
    }
    
    //MARK: 进入发布界面
    fileprivate func enterPreviewVC(){
        postcard?.backImage         = backEditView?.getBackImage()
        postcard?.photoLocationText = backEditView?.locationText
        postcard?.countryInEn       = backEditView!.countryInEn
        postcard?.countryInCn       = backEditView!.countryInCn
        postcard?.stateInCN         = backEditView!.stateInCN
        postcard?.stateInEn         = backEditView!.stateInEn
        postcard?.cityInCN          = backEditView!.cityInCN
        postcard?.cityInEN          = backEditView!.cityInEn
        
        if postcard?.isComeFromPurchase == false{
            let publishVC: MUPublishController = MUPublishController()
            publishVC.umengViewName = "发布"
            publishVC.postcard = postcard
            publishVC.postcardData = postcardData
            _ = navigationController?.pushViewController(publishVC, animated: true)
        }else{
            MobClick.event("buy_chooseAddress")//友盟点击事件
            let addressListVC: MUAddressListController = MUAddressListController()
            addressListVC.umengViewName = "地址列表"
            addressListVC.postcard = postcard
            addressListVC.postcardData = postcardData
            addressListVC.isComeFromPurchase = true
            _ = navigationController?.pushViewController(addressListVC, animated: true)
        }
    }

    //MARK: 填入文字
    fileprivate func getTextAndFont(){
        if postcard?.message != "" && backEditView?.shouldPlaceHolderShow == true{
            backEditView?.greetingTextView?.text = postcard?.message
            backEditView?.greetingTextView?.fontName = postcard?.messageFontName
        }
        if backEditView?.greetingTextView?.text != "" {
            backEditView?.resizeTextViewAndFont()
        }
    }
}
