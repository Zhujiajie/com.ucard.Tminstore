//
//  MUPhotoVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/9/27.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK: -----------选择照片的控制器-----------

import UIKit
import Photos
import PhotosUI
import SnapKit

class MUPhotoVC: BaseViewController {
    
    /// 相册类型
    fileprivate enum Section: Int {
        case allPhotos = 0
        case smartAlbums = 1
        case userCollections = 2
        
        static let count = 3
    }
    
    /// 成功获得相片的闭包
    var getImageClosure: ((_ image: UIImage, _ location: CLLocation?)->())?
    
    /// scrollView滚动到底部了
    var scrollToBottom: (()->())?
    
    /// 导航栏的自定义View
    var titleView: MUView?
    
    /// 展示图片的collectionView
    fileprivate var photoCollectionView: MUCollectionView?
    
    /// 用于获取相片缩略图
    fileprivate var imageManager: PHCachingImageManager?
    
    /// 从相册拉取的资源
    fileprivate var assetsFetchResults: PHFetchResult<PHAsset>?
    
    /// 所有相册
    fileprivate var allPhotos: PHFetchResult<PHAsset>?
    /// 智能相册
    fileprivate var smartAlbums: PHFetchResult<PHAssetCollection>?
    /// 用户相册
    fileprivate var userCollections: PHFetchResult<PHCollection>?
    
    /// 获取相片的参数
    fileprivate var fetchOptions: PHFetchOptions?
    
    /// 图片请求的requestID
    fileprivate var imageRequestID: Int32?
    
    /// 从PHCachingImageManager处获得的缩略图大小
    fileprivate var assetGridThumbnailSize: CGSize = CGSize.zero
    
    /// 被选中的cell的IndexPath
    fileprivate var selectedCellIndexPath: IndexPath?
    
    ///展示相册的高斯模糊视图
    fileprivate var albumVisualView: MUVisualEffectView?
    
    /// 是否需要展示相册视图
    fileprivate var shouldShowAlbumVisualView: Bool = true
    /// 是否停止获取相片
    fileprivate var isStopGetPhoto: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.white
        view.layer.masksToBounds = true
        
        setTitleView()
        getPhoto()//从系统相册获取图片资源
        setPhotoCollectionView()//设置collectionView
        setAlbumVisualView()
        closures()
        autoLayoutAnimation(view: view, withDelay: 0.0)
    }
    
    //MARK: 离开页面时，停止获取图片
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cancelRequestPhoto()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dismissSVProgress()
    }
    
    //MARK: 设置切换照片来源的视图
    fileprivate func setTitleView(){
        titleView = MUView.customTitleView()
        view.addSubview(titleView!)
        titleView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.height.equalTo(60/baseWidth)
        })
    }
    
    //MARK:设置collectionView
    fileprivate func setPhotoCollectionView(){
        photoCollectionView = MUCollectionView.photoCollectionView()
        view.addSubview(photoCollectionView!)
        photoCollectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.top.equalTo(titleView!.snp.bottom)
        })
        photoCollectionView?.delegate = self
        photoCollectionView?.dataSource = self
    }
    
    //MARK: 设置展示相册的高斯模糊视图
    fileprivate func setAlbumVisualView(){
        albumVisualView = MUVisualEffectView.albumVisualView()
        view.addSubview(albumVisualView!)
        albumVisualView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.equalTo(view)
            make.size.equalTo(photoCollectionView!)
            make.bottom.equalTo(view).offset(-HEIGHT(view))
        })
        
        albumVisualView?.albumTableView?.delegate = self
        albumVisualView?.albumTableView?.dataSource = self
    }
    
    //MARK: 从系统相册获取图片资源
    /**
     从系统相册获取图片资源
     */
    fileprivate func getPhoto() {
        
        imageManager = PHCachingImageManager()
        
        let scale: CGFloat = UIScreen.main.scale
        assetGridThumbnailSize = CGSize(width: photoCollectionViewCellSize.width * scale, height: photoCollectionViewCellSize.height * scale)
        
        fetchOptions = PHFetchOptions()
        fetchOptions?.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions?.includeHiddenAssets = true//包含隐藏的照片
        
        allPhotos = PHAsset.fetchAssets(with: fetchOptions)//所有相册
        smartAlbums = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.smartAlbum, subtype: PHAssetCollectionSubtype.albumRegular, options: nil)//智能相册
        userCollections = PHCollectionList.fetchTopLevelUserCollections(with: nil)//用户相册
        
        assetsFetchResults = allPhotos
        
        PHPhotoLibrary.shared().register(self)
    }
    
    //MARK: 如果已经有选中的cell，则滚动到该cell
    /**
     如果已经有选中的cell，则滚动到该cell
     */
    func animateCollectionViewToSelectedCell(indexPath: IndexPath? = nil){
        if let indexPath: IndexPath = indexPath{
            photoCollectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
        }else if let indexPath: IndexPath = selectedCellIndexPath {
            photoCollectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
        }
    }
    
    //MARK: 取消获取相片
    /// 取消获取相片
    func cancelRequestPhoto(){
        if imageManager != nil && imageRequestID != nil{
            imageManager!.cancelImageRequest(imageRequestID!)
            dismissSVProgress()
            isStopGetPhoto = true
        }
    }
    
    //MARK: 获取相册照片
    /// 获取相册照片
    ///
    /// - parameter indexPath: NSIndexPath
    func getPhotoFromLibrary(indexPath: IndexPath){
        //取消图片获取
        if imageRequestID != nil {
            imageManager?.cancelImageRequest(imageRequestID!)
        }
        
        isStopGetPhoto = false
        
        SVProgressHUD.setDefaultMaskType(.none)
        SVProgressHUD.setDefaultStyle(.light)
        
        let options: PHImageRequestOptions = PHImageRequestOptions()
        options.deliveryMode = PHImageRequestOptionsDeliveryMode.opportunistic//获取原图
        options.resizeMode = PHImageRequestOptionsResizeMode.none
        options.isSynchronous = false//异步获取
        options.isNetworkAccessAllowed = true
        options.progressHandler = {[weak self] (resultTuple: (Double, Error?, UnsafeMutablePointer<ObjCBool>, [AnyHashable: Any]?)) in
            if self?.isStopGetPhoto == false{
                SVProgressHUD.showProgress(Float(resultTuple.0))
            }
            if resultTuple.0 == 1.0{
                self?.dismissSVProgress()
            }
        }
        
        let asset: PHAsset = assetsFetchResults![indexPath.item]
        
        //防止加载全景图
        var targetSize: CGSize = CGSize.zero
        
        if asset.pixelWidth > 6000 || asset.pixelHeight > 6000 {
            targetSize = CGSize(width: kCardWidth, height: kCardHeight)
        }else{
            targetSize = PHImageManagerMaximumSize
        }
        
        //请求图片，并保存requestID
        imageRequestID = imageManager?.requestImage(for: asset, targetSize: targetSize, contentMode: PHImageContentMode.default, options: options, resultHandler: { [weak self] (result: UIImage?, info: [AnyHashable: Any]?) in
            if result == nil {
                return
            }
            if (info![PHImageCancelledKey] as? String) != nil || (info![PHImageErrorKey] as? String) != nil{
                self?.dismissSVProgress()
            }//请求取消
            
            if info![PHImageResultIsDegradedKey] as? Bool == false {//请求完成
                self?.getImageSuccess(image: result, withLocation: asset.location)
                self?.selectedCellIndexPath = indexPath//保存被选中的cell的IndexPath
            }
            })
        
        //取消cell的选中状态
        let cells: [MUPhotoCollectionViewCell]? = photoCollectionView?.visibleCells as? [MUPhotoCollectionViewCell]
        _ = cells?.map({ (cell: MUPhotoCollectionViewCell) in
            cell.isCellSelected = false
        })
        
        if let cell: MUPhotoCollectionViewCell = photoCollectionView?.cellForItem(at: IndexPath(row: indexPath.row, section: 0)) as? MUPhotoCollectionViewCell{
            cell.isCellSelected = true//给选中的cell，添加边框
        }
    }
    
    //MARK: 各种回调
    fileprivate func closures(){
        //MARK: 点击titleView的事件，切换相册
        titleView?.touchTitleViewClosure = {[weak self] _ in
            self?.albumVisualAnimation()
        }
    }
    
    //MARK: 展示相册的动画
    fileprivate func albumVisualAnimation(title: String? = nil){
        albumVisualView?.snp.updateConstraints({ (make: ConstraintMaker) in
            if shouldShowAlbumVisualView == true{
                make.bottom.equalTo(view).offset(0)
            }else{
                make.bottom.equalTo(view).offset(-HEIGHT(view))
            }
        })
        if shouldShowAlbumVisualView == true{
            titleView?.rotateArrowIconAnimation()
        }else{
            titleView?.changeTitleViewTitle(title: title)
        }
        shouldShowAlbumVisualView = !shouldShowAlbumVisualView
        autoLayoutAnimation(view: view, withDelay: 0.0)
    }
    
    //MARK: 成功获取到图片
    fileprivate func getImageSuccess(image: UIImage?, withLocation location: CLLocation? = nil){
        if image != nil{
            if location == nil{
                getImageClosure?(image!, currentUserLocation)
            }else{
                getImageClosure?(image!, location)
            }
        }
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
}

//MARK: --------- UICollectionViewDelegate, UICollectionViewDataSource ------------
extension MUPhotoVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    //MARK: 点击cell获得照片
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        getPhotoFromLibrary(indexPath: IndexPath(item: indexPath.item, section: indexPath.section))
        animateCollectionViewToSelectedCell(indexPath: IndexPath(item: indexPath.item, section: indexPath.section))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        guard let count: Int = assetsFetchResults?.count, count > 0 else { return 0 }
        return count
    }
    
    //MARK: 获取缩略图，设置cell的图片
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell: MUPhotoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MUPhotoCollectionViewCellReuseIdentifier, for: indexPath) as! MUPhotoCollectionViewCell
        
        //给cell添加边框，或者取消边框
        if selectedCellIndexPath != nil && indexPath == selectedCellIndexPath {
            cell.isCellSelected = true
        }else{
            cell.isCellSelected = false
        }
        
        let asset: PHAsset = assetsFetchResults![indexPath.item]
        
        cell.representedAssetIdentifier = asset.localIdentifier
        
        //livePhoto
        if #available(iOS 9.1, *) {
            if (asset.mediaSubtypes == PHAssetMediaSubtype.photoLive) {
                let badge: UIImage = PHLivePhotoView.livePhotoBadgeImage(options: .overContent)
                cell.image = badge
            }
        }
        
        //异步请求图片
        imageManager?.requestImage(for: asset, targetSize: assetGridThumbnailSize, contentMode: PHImageContentMode.aspectFill, options: nil, resultHandler: { (result: UIImage?, _) in
            if cell.representedAssetIdentifier == asset.localIdentifier {
                cell.image = result
            }
        })
        
        return cell
    }
    
    //MARK: 监测scrollView滚动到底部了
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.height
        if bottomEdge >= scrollView.contentSize.height{
            scrollToBottom?()
        }
    }
}

//MARK:---------PHPhotoLibraryChangeObserver---------
extension MUPhotoVC: PHPhotoLibraryChangeObserver{
    //MARK: 监听相册是否被更改
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        DispatchQueue.main.sync {
            //检查已展示的asset是否已被更改
            if let result: PHFetchResult<PHAsset> = assetsFetchResults {
                if let collectionChanges: PHFetchResultChangeDetails<PHAsset> = changeInstance.changeDetails(for: result) {
                    assetsFetchResults = collectionChanges.fetchResultAfterChanges
                    photoCollectionView?.reloadData()
                }
            }
        }
    }
}

extension MUPhotoVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section(rawValue: section)! {
        case Section.allPhotos: return 0
        case Section.smartAlbums:
            if let album: PHFetchResult<PHAssetCollection> = smartAlbums{
                return album.count
            }else{
                return 0
            }
        case Section.userCollections:
            if let album: PHFetchResult<PHCollection> = userCollections{
                return album.count
            }else{
                return 0
            }
        }
    }
    
    //MARK: 设置tableViewCell的内容
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch Section(rawValue: indexPath.section)! {
        case Section.allPhotos:
        
            let cell: MUAlbumTableViewCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.allPhotos.rawValue, for: indexPath) as! MUAlbumTableViewCell
            cell.asset = allPhotos!.object(at: indexPath.row)
            cell.title = NSLocalizedString("所有相片", comment: "")
            cell.numberOfPhotos = allPhotos!.count
            
            return cell
            
        case Section.smartAlbums:
            
            let cell: MUAlbumTableViewCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.smartAlbums.rawValue, for: indexPath) as! MUAlbumTableViewCell
            let collection: PHAssetCollection = smartAlbums!.object(at: indexPath.row)
            let fetchResult: PHFetchResult<PHAsset> = PHAsset.fetchAssets(in: collection, options: fetchOptions!)
            cell.asset = fetchResult.firstObject
            cell.title = collection.localizedTitle!
            cell.numberOfPhotos = fetchResult.count
            
            return cell
            
        case Section.userCollections:
            
            let cell: MUAlbumTableViewCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.userCollections.rawValue, for: indexPath) as! MUAlbumTableViewCell
            let collection: PHAssetCollection = userCollections!.object(at: indexPath.row) as! PHAssetCollection
            let fetchResult: PHFetchResult<PHAsset> = PHAsset.fetchAssets(in: collection, options: fetchOptions!)
            cell.asset = fetchResult.firstObject
            cell.title = collection.localizedTitle!
            cell.numberOfPhotos = fetchResult.count
            
            return cell
        }
     }
    
    //MARK: 点击album Cell改变标题，并重新刷新collectionView
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch Section(rawValue: indexPath.section)! {
        case Section.allPhotos:
            assetsFetchResults = allPhotos
        case Section.smartAlbums:
            let collection: PHAssetCollection = smartAlbums!.object(at: indexPath.row)
            assetsFetchResults = PHAsset.fetchAssets(in: collection, options: fetchOptions!)
        case Section.userCollections:
            let collection: PHAssetCollection = userCollections!.object(at: indexPath.row) as! PHAssetCollection
            assetsFetchResults = PHAsset.fetchAssets(in: collection, options: fetchOptions!)
        }
        let cell: MUAlbumTableViewCell = tableView.cellForRow(at: indexPath) as! MUAlbumTableViewCell
        albumVisualAnimation(title: cell.title)//改变标题，并进行动画
        photoCollectionView?.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
