//
//  MUCropVideoVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/10/8.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import MobileCoreServices

class MUCropVideoVC: BaseViewController {
    
    var getVideoSuccess: ((_ videPathURL: URL)->())?
    
    /// 重新录制视频
    var reRecordClosure: (()->())?
    
    /// 是否是从地图界面过来的
    var shouldUseDismiss: Bool = false
    
    /// 是否是从地图发布界面过来的
    var isComeFromMapView: Bool = false
    
    /// 原视频在本地的地址
    var videoURL: URL?
    /// 原视频的AVAsset
    var asset: AVAsset?
    
    /// 控制播放和确认裁剪的视图
    fileprivate var cropView: MUView?
    
    /// 播放器是否在播放
    fileprivate var isPlaying: Bool = false
    
    fileprivate var player: AVPlayer?
    
    fileprivate var playerItem: AVPlayerItem?
    
    fileprivate var playerLayer: AVPlayerLayer?
    
    /// 控制视频播放进度的计时器
    fileprivate var playerBackTimer: Timer?
    
    fileprivate var videoPlayerBackPositon: CGFloat = 0
    
    /// 裁剪视频的视图
    fileprivate var trimmerView: ICGVideoTrimmerView?
    
    fileprivate var videoLayer: UIView?
    
    /// 视频输出地址
    fileprivate var videoPathString: String = ""
    
    fileprivate var exportSession: AVAssetExportSession?
    
    fileprivate var startTime: CGFloat = 0
    
    fileprivate var stopTime: CGFloat = 0
    
    /// 提示label
    fileprivate var introLabel: MULabel?
    
    fileprivate var timeString: String?{
        didSet{
            cropView?.timeLable?.text = timeString!
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do{
            let cachesURL: URL = try FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)//获取缓存文件夹路径
            let videoFolder: String = cachesURL.path + "/videoCache"//获取视频存储的文件夹路径
            if FileManager.default.fileExists(atPath: videoFolder) == false {
                try FileManager.default.createDirectory(atPath: videoFolder, withIntermediateDirectories: false, attributes: nil)
            }//如果文件夹不存在，则创建一个
            videoPathString = videoFolder + "/\(UUID().uuidString).mp4"//设置视频存放的路径
        }catch{fatalError(error.localizedDescription)}
        
        view.backgroundColor = UIColor(hexString: "#F9F9F9")
        
        setNavigationBar()
        setCropView()
        creatPlayer()
        setTrimmerView()
        setPlayBackTimer()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    //MARK: 设置伪导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        setNavigationBarRightButtonWithImageName("confirmButton")
        navigationController?.navigationBar.tintColor = UIColor(hexString: "4CDFED")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("编辑视频", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 创建播放器
    fileprivate func creatPlayer(){
        
        let videoLayerFrame: CGRect = CGRect(x: 0, y: 32/baseWidth, width: screenWidth, height: 265/baseWidth)
        videoLayer = UIView(frame: videoLayerFrame)
        videoLayer?.backgroundColor = UIColor.black
        view.addSubview(videoLayer!)
        
        if asset == nil{
            asset = AVAsset(url: videoURL!)
        }
        
        playerItem = AVPlayerItem(asset: asset!)
        player = AVPlayer(playerItem: playerItem!)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.contentsGravity = AVLayerVideoGravityResizeAspect
        player?.actionAtItemEnd = .none//播放完成后的动作
        
        videoLayer?.layer.addSublayer(playerLayer!)
        playerLayer?.frame = videoLayer!.bounds
        
        // 介绍Label
        introLabel = MULabel.introLabel()
        view.addSubview(introLabel!)
        introLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.top.equalTo(videoLayer!.snp.bottom).offset(25/baseWidth)
            make.height.equalTo(16)
        })
    }
    
    //MARK: 设置裁剪视图
    fileprivate func setTrimmerView(){
        
        //important！必须先从asset初始化，然后再设置frame值
        trimmerView = ICGVideoTrimmerView(asset: asset!)
        trimmerView?.frame = CGRect(x: 0, y: 352/baseWidth, width: screenWidth, height: 54/baseWidth)
        trimmerView?.backgroundColor = UIColor.white
        trimmerView?.leftThumbImage = #imageLiteral(resourceName: "trackImageIcon")
        trimmerView?.rightThumbImage = #imageLiteral(resourceName: "trackImageIcon")
        trimmerView?.showsRulerView = false
        trimmerView?.maxLength = CGFloat(videoMaxDuration)
        trimmerView?.minLength = CGFloat(5)
        
        view.addSubview(trimmerView!)
        
        trimmerView?.resetSubviews()
        trimmerView?.delegate = self
        
        //获取视频的结束时间
        let endTime: CGFloat = CGFloat(CMTimeGetSeconds(asset!.duration))
        if endTime >= CGFloat(videoMaxDuration) {
            stopTime = CGFloat(videoMaxDuration)
            timeString = "00:30"
        }else{
            stopTime = endTime
            timeString = String(format: "00:%02.0f", endTime)
        }
    }
    
    //MARK: 设置确认裁剪的视图
    fileprivate func setCropView(){
        
        cropView = MUView.setCropView()
        view.addSubview(cropView!)
        cropView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(76/baseWidth)
        })
        
        //MARK: 点击播放按钮的事件
        cropView?.touchPlayButton = {[weak self] _ in
            var imageName: String = ""
            if self?.isPlaying == true{
                self?.player?.pause()
                self?.playerBackTimer?.invalidate()
                imageName = "playButton2"
            }else{
                self?.player?.play()
                self?.setPlayBackTimer()
                imageName = "pauseButton2"
            }
            self?.isPlaying = !(self?.isPlaying)!
            self?.trimmerView?.hideTracker(!(self?.isPlaying)!)//是否隐藏进度条
            self?.cropView?.playButton?.setImage(UIImage(named: imageName), for: UIControlState.normal)
        }
    }
    
    
    //MARK: 监控播放进度
    fileprivate func setPlayBackTimer(){
        playerBackTimer = Timer(timeInterval: 1.0, target: self, selector: #selector(playFromBegin(_:)), userInfo: nil, repeats: true)
        RunLoop.current.add(playerBackTimer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    //MARK: 重头播放视频
    func playFromBegin(_ timer: Timer){
        if player != nil {
            videoPlayerBackPositon = CGFloat(CMTimeGetSeconds(player!.currentTime()))//获取当前播放进度
            trimmerView?.seek(toTime: videoPlayerBackPositon)//将进度条移动到指定时间点
            if videoPlayerBackPositon >= stopTime{
                videoPlayerBackPositon = startTime
                seekVideoToPos(pos: startTime)
                trimmerView?.seek(toTime: startTime)
            }//播放到stopTime时，自动重播
        }
    }
    
    //MARK: 裁剪视频
    fileprivate func cropVideo(){
        
        showSVProgress(title: nil)
        
        player?.pause()//先停止播放视频，销毁定时器
        playerBackTimer?.invalidate()
        let url: URL = URL(fileURLWithPath: videoPathString)
        //如果已经有视频存在，则先删除
        if FileManager.default.fileExists(atPath: url.path) == true{
            do{
                try FileManager.default.removeItem(at: url)
            }catch{ print(error) }
        }
        
        
        let compatiblePresets: [String] = AVAssetExportSession.exportPresets(compatibleWith: asset!)
        if compatiblePresets.contains(AVAssetExportPresetMediumQuality) {
            exportSession = AVAssetExportSession(asset: asset!, presetName: AVAssetExportPresetMediumQuality)
            
            exportSession?.outputURL = url//输出位置
            exportSession?.outputFileType = AVFileTypeMPEG4//视频格式
            let start: CMTime = CMTimeMakeWithSeconds(Double(startTime), asset!.duration.timescale)
            let duration: CMTime = CMTimeMakeWithSeconds(Double(stopTime), asset!.duration.timescale)
            let range: CMTimeRange = CMTimeRangeMake(start, duration)
            exportSession?.timeRange = range//截取视频的时间范围
            
            exportSession?.exportAsynchronously(completionHandler: { [weak self]_ in
                
                self?.dismissSVProgress()
                
                switch (self?.exportSession?.status)!{
                case AVAssetExportSessionStatus.failed, AVAssetExportSessionStatus.cancelled:
                    var errorString: String
                    if let error: Error = self?.exportSession?.error{
                        errorString = error.localizedDescription
                    }else{
                        errorString = NSLocalizedString("压缩失败", comment: "")
                    }
                    self?.alert(alertTitle: errorString)
                default:
                    DispatchQueue.main.async(execute: {
//                        if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path) == true{
//                            UISaveVideoAtPathToSavedPhotosAlbum(url.path, nil, nil, nil)
//                        }//保存裁剪的视频到相册
                        self?.cropVideoSuccess()//裁剪视频成功，进入预览界面
                    })
                }
            })
        }
    }
    
    //MARK: 裁剪视频成功，返回
    fileprivate func cropVideoSuccess(){
        if isComeFromMapView == true{
            getVideoSuccess?(URL(fileURLWithPath: videoPathString))
        }else{
            for vc: UIViewController in navigationController!.viewControllers{
                if vc.isKind(of: MUViewController.self){
                    _ = navigationController?.popToViewController(vc, animated: true)
                    getVideoSuccess?(URL(fileURLWithPath: videoPathString))
                }else if vc.isKind(of: MUViewRecordVC.self){
                    _ = navigationController?.popToViewController(vc, animated: true)
                    getVideoSuccess?(URL(fileURLWithPath: videoPathString))
                }
            }
        }
    }
    
    //MARK: 点击导航栏左按钮的事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        playerBackTimer?.invalidate()
        player?.pause()
        player = nil
        if shouldUseDismiss == false{
            _ = navigationController?.popViewController(animated: true)
        }else{
//            //弹窗提示用户是否重新录制视频
//            deleteAlert(alertTitle: NSLocalizedString("重新录制视频？", comment: ""), leftButtonTitle: NSLocalizedString("继续", comment: ""), rightButtonTitle: NSLocalizedString("重新录制", comment: ""), leftClosure: nil, rightClosure: { [weak self] _ in
//                self?.reRecordClosure?()
//            }, presentCompletion: nil)
            dismissSelf()
        }
    }
    
    //MARK: 点击确认按钮的事件，确认裁剪视频
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        cropVideo()
    }
}

extension MUCropVideoVC: ICGVideoTrimmerDelegate{
    
    //MARK: 裁剪视频的回调
    func trimmerView(_ trimmerView: ICGVideoTrimmerView, didChangeLeftPosition startTime: CGFloat, rightPosition endTime: CGFloat) {
        if startTime != self.startTime {
            seekVideoToPos(pos: startTime)
        }
        self.startTime = startTime
        self.stopTime = endTime
        
        let seconds: CGFloat = endTime - startTime
        if seconds >= CGFloat(videoMaxDuration) {
            timeString = "00:30"
        }else{
            timeString = String(format: "00:%02.0f", seconds)
        }
    }
    
    //MARK: 将视频移动到制定时间点
    fileprivate func seekVideoToPos(pos: CGFloat){
        videoPlayerBackPositon = pos//获取视频起点时间
        let time: CMTime = CMTimeMakeWithSeconds(Double(videoPlayerBackPositon), player!.currentTime().timescale)
        player?.seek(to: time, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero)
    }
}
