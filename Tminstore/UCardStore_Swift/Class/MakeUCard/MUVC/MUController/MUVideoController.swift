//
//  MUVideoController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/20.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 选择视频的控制器

import UIKit
import SnapKit
import Photos
import PhotosUI

class MUVideoController: BaseViewController {
    
    /// 相册类型
    fileprivate enum Section: Int {
        case allPhotos = 0
        case smartAlbums = 1
        case userCollections = 2
        
        static let count = 3
    }
    
    /// 成功获得相片的闭包
    var getVideoClosure: ((_ asset: AVAsset)->())?
    
    /// scrollView滚动到底部了
    var scrollToBottom: (()->())?
    
    /// 导航栏的自定义View
    var titleView: MUView?
    
    /// 展示图片的collectionView
    fileprivate var photoCollectionView: MUCollectionView?
    
    /// 用于获取相片缩略图
    fileprivate var imageManager: PHCachingImageManager?
    
    /// 从相册拉取的资源
    fileprivate var assetsFetchResults: PHFetchResult<PHAsset>?
    
    /// 所有相册
    fileprivate var allPhotos: PHFetchResult<PHAsset>?
    /// 智能相册
    fileprivate var smartAlbums: PHFetchResult<PHAssetCollection>?
    /// 用户相册
    fileprivate var userCollections: PHFetchResult<PHCollection>?
    
    /// 获取相片的参数
    fileprivate var fetchOptions: PHFetchOptions?
    
    /// 图片请求的requestID
    fileprivate var imageRequestID: Int32?
    
    /// 从PHCachingImageManager处获得的缩略图大小
    fileprivate var assetGridThumbnailSize: CGSize = CGSize.zero
    
    /// 被选中的cell的IndexPath
    fileprivate var selectedCellIndexPath: IndexPath?
    
    ///展示相册的高斯模糊视图
    fileprivate var albumVisualView: MUVisualEffectView?
    
    /// 是否需要展示相册视图
    fileprivate var shouldShowAlbumVisualView: Bool = true
    /// 是否停止获取相片
    fileprivate var isStopGetPhoto: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        view.layer.masksToBounds = true
        
        setTitleView()
        getPhoto()//从系统相册获取图片资源
        setPhotoCollectionView()//设置collectionView
        setAlbumVisualView()
        closures()
        autoLayoutAnimation(view: view, withDelay: 0.0)
    }
    
    //MARK: 离开页面时，停止获取图片
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        cancelRequestPhoto()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        dismissSVProgress()
    }
    
    //MARK: 设置切换照片来源的视图
    fileprivate func setTitleView(){
        titleView = MUView.customTitleView(isVideo: true)
        view.addSubview(titleView!)
        titleView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.height.equalTo(60/baseWidth)
        })
    }
    
    //MARK:设置collectionView
    fileprivate func setPhotoCollectionView(){
        photoCollectionView = MUCollectionView.photoCollectionView()
        view.addSubview(photoCollectionView!)
        photoCollectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.top.equalTo(titleView!.snp.bottom)
        })
        photoCollectionView?.delegate = self
        photoCollectionView?.dataSource = self
    }
    
    //MARK: 设置展示相册的高斯模糊视图
    fileprivate func setAlbumVisualView(){
        albumVisualView = MUVisualEffectView.albumVisualView()
        view.addSubview(albumVisualView!)
        albumVisualView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.equalTo(view)
            make.size.equalTo(photoCollectionView!)
            make.bottom.equalTo(view).offset(-HEIGHT(view))
        })
        
        albumVisualView?.albumTableView?.delegate = self
        albumVisualView?.albumTableView?.dataSource = self
    }
    
    //MARK: 从系统相册获取图片资源
    /**
     从系统相册获取图片资源
     */
    fileprivate func getPhoto() {
        
        imageManager = PHCachingImageManager()
        
        let scale: CGFloat = UIScreen.main.scale
        assetGridThumbnailSize = CGSize(width: photoCollectionViewCellSize.width * scale, height: photoCollectionViewCellSize.height * scale)
        
        fetchOptions = PHFetchOptions()
        fetchOptions?.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.video.rawValue)//只获取视频
        fetchOptions?.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions?.includeHiddenAssets = true//包含隐藏的照片
        
        allPhotos = PHAsset.fetchAssets(with: fetchOptions)//所有相册
        smartAlbums = PHAssetCollection.fetchAssetCollections(with: PHAssetCollectionType.smartAlbum, subtype: PHAssetCollectionSubtype.albumRegular, options: nil)//智能相册
        userCollections = PHCollectionList.fetchTopLevelUserCollections(with: nil)//用户相册
        
        assetsFetchResults = allPhotos
        
        PHPhotoLibrary.shared().register(self)
    }
    
    //MARK: 如果已经有选中的cell，则滚动到该cell
    /**
     如果已经有选中的cell，则滚动到该cell
     */
    func animateCollectionViewToSelectedCell(indexPath: IndexPath? = nil){
        if let indexPath: IndexPath = indexPath{
            photoCollectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
        }else if let indexPath: IndexPath = selectedCellIndexPath {
            photoCollectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.centeredVertically, animated: true)
        }
    }
    
    //MARK: 取消获取相片
    /// 取消获取相片
    func cancelRequestPhoto(){
        if imageManager != nil && imageRequestID != nil{
            imageManager!.cancelImageRequest(imageRequestID!)
            dismissSVProgress()
            isStopGetPhoto = true
        }
    }
    
    //MARK: 获取相册视频
    /// 获取相册视频
    ///
    /// - parameter indexPath: NSIndexPath
    func getPhotoFromLibrary(indexPath: IndexPath){
        //取消图片获取
        if imageRequestID != nil {
            imageManager?.cancelImageRequest(imageRequestID!)
        }
        
        isStopGetPhoto = false
        
        //增加加载提示框
        SVProgressHUD.setDefaultMaskType(.none)
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.show()
        
        let options: PHVideoRequestOptions = PHVideoRequestOptions()
        options.deliveryMode = PHVideoRequestOptionsDeliveryMode.mediumQualityFormat//获取中等质量的视频
        options.isNetworkAccessAllowed = true
        options.progressHandler = {[weak self] (resultTuple: (Double, Error?, UnsafeMutablePointer<ObjCBool>, [AnyHashable: Any]?)) in
            if self?.isStopGetPhoto == false{
                SVProgressHUD.showProgress(Float(resultTuple.0))
            }
        }
        
        let asset: PHAsset = assetsFetchResults![indexPath.item]
        
        imageRequestID = imageManager?.requestAVAsset(forVideo: asset, options: options
            , resultHandler: { [weak self] (asset: AVAsset?, _, _) in
                
                if asset != nil{
                    DispatchQueue.main.async {
                        self?.dismissSVProgress()
                        self?.getVideoClosure?(asset!)//输出视频成功
                        self?.selectedCellIndexPath = indexPath//保存被选中的cell的IndexPath
                    }
                }else{
                    DispatchQueue.main.async {
                        self?.handelNetWorkError(nil)
                    }
                }
        })
        
//        //请求图片，并保存requestID
//        imageRequestID = imageManager?.requestExportSession(forVideo: asset, options: options, exportPreset: AVAssetExportPresetMediumQuality, resultHandler: { [weak self] (session: AVAssetExportSession?, dic: [AnyHashable : Any]?) in
//            
//            if session != nil {
//                
//                do{
//                    let cachesURL: URL = try FileManager.default.url(for: FileManager.SearchPathDirectory.cachesDirectory, in: FileManager.SearchPathDomainMask.userDomainMask, appropriateFor: nil, create: true)//获取缓存文件夹路径
//                    let cachesPath: String = cachesURL.path//获取视频存储的文件夹路径
//                    let videoPathString: String = cachesPath + "/videoCache/\(UUID().uuidString).mp4"//设置视频保存路径
//                    
//                    let cacheDirectory: String = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first!//获取缓存文件夹
//                    let videoFolder: String = cacheDirectory + "/videoCache"
//                    
//                    if FileManager.default.fileExists(atPath: videoFolder) == false {
//                        do {
//                            try FileManager.default.createDirectory(atPath: videoFolder, withIntermediateDirectories: false, attributes: nil)
//                        }catch{
//                            self?.handelNetWorkError(error)
//                            print(error)
//                        }
//                    }//如果文件夹不存在，则创建一个
//                    
//                    session!.outputURL = URL(fileURLWithPath: videoPathString)//输出位置
//                    session!.outputFileType = AVFileTypeMPEG4//视频格式
//                    session!.exportAsynchronously(completionHandler: {
//                        switch session!.status{
//                        case AVAssetExportSessionStatus.failed, AVAssetExportSessionStatus.cancelled:
//                            DispatchQueue.main.async {
//                                self?.handelNetWorkError(session?.error)
//                            }
//                        default:
//                            DispatchQueue.main.async {
//                                self?.dismissSVProgress()
//                                self?.getVideoClosure?(videoPathString)//输出视频成功
//                                self?.selectedCellIndexPath = indexPath//保存被选中的cell的IndexPath
//                            }
//                        }
//                    })
//                }catch{
//                    DispatchQueue.main.async {
//                        self?.handelNetWorkError(error)
//                        print(error)
//                    }
//                }
//            }
//        })
        
        //取消cell的选中状态
        let cells: [MUPhotoCollectionViewCell]? = photoCollectionView?.visibleCells as? [MUPhotoCollectionViewCell]
        _ = cells?.map({ (cell: MUPhotoCollectionViewCell) in
            cell.isCellSelected = false
        })
        
        if let cell: MUPhotoCollectionViewCell = photoCollectionView?.cellForItem(at: IndexPath(row: indexPath.row, section: 0)) as? MUPhotoCollectionViewCell{
            cell.isCellSelected = true//给选中的cell，添加边框
        }
    }
    
    //MARK: 各种回调
    fileprivate func closures(){
        //MARK: 点击titleView的事件，切换相册
        titleView?.touchTitleViewClosure = {[weak self] _ in
            self?.albumVisualAnimation()
        }
    }
    
    //MARK: 展示相册的动画
    fileprivate func albumVisualAnimation(title: String? = nil){
        albumVisualView?.snp.updateConstraints({ (make: ConstraintMaker) in
            if shouldShowAlbumVisualView == true{
                make.bottom.equalTo(view).offset(0)
            }else{
                make.bottom.equalTo(view).offset(-HEIGHT(view))
            }
        })
        if shouldShowAlbumVisualView == true{
            titleView?.rotateArrowIconAnimation()
        }else{
            titleView?.changeTitleViewTitle(title: title)
        }
        shouldShowAlbumVisualView = !shouldShowAlbumVisualView
        autoLayoutAnimation(view: view, withDelay: 0.0)
    }
    
    deinit {
        PHPhotoLibrary.shared().unregisterChangeObserver(self)
    }
    
    override func handelNetWorkError(_ error: Error? = nil, withResult result: [String : Any]? = nil, loginSuccessClosure loginSuccess: (() -> ())? = nil) {
        dismissSVProgress()
        super.handelNetWorkError(error, withResult: result, loginSuccessClosure: loginSuccess)
    }
}

//MARK: --------- UICollectionViewDelegate, UICollectionViewDataSource ------------
extension MUVideoController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    //MARK: 点击cell获得照片
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        getPhotoFromLibrary(indexPath: IndexPath(item: indexPath.item, section: indexPath.section))
        animateCollectionViewToSelectedCell(indexPath: IndexPath(item: indexPath.item, section: indexPath.section))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        guard let count: Int = assetsFetchResults?.count, count > 0 else { return 0 }
        return count
    }
    
    //MARK: 获取缩略图，设置cell的图片
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let cell: MUPhotoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MUPhotoCollectionViewCellReuseIdentifier, for: indexPath) as! MUPhotoCollectionViewCell
        
        //给cell添加边框，或者取消边框
        if selectedCellIndexPath != nil && indexPath == selectedCellIndexPath {
            cell.isCellSelected = true
        }else{
            cell.isCellSelected = false
        }
        
        let asset: PHAsset = assetsFetchResults![indexPath.item]
        
        cell.representedAssetIdentifier = asset.localIdentifier
        
        //livePhoto
        if #available(iOS 9.1, *) {
            if (asset.mediaSubtypes == PHAssetMediaSubtype.photoLive) {
                let badge: UIImage = PHLivePhotoView.livePhotoBadgeImage(options: .overContent)
                cell.image = badge
            }
        }
        
        //异步请求图片
        imageManager?.requestImage(for: asset, targetSize: assetGridThumbnailSize, contentMode: PHImageContentMode.aspectFill, options: nil, resultHandler: { (result: UIImage?, _) in
            if cell.representedAssetIdentifier == asset.localIdentifier {
                cell.image = result
            }
        })
        
        return cell
    }
    
    //MARK: 监测scrollView滚动到底部了
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.height
        if bottomEdge >= scrollView.contentSize.height{
            scrollToBottom?()
        }
    }
}

//MARK:---------PHPhotoLibraryChangeObserver---------
extension MUVideoController: PHPhotoLibraryChangeObserver{
    //MARK: 监听相册是否被更改
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        
        DispatchQueue.main.sync {
            //检查已展示的asset是否已被更改
            if let result: PHFetchResult<PHAsset> = assetsFetchResults {
                if let collectionChanges: PHFetchResultChangeDetails<PHAsset> = changeInstance.changeDetails(for: result) {
                    assetsFetchResults = collectionChanges.fetchResultAfterChanges
                    photoCollectionView?.reloadData()
                }
            }
        }
    }
}

extension MUVideoController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Section.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Section(rawValue: section)! {
        case Section.allPhotos: return 0
        case Section.smartAlbums:
            if let album: PHFetchResult<PHAssetCollection> = smartAlbums{
                return album.count
            }else{
                return 0
            }
        case Section.userCollections:
            if let album: PHFetchResult<PHCollection> = userCollections{
                return album.count
            }else{
                return 0
            }
        }
    }
    
    //MARK: 设置tableViewCell的内容
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch Section(rawValue: indexPath.section)! {
        case Section.allPhotos:
            
            let cell: MUAlbumTableViewCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.allPhotos.rawValue, for: indexPath) as! MUAlbumTableViewCell
            cell.asset = allPhotos!.object(at: indexPath.row)
            cell.title = NSLocalizedString("所有相片", comment: "")
            cell.numberOfPhotos = allPhotos!.count
            
            return cell
            
        case Section.smartAlbums:
            
            let cell: MUAlbumTableViewCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.smartAlbums.rawValue, for: indexPath) as! MUAlbumTableViewCell
            let collection: PHAssetCollection = smartAlbums!.object(at: indexPath.row)
            let fetchResult: PHFetchResult<PHAsset> = PHAsset.fetchAssets(in: collection, options: fetchOptions!)
            cell.asset = fetchResult.firstObject
            cell.title = collection.localizedTitle!
            cell.numberOfPhotos = fetchResult.count
            
            return cell
            
        case Section.userCollections:
            
            let cell: MUAlbumTableViewCell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.userCollections.rawValue, for: indexPath) as! MUAlbumTableViewCell
            let collection: PHAssetCollection = userCollections!.object(at: indexPath.row) as! PHAssetCollection
            let fetchResult: PHFetchResult<PHAsset> = PHAsset.fetchAssets(in: collection, options: fetchOptions!)
            cell.asset = fetchResult.firstObject
            cell.title = collection.localizedTitle!
            cell.numberOfPhotos = fetchResult.count
            
            return cell
        }
    }
    
    //MARK: 点击album Cell改变标题，并重新刷新collectionView
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch Section(rawValue: indexPath.section)! {
        case Section.allPhotos:
            assetsFetchResults = allPhotos
        case Section.smartAlbums:
            let collection: PHAssetCollection = smartAlbums!.object(at: indexPath.row)
            assetsFetchResults = PHAsset.fetchAssets(in: collection, options: fetchOptions!)
        case Section.userCollections:
            let collection: PHAssetCollection = userCollections!.object(at: indexPath.row) as! PHAssetCollection
            assetsFetchResults = PHAsset.fetchAssets(in: collection, options: fetchOptions!)
        }
        let cell: MUAlbumTableViewCell = tableView.cellForRow(at: indexPath) as! MUAlbumTableViewCell
        albumVisualAnimation(title: cell.title)//改变标题，并进行动画
        photoCollectionView?.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
