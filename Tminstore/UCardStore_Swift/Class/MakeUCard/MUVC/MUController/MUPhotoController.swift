//
//  MUPhotoController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/19.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import CoreLocation

class MUPhotoController: BaseViewController {
    
    var postcardModel: MUFinalPostcardModel?
    ///成功获得相片
    var getPhotoSuccess: (()->())?
    ///退出的闭包回调
    var dismissClosure: ((_ shouldSetPlayer: Bool)->())?
    
    /// 摄像头预览试图，用户接受手势
    fileprivate var previewView: UIView?
    
    /// 播放摄像头接收到的画面的layer
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer?
    
    /// 录制视频的类
    fileprivate var vision: ZTJVison?
    /// 聚焦的动画视图
    fileprivate var focusView: PBJFocusView?
    /// 确认视频的按钮
    fileprivate var confirmButton: VideoCommentButton?
    /// 弹出展示照片的视图
    fileprivate var showPhotoButton: MUButton?
    /// 隐藏选择照片视图的按钮
    fileprivate var hidePhotoButton: MUButton?
    /// 闪光灯按钮
    fileprivate var torchBarItem: LocalARBarButtonItem?
    
    /// 录屏的按钮
    fileprivate var switchCameraBarItem: LocalARBarButtonItem?
    /// 图片裁切的界面
    fileprivate var imageCropVC: MUPhotoCropController?
    /// 选择照片的控制器
    fileprivate var choosePhotoVC: MUPhotoVC?
    /// 当前照片视图离顶部的距离
    fileprivate var currentPhotoControllerOffset: CGFloat = 160/baseWidth
    // MARK: 是否隐藏状态栏
    override var prefersStatusBarHidden : Bool {
        return true
    }
    /// 录制视频的界面
    fileprivate var videoVC: MUViewRecordVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        setVideoRecorder()
        setPreviewView()
        setButtons()
        setChoosePhotoController()
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showPhotoController()
        vision?.startPreview()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        vision?.stopPreview()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        hidePhotoController()
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        transparentNavigationBar()
        setNavigationBarLeftButtonWithImageName("photoDismiss")
        navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        torchBarItem = LocalARBarButtonItem.torchBarItem()
        torchBarItem?.shouldControllerTorchSelf = true
        switchCameraBarItem = LocalARBarButtonItem.switchCameraBarItem()
        navigationItem.rightBarButtonItems = [switchCameraBarItem!, torchBarItem!]
    }

    //MARK: 设置视频录制
    fileprivate func setVideoRecorder(){
        if vision == nil {
            showSVProgress(title: nil)//初始化控件时，不允许交互
            vision = ZTJVison.ztJVison(device: PBJCameraDevice.back, andMaxDuration: 10.0, andCameraMode: PBJCameraMode.photo)
            vision?.delegate = self
        }
        vision?.startPreview()//开始预览
        focusView = PBJFocusView()//设置聚焦视图
    }
    
    //MARK: 设置预览界面
    fileprivate func setPreviewView(){
        
        previewView = UIView(frame: UIScreen.main.bounds)
        previewView?.backgroundColor = UIColor.black
        view.addSubview(previewView!)
        previewView?.frame.origin.y = -navBarHeight
        
        //点按重新聚焦
        let focusTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToFocus(_:)))
        previewView?.addGestureRecognizer(focusTap)
        
        previewLayer = vision?.previewLayer
        previewLayer?.frame = (previewView?.bounds)!
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        previewView?.layer.addSublayer(previewLayer!)
    }
    
    //MARK: 点击屏幕，重新聚焦
    func tapToFocus(_ tap: UITapGestureRecognizer){
        
        let tapPoint: CGPoint = tap.location(in: previewView!)
        
        focusView!.frame.origin.x = tapPoint.x - WIDTH(focusView!)/2
        focusView!.frame.origin.y = tapPoint.y - HEIGHT(focusView!)/2
        previewView?.addSubview(focusView!)
        focusView?.startAnimation()
        
        let adjustPoint: CGPoint = PBJVisionUtilities.convertToPointOfInterest(fromViewCoordinates: tapPoint, inFrame: previewView!.frame)
        
        vision?.focusExposeAndAdjustWhiteBalance(atAdjustedPoint: adjustPoint)
    }
    
    //MARK: 设置按钮
    fileprivate func setButtons(){
        confirmButton = VideoCommentButton.takePhotoButton()
        view.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-37/baseWidth)
            make.size.equalTo(CGSize(width: 83/baseWidth, height: 83/baseWidth))
        })
        
        showPhotoButton = MUButton.showPhotoButton()
        view.addSubview(showPhotoButton!)
        showPhotoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(confirmButton!)
            make.left.equalTo(confirmButton!.snp.right).offset(50/baseWidth)
            make.size.equalTo(CGSize(width: 25.6/baseWidth, height: 45.5/baseWidth))
        })
    }
    
    //MARK: 设置选择照片的控制器
    fileprivate func setChoosePhotoController(){
        choosePhotoVC = MUPhotoVC()
        navigationController?.view.addSubview(choosePhotoVC!.view)
        choosePhotoVC?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(navigationController!.view).offset(currentPhotoControllerOffset)
            make.left.right.equalTo(navigationController!.view)
            make.height.equalTo(screenHeight)
        })
        
        let pan: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panToMovePhotoController(pan:)))
        choosePhotoVC?.titleView?.addGestureRecognizer(pan)
        
        hidePhotoButton = MUButton.hidePhotoButton()
        navigationController?.view.addSubview(hidePhotoButton!)
        hidePhotoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(choosePhotoVC!.view)
            make.bottom.equalTo(choosePhotoVC!.view.snp.top).offset(-25/baseHeight)
            make.size.equalTo(CGSize(width: 25.6/baseWidth, height: 45.5/baseWidth))
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 点击拍照按钮拍照
        confirmButton?.touchButtonClosure = {[weak self] _ in
            if self?.vision?.canCapturePhoto == true{
                self?.vision?.capturePhoto()
            }
        }
        
        //MARK: 点击导航栏右按钮，切换摄像头正反面
        switchCameraBarItem?.touchMoreButtonClosure = {[weak self] _ in
            self?.touchRotateButton()
        }
        
        //MARK: 点击闪光灯按钮，打开或者关闭闪光灯
        torchBarItem?.touchMoreButtonClosure = {[weak self] _ in
            if self?.vision?.isFlashAvailable == true{
                if self?.vision?.flashMode == PBJFlashMode.off{
                    self?.vision?.flashMode = PBJFlashMode.on
                    self?.torchBarItem?.image = UIImage(named: "localTorchOn")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                }else if self?.vision?.flashMode == PBJFlashMode.on{
                    self?.vision?.flashMode = PBJFlashMode.off
                    self?.torchBarItem?.image = UIImage(named: "localTorchOff")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
                }
            }
        }
        
        //MARK: 点击隐藏照片选择器按钮的事件
        hidePhotoButton?.touchButtonClosure = {[weak self] _ in
            self?.hidePhotoController()
        }
        
        //MARK: 点击展示照片选择器按钮的事件
        showPhotoButton?.touchButtonClosure = {[weak self] _ in
            self?.showPhotoController()
        }
        
        //MARK: 成功获得照片
        choosePhotoVC?.getImageClosure = {[weak self] (image: UIImage, location: CLLocation?) in
            self?.postcardModel?.photoLocation = location
            self?.enterPhotoCropVC(image: image)
        }
        
        //MARK: 选择照片的视图，滚动到底部了
        choosePhotoVC?.scrollToBottom = {[weak self] _ in
            self?.fullScreenToShowPhotoController()
        }
    }
    
    //MARK: 点击切换摄像头的按钮，切换前后摄像头
    func touchRotateButton()  {
        guard let vision: ZTJVison = self.vision else { return}
        if vision.cameraDevice == PBJCameraDevice.back {
            vision.cameraDevice = PBJCameraDevice.front
        }else{
            vision.cameraDevice = PBJCameraDevice.back
        }
    }
    
    //MARK: 点击导航栏左按钮
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf(animated: true, completion: {[weak self] _ in
            self?.dismissClosure?(false)
        })
    }
    
    //MARK: 进入照片裁剪界面
    fileprivate func enterPhotoCropVC(image: UIImage){
        imageCropVC = MUPhotoCropController(croppingStyle: TOCropViewCroppingStyle.default, image: image)
        imageCropVC?.delegate = self
        present(imageCropVC!, animated: true, completion: nil)
    }
    
    //MARK: 全屏展示选择照片的视图
    fileprivate func fullScreenToShowPhotoController(){
        choosePhotoVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(navigationController!.view).offset(0)
        })
        autoLayoutAnimation(view: navigationController!.view)
        currentPhotoControllerOffset = 0
    }
    
    //MARK: 隐藏选择照片的视图
    fileprivate func hidePhotoController(){
        choosePhotoVC?.cancelRequestPhoto()//先取消获取相片
        choosePhotoVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(navigationController!.view).offset(screenHeight + 80)
        })
        autoLayoutAnimation(view: navigationController!.view)
        currentPhotoControllerOffset = 160/baseWidth
    }
    
    //MARK: 展示选择照片的控制器
    fileprivate func showPhotoController(){
        choosePhotoVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(navigationController!.view).offset(160/baseWidth)
        })
        autoLayoutAnimation(view: navigationController!.view)
        currentPhotoControllerOffset = 160/baseWidth
    }
    
    //MARK: 滑动，移动选择照片的控制器
    func panToMovePhotoController(pan: UIPanGestureRecognizer){
        if let view: UIView = pan.view{
            let movePoint: CGPoint = pan.translation(in: view)
            switch pan.state {
            case UIGestureRecognizerState.began, UIGestureRecognizerState.changed:
                choosePhotoVC?.view.snp.updateConstraints({ (make: ConstraintMaker) in
                    make.top.equalTo(navigationController!.view).offset(currentPhotoControllerOffset + movePoint.y)
                })
            default:
                if movePoint.y < 0{
                    fullScreenToShowPhotoController()
                }else if movePoint.y > screenHeight/2 - 160/baseHeight{
                    hidePhotoController()
                }else{
                    showPhotoController()
                }
                pan.setTranslation(CGPoint.zero, in: view)
            }
        }
    }
}
extension MUPhotoController: PBJVisionDelegate, TOCropViewControllerDelegate{
    
    //MARK: 对焦和曝光的代理
    func visionDidStopFocus(_ vision: PBJVision) {
        if focusView != nil {
            focusView?.stopAnimation()
        }
    }
    
    func visionDidChangeExposure(_ vision: PBJVision) {
        if focusView != nil {
            DispatchQueue.main.async {[weak self] _ in
                self?.focusView?.stopAnimation()
            }
        }
    }
    
    //MARK: 可以录制之后，再绘制录制按钮
    func visionSessionDidStart(_ vision: PBJVision) {
        dismissSVProgress()
    }
    
    //MARK: 拍摄完成
    func vision(_ vision: PBJVision, capturedPhoto photoDict: [AnyHashable : Any]?, error: Error?) {
        
        if error != nil{
            handelNetWorkError(error, withResult: nil)
            return
        }
        
        if let image: UIImage = photoDict?[PBJVisionPhotoImageKey] as? UIImage{
            enterPhotoCropVC(image: image)
            postcardModel?.photoLocation = currentUserLocation//获取当前的定位信息
        }else{
            handelNetWorkError()
        }
    }
    
    //MARK: 取消裁切照片
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        dismiss(animated: true, completion: nil)
    }
    //MARK: 成功获得照片
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        postcardModel?.frontImage = image
        getPhotoSuccess?()
//        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)//保存拍摄的照片
        
        if postcardModel?.videoPath == nil{
            dismiss(animated: true, completion: nil)
            videoVC = MUViewRecordVC()
            videoVC?.postcardModel = postcardModel
            navigationController?.pushViewController(videoVC!, animated: true)
            videoVC?.getVideoSuccess = {[weak self] _ in
                self?.dismissClosure?(true)
                self?.dismissSelf()
            }
        }else{
            dismissSelf()
        }
    }
}
