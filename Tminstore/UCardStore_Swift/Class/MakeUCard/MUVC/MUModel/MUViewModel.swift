//
//  MUViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/4.
//  Copyright © 2016年 ucard. All rights reserved.
//


class MUViewModel: BaseViewModel {

    fileprivate let dataModel: MUDataModel = MUDataModel()
    
    /// 需要添加视频
    var shouldAddVideo: (()->())?
    
    /// 需要添加相片
    var shouldAddPhoto: (()->())?
    
    /// 可以进入明信片背面的闭包
    var checkFrontViewSuccessClosure: (()->())?
    
    /// AR图片已经存在的闭包
    var arImageAlreadyExit: (()->())?
    
    /// AR图片的可识别度不够
    var arImageIsBad: (()->())?
    
    /// 网络请求失败的闭包
    var networkErrorClosure: ((_ error: Error?)->())?
    
    /// 背面还需要添加文字
    var shouldAddText: ((_ textViewTag: Int)->())?
    
    /// 背面检查通过
    var backCheckSuccess: (()->())?
    
    //MARK: 判断背面祝福语是否已经都添加了
    /**
     判断背面祝福语是否已经都添加了
     */
    func checkBackView(postcard: MUFinalPostcardModel) {
        //祝福语是否已填写
        if postcard.message  == ""{
            shouldAddText?(0)
            return
        }
        backCheckSuccess?()//检查通过
    }
    
    //MARK: 检测AR图片的网络请求
    /// 检测AR图片的网络请求
    ///
    /// - Parameter image: UIImage
    func checkARImage(image: UIImage){
        
        showSVProgress(title: NSLocalizedString("检测AR图片中，请稍后", comment: ""))
        
        dataModel.detectImage(image: image, successClosure: { [weak self] _ in
            self?.checkFrontViewSuccessClosure?()
            }, failureClosure: { [weak self] (error: Error?) in
            self?.networkErrorClosure?(error)
            }, lowGradeClosure: {[weak self] _ in
                self?.arImageIsBad?()
            }, completeClosure: { [weak self] _ in
                self?.dismissSVProgress()
        })
        
//        dataModel.checkARImageSimilar(image: image, successClosure: { [weak self] _ in
//            self?.checkFrontViewSuccessClosure?()
//            }, failureClosure: {[weak self] (error: Error?) in
//            self?.networkErrorClosure?(error)
//        }, similarClosure: { [weak self] _ in
//            self?.arImageAlreadyExit?()
//        }, lowGradeClosure: { [weak self] _ in
//            self?.arImageIsBad?()
//        }, completeClosure: { [weak self] _ in
//            self?.dismissSVProgress()
//        })
    }
    
    //MARK: 判断明信片信息是否齐全
    /// 判断明信片信息是否齐全
    ///
    /// - Parameter postcard: MUFinalPostcardModel
    /// - Returns: Bool
    func checkPostcard(postcard: MUFinalPostcardModel)->Bool{
        
        // 判断正面图片是否已经添加
        if postcard.frontImage == nil{
            shouldAddPhoto?()//需要添加图片
            return false
        }
        // 判断视频是否已经添加
        if let videoURL: URL = postcard.videoPath{
            if FileManager.default.fileExists(atPath: videoURL.path) == true{
                return true
            }
        }
        shouldAddVideo?()//需要添加视频
        return false
    }
        
    //MARK: 检查是否需要删除coreData
    /// 检查是否需要删除coreData
    ///
    /// - Parameter postcardData: Postcard
    func checkCoreData(postcardData: Postcard){
        dataModel.checkCoreData(postcardData: postcardData)
    }
    
}
