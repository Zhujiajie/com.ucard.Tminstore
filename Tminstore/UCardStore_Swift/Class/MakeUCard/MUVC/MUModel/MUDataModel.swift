//
//  MUDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/4.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreData

class MUDataModel: BaseDataModel {
    
    fileprivate let easyARModel: EasyARModel = EasyARModel()
    
    override init() {
        super.init()
        if fontHasLoaded == false{
            loadFont(url: appAPIHelp.fontPath())//加载中文字体
        }
    }

//    //MARK: 检查AR图片的重复性和可识别度
//    /**
//     检查AR图片的重复性和可识别度
//     
//     - parameter success:  成功的闭包
//     - parameter failure:  网络失败的闭包
//     - parameter similar:  已经有相似图片的闭包
//     - parameter lowGrade: 可识别度太低的闭包
//     - parameter complete: 网络请求完成的闭包
//     */
//    func checkARImageSimilar(image: UIImage, successClosure success: (()->())?, failureClosure failure: ((_ error: Error?)->())?, similarClosure similar: (()->())?, lowGradeClosure lowGrade: (()->())?, completeClosure complete: (()->())?){
//        
//        //压缩检测的图片
//        let imageData: Data? = UIImageJPEGRepresentation(image, 0.1)
//        if imageData == nil {
//            failure?(nil)
//        }
//        let image: UIImage? = UIImage(data: imageData!)
//        if image == nil {
//            failure?(nil)
//        }
//        
//        easyARModel.easyARSimilar(image: image!, successClosure: {
//            success?()
//        }, failureClourse: { (error: Error?) in
//                failure?(error)
//            }, similarClosure: { 
//                similar?()
//            }, lowGradeClosure: { 
//                lowGrade?()
//            }) { 
//                complete?()
//        }
//    }
    
    //MARK: 检测图片的可识别度
    /// 检测图片的可识别度
    ///
    /// - Parameters:
    ///   - image: 需要检测测图片
    ///   - success: 成功的闭包
    ///   - failure: 失败的闭包
    ///   - lowGrade: 识别度太低的闭包
    ///   - complete: 完成的闭包
    func detectImage(image: UIImage, successClosure success: (()->())?, failureClosure failure: ((_ error: Error?)->())?, lowGradeClosure lowGrade: (()->())?, completeClosure complete: (()->())?){
        
//        //压缩检测的图片
//        let imageData: Data? = UIImagePNGRepresentation(image)
//        if imageData == nil {
//            failure?(nil)
//        }
//        let image: UIImage? = UIImage(data: imageData!)
//        if image == nil {
//            failure?(nil)
//        }
        
        easyARModel.easyARDetection(image: image, successClosure: {
            success?()
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }, lowGradeClosure: {
            lowGrade?()
        }, completeClosure: {
            complete?()
        })
    }
    
    //MARK: 加载字体
    /// 加载字体
    ///
    /// - parameter url: NSURL
    fileprivate func loadFont(url: URL){
//        doInBackground(inBackground: {
//            if let providerRef: CGDataProvider = CGDataProvider(url: url as CFURL) {
//                let font: CGFont = CGFont(providerRef)
//                fontHasLoaded = CTFontManagerRegisterGraphicsFont(font, nil)
//            }
//            }, backToMainQueue: nil)
    }
    
    //MARK: 检查coredata是否需要删除
    /// 检查coredata是否需要删除
    ///
    /// - Parameter postcardData: Postcard
    func checkCoreData(postcardData: Postcard){
        if postcardData.frontPreviewImage == nil{
            deletePostcardData(postcardData: postcardData)// 删除CoreData
        }
    }
}
