//
//  MUEditAddressViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/28.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUEditAddressViewModel: BaseViewModel {
    
    let dataModel: MUEditAddressDataModel = MUEditAddressDataModel()
    
    /// 新增或修改地址成功
    var addOrEditAddressSuccess: (()->())?
    
    /// 错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    //MARK: 新增地址信息
    /// 新增地址信息
    ///
    /// - Parameter tableView: 呈现地址信息的tableView
    func addUserAddress(addressModel: AddressModel) {
        
        showSVProgress(title: nil)
        
        dataModel.addAddress(addressModel: addressModel, successClosure: { [weak self] (result: [String : Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.addOrEditAddressSuccess?()
                return
            }
            self?.errorClosure?(nil, result)
            }, failureClosure: { [weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
            }, completionClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 更新地址的请求
    /// 更新地址的请求
    ///
    /// - Parameters:
    ///   - tableView: 呈现地址信息的tableView
    ///   - addressId: 地址的ID
    func updateAddress(addressModel: AddressModel) {
        
        showSVProgress(title: nil)
        
        dataModel.updateAddress(addressModel: addressModel, successClosure: { [weak self] (result: [String: Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.addOrEditAddressSuccess?()
                return
            }
            self?.errorClosure?(nil, result)
            }, failureClosure: { [weak self](error: Error?) in
                self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
}
