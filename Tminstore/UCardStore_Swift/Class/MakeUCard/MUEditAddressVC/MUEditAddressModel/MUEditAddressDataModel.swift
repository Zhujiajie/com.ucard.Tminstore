//
//  MUEditAddressDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/28.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUEditAddressDataModel: BaseDataModel {

    //MARK: 新增地址的网络请求
    /// 新增地址的网络请求
    ///
    /// - Parameters:
    ///   - parameters: 参数
    ///   - success: 成功的闭包
    ///   - failsure: 失败的闭包
    ///   - completion: 完成闭包
    func addAddress(addressModel: AddressModel, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        var isDefault: String
        if addressModel.isDefault == true{
            isDefault = "01"
        }else{
            isDefault = "00"
        }
        
        let parameters: [String: Any] = [
            "access_token": getUserToken(),
            "mailName": addressModel.mailName,
            "phone": addressModel.phoneNumber,
            "postCode": addressModel.postcode,
            "isDefault": isDefault,
            "country": addressModel.country,
            "province": addressModel.province,
            "city": addressModel.city,
            "district": addressModel.district,
            "detailedAddress": addressModel.detailedAddress
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.addUserAddressAPI, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error) in
            failsure?(error)
        }) { 
            completion?()
        }
    }
    
    //MARK: 编辑地址的请求
    /**
     编辑地址的请求
     
     - parameter addressId:  地址ID
     - parameter parameters: 参数
     - parameter success:    成功的闭包
     - parameter failsure:   失败的闭包
     */
    func updateAddress(addressModel: AddressModel, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        var isDefault: String
        if addressModel.isDefault == true{
            isDefault = "01"
        }else{
            isDefault = "00"
        }
        
        let parameters: [String: Any] = [
            "access_token": getUserToken(),
            "mailName": addressModel.mailName,
            "phone": addressModel.phoneNumber,
            "postCode": addressModel.postcode,
            "isDefault": isDefault,
            "country": addressModel.country,
            "province": addressModel.province,
            "city": addressModel.city,
            "district": addressModel.district,
            "detailedAddress": addressModel.detailedAddress,
            "addressId": addressModel.addressId
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.updateAddressAPI, ParametersDictionary: parameters, successClosure: {(result: [String: Any]) in
            success?(result)
        }, failureClourse: {(error) in
            failsure?(error)
        }, completionClosure: {
            completion?()
        })
    }
}
