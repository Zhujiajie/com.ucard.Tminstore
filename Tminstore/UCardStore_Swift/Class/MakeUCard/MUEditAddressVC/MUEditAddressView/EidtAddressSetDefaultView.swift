//
//  EidtAddressSetDefaultView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/6/5.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class EidtAddressSetDefaultView: BaseView {

    var addressModel: AddressModel?{
        didSet{
            setDefault()
        }
    }
    
    fileprivate var defaultIcon: UIView?
    fileprivate var label: BaseLabel?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        
        defaultIcon = UIView()
        addSubview(defaultIcon!)
        defaultIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(self).offset(25/baseWidth)
            make.left.equalTo(self).offset(15/baseWidth)
            make.size.equalTo(CGSize(width: 16/baseWidth, height: 16/baseWidth))
        })
        defaultIcon?.layer.cornerRadius = 8/baseWidth
        
        label = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 14), andTextColorHex: "525252", andText: NSLocalizedString("设为默认地址", comment: ""))
        addSubview(label!)
        label?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(defaultIcon!)
            make.left.equalTo(defaultIcon!.snp.right).offset(12/baseWidth)
            make.right.equalTo(self)
            make.bottomMargin.equalTo(defaultIcon!)
        })
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToSetDefault(tap:)))
        addGestureRecognizer(tap)
    }
    
    //MARK: 点击自己，设置是否为默认地址
    func tapToSetDefault(tap: UITapGestureRecognizer){
        addressModel?.isDefault = !addressModel!.isDefault
        setDefault()
    }
    
    //MARK: 设置是否为默认地址
    fileprivate func setDefault(){
        if addressModel?.isDefault == true {
            defaultIcon?.layer.borderColor = UIColor.clear.cgColor
            defaultIcon?.backgroundColor = UIColor(hexString: "FF4B11")
        }else{
            defaultIcon?.layer.borderColor = UIColor(hexString: "979797").cgColor
            defaultIcon?.layer.borderWidth = 1
            defaultIcon?.backgroundColor = UIColor.clear
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
