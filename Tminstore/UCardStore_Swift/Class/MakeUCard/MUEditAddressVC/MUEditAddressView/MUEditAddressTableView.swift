//
//  MUEditAddressTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/25.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUEditAddressTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        backgroundColor = UIColor.white
        register(MUEditAddressTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MUEditAddressTableViewCellReuseIdentifier)
        isScrollEnabled = false
        tableFooterView = UIView()
        contentInset = UIEdgeInsets(top: 6/baseHeight, left: 0, bottom: 0, right: 0)
        estimatedRowHeight = 70
        
        let footerView: EidtAddressSetDefaultView = EidtAddressSetDefaultView()
        footerView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: 70/baseWidth)
        tableFooterView = footerView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
