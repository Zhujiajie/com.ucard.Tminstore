//
//  MUEditAddressTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/28.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SnapKit

class MUEditAddressTableViewCell: UITableViewCell {

    /// 需要弹出地址选择器
    var showAreaPicker: (()->())?
    
    /// cell的数据模型
    var addressModel: AddressModel?{
        didSet{
            if addressModel != nil{
                setCellData()
                cellIsReady = true
            }
        }
    }
    
    /// 根据indexPath来设置Cell的UI
    var indexPath: IndexPath?{
        didSet{
            if titleLabel?.text == nil{
                setCellUI()
            }   
        }
    }
    
    // 标题数组
    let titleArray: [String] = [NSLocalizedString("姓名", comment: ""), NSLocalizedString("联系电话", comment: ""), NSLocalizedString("邮编(选填)", comment: ""), NSLocalizedString("国家", comment: ""), NSLocalizedString("省·市·区", comment: ""), NSLocalizedString("详细地址", comment: "")]
    
    /// 输入地址信息的textField
    var textField: UITextField?
    
    /// 输入详细地址的textView
    var textView: FSTextView?
    
    /// 展示城市信息的label
    var cityInfoLabel: BaseLabel?
    
    /// 信息是否已完整
    var cellIsReady: Bool = false
    
    /// 标题
    fileprivate var titleLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        //cell的标题
        titleLabel                = UILabel()
        titleLabel?.textColor     = UIColor(hexString: "7D7D7D")
        titleLabel?.font          = UIFont.systemFont(ofSize: 18)
        titleLabel?.numberOfLines = 3
        titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        contentView.addSubview(titleLabel!)
    }
    
    //MARK: 设置输入地址信息的textField
    /**
     设置输入地址信息的textField
     */
    fileprivate func setCellUI() {
        
        titleLabel?.text = titleArray[indexPath!.row]//设置标题
        
        //设置cell的类型
        switch indexPath!.row {
        case 0,1,2,3://填写姓名，电话，邮编，国家的cell
            setTextField()
        case 4:
            setCityInfoLabel()//设置展示城市信息的label
        default://填写详细地址，城市的cell
            setTextView()
        }
     }
    
    //MARK: 设置textField
    fileprivate func setTextField(){
        //对不同额Cell的title做不同的布局
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(13/baseHeight)
            make.left.equalTo(contentView).offset(15/baseWidth)
            make.bottom.equalTo(contentView).offset(-14/baseHeight)
            make.width.equalTo(90/baseWidth)
            make.height.equalTo(26)
        })
        
        textField = UITextField()
        textField?.textAlignment = NSTextAlignment.right
        textField?.font = UIFont.systemFont(ofSize: 16)
        contentView.addSubview(textField!)
        textField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(titleLabel!)
            make.left.equalTo(titleLabel!.snp.right).offset(5/baseWidth)
            make.right.equalTo(contentView).offset(-15/baseWidth)
            make.height.equalTo(titleLabel!)
        })
        textField?.delegate = self
        
        if indexPath!.row == 1 || indexPath!.row == 2{
            textField?.keyboardType = UIKeyboardType.numberPad//输入手机号码的cell
        }
    }
    
    //MARK: 设置textView
    fileprivate func setTextView(){
        //对不同额Cell的title做不同的布局
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(13/baseHeight)
            make.left.equalTo(contentView).offset(15/baseWidth)
            make.right.equalTo(contentView).offset(-15/baseHeight)
            make.height.equalTo(25)
        })
        
        textView = FSTextView()
        textView?.textAlignment = NSTextAlignment.right
        textView?.font = UIFont.systemFont(ofSize: 16)
        contentView.addSubview(textView!)
        textView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(13/baseHeight)
            make.leftMargin.equalTo(titleLabel!)
            make.rightMargin.equalTo(titleLabel!)
            make.height.equalTo(50)
            make.bottom.equalTo(contentView).offset(-13/baseHeight)
        })
        textView?.delegate = self
    }
    
    //MARK: 设置展示城市信息的label
    fileprivate func setCityInfoLabel(){
        //对不同额Cell的title做不同的布局
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(13/baseHeight)
            make.left.equalTo(contentView).offset(15/baseWidth)
            make.right.equalTo(contentView).offset(-15/baseHeight)
            make.height.equalTo(25)
        })
        
        cityInfoLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 16), andTextColorHex: "393939", andTextAlignment: NSTextAlignment.right, andNumberOfLines: 1)
        contentView.addSubview(cityInfoLabel!)
        cityInfoLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(titleLabel!.snp.bottom).offset(13/baseHeight)
            make.leftMargin.equalTo(titleLabel!)
            make.rightMargin.equalTo(titleLabel!)
            make.height.equalTo(50)
            make.bottom.equalTo(contentView).offset(-13/baseHeight)
        })
    }
    
    //MARK: 设置cell的数据
    fileprivate func setCellData(){
        switch indexPath!.row {
        case 0:
            textField?.text = addressModel?.mailName
        case 1:
            textField?.text = addressModel?.phoneNumber
        case 2:
            textField?.text = addressModel?.postcode
        case 3:
            textField?.text = addressModel?.country
        case 4:
            cityInfoLabel?.text = addressModel!.province + addressModel!.city + addressModel!.district
        default:
            textView?.text = addressModel!.detailedAddress
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK:--------------UITextFieldDelegate-----------------
extension MUEditAddressTableViewCell: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //必填项为空，提示用户补充填写
        if textField.text == "" || textField.text == nil{
            if indexPath?.row == 2{
                cellIsReady = true//邮编为可选，可不填
            }else{
              cellIsReady = false
            }
        }else{
            switch indexPath!.row {
            case 0:
                addressModel?.mailName = textField.text!
            case 1:
                addressModel?.phoneNumber = textField.text!
            case 2:
                addressModel?.postcode = textField.text!
            default:
                addressModel?.country = textField.text!
            }
            cellIsReady = true
        }
    }
}

//MARK: -----------UITextViewDelegate---------------------
extension MUEditAddressTableViewCell: UITextViewDelegate{
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == NSLocalizedString("详细地址", comment: "") || textView.text == ""{
            cellIsReady = false
        }else{
            if indexPath?.row == 5{
                addressModel?.detailedAddress = textView.text
            }
            cellIsReady = true
        }
    }
}
