//
//  MUEditAddressController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/28.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SnapKit
import STPickerView

//MARK:-------------编辑地址的页面---------------

class MUEditAddressController: BaseViewController {

    fileprivate let viewModel: MUEditAddressViewModel = MUEditAddressViewModel()
    
    /// 成功新增或修改地址的闭包
    var addOrEditAddressSuccess: (()->())?
    
    /// 从地址列表页面传过来的地址模型
    var addressModel: AddressModel?
    
    /// tableView
    fileprivate var tableView: UITableView?
    
    /// 选择城市的pickerView
    fileprivate var areaPickerView: STPickerArea?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        setNavigationBar()//设置导航栏
        setTableViewAndPickcerView()//设置tableView
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
        if addressModel == nil{
            addressModel = AddressModel()
            addressModel?.isEdting = false//新增地址
        }
        //设置是否为默认地址的视图
        if let footerView: EidtAddressSetDefaultView = tableView?.tableFooterView as? EidtAddressSetDefaultView{
            footerView.addressModel = addressModel
        }
        for cell: UITableViewCell in tableView!.visibleCells{
            if let addressCell: MUEditAddressTableViewCell = cell as? MUEditAddressTableViewCell{
                addressCell.addressModel = addressModel
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        setNavigationBarRightButtonWithTitle(NSLocalizedString("保存", comment: ""))
        navigationItem.rightBarButtonItem?.tintColor = UIColor(hexString: "686868")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        var str: String
        if addressModel != nil{
            str = NSLocalizedString("编辑地址", comment: "")
        }else{
            str = NSLocalizedString("新增地址", comment: "")
        }
        setNavigationAttributeTitle(str, attributeDic: attDic)
    }

    //MARK: 设置tableView
    fileprivate func setTableViewAndPickcerView(){
        tableView = MUEditAddressTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
        
        areaPickerView = STPickerArea()
        areaPickerView?.delegate = self
        areaPickerView?.contentMode = STPickerContentMode.bottom
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 修改或新增地址成功
        viewModel.addOrEditAddressSuccess = {[weak self] _ in
            _ = self?.navigationController?.popViewController(animated: true)
            self?.addOrEditAddressSuccess?()
        }
        
        //MARK: 网络请求失败
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 点击导航栏左按钮的点击事件
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        alert(alertTitle: NSLocalizedString("你还未保存，确定离开", comment: ""), leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("确定", comment: ""), rightClosure: { [weak self] _ in
            _ = self?.navigationController?.popViewController(animated: true)
        })
    }
    
    //MARK: 点击导航栏保存按钮的事件
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        
        //检查地址信息完整
        for cell: UITableViewCell in tableView!.visibleCells {
            
            let addressCell: MUEditAddressTableViewCell = cell as! MUEditAddressTableViewCell
            
            //停止编辑状态
            if addressCell.indexPath?.row == 5{
                addressCell.textView?.endEditing(true)
            }else{
                addressCell.textField?.endEditing(true)
            }
            
            if addressCell.cellIsReady == false{
                switch addressCell.indexPath!.row {
                case 4:
                    areaPickerView?.show()
                case 5:
                    addressCell.textView?.becomeFirstResponder()
                default:
                    addressCell.textField?.becomeFirstResponder()
                }
                return
            }
        }
        
        //编辑已有地址的状态
        if addressModel?.isEdting == true {
            viewModel.updateAddress(addressModel: addressModel!)
            return
        }
        
        //检查地址信息完整，提交新地址信息
        viewModel.addUserAddress(addressModel: addressModel!)
    }
}

//MARK: ------------UITableViewDelegate, UITableViewDataSource------------
extension MUEditAddressController: UITableViewDelegate, UITableViewDataSource, STPickerAreaDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell: MUEditAddressTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUEditAddressTableViewCellReuseIdentifier, for: indexPath) as! MUEditAddressTableViewCell
        cell.indexPath = indexPath
        cell.addressModel = addressModel
        //弹出城市选择器
        cell.showAreaPicker = {[weak self] _ in
            self?.areaPickerView?.show()
        }
        return cell
    }
    //MARK: 点击cell，开始编辑
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell: MUEditAddressTableViewCell = tableView.cellForRow(at: indexPath) as! MUEditAddressTableViewCell
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0, 1, 2, 3:
            cell.textField?.becomeFirstResponder()
        case 4:
            view.endEditing(true)
            areaPickerView?.show()
        default:
            cell.textView?.becomeFirstResponder()
        }
    }
    
    //MARK: 成功取得城市信息
    func pickerArea(_ pickerArea: STPickerArea, province: String, city: String, area: String) {
        addressModel?.province = province
        addressModel?.city = city
        if area == ""{
            addressModel?.district = " "
        }else{
          addressModel?.district = area
        }
        if let cell: MUEditAddressTableViewCell = tableView?.cellForRow(at: IndexPath.init(row: 4, section: 0)) as? MUEditAddressTableViewCell{
            cell.addressModel = addressModel
        }//设置cell的数据
    }
}

