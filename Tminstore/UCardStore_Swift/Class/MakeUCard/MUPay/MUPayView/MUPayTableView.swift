//
//  MUPayTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/26.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUPayTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        backgroundColor = UIColor(hexString: "#F4F7F9")
        register(MUPayTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MUPayTableViewCellReuseIdentifier)
        estimatedRowHeight = 112
        isScrollEnabled = false
        
        tableFooterView = UIView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
