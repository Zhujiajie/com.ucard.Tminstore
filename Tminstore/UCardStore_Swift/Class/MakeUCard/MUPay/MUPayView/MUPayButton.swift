//
//  MUPayButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/27.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUPayButton: UIButton {

    var touchButton: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
    }

    //MARK: 确认支付的按钮
    /// 确认支付的按钮
    ///
    /// - Returns: MUPayButton
    class func confirmPayButton()->MUPayButton{
        
        let button: MUPayButton = MUPayButton()
        button.backgroundColor = UIColor(hexString: "4CDFED")
        
        let str: String = NSLocalizedString("确认支付", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        
        button.setAttributedTitle(attStr, for: .normal)
        
        return button
    }
    
    //MARK: 支付完成之后的按钮
    /// 支付完成之后的按钮
    ///
    /// - Parameter isShareButton: 是否是分享按钮
    /// - Returns: MUPayButton
    class func paySuccessButton(isShareButton: Bool)->MUPayButton{
        
        let button: MUPayButton = MUPayButton()
        button.backgroundColor = UIColor(hexString: "60DBE8")
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        
        var str:String
        if isShareButton == true{
            str = NSLocalizedString("投递时空", comment: "")
        }else{
            str = NSLocalizedString("返回时空", comment: "")
        }
        
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        
        button.setAttributedTitle(attStr, for: .normal)
        
        return button
    }
    
    //MARK: 点击按钮的事件
    func touchButton(button: UIButton){
        touchButton?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
