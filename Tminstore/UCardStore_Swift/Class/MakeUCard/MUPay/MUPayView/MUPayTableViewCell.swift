//
//  MUPayTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/1.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUPayTableViewCell: UITableViewCell {

    /// 需要支付的金额
    var priceNeedToPay: Double = 0.0
    
    var indexPath: IndexPath?{
        didSet{
            if indexPath?.section == 1{
                setPriceCell()
            }else{
                setPayStyleCell()
            }
        }
    }
    
    /// 地址模型
    var addressModel: AddressModel?{
        didSet{
            setAddressCell()//设置显示地址的cell
        }
    }
    
    /// cell是否被选中
    var isCellSelected: Bool = false{
        didSet{
            if selectedIconImageView != nil{
                if isCellSelected == true{
                    selectedIconImageView?.image = #imageLiteral(resourceName: "paySelected")
                }else{
                    selectedIconImageView?.image = #imageLiteral(resourceName: "payDeselected")
                }
            }
        }
    }
    
    /// 地址相关的UI
    fileprivate var nameLabel: UILabel?
    fileprivate var postcodeLabel: UILabel?
    fileprivate var phoneLabel: UILabel?
    fileprivate var stateLabel: UILabel?
    fileprivate var detailAddressLabel: UILabel?
    fileprivate var addressIcon01: UIImageView?
    fileprivate var addressIcon02: UIImageView?
    
    ///标题
    fileprivate var titleLabel: UILabel?
    /// 价格标签
    fileprivate var priceLabel: UILabel?
    
    /// 支付方式
    fileprivate var payStyleImageView: UIImageView?
    
    /// cell被选中的icon
    fileprivate var selectedIconImageView: UIImageView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
    }
    
    //MARK: 设置地址cell
    fileprivate func setAddressCell(){
        
        addressIcon01 = UIImageView(image: #imageLiteral(resourceName: "payAddressIcon01"))
        addressIcon01?.contentMode = UIViewContentMode.scaleAspectFill
        contentView.addSubview(addressIcon01!)
        addressIcon01?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.height.equalTo(3/baseHeight)
        })
        
        addressIcon02 = UIImageView(image: #imageLiteral(resourceName: "payAddressIcon02"))
        addressIcon02?.contentMode = UIViewContentMode.scaleAspectFill
        contentView.addSubview(addressIcon02!)
        addressIcon02?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.left.right.equalTo(contentView)
            make.height.equalTo(3/baseHeight)
        })
        
        nameLabel = UILabel()
        nameLabel?.font = UIFont.systemFont(ofSize: 17)
        nameLabel?.text = addressModel?.mailName
        nameLabel?.textColor = UIColor(hexString: "403F3F")
        contentView.addSubview(nameLabel!)
        nameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(addressIcon01!.snp.bottom).offset(12/baseHeight)
            make.left.equalTo(contentView).offset(19/baseWidth)
            make.height.equalTo(20)
        })
        
        postcodeLabel = UILabel()
        postcodeLabel?.textColor = UIColor(hexString: "403F3F")
        postcodeLabel?.font = UIFont.systemFont(ofSize: 14)
        postcodeLabel?.text = addressModel?.postcode
        contentView.addSubview(postcodeLabel!)
        postcodeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(contentView).offset(-38/baseWidth)
            make.centerY.equalTo(nameLabel!)
            make.height.equalTo(18)
        })
        
        phoneLabel = UILabel()
        phoneLabel?.textColor = UIColor(hexString: "393939")
        phoneLabel?.font = UIFont.systemFont(ofSize: 16)
        phoneLabel?.text = addressModel?.phoneNumber
        contentView.addSubview(phoneLabel!)
        phoneLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.leftMargin.equalTo(nameLabel!)
            make.top.equalTo(nameLabel!.snp.bottom).offset(7/baseHeight)
            make.height.equalTo(22)
        })
        
        detailAddressLabel = UILabel()
        detailAddressLabel?.textColor = UIColor(hexString: "403F3F")
        detailAddressLabel?.font = UIFont.systemFont(ofSize: 14)
        detailAddressLabel?.text = addressModel!.country + addressModel!.province + addressModel!.detailedAddress
        detailAddressLabel?.numberOfLines = 3
        contentView.addSubview(detailAddressLabel!)
        detailAddressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(phoneLabel!.snp.bottom).offset(8/baseHeight)
            make.leftMargin.equalTo(nameLabel!)
            make.right.equalTo(contentView).offset(-19/baseWidth)
            make.bottom.equalTo(addressIcon02!.snp.top).offset(-8/baseHeight)
        })
    }
    
    
    //MARK: 设置订单价格cell
    fileprivate func setPriceCell(){
        
        titleLabel = UILabel()
        titleLabel?.textColor = UIColor(hexString: "6C6C6C")
        titleLabel?.font = UIFont.systemFont(ofSize: 14)
        titleLabel?.text = NSLocalizedString("订单总价", comment: "")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(18/baseHeight)
            make.left.equalTo(contentView).offset(20/baseWidth)
            make.bottom.equalTo(contentView).offset(-18/baseHeight)
            make.height.equalTo(20)
        })
        
        priceLabel = UILabel()
        priceLabel?.textColor = UIColor(hexString: "6ED7E4")
        if #available(iOS 8.2, *) {
            priceLabel?.font = UIFont.systemFont(ofSize: 20, weight: UIFontWeightMedium)
        } else {
            priceLabel?.font = UIFont.systemFont(ofSize: 20)
        }
        priceLabel?.text = String(format: "%.2f", priceNeedToPay)//设置价格
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(titleLabel!)
            make.right.equalTo(contentView).offset(-20/baseWidth)
        })
    }
    
    //MARK: 设置支付方式的cell
    fileprivate func setPayStyleCell(){
        
        var payStyleImage: UIImage
        var titleString: String
        var selectedImage: UIImage
        
        switch indexPath!.row {
        case 0:
            payStyleImage = #imageLiteral(resourceName: "weChatPay")
            titleString = NSLocalizedString("微信支付", comment: "")
            selectedImage = #imageLiteral(resourceName: "paySelected")
        default:
            payStyleImage = #imageLiteral(resourceName: "aliPay")
            titleString = NSLocalizedString("支付宝支付", comment: "")
            selectedImage = #imageLiteral(resourceName: "payDeselected")

        }
        
        payStyleImageView = UIImageView(image: payStyleImage)
        contentView.addSubview(payStyleImageView!)
        payStyleImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(23/baseWidth)
            make.top.equalTo(contentView).offset(13/baseHeight)
            make.bottom.equalTo(contentView).offset(-13/baseHeight)
            make.size.equalTo(CGSize(width: 34/baseWidth, height: 34/baseWidth))
        })
        
        titleLabel = UILabel()
        titleLabel?.textColor = UIColor(hexString: "ABABAB")
        titleLabel?.font = UIFont.systemFont(ofSize: 14)
        titleLabel?.text = titleString
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(payStyleImageView!.snp.right).offset(50/baseWidth)
            make.centerY.equalTo(contentView)
            make.height.equalTo(20)
        })
        
        selectedIconImageView = UIImageView(image: selectedImage)
        selectedIconImageView?.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(selectedIconImageView!)
        selectedIconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(contentView).offset(-20/baseWidth)
            make.centerY.equalTo(contentView)
            make.size.equalTo(CGSize(width: 26/baseWidth, height: 26/baseWidth))
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        nameLabel?.removeFromSuperview()
        postcodeLabel?.removeFromSuperview()
        phoneLabel?.removeFromSuperview()
        stateLabel?.removeFromSuperview()
        detailAddressLabel?.removeFromSuperview()
        addressIcon01?.removeFromSuperview()
        addressIcon02?.removeFromSuperview()
        
        payStyleImageView?.removeFromSuperview()
        selectedIconImageView?.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
