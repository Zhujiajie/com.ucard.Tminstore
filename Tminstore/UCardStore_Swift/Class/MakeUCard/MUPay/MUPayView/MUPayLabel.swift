//
//  MUPayLabel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/31.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUPayLabel: UILabel {
    
    //MARK: 支付成功的Label
    /// 支付成功的Label
    ///
    /// - Returns: MUPayLabel
    class func paySuccessLabel()->MUPayLabel{
        
        let label: MUPayLabel = MUPayLabel()
        label.font = UIFont.systemFont(ofSize: 24)
        label.textColor = UIColor(hexString: "565656")
        label.text = NSLocalizedString("支付成功", comment: "")
        
        return label
    }
    
    //MARK: 支付结果标题
    /// 支付结果标题
    ///
    /// - Parameter payTitleStyle: 标题类型
    /// - Returns: MUPayLabel
    class func payTitleLabel(payTitleStyle: PayTitleStyle)->MUPayLabel{
        
        let label: MUPayLabel = MUPayLabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.textColor = UIColor(hexString: "696969")
        label.textAlignment = NSTextAlignment.right
        
        switch payTitleStyle {
        case PayTitleStyle.number:
            label.text = NSLocalizedString("订单编号", comment: "")
        case PayTitleStyle.method:
            label.text = NSLocalizedString("交易方式", comment: "")
        default:
            label.text = NSLocalizedString("下单时间", comment: "")
        }
        
        return label
    }
    
    //MARK: 支付结果详情的标题
    class func payDetailLabel(payModel: PayModel, andPayTitleStyle payTitleStyle: PayTitleStyle)->MUPayLabel{
        
        let label: MUPayLabel = MUPayLabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = UIColor(hexString: "BEBEBE")
        
        switch payTitleStyle {
        case PayTitleStyle.number:
            label.text = payModel.orderNo
        case PayTitleStyle.method:
            var str: String
            switch payModel.payStyle {
            case PayStyle.weChat:
                str = NSLocalizedString("微信支付", comment: "")
            case PayStyle.aliPay:
                str = NSLocalizedString("支付宝支付", comment: "")
            case PayStyle.coupon:
                str = NSLocalizedString("优惠券支付", comment: "")
            default:
                str = NSLocalizedString("未知", comment: "")
            }
            label.text = str
        default:
            let date: Date = Date()
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            dateFormatter.timeStyle = DateFormatter.Style.short
            label.text = dateFormatter.string(from: date)
        }
        
        return label
    }
}
