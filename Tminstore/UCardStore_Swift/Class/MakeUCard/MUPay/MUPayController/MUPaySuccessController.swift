//
//  MUPaySuccessController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/4.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:----------------支付成功的页面---------------------


import UIKit
import SnapKit
import IQKeyboardManagerSwift

class MUPaySuccessController: BaseViewController {

    fileprivate let viewModel: MUPayViewModel = MUPayViewModel()
    
    var postcard: MUFinalPostcardModel?
    var order: OrderPostcardModel?
    
    /// 订单模型
    var payModel: PayModel?
    
    /// 上部分白色的UI
    fileprivate var upperContainerView: UIView?
    
    /// 投递到时空圈的按钮
    fileprivate var shareButton: MUPayButton?
    
    /// 返回的按钮
    fileprivate var backButton: MUPayButton?
    
    /// 积分提示框
    fileprivate var creditAlerVC: MUPublishAlertVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // 禁止滑动返回
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        setNavigationBar()//设置导航栏
        setUI()//设置UI
        closure()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("支付成功", comment: ""), attributeDic: attDic)
        navigationItem.hidesBackButton = true
    }

    fileprivate func setUI(){
        
        view.backgroundColor = UIColor(hexString: "#F4F7F9")
        
        setUpperUI()//设置上部分的UI界面
        setBlowUI()//设置下半部份的UI
        closure()
    }
    
    //MARK: 设置上部分的UI界面
    fileprivate func setUpperUI(){
        
        upperContainerView = UIView()//上部分白色的界面
        upperContainerView?.backgroundColor = UIColor.white
        view.addSubview(upperContainerView!)
        upperContainerView?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(view).offset(9/baseHeight)
            make.centerX.equalTo(view)
            make.width.equalTo(screenWidth)
            make.height.equalTo(251/baseWidth)
        }
        
        //支付成功的图片
        let successImageView: UIImageView = UIImageView(image: #imageLiteral(resourceName: "paySuccess"))
        upperContainerView?.addSubview(successImageView)
        successImageView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(upperContainerView!).offset(13/baseHeight)
            make.centerX.equalTo(upperContainerView!)
            make.size.equalTo(CGSize(width: 87/baseWidth, height: 87/baseWidth))
        }
        
        //支付成功的Label
        let successLabel: MUPayLabel = MUPayLabel.paySuccessLabel()
        upperContainerView?.addSubview(successLabel)
        successLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(successImageView.snp.bottom).offset(21/baseWidth)
            make.centerX.equalTo(successImageView)
            make.height.equalTo(26)
        }
        
        
        //返回到社区的按钮
        backButton = MUPayButton.paySuccessButton(isShareButton: false)
        upperContainerView?.addSubview(backButton!)
        backButton?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(upperContainerView!).offset(176/baseWidth)
            make.centerX.equalTo(upperContainerView!)
            make.size.equalTo(CGSize(width: 142/baseWidth, height: 47/baseWidth))
        }
        
//        if (order != nil && order!.isShared == false) || order == nil{
//            
//            if postcard?.isComeFromPurchase == false{
//                //分享到社区的按钮
//                shareButton = MUPayButton.paySuccessButton(isShareButton: true)
//                upperContainerView?.addSubview(shareButton!)
//                shareButton?.snp.makeConstraints({ (make: ConstraintMaker) in
//                    make.top.equalTo(upperContainerView!).offset(176/baseWidth)
//                    make.left.equalTo(upperContainerView!).offset(29/baseWidth)
//                    make.size.equalTo(CGSize(width: 142/baseWidth, height: 47/baseWidth))
//                })
//                
//                //返回到社区的按钮
//                backButton = MUPayButton.paySuccessButton(isShareButton: false)
//                upperContainerView?.addSubview(backButton!)
//                backButton?.snp.makeConstraints { (make: ConstraintMaker) in
//                    make.topMargin.equalTo(shareButton!)
//                    make.right.equalTo(upperContainerView!).offset(-29/baseWidth)
//                    make.size.equalTo(shareButton!)
//                }
//            }
//            // 买图流程
//            else{
//                
//            }
//            
//        }else if (order != nil && order!.isShared == true){
//            
//            //返回到社区的按钮
//            backButton = MUPayButton.paySuccessButton(isShareButton: false)
//            upperContainerView?.addSubview(backButton!)
//            backButton?.snp.makeConstraints { (make: ConstraintMaker) in
//                make.top.equalTo(upperContainerView!).offset(176/baseWidth)
//                make.centerX.equalTo(upperContainerView!)
//                make.size.equalTo(CGSize(width: 142/baseWidth, height: 47/baseWidth))
//            }
//        }
    }
    
    //MARK: 设置下部分白色的UI
    fileprivate func setBlowUI(){
        
        let blowContainerView: UIView = UIView()
        blowContainerView.backgroundColor = UIColor.white
        view.addSubview(blowContainerView)
        blowContainerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(upperContainerView!.snp.bottom).offset(19/baseHeight)
            make.centerX.equalTo(upperContainerView!)
            make.width.equalTo(screenWidth)
            make.height.equalTo(124/baseWidth)
        }
        
        //显示订单编号的两个label
        let serialLable1: MUPayLabel = MUPayLabel.payTitleLabel(payTitleStyle: PayTitleStyle.number)
        blowContainerView.addSubview(serialLable1)
        serialLable1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(blowContainerView).offset(22/baseHeight)
            make.right.equalTo(blowContainerView).offset(-231/baseWidth)
        }
        
        let serialLabel2: MUPayLabel = MUPayLabel.payDetailLabel(payModel: payModel!, andPayTitleStyle: PayTitleStyle.number)
        blowContainerView.addSubview(serialLabel2)
        serialLabel2.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.equalTo(serialLable1.snp.right).offset(17/baseWidth)
            make.centerY.equalTo(serialLable1)
        }
        
        //显示交易方式的label
        let typeLabel1: MUPayLabel = MUPayLabel.payTitleLabel(payTitleStyle: PayTitleStyle.method)
        blowContainerView.addSubview(typeLabel1)
        typeLabel1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(serialLable1.snp.bottom).offset(9/baseHeight)
            make.rightMargin.equalTo(serialLable1)
        }
        
        let typeLabel2: MUPayLabel = MUPayLabel.payDetailLabel(payModel: payModel!, andPayTitleStyle: PayTitleStyle.method)
        blowContainerView.addSubview(typeLabel2)
        typeLabel2.snp.makeConstraints { (make: ConstraintMaker) in
            make.leftMargin.equalTo(serialLabel2)
            make.centerY.equalTo(typeLabel1)
        }
        
        //显示下单时间的label
        let dateLabel1: MUPayLabel = MUPayLabel.payTitleLabel(payTitleStyle: PayTitleStyle.orderTime)
        blowContainerView.addSubview(dateLabel1)
        dateLabel1.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(typeLabel1.snp.bottom).offset(9/baseHeight)
            make.rightMargin.equalTo(serialLable1)
        }
        
        let dateLabel2: MUPayLabel = MUPayLabel.payDetailLabel(payModel: payModel!, andPayTitleStyle: PayTitleStyle.orderTime)
        blowContainerView.addSubview(dateLabel2)
        dateLabel2.snp.makeConstraints { (make: ConstraintMaker) in
            make.leftMargin.equalTo(serialLabel2)
            make.centerY.equalTo(dateLabel1)
        }
    }
    
    //MARK: 弹出积分确认视图
    fileprivate func showCreditView(){
        
        creditAlerVC = MUPublishAlertVC()
        creditAlerVC?.payModel = payModel
        addChildViewController(creditAlerVC!)
        view.addSubview(creditAlerVC!.view)
        creditAlerVC?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        //MARK: 成功分享到时空圈
        creditAlerVC?.shareToCommunitySuccess = {[weak self] _ in
            //从订单界面来的
            if self?.order == nil {
                // 弹窗提示用户发布成功
                self?.alert(alertTitle: NSLocalizedString("发布成功", comment: ""),leftClosure: {
                    self?.dismissToMainVC()
                })
            }else{
                self?.backProfileVC()
            }
        }
    }
    
    
    //MARK: 闭包回调
    fileprivate func closure(){
        //MARK: 点击分享到时空圈的按钮
        shareButton?.touchButton = {[weak self] _ in
            MobClick.event("shareafterpay")//友盟点击事件
            self?.showCreditView()
        }
        
//        //MARK: 成功投递到时空圈，返回主页
//        viewModel.successClosure = { [weak self] _ in
//            
//            if self?.order == nil {
//                // 弹窗提示用户发布成功
//                self?.alert(alertTitle: NSLocalizedString("发布成功", comment: ""),leftClosure: {
//                    self?.dismissToMainVC()
//                })
//            }else{
//                self?.backProfileVC()
//            }
//        }
        
//        //MARK: 网络请求出错
//        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
//            self?.handelNetWorkError(error, withResult: result)
//        }
        
        //MARK: 点击返回时空的按钮
        backButton?.touchButton = { [weak self] _ in
            if self?.order == nil {
                self?.dismissToMainVC()
            }else{
                self?.backProfileVC(shouldAlert: false)
            }
        }
    }
    
    //MARK: 返回个人主页
    /// 返回个人主页
    ///
    /// - Parameter shouldAlert: 如果是发布成功的，需要弹窗
    fileprivate func backProfileVC(shouldAlert: Bool = true){
        app.containerVC?.dismiss(animated: true, completion: nil)
//        if let vcArray: [UIViewController] = navigationController?.viewControllers{
//            for vc: UIViewController in vcArray{
//                if vc.isKind(of: PMainController.self) == true{
//                    let vc: PMainController = vc as! PMainController
//                    vc.refreshOrderData()//投递成功，刷新数据
//                }
//            }
//        }
//        if shouldAlert == true{
//            // 弹窗提示用户发布成功
//            alert(alertTitle: NSLocalizedString("发布成功", comment: ""),leftClosure: {[weak self] _ in
//                _ = self?.navigationController?.popToRootViewController(animated: true)//返回个人主页
//            })
//        }else{
//            _ = navigationController?.popToRootViewController(animated: true)//返回个人主页
//        }
    }
    
    //MARK: 返回主页
    fileprivate func dismissToMainVC(){
        navigationController?.popToRootViewController(animated: true)
//        let vcArray: [UIViewController] = navigationController!.viewControllers
//        for vc: UIViewController in vcArray{
//            if vc.isKind(of: MUViewController.self){
//                let muVC: MUViewController = vc as! MUViewController
//                muVC.dismissSelf()
//            }
//            //买图，返回社区主页
//            else if vc.isKind(of: MainController.self){
//                // 通知后端，更改买图数量
//                MobClick.event("buy_buySuccess")//友盟点击事件
//                viewModel.changePurchaseCount(originalId: postcard!.originalId)
//                _ = navigationController?.popToRootViewController(animated: true)
//            }
//        }
    }
}
