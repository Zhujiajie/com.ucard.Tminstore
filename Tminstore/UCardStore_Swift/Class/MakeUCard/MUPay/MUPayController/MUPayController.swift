//
//  MUPayController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/1.
//  Copyright © 2016年 ucard. All rights reserved.
//

//MARK:--------------支付界面--------------------

import UIKit
import SnapKit

class MUPayController: BaseViewController {
    
    /// Core Data
    var postcardData: Postcard?
    
    var order: OrderPostcardModel?
    
    /// 支付信息，包括支付渠道和优惠券信息
    var payModel: PayModel?
    
    let viewModel: MUPayViewModel = MUPayViewModel()
    
    var postcard: MUFinalPostcardModel?
    
    /// 地址模型
    var addressModel: AddressModel?
    
    fileprivate var tableView: MUPayTableView?
    
    /// 确认支付的按钮
    fileprivate var confirmButton: MUPayButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        setConfirmButton()// 设置确认支付的按钮
        setTableView()//设置tableView
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("在线支付", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置确认支付的按钮
    fileprivate func setConfirmButton(){
        confirmButton = MUPayButton.confirmPayButton()
        view.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(51/baseHeight)
        })
    }
    
    //MARK: 设置tableView
    fileprivate func setTableView(){
        tableView = MUPayTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(confirmButton!.snp.top)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        //MARK: 点击确认支付按钮，开始请求支付信息
        confirmButton?.touchButton = {[weak self] _ in
            if self?.postcard?.isComeFromPurchase == false{
                MobClick.event("paybutton")//友盟点击事件
            }else{
                MobClick.event("buy_pay")//友盟点击事件
            }
            if self?.order == nil {
                self?.viewModel.requestPayInfo(addressModel: (self?.addressModel)!, andPayModel: (self?.payModel)!)
            }else{
                self?.viewModel.payAgain(payModel: (self?.payModel)!, andOrderNo: (self?.order?.paymentOrderNo)!, andMailOrderNo: (self?.order?.mailOrderNo)!)
            }
        }
        
        //MARK: 网络请求失败
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 支付成功，进入支付成功的界面
        viewModel.successClosure = {[weak self] _ in
            if self?.postcardData != nil {
                self?.viewModel.deleteCoreData(postcardData: (self?.postcardData)!)
            }
            let paySuccessVC: MUPaySuccessController = MUPaySuccessController()
            paySuccessVC.payModel = self?.payModel
            paySuccessVC.postcard = self?.postcard
            paySuccessVC.order = self?.order
            
            _ = self?.navigationController?.pushViewController(paySuccessVC, animated: true)
        }
    }
}

//MARK:-----------------
extension MUPayController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        switch section {
        case 0,1:
            return 1
        default:
            return 2
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell: MUPayTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUPayTableViewCellReuseIdentifier, for: indexPath) as! MUPayTableViewCell
        
        if indexPath.section == 0{
            cell.addressModel = addressModel
        }else{
            cell.priceNeedToPay = payModel!.priceNeedToPay
            cell.indexPath = indexPath
        }
        
        return cell
    }
    
    //MARK: section之间的间距。以及支付cell上方的标题
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch section {
        case 0:
            return 6/baseHeight
        case 1:
            return 28/baseHeight
        default:
            return 42/baseHeight
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2{
            let view: UIView = UIView()
            let label: UILabel = UILabel()
            label.text = NSLocalizedString("选择支付方式", comment: "")
            label.font = UIFont.systemFont(ofSize: 12)
            label.textColor = UIColor(hexString: "B2B2B2")
            view.addSubview(label)
            label.snp.makeConstraints { (make: ConstraintMaker) in
                make.centerY.equalTo(view)
                make.left.equalTo(20/baseWidth)
            }
            return view
        }else{
            return nil
        }
    }
    
    //MARK: 选中cell改变支付渠道
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cells: [MUPayTableViewCell] = tableView.visibleCells as! [MUPayTableViewCell]
        _ = cells.map { (cell: MUPayTableViewCell) in
            cell.isCellSelected = false
        }
        let cell: MUPayTableViewCell = tableView.cellForRow(at: indexPath) as! MUPayTableViewCell
        cell.isCellSelected = true
        if indexPath.row == 0{
            payModel!.payStyle = PayStyle.weChat
        }else{
            payModel!.payStyle = PayStyle.aliPay
        }
    }
}
