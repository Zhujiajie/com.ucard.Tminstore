//
//  MUPayViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/1.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUPayViewModel: BaseViewModel {
    
    
    
    /// 上传订单成功的闭包。可能支付，也可能不支付
    var successClosure: (()->())?
    /// 错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 支付模型
    fileprivate var payModel: PayModel?
    
    fileprivate let dataModel: MUPayDataModel = MUPayDataModel()
    fileprivate let userInfoDataModel: PMainDataModel = PMainDataModel()
    
    //MARK: 上传订单
    func requestPayInfo(addressModel: AddressModel, andPayModel payModel: PayModel) {
        
        showSVProgress(title: nil)
        
        //先尝试获取用户个人信息，成功之后才会开始上传
        userInfoDataModel.getUserInfo(success: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let userDic: [String: Any] = data["user"] as! [String: Any]
                _ = UserInfoModel.initFromDic(dic: userDic)
                self?.dataModel.requestPayInfo(addressModel: addressModel, andPayModel: payModel)
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
                self?.errorClosure?(error, nil)
                self?.dismissSVProgress()
            }, completionClosure: nil)

        //MARK: 上传成功，需要支付的订单
        dataModel.uploadSuccess = {[weak self](result: String) in
            self?.pingppPay(charge: result)
        }
        
        //出错
        dataModel.netWorkErrorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.errorClosure?(error, result)
            self?.dismissSVProgress()
        }
        
        // 完成
        dataModel.completion = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 上传订单成功，去支付
    fileprivate func pingppPay(charge: String){
        let chargeModel: NSObject = charge as NSObject
        
//        print(charge)
        
        Pingpp.createPayment(chargeModel, appURLScheme: appAPIHelp.kAppURLScheme, withCompletion: { [weak self](result: String?, pingError: PingppError?) in
            if result != nil{
//                print(result!)
//                print(pingError!.code)
                switch result!{
                case "success"://成功
                    self?.successClosure?()
                    self?.dataModel.notifyPaymentResult()
                    
                case "cancel":// 取消
                    let message: [String: Any] = ["message": NSLocalizedString("支付被取消", comment: "")]
                    self?.errorClosure?(nil, message)
                    
                default:// 失败
                    let message: [String: Any] = ["message": NSLocalizedString("支付失败", comment: "")]
                    self?.errorClosure?(nil, message)
                }
            }
            if pingError != nil{
                print(pingError!)
            }
        })
    }
    
    //MARK: 二次支付的请求
    func payAgain(payModel: PayModel, andOrderNo orderNo: String, andMailOrderNo mailOrderNo: String){
        
        showSVProgress(title: nil)
        
        //先尝试获取用户个人信息，成功之后才会开始支付
        userInfoDataModel.getUserInfo(success: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let userDic: [String: Any] = data["user"] as! [String: Any]
                _ = UserInfoModel.initFromDic(dic: userDic)
                self?.dataModel.payAgain(payModel: payModel, andOrderNO: orderNo, andMailOrderNo: mailOrderNo)
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
                self?.errorClosure?(error, nil)
                self?.dismissSVProgress()
            }, completionClosure: nil)
        
        //MARK: 上传成功，需要支付的订单
        dataModel.uploadSuccess = {[weak self](result: String) in
            self?.pingppPay(charge: result)
        }
        
        //出错
        dataModel.netWorkErrorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.errorClosure?(error, result)
            self?.dismissSVProgress()
        }
        
        // 完成
        dataModel.completion = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    
    //MARK: 将已付款的AR邮寄订单分享到时空圈
    /// 将已付款的AR邮寄订单分享到时空圈
    ///
    /// - Parameter payModel: 支付模型
    func shareARToCommunity(payModel: PayModel, andIsShareToCommunityAgain again: Bool) {
        
        showSVProgress(title: nil)
        
        dataModel.shareToCommunity(payModel: payModel, andIsShareToCommunityAgain: again, successClosure: { [weak self](result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.successClosure?()
            }else{
                self?.errorClosure?(nil, result)
            }
            }, errorClosure: { [weak self] (error: Error?) in
            self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
//    //MARK: 取消分享到时空圈
//    /// 取消分享到时空圈
//    ///
//    /// - Parameter originalId: 卡片的originalId
//    func cancelRelease(originalId: String){
//        showSVProgress(title: nil)
//        dataModel.cancelRelease(originalId: originalId, successClosure: { [weak self](result: [String : Any]) in
//            if let code: Int = result["code"] as? Int, code == 1000{
//                self?.successClosure?()
//            }else{
//                self?.errorClosure?(nil, result)
//            }
//            }, errorClosure: { [weak self] (error: Error?) in
//                self?.errorClosure?(error, nil)
//        }) { [weak self] _ in
//            self?.dismissSVProgress()
//        }
//    }
    
    //MARK: 发布完成之后，删除coreData数据
    /// 发布完成之后，删除coreData数据
    ///
    /// - Parameter postcardData: Postcard
    func deleteCoreData(postcardData: Postcard){
        dataModel.deletePostcardData(postcardData: postcardData)
    }
    
    //MARK: 买图成功之后，更改买图数量的请求
    /// 买图成功之后，更改买图数量的请求
    ///
    /// - Parameter originalId: 原创ID
    func changePurchaseCount(originalId: String){
        dataModel.changePurchaseNumber(originalId: originalId)
    }
}
