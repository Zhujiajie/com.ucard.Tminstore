//
//  MUPayDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/7/3.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreLocation

class MUPayDataModel: BaseDataModel {
    
    ///上传成功的闭包，返回支付信息
    var uploadSuccess: ((_ result: String)->())?
    /// 免费的订单，无需支付
    var paySuccess: (()->())?
    
    /// 错误的闭包
    var netWorkErrorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 完成的闭包
    var completion: (()->())?
    
    /// 地址对象
    fileprivate var address: AddressModel?
    /// 支付方式
    fileprivate var payModel: PayModel?
    
    //MARK: 向订单提交支付信息
    func requestPayInfo(addressModel: AddressModel, andPayModel aPayModel: PayModel){
        
        self.address = addressModel
        self.payModel = aPayModel
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "orderNO": payModel!.orderNo,
            "amount": payModel!.priceNeedToPay * 100,
            "channel": payModel!.payStyle.rawValue,
            "currency": "cny"
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.mailOrderPayAPI, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String: Any]) in
            
//            print(result)
            
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                if let charge: String = data["charge"] as? String{
//                    print(charge)
                    //获取订单号
                    let orderNO: String = data["orderNo"] as! String
                    self?.payModel?.orderNo = orderNO
                    // 获取ping++订单号
                    let chargeId: String = data["id"] as! String
                    self?.payModel?.chargeId = chargeId
                    
                    self?.uploadSuccess?(charge)//需要支付的订单
                }
            }
//            else if let code: Int = result["code"] as? Int, code == 1004{
//                self?.payModel?.payStyle = PayStyle.coupon
//                self?.paySuccess?()//无需支付的订单
//            }
            else{
                self?.netWorkErrorClosure?(nil, result)
            }
        }, failureClourse: { [weak self] (error: Error?) in
            self?.netWorkErrorClosure?(error, nil)
        }) { [weak self] _ in
            self?.completion?()
        }
    }
    
    //MARK: 二次支付的请求
    /// 二次支付的请求
    ///
    /// - Parameters:
    ///   - payModel: 支付模型
    ///   - orderNo: 订单编号
    func payAgain(payModel: PayModel, andOrderNO orderNo: String, andMailOrderNo mailOrderNo: String){
        
        self.payModel = payModel
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "orderNo": orderNo,
            "mailOrderNo": mailOrderNo,
            "payChannel": payModel.payStyle.rawValue,
            "currency": "cny"
        ] as [String: Any]
        
//        print(parameters)
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.payAgainAPI, ParametersDictionary: parameters, successClosure: { [weak self](result: [String : Any]) in
            
//            print(result)
            
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                if let charge: String = data["charge"] as? String{
//                    print(charge)
                    //获取订单号
                    let orderNO: String = data["orderNo"] as! String
                    self?.payModel?.orderNo = orderNO
                    // 获取ping++订单号
                    let chargeId: String = data["id"] as! String
                    self?.payModel?.chargeId = chargeId
                    self?.uploadSuccess?(charge)//需要支付的订单
                }else{
                    self?.payModel?.payStyle = PayStyle.coupon
                    self?.paySuccess?()//无需支付的订单
                }
            }else{
                self?.netWorkErrorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
            self?.netWorkErrorClosure?(error, nil)
        }) { [weak self] _ in
            self?.completion?()
        }
    }
    
    //MARK: 将已付款的AR邮寄订单分享到时空圈
    /// 将已付款的AR邮寄订单分享到时空圈
    ///
    /// - Parameter payModel: 支付模型
    func shareToCommunity(payModel: PayModel, andIsShareToCommunityAgain again: Bool, successClosure success: ((_ result: [String: Any])->())?, errorClosure failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
        
        var urlStr: String
        var parameters: [String: Any]
        
        if again == true{
            urlStr = appAPIHelp.shareToCommunityAgainAPI
            parameters = [
                "token": getUserToken(),
                "originalId": payModel.originalId,
                "pointPrice": payModel.pointPrice
                ] as [String: Any]
        }else{
            urlStr = appAPIHelp.shareARMailToCommunityAPI
            parameters = [
            "token": getUserToken(),
            "originalId": payModel.originalId,
            "pointPrice": payModel.pointPrice,
            "isOnMarket": "01"
            ] as [String: Any]
        }
        
        sendPOSTRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failure?(error)
        }) { 
            completion?()
        }
    }
    
//    //MARK: 取消发布到时空圈
//    /// 取消发布到时空圈
//    ///
//    /// - Parameters:
//    ///   - originalId: 卡片的originalId
//    ///   - success: 成功的闭包
//    ///   - failure: 失败的闭包
//    ///   - completion: 完成的闭包
//    func cancelRelease(originalId: String, successClosure success: ((_ result: [String: Any])->())?, errorClosure failure: ((_ error: Error?)->())?, completionClosure completion: (()->())?){
//        
//        let parameters: [String: Any] = [
//            "token": getUserToken(),
//            "originalId": originalId
//        ] as [String: Any]
//        
//        sendPUTRequestWithURLString(URLStr: cancelReleaseAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
//            success?(result)
//        }, failureClourse: { (error: Error?) in
//            failure?(error)
//        }) { 
//            completion?()
//        }
//    }
    
    //MARK: 支付完成之后，通知后台支付成功
    /// 支付完成之后，通知后台支付成功
    func notifyPaymentResult(){
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "orderNo": payModel!.orderNo,
            "chargeId": payModel!.chargeId
        ]
        
//        print(parameters)
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.notifyPaymentResultAPI, ParametersDictionary: parameters, successClosure: { (result: [String: Any]) in
//            print(result)
        }, failureClourse: nil, completionClosure: nil)
    }
    
    //MARK: 买图成功之后，更改买图数量
    /// 买图成功之后，更改买图数量
    ///
    /// - Parameter originalId: 原创ID
    func changePurchaseNumber(originalId: String){
        
        let urlString: String = appAPIHelp.changePurchaseCountAPI + "/" + originalId + "/orderTotal"
        
        let parameters: [String: Any] = [
            "token": getUserToken()
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: nil, failureClourse: nil)
    }
}
