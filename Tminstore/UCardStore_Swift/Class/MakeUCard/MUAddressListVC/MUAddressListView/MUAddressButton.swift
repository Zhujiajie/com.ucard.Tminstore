//
//  MUAddressButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/7.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUAddressButton: UIButton {

    /// 点击增加地址按钮的事件
    var touchAddAddressButton: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addTarget(self, action: #selector(touchAddAddressButton(_:)), for: UIControlEvents.touchUpInside)
    }
    
    /// 设置确认地址的按钮
    ///
    /// - Returns: MUAddressButton
    class func confirmAddressButton()-> MUAddressButton{
        
        let confirmAddressButton: MUAddressButton = MUAddressButton()
        
        confirmAddressButton.backgroundColor = UIColor(hexString: "4CDFED")
        
        let str: String = NSLocalizedString("确认", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white] as [String: Any]
        let attStr: NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        
        confirmAddressButton.setAttributedTitle(attStr, for: UIControlState.normal)
        
        return confirmAddressButton
    }
    
    //MARK: 点击增加地址按钮
    func touchAddAddressButton(_ button: UIButton){
        touchAddAddressButton?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
