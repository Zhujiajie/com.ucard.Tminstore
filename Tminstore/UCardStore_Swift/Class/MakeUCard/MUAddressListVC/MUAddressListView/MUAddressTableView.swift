//
//  MUAddressTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/9/7.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import MJRefresh

class MUAddressTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        backgroundColor = UIColor(hexString: "#F4F7F9")
        setTableView()
    }
    
    fileprivate func setTableView(){
        
        self.separatorStyle     = UITableViewCellSeparatorStyle.none
        self.register(MUAddressCell.self, forCellReuseIdentifier: appReuseIdentifier.MUAddressListVCCellReuseIdentifier)
        self.estimatedRowHeight = 82//开启自动计算行高
        self.rowHeight          = UITableViewAutomaticDimension
        self.tableFooterView    = UIView()
        
        //设置下拉刷新
        self.mj_header = BaseAnimationHeader()
        
        self.mj_footer = BaseRefreshFooter()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
