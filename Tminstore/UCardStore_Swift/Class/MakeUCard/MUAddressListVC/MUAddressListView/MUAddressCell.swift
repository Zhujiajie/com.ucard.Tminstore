//
//  MUAddressCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import MGSwipeTableCell
import SnapKit

let deselectAddressCellNotification: String = "deselectAddressCellNotification"

class MUAddressCell: MGSwipeTableCell {
    
    /// 根据cell是否被选中，设置图片
    var addressIsSelected: Bool = false{
        didSet{
            if addressIsSelected == false{
                selectedImageView?.image = UIImage(named: "addressUnselected")
            }else{
                selectedImageView?.image = UIImage(named: "addressSelected")
            }
        }
    }
    
    /// 点击删除按钮的闭包
    var touchDeleteAction: (()->())?
    
    /// 点击编辑的闭包
    var touchEditAction: (()->())?
    
    /// 收件人的姓名
    fileprivate var nameLabel: UILabel?
    /// 手机号码Label
    fileprivate var photoNumberLabel: UILabel?
    /// 收件人的邮政编码
    fileprivate var postCodeLabel: UILabel?
    /// 地址
    fileprivate var addressLabel: UILabel?
    /// 用图片展示cell是否被选中
    fileprivate var selectedImageView: UIImageView?

    /// 地址模型
    var addressModel: AddressModel?{
        didSet{
            setCellData()
        }
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.white
        
        setAddressListCellUI()
        swipeLeftActions()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: deselectAddressCellNotification), object: nil, queue: nil) { [weak self] (_) in
            self?.addressIsSelected = false
        }
    }

    
    //MARK: 设置地址列表cell的UI
    fileprivate func setAddressListCellUI(){
        
        selectedImageView = UIImageView()
        selectedImageView?.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(selectedImageView!)
        selectedImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(10/baseWidth)
            make.size.equalTo(CGSize(width: 16/baseWidth, height: 16/baseWidth))
        })
        
        nameLabel           = UILabel()
        nameLabel?.font          = UIFont.systemFont(ofSize: 14)
        nameLabel?.textColor     = UIColor.black
        nameLabel?.numberOfLines = 1
        contentView.addSubview(nameLabel!)
        nameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(13/baseHeight)
            make.left.equalTo(selectedImageView!.snp.right).offset(12/baseWidth)
            make.height.equalTo(18)
        })
        
//        postCodeLabel           = UILabel()
//        postCodeLabel?.textColor     = UIColor(hexString: "403F3F")
//        postCodeLabel?.font          = UIFont.systemFont(ofSize: 14)
//        postCodeLabel?.numberOfLines = 1
//        contentView.addSubview(postCodeLabel!)
//        postCodeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
//            make.centerY.equalTo(nameLabel!)
//            make.right.equalTo(contentView).offset(-20/baseWidth)
//            make.height.equalTo(nameLabel!)
//        })
        
        photoNumberLabel           = UILabel()
        photoNumberLabel?.font          = UIFont.systemFont(ofSize: 14)
        photoNumberLabel?.textColor     = UIColor.black
        photoNumberLabel?.numberOfLines = 1
        contentView.addSubview(photoNumberLabel!)
        photoNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(nameLabel!)
            make.right.equalTo(contentView).offset(-20/baseWidth)
            make.height.equalTo(nameLabel!)
        })
        
        addressLabel           = UILabel()
        addressLabel?.font          = UIFont.systemFont(ofSize: 14)
        addressLabel?.textColor     = UIColor(hexString: "9D9D9D")
        addressLabel?.numberOfLines = 3
        contentView.addSubview(addressLabel!)
        addressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nameLabel!.snp.bottom).offset(11/baseHeight)
            make.leftMargin.equalTo(nameLabel!)
            make.rightMargin.equalTo(photoNumberLabel!)
            make.bottom.equalTo(contentView).offset(-13/baseHeight)
        })
    }
    
    //MARK: 设置cell的左划动作
    fileprivate func swipeLeftActions(){
        //删除
        let deleteButton: MGSwipeButton = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "deleteAddress"), backgroundColor: UIColor(hexString: "FF3732")) { [weak self] (_) -> Bool in
            self?.touchDeleteAction?()
            return true
        }
        deleteButton.buttonWidth = 82/baseWidth
        //编辑
        let editButton: MGSwipeButton = MGSwipeButton(title: "", icon: #imageLiteral(resourceName: "editAddressCell"), backgroundColor: UIColor(hexString: "D0D0D0")) { [weak self](_) -> Bool in
            self?.touchEditAction?()
            return true
        }
        editButton.buttonWidth = 82/baseWidth
        
        self.rightButtons = [deleteButton, editButton]
        self.rightSwipeSettings.transition = MGSwipeTransition.clipCenter
    }
    
    
    /**
     设置cell的数据
     */
    fileprivate func setCellData() {
        var fullAddressText: NSAttributedString
        if languageCode() == "CN" {
            fullAddressText = NSAttributedString(string: "\(addressModel!.country)\(addressModel!.province)\(addressModel!.detailedAddress)")
        }else{
            fullAddressText = NSAttributedString(string: "\(addressModel!.detailedAddress)\(addressModel!.province)\(addressModel!.country)")
        }
        
        if addressModel?.isDefault == true{
            
            let defaultString: NSAttributedString = NSAttributedString(string: NSLocalizedString("【我的地址】", comment: ""))
            let attStr: NSMutableAttributedString = NSMutableAttributedString(string: defaultString.string + fullAddressText.string)
            
            let range1: NSRange = NSMakeRange(0, defaultString.length)
            let range2: NSRange = NSMakeRange(defaultString.length, fullAddressText.length)
            
            let attDic1: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "DA6829")]
            let attDic2: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 14), NSForegroundColorAttributeName: UIColor(hexString: "9D9D9D")]
            
            attStr.addAttributes(attDic1, range: range1)
            attStr.addAttributes(attDic2, range: range2)
            
            addressLabel?.attributedText = attStr
        }else{
            addressLabel?.text = fullAddressText.string
        }
        
        nameLabel?.text        = addressModel!.mailName
        photoNumberLabel?.text = addressModel!.phoneNumber
        postCodeLabel?.text    = addressModel!.postcode
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        selectedImageView?.image = nil
        nameLabel?.text          = nil
        photoNumberLabel?.text   = nil
        postCodeLabel?.text      = nil
        addressLabel?.text       = nil
        addressLabel?.attributedText = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
}
