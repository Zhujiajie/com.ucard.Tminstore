//
//  MUAddressListDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import Alamofire

class MUAddressListDataModel: BaseDataModel {

    //MARK: 获取用户地址列表
    /// 获取用户地址列表
    ///
    /// - Parameters:
    ///   - success: 成功的闭包
    ///   - failsure: 失败的闭包
    ///   - completion: 完成的闭包
    func getUserAddressList(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
        
        let url: String = appAPIHelp.getUserAddressListAPI + "\(pageIndex)"
        let para: [String: Any] = ["access_token": getUserToken()] as [String: Any]
        sendPOSTRequestWithURLString(URLStr: url, ParametersDictionary: para, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }) {
            completion?()
        }
    }
    
    //MARK: 删除地址的请求
    /// 删除地址的请求
    ///
    /// - Parameters:
    ///   - addressId: 地址ID
    ///   - success: 成功的闭包
    ///   - failsure: 失败的闭包
    ///   - completion: 完成的闭包
    func deleteAddress(addressId: String, successClosure success: ((_ result: [String: Any])->())?, failureClosure failsure: ((_ error: Error?)->())?, completionClosure completion: (()->())?) {
    
        let para: [String: Any] = ["addressId": addressId] as [String: Any]
        
        sendGETRequestWithURLString(URLStr: appAPIHelp.deleteUserAddressAPI, ParametersDictionary: para, urlEncoding: URLEncoding.default, successClosure: { (result: [String: Any]) in
            success?(result)
        }, failureClourse: { (error: Error?) in
            failsure?(error)
        }) { 
            completion?()
        }
    }
}
