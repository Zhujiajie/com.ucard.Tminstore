//
//  MUAddressListViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUAddressListViewModel: BaseViewModel {

    let dataModel: MUAddressListDataModel = MUAddressListDataModel()
    
    var userAddressArray: [AddressModel] = [AddressModel]()
    
    /// 成功获取到地址的闭包
    var getAddressSuccess: (()->())?
    
    /// 成功删除地址的闭包
    var deleteAddressSuccess: ((_ indexPath: IndexPath)->())?
    
    /// 没有更多数据了
    var noMoreData: (()->())?
    
    /// 获得了更多数据
    var getMoreData: ((_ indexSet: IndexSet)->())?
    
    /// 错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    var pageIndex: Int = 1
    
    //MARK: 获取用户地址列表
    /**
     获取用户地址列表
     */
    func getUserAddressList() {
        
        if pageIndex == 1{
//            showSVProgress(title: nil, andMaskType: .none)
            userAddressArray.removeAll()
        }
        
        dataModel.getUserAddressList(pageIndex: pageIndex, successClosure: { [weak self] (result: [String: Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any]        = result["data"] as! [String: Any]
                let addressList: [[String: Any]] = data["addressList"] as! [[String: Any]]
                
                if addressList.isEmpty == true{
                    self?.noMoreData?()
                }else{
                    
                    if self?.pageIndex == 1 {
                        for dic: [String: Any] in addressList{
                            let address: AddressModel = AddressModel.initFromeAddressDic(dic: dic)
                            self?.userAddressArray.append(address)
                        }
                        self?.getAddressSuccess?()
                        
                    }else{//获得了更多的数据
                        
                        var newAddressArray: [AddressModel] = [AddressModel]()
                        
                        for dic: [String: Any] in addressList{
                            let address: AddressModel = AddressModel.initFromeAddressDic(dic: dic)
                            newAddressArray.append(address)
                        }
                        
                        // 获得要插入数据的indexSet
                        let range: NSRange = NSMakeRange((self?.userAddressArray.count)!, newAddressArray.count)
                        let indexSet: IndexSet = IndexSet(integersIn: range.toRange() ?? 0..<0)
                        //传递新数据，并存储新数据
                        self?.userAddressArray.append(contentsOf: newAddressArray)
                        self?.getMoreData?(indexSet)
                    }
                    self?.pageIndex += 1
                }
            }else{
                self?.errorClosure?(nil, result)
            }
        }, failureClosure: { [weak self](error) in
            self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 删除地址的请求
    /**
     删除地址的请求

     */
    func deleteAddress(addressId: String, andIndexPath indexPath: IndexPath) {
        showSVProgress(title: nil)
        dataModel.deleteAddress(addressId: addressId, successClosure: { [weak self](result) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.deleteAddressSuccess?(indexPath)
            }else{
                self?.errorClosure?(nil, result)
            }
        }, failureClosure: { [weak self](error) in
            self?.errorClosure?(error, nil)
        }) { [weak self] _ in
            self?.dismissSVProgress()
        }
    }
}
