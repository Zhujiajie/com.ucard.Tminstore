//
//  MUAddressListController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/29.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import MJRefresh
import SnapKit

class MUAddressListController: BaseViewController {

    /// 是否是从个人设置界面来的
    var isComeFromProfile: Bool = false
    
    /// 是否是从买图流程来的
    var isComeFromPurchase: Bool = false
    
    /// Core Data
    var postcardData: Postcard?
    
    var postcard: MUFinalPostcardModel?
    
    fileprivate let viewModel: MUAddressListViewModel = MUAddressListViewModel()
    
    /// 选中的cell的index
    var selectedCellIndex: IndexPath?{
        didSet{
            if selectedCellIndex != nil{
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
        }
    }
    
    /// 用户选中cell，返回地址信息
    var chooseAddressClosure: ((_ addressID: Int)->())?
    
    /// 确认地址的按钮
    fileprivate var confirmButton: BaseButton?
    
    /// 呈现地址的tableView
    fileprivate var tableView: MUAddressTableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()//设置导航栏
        if isComeFromProfile == false {
            setConfirmButton()//设置确认地址的按钮
        }
        setTableView()//设置tableView
        closures()//各种闭包回调
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView?.mj_header.beginRefreshing()//开始获取用户数据
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        
        var titleString: String
        
        if isComeFromProfile == true{
            titleString = "地址"
        }else{
            titleString = "选择地址"
        }
        
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString(titleString, comment: ""), attributeDic: attDic)
        
        setNavigationBarRightButtonWithTitle(NSLocalizedString("添加", comment: ""))
        navigationItem.rightBarButtonItem?.tintColor = UIColor(hexString: "686868")
    }
    
    //MARK: 设置确认地址的按钮
    fileprivate func setConfirmButton(){
        confirmButton = BaseButton.normalTitleButton(title: NSLocalizedString("选择地址", comment: ""), andTextFont: UIFont.systemFont(ofSize: 16), andTextColorHexString: "FFFFFF", andBackgroundColorHexString: "4CEAED")
        view.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view)
            make.left.right.bottom.equalTo(view)
            make.height.equalTo(51/baseHeight)
        })
    }
    
    //MARK: 设置tableViw
    fileprivate func setTableView(){
        tableView = MUAddressTableView()
        tableView?.delegate = self
        tableView?.dataSource = self
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            if isComeFromProfile{
                make.bottom.equalTo(view)
            }else{
                make.bottom.equalTo(confirmButton!.snp.top)
            }
        })
    }
    
    //MARK: 获取用户地址列表
    fileprivate func getUserAddressList(){
        viewModel.getUserAddressList()
    }
    
    //MARK: 点击导航栏右按钮，添加地址
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        enterEditAddressVC(addressModel: nil)
    }
    
    //MARK: 进入地址编辑界面
    fileprivate func enterEditAddressVC(addressModel: AddressModel?){
        let eaVC: MUEditAddressController = MUEditAddressController()
        eaVC.umengViewName = "新增地址"
        eaVC.addressModel = addressModel
        navigationController?.pushViewController(eaVC, animated: true)
    }
}

//MARK:--------UITableViewDelegate, UITableViewDataSource----------------------
extension MUAddressListController: UITableViewDelegate, UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.userAddressArray.count
    }
    
    //MARK: 设置cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let cell: MUAddressCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUAddressListVCCellReuseIdentifier, for: indexPath) as! MUAddressCell
        
        cell.addressModel = viewModel.userAddressArray[indexPath.section]
        
        if selectedCellIndex == indexPath {
            cell.addressIsSelected = true
        }else{
            cell.addressIsSelected = false
        }
        
        //MARK: 删除cell
        cell.touchDeleteAction = {[weak self] _ in
            let indexPathToDelete: IndexPath = (self?.tableView?.indexPath(for: cell))!
            self?.viewModel.deleteAddress(addressId: (self?.viewModel.userAddressArray[indexPathToDelete.section].addressId)!, andIndexPath: indexPathToDelete)
        }
        
        //MARK: 编辑cell
        cell.touchEditAction = {[weak self] _ in
            self?.enterEditAddressVC(addressModel: self?.viewModel.userAddressArray[indexPath.section])
        }
        
        return cell
    }
    
    //MARK: 选择了cell，获取地址信息
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //选择优惠券，发生震动
        if #available(iOS 10.0, *) {
            let selectionGenerator: UISelectionFeedbackGenerator = UISelectionFeedbackGenerator()
            selectionGenerator.prepare()
            selectionGenerator.selectionChanged()
        }
        tableView.deselectRow(at: indexPath, animated: true)
        let cell: MUAddressCell = tableView.cellForRow(at: indexPath) as! MUAddressCell
        NotificationCenter.default.post(name: Notification.Name(rawValue: deselectAddressCellNotification), object: nil)
        cell.addressIsSelected = true
        selectedCellIndex = indexPath//保存选中的cell的indexPath
    }
    
    //MARK: 设置Cell之间的间距
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 6/baseHeight
        }else{
            return 3/baseHeight
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view: UIView = UIView(frame: CGRect(x: 0, y: 0, width: screenHeight, height: 6))
        view.backgroundColor = UIColor.clear
        return view
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        print(editingStyle.rawValue)
    }
}

extension MUAddressListController{
    
    //MARK: 各种闭包回调
    fileprivate func closures(){
        
        //MARK: 点击了确认地址按钮
        confirmButton?.touchButtonClosure = {[weak self] _ in
            if self?.selectedCellIndex != nil {
                self?.postcard?.addressModel = self?.viewModel.userAddressArray[(self?.selectedCellIndex?.section)!]
                _ = self?.navigationController?.popViewController(animated: true)
            }
        }
        
        //MARK: 设置下拉刷新
        tableView?.mj_header.refreshingBlock = { [weak self]  _ in
            self?.viewModel.pageIndex = 1
            self?.getUserAddressList()
            self?.tableView?.mj_footer.resetNoMoreData()
        }
        
        //MARK: 上拉刷新
        tableView?.mj_footer.refreshingBlock = {[weak self] _ in
            self?.getUserAddressList()
        }
        
        //MARK: 成功获取地址信息
        viewModel.getAddressSuccess = {[weak self] _ in
            self?.tableView?.reloadData()
            self?.tableView?.mj_header.endRefreshing()
        }
        
        //MARK: 网络请求失败
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.tableView?.mj_header.endRefreshing()
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 成功删除地址
        viewModel.deleteAddressSuccess = {[weak self] (indexPath: IndexPath) in
            self?.viewModel.userAddressArray.remove(at: indexPath.section)
            self?.tableView?.beginUpdates()
            self?.tableView?.deleteSections(IndexSet(integer: indexPath.section), with: UITableViewRowAnimation.automatic)
            self?.tableView?.endUpdates()
            if self?.selectedCellIndex == indexPath{
                self?.selectedCellIndex = nil
            }
        }
        
        //MARK: 没有更多数据
        viewModel.noMoreData = {[weak self] _ in
            self?.tableView?.mj_header.endRefreshing()
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
        }
        
        //MARK: 获得了更多数据
        viewModel.getMoreData = {[weak self] (indexSet: IndexSet) in
            self?.tableView?.mj_footer.endRefreshingWithNoMoreData()
            self?.tableView?.beginUpdates()
            self?.tableView?.insertSections(indexSet, with: UITableViewRowAnimation.automatic)
            self?.tableView?.endUpdates()
        }
    }
}
