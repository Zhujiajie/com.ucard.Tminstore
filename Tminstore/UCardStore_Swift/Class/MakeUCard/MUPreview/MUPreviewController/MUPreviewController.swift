//
//  MUPreviewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/27.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

//MARK: -----------------------确认订单的界面---------------------

class MUPreviewController: BaseViewController {
    
    /// Core Data
    var postcardData: Postcard?
    
    var postcard: MUFinalPostcardModel?
    
    var addressModel: AddressModel?
    
    /// 是否已经检查过优惠券数量
    fileprivate var getCouponCount: Bool = false
    
    fileprivate var tableView: MUPreViewTableView?
    
    fileprivate var commitButton: MUPreviewButton?
    
    fileprivate let viewModel: MUPreviewViewModel = MUPreviewViewModel()
    
    fileprivate let couponViewModel: MUCouponViewModel = MUCouponViewModel()
    
    /// 放置明信片正反面图片的视图
    fileprivate var containerView: MUPreviewView?
    
    /// 支付模型
    fileprivate var payModel: PayModel = PayModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()//设置导航栏
        setCommitButton()
        setTableView()
        setContainerView()
        closures()//各种闭包回调
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if getCouponCount == false{
            getCouponCount = true
            couponViewModel.getCouponList()//视图加载完毕后，请求优惠券个数
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("确认订单", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置确认提交的按钮
    fileprivate func setCommitButton(){
        commitButton = MUPreviewButton.commitButton()
        view.addSubview(commitButton!)
        commitButton?.snp.makeConstraints({ (make) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(51/baseHeight)
        })
    }
    
    //MARK: 设置tableView
    fileprivate func setTableView(){
        tableView = MUPreViewTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make) in
            make.top.left.right.equalTo(view)
            make.bottom.equalTo(commitButton!.snp.top)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
    //MARK: 设置底部预览视图
    fileprivate func setContainerView(){
        containerView = MUPreviewView.containerView(finalPostcardModel: postcard!)
        tableView?.tableFooterView = containerView
    }
    
    //MARK: 各种闭包回调
    fileprivate func closures(){
        //MARK: 点击提交订单的事件，先上传
        commitButton?.touchCommitButton = {[weak self] _ in
            if self?.postcard?.isComeFromPurchase == false{
                MobClick.event("commitorder")//友盟点击事件
            }else{
                MobClick.event("buy_confirmOrder")//友盟点击事件
            }
            self?.viewModel.uploadPostcard(postcard: (self?.postcard)!, andAddressModel: (self?.addressModel)!, andPayModel: (self?.payModel)!)
        }
        
        //MARK: 网络请求失败
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 上传订单成功，进入支付成功的界面
        viewModel.successClosure = {[weak self] _ in
            // 待支付金额不为零，进入支付界面
            if self?.payModel.priceNeedToPay != 0.0{
                self?.enterPayVC()
            }else{
                self?.couponFreePay()//金额为0，用优惠券全免支付
            }
        }
        
        //MARK: 优惠券支付成功
        viewModel.couponFreePaySuccess = {[weak self] _ in
            if self?.postcardData != nil {
                self?.viewModel.deleteCoreData(postcardData: (self?.postcardData)!)
            }
            let paySuccessVC: MUPaySuccessController = MUPaySuccessController()
            paySuccessVC.payModel = self?.payModel
            paySuccessVC.postcard = self?.postcard
            _ = self?.navigationController?.pushViewController(paySuccessVC, animated: true)
        }
        
        //MARK: 获取优惠券个数成功
        couponViewModel.totalCoupon = {[weak self] (total: Int, flag: Bool) in
            if let cell: MUPreviewTableViewCell = self?.tableView?.cellForRow(at: IndexPath(row: 0, section: 2)) as? MUPreviewTableViewCell{
                if total != 0{
                    cell.couponContent = "\(total)" + NSLocalizedString("张优惠券可用", comment: "")
                }else if flag == true{
                    cell.couponContent = "1" + NSLocalizedString("张优惠券可用", comment: "")
                }
            }
        }
    }
    
    //MARK: 进入支付界面
    fileprivate func enterPayVC(){
        let payVC: MUPayController = MUPayController()
        payVC.postcard = postcard
        payVC.postcardData = postcardData
        payVC.payModel = payModel
        payVC.addressModel = addressModel
        _ = navigationController?.pushViewController(payVC, animated: true)
        CFRunLoopWakeUp(CFRunLoopGetCurrent())
    }
    
    //MARK: 优惠券全免支付
    fileprivate func couponFreePay(){
        payModel.payStyle = PayStyle.coupon
        if postcardData != nil {
            viewModel.deleteCoreData(postcardData: postcardData!)
        }
        let paySuccessVC: MUPaySuccessController = MUPaySuccessController()
        paySuccessVC.payModel = payModel
        paySuccessVC.postcard = postcard
        _ = navigationController?.pushViewController(paySuccessVC, animated: true)
    }
}

extension MUPreviewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MUPreviewTableViewCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUPreviewTableViewCellReuseIdentifier, for: indexPath) as! MUPreviewTableViewCell
        
        switch indexPath.section {
        case 0:
            cell.addressModel = addressModel
        case 2:
            if payModel.couponModel != nil{
                cell.couponContent = payModel.couponModel!.couponContent
            }else{
                cell.couponContent = NSLocalizedString("未选择优惠券", comment: "")
            }
        case 3:
            cell.priceNeedToPay = payModel.priceNeedToPay
        default:
            break
        }
        
        cell.cellIndexPath = indexPath//设置UI
        
        return cell
    }
    
    //MARK: 点击地址栏，返回选择地址的界面。点击优惠券栏，则去优惠券界面
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0{
            _ = navigationController?.popViewController(animated: true)
        }else if indexPath.section == 2{
            let couponVC: MUCouponVC = MUCouponVC()
            _ = navigationController?.pushViewController(couponVC, animated: true)
            //MARK: 使用优惠券成功，返回刷新数据
            couponVC.useCouponSuccess = {[weak self] (coupon: CouponModel) in
                self?.payModel.couponModel = coupon
//                self?.payModel.priceNeedToPay = postcardPrice * coupon.couponPromotion
                self?.tableView?.reloadData()
            }
        }
    }
    
    //MARK: 设置cell之间的间距
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 || section == 1{
            return 9/baseHeight
        }else{
            return 3/baseHeight
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view: UIView = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
}
