//
//  MUPreviewButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/30.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUPreviewButton: UIButton {

    /// 点击提交按钮的事件
    var touchCommitButton: (()->())?
    
    /// 点击播放按钮的事件
    var touchPlayButton: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //MARK: 提交的按钮
    /**
     提交订单的按钮
     
     - returns: MUPreviewButton
     */
    class func commitButton()->MUPreviewButton{
        
        let commitButton: MUPreviewButton = MUPreviewButton()
        
        commitButton.backgroundColor = UIColor(hexString: "4CDFED")
        
        let str: String    = NSLocalizedString("提交订单", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white] as [String: Any]
        let attStr:NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        
        commitButton.setAttributedTitle(attStr, for: UIControlState.normal)
        commitButton.addTarget(commitButton, action: #selector(commitButton.touchCommitButton(_:)), for: UIControlEvents.touchUpInside)
        
        return commitButton
    }
    
    //MARK: 进入播放界面的按钮
    /**
     进入播放界面的按钮
     
     - returns: MUPreviewButton
     */
    class func playButton()-> MUPreviewButton{
        let playButton: MUPreviewButton = MUPreviewButton()
        playButton.setImage(UIImage(named: "playButton"), for: UIControlState.normal)
        playButton.addTarget(playButton, action: #selector(touchPlayButton(_:)), for: UIControlEvents.touchUpInside)
        return playButton
    }
    
    //MARK: 点击确认提交按钮的事件
    func touchCommitButton(_ button: UIButton) {
        touchCommitButton?()
    }
    
    //MARK: 点击播放按钮的事件
    func touchPlayButton(_ button: UIButton) {
        touchPlayButton?()
        self.isHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
