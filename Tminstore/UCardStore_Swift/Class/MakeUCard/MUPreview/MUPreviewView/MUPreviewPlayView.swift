//
//  MUPreviewPlayView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/31.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUPreviewPlayView: UIVisualEffectView {
    
    /// 点击退出播放界面
    var tapToDismiss: (()->())?
    
    /// 视频地址
    var videoPath: URL?
    
    /// 播放器
    var player: ZTPlayer?
    
    var pauseButtonImageView: UIImageView?
    var progressView: UIProgressView?
    var leftTimeLabel: UILabel?
    
    fileprivate var timer: Timer?
    
    override init(effect: UIVisualEffect?) {
        
        let blur: UIBlurEffect = UIBlurEffect(style: .dark)
        super.init(effect: blur)
        self.contentView.isUserInteractionEnabled = true
    }
    
    //MARK: 设置播放器视图
    class func playerView(videoPath: URL)->MUPreviewPlayView{
        
        let playerView: MUPreviewPlayView = MUPreviewPlayView()
        playerView.videoPath = videoPath
        
        playerView.setPlayer()
        
        //添加点击退出的手势
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: playerView, action: #selector(tapToDismiss(_:)))
        tap.numberOfTapsRequired = 1
        playerView.addGestureRecognizer(tap)
        playerView.isHidden = true
        
        return playerView
    }
    
    //MARK: 设置播放器
    fileprivate func setPlayer(){
        
        if player == nil {
            
            player = ZTPlayer()
            
            player?.view.frame = CGRect(x: 0, y: 107/baseHeight, width: screenWidth, height: 265/baseWidth)
            self.contentView.addSubview(player!.view)
            
            player?.view.backgroundColor = UIColor.black
            player?.fillMode = AVLayerVideoGravityResizeAspectFill//全屏播放
            player?.view.alpha = 0
            
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToPlay(_:)))
            tap.numberOfTapsRequired = 1
            player?.view.addGestureRecognizer(tap)
            
            setprogressViewPauseButtonTimeLable()//设置播放的控件
            setTimer()//设置计时器
        }
        
        player?.url = videoPath//设置资源
        player?.playbackLoops = true//循环播放
        
        //监控app是否进入后台
        app.appDidEnterBackground = {[weak self] _ in
            if self?.pauseButtonImageView != nil {
                self?.pauseButtonImageView?.isHidden = false
            }
        }
    }
    
    //MARK: 设置进度条和时间标签
    fileprivate func setprogressViewPauseButtonTimeLable(){
        
        //暂停播放的按钮视图
        pauseButtonImageView = UIImageView(image: UIImage(named: "play"))
        self.contentView.insertSubview(pauseButtonImageView!, aboveSubview: player!.view)
        pauseButtonImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(player!.view.snp.center)
        })
        pauseButtonImageView?.isHidden = true
        
        //播放进度
        if leftTimeLabel == nil {
            
            //进度条
            progressView = UIProgressView(frame: CGRect(x: 0, y: Y(player!.view) + HEIGHT(player!.view) + 1.5, width: screenWidth, height: 2))
            progressView?.transform = CGAffineTransform(scaleX: 1.0, y: 2.5)
            self.contentView.addSubview(progressView!)
            
            progressView?.progress = 0.0
            progressView?.progressTintColor = UIColor(hexString: "41D0DF")
            
            leftTimeLabel = UILabel(frame: CGRect(x: 0, y: Y(progressView!) + 8.5, width: 40, height: 18))
            leftTimeLabel?.font = UIFont.systemFont(ofSize: 13)
            leftTimeLabel?.textColor = UIColor(hexString: "8B8B8B")
            self.contentView.addSubview(leftTimeLabel!)
            leftTimeLabel?.center.x = screenWidth/2
        }
        
        leftTimeLabel?.alpha  = 0
        progressView?.alpha   = 0
    }
    
    //MARK: 设置计时器
    fileprivate func setTimer(){
        timer = Timer(timeInterval: 0.1, target: self, selector: #selector(updatePlayerTime(_:)), userInfo: nil, repeats: true)
        RunLoop.current.add(timer!, forMode: RunLoopMode.defaultRunLoopMode)
    }
    
    //MARK: 通过计时器获得播放进度
    func updatePlayerTime(_ timer: Timer){
        
        if player == nil || progressView == nil {return}
        //设置进度条的进度
        let process = Float((player?.currentTime)!/(player?.maximumDuration)!)
        progressView?.setProgress(process, animated: false)
        var seconds: Double = player!.currentTime
        if seconds < 0 {
            seconds = 0.0
        }
        leftTimeLabel?.text = String(format: "00:%02.0f", seconds)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 点击手势
extension MUPreviewPlayView{
    
    // MARK:处理点击手势，点击播放或者暂停
    func tapToPlay(_ gestureRecognizer: UITapGestureRecognizer) {
        
        switch (player!.playbackState.rawValue) {
        case PlaybackState.stopped.rawValue:
            player?.playFromBeginning()
        case PlaybackState.paused.rawValue:
            player?.playFromCurrentTime()
        case PlaybackState.playing.rawValue:
            player?.pause()
        case PlaybackState.failed.rawValue:
            player?.pause()
        default:
            player?.pause()
        }
        
        //播放暂停时，播放
        if player?.playbackState.rawValue == PlaybackState.paused.rawValue {
            pauseButtonImageView?.isHidden = false
        }else{
            pauseButtonImageView?.isHidden = true
        }
    }
    
    //MARK: 点击播放视图的其他地方，退出
    func tapToDismiss(_ tap: UITapGestureRecognizer){
        tapToDismiss?()
    }
}

//MARK: 对外开放的接口
extension MUPreviewPlayView{
    
    //MARK: 隐藏pauseButtonImageView
    /**
     隐藏pauseButtonImageView
     */
    func hidePauseButtonImageView(){
        if pauseButtonImageView != nil {
            pauseButtonImageView?.isHidden = true
        }
    }
    
    //MARK:销毁播放器
    /**
     销毁播放器
     */
    func firePlayer() {
        
        timer?.invalidate()
        player?.stop()
        
        delay(seconds: 0.5, completion: {[weak self] _ in
            
            self?.isHidden = true
            self?.player                      = nil
            self?.pauseButtonImageView?.removeFromSuperview()
            self?.leftTimeLabel?.removeFromSuperview()
            self?.progressView?.removeFromSuperview()
            
            })
    }
}
