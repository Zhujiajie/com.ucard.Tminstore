//
//  MUPreViewTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/25.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUPreViewTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        backgroundColor = UIColor(hexString: "#F4F7F9")
        register(MUPreviewTableViewCell.self, forCellReuseIdentifier: appReuseIdentifier.MUPreviewTableViewCellReuseIdentifier)
        estimatedRowHeight = 101
        separatorStyle = UITableViewCellSeparatorStyle.none//去除分割线
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
