//
//  MUPreviewView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/30.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUPreviewView: UIView {

    /// 明信片模型
    var finalPostcardModel: MUFinalPostcardModel?
    
    var frontImageView: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //MARK: 设置containerView
    /**
     设置非AR卡片的containerView
     
     - returns: MUPreviewView
     */
    class func containerView(finalPostcardModel: MUFinalPostcardModel)-> MUPreviewView{
        
        let containerView: MUPreviewView = MUPreviewView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 239/baseWidth + 16/baseHeight))
        
        containerView.backgroundColor = UIColor(hexString: "#F4F7F9")
        containerView.finalPostcardModel = finalPostcardModel
        
        containerView.setImageViews()
        
        return containerView
    }

    //MARK: 设置ImageView
    fileprivate func setImageViews(){
        
        frontImageView = UIImageView()
        frontImageView?.backgroundColor = UIColor.white
        frontImageView?.contentMode = UIViewContentMode.scaleToFill
        self.addSubview(frontImageView!)
        frontImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 8/baseHeight, left: 9/baseWidth, bottom: 8/baseHeight, right: 9/baseWidth))
        })
        setData()
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        if let image: UIImage = finalPostcardModel?.frontImage{
            frontImageView?.image = image
        }else{
            frontImageView?.sd_setImage(with: finalPostcardModel?.buyFrontImageURL)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
