//
//  MUPreviewTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/25.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUPreviewTableViewCell: UITableViewCell {
    /// 地址信息
    var addressModel: AddressModel?
    /// 优惠券信息
    var couponContent: String = ""{
        didSet{
            if couponContent != ""{
                priceLabel?.text = couponContent
            }
        }
    }
    /// 需要支付的金额
    var priceNeedToPay: Double = 0.0
    
    var cellIndexPath: IndexPath?{
        didSet{
            switch cellIndexPath!.section{
            case 0:
                cellStyle = PreviewCellStyle.address
            case 1:
                cellStyle = PreviewCellStyle.originPrice
            case 2:
                cellStyle = PreviewCellStyle.coupon
            default:
                cellStyle = PreviewCellStyle.price
            }
        }
    }
    
    fileprivate var cellStyle: PreviewCellStyle?{
        didSet{
            setCellUI()
        }
    }
    
    var payModel: PayModel?
    
    /// 收件人姓名
    fileprivate var nameLabel: UILabel?
    /// 手机号的Label
    fileprivate var photoNumberLabel: UILabel?
    /// 邮编的Label
    fileprivate var postcodeLabel: UILabel?
    /// 详细地址的label
    fileprivate var detialAddressLabel: UILabel?
    
    /// Icon
    fileprivate var iconImageView: UIImageView?
    /// 标题Label
    fileprivate var titleLabel: UILabel?
    /// 价格Label
    fileprivate var priceLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    //MARK: 设置cell的UI
    fileprivate func setCellUI(){
        switch cellStyle! {
        case PreviewCellStyle.address:
            setAddressCellUI()
        case PreviewCellStyle.originPrice:
            setOriginPriceCell()
        case PreviewCellStyle.coupon:
            setCouponCell()
        default:
            setPriceNeedToPurchaseCell()
        }
    }
    
    //MARK: 设置地址的Cell
    fileprivate func setAddressCellUI(){
        
        accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        selectionStyle = UITableViewCellSelectionStyle.default
        
        nameLabel            = UILabel()
        nameLabel?.textColor = UIColor(hexString: "0B0B0B")
        nameLabel?.font      = UIFont.systemFont(ofSize: 17)
        nameLabel?.text      = addressModel!.mailName
        contentView.addSubview(nameLabel!)
        nameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(17/baseHeight)
            make.left.equalTo(contentView).offset(20/baseWidth)
            make.height.equalTo(20)
        })
        
        postcodeLabel            = UILabel()
        postcodeLabel?.textColor = UIColor(hexString: "403F3F")
        postcodeLabel?.font      = UIFont.systemFont(ofSize: 14)
        postcodeLabel?.text      = addressModel?.postcode
        contentView.addSubview(postcodeLabel!)
        postcodeLabel?.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(19/baseHeight)
            make.right.equalTo(contentView)
            make.height.equalTo(18)
        }
        
        photoNumberLabel = UILabel()
        photoNumberLabel?.textColor = UIColor(hexString: "393939")
        photoNumberLabel?.font = UIFont.systemFont(ofSize: 16)
        photoNumberLabel?.text = addressModel?.phoneNumber
        contentView.addSubview(photoNumberLabel!)
        photoNumberLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nameLabel!.snp.bottom).offset(7/baseHeight)
            make.leftMargin.equalTo(nameLabel!)
        })
        
        detialAddressLabel                = UILabel()
        detialAddressLabel?.textColor     = UIColor(hexString: "403F3F")
        detialAddressLabel?.font          = UIFont.systemFont(ofSize: 14)
        detialAddressLabel?.text          = addressModel!.country + addressModel!.province + addressModel!.detailedAddress
        detialAddressLabel?.numberOfLines = 3
        contentView.addSubview(detialAddressLabel!)
        detialAddressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(photoNumberLabel!.snp.bottom).offset(7/baseHeight)
            make.leftMargin.equalTo(nameLabel!)
            make.right.equalTo(contentView)
            make.bottom.equalTo(contentView).offset(-16/baseHeight)
        })
    }
    
    //MARK: 设置原价cell
    fileprivate func setOriginPriceCell(){
        
        accessoryType = UITableViewCellAccessoryType.none
        selectionStyle = UITableViewCellSelectionStyle.none
        
        iconImageView                = UIImageView(image: #imageLiteral(resourceName: "originPriceIcon"))
        iconImageView?.contentMode   = UIViewContentMode.scaleAspectFit
        iconImageView?.clipsToBounds = true
        contentView.addSubview(iconImageView!)
        iconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(19/baseWidth)
            make.size.equalTo(CGSize(width: 34/baseWidth, height: 29/baseWidth))
            make.top.equalTo(contentView).offset(13/baseHeight)
            make.bottom.equalTo(contentView).offset(-13/baseHeight)
        })
        
        titleLabel            = UILabel()
        titleLabel?.textColor = UIColor(hexString: "ABABAB")
        titleLabel?.font      = UIFont.systemFont(ofSize: 14)
        titleLabel?.text      = NSLocalizedString("时记卡片定价", comment: "")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(iconImageView!.snp.right).offset(21/baseWidth)
            make.height.equalTo(20/baseHeight)
        })
        
        priceLabel = UILabel()
        priceLabel?.textColor = UIColor(hexString: "777777")
        priceLabel?.font = UIFont.systemFont(ofSize: 14)
        priceLabel?.text = String(format: "¥ %.2f", postcardPrice)//暂时定价19.00
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-35/baseWidth)
            make.height.equalTo(titleLabel!)
        })
    }
    
    //MARK: 设置优惠券的cell
    fileprivate func setCouponCell(){
        
        accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        selectionStyle = UITableViewCellSelectionStyle.default
        
        iconImageView                = UIImageView(image: #imageLiteral(resourceName: "couponIcon"))
        iconImageView?.contentMode   = UIViewContentMode.scaleAspectFit
        iconImageView?.clipsToBounds = true
        contentView.addSubview(iconImageView!)
        iconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(19/baseWidth)
            make.size.equalTo(CGSize(width: 34/baseWidth, height: 29/baseWidth))
            make.top.equalTo(contentView).offset(13/baseHeight)
            make.bottom.equalTo(contentView).offset(-13/baseHeight)
        })
        
        titleLabel            = UILabel()
        titleLabel?.textColor = UIColor(hexString: "ABABAB")
        titleLabel?.font      = UIFont.systemFont(ofSize: 14)
        titleLabel?.text      = NSLocalizedString("优惠券", comment: "")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(iconImageView!.snp.right).offset(21/baseWidth)
            make.height.equalTo(20/baseHeight)
        })
        
        priceLabel = UILabel()
        priceLabel?.textColor = UIColor(hexString: "DB9C66")
        priceLabel?.font = UIFont.systemFont(ofSize: 14)
        priceLabel?.text = couponContent
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView)
            make.height.equalTo(titleLabel!)
        })
    }
    
    //MARK:需要支付金额的cell
    fileprivate func setPriceNeedToPurchaseCell(){
        
        accessoryType = UITableViewCellAccessoryType.none
        selectionStyle = UITableViewCellSelectionStyle.none
        
        titleLabel            = UILabel()
        titleLabel?.textColor = UIColor(hexString: "ABABAB")
        titleLabel?.font      = UIFont.systemFont(ofSize: 14)
        titleLabel?.text      = NSLocalizedString("待支付", comment: "")
        contentView.addSubview(titleLabel!)
        titleLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(contentView).offset(20/baseWidth)
            make.height.equalTo(20/baseHeight)
            make.top.equalTo(contentView).offset(19/baseHeight)
            make.bottom.equalTo(contentView).offset(-19/baseHeight)
        })
        
        priceLabel = UILabel()
        priceLabel?.textColor = UIColor(hexString: "DB9C66")
        priceLabel?.font = UIFont.systemFont(ofSize: 14)
        priceLabel?.text = String(format: "%.2f", priceNeedToPay)
        contentView.addSubview(priceLabel!)
        priceLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-35/baseWidth)
            make.height.equalTo(titleLabel!)
        })
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel?.removeFromSuperview()
        photoNumberLabel?.removeFromSuperview()
        postcodeLabel?.removeFromSuperview()
        detialAddressLabel?.removeFromSuperview()
        iconImageView?.removeFromSuperview()
        titleLabel?.removeFromSuperview()
        priceLabel?.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
