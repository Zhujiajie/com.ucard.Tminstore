//
//  MUPreviewAnimationModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/8/30.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUPreviewAnimationModel: BaseAnimationModel {

    var superView: UIView?
    var playerContainerView: MUPreviewPlayView?
    
    //MARK: 播放器出现的动画
    /**
     点击了播放按钮，出现播放界面
     */
    func playerAnimationAppear() {
        
        playerContainerView?.isHidden = false
        playerContainerView?.snp.updateConstraints({ (make: ConstraintMaker) in
            make.size.equalTo(CGSize(width: screenWidth, height: screenHeight + nav_statusHeight))
        })
        autoLayoutAnimation(view: superView!, withDelay: 0)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            
            self.playerContainerView!.player?.view.alpha = 1
            self.playerContainerView!.leftTimeLabel!.alpha = 1
            self.playerContainerView!.progressView!.alpha = 1
            
        }) { (_) in
            self.playerContainerView!.player?.playFromBeginning()
        }
    }
    
    //MARK: 退出播放界面
    /**
     退出播放界面
     */
    func dismissPlayer(complete: (()->())?) {
        
        playerContainerView?.player?.stop()
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            
            self.playerContainerView?.player?.view.alpha = 0
            self.playerContainerView?.leftTimeLabel?.alpha = 0
            self.playerContainerView?.progressView?.alpha = 0
            
        }) { (_) in
            
            self.playerContainerView?.snp.remakeConstraints({ (make: ConstraintMaker) in
                make.center.equalTo(self.superView!.snp.center)
                make.width.equalTo(1)
                make.height.equalTo(1)
            })
            self.autoLayoutAnimation(view: self.superView!, withDelay: 0.0)
            
            complete?()
        }
    }
}

