//
//  MUPreviewViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUPreviewViewModel: BaseViewModel {
    
    /// 上传订单成功的闭包。可能支付，也可能不支付
    var successClosure: (()->())?
    
    /// 用优惠券支付成功
    var couponFreePaySuccess: (()->())?
    
    /// 错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    fileprivate let dataModel: MUPreviewDataModel = MUPreviewDataModel()
    fileprivate let userInfoDataModel: PMainDataModel = PMainDataModel()
    fileprivate let payDataModel: MUPayDataModel = MUPayDataModel()
    
    //MARK: 上传订单
    func uploadPostcard(postcard: MUFinalPostcardModel, andAddressModel addressModel: AddressModel, andPayModel payModel: PayModel) {
        
        showSVProgress(title: nil)
        
        //先尝试获取用户个人信息，成功之后才会开始上传
        userInfoDataModel.getUserInfo(success: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let userDic: [String: Any] = data["user"] as! [String: Any]
                _ = UserInfoModel.initFromDic(dic: userDic)
                self?.dataModel.uploadPostcard(postcardModel: postcard, andAddressModel: addressModel, andPayModel: payModel)
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
                self?.errorClosure?(error, nil)
                self?.dismissSVProgress()
            }, completionClosure: nil)
        
        //MARK: 上传成功，需要支付的订单
        dataModel.uploadSuccess = {[weak self] _ in
            self?.successClosure?()
        }
        
        //出错
        dataModel.networkErrorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            DispatchQueue.main.async {
                self?.errorClosure?(error, result)
                self?.dismissSVProgress()
            }
        }
        
        // 完成
        dataModel.completion = {[weak self] _ in
            DispatchQueue.main.async {
                self?.dismissSVProgress()
            }
        }
    }
    
//    //MARK: 优惠券支付
//    func couponFreePay(addressModel: AddressModel, andPayModel payModel: PayModel){
//        
//        showSVProgress(title: nil)
//        
//        payDataModel.requestPayInfo(addressModel: addressModel, andPayModel: payModel)
//        
//        // 优惠券全免支付成功
//        payDataModel.paySuccess = {[weak self] _ in
//            self?.couponFreePaySuccess?()
//        }
//        // 出错
//        payDataModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
//            self?.errorClosure?(error, result)
//        }
//        // 完成
//        payDataModel.completion = {[weak self] _ in
//            self?.dismissSVProgress()
//        }
//    }
    
    //MARK: 发布完成之后，删除coreData数据
    /// 发布完成之后，删除coreData数据
    ///
    /// - Parameter postcardData: Postcard
    func deleteCoreData(postcardData: Postcard){
        dataModel.deletePostcardData(postcardData: postcardData)
    }
}
