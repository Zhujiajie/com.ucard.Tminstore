//
//  MUPreviewDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/18.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import CoreLocation

class MUPreviewDataModel: BaseDataModel {

    ///上传成功的闭包，返回支付信息
    var uploadSuccess: (()->())?
    
    /// 错误的闭包
    var networkErrorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 完成的闭包
    var completion: (()->())?
    
    /// 明信片对象
    fileprivate var postcard: MUFinalPostcardModel?
    /// 地址对象
    fileprivate var address: AddressModel?
    /// 支付方式
    fileprivate var payModel: PayModel?
    
    //MARK: 上传明信片订单
    /// 上传明信片订单
    ///
    /// - Parameters:
    ///   - postcardModel: 明信片对象
    ///   - addressModel: 地址模型
    ///   - payChannel: 支付渠道
    ///   - couponModel: 是否使用了优惠券
    func uploadPostcard(postcardModel: MUFinalPostcardModel, andAddressModel addressModel: AddressModel, andPayModel payModel: PayModel){
        
        postcard      = postcardModel
        address       = addressModel
        
        self.payModel = payModel
        
        postcard?.frontImageUUID   = UUID().uuidString
        postcard?.backImageUUID    = UUID().uuidString
        postcard?.addressImageUUID = UUID().uuidString
        postcard?.videoID          = UUID().uuidString
        
        //如果是买图。则只提交背面图片
        if postcard?.isComeFromPurchase == false{
            uploadFronImage()
        }else{
            uploadBackImage()
        }
    }
    
    //MARK: 上传正面图片
    fileprivate func uploadFronImage(){
        
        guard let imageData: Data = UIImagePNGRepresentation(postcard!.frontImage!) else{
            networkErrorClosure?(nil, nil)
            return
        }
        
        uploadToUpyun(data: imageData, andSaveKey: frontImageUpun + "\(postcard!.frontImageUUID).png", andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.uploadBackImage()
            }, failClosure: { [weak self] (error: Error?, _, _) in
            self?.networkErrorClosure?(error, nil)
            }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                self?.uploadImageProgress(percent: (completeCount/totalCount / Float(2)))
        })
    }
    
    //MARK: 上传背面图片
    fileprivate func uploadBackImage(){
        
        guard let imageData: Data = UIImagePNGRepresentation(postcard!.backImage!)  else{
            networkErrorClosure?(nil, nil)
            return
        }
        
        uploadToUpyun(data: imageData, andSaveKey: backImageUpun + "\(postcard!.backImageUUID).png", andParameters: nil, successClosure: { [weak self] (_, _) in
            //判读是否是买图
            if self?.postcard?.isComeFromPurchase == false{
                self?.uploadVideo()
            }else{
                self?.uploadOrder()
            }
            }, failClosure: { [weak self] (error: Error?, _, _) in
            self?.networkErrorClosure?(error, nil)
            }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                if self?.postcard?.isComeFromPurchase == false{
                    self?.uploadImageProgress(percent: (completeCount/totalCount / Float(2)) + 0.5)
                }else{
                    self?.uploadImageProgress(percent: completeCount/totalCount)
                }
        })
    }
    
//    //MARK: 上传地址图片
//    fileprivate func uploadAddressImage(){
//        
//        let upyun: UpYun = UpYun()
//        
//        upyun.uploadFile(postcard!.addressImage!, saveKey: addressImageUpun + "\(postcard!.addressImageUUID).png")
//        
//        //上传成功，上传背面图片
//        upyun.successBlocker = { [weak self] _ in
//            self?.uploadVideo()
//        }
//        //上传失败
//        upyun.failBlocker = { [weak self](error: Error?) in
//            self?.errorClosure?(error, nil)
//        }
//        //上传的进度
//        upyun.progressBlocker = {[weak self](percent: CGFloat, _) in
//            self?.uploadImageProgress(percent: (percent / 3) + 0.66)
//        }
//    }
    
    //MARK: 上传视频到又拍云
    func uploadVideo(){
        
        do{
            let videoData: Data = try Data(contentsOf: postcard!.videoPath!, options: [])
            
            let parameters: [String: Any] = [
                "name": "naga",
                "type": "video",
                "avopts": "/wmImg/L2lPU0ltYWdlcy93YXRlcm1hcmswMS5wbmc=/wmGravity/northwest/wmDx/20/wmDy/15",
                "save_as": videoUpun + "\(postcard!.videoID).mp4"
            ]
            
            uploadToUpyun(data: videoData, andSaveKey: videoUpun + "\(postcard!.videoID).mp4", andParameters: ["apps": [parameters]], successClosure: { [weak self] (_, _) in
                self?.uploadOrder()
                }, failClosure: { [weak self] (error: Error?, _, _) in
                self?.networkErrorClosure?(error, nil)
                }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                    self?.uploadVideoProgress(percent: completeCount/totalCount)
            })
            
        }catch{networkErrorClosure?(error, nil)}
    }
    
//    //MARK: 上传进度
//    fileprivate func uploadImageProgress(percent: Float){
//        SVProgressHUD.showProgress(percent, status: NSLocalizedString("上传图片中", comment: ""))
//    }
//    
//    //MARK: 上传视频的进度
//    fileprivate func uploadVideoProgress(percent: Float){
//        SVProgressHUD.showProgress(percent, status: NSLocalizedString("上传AR视频中", comment: ""))
//    }
    
    //MARK: 上传订单
    fileprivate func uploadOrder(){
        
        var longitude: Double
        var latitude: Double
        
        if let coordinate: CLLocationCoordinate2D = postcard?.photoLocation?.coordinate {
            longitude = coordinate.longitude
            latitude = coordinate.latitude
        }else{
            longitude = 0.0
            latitude = 0.0
        }
        
        var couponId: String
        var payChannal: String//支付方式
        if payModel!.couponModel?.couponCode != nil{
            couponId = payModel!.couponModel!.couponCode
            payChannal = "00"
        }else{
            couponId = ""
            payChannal = ""
        }

        var country: String
        var state: String
        var city: String
        if languageCode() == "CN"{
            country = postcard!.countryInCn
            state = postcard!.stateInCN
            city = postcard!.cityInCN
        }else{
            country = postcard!.countryInEn
            state = postcard!.stateInEn
            city = postcard!.cityInEN
        }
        
        var parameters: [String: Any]
        
        //买图
        if postcard?.isComeFromPurchase == false{
            
            parameters = [
                
                "token": getUserToken(),
                "frontImageUrl": postcard!.frontImageURLPath,
                "backImageUrl": postcard!.backImageURLPath,
                "backAddressImageUrl": postcard!.addressImageURLPath,
                "videoUrl": postcard!.videoURLPath,
                "frontImageRemark": postcard!.frontImageRemark,
                "backImageRemark": postcard!.backImageRemark,
                "videoRemark": postcard!.videoRemark,
                "longitude": longitude,
                "latitude": latitude,
                "addressId": address!.addressId,
                "country": country,
                "province": state,
                "city": city,
                "district": "",
                "detailedAddress": address!.detailedAddress,
                "backContent": postcard!.message,
                "orderAmount": payModel!.priceNeedToPay,
                "quantity": 1,
                "payChannel": payChannal,
                "currency": "cny",
                "couponId": couponId,
                "customerType": "C"
                
                ] as [String: Any]
        }
        //正常订单
        else{
            parameters = [
                
                "token": getUserToken(),
                "originalId": postcard!.originalId,
                "backImageUrl": postcard!.backImageURLPath,
                "longitude": longitude,
                "latitude": latitude,
                "addressId": address!.addressId,
                "country": country,
                "province": state,
                "city": city,
                "district": "",
                "backContent": postcard!.message,
                "orderAmount": payModel!.priceNeedToPay,
                "quantity": 1,
                "payChannel": payChannal,
                "currency": "cny",
                "couponId": couponId
                
                ] as [String: Any]
        }
        
//        print(parameters)
        
        //接口地址
        var urlString: String
        if postcard?.isComeFromPurchase == false{
            urlString = appAPIHelp.uploadMainOrderAPI
        }else{
            urlString = appAPIHelp.purchaseConfirmOrderAPI
        }
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String: Any]) in
            
//            print(result)
            
            if let code: Int = result["code"] as? Int, code == 1000{
                
                let data: [String: Any] = result["data"] as! [String: Any]
                let paymentBean: [String: Any] = data["paymentBean"] as! [String: Any]
                
                let originalId: String = paymentBean["originalId"] as! String
                self?.payModel?.originalId = originalId
                let orderNO: String = paymentBean["orderNO"] as! String
                self?.payModel?.orderNo = orderNO
                
                self?.uploadSuccess?()//上传成功
            }else{
                self?.networkErrorClosure?(nil, result)
            }
            
            }, failureClourse: { [weak self](error: Error?) in
                self?.networkErrorClosure?(error, nil)
        }) { [weak self] _ in
            self?.completion?()
        }
    }
}
