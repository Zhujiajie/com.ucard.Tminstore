//
//  MUPublishViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/1/3.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUPublishViewModel: BaseViewModel {
    
    /// 成功的闭包
    var successClosure: ((_ activityFlag: Bool)->())?
    /// 错误的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    fileprivate let dataModel: MUPublishDataModel = MUPublishDataModel()
    fileprivate let userInfoDataModel: PMainDataModel = PMainDataModel()
    
    //MARK: 上传订单
    func shareToCommunity(postcard: MUFinalPostcardModel) {
        
        showSVProgress(title: nil)
        
        //先尝试获取用户个人信息，成功之后才会开始上传
        userInfoDataModel.getUserInfo(success: { [weak self] (result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                let data: [String: Any] = result["data"] as! [String: Any]
                let userDic: [String: Any] = data["user"] as! [String: Any]
                _ = UserInfoModel.initFromDic(dic: userDic)
                self?.dataModel.shareToCommunity(postcardModel: postcard)
            }else{
                self?.errorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
            self?.errorClosure?(error, nil)
            self?.dismissSVProgress()
        }, completionClosure: nil)
        
        //MARK: 上传成功
        dataModel.uploadSuccess = {[weak self](result: [String: Any]) in
//            print(result)
            if let data: [String: Any] = result["data"] as? [String: Any]{
                if let activityFlag: Bool = data["activityFlag"] as? Bool{
                    self?.successClosure?(activityFlag)
                    return
                }
            }
            self?.successClosure?(false)
        }
        
        //出错
        dataModel.networkErrorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            DispatchQueue.main.async {
                self?.errorClosure?(error, result)
                self?.dismissSVProgress()
            }
        }
        
        // 完成
        dataModel.completion = {[weak self] _ in
            self?.dismissSVProgress()
        }
    }
    
    //MARK: 初始化一个coredata模型
    /// 初始化一个coredata模型
    func initCoreDataModel()->Postcard{
        return dataModel.initCoreDataModel()
    }
    
    //MARK: 保存数据到CoreData中
    /// 保存数据到CoreData中
    ///
    /// - Parameters:
    ///   - postcard: MUFinalPostcardModel
    ///   - main: 返回主线程
    func saveDataToCoreData(postcard: MUFinalPostcardModel, toPostcardData postcardData: Postcard, returnMainThread main: (()->())?){
        doInBackground(inBackground: { [weak self] _ in
            self?.dataModel.saveData(postcard: postcard, toPostcardData: postcardData)
        }) { 
            main?()
        }
    }
    
    //MARK: 发布完成之后，删除coreData数据
    /// 发布完成之后，删除coreData数据
    ///
    /// - Parameter postcardData: Postcard
    func deleteCoreData(postcardData: Postcard){
        dataModel.deletePostcardData(postcardData: postcardData)
    }
}
