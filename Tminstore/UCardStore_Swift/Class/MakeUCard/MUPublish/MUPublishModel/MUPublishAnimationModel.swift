//
//  MUPublishAnimationModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/26.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUPublishAnimationModel: BaseAnimationModel {
    
    var superView: UIView?
    var containerView: MUPublishContainerView?
    var playerContainerView: MUPreviewPlayView?
    
    //MARK: 切换明信片正反面
    /**
     切换明信片正反面
     
     - parameter isFront: 明信片是否正面朝上
     */
    func switchContainerViewAnimation(isFront: Bool, isHorizon horizon: Bool, andIsAR isAR: Bool) {
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(.easeInOut)
        UIView.setAnimationDuration(0.75)
        
        let first: Int = containerView!.subviews.index(of: containerView!.frontImageView!)!
        let second: Int = containerView!.subviews.index(of: containerView!.backImageView!)!
        
        //返回到正面
        if isFront {
            UIView.setAnimationTransition(UIViewAnimationTransition.flipFromLeft, for: containerView!, cache: true)
            containerView?.exchangeSubview(at: first, withSubviewAt: second)
        }else{
            UIView.setAnimationTransition(UIViewAnimationTransition.flipFromRight, for: containerView!, cache: true)
            containerView?.exchangeSubview(at: second, withSubviewAt: first)
        }
        UIView.commitAnimations()
        autoLayoutAnimation(view: superView!, withDelay: 0.0)
    }
    
    //MARK: 播放器出现的动画
    /**
     点击了播放按钮，出现播放界面
     */
    func playerAnimationAppear() {
        
        playerContainerView?.isHidden = false
        playerContainerView?.snp.updateConstraints({ (make: ConstraintMaker) in
            make.size.equalTo(CGSize(width: screenWidth, height: screenHeight + nav_statusHeight))
        })
        autoLayoutAnimation(view: superView!, withDelay: 0)
        
        UIView.animate(withDuration: 0.5, delay: 0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            
            self.playerContainerView!.player?.view.alpha = 1
            self.playerContainerView!.leftTimeLabel!.alpha = 1
            self.playerContainerView!.progressView!.alpha = 1
            
        }) { (_) in
            self.playerContainerView!.player?.playFromBeginning()
        }
    }
    
    //MARK: 退出播放界面
    /**
     退出播放界面
     */
    func dismissPlayer(complete: (()->())?) {
        
        playerContainerView?.player?.stop()
        
        UIView.animate(withDuration: 0.2, delay: 0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            
            self.playerContainerView?.player?.view.alpha = 0
            self.playerContainerView?.leftTimeLabel?.alpha = 0
            self.playerContainerView?.progressView?.alpha = 0
            
        }) { (_) in
            
            self.playerContainerView?.snp.remakeConstraints({ (make: ConstraintMaker) in
                make.center.equalTo(self.superView!)
                make.width.equalTo(1)
                make.height.equalTo(1)
            })
            self.autoLayoutAnimation(view: self.superView!, withDelay: 0.0)
            
            complete?()
        }
    }
}
