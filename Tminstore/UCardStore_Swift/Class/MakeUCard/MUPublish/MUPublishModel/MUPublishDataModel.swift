//
//  MUPublishDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/31.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData

/// 制作的明信片的数据模型
let PostcardEntityName: String = "Postcard"
let FrontViewEntityName: String = "FrontView"
let BackViewEntityName: String = "BackView"

class MUPublishDataModel: BaseDataModel {

    ///上传成功的闭包，返回支付信息
    var uploadSuccess: ((_ result: [String: Any])->())?
    
    /// 错误的闭包
    var networkErrorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 完成的闭包
    var completion: (()->())?
    
    /// 明信片对象
    fileprivate var postcard: MUFinalPostcardModel?
    
    //MARK: 将卡片投递到时空圈
    func shareToCommunity(postcardModel: MUFinalPostcardModel){
        
        postcard = postcardModel
        postcard?.frontImageUUID   = UUID().uuidString
        postcard?.backImageUUID    = UUID().uuidString
        postcard?.videoID          = UUID().uuidString

        uploadFrontImage()//先上传正面图片
    }
    
    //MARK: 上传正面图片
    fileprivate func uploadFrontImage(){
        
        //压缩一下图片
        let newImage: UIImage = UIImage(image: postcard!.frontImage!, scaledToWidth: 1000)
        
        guard let imageData: Data = UIImagePNGRepresentation(newImage) else {
            networkErrorClosure?(nil, nil)
            return }
        
        uploadToUpyun(data: imageData, andSaveKey: communityFrontImageUpun + "\(postcard!.frontImageUUID).png", andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.uploadVideo()
            }, failClosure: { [weak self] (error: Error?, _, _) in
            self?.networkErrorClosure?(error, nil)
            }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                self?.uploadImageProgress(percent: completeCount/totalCount)
        })
    }
    
//    //MARK: 上传背面图片
//    fileprivate func uploadBackImage(){
//        
//        let upyun: UpYun = UpYun()
//        
//        upyun.uploadFile(postcard!.backImage!, saveKey: communityBackImageUpun + "\(postcard!.backImageUUID).png")
//        
//        //上传成功，上传背面图片
//        upyun.successBlocker = { [weak self] _ in
//            self?.uploadVideo()
//        }
//        //上传失败
//        upyun.failBlocker = { [weak self](error: Error?) in
//            self?.networkErrorClosure?(error, nil)
//        }
//        //上传的进度
//        upyun.progressBlocker = {[weak self](percent: CGFloat, _) in
//            self?.uploadImageProgress(percent: (percent / 2) + 0.5)
//        }
//    }
    
    //MARK: 上传视频到又拍云
    func uploadVideo(){
        
        do{
            let videoData: Data = try Data(contentsOf: postcard!.videoPath!, options: [])
            
            let parameters: [String: Any] = [
                "name": "naga",
                "type": "video",
                "avopts": "/wmImg/L2lPU0ltYWdlcy93YXRlcm1hcmswMS5wbmc=/wmGravity/northwest/wmDx/20/wmDy/15",
                "save_as": communityVideoUpun + "\(postcard!.videoID).mp4"
            ]
            
            uploadToUpyun(data: videoData, andSaveKey: communityVideoUpun + "\(postcard!.videoID).mp4", andParameters: ["apps": [parameters]], successClosure: { [weak self] (_, _) in
                self?.uploadOrder()
                }, failClosure: { [weak self] (error: Error?, _, _) in
                self?.networkErrorClosure?(error, nil)
                }, progressClosure: {[weak self] (completeCount: Float, totalCount: Float) in
                    self?.uploadVideoProgress(percent: completeCount/totalCount)
            })
            
        }catch{networkErrorClosure?(error, nil)}
    }
    
//    //MARK: 上传进度
//    fileprivate func uploadImageProgress(percent: Float){
//        SVProgressHUD.showProgress(percent, status: NSLocalizedString("上传图片中", comment: ""))
//    }
//
//    //MARK: 上传视频的进度
//    fileprivate func uploadVideoProgress(percent: Float){
//        SVProgressHUD.showProgress(percent, status: NSLocalizedString("上传AR视频中", comment: ""))
//    }
    
    //MARK: 上传订单
    fileprivate func uploadOrder(){
        
        var urlString: String
        if postcard?.isPrivate == true{
            urlString = appAPIHelp.releaseToPrivateAPI
        }else{
            urlString = appAPIHelp.shareToCommunityAPI
        }
        
        var longitude: Double
        var latitude: Double
        
        if let coordinate: CLLocationCoordinate2D = postcard?.photoLocation?.coordinate {
            longitude = coordinate.longitude
            latitude = coordinate.latitude
        }else{
            longitude = 0.0
            latitude = 0.0
        }

        var country: String
        var state: String
        var city: String
        if languageCode() == "CN"{
            country = postcard!.countryInCn
            state = postcard!.stateInCN
            city = postcard!.cityInCN
        }else{
            country = postcard!.countryInEn
            state = postcard!.stateInEn
            city = postcard!.cityInEN
        }
        
        // JSON Body
        let body: [String : Any] = [
            
            "token": getUserToken(),
            "frontImageUrl": postcard!.communityFrontImageURLPath,
            "backImageUrl": postcard!.communityBackImageURLPath,
            "videoUrl": postcard!.communityVideoURLPath,
            "pointPrice": postcard!.pointPrice,
            "frontImageRemark": postcard!.frontImageRemark,
            "backImageRemark": postcard!.backImageRemark,
            "videoRemark": postcard!.videoRemark,
            "backContent": postcard!.message,
            "isOnMarket": "01",
            "longitude": longitude,
            "latitude": latitude,
            "country": country,
            "province": state,
            "city": city,
            "district": "",
            "detailedAddress": "",
            "isAr": "00"
            
            ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: body, successClosure: { [weak self](result: [String: Any]) in
            if let code: Int = result["code"] as? Int, code == 1000{
                self?.uploadSuccess?(result)
            }else{
                self?.networkErrorClosure?(nil, result)
            }
            }, failureClourse: { [weak self](error: Error?) in
                self?.networkErrorClosure?(error, nil)
        }) {[weak self] _ in
            self?.completion?()
        }
    }
    
    //MARK: 初始化一个core date模型
    /// 初始化一个core date模型
    func initCoreDataModel()->Postcard{
        
        let postcardData: Postcard = NSEntityDescription.insertNewObject(forEntityName: PostcardEntityName, into: app.managedObjectContext) as! Postcard
        let frontView: FrontView = NSEntityDescription.insertNewObject(forEntityName: FrontViewEntityName, into: app.managedObjectContext) as! FrontView
        let backView: BackView = NSEntityDescription.insertNewObject(forEntityName: BackViewEntityName, into: app.managedObjectContext) as! BackView
        
        postcardData.frontView = frontView
        postcardData.backView = backView
        
        postcardData.memberCode = getUserID()
        postcardData.createdDate = NSDate()
        
        return postcardData
    }
    
    //MARK: 保存数据到CoreData中
    /// 保存数据到CoreData中
    ///
    /// - Parameters:
    ///   - postcard: MUFinalPostcardModel
    func saveData(postcard: MUFinalPostcardModel, toPostcardData postcardData: Postcard){
        
        if let frontImage: UIImage = postcard.frontImage{
            let frintImageData: Data? = UIImageJPEGRepresentation(frontImage, 1.0)
            postcardData.frontPreviewImage = frintImageData as NSData?
        }
        if let backImage: UIImage = postcard.backImage{
            let backImageData: Data? = UIImageJPEGRepresentation(backImage, 1.0)
            postcardData.backPreviewImage = backImageData as NSData?
        }
        if let signatureImage: UIImage = postcard.signatureImage{
            let signatureImageData: Data? = UIImageJPEGRepresentation(signatureImage, 1.0)
            postcardData.backView?.signatureImage = signatureImageData
        }
        
//        postcardData.frontImageUUID   = postcard.frontImageUUID
//        postcardData.backImageUUID    = postcard.backImageUUID
//        postcardData.addressImageUUID = postcard.addressImageUUID
//        postcardData.videoID          = postcard.videoID
        postcardData.videoPath        = postcard.videoPath?.path
        postcardData.frontImageRemark = postcard.frontImageRemark
        postcardData.backImageRemark  = postcard.backImageRemark
        postcardData.videoRemark      = postcard.videoRemark
        
        postcardData.backView?.text0 = postcard.message
        postcardData.backView?.text0FontName = postcard.messageFontName
        postcardData.backView?.text0FontSize = NSNumber(integerLiteral: postcard.messageFontSize)
        
        postcardData.photoLocation     = postcard.photoLocation as NSObject?
        postcardData.photoLocationText = postcard.photoLocationText
        
        postcardData.createdDate = Date() as NSDate
        
        app.saveContext()
    }
}
