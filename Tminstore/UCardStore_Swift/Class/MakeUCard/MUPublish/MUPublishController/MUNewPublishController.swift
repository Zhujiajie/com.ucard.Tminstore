//
//  MUNewPublishController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import ChameleonFramework
import IQKeyboardManagerSwift

class MUNewPublishController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    fileprivate let viewModel: MUPublishViewModel = MUPublishViewModel()
    
    var postcardData: Postcard?
    var postcard: MUFinalPostcardModel?
    
    fileprivate var tableView: MUPublishTableView?
    fileprivate var releaseButton: UIButton?
    fileprivate var releaseInfoView: MUNewPulishAlertView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor(hexString: "#F4F7F9")
        setNavigationBar()
        
        tableView = MUPublishTableView()
        view.addSubview(tableView!)
        tableView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(8/baseWidth)
            make.left.bottom.right.equalTo(view)
        })
        tableView?.delegate = self
        tableView?.dataSource = self
        
        releaseButton = UIButton()
        releaseButton?.backgroundColor = UIColor(gradientStyle: UIGradientStyle.leftToRight, withFrame: CGRect(x: 0, y: 0, width: 335/baseWidth, height: 56/baseWidth), andColors: [UIColor(hexString: "#7AEFED"), UIColor(hexString: "#63E4EE")])
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 18), NSForegroundColorAttributeName: UIColor.white]
        let attStr: NSAttributedString = NSAttributedString(string: "发布", attributes: attDic)
        releaseButton?.setAttributedTitle(attStr, for: UIControlState.normal)
        view.addSubview(releaseButton!)
        releaseButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(-25/baseWidth)
            make.centerX.equalTo(view)
            make.size.equalTo(CGSize(width: 335/baseWidth, height: 56/baseWidth))
        })
        releaseButton?.addTarget(self, action: #selector(touchReleaseButton(button:)), for: UIControlEvents.touchUpInside)
        
        closures()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("发布", comment: ""), attributeDic: attDic)
        setNavigationBarRightButtonWithTitle(NSLocalizedString("保存", comment: ""))
    }

    //MARK: 点击发布按钮的事件
    func touchReleaseButton(button: UIButton){
        if postcard?.isPrivate == false{
            releaseInfoView = MUNewPulishAlertView(frame: UIScreen.main.bounds)
            navigationController?.view.addSubview(releaseInfoView!)
            //MARK: 成功获取价格。开始发布
            releaseInfoView?.getPointPriceSuccess = {[weak self] (price: Int) in
                self?.postcard?.pointPrice = price
            }
            //MARK: 获取定价失败
            releaseInfoView?.getPointPriceFailed = {[weak self] _ in
                self?.alert(alertTitle: NSLocalizedString("价格需要在3~100之间", comment: ""))
            }
            //MARK: 点击发布按钮，开始发布
            releaseInfoView?.touchReleaseClosure = {[weak self] _ in
                self?.viewModel.shareToCommunity(postcard: (self?.postcard)!)
            }
        }else{
            viewModel.shareToCommunity(postcard: postcard!)
        }
    }
    
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        showSVProgress(title: nil)
        if postcardData == nil{
            postcardData = viewModel.initCoreDataModel()
            for vc: UIViewController in navigationController!.viewControllers{
                if let controller: MUViewController = vc as? MUViewController{
                    controller.postcardData = postcardData
                }
            }
        }
        if postcardData == nil {return}
        viewModel.saveDataToCoreData(postcard: postcard!, toPostcardData: postcardData!, returnMainThread: {[weak self] _ in
            self?.dismissSVProgress()
            self?.saveSuccessAlert()
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 网络请求出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 成功投递到时空圈
        viewModel.successClosure = { [weak self] (activityFlag: Bool) in
            if self?.postcardData != nil{//删除coredata
                self?.viewModel.deleteCoreData(postcardData: (self?.postcardData)!)
            }
            
            if activityFlag == false{
                self?.alert(alertTitle: NSLocalizedString("发布成功", comment: ""),leftClosure: {
                    self?.backToMainController()
                })
            }else{
                let vc: ActivityVC = ActivityVC()
                vc.backImage = self?.getScreenImage()
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self?.present(vc, animated: true, completion: nil)
                vc.dismissClosure = {[weak self] _ in
                    self?.alert(alertTitle: NSLocalizedString("发布成功", comment: ""),leftClosure: {
                        self?.backToMainController()
                    })
                }
            }
        }
    }
    
    //MARK: 返回主页
    func backToMainController(){
//        if let nav: PMainNavigationVC = app.tabBarVC?.viewControllers?[4] as? PMainNavigationVC{
//            nav.popToRootViewController(animated: false)
//        }
//        app.tabBarVC?.dismiss(animated: true, completion: nil)
//        //如果是发布成功，则刷新社区主页
//        if postcardData == nil{
//            app.tabBarVC?.selectedIndex = 0
//            if let nav: UINavigationController = app.tabBarVC?.viewControllers?.first as? UINavigationController, let mainVC: MainController = nav.viewControllers.first as? MainController{
//                mainVC.loadData()
//            }
//        }
    }

    //MARK: 保存成功之后的提示框
    fileprivate func saveSuccessAlert(){
        //提醒用户保存成功
        if #available(iOS 10.0, *) {
            let notificationGenerator: UINotificationFeedbackGenerator = UINotificationFeedbackGenerator()
            notificationGenerator.prepare()
            notificationGenerator.notificationOccurred(UINotificationFeedbackType.success)
        }
        alert(alertTitle: NSLocalizedString("保存成功", comment: ""), messageString: NSLocalizedString("返回时空圈还是继续发布？", comment: ""), leftButtonTitle: NSLocalizedString("返回", comment: ""), rightButtonTitle: NSLocalizedString("继续", comment: ""), leftClosure: {[weak self] _ in
            self?.backToMainController()
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: MUNewPublishTableViewContentCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUNewPublishTableViewContentCellReuseIdentifier, for: indexPath) as! MUNewPublishTableViewContentCell
            cell.postcard = postcard
            return cell
        case 1:
            let cell: MUNewPublishTableViewLocationCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUNewPublishTableViewLocationCellReuseIdentifier, for: indexPath) as! MUNewPublishTableViewLocationCell
            cell.postcard = postcard
            return cell
        default:
            let cell: MUNewPublishTableViewPrivateCell = tableView.dequeueReusableCell(withIdentifier: appReuseIdentifier.MUNewPublishTableViewPrivateCellReuseIdentifier, for: indexPath) as! MUNewPublishTableViewPrivateCell
            cell.postcard = postcard
            //MARK: 点击了切换私人或者公开
            cell.tapSwitchControlClosure = {[weak self] _ in
                if hasReleaseToPrivateInfoShowed() == false{
                    self?.alert(alertTitle: "您刚制作的内容属于私人\n制作可以在个人界面-“社区”\n中查看免费赠送3次扫描哦")
                }
            }
            return cell
        }
    }
}
