//
//  MUPublishController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/26.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SnapKit

class MUPublishController: BaseViewController {

    fileprivate let viewModel: MUPublishViewModel = MUPublishViewModel()
    
    /// Core Data
    var postcardData: Postcard?
    
    var postcard: MUFinalPostcardModel?
    
    /// 展示明信片正反面的视图
    fileprivate var containerView: MUPublishContainerView?
    /// 播放按钮
    fileprivate var playButton: MUPreviewButton?
    /// 填写标签的视图
    fileprivate var tagView: MUPublishContainerView?
    /// 展示定位信息的视图
    fileprivate var locationView: MUPublishContainerView?
    
    /// 承载播放器的视图
    fileprivate var playerContainerView: MUPreviewPlayView?
    /// 播放器
    fileprivate var player: ZTPlayer?
    
    fileprivate var animationModel: MUPublishAnimationModel?
    
    /// 投递到时空圈的按钮
    fileprivate var publishButton: MUPublishButton?
    /// 寄送给好友的按钮
    fileprivate var mailButton: MUPublishButton?
    /// 积分提示框
    fileprivate var creditAlerVC: MUPublishAlertVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexString: "#F4F7F9")
        
        setNavigationBar()
        setContainerView()
        setPlayButton()
        setTagView()
        setTwoButtons()
        closures()
        setAnimationModel()//设置动画模型
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setLocationView()//设置定位信息视图
        IQKeyboardManager.sharedManager().enable = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        setNavigationBarRightButtonWithTitle(NSLocalizedString("保存", comment: ""))
        navigationItem.rightBarButtonItem?.tintColor = UIColor(hexString: "686868")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("发布", comment: ""), attributeDic: attDic)
    }

    //MARK: 设置明信片
    fileprivate func setContainerView(){
        containerView = MUPublishContainerView.containerView(finalPostcardModel: postcard!)
        view.addSubview(containerView!)
        containerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(view).offset(8/baseHeight)
            make.left.right.equalTo(view)
            make.height.equalTo(258/baseWidth)
        })
    }
    
    //MARK: 设置打开播放器的按钮
    fileprivate func setPlayButton(){
        playButton = MUPreviewButton.playButton()
        view.addSubview(playButton!)
        playButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(containerView!.frontImageView!).offset(10/baseWidth)
            make.bottom.equalTo(containerView!.frontImageView!).offset(-10/baseWidth)
            make.size.equalTo(CGSize(width: 33/baseWidth, height: 33/baseWidth))
        })
        //MARK: 点击播放按钮，开始播放视频
        playButton?.touchPlayButton = {[weak self] _ in
            self?.setPlayer()
        }
    }

    //MARK: 设置填写标签的视图
    fileprivate func setTagView(){
        tagView = MUPublishContainerView.addTagView()
        view.addSubview(tagView!)
        tagView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(containerView!.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(51/baseHeight)
        })
    }
    
    //MARK: 设置定位信息的视图
    fileprivate func setLocationView(){
        locationView = MUPublishContainerView.locationView(photoLocationText: postcard?.photoLocationText)
        view.addSubview(locationView!)
        locationView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(tagView!.snp.bottom).offset(2/baseHeight)
            make.left.right.equalTo(tagView!)
            make.height.equalTo(54/baseHeight)
        })
    }
    
    //MARK: 设置动画模型
    fileprivate func setAnimationModel(){
        animationModel = MUPublishAnimationModel()
        animationModel?.superView = view
        animationModel?.containerView = containerView!
    }
    
    //MARK: 设置播放器相关
    fileprivate func setPlayer(){
        
        playerContainerView = MUPreviewPlayView.playerView(videoPath: postcard!.videoPath!)
        
        view.addSubview(playerContainerView!)
        playerContainerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
            make.size.equalTo(CGSize(width: 1, height: 1))
        })
        view.setNeedsUpdateConstraints()//约束需要更新
        view.updateConstraintsIfNeeded()//检测需要更新的约束
        view.layoutIfNeeded()//强制更新约束
        
        navigationController?.navigationBar.layer.zPosition = -1//将视频播放界面推到最前端
        
        player = playerContainerView?.player
        self.addChildViewController(player!)
        player?.didMove(toParentViewController: self)
        
        animationModel?.playerContainerView = playerContainerView
        animationModel?.playerAnimationAppear()//进入播放界面
        playerClousre()//必须在这里调用
    }

    //MARK: 设置两个按钮
    fileprivate func setTwoButtons(){
        mailButton = MUPublishButton.mailButton()
        view.addSubview(mailButton!)
        mailButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.bottom.equalTo(view)
            make.height.equalTo(54/baseHeight)
        })
        
        publishButton = MUPublishButton.publishButton()
        view.addSubview(publishButton!)
        publishButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.equalTo(view)
            make.bottom.equalTo(mailButton!.snp.top).offset(-6/baseHeight)
            make.height.equalTo(mailButton!)
        })
    }
    
    //MARK: 设置积分视图
    fileprivate func showCreditView(){
        
        creditAlerVC = MUPublishAlertVC()
        creditAlerVC?.postcard = postcard
        addChildViewController(creditAlerVC!)
        view.addSubview(creditAlerVC!.view)
        creditAlerVC?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
        
        //MARK: 成功分享到时空圈
        creditAlerVC?.shareToCommunitySuccess = {[weak self] _ in
            if self?.postcardData != nil{//删除coredata
                self?.viewModel.deleteCoreData(postcardData: (self?.postcardData)!)
            }
            self?.alert(alertTitle: NSLocalizedString("发布成功", comment: ""),leftClosure: {
                self?.backToMainController()
            })
        }
    }
    
    //MARK: 点击导航栏右按钮，保存草稿
    override func navigationBarRightBarButtonAction(_ button: UIBarButtonItem) {
        navigationItem.rightBarButtonItem?.isEnabled = false
        doInBackground(inBackground: { [weak self] _ in
            self?.saveData()
        }, backToMainQueue: nil)
    }
    
    //MARK: 保存数据
    fileprivate func saveData(){
        if postcardData == nil {
            postcardData = viewModel.initCoreDataModel()
            let vcArray: [UIViewController] = navigationController!.viewControllers
            for vc: UIViewController in vcArray{
                if vc.isKind(of: MUViewController.self){
                    let vc: MUViewController = vc as! MUViewController
                    vc.postcardData = postcardData
                }
                if vc.isKind(of: MUBackVC.self){
                    let vc: MUBackVC = vc as! MUBackVC
                    vc.postcardData = postcardData
                }
            }
        }
        viewModel.saveDataToCoreData(postcard: postcard!, toPostcardData: postcardData!) { [weak self] _ in
            DispatchQueue.main.async {
                self?.saveSuccessAlert()
                self?.navigationItem.rightBarButtonItem?.isEnabled = true
            }
        }
    }
    
    //MARK: 保存成功之后的提示框
    fileprivate func saveSuccessAlert(){
        //提醒用户保存成功
        if #available(iOS 10.0, *) {
            let notificationGenerator: UINotificationFeedbackGenerator = UINotificationFeedbackGenerator()
            notificationGenerator.prepare()
            notificationGenerator.notificationOccurred(UINotificationFeedbackType.success)
        }
        alert(alertTitle: NSLocalizedString("保存成功", comment: ""), messageString: NSLocalizedString("返回时空圈还是继续发布？", comment: ""), leftButtonTitle: NSLocalizedString("返回", comment: ""), rightButtonTitle: NSLocalizedString("继续", comment: ""), leftClosure: {[weak self] _ in
            self?.backToMainController()
        })
    }
}

extension MUPublishController{
    
    //MARK: 各种回调
    func closures(){
        //MARK: 点击翻转图片正反面
        containerView?.switchAimation = {[weak self] _ in
            self?.playButton?.isHidden = (self?.containerView?.isFront)!//是否隐藏播放按钮
            self?.animationModel?.switchContainerViewAnimation(isFront: (self?.containerView?.isFront)!, isHorizon: true, andIsAR: false)
        }
        
        //MARK: 点击投递时空圈按钮
        publishButton?.touchButton = {[weak self] _ in
            MobClick.event("sharetocommunity")//友盟点击事件
            if self?.postcard == nil {return}
            self?.showCreditView()
        }
        
        //MARK: 点击寄送给好友的按钮，进入选择地址列表
        mailButton?.touchButton = {[weak self] _ in
            MobClick.event("mailtofriend")//友盟点击事件
            let addressListVC: MUAddressListController = MUAddressListController()
            addressListVC.umengViewName = "地址列表"
            addressListVC.postcard = self?.postcard
            addressListVC.postcardData = self?.postcardData
            _ = self?.navigationController?.pushViewController(addressListVC, animated: true)
        }
        
        //MARK: 用户完成给正面图片添加标题，获取数据
        tagView?.textFieldEndEditingClosure = {[weak self] (frontImageRemark: String) in
            self?.postcard?.frontImageRemark = frontImageRemark
        }
        
        //MARK: 网络请求出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 返回主页
    func backToMainController(){
        if let vcArray: [UIViewController] = navigationController?.viewControllers{
            for vc: UIViewController in vcArray{
                if vc.isKind(of: MUViewController.self){
                    let muVC: MUViewController = vc as! MUViewController
                    muVC.dismissSelf()
                }
            }
        }
    }
    
    //MARK: 播放器相关闭包
    func playerClousre() {
        //MARK: 在播放界面点击退出，然后销毁播放器
        playerContainerView?.tapToDismiss = {[weak self] _ in
            self?.animationModel?.dismissPlayer(complete: {
                self?.navigationController?.navigationBar.layer.zPosition = 0//恢复导航栏的位置
                self?.playerContainerView?.firePlayer()
                self?.playButton?.isHidden = false
                self?.player = nil
            })
        }
    }
}
