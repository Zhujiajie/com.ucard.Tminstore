//
//  MUPublishAlertVC.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/26.
//  Copyright © 2016年 ucard. All rights reserved.
//

//-------------------发布明信片，选择投递到时空圈还是邮寄的页面------------------
import UIKit
import SnapKit
import IQKeyboardManagerSwift

/// 发布明信片，选择投递到时空圈还是邮寄的页面
class MUPublishAlertVC: BaseViewController {

    fileprivate let publishViewModel: MUPublishViewModel = MUPublishViewModel()
    
    fileprivate let payViewModel: MUPayViewModel = MUPayViewModel()
    
    /// 明信片模型
    var postcard: MUFinalPostcardModel?
    /// 订单模型
    var payModel: PayModel?
    /// 重新发布到时空圈
    var isShareToCommunityAgain: Bool = false
    
    /// 分享成功
    var shareToCommunitySuccess: (()->())?
    
    fileprivate var cancelButton: MUPublishButton?
    fileprivate var confirmButton: MUPublishButton?
    /// 展示积分购买说明的collectionView
    fileprivate var collectionView: MUPublishCollectionView?
    
    fileprivate var pageControl: UIPageControl?
    
    /// 是否要切换到输入积分状态
    fileprivate var shouldConfirmCredit: Bool = true
    /// 用户输入的积分额度
    fileprivate var pointPrice: Int = 0
    
    ///将UI都放在这个视图上
    fileprivate var containerView: UIView?
    
    /// 退出按钮
    fileprivate var dismissButton: MUPublishButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(white: 0.0, alpha: 0.35)
        view.alpha = 0.0
        
        setContainerView()
        setCollectionView()
        setTwoButton()
        setDismissButton()
        closures()
        appearAnimation()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        IQKeyboardManager.sharedManager().enable = false
    }
    
    //MARK: 设置承载视图
    fileprivate func setContainerView(){
        containerView = UIView()
        containerView?.backgroundColor = UIColor.white
        containerView?.layer.cornerRadius = 8
        containerView?.layer.masksToBounds = true
        
        view.addSubview(containerView!)
        //先隐藏
        containerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-screenHeight)
            make.size.equalTo(CGSize(width: 274/baseWidth, height: 266/baseHeight))
        })
    }
    
    //MARK: 展示积分购买说明的collectionView
    fileprivate func setCollectionView(){
        collectionView = MUPublishCollectionView.publishCollectionView()
        containerView?.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(containerView!)
            make.bottom.equalTo(containerView!).offset(-69/baseHeight)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        pageControl = UIPageControl()
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "79D3E0")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D8D8D8")
        pageControl?.currentPage = 0
        pageControl?.numberOfPages = 2
        containerView?.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom)
            make.centerX.equalTo(containerView!)
            make.height.equalTo(6)
        })
        // 暂时隐藏该UI控件
        pageControl?.isHidden = true
    }
    
    //MARK: 设置按钮
    fileprivate func setTwoButton(){
        
        cancelButton = MUPublishButton.creditIntrolButton(isConfirm: false)
        containerView?.addSubview(cancelButton!)
        cancelButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.equalTo(containerView!)
            make.right.equalTo(containerView!).offset(-138/baseWidth)
            make.height.equalTo(42/baseHeight)
        })
        
        confirmButton = MUPublishButton.creditIntrolButton(isConfirm: true)
        containerView?.addSubview(confirmButton!)
        confirmButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.right.bottom.equalTo(containerView!)
            make.left.equalTo(containerView!).offset(138/baseWidth)
            make.height.equalTo(cancelButton!)
        })
    }
    
    //MARK: 设置退出按钮
    fileprivate func setDismissButton(){
        //先隐藏
        dismissButton = MUPublishButton.dismissCreditIntrolViewButton()
        view.addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-screenHeight)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        })
    }
    
    //MARK: 切换到输入积分的状态
    fileprivate func switchToInputCredit(){
        cancelButton?.snp.updateConstraints({ (make: ConstraintMaker) in
            make.right.equalTo(containerView!).offset(-WIDTH(containerView!))
        })
        confirmButton?.snp.updateConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(containerView!)
        })
        autoLayoutAnimation(view: view, withDelay: 0.0)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            self.confirmButton?.alpha = 0.5
        }, completion: {(_)->() in
            self.confirmButton?.isEnabled = false
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击退出按钮，退出
        dismissButton?.touchButton = {[weak self] _ in
            self?.dismissSelfAnimation()
        }
        
        //MARK: 点击取消按钮的事件
        cancelButton?.touchButton = {[weak self] _ in
            self?.dismissSelfAnimation()
        }
        
        //MARK: 点击确认按钮的事件
        confirmButton?.touchButton = {[weak self] _ in
            
            //点击确认按钮直接投递
            //直接分享到时空圈
            self?.pointPrice = 5
            if self?.postcard != nil {
                self?.postcard?.pointPrice = (self?.pointPrice)!
                self?.publishViewModel.shareToCommunity(postcard: (self?.postcard)!)
            }
                // 邮寄订单分享到时空圈
            else{
                self?.payModel?.pointPrice = (self?.pointPrice)!
                self?.payViewModel.shareARToCommunity(payModel: (self?.payModel)!, andIsShareToCommunityAgain: (self?.isShareToCommunityAgain)!)
            }
            
//            if self?.shouldConfirmCredit == true{
//                self?.switchToInputCredit()
//                self?.shouldConfirmCredit = false
//                self?.collectionView?.scrollToItem(at: IndexPath.init(item: 1, section: 0), at: .centeredHorizontally, animated: true)//滚动到下一个cell
//                self?.pageControl?.currentPage = 1
//            }else{
//                
//                //直接分享到时空圈
//                if self?.postcard != nil {
//                    self?.postcard?.pointPrice = (self?.pointPrice)!
//                    self?.publishViewModel.shareToCommunity(postcard: (self?.postcard)!)
//                }
//                // 邮寄订单分享到时空圈
//                else{
//                    self?.payModel?.pointPrice = (self?.pointPrice)!
//                    self?.payViewModel.shareARToCommunity(payModel: (self?.payModel)!, andIsShareToCommunityAgain: (self?.isShareToCommunityAgain)!)
//                }
//            }
        }
        
        //MARK: 成功投递到时空圈
        publishViewModel.successClosure = { [weak self] _ in
            self?.dismissSelfAnimation()
            self?.shareToCommunitySuccess?()
        }
        payViewModel.successClosure = {[weak self] _ in
            self?.dismissSelfAnimation()
            self?.shareToCommunitySuccess?()
        }
        
        
        //MARK: 网络请求出错
        publishViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        payViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 出现的动画
    fileprivate func appearAnimation(){
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.view.alpha = 1.0
            }, completion: {[weak self] (_) in
                self?.dismissButton?.snp.updateConstraints({ (make: ConstraintMaker) in
                    make.bottom.equalTo((self?.view)!).offset(-162/baseHeight)
                })
                autoLayoutAnimation(view: (self?.view)!, withDelay: 0.0)
                self?.containerView?.snp.updateConstraints({ (make: ConstraintMaker) in
                    make.bottom.equalTo((self?.view)!).offset(-224/baseHeight)
                })
                autoLayoutAnimation(view: (self?.view)!, withDelay: 0.2)
        })
    }
    
    
    //MARK: 退出的动画
    fileprivate func dismissSelfAnimation(){
        
        dismissButton?.snp.updateConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(screenHeight)
        })
        autoLayoutAnimation(view: view, withDelay: 0.0)
        containerView?.snp.updateConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(screenHeight)
        })
        autoLayoutAnimation(view: view, withDelay: 0.2) { [weak self] _ in
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                self?.view.alpha = 0
            }, completion: { (_) in
                self?.view.removeFromSuperview()
            })
        }
    }
    
}

extension MUPublishAlertVC: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: MUPublishCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MUPublishCollectionViewCellReuseIdentifier, for: indexPath) as! MUPublishCollectionViewCell
        
        cell.indexPath = indexPath
        
//        pageControl?.currentPage = indexPath.item
        
        if indexPath.item == 1 {
            // 成功获取用户设定的积分制
            cell.getPointPriceSuccess = {[weak self] (pointPrice: Int) in
                self?.pointPrice = pointPrice
                // 确认按钮可以点击了
                UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                    self?.confirmButton?.alpha = 1.0
                }, completion: {(_)->() in
                    self?.confirmButton?.isEnabled = true
                })
            }
            // 积分获取失败
            cell.getPointPriceFailed = {[weak self] _ in
                self?.alert(alertTitle: NSLocalizedString("积分需要在1~1000之间", comment: ""))
            }
        }
        return cell
    }
}
