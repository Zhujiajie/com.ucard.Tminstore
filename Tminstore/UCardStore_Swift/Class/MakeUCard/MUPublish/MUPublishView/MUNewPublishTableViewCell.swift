//
//  MUNewPublishTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire

class MUNewPublishTableViewContentCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UITextViewDelegate {

    var postcard: MUFinalPostcardModel?
    
    /// 承载图片和视频的collectionView
    fileprivate var collectionView: PDraftCollectionView?
    /// 展示页数的指示器
    fileprivate var pageControl: UIPageControl?
    /// 输入文字描述的textView
    fileprivate var remarkTextView: FSTextView?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        collectionView = PDraftCollectionView()
        collectionView?.delegate = self
        collectionView?.dataSource = self
        contentView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.right.equalTo(contentView)
            make.height.equalTo(278/baseWidth)
        })
        
        // 页数指示器
        pageControl = UIPageControl()
        pageControl?.numberOfPages = 2
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "60DBE8")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D6D6D6")
        contentView.addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(collectionView!.snp.bottom).offset(-10)
            make.centerX.equalTo(contentView)
        })
        
        remarkTextView = FSTextView()
        remarkTextView?.font = UIFont.systemFont(ofSize: 12)
        remarkTextView?.placeholder = NSLocalizedString("点击添加文字描述...", comment: "")
        contentView.addSubview(remarkTextView!)
        remarkTextView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(pageControl!.snp.bottom).offset(-10)
            make.bottom.equalTo(contentView)
            make.height.equalTo(60/baseWidth)
            make.left.equalTo(contentView).offset(9/baseWidth)
            make.right.equalTo(contentView).offset(-9/baseWidth)
        })
        remarkTextView?.delegate = self
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#E7E8EA")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(1)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    //MARK: 设置cell的内容
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0{
            let cell: PDraftImageCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftImageCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftImageCollectionViewCell
            cell.image = postcard?.frontImage
            return cell
        }else{
            let cell: PDraftVideoCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.PDraftVideoCollectionViewCellappReuseIdentifier, for: indexPath) as! PDraftVideoCollectionViewCell
            cell.videoPath = postcard?.videoPath
            return cell
        }
    }
    
    //MARK: 不显示Cell时。需要暂停播放视频
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell: PDraftVideoCollectionViewCell = cell as? PDraftVideoCollectionViewCell{
            cell.pausePlayer(hidePlayButton: true)
        }
    }
    
    //MARK: 将视频cell滚动到屏幕
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x == screenWidth{
            if let cell: PDraftVideoCollectionViewCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? PDraftVideoCollectionViewCell{
                cell.playVideo()
            }
        }
        pageControl?.currentPage = Int(scrollView.contentOffset.x / screenWidth)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        postcard?.frontImageRemark = textView.text
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MUNewPublishTableViewLocationCell: UITableViewCell{
    
    var postcard: MUFinalPostcardModel?
    
    /// 当前城市的中文名称
    var cityInCN: String = ""
    var stateInCN: String = ""
    var countryInCn: String = ""
    /// 当前城市的英文名称
    var cityInEn: String = ""
    var stateInEn: String = ""
    var countryInEn: String = ""
    
    /// 地理位置的文字
    var locationText: String = ""{
        didSet{
            locationLabel?.text = locationText
        }
    }
    
    fileprivate var locationIcon: UIImageView?
    fileprivate var locationLabel: UILabel?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        locationIcon = UIImageView(image: UIImage(named: "publishLocation"))
        locationIcon?.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(locationIcon!)
        locationIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(16/baseWidth)
            make.left.equalTo(contentView).offset(18/baseWidth)
            make.bottom.equalTo(contentView).offset(-16/baseWidth)
            make.size.equalTo(CGSize(width: 17/baseWidth, height: 23/baseWidth))
        })
        
        locationLabel = UILabel()
        locationLabel?.text = NSLocalizedString("位置", comment: "")
        locationLabel?.font = UIFont.systemFont(ofSize: 17)
        contentView.addSubview(locationLabel!)
        locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(locationIcon!.snp.right).offset(18/baseWidth)
            make.right.equalTo(contentView).offset(-5/baseWidth)
        })
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#E7E8EA")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(1)
        }
        
        requestLocationInfo(location: currentUserLocation)
    }
    
    //MARK: 开始反编译地理信息
    /**
     开始反编译地理信息
     */
    func requestLocationInfo(location: CLLocation?) {
        //通过谷歌地图API反编译地址，分别获取中文和英文的字段
        var language: String
        if getPreferredLanguage()?.contains("zh") == true {
            language = "zh-CN"
        }else{
            language = "en-GB"
        }
        requestCityName(language: language, withLocation: location)
    }
    
    //MARK: 谷歌API反编译地址
    /**
     谷歌API反编译地址
     
     - parameter location: 地理位置 CLLocation
     - parameter language: 英文字段或中文字段
     */
    fileprivate func requestCityName(language: String, withLocation location: CLLocation?) {
        
        /**
         谷歌地址反编译
         GET https://maps.google.cn/maps/api/geocode/json 国内服务器
         GET https://maps.googleapis.com/maps/api/geocode/json 国外服务器
         */
        
        // Add Headers
        let headers: [String: String] = [
            "Content-Type":"application/x-www-form-urlencoded; charset=utf-8",
            ]
        
        //解析出经纬度
        var latlng: String
        
        if let coordinate: CLLocationCoordinate2D = location?.coordinate{
            latlng = "\(coordinate.latitude),\(coordinate.longitude)"
        }else{
            latlng = getLocationString()
        }
        
        // Add URL parameters
        let urlParams: [String: Any] = [
            "latlng": latlng,
            "key": appAPIHelp.googleAPIKey,
            "result_type": "locality",
            "location_type": "APPROXIMATE",
            "language": language,
            ]
        
        //分别尝试中国和国外的服务器
        alamofireRequest(url: "https://maps.google.cn/maps/api/geocode/json", withUrlParams: urlParams, withHeaders: headers, withLanguage: language)
        //        alamofireRequest(url: "https://maps.googleapis.com/maps/api/geocode/json", withUrlParams: urlParams, withHeaders: headers, withLanguage: language)
    }
    
    //MARK: 反编译地址的网络请求
    /**
     反编译地址的网络请求
     
     - parameter url:       服务器地址
     - parameter urlParams: 参数
     - parameter headers:   请求头
     - parameter language:  字段语言
     */
    fileprivate func alamofireRequest(url: String, withUrlParams urlParams: [String: Any], withHeaders headers: [String: String], withLanguage language: String){
        
        // Fetch Request
        Alamofire.request(url, method: HTTPMethod.get, parameters: urlParams, encoding: URLEncoding.default, headers: headers)
            .validate()
            .responseJSON {[weak self ] (response: DataResponse<Any>) in
                //                if let value = response.result.value{
                //                    print(value)
                //                }
                switch response.result{
                case .success(let json as [String: Any]):
                    
                    if let resultArray: [[String: Any]] = json["results"] as? [[String: Any]]{
                        
                        if let address_components: [[String: AnyObject]] = resultArray.first?["address_components"] as? [[String: AnyObject]] {
                            
                            switch language{
                                
                            case "zh-CN":
                                
                                for dic: [String: Any] in address_components {
                                    
                                    let type: [String] = dic["types"] as! [String]
                                    
                                    switch type[0]{
                                        
                                    case "locality":
                                        self?.cityInCN = dic["long_name"] as! String
                                    case "administrative_area_level_1":
                                        self?.stateInCN = dic["long_name"] as! String
                                    case "country":
                                        self?.countryInCn = dic["long_name"] as! String
                                    default: break
                                    }
                                }
                                
                            case "en-GB":
                                
                                for dic: [String: Any] in address_components {
                                    
                                    let type: [String] = dic["types"] as! [String]
                                    
                                    switch type[0]{
                                        
                                    case "locality":
                                        self?.cityInEn = dic["long_name"] as! String
                                    case "administrative_area_level_1":
                                        self?.stateInEn = dic["long_name"] as! String
                                    case "country":
                                        self?.countryInEn = dic["long_name"] as! String
                                    default: break
                                    }
                                }
                                
                            default: break
                            }
                            DispatchQueue.main.async {
                                self?.setLocationLabelText()//通过地址字段，拼接完整的地址
                            }
                        }
                    }
                case .failure(_):
                    break
                default:
                    break
                }
        }
    }
    
    //MARK: 拼接地址
    /**
     拼接地址
     */
    fileprivate func setLocationLabelText(){
        
        let systemLan: String? = getPreferredLanguage()//获取系统语言
        
        if systemLan == nil {
            
            locationText = "\(cityInEn) \(stateInEn) \(countryInEn)"
            
        }else if systemLan!.contains("zh"){//中文
            
            locationText = "\(countryInCn) \(stateInCN) \(cityInCN)"
            
        }else{//其它语言
            
            locationText = "\(cityInEn) \(stateInEn) \(countryInEn)"
        }
        
        postcard?.countryInCn = countryInCn
        postcard?.countryInEn = countryInEn
        postcard?.stateInCN = stateInCN
        postcard?.stateInEn = stateInEn
        postcard?.cityInCN = cityInCN
        postcard?.cityInEN = cityInEn
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class MUNewPublishTableViewPrivateCell: UITableViewCell{
    
    ///点击了切换私人或者公开
    var tapSwitchControlClosure: (()->())?
    
    var postcard: MUFinalPostcardModel?{
        didSet{
            setData()
        }
    }
    
    fileprivate var privateIcon: UIImageView?
    fileprivate var privateLabel: UILabel?
    fileprivate var privateSwitch: UISwitch?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = UITableViewCellSelectionStyle.none
        
        privateIcon = UIImageView(image: UIImage(named: "privateIcon"))
        privateIcon?.contentMode = UIViewContentMode.scaleAspectFit
        contentView.addSubview(privateIcon!)
        privateIcon?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(16/baseWidth)
            make.left.equalTo(contentView).offset(18/baseWidth)
            make.bottom.equalTo(contentView).offset(-16/baseWidth)
            make.size.equalTo(CGSize(width: 17/baseWidth, height: 23/baseWidth))
        })
        
        privateSwitch = UISwitch()
        privateSwitch?.onTintColor = UIColor(hexString: "3EE4EF")
        contentView.addSubview(privateSwitch!)
        privateSwitch?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.right.equalTo(contentView).offset(-22/baseWidth)
        })
        privateSwitch?.addTarget(self, action: #selector(switchPrivate(switcher:)), for: UIControlEvents.valueChanged)
        
        privateLabel = UILabel()
        privateLabel?.text = NSLocalizedString("公开", comment: "")
        privateLabel?.font = UIFont.systemFont(ofSize: 17)
        contentView.addSubview(privateLabel!)
        privateLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(contentView)
            make.left.equalTo(privateIcon!.snp.right).offset(18/baseWidth)
            make.right.equalTo(privateSwitch!.snp.left).offset(-5/baseWidth)
        })
        
        let grayLine: UIView = UIView()
        grayLine.backgroundColor = UIColor(hexString: "#E7E8EA")
        contentView.addSubview(grayLine)
        grayLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(1)
        }
    }
    
    fileprivate func setData(){
        privateSwitch?.setOn(postcard!.isPrivate, animated: true)
    }
    
    //MARK: 点击了开关
    func switchPrivate(switcher: UISwitch){
        postcard?.isPrivate = switcher.isOn
        if postcard?.isPrivate == true{
            privateLabel?.text = NSLocalizedString("不公开", comment: "")
        }else{
            privateLabel?.text = NSLocalizedString("公开", comment: "")
        }
        tapSwitchControlClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

