//
//  MUNewPulishAlertView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/27.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 发布到社区，提示框
import UIKit
import SnapKit

class MUNewPulishAlertView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {

    /// 点击按钮的事件
    var touchReleaseClosure: (()->())?
    
    /// 积分获取失败，需要在1~1000之间
    var getPointPriceFailed: (()->())?
    
    /// 成功获得用户输入的积分额度
    var getPointPriceSuccess: ((_ point: Int)->())?
    
    fileprivate var pageControl: UIPageControl?
    fileprivate var collectionView: MUPublishCollectionView?
    fileprivate let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName: UIColor.white]
    fileprivate var button: UIButton?
    fileprivate var dismissButton: MUPublishButton?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.35)
        alpha = 0
        
//        //点击退出
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapIntroView(tap:)))
//        addGestureRecognizer(tap)
        
        //底部白色视图
        let containerView: UIView = UIView()
        containerView.backgroundColor = UIColor.white
        containerView.layer.cornerRadius = 8
        containerView.layer.masksToBounds = true
        addSubview(containerView)
        containerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.top.equalTo(self).offset(177/baseWidth)
            make.size.equalTo(CGSize(width: 274/baseWidth, height: 262/baseWidth))
        }
        
        //确认的按钮
        button = UIButton()
        button?.backgroundColor = UIColor(hexString: "6ED7E4")
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("拒绝", comment: ""), attributes: attDic)
        button?.setAttributedTitle(attStr, for: UIControlState.normal)
        containerView.addSubview(button!)
        button?.snp.makeConstraints { (make: ConstraintMaker) in
            make.height.equalTo(42/baseWidth)
            make.bottom.left.right.equalTo(containerView)
        }
        button?.addTarget(self, action: #selector(touchPurchaseButton(button:)), for: UIControlEvents.touchUpInside)
        
        pageControl = UIPageControl()
        pageControl?.currentPageIndicatorTintColor = UIColor(hexString: "6ED7E4")
        pageControl?.pageIndicatorTintColor = UIColor(hexString: "D8D8D8")
        pageControl?.currentPage = 0
        pageControl?.numberOfPages = 2
        addSubview(pageControl!)
        pageControl?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(button!.snp.top)
            make.centerX.equalTo(containerView)
        })
        
        collectionView = MUPublishCollectionView.releaseToCommunituCollection()
        containerView.addSubview(collectionView!)
        collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(containerView)
            make.bottom.equalTo(pageControl!.snp.top)
        })
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        //先隐藏
        dismissButton = MUPublishButton.dismissCreditIntrolViewButton()
        addSubview(dismissButton!)
        dismissButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(self)
            make.top.equalTo(containerView.snp.bottom).offset(25/baseWidth)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        })
        
        dismissButton?.touchButton = {[weak self] _ in
            self?.dismissAnimation()
        }
        
        //渐变和缩放动画
        containerView.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {[weak self] _ in
            self?.alpha = 1
            containerView.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    //MARK: 点击退出
    func tapIntroView(tap: UITapGestureRecognizer){
        dismissAnimation()
    }
    
    //MARK: 消失的动画
    /// 消失的动画
    func dismissAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
            self.alpha = 0
        }) { (_) in
            self.layer.removeAllAnimations()
            self.removeFromSuperview()
        }
    }
    
    //MARK: 点击按钮事件
    func touchPurchaseButton(button: UIButton) {
        if collectionView?.contentOffset.x == 0{
            dismissAnimation()
        }else{
            if let cell: MUNewPublishCollectionViewPriceCell = collectionView?.cellForItem(at: IndexPath(item: 1, section: 0)) as? MUNewPublishCollectionViewPriceCell{
                cell.textField?.resignFirstResponder()
                touchReleaseClosure?()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0{
            let cell: MUNewPulishCollectionViewInfoCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MUNewPulishCollectionViewInfoCellappReuseIdentifier, for: indexPath) as! MUNewPulishCollectionViewInfoCell
            return cell
        }else{
            let cell: MUNewPublishCollectionViewPriceCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MUNewPublishCollectionViewPriceCellReuseIdentifier, for: indexPath) as! MUNewPublishCollectionViewPriceCell
            //MARK: 获取定价失败
            cell.getPointPriceFailed = {[weak self] _ in
                self?.getPointPriceFailed?()
            }
            //MARK: 获取定价成功
            cell.getPointPriceSuccess = { [weak self] (price: Int) in
                self?.getPointPriceSuccess?(price)
            }
            return cell
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        var pageIndex: Int
        if scrollView.contentOffset.x == 0{
            let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("拒绝", comment: ""), attributes: attDic)
            button?.setAttributedTitle(attStr, for: UIControlState.normal)
            pageIndex = 0
        }else{
            let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("完成", comment: ""), attributes: attDic)
            button?.setAttributedTitle(attStr, for: UIControlState.normal)
            pageIndex = 1
        }
        pageControl?.currentPage = pageIndex
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
