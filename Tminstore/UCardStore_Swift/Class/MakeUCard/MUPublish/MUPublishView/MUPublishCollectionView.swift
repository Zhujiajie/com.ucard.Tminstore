//
//  MUPublishCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/27.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUPublishCollectionView: UICollectionView {

    /// 展示积分购买说明的collectionView
    ///
    /// - Returns: MUPublishCollectionView
    class func publishCollectionView()->MUPublishCollectionView{
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 274/baseWidth, height: 192/baseHeight)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        
        let collectionView: MUPublishCollectionView = MUPublishCollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        collectionView.register(MUPublishCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.MUPublishCollectionViewCellReuseIdentifier)
        collectionView.isScrollEnabled = false
        collectionView.isPagingEnabled = true
        
        return collectionView
    }
    
    //MARK: 发布到社区的说明collectionView
    /// 发布到社区的说明collectionView
    ///
    /// - Returns: MUPublishCollectionView
    class func releaseToCommunituCollection()->MUPublishCollectionView{
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 274/baseWidth, height: 163/baseHeight)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        
        let collectionView: MUPublishCollectionView = MUPublishCollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        collectionView.register(MUNewPulishCollectionViewInfoCell.self, forCellWithReuseIdentifier: appReuseIdentifier.MUNewPulishCollectionViewInfoCellappReuseIdentifier)
        collectionView.register(MUNewPublishCollectionViewPriceCell.self, forCellWithReuseIdentifier: appReuseIdentifier.MUNewPublishCollectionViewPriceCellReuseIdentifier)
        
        collectionView.isPagingEnabled = true
        
        return collectionView

    }
}
