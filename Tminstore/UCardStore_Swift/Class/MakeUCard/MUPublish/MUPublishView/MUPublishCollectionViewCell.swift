//
//  MUPublishCollectionViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/27.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUPublishCollectionViewCell: UICollectionViewCell, UITextFieldDelegate{
    
    fileprivate let num: String = "0123456789"
    
    var indexPath: IndexPath?{
        didSet{
            if indexPath?.item == 0 {
                setIntroLabel()
            }else{
                setCreditLabelAndCreditTextField()
            }
        }
    }
    
    /// 积分获取失败，需要在1~1000之间
    var getPointPriceFailed: (()->())?
    
    /// 成功获得用户输入的积分额度
    var getPointPriceSuccess: ((_ point: Int)->())?
    
    fileprivate var introLabel: UILabel?
    
    fileprivate var creditsTextField: UITextField?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = UIColor.white
     
        introLabel = UILabel()
        introLabel?.textColor = UIColor(hexString: "757575")
        introLabel?.font = UIFont.systemFont(ofSize: 17)
        introLabel?.textAlignment = NSTextAlignment.center
        introLabel?.numberOfLines = 0
        contentView.addSubview(introLabel!)
    }
    
    //MARK: 设置介绍文字
    fileprivate func setIntroLabel(){
        introLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
//        introLabel?.text = NSLocalizedString("快来时记和大家一起分享\n你的美好回忆吧\n它可能会被其他用户看重的哦~", comment: "")
        introLabel?.text = NSLocalizedString("您投递至时空圈的图像\n可以被其他用户购买\n做成纸质AR魔法卡", comment: "")
    }
    
    //MARK: 设置积分购买介绍和积分输入框
    fileprivate func setCreditLabelAndCreditTextField(){
        introLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(contentView)
            make.bottom.equalTo(contentView).offset(-80/baseHeight)
        })
        introLabel?.text = NSLocalizedString("请设置您此次回忆的金额", comment: "")
        
        creditsTextField                = UITextField()
        creditsTextField?.textAlignment = NSTextAlignment.center
        creditsTextField?.borderStyle   = UITextBorderStyle.roundedRect
        creditsTextField?.font          = UIFont.systemFont(ofSize: 12)
        creditsTextField?.keyboardType  = UIKeyboardType.numberPad
        creditsTextField?.placeholder   = NSLocalizedString("点击输入", comment: "")
        
        contentView.addSubview(creditsTextField!)
        creditsTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(introLabel!.snp.bottom)
            make.centerX.equalTo(introLabel!)
            make.size.equalTo(CGSize(width: 100/baseWidth, height: 39/baseWidth))
        })
        
        creditsTextField?.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let str: String = textField.text, str != ""{
            let cs: CharacterSet = CharacterSet(charactersIn: num).inverted//得到除数字之外的集合
            let filterString: String = str.components(separatedBy: cs).joined(separator: "")//过滤其他字符
            textField.text = filterString
            if let pointPrice: Int = Int(filterString){
                if pointPrice < 1 || pointPrice > 1000{
                    getPointPriceFailed?()
                }else{
                    getPointPriceSuccess?(pointPrice)
                }
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        introLabel?.removeFromSuperview()
        creditsTextField?.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 介绍发布到社区业务的cell
let MUNewPulishCollectionViewInfoCellappReuseIdentifier: String = "MUNewPulishCollectionViewInfoCellappReuseIdentifier"

class MUNewPulishCollectionViewInfoCell: UICollectionViewCell{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "757575")
        label.font = UIFont.systemFont(ofSize: 17)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.text = "快来时记和大家一起分享你的美好回忆吧，它可能会被其他用户买下，您将会有一笔收入进账，同时时记会收取10%的平台费。免费赠送3次扫描哦~"
        contentView.addSubview(label)
        label.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(25/baseWidth)
            make.left.equalTo(contentView).offset(12/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
            make.bottom.equalTo(contentView)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: 让用户输入定价的cell
class MUNewPublishCollectionViewPriceCell: UICollectionViewCell, UITextFieldDelegate{
    
    fileprivate let num: String = "0123456789"
    
    /// 积分获取失败，需要在1~1000之间
    var getPointPriceFailed: (()->())?
    
    /// 成功获得用户输入的积分额度
    var getPointPriceSuccess: ((_ point: Int)->())?
    /// 输入定价的textField
    var textField: UITextField?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "757575")
        label.font = UIFont.systemFont(ofSize: 17)
        label.numberOfLines = 0
        label.textAlignment = NSTextAlignment.center
        label.text = "请设置您此次回忆的金额\n金额限度为3-100元"
        contentView.addSubview(label)
        label.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(32/baseWidth)
            make.left.equalTo(contentView).offset(42/baseWidth)
            make.right.equalTo(contentView).offset(-42/baseWidth)
        }
        
        textField = UITextField()
        textField?.placeholder = NSLocalizedString("点击输入", comment: "")
        textField?.keyboardType = UIKeyboardType.numberPad
        textField?.font = UIFont.systemFont(ofSize: 12)
        textField?.textAlignment = NSTextAlignment.center
        textField?.borderStyle = UITextBorderStyle.roundedRect
        contentView.addSubview(textField!)
        textField?.snp.makeConstraints { (make: ConstraintMaker) in
            make.bottom.equalTo(contentView).offset(-10/baseWidth)
            make.centerX.equalTo(contentView)
            make.size.equalTo(CGSize(width: 100/baseWidth, height: 39/baseWidth))
        }
        textField?.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let str: String = textField.text, str != ""{
            let cs: CharacterSet = CharacterSet(charactersIn: num).inverted//得到除数字之外的集合
            let filterString: String = str.components(separatedBy: cs).joined(separator: "")//过滤其他字符
            textField.text = filterString
            if let pointPrice: Int = Int(filterString){
                if pointPrice < 3 || pointPrice > 300{
                    getPointPriceFailed?()
                    textField.text = nil
                }else{
                    getPointPriceSuccess?(pointPrice)
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
