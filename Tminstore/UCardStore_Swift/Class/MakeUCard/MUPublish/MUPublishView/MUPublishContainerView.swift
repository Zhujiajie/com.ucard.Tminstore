//
//  MUPublishContainerView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/26.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUPublishContainerView: UIView {

    /// 点击的事件
    var tapClosure: (()->())?
    
    /// 填写备注的textField结束编辑的闭包
    var textFieldEndEditingClosure: ((_ text: String)->())?
    
    /// 明信片模型
    var finalPostcardModel: MUFinalPostcardModel?
    
    var frontImageView: UIImageView?
    var backImageView: UIImageView?
    
    /// 点击了containerView，切换正反面动画
    var switchAimation: (()->())?
    
    /// 是否是正面
    var isFront: Bool = true
    
    /// 标题的ICON
    fileprivate var tagIconImageView: UIImageView?
    /// 填写标签的textField
    fileprivate var tagTextField: UITextField?

    
    /// 定位标志ICON
    fileprivate var locationIconImageView: UIImageView?
    /// 定位信息
    fileprivate var locationLabel: UILabel?
    /// 定位的加载动画
    fileprivate var locatingActiveView: UIActivityIndicatorView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    //MARK: 设置containerView
    /**
     - returns: MUPreviewView
     */
    class func containerView(finalPostcardModel: MUFinalPostcardModel)-> MUPublishContainerView{
        
        let containerView: MUPublishContainerView = MUPublishContainerView()
        
        containerView.finalPostcardModel = finalPostcardModel
        
        containerView.backgroundColor = UIColor.white
        
        containerView.setImageViews()
        
        //添加点击手势
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: containerView, action: #selector(tapContainerView(_:)))
        tap.cancelsTouchesInView = true
        tap.numberOfTapsRequired = 1
        containerView.addGestureRecognizer(tap)
        
        containerView.finalPostcardModel = finalPostcardModel
        
        return containerView
    }
    
    //MARK: 设置ImageView
    fileprivate func setImageViews(){
        
        frontImageView = UIImageView()
        frontImageView?.backgroundColor = UIColor.white
        frontImageView?.contentMode = UIViewContentMode.scaleToFill
        self.addSubview(frontImageView!)
        frontImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 8/baseHeight, left: 8/baseWidth, bottom: 8/baseHeight, right: 8/baseWidth))
        })
        
        backImageView = UIImageView()
        backImageView?.contentMode = UIViewContentMode.scaleToFill
        self.insertSubview(backImageView!, belowSubview: frontImageView!)
        backImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self).inset(UIEdgeInsets(top: 8/baseHeight, left: 8/baseWidth, bottom: 8/baseHeight, right: 8/baseWidth))
        })
        
        setData()
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        frontImageView?.image = finalPostcardModel?.frontImage
        backImageView?.image  = finalPostcardModel?.backImage
    }
        
    //MARK: 点击了containerView，切换明信片正反面
    func tapContainerView(_ tap: UITapGestureRecognizer) {
        switchAimation?()
        isFront = !isFront
    }
    
    //MARK: 添加标签的视图
    /// 添加标签的视图
    ///
    /// - Returns: MUPublishContainerView
    class func addTagView()->MUPublishContainerView{
        
        let containerView: MUPublishContainerView = MUPublishContainerView()
        containerView.backgroundColor = UIColor.white
        
        containerView.tagIconImageView = UIImageView(image: #imageLiteral(resourceName: "tag"))
        containerView.tagIconImageView?.contentMode = UIViewContentMode.scaleAspectFit
        containerView.addSubview(containerView.tagIconImageView!)
        containerView.tagIconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(containerView).offset(4/baseHeight)
            make.left.equalTo(containerView).offset(20/baseWidth)
            make.size.equalTo(CGSize(width: 18/baseWidth, height: 18/baseWidth))
        })
        
        containerView.tagTextField = UITextField()
        containerView.tagTextField?.placeholder = NSLocalizedString("添加标题", comment: "")
        containerView.tagTextField?.font = UIFont.systemFont(ofSize: 14)
        containerView.addSubview(containerView.tagTextField!)
        containerView.tagTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottomMargin.equalTo(containerView.tagIconImageView!)
            make.left.equalTo(containerView.tagIconImageView!.snp.right).offset(9/baseWidth)
            make.right.equalTo(containerView).offset(-20/baseWidth)
            make.height.equalTo(17)
        })
        containerView.tagTextField?.delegate = containerView
        
        return containerView
    }
    
    //MARK: 设置定位信息的view
    /// 设置定位信息的view
    ///
    /// - Parameter photoLocationText: 相片的定位信息
    /// - Parameter andShouldShowArrow: 是否要展示右箭头
    /// - Returns: MUPublishContainerView
    class func locationView(photoLocationText: String?, andShouldShowArrow shouldShowArrow: Bool = false)->MUPublishContainerView{
        
        let locationView = MUPublishContainerView()
        locationView.backgroundColor = UIColor.white
        
        locationView.locationIconImageView = UIImageView(image: #imageLiteral(resourceName: "publishLocation"))
        locationView.locationIconImageView?.contentMode = UIViewContentMode.scaleAspectFit
        locationView.addSubview(locationView.locationIconImageView!)
        locationView.locationIconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(locationView)
            make.left.equalTo(locationView).offset(18/baseWidth)
            make.size.equalTo(CGSize(width: 17/baseWidth, height: 23/baseWidth))
        })
        
        locationView.locationLabel = UILabel()
        locationView.locationLabel?.textColor = UIColor(hexString: "6D6D6D")
        locationView.locationLabel?.font = UIFont.systemFont(ofSize: 17)
        
        
        if photoLocationText == nil || photoLocationText == ""{
            locationView.locationLabel?.text = NSLocalizedString("无法识别定位信息", comment: "")
        }else{
            locationView.locationLabel?.text = photoLocationText!
        }
        
        let rightArrow: UIImageView = UIImageView(image: UIImage(named: "grayRightArrow"))
        rightArrow.contentMode = UIViewContentMode.scaleAspectFit
        
        locationView.addSubview(locationView.locationLabel!)
        
        if shouldShowArrow == true{
            locationView.addSubview(rightArrow)
            rightArrow.snp.makeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(locationView)
                make.right.equalTo(locationView).offset(-20/baseWidth)
                make.size.equalTo(CGSize(width: 8/baseWidth, height: 13/baseWidth))
            })
            locationView.locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(locationView)
                make.left.equalTo(locationView.locationIconImageView!.snp.right).offset(18/baseWidth)
                make.right.equalTo(rightArrow.snp.left).offset(-5/baseWidth)
            })
            locationView.locationLabel?.numberOfLines = 3
        }else{
            locationView.locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.centerY.equalTo(locationView)
                make.left.equalTo(locationView.locationIconImageView!.snp.right).offset(18/baseWidth)
                make.right.equalTo(locationView).offset(-18/baseWidth)
            })
            locationView.locationLabel?.numberOfLines = 2
        }
        
        //添加手势
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: locationView, action: #selector(locationView.tapSelf(tap:)))
        locationView.addGestureRecognizer(tap)
        
        return locationView
    }
    
    //MARK: 地图发布界面的输入框
    /// 地图发布界面的输入框
    ///
    /// - Returns: MUPublishContainerView
    class func mapReleaseInputView()->MUPublishContainerView{
        
        let containerView: MUPublishContainerView = MUPublishContainerView()
        containerView.backgroundColor = UIColor.white
        
        containerView.tagTextField = UITextField()
        containerView.tagTextField?.placeholder = NSLocalizedString("点击添加文字描述...", comment: "")
        containerView.tagTextField?.font = UIFont.systemFont(ofSize: 12)
        containerView.addSubview(containerView.tagTextField!)
        containerView.tagTextField?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(containerView)
        })
        containerView.tagTextField?.delegate = containerView
        
        return containerView
    }
    
    //MARK: 定位时的加载动画
    func locatingAnimation(){
        
        if locationIconImageView == nil {return}
        
        if locatingActiveView == nil{
            locatingActiveView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
            locatingActiveView?.hidesWhenStopped = true
            addSubview(locatingActiveView!)
            locatingActiveView?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.center.equalTo(locationIconImageView!)
            })
        }
        locatingActiveView?.startAnimating()
        locationIconImageView?.isHidden = true
    }
    
    //MARK: 停止动画，并写入地址信息
    func inputLocationString(locationString: String?){
        if locationIconImageView == nil {return}
        locatingActiveView?.stopAnimating()
        locationIconImageView?.isHidden = false
        if locationString == nil{
            locationLabel?.text = NSLocalizedString("无法识别定位信息", comment: "")
        }else{
            locationLabel?.text = locationString!
        }
    }
    
    //MARK: 点击视图时间
    func tapSelf(tap: UITapGestureRecognizer){
        tapClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
extension MUPublishContainerView: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let text: String = textField.text{
            textFieldEndEditingClosure?(text)
        }
    }
    
    //MARK: 限制可输入的文字为20个
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount: Int = textField.text?.characters.count ?? 0
        if (range.length + range.location) > currentCharacterCount{
            return false
        }
        let newLength: Int = currentCharacterCount + string.characters.count - range.length
        return newLength <= 40
    }
}

