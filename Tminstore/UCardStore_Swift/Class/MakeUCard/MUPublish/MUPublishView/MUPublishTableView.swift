//
//  MUPublishTableView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/27.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUPublishTableView: UITableView {

    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        
        register(MUNewPublishTableViewContentCell.self, forCellReuseIdentifier: appReuseIdentifier.MUNewPublishTableViewContentCellReuseIdentifier)
        register(MUNewPublishTableViewLocationCell.self, forCellReuseIdentifier: appReuseIdentifier.MUNewPublishTableViewLocationCellReuseIdentifier)
        register(MUNewPublishTableViewPrivateCell.self, forCellReuseIdentifier: appReuseIdentifier.MUNewPublishTableViewPrivateCellReuseIdentifier)
        
        estimatedRowHeight = 327/baseWidth
        
        separatorStyle = UITableViewCellSeparatorStyle.none
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
