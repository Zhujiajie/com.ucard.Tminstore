//
//  MUPublishButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/12/26.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit

class MUPublishButton: UIButton {

    /// 点击按钮的事件
    var touchButton: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 7/baseWidth, bottom: 0, right: 0)
        addTarget(self, action: #selector(touchButtonAction(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 发布到时空圈的按钮
    /// 发布到时空圈的按钮
    ///
    /// - Returns: MUPublishButton
    class func publishButton()->MUPublishButton{
        let publishButton: MUPublishButton = MUPublishButton()
        publishButton.setImage(#imageLiteral(resourceName: "publishIcon"), for: UIControlState.normal)
        
        let str: String    = NSLocalizedString("投递时空圈", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName: UIColor(hexString: "565656")] as [String: Any]
        let attStr:NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        
        publishButton.setAttributedTitle(attStr, for: UIControlState.normal)
        
        return publishButton
    }

    //MARK: 寄送给好友的按钮
    /// 寄送给好友的按钮
    ///
    /// - Returns: MUPublishButton
    class func mailButton()->MUPublishButton{
        let mailButton: MUPublishButton = MUPublishButton()
        mailButton.setImage(#imageLiteral(resourceName: "mailIcon"), for: UIControlState.normal)
        
        let str: String    = NSLocalizedString("寄给好友", comment: "")
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName: UIColor(hexString: "565656")] as [String: Any]
        
        let attStr:NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        
        mailButton.setAttributedTitle(attStr, for: UIControlState.normal)
        mailButton.addTarget(mailButton, action: #selector(mailButton.touchButtonAction(button:)), for: UIControlEvents.touchUpInside)
        
        return mailButton
    }
    
    //MARK: 积分说明页面的按钮
    /// 积分说明页面的按钮
    ///
    /// - Parameter isConfirm: 是否是确认按钮
    /// - Returns: MUPublishButton
    class func creditIntrolButton(isConfirm: Bool)->MUPublishButton{
        
        let button: MUPublishButton = MUPublishButton()
        button.backgroundColor = UIColor(hexString: "6ED7E4")
        
        var str: String
        if isConfirm == true{
            str = NSLocalizedString("确认", comment: "")
        }else{
            str = NSLocalizedString("取消", comment: "")
        }
        let attDic: [String: Any] = [NSFontAttributeName: UIFont.systemFont(ofSize: 17), NSForegroundColorAttributeName: UIColor.white] as [String: Any]
        let attStr:NSAttributedString = NSAttributedString(string: str, attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        
        return button
    }
    
    //MARK: 退出积分说明视图的按钮
    /// 退出积分说明视图的按钮
    ///
    /// - Returns: MUPublishButton
    class func dismissCreditIntrolViewButton()->MUPublishButton{
        let button: MUPublishButton = MUPublishButton()
        button.backgroundColor = UIColor.clear
        button.setImage(#imageLiteral(resourceName: "dismissIntrol"), for: UIControlState.normal)
        return button
    }
    
    //MARK: 点击按钮事件
    func touchButtonAction(button: UIButton){
        touchButton?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
