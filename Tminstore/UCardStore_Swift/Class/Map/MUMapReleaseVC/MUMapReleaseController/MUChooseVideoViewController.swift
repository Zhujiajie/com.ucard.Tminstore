//
//  MUChooseVideoViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MobileCoreServices

class MUChooseVideoViewController: UIImagePickerController {

    override func viewDidLoad() {
        super.viewDidLoad()

        sourceType = UIImagePickerControllerSourceType.photoLibrary
        mediaTypes = [kUTTypeMovie as String]//只选取视频
    }
}
