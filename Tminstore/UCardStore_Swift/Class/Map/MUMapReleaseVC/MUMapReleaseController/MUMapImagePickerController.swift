//
//  MUMapImagePickerController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUMapImagePickerController: UIImagePickerController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let takePhotoIntroView: MUMapTakePhotoIntroView = MUMapTakePhotoIntroView()
        view.addSubview(takePhotoIntroView)
        takePhotoIntroView.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
            make.size.equalTo(CGSize(width: 142/baseWidth, height: 89/baseWidth))
        })
        takePhotoIntroView.isTakePhoto = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sourceType = UIImagePickerControllerSourceType.camera
        showsCameraControls = true
    }
}
