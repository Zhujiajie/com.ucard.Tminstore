//
//  MUMapVideoPickerController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import MobileCoreServices

class MUMapVideoPickerController: UIImagePickerController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    /// 选择视频的闭包
    var chooseVideoSuccessClosure: ((_ videoURL: URL)->())?
    
    /// 选取视频的界面
    fileprivate var videoPicker: MUChooseVideoViewController?
    
    /// 展示第一个视频的imageView
    fileprivate var videoImageView: MUImageView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let takePhotoIntroView: MUMapTakePhotoIntroView = MUMapTakePhotoIntroView()
        view.addSubview(takePhotoIntroView)
        takePhotoIntroView.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
            make.size.equalTo(CGSize(width: 142/baseWidth, height: 89/baseWidth))
        })
        takePhotoIntroView.isTakePhoto = false
        
        setButtons()
        
        closures()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sourceType = UIImagePickerControllerSourceType.camera
        allowsEditing = true
        showsCameraControls = true
        mediaTypes = [kUTTypeMovie as String]
        videoMaximumDuration = videoMaxDuration//最长60秒录制时间
        videoQuality = UIImagePickerControllerQualityType.typeIFrame960x540
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //完成录制的通知
        NotificationCenter.default.addObserver(self, selector: #selector(captureDidStart(notification:)), name: NSNotification.Name("_UIImagePickerControllerUserDidCaptureItem"), object: nil)
        //重新录制的通知
        NotificationCenter.default.addObserver(self, selector: #selector(rejectItem(notification:)), name: NSNotification.Name("_UIImagePickerControllerUserDidRejectItem"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if presentedViewController?.isKind(of: MUChooseVideoViewController.self) == true || presentedViewController?.isKind(of: MUCropVideoVC.self) == true{
            videoImageView?.alpha = 0.0
        }else{
            videoImageView?.alpha = 1.0//离开视图是，将视频选择视图重置
        }
    }
    
    //MARK: 开始录制视频
    func captureDidStart(notification: Notification){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.videoImageView?.alpha = 0.0
        }, completion: nil)
    }
    
    //MARK: 重新开始录制
    func rejectItem(notification: Notification){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.videoImageView?.alpha = 1.0
            }, completion: nil)
    }
    
    //MARK: 设置按钮
    fileprivate func setButtons(){
        videoImageView = MUImageView.videoImageView()
        view.addSubview(videoImageView!)
        videoImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(view).offset(70/baseWidth)
            make.bottom.equalTo(view).offset(-20/baseWidth)
            make.size.equalTo(CGSize(width: 60/baseWidth, height: 60/baseWidth))
        })
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击视频图片的事件
        videoImageView?.tapImageViewClosure = {[weak self] _ in
            if self?.videoPicker == nil{
                self?.videoPicker = MUChooseVideoViewController()
                self?.videoPicker?.delegate = self
            }
            DispatchQueue.main.async {
                self?.present((self?.videoPicker)!, animated: true, completion: nil)
            }
        }
        
        //MARK: 成功获取到用户的第一个视频缩略图
        videoImageView?.fetchImageSuccess = {[weak self] _ in
            UIView.animate(withDuration: 0.5, delay: 1.0, options: [UIViewAnimationOptions.curveEaseOut], animations: {
                self?.videoImageView?.alpha = 1
            }, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let url: URL = info[UIImagePickerControllerMediaURL] as? URL{
            //判断视频是否短于5秒
            let asset: AVAsset = AVAsset(url: url)
            let time: Float64 = CMTimeGetSeconds(asset.duration)
            //若视频短于5秒，重新进入视频拍摄界面
            if time <= 5 {
                alert(alertTitle: NSLocalizedString("视频不能短于5秒", comment: ""), leftClosure: { [weak self] _ in
                    self?.dismiss(animated: false, completion: {
                        self?.present((self?.videoPicker)!, animated: false, completion: nil)
                    })
                })
                return
            }
            
            //进入视频裁剪界面
            let videoCropVC: MUCropVideoVC = MUCropVideoVC()
            videoCropVC.videoURL = url
            videoCropVC.shouldUseDismiss = true
            videoCropVC.isComeFromMapView = true
            videoCropVC.umengViewName = "视频剪切"
            let nav: UINavigationController = UINavigationController(rootViewController: videoCropVC)
            dismiss(animated: true, completion: { [weak self] _ in
                self?.present(nav, animated: true, completion: nil)
            })
            //MARK: 在裁剪界面点取消，重新开始录制视频
            videoCropVC.reRecordClosure = {[weak self] _ in
                self?.dismiss(animated: true, completion: nil)
            }
            //MARK: 裁剪视频成功
            videoCropVC.getVideoSuccess = {[weak self] (videoPathURL: URL) in
                self?.chooseVideoSuccessClosure?(videoPathURL)
            }
        }else{
            
            dismiss(animated: true, completion: {[weak self] _ in
                self?.alert(alertTitle: "不支持的视频格式")
            })
        }
    }
    
    //MARK: 创建提示框
    /**
     创建提示框
     
     - parameter title:      标题
     - parameter message:    信息
     - parameter leftTitle:  左按钮标题 默认 字符串 “OK” or “好的”
     - parameter rightTitle: 右按钮标题
     - parameter left:       左按钮点击事件
     - parameter right:      右按钮点击事件
     - parameter complition: 提示框弹出完成之后的闭包
     */
    func alert(alertTitle title: String?, messageString message: String? = nil, leftButtonTitle leftTitle: String = NSLocalizedString("好的", comment: ""), rightButtonTitle rightTitle: String? = nil, leftClosure left: (()->())? = nil, rightClosure right: (()->())? = nil, presentComplition complition: (()->())? = nil){
        
        let ac: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        
        let leftAc: UIAlertAction = UIAlertAction(title: leftTitle, style: .cancel) { (_) in
            left?()
        }// 左按钮
        ac.addAction(leftAc)
        
        if (rightTitle != nil) {
            let rightAc: UIAlertAction = UIAlertAction(title: rightTitle, style: .default, handler: { (_) in
                right?()
            })//右按钮
            ac.addAction(rightAc)
        }
        
        present(ac, animated: true) {
            complition?()
        }//提示框弹出结束后的事件
    }
}
