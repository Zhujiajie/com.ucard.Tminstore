//
//  MapReleaseController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import MapKit
import CoreLocation
import IQKeyboardManagerSwift

class MapReleaseController: BaseViewController {

    fileprivate let mapViewModel: MapViewModel = MapViewModel()
    
    fileprivate let viewModel: MapReleaseViewModel = MapReleaseViewModel()
    
    var photo: UIImage?
    
    var videoPathURL: URL?
    
    ///底部灰色线条
    fileprivate var grayLine: UIView?
    /// 地图
    fileprivate var mapView: MKMapView?
    ///输入描述的界面
    fileprivate var descriptionView: MUPublishContainerView?
    /// 展示定位信息的视图
    fileprivate var locationView: MUPublishContainerView?
    ///发布的按钮
    fileprivate var releaseButton: MapViewButton?
    /// 发布成功的弹窗
    fileprivate var releaseSuccessAlert: MainView?
    
    /// 搜索位置的控制器
    fileprivate lazy var locationSearchVC: MapLocationViewController = MapLocationViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        grayLine = UIView()
        grayLine?.backgroundColor = UIColor(hexString: "#F4F7F9")
        view.addSubview(grayLine!)
        grayLine?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.left.right.equalTo(view)
            make.height.equalTo(8/baseWidth)
        })
        setMapView()
        setImputTextView()
        setReleaseButton()
        closures()
    }
    
    //MARK: 设置地图
    fileprivate func setMapView(){
        
        mapView = MKMapView(frame: CGRect.init(x: 8/baseWidth, y: 17/baseWidth, width: 360/baseWidth, height: 242/baseWidth))
        
        //地图不能交互
        mapView?.showsPointsOfInterest = false//不展示景点
        mapView?.showsBuildings = false//不展示建筑物
        mapView?.isRotateEnabled = false//可以旋转
        mapView?.isPitchEnabled = false//不可以选择摄像头
        mapView?.isZoomEnabled = false
        mapView?.isScrollEnabled = false
        mapView?.delegate = self
        view.addSubview(mapView!)
        
        setMapViewRegin()//设置定位位置
    }

    //MARK: 设置输入框
    fileprivate func setImputTextView(){
        descriptionView = MUPublishContainerView.mapReleaseInputView()
        view.addSubview(descriptionView!)
        descriptionView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(mapView!.snp.bottom).offset(7/baseWidth)
            make.left.equalTo(view).offset(9/baseWidth)
            make.right.equalTo(view).offset(-9/baseWidth)
            make.height.equalTo(54/baseWidth)
        })
        
        //分隔线
        let grayLine01: UIView = UIView()
        grayLine01.backgroundColor = grayLine?.backgroundColor
        view.addSubview(grayLine01)
        grayLine01.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(descriptionView!.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(2)
        }
        
        locationView = MUPublishContainerView.locationView(photoLocationText: " ", andShouldShowArrow: true)
        view.addSubview(locationView!)
        locationView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(grayLine01.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(108/baseWidth)
        })
        
        //分隔线
        let grayLine02: UIView = UIView()
        grayLine02.backgroundColor = grayLine?.backgroundColor
        view.addSubview(grayLine02)
        grayLine02.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(locationView!.snp.bottom)
            make.left.right.equalTo(view)
            make.height.equalTo(2)
        }
    }
    
    //MARK: 设置发布按钮
    fileprivate func setReleaseButton(){
        releaseButton = MapViewButton.releaseButton()
        view.addSubview(releaseButton!)
        releaseButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-35/baseWidth)
            make.size.equalTo(CGSize(width: 335/baseWidth, height: 56/baseWidth))
        })
        releaseButton?.alpha = 0.5
        releaseButton?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        IQKeyboardManager.sharedManager().enable = true
        
        //加载默认的地址信息
        if viewModel.detailAddress == ""{
            viewModel.requsetLocationInfo(location: currentUserLocation!)
        }
        
        //加载提示图片
        if hasLoadReleaseToMapInfoImageView() == false{
            let infoImageView: MapReleaseInfoView = MapReleaseInfoView(frame: UIScreen.main.bounds)
            navigationController?.view.addSubview(infoImageView)
            setLoadReleaseToMapInfoImageView()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
//        IQKeyboardManager.sharedManager().enable = false
    }

    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("发布", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 发布成功的弹窗
    fileprivate func setReleaseSuccessAlert(annotation: ZTAnnotation){
        releaseSuccessAlert = MainView.releareToMapSuccessAlert(frame: UIScreen.main.bounds)
        navigationController?.view.addSubview(releaseSuccessAlert!)
        releaseSuccessAlert?.touchPurchaseButtonClosure = {[weak self] _ in
            self?.mapViewModel.requestVideoURL(annotation: annotation)//下载AR识别图
        }
    }
    
    //MARK: 设置mapView的位置
    fileprivate func setMapViewRegin(){
        //先删除原先的 annotation
        if let annotation: ZTAnnotation = mapView?.annotations.first as? ZTAnnotation{
            mapView?.removeAnnotation(annotation)
        }
        
        //显示精度
        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005)
        let region: MKCoordinateRegion = MKCoordinateRegionMake(currentUserLocation!.coordinate, span)
        mapView?.setRegion(region, animated: true)
        
        let annotation: ZTAnnotation = ZTAnnotation(coordinate: currentUserLocation!.coordinate, title: nil, subTitle: nil)
        mapView?.addAnnotation(annotation)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 获得图片的备注
        descriptionView?.textFieldEndEditingClosure = {[weak self] (text: String) in
            self?.viewModel.frontImageRemark = text
        }
        
        //MARK: 点击发布按钮的事件
        releaseButton?.touchButtonClosure = {[weak self] _ in
            self?.viewModel.releaseToMap(photo: (self?.photo)!, andVideoPathURL: (self?.videoPathURL)!)
        }
        
        //MARK: 点击换地址
        locationView?.tapClosure = {[weak self] _ in
            _ = self?.navigationController?.pushViewController((self?.locationSearchVC)!, animated: true)
            //MARK: 成功获得地址信息
            self?.locationSearchVC.chooseLocation = {(info: AMapPOI) in
                //保存当前定位信息
                currentUserLocation = CLLocation(latitude: CLLocationDegrees(info.location.latitude), longitude: CLLocationDegrees(info.location.longitude))
                self?.locationView?.inputLocationString(locationString: info.name + "\n" + info.address)
                self?.viewModel.detailAddress = info.name + "\n" + info.address
                self?.setMapViewRegin()
            }
        }
        
        //MARK: 成功得到地址信息
        viewModel.getLocationInfoSuccess = {[weak self] (locationString: String?) in
            self?.locationView?.inputLocationString(locationString: locationString)
            self?.releaseButton?.alpha = 1.0
            self?.releaseButton?.isEnabled = true
        }
        
        //MARK: 出错的闭包
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 上传成功的闭包
        viewModel.releaseSuccess = {[weak self] (annotation: ZTAnnotation) in
            self?.setReleaseSuccessAlert(annotation: annotation)
        }
        
        //MARK: 下载AR识别图成功
        mapViewModel.downLoadARImageSusscess = {[weak self] (imagePath: String, annotation: ZTAnnotation) in
            let localAR: LocalARVC = LocalARVC()
            localAR.arImagePath = imagePath
            localAR.annotation = annotation
            localAR.isComeFromReleaseVC = true
            let nav: UINavigationController = UINavigationController(rootViewController: localAR)
            self?.present(nav, animated: true, completion: nil)
        }
        
        //MARK: 弹出活动窗口
        viewModel.activityAlert = {[weak self] (annotation: ZTAnnotation) in
            let vc: ActivityVC = ActivityVC()
            vc.backImage = self?.getScreenImage()
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self?.present(vc, animated: true, completion: nil)
            vc.dismissClosure = {[weak self] _ in
                self?.setReleaseSuccessAlert(annotation: annotation)
            }
        }
    }
}

extension MapReleaseController: MKMapViewDelegate{
    //MARK: 在地图上添加一个点
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: ZTAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: appReuseIdentifier.annotationReuseIndetifier) as? ZTAnnotationView
        if annotationView == nil {
            annotationView = ZTAnnotationView(annotation: annotation, reuseIdentifier: appReuseIdentifier.annotationReuseIndetifier)
            annotationView?.leftCalloutAccessoryView = nil
            annotationView?.rightCalloutAccessoryView = nil
        }
        annotationView?.image = UIImage(named: "userReleaseAnnotation")
        return annotationView
    }
}
