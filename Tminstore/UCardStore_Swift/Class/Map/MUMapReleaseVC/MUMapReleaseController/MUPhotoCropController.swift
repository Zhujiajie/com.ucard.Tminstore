//
//  MUPhotoCropController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MUPhotoCropController: TOCropViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        aspectRatioPreset = TOCropViewControllerAspectRatioPreset.preset7x5
        aspectRatioLockEnabled = true
        resetAspectRatioEnabled = false
    }
    
}
