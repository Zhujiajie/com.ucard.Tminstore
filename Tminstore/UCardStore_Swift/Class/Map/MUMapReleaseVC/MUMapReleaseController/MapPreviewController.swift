//
//  MapPreviewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MapPreviewController: BaseViewController {
    
    fileprivate let viewModel: MUPublishViewModel = MUPublishViewModel()
    
    /// 裁剪好的视频
    var videoPathURL: URL?{
        didSet{
            if videoPathURL != nil{
                backgroundImageView?.removeFromSuperview()
                backgroundImageView = nil
            }
        }
    }
    
    /// 用户拍摄的照片
    var photo: UIImage?{
        didSet{
            imageView?.image = photo
//            imageIsOK = false
        }
    }
    
    /// 背景图片
    var backgroundImage: UIImage?
    
    /// 背景视图
    fileprivate var backgroundImageView: UIImageView?
    
    fileprivate lazy var easyARModel: EasyARModel = EasyARModel()
    
    /// 图片识别图是否可以
    fileprivate var imageIsOK: Bool = true//暂时取消图片识别度测试
    
    /// 展示明信片正反面的视图
    fileprivate var imageView: MapImageView?
    /// 播放按钮
    fileprivate var playButton: MUPreviewButton?
    
    /// 承载播放器的视图
    fileprivate var playerContainerView: MUPreviewPlayView?
    /// 播放器
    fileprivate var player: ZTPlayer?
    
    fileprivate var animationModel: MUPublishAnimationModel?
    
    /// 继续发布的按钮
    fileprivate var continueButton: MapViewButton?
    /// 更换AR视频的按钮
    fileprivate var changeARVideoButton: MapViewButton?
    
    /// 拍照的控制器
    fileprivate var imagePickerController: MUMapImagePickerController?
    /// 录制视频的控制器
    fileprivate var videoRecordPickerController: MUMapVideoPickerController?
    
    /// 图片裁切的界面
    fileprivate var imageCropVC: MUPhotoCropController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        setNavigationBar()
        setImageViewAndButton()
        closures()
        setAnimationModel()//设置动画模型
        
        backgroundImageView = UIImageView(image: backgroundImage)
        backgroundImageView?.contentMode = UIViewContentMode.scaleToFill
        navigationController?.view.addSubview(backgroundImageView!)
        backgroundImageView?.frame = UIScreen.main.bounds
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if photo == nil && imagePickerController == nil{
            imagePickerController = MUMapImagePickerController()
//            imagePickerController?.sourceType = UIImagePickerControllerSourceType.camera
//            imagePickerController?.showsCameraControls = true
            imagePickerController?.delegate = self
            present(imagePickerController!, animated: true, completion: nil)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("预览", comment: ""), attributeDic: attDic)
    }
    
    //MARK: 设置图片
    fileprivate func setImageViewAndButton(){
        imageView = MapImageView(frame: CGRect.zero)
        view.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.top.equalTo(43/baseWidth)
            make.size.equalTo(CGSize(width: screenWidth, height: 265/baseWidth))
        })
        
        playButton = MUPreviewButton.playButton()
        imageView?.addSubview(playButton!)
        playButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(imageView!).offset(29/baseWidth)
            make.bottom.equalTo(imageView!).offset(-10/baseWidth)
            make.size.equalTo(CGSize(width: 33/baseWidth, height: 33/baseWidth))
        })
        
        changeARVideoButton = MapViewButton.changeARViewButton()
        view.addSubview(changeARVideoButton!)
        changeARVideoButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-35/baseWidth)
            make.size.equalTo(CGSize(width: 335/baseWidth, height: 56/baseWidth))
        })
        
        continueButton = MapViewButton.continueButton()
        view.addSubview(continueButton!)
        continueButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(changeARVideoButton!)
            make.bottom.equalTo(changeARVideoButton!.snp.top).offset(-14/baseWidth)
            make.size.equalTo(changeARVideoButton!)
        })
    }
    
    //MARK: 设置动画模型
    fileprivate func setAnimationModel(){
        animationModel = MUPublishAnimationModel()
        animationModel?.superView = view
    }
    
    //MARK: 设置播放器相关
    fileprivate func setPlayer(){
        
        if playerContainerView != nil{
            playerContainerView?.removeFromSuperview()
            playerContainerView = nil
        }
        
        playerContainerView = MUPreviewPlayView.playerView(videoPath: videoPathURL!)
        
        view.addSubview(playerContainerView!)
        playerContainerView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(view)
            make.size.equalTo(CGSize(width: 1, height: 1))
        })
        view.setNeedsUpdateConstraints()//约束需要更新
        view.updateConstraintsIfNeeded()//检测需要更新的约束
        view.layoutIfNeeded()//强制更新约束
        
        navigationController?.navigationBar.layer.zPosition = -1//将视频播放界面推到最前端
        
        player = playerContainerView?.player
        self.addChildViewController(player!)
        player?.didMove(toParentViewController: self)
        
        animationModel?.playerContainerView = playerContainerView
        animationModel?.playerAnimationAppear()//进入播放界面
        playerClousre()//必须在这里调用
    }
    
    //MARK: 点击导航栏左按钮，提示用户是否退出
    override func navigationBarLeftBarButtonAction(_ button: UIBarButtonItem) {
        dismissSelf()
//        deleteAlert(alertTitle: NSLocalizedString("确定取消发布？", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("继续", comment: ""), rightButtonTitle: NSLocalizedString("取消", comment: ""), leftClosure: nil, rightClosure: {[weak self] _ in
//            self?.dismissSelf()
//        }, presentCompletion: nil)
    }
}

extension MapPreviewController: UIGestureRecognizerDelegate{
    
    //MARK: 各种回调
    func closures(){
        
        //MARK: 点击图片重新拍照
        imageView?.tapImageViewClosure = {[weak self] _ in
            if self?.imagePickerController == nil{
                self?.imagePickerController = MUMapImagePickerController()
                self?.imagePickerController?.delegate = self
            }
            self?.present((self?.imagePickerController)!, animated: true, completion: nil)
        }
        
        //MARK: 点击更换AR视频的按钮，更换视频
        changeARVideoButton?.touchButtonClosure = {[weak self] _ in
            //成功获取到照片，进入视频录制界面
            if self?.videoRecordPickerController == nil{
                self?.videoRecordPickerController = MUMapVideoPickerController()
                self?.videoRecordPickerController?.delegate = self
            }
            self?.present((self?.videoRecordPickerController)!, animated: true, completion: nil)
            
            //视频录制界面，裁剪成功
            self?.videoRecordPickerController?.chooseVideoSuccessClosure = {(videoURL: URL) in
                self?.videoPathURL = videoURL
                self?.dismiss(animated: true, completion: nil)
            }
        }
        
        //MARK: 点击播放按钮，开始播放视频
        playButton?.touchPlayButton = {[weak self] _ in
            self?.setPlayer()
        }
        
        //MARK: 网络请求出错
        viewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 点击继续按钮的事件
        continueButton?.touchButtonClosure = {[weak self] _ in
            if self?.imageIsOK == false{
                self?.checkImage()
            }else{
                self?.enterReleaseVC()
            }
        }
    }
    
    // MARK: 检查照片的可识别度
    fileprivate func checkImage(){
        
        if photo == nil {return}
        
        //压缩检测的图片
        let image: UIImage = UIImage(image: photo!, scaledToWidth: 200)
        
        showSVProgress(title: NSLocalizedString("检测AR图片中，请稍后", comment: ""))
        
        easyARModel.easyARDetection(image: image, successClosure: { [weak self] _ in
            self?.enterReleaseVC()
            self?.imageIsOK = true
            }, failureClourse: {[weak self] (error: Error?) in
            self?.handelNetWorkError(error)
            }, lowGradeClosure: {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("AR照片识别度太低，请重新选择照片", comment: ""))
            }, completeClosure: {[weak self] _ in
                self?.dismissSVProgress()
        })
    }
    
    //MARK: 进入发布界面
    fileprivate func enterReleaseVC(){
        let releaseVC: MapReleaseController = MapReleaseController()
        releaseVC.photo = photo
        releaseVC.videoPathURL = videoPathURL
        _ = navigationController?.pushViewController(releaseVC, animated: true)
    }
    
    //MARK: 播放器相关闭包
    func playerClousre() {
        //MARK: 在播放界面点击退出，然后销毁播放器
        playerContainerView?.tapToDismiss = {[weak self] _ in
            self?.animationModel?.dismissPlayer(complete: {
                self?.navigationController?.navigationBar.layer.zPosition = 0//恢复导航栏的位置
                self?.playerContainerView?.firePlayer()
                self?.playButton?.isHidden = false
                self?.player = nil
            })
        }
    }
    
    //MARK: 询问用户是否退出
    fileprivate func askWhetherToQuit(picker: UIImagePickerController){
        dismiss(animated: true, completion: {[weak self] _ in
            if self?.photo == nil || self?.videoPathURL == nil{
                //询问用户是否继续
//                self?.deleteAlert(alertTitle: NSLocalizedString("确定取消发布？", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("继续", comment: ""), rightButtonTitle: NSLocalizedString("取消", comment: ""), leftClosure: {                    self?.present(picker, animated: true, completion: nil)
//                }, rightClosure: {[weak self] _ in
//                    self?.dismiss(animated: false, completion: nil)
//                    }, presentCompletion: nil)
                self?.dismissSelf(animated: false, completion: nil)
            }
        })
    }
}

//MARK: UIImagePickerControllerDelegate, UINavigationControllerDelegate
extension MapPreviewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        askWhetherToQuit(picker: picker)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        //拍照的控制器
        if picker.isKind(of: MUMapImagePickerController.self) == true{
            if let image: UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                imageCropVC = MUPhotoCropController(croppingStyle: TOCropViewCroppingStyle.default, image: image)
                imageCropVC?.delegate = self
                picker.present(imageCropVC!, animated: true, completion: nil)
            }
        }
        //录像的控制器
        else if picker.isKind(of: MUMapVideoPickerController.self) == true{
            if let url: URL = info[UIImagePickerControllerMediaURL] as? URL{
                //判断视频是否短于5秒
                let asset: AVAsset = AVAsset(url: url)
                let time: Float64 = CMTimeGetSeconds(asset.duration)
                //若视频短于5秒，重新进入视频拍摄界面
                if time <= 5 {
                    dismiss(animated: false, completion: { [weak self] _ in
                        self?.alert(alertTitle: NSLocalizedString("视频不能短于5秒", comment: ""), leftClosure: { [weak self] _ in
                            self?.present((self?.videoRecordPickerController)!, animated: false, completion: nil)
                        })
                    })
                    return
                }
                
                //进入视频裁剪界面
                let videoCropVC: MUCropVideoVC = MUCropVideoVC()
                videoCropVC.videoURL = url
                videoCropVC.shouldUseDismiss = true
                videoCropVC.isComeFromMapView = true
                videoCropVC.umengViewName = "视频剪切"
                let nav: UINavigationController = UINavigationController(rootViewController: videoCropVC)
                videoRecordPickerController?.present(nav, animated: true, completion: nil)
                //MARK: 在裁剪界面点取消，重新开始录制视频
                videoCropVC.reRecordClosure = {[weak self] _ in
                    self?.dismiss(animated: false, completion: { 
                        self?.present((self?.videoRecordPickerController)!, animated: false, completion: nil)
                    })
                }
                //MARK: 裁剪视频成功
                videoCropVC.getVideoSuccess = {[weak self] (videoPathURL: URL) in
                    self?.videoPathURL = videoPathURL
                    self?.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}

extension MapPreviewController:  TOCropViewControllerDelegate{
    
    //MARK: 若没有照片。则询问用户是否退出
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        if photo == nil{
            askWhetherToQuit(picker: imagePickerController!)
        }else{
            dismiss(animated: true, completion: nil)
        }
    }
    //MARK: 成功获得照片
    func cropViewController(_ cropViewController: TOCropViewController, didCropToImage image: UIImage, rect cropRect: CGRect, angle: Int) {
        
        photo = image
        
//        UIImageWriteToSavedPhotosAlbum(photo!, nil, nil, nil)//保存拍摄的照片
        
        dismiss(animated: true, completion: nil)
        
        if videoPathURL != nil {return}//如果已经录制过视频的，直接跳过
        
        //成功获取到照片，进入视频录制界面
        if videoRecordPickerController == nil{
            videoRecordPickerController = MUMapVideoPickerController()
            videoRecordPickerController?.delegate = self
        }
        present(videoRecordPickerController!, animated: true, completion: nil)
        
        //MARK: 视频录制界面，裁剪成功
        videoRecordPickerController?.chooseVideoSuccessClosure = {[weak self] (videoURL: URL) in
            self?.videoPathURL = videoURL
            self?.dismiss(animated: true, completion: nil)
        }
        
    }
}
