//
//  MapReleaseDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import Alamofire
import CoreLocation

class MapReleaseDataModel: BaseDataModel {

    ///成功获得定位信息的闭包
    var getLocationInfoSuccess: ((_ location: String?)->())?
    
    /// 获得上传结果
    var getReleaseResult: ((_ result: [String: Any])->())?
    
    /// 图片的描述
    var frontImageRemark: String = ""
    
    /// 将经纬度转换为详细地址
    fileprivate let geocoder: CLGeocoder = CLGeocoder()
    fileprivate var userLocation: CLLocation?
    
    var detailAddress: String = ""
    
//    /// 当前城市的中文名称
//    fileprivate var cityInCN: String = ""
//    fileprivate var stateInCN: String = ""
//    fileprivate var countryInCn: String = ""
//    /// 当前城市的英文名称
//    fileprivate var cityInEn: String = ""
//    fileprivate var stateInEn: String = ""
//    fileprivate var countryInEn: String = ""
    
    /// 当前城市的详细信息
    var city: String = ""
    var province: String = ""
    var country: String = ""
    
    fileprivate var frontImageURLString: String?
    fileprivate var videoURLString: String?
    fileprivate var videoThumbnailURLString: String?
    
    fileprivate var frontImageKey: String?
    fileprivate var videoThumbnailKey: String?
    fileprivate var videoKey: String?
    
    fileprivate var photo: UIImage?
    fileprivate var videoPathURL: URL?
    
    //MARK: 发布到地图
    func releaseToMap(photo: UIImage, andVideoPathURL videoPathURL: URL){
        
        self.photo = photo
        self.videoPathURL = videoPathURL
        
        //设置上传的路径
        if frontImageURLString == nil{
            frontImageKey = mapFrontImageUpun + UUID().uuidString + ".png"
            frontImageURLString = appAPIHelp.upyunBase + frontImageKey!
        }
        if videoURLString == nil{
            videoKey = mapVideoUpun + UUID().uuidString + ".mp4"
            videoURLString = appAPIHelp.upyunBase + videoKey!
        }
        if videoThumbnailURLString == nil{
            videoThumbnailKey = mapVideoThumbnailUpun + UUID().uuidString + ".png"
            videoThumbnailURLString = appAPIHelp.upyunBase + videoThumbnailKey!
        }
        if detailAddress == ""{
            setDetailAddressText()
        }
        
        uploadFrontImage()
    }
    
    //MARK: 上传正面图片
    fileprivate func uploadFrontImage(){
        
        //压缩图片
        let newImage: UIImage = UIImage(image: photo!, scaledToWidth: 1000)
        
        guard let imageData: Data = UIImagePNGRepresentation(newImage) else {
            errorClosure?(nil)
            return
        }
        
        uploadToUpyun(data: imageData, andSaveKey: frontImageKey!, andParameters: nil, successClosure: { [weak self] (_, _) in
            self?.uploadThumbnail()
            }, failClosure: { [weak self] (error: Error?, _, _) in
            self?.errorClosure?(error)
            }, progressClosure: {(completeCount: Float, totalCount: Float) in
                SVProgressHUD.showProgress(completeCount/totalCount/Float(2), status: NSLocalizedString("上传图片中", comment: ""))
        })
    }
    
    //MARK: 上传视频缩略图到又拍云
    fileprivate func uploadThumbnail(){
        
        guard let thumbnailImage: UIImage = getThumbnailImage(videoPath: videoPathURL!.path), let imageData: Data = UIImagePNGRepresentation(thumbnailImage) else {
            errorClosure?(nil)
            return }
        
        uploadToUpyun(data: imageData, andSaveKey: videoThumbnailKey!, andParameters: nil, successClosure: {[weak self] (_, _) in
            self?.uploadVideo()
            }, failClosure: { [weak self] (error: Error?, _, _) in
            self?.errorClosure?(error)
            }, progressClosure: {(completeCount: Float, totalCount: Float) in
                SVProgressHUD.showProgress(completeCount/totalCount/Float(2) + 0.5, status: NSLocalizedString("上传图片中", comment: ""))
        })
    }
    
    //MARK: 上传视频到又拍云
    fileprivate func uploadVideo(){
        
        do{
            let videoData: Data = try Data.init(contentsOf: videoPathURL!, options: [])
            
            let parameters: [String: Any] = [
                "name": "naga",
                "type": "video",
                "avopts": "/wmImg/L2lPU0ltYWdlcy93YXRlcm1hcmswMS5wbmc=/wmGravity/northwest/wmDx/20/wmDy/15",
                "save_as": videoKey!
            ]
            
            uploadToUpyun(data: videoData, andSaveKey: videoKey!, andParameters: ["apps": [parameters]], successClosure: { [weak self] (_, _) in
                self?.releaseToMap()
                }, failClosure: { [weak self] (error: Error?, _, _) in
                self?.errorClosure?(error)
                }, progressClosure: {(completeCount: Float, totalCount: Float) in
                    SVProgressHUD.showProgress(completeCount/totalCount, status: NSLocalizedString("上传AR视频中", comment: ""))
            })
            
        }catch{errorClosure?(error)}
    }
    
    //MARK: 发布到地图的请求
    fileprivate func releaseToMap(){
        
        if languageCode() == "CN"{
            country = "中国"
        }else{
            country = "China"
        }
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "frontImageUrl": frontImageURLString!,
            "videoUrl": videoURLString!,
            "videoThumbnail": videoThumbnailURLString!,
            "frontImageRemark": frontImageRemark,
            "videoRemark": "",
            "province": province,
            "country": country,
            "city": city,
            "district": "",
            "detailedAddress": detailAddress,
            "longitude": currentUserLocation!.coordinate.longitude,
            "latitude": currentUserLocation!.coordinate.latitude
        ]
        
//        print(parameters)
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.releaseToMapAPI, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String : Any]) in
            self?.getReleaseResult?(result)
//            print(result)
            }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: { [weak self] _ in
                self?.completionClosure?()
        })
    }
    
//    //MARK: 请求定位信息
//    //MARK: 开始反编译地理信息
//    /**
//     开始反编译地理信息
//     */
//    func requestLocationInfo(location: CLLocation?) {
//        //通过谷歌地图API反编译地址，分别获取中文和英文的字段
//        var language: String
//        if getPreferredLanguage()?.contains("zh") == true {
//            language = "zh-CN"
//        }else{
//            language = "en-GB"
//        }
//        requestCityName(language: language, withLocation: location)
//    }
    
//    //MARK: 谷歌API反编译地址
//    /**
//     谷歌API反编译地址
//     
//     - parameter location: 地理位置 CLLocation
//     - parameter language: 英文字段或中文字段
//     */
//    fileprivate func requestCityName(language: String, withLocation location: CLLocation?) {
//        
//        userLocation = location
//        
//        /**
//         谷歌地址反编译
//         GET https://maps.google.cn/maps/api/geocode/json 国内服务器
//         GET https://maps.googleapis.com/maps/api/geocode/json 国外服务器
//         */
//        
//        // Add Headers
//        let headers: [String: String] = [
//            "Content-Type":"application/x-www-form-urlencoded; charset=utf-8",
//            ]
//        
//        //解析出经纬度
//        var latlng: String
//        
//        if let coordinate: CLLocationCoordinate2D = location?.coordinate{
//            latlng = "\(coordinate.latitude),\(coordinate.longitude)"
//        }else{
//            latlng = getLocationString()
//        }
//        
//        // Add URL parameters
//        let urlParams: [String: Any] = [
//            "latlng":latlng,
//            "key":googleAPIKey,
//            "result_type":"locality",
//            "location_type":"APPROXIMATE",
//            "language":language,
//            ]
//        
//        //分别尝试中国和国外的服务器
//        alamofireRequest(url: "https://maps.google.cn/maps/api/geocode/json", withUrlParams: urlParams, withHeaders: headers, withLanguage: language)
//        //        alamofireRequest(url: "https://maps.googleapis.com/maps/api/geocode/json", withUrlParams: urlParams, withHeaders: headers, withLanguage: language)
//    }
    
//    //MARK: 反编译地址的网络请求
//    /**
//     反编译地址的网络请求
//     
//     - parameter url:       服务器地址
//     - parameter urlParams: 参数
//     - parameter headers:   请求头
//     - parameter language:  字段语言
//     */
//    fileprivate func alamofireRequest(url: String, withUrlParams urlParams: [String: Any], withHeaders headers: [String: String], withLanguage language: String){
//        
//        sendGETRequestWithURLString(URLStr: url, ParametersDictionary: urlParams, urlEncoding: URLEncoding.default, successClosure: { [weak self] (result: [String : Any]) in
//            
//            if let resultArray: [[String: Any]] = result["results"] as? [[String: Any]]{
//                
//                if let address_components: [[String: AnyObject]] = resultArray.first?["address_components"] as? [[String: AnyObject]] {
//                    
//                    switch language{
//                        
//                    case "zh-CN":
//                        
//                        for dic: [String: Any] in address_components {
//                            
//                            let type: [String] = dic["types"] as! [String]
//                            
//                            switch type[0]{
//                                
//                            case "locality":
//                                self?.cityInCN = dic["long_name"] as! String
//                            case "administrative_area_level_1":
//                                self?.stateInCN = dic["long_name"] as! String
//                            case "country":
//                                self?.countryInCn = dic["long_name"] as! String
//                            default: break
//                            }
//                        }
//                        
//                    case "en-GB":
//                        
//                        for dic: [String: Any] in address_components {
//                            
//                            let type: [String] = dic["types"] as! [String]
//                            
//                            switch type[0]{
//                                
//                            case "locality":
//                                self?.cityInEn = dic["long_name"] as! String
//                            case "administrative_area_level_1":
//                                self?.stateInEn = dic["long_name"] as! String
//                            case "country":
//                                self?.countryInEn = dic["long_name"] as! String
//                            default: break
//                            }
//                        }
//                        
//                    default: break
//                    }
////                    self?.setDetailAddressText()//通过地址字段，拼接完整的地址
//                }
//            }
//            }, failureClourse: { [weak self] (error: Error?) in
//            self?.errorClosure?(error)
//            self?.getLocationInfoSuccess?(nil)
//            }, completionClosure: {[weak self] _ in
//                self?.completionClosure?()
//        })
//    }
    
    //MARK: 获得详细地址
    /**
     获得详细地址
     */
    fileprivate func setDetailAddressText(){
        if userLocation == nil {return}
        geocoder.reverseGeocodeLocation(userLocation!) { [weak self] (results: [CLPlacemark]?, error: Error?) in
            if let result: CLPlacemark = results?.first, error == nil {
                if let addressArray: [String] = result.addressDictionary?["FormattedAddressLines"] as? [String]{
                    if let address: String = addressArray.first{
                        self?.detailAddress = address
                        self?.getLocationInfoSuccess?(address)
                        return
                    }
                }
            }
            self?.getLocationInfoSuccess?(nil)
        }
    }
    
    //MARK: 获取视频的缩略图
    /// 获取视频的缩略图
    ///
    /// - Parameter videoPath: 视频的路径
    /// - Returns: UIImage?
    fileprivate func getThumbnailImage(videoPath: String)->UIImage?{
        
        let asset: AVAsset = AVAsset(url: URL(fileURLWithPath: videoPath))
        let gen: AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        gen.appliesPreferredTrackTransform = true
        let time: CMTime = CMTime(seconds: 0, preferredTimescale: 600)
        
        var thumnailImage: UIImage?
        
        do {
            let image: CGImage = try gen.copyCGImage(at: time, actualTime: nil)
            thumnailImage = UIImage(cgImage: image)
            return thumnailImage
        }catch{
            errorClosure?(error)
            print(error)
            return nil
        }
    }
}
