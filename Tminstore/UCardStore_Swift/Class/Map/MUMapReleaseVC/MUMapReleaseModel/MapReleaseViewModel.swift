//
//  MapReleaseViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MapReleaseViewModel: BaseViewModel {
    
    fileprivate let locationViewModel: MapLocationViewModel = MapLocationViewModel()
    
    fileprivate let dataModel: MapReleaseDataModel = MapReleaseDataModel()
    
    /// 成功获得地址信息
    var getLocationInfoSuccess: ((_ location: String?)->())?
    
    /// 图片的描述
    var frontImageRemark: String = ""{
        didSet{
            dataModel.frontImageRemark = frontImageRemark
        }
    }
    
    /// 详细地址
    var detailAddress: String = ""{
        didSet{
            dataModel.detailAddress = detailAddress
        }
    }
    
    /// 出错的闭包
//    var errorClosure: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 上传成功
    var releaseSuccess: ((_ annotation: ZTAnnotation)->())?
    /// 需要弹出活动窗口
    var activityAlert: ((_ annotation: ZTAnnotation)->())?
    
    override init() {
        super.init()
        
        //MARK: 成功获得地址信息
        locationViewModel.searchSuccess = {[weak self] _ in
            if let info: AMapPOI = self?.locationViewModel.locationInfos.first{
                self?.getLocationInfoSuccess?(info.name + " \n" + info.address)
                self?.detailAddress = info.name + " \n" + info.address
            }
        }
        
        //MARK: 成功获得详细地址信息
        locationViewModel.getAddressInfoSuccess = {[weak self] (addressComponent: AMapAddressComponent) in
            self?.dataModel.city = addressComponent.city
            self?.dataModel.province = addressComponent.province
        }
        
        //MARK: 停止动画
        dataModel.completionClosure = {[weak self] _ in
            self?.dismissSVProgress()
        }
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            DispatchQueue.main.async {
                self?.errorClosure?(error, nil)
                self?.dismissSVProgress()
            }
        }
        
        //MARK: 上传成功的闭包
        dataModel.getReleaseResult = {[weak self] (result: [String: Any]) in
//            print(result)
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let dic: [String: Any] = data["spaceTimeCircle"] as? [String: Any]{
                        self?.analysisResult(dic: dic, andData: data)
                        return
                    }
                }
            }
            self?.errorClosure?(nil, result)
        }
    }
    
    //MARK: 请求定位的具体信息
    func requsetLocationInfo(location: CLLocation){
//        dataModel.requestLocationInfo(location: location)
        locationViewModel.requestAroundPOI(keyWord: nil)
        locationViewModel.requestLocationInfo()//请求地址信息
    }
    
    //MARK: 发布到地图
    func releaseToMap(photo: UIImage, andVideoPathURL videoPathURL: URL){
        showSVProgress(title: nil)
        dataModel.releaseToMap(photo: photo, andVideoPathURL: videoPathURL)
    }
    
    //MARK: 分析后端返回结果，进入AR本地识别界面
    fileprivate func analysisResult(dic: [String: Any], andData data: [String: Any]){
        let postcardModel: PostcardModel = PostcardModel.initFromSpaceTimeCircle(dic: dic)
        let annotation: ZTAnnotation = ZTAnnotation(coordinate: postcardModel.photoLocation!.coordinate, title: postcardModel.nickName, subTitle: nil)
        annotation.postcardModel = postcardModel
        
        if let activityFlag: Bool = data["activityFlag"] as? Bool, activityFlag == true{
            activityAlert?(annotation)
        }else{
            releaseSuccess?(annotation)
        }
    }
}
