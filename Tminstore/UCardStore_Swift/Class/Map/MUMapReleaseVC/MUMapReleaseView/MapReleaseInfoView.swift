//
//  MapReleaseInfoView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MapReleaseInfoView: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentMode = UIViewContentMode.scaleToFill
        sd_setImage(with: appAPIHelp.releaseInfoImage())
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToDismiss(tap:)))
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
    
    func tapToDismiss(tap: UITapGestureRecognizer){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 0
            }, completion: {[weak self] (_) in
                self?.removeFromSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
