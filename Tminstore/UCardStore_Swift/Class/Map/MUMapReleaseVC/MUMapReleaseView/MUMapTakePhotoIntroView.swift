//
//  MUMapTakePhotoIntroView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MUMapTakePhotoIntroView: UIView {

    /// 是否是拍照界面
    var isTakePhoto: Bool = true{
        didSet{
            setIntroLabel()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(white: 1.0, alpha: 0.83)
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
        
        //点击消失
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapToDismiss(tap:)))
        addGestureRecognizer(tap)
        
        //计时器消失
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(dismissTimer(time:)), userInfo: nil, repeats: false)
    }
    
    //MARK: 设置介绍Label
    fileprivate func setIntroLabel(){
        let label: UILabel = UILabel()
        label.textColor = UIColor(hexString: "858585")
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = NSTextAlignment.center
        if isTakePhoto == true{
            label.text = NSLocalizedString("对你想留下记忆的物品拍一张照片", comment: "")
        }else{
            label.text = NSLocalizedString("给你的拍下的照片添加一段视频记忆", comment: "")
        }
        label.numberOfLines = 0
        addSubview(label)
        label.snp.makeConstraints { (make: ConstraintMaker) in
            make.center.equalTo(self)
            make.size.equalTo(CGSize(width: 115/baseWidth, height: 60/baseWidth))
        }
    }
    
    //MARK: 计时五秒之后消失
    func dismissTimer(time: Timer){
        dismissAniamtion()
    }
    
    //MARK: 点击消失
    func tapToDismiss(tap: UITapGestureRecognizer){
        dismissAniamtion()
    }
    
    //MARK: 消失的动画
    fileprivate func dismissAniamtion(){
        UIView.animate(withDuration: 0.5, animations: { [weak self] _ in
            self?.alpha = 0
            }, completion: {[weak self] (_) in
                self?.removeFromSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
