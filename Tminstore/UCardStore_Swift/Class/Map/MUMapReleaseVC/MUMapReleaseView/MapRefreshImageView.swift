//
//  MapRefreshImageView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MapRefreshImageView: UIImageView {

    ///点击imageView的事件
    var tapImageViewClosure: (()->())?
    
    fileprivate var activityIndicator: UIActivityIndicatorView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        image = UIImage(named: "mapRefresh")
        contentMode = UIViewContentMode.scaleToFill
        isUserInteractionEnabled = true
        
        layer.cornerRadius = 18/baseWidth
        layer.masksToBounds = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapImageView(tap:)))
        addGestureRecognizer(tap)
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        addSubview(activityIndicator!)
        activityIndicator?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(self)
        })
        activityIndicator?.hidesWhenStopped = true
    }
    
    //MARK: 开始刷新动画
    func beginRefresh(){
        
        image = nil
        backgroundColor = UIColor(white: 1.0, alpha: 0.9)
        activityIndicator?.startAnimating()
        isUserInteractionEnabled = false
    }
    
    //MARK: 停止刷新动画
    /// 停止刷新动画
    func stopRefresh(){
        activityIndicator?.stopAnimating()
        image = UIImage(named: "mapRefresh")
        backgroundColor = UIColor.clear
        isUserInteractionEnabled = true
    }
    
    //MARK: 点击imageView的事件
    func tapImageView(tap: UITapGestureRecognizer){
        beginRefresh()
        tapImageViewClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
