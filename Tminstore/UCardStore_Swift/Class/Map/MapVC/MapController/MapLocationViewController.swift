//
//  MapLocationViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MapLocationViewController: BaseViewController, UISearchResultsUpdating, UITableViewDelegate, UITableViewDataSource {

    var chooseLocation: ((_ info: AMapPOI)->())?
    
    fileprivate let viewModel: MapLocationViewModel = MapLocationViewModel()
    
    fileprivate let locationCellReuseIdentifier: String = "locationCellReuseIdentifier"
    
    fileprivate var searchController: UISearchController?
    fileprivate var tableView: UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setNavigationBar()
        
        searchController = UISearchController(searchResultsController: nil)
        searchController?.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        searchController?.searchResultsUpdater = self
        
        tableView = UITableView()
        tableView?.register(MapLocationTableViewCell.self, forCellReuseIdentifier: locationCellReuseIdentifier)
        tableView?.delegate = self
        tableView?.dataSource = self
        tableView?.tableHeaderView = searchController?.searchBar
        view.addSubview(tableView!)
        tableView?.frame = view.bounds
        
        tableView?.tableFooterView = UIView()
        
        //成功返回地址信息，开始展示
        viewModel.searchSuccess = {[weak self] _ in
            self?.tableView?.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.requestAroundPOI(keyWord: nil)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        // 设置左右按钮
        setNavigationBarLeftButtonWithImageName("backArrow")
        navigationItem.leftBarButtonItem?.tintColor = UIColor(hexString: "4CDFED")
        // 设置标题
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4D4D4D"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)] as [String: Any]
        setNavigationAttributeTitle(NSLocalizedString("所在位置", comment: ""), attributeDic: attDic)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.locationInfos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: MapLocationTableViewCell = tableView.dequeueReusableCell(withIdentifier: locationCellReuseIdentifier, for: indexPath) as! MapLocationTableViewCell
        
        let locationInfo: AMapPOI = viewModel.locationInfos[indexPath.row]
        cell.textLabel?.text = locationInfo.name
        cell.detailTextLabel?.text = locationInfo.address + " \(locationInfo.distance) m"
        
        return cell
    }
    
    //MARK: 点击cell，选择地址信息
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chooseLocation?(viewModel.locationInfos[indexPath.row])
        _ = navigationController?.popViewController(animated: true)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.requestAroundPOI(keyWord: searchController.searchBar.text)
    }
}
