//
//  MapCollectionViewController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/27.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage
import SnapKit
import CCHMapClusterController

class MapCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    /// 滚动了collectionView
    var collectionViewScrolled: ((_ indexPath: IndexPath)->())?
    
    /// 选择一个cell
    var didSelecteCell: ((_ originalAnnotation: CCHMapClusterAnnotation, _ selectedAnnotation: ZTAnnotation)->())?
    /// 点击了头像
    var tapUserPortraitClosure: ((_ memberCode: String)->())?
    
    /// 聚合的annotation，用户deselect
    var originalAnnotation: CCHMapClusterAnnotation?
    
    ///数据源
    var annotations: [ZTAnnotation] = [ZTAnnotation](){
        didSet{
            collectionView?.reloadData()
            currentIndexPath = IndexPath(item: 0, section: 0)
        }
    }
    
    /// 当前的indexPath
    var currentIndexPath: IndexPath = IndexPath(item: 0, section: 0){
        didSet{
            collectionView?.scrollToItem(at: currentIndexPath, at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
    }
    
    /// cell的缩进
    fileprivate let cellInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 2, bottom: 0, right: 2)
    
    init() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: screenWidth - 4, height: 326/baseWidth)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumLineSpacing = 4
        super.init(collectionViewLayout: layout)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        collectionView?.isPagingEnabled = true
        collectionView?.backgroundColor = UIColor.clear
        collectionView!.register(MapCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.MapCollectionViewCellReuseIdentifier)
        collectionView?.indicatorStyle = UIScrollViewIndicatorStyle.white//滚动条设置为白色
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return annotations.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MapCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MapCollectionViewCellReuseIdentifier, for: indexPath) as! MapCollectionViewCell
        cell.postcardModel = annotations[indexPath.item].postcardModel
        
        //MARK: 点击cell上的图片
        cell.tapImageClosure = {[weak self] _ in
            self?.didSelecteCell?((self?.originalAnnotation)!, (self?.annotations[indexPath.item])!)
        }
        //MARK: 点击用户头像
        cell.tapUserPortraitClosure = {[weak self] _ in
            let memberCode: String = (self?.annotations[indexPath.item].postcardModel?.memberCode)!
            self?.tapUserPortraitClosure?(memberCode)
        }
        
        return cell
    }

    // MARK: UICollectionViewDelegate
    //MARK: 选择一个cell
//    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        didSelecteCell?(originalAnnotation!, annotations[indexPath.item])
//    }
    
    //MARK: 滚动cell
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let indexPath: IndexPath = IndexPath(item: Int(scrollView.contentOffset.x / screenWidth), section: 0)
        collectionViewScrolled?(indexPath)
    }
    
    //MARK: 每个Cell的缩进
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return cellInsets
    }
}

class MapCollectionViewCell: UICollectionViewCell{
    
    /// 点击图片
    var tapImageClosure: (()->())?
    /// 点击用户头像
    var tapUserPortraitClosure: (()->())?
    
    /// 数据源
    var postcardModel: PostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 用户头像
    fileprivate var userPortrait: BaseImageView?
    /// 用户昵称
    fileprivate var nickNameLabel: BaseLabel?
    /// 用户留下的备注
    fileprivate var remarkLabel: BaseLabel?
    /// 时间
    fileprivate var createTimeLabel: BaseLabel?
    /// 图片
    fileprivate var imageView: BaseImageView?
    /// 详细地址
    fileprivate var detailAddressLabel: BaseLabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.white
        
        let shape: CAShapeLayer = CAShapeLayer()
        shape.path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [UIRectCorner.topLeft, UIRectCorner.topRight], cornerRadii: CGSize(width: 10/baseWidth, height: 10/baseWidth)).cgPath
        contentView.layer.mask = shape
        
        userPortrait = BaseImageView.userPortraitView(cornerRadius: 22/baseWidth)
        contentView.addSubview(userPortrait!)
        userPortrait?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(15/baseWidth)
            make.left.equalTo(contentView).offset(15/baseWidth)
            make.size.equalTo(CGSize(width: 44/baseWidth, height: 44/baseWidth))
        })
        userPortrait?.addTapGesture()
        //MARK: 点击用户头像
        userPortrait?.tapImageView = {[weak self] _ in
            self?.tapUserPortraitClosure?()
        }
        
        createTimeLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "B8B8B8")
        contentView.addSubview(createTimeLabel!)
        createTimeLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView).offset(23/baseWidth)
            make.right.equalTo(contentView).offset(-12/baseWidth)
        })
        
        nickNameLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 18))
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.topMargin.equalTo(userPortrait!)
            make.left.equalTo(userPortrait!.snp.right).offset(10/baseWidth)
        })
        
        remarkLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "A0A0A0")
        contentView.addSubview(remarkLabel!)
        remarkLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(6/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(createTimeLabel!)
        })
        
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        contentView.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(contentView)
            make.top.equalTo(userPortrait!.snp.bottom).offset(10/baseWidth)
        })
        imageView?.addTapGesture()
        //MARK: 点击图片
        imageView?.tapImageView = {[weak self] _ in
            self?.tapImageClosure?()
        }
        
        let maskView: BaseView = BaseView.maskView(alpha: 0.6)
        contentView.addSubview(maskView)
        maskView.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(imageView!)
            make.height.equalTo(64/baseWidth)
        }
        
        let locationIcon: BaseImageView = BaseImageView.normalImageView(andImageName: "clueLocationIcon")
        maskView.addSubview(locationIcon)
        locationIcon.snp.makeConstraints { (make: ConstraintMaker) in
            make.centerY.equalTo(maskView)
            make.left.equalTo(maskView).offset(37/baseWidth)
            make.size.equalTo(CGSize(width: 20/baseWidth, height: 25/baseWidth))
        }
        
        detailAddressLabel = BaseLabel.normalLabel(font: UIFont.systemFont(ofSize: 12), andTextColorHex: "FFFFFF", andNumberOfLines: 2)
        maskView.addSubview(detailAddressLabel!)
        detailAddressLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerY.equalTo(locationIcon)
            make.left.equalTo(locationIcon.snp.right).offset(26/baseWidth)
            make.right.equalTo(maskView).offset(-37/baseWidth)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        userPortrait?.sd_setImage(with: postcardModel?.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
        if let date: Date = postcardModel?.createDate{
            createTimeLabel?.text = getDateText(date: date)
        }
        nickNameLabel?.text = postcardModel?.nickName
        remarkLabel?.text = postcardModel?.remark
        if let url: URL = postcardModel?.frontImageURL{
            //先判断是否已缓存图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                
                if result == true{
                    self?.imageView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.postcardModel?.frontPlaceImageURL{
                        self?.imageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.imageView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.imageView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.imageView?.sd_setImage(with: url)
                    }
                }
            })
        }
        detailAddressLabel?.text = postcardModel?.detailedAddress
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
