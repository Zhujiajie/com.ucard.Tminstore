
//  MapController.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/14.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import MapKit
import SDWebImage
import CCHMapClusterController
import MobileCoreServices

/// 默认最远距离
let maxDistance: Double = 500

class MapController: BaseViewController {
    
    /// 点击导航栏左按钮的事件
    var touchLeftBarButtonItemClosure: (()->())?
    
    /// 退出视图时的闭包
    var dismissClosure: (()->())?
    
    fileprivate let viewModel: MapViewModel = MapViewModel()
    fileprivate let userViewModel: PUserViewModel = PUserViewModel()
    
    /// 导航栏左按钮
    var leftBarButtonItem: BaseBarButtonItem?
//    /// 导航栏右按钮
//    var rightBarButtonItem: BaseBarButtonItem?
    
    /// 底部白色视图
    fileprivate var whiteBottomView: BaseView?
    /// 发布到地图的按钮
    fileprivate var addButton: MapViewButton?
    /// 进入商城的按钮
    fileprivate var marketButton: BaseButton?
    ///进入AR的按钮
    fileprivate var enterARButton: BaseButton?
    
    /// 加载的动画
    fileprivate var loadingAnimationView: MapLoadingView?
    
    /// 地图
    fileprivate var mapView: MKMapView?
    /// 监测heading的manager
    fileprivate var headingManager: CLLocationManager?
    /// 是否需要请求天气信息
    fileprivate var shouldRequestWeatherInfo: Bool = true
    
    /// 合并annotation的算法控制器，普通点
    fileprivate var mapClusterController: CCHMapClusterController?
    /// 特殊组织的聚合点
    fileprivate var annotherClusterController: CCHMapClusterController?
    
    /// 点击annotationView之后弹出的详细视图
    fileprivate var annotationDetailView: ZTAnnotationDetailView?
    
    /// annotation太远，显示的视图
    fileprivate var tooFarDetialView: ZTAnnotaionTooFarView?
    
    /// 刷新视图
    fileprivate var mapRefreshImageView: MapRefreshImageView?
    /// 地图介绍视图
    fileprivate var mapIntroView: MapIntroView?
    /// 展示annotation详情的collectionView
    fileprivate var detailCollectionViewController: MapCollectionViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        setLocation()
        setDismissButtonAndVisualView()
        setRefreshView()
        setMapClusterController()
        
        if #available(iOS 9.0, *) {
            setDetailViews()
        }else{
            if annotationDetailView == nil{
                annotationDetailView = ZTAnnotationDetailView()
            }
        }
        setDetailCollectionViewController()
        closures()
        viewModel.locateToLastLocation()//定位到上一次的位置
        //如果数据为空，则先加载老数据
        if viewModel.annotations.isEmpty == false{
            mapClusterController?.addAnnotations(viewModel.annotations, withCompletionHandler: nil)
            annotherClusterController?.addAnnotations(viewModel.anotherAnnotations, withCompletionHandler: nil)
        }else{
            requestLocation()//请求定位数据
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if mapView?.showsUserLocation == false{
            headingManager?.delegate = self
            mapView?.delegate = self
            mapView?.showsUserLocation = true
            headingManager?.startUpdatingHeading()
        }
        //呈现加载动画
        if loadingAnimationView != nil{
            loadingAnimationView?.loadingAnimation()
        }
        
        //第一次点击后，出现介绍视图
        if hasLoadMapIntroView() == false{
            mapIntroView = MapIntroView()
            self.view.addSubview(mapIntroView!)
            mapIntroView?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.left.bottom.right.equalTo(self.view)
                make.top.equalTo(view).offset(-50/baseWidth)
            })
            setHasLoadMapIntroView()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        headingManager?.delegate = nil
        mapView?.delegate = nil
        
        viewModel.locationManager?.stopUpdatingLocation()
        headingManager?.stopUpdatingHeading()
        
        mapView?.showsUserLocation = false
    }
    
    //MARK: 设置是否有未读消息
    func setMessageStatus(hasNewMessage: Bool){
        var imageName: String
        if hasNewMessage {
            imageName = "NewPerson"
        }else{
            imageName = "Person"
        }
        leftBarButtonItem?.image = UIImage(named: imageName)?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
    }
    
    //MARK: 设置导航栏
    fileprivate func setNavigationBar(){
        
        let titleView: UIImageView = UIImageView(image: UIImage(named: "TimeoryTitle"))
        navigationItem.titleView = titleView
        
        // 自定义导航栏按钮
        leftBarButtonItem = BaseBarButtonItem.showPersonItem()
        navigationItem.leftBarButtonItem = leftBarButtonItem
//        rightBarButtonItem = BaseBarButtonItem.showARItem()
//        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    
    //MARK: 设置定位
    fileprivate func setLocation(){
        
        //监测手机朝向
        headingManager = CLLocationManager()
        headingManager?.requestWhenInUseAuthorization()
        headingManager?.startUpdatingHeading()
        headingManager?.delegate = self
        
        //底部白色视图
        whiteBottomView = BaseView.colorLine(colorString: "#FFFFFF")
        view.addSubview(whiteBottomView!)
        whiteBottomView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(view)
            make.height.equalTo(49/baseWidth)
        })
        
        //地图
        mapView = MKMapView()
        mapView?.showsUserLocation = true//展示用户点
        mapView?.showsPointsOfInterest = false//不展示景点
        mapView?.showsBuildings = false//不展示建筑物
        mapView?.isRotateEnabled = true//可以旋转
        mapView?.isPitchEnabled = false//不可以选择摄像头
        mapView?.layoutMargins = UIEdgeInsets(top: 60/baseWidth, left: 0, bottom: 0, right: 18/baseWidth)//偏移地图的罗盘位置
        
        mapView?.delegate = self
        view.addSubview(mapView!)
        mapView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.top.right.equalTo(view)
            make.bottom.equalTo(whiteBottomView!.snp.top)
        })
    }
    
    //MARK: 设置退出按钮和状态栏下的高斯模糊
    fileprivate func setDismissButtonAndVisualView(){
        
        //发布到地图的按钮
        addButton = MapViewButton.addButton()
        view.addSubview(addButton!)
        addButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.centerX.equalTo(view)
            make.bottom.equalTo(view).offset(-8/baseWidth)
            make.size.equalTo(CGSize(width: 61/baseWidth, height: 61/baseWidth))
        })
        
        //进入商城的按钮
        marketButton = BaseButton.normalIconButton(iconName: "market")
        view.addSubview(marketButton!)
        marketButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.bottom.equalTo(view).offset(-6/baseWidth)
            make.right.equalTo(view).offset(-54/baseWidth)
            make.size.equalTo(CGSize(width: 34/baseWidth, height: 34/baseWidth))
        })
        
        //进入AR的按钮
        enterARButton = BaseButton.normalIconButton(iconName: "enterAR")
        view.addSubview(enterARButton!)
        enterARButton?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(view).offset(55/baseWidth)
            make.bottomMargin.equalTo(marketButton!)
            make.size.equalTo(marketButton!)
        })
    }
    
    //MARK: 设置mapClusterController
    fileprivate func setMapClusterController(){
        mapClusterController = CCHMapClusterController(mapView: mapView!)
        mapClusterController?.delegate = self
        mapClusterController?.isDebuggingEnabled = false
        mapClusterController?.cellSize = 100
        mapClusterController?.marginFactor = 0.5
        mapClusterController?.maxZoomLevelForClustering = 50
        
        annotherClusterController = CCHMapClusterController(mapView: mapView!)
        annotherClusterController?.delegate = self
        annotherClusterController?.isDebuggingEnabled = false
        annotherClusterController?.cellSize = 100
        annotherClusterController?.marginFactor = 0.5
        annotherClusterController?.maxZoomLevelForClustering = 50
    }
    
    //MARK: 定位到当前位置
    fileprivate func setCurrentRegion(region: MKCoordinateRegion, andCoordinate coordinate: CLLocationCoordinate2D){
        //缩放地图并定位
        mapView?.setRegion(region, animated: true)
        //添加淡蓝色图层
        mapView?.removeOverlays(mapView!.overlays)
        let circle: MKCircle = MKCircle(center: coordinate, radius: maxDistance)
        mapView?.add(circle)
    }
    
    //MARK: 进入发布模块
    fileprivate func enterReleaseModel(){
        
        // 请求摄像头权限
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { [weak self](result: Bool) in
            DispatchQueue.main.async {
                if result == true {
                    //进去发布界面
                    let previewVC: MapPreviewController = MapPreviewController()
                    if let nav: BaseNavigationController = self?.navigationController as? BaseNavigationController{
                        previewVC.backgroundImage = nav.getScreenImage()
                    }
                    previewVC.umengViewName = "地图预览"
                    let nav: UINavigationController = UINavigationController(rootViewController: previewVC)
                    self?.present(nav, animated: false, completion: nil)
                }else{
                    //引导用户去开启摄像头权限
                    self?.alert(alertTitle: NSLocalizedString("时记需要获得相册权限才能正常工作哦", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("就不给你权限", comment: ""), rightButtonTitle: NSLocalizedString("这就去设置", comment: ""), leftClosure: nil, rightClosure: {
                        let settingURL: URL = URL(string: UIApplicationOpenSettingsURLString)!
                        if UIApplication.shared.canOpenURL(settingURL){
                            UIApplication.shared.openURL(settingURL)
                        }
                    }, presentComplition: nil)
                }
            }
        }
    }
    
    //MARK: 设置展示annotationView详情的collectionViewController
    fileprivate func setDetailCollectionViewController(){
        detailCollectionViewController = MapCollectionViewController()
        view.addSubview(detailCollectionViewController!.view)
        detailCollectionViewController?.view.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.right.equalTo(view)
            make.height.equalTo(326/baseWidth)
            make.bottom.equalTo(view).offset(330/baseWidth)
        })
    }
    
    //MARK: 展示annotationView详情的collectionViewController出现或者隐藏
    fileprivate func showOrHideDetailCollectionViewController(show: Bool, andAnnotations annotations: [ZTAnnotation]?, andClusterAnnotaton clusterAnnotation: CCHMapClusterAnnotation?){
        if show == true && annotations != nil && clusterAnnotation != nil{
            detailCollectionViewController?.annotations = annotations!
            detailCollectionViewController?.originalAnnotation = clusterAnnotation
        }
        detailCollectionViewController?.view.snp.updateConstraints({ (make: ConstraintMaker) in
            if show{
                make.bottom.equalTo(view)
            }else{
                make.bottom.equalTo(view).offset(330/baseWidth)
            }
        })
        autoLayoutAnimation(view: view)
    }
    
    //MARK: 闭包回调
    fileprivate func closures(){
        
        //MARK: 点击个人按钮的事件
        leftBarButtonItem?.touchItemClosure = { [weak self] _ in
            MobClick.event("enterPerson")
            self?.touchLeftBarButtonItemClosure?()
        }
        
        //MARK: 点击AR按钮，进入AR扫描界面
        enterARButton?.touchButtonClosure = {[weak self] _ in
            MobClick.event("enter_ar")
            self?.enterARVC()
        }
//        rightBarButtonItem?.touchItemClosure = {[weak self] _ in
//            self?.enterARVC()
//        }
        
        //MARK: 点击添加按钮，进入预览界面
        addButton?.touchButtonClosure = {[weak self] _ in
            MobClick.event("add_map")
            self?.enterReleaseModel()
        }
        
        //MARK: 点击商城按钮的事件
        marketButton?.touchButtonClosure = {[weak self] _ in
            MobClick.event("enter_store")
//            self?.alert(alertTitle: NSLocalizedString("功能开发中\n敬请期待", comment: ""))
            let vc: StoreMainController = StoreMainController()
            self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 点击刷新图片，刷新数据
        mapRefreshImageView?.tapImageViewClosure = {[weak self] _ in
            self?.requestLocation()
            //取消annotationView的选中
            if #available(iOS 9.0, *) {
                if let annotation: MKAnnotation = self?.mapView?.selectedAnnotations.first{
                    self?.mapView?.deselectAnnotation(annotation, animated: true)
                }
            } else {
                self?.annotationDetailView?.dismissSelfAnimation()
            }
        }
        
        //MARK: 点击cell，进入AR识别界面
        detailCollectionViewController?.didSelecteCell = {[weak self] 
            (originalAnnotaion: CCHMapClusterAnnotation, annotation: ZTAnnotation) in
            MobClick.event("arImageSelect")
            self?.mapView?.deselectAnnotation(originalAnnotaion, animated: true)
            self?.viewModel.requestVideoURL(annotation: annotation)//下载AR识别图
        }
        
//        //MARK: 点击头像, 进入用户信息界面
//        detailCollectionViewController?.tapUserPortraitClosure = {[weak self] (memberCode: String) in
//            self?.userViewModel.requestOtherUserInfo(memberCode: memberCode)
//        }
        
        //MARK: collectionView滚动了
        detailCollectionViewController?.collectionViewScrolled = {[weak self] (indexPath: IndexPath) in
            self?.annotationDetailView?.selecteCell(indexPath: indexPath)
        }
        
        //MARK: 选择头像，滚动下方的collectionView
        annotationDetailView?.didSelecteCell = {[weak self] (indexPath: IndexPath) in
            self?.detailCollectionViewController?.currentIndexPath = indexPath
        }
        
        //MARK: 成功获取到地图数据
        viewModel.getMapDataSuccess = {[weak self] _ in
            //先移除老的数据
            if let annotations: Set<ZTAnnotation> = self?.mapClusterController?.annotations as? Set<ZTAnnotation>{
                self?.mapClusterController?.removeAnnotations(Array(annotations), withCompletionHandler: {
                    self?.mapClusterController?.addAnnotations((self?.viewModel.annotations)!, withCompletionHandler: nil)
                })
            }
            if let annotations: Set<ZTAnnotation> = self?.annotherClusterController?.annotations as? Set<ZTAnnotation>{
                self?.annotherClusterController?.removeAnnotations(Array(annotations), withCompletionHandler: {
                    self?.annotherClusterController?.addAnnotations((self?.viewModel.anotherAnnotations)!, withCompletionHandler: nil)
                })
            }
            //如果是第一次进入地图，则先加载地图攻略
            if hasLoadMap() == false{
                delay(seconds: 2, completion: { 
                    self?.enterStrategyWebview()
                })
                setHasLoadMap()
            }
        }
        
        //MARK: 加载动画需要停止
        viewModel.loadingAnimationShouldStop = {[weak self] _ in
            self?.mapRefreshImageView?.stopRefresh()
            if self?.loadingAnimationView != nil{
                self?.loadingAnimationView?.stopAnimation()
                self?.loadingAnimationView = nil
            }
        }
        
        //MARK: 错误的闭包
        viewModel.netWorkError = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
        
        //MARK: 下载AR识别图成功
        viewModel.downLoadARImageSusscess = {[weak self] (imagePath: String, annotation: ZTAnnotation) in
            let localAR: LocalARVC = LocalARVC()
            localAR.arImagePath = imagePath
            localAR.annotation = annotation
            let nav: UINavigationController = UINavigationController(rootViewController: localAR)
            self?.present(nav, animated: true, completion: nil)
        }
        
        //MARK: 需要提醒用户提设置摄像头权限
        viewModel.shouldGetCameraAuthorication = {[weak self] _ in
            //引导用户去开启摄像头权限
            self?.alert(alertTitle: NSLocalizedString("AR功能取得摄像头权限才能正常工作", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("就不给你权限", comment: ""), rightButtonTitle: NSLocalizedString("这就去设置", comment: ""), leftClosure: nil, rightClosure: {
                let settingURL: URL = URL(string: UIApplicationOpenSettingsURLString)!
                if UIApplication.shared.canOpenURL(settingURL){
                    UIApplication.shared.openURL(settingURL)
                }
            }, presentComplition: nil)
        }
        
        //MARK: 成功获取定位信息，缩放地图
        viewModel.setMapViewRegion = {[weak self] (region: MKCoordinateRegion, coordinate: CLLocationCoordinate2D) in
            self?.setCurrentRegion(region: region, andCoordinate: coordinate)
        }
        
        //MARK: 地图上数据为空
        viewModel.emptyData = {[weak self] _ in
            self?.alert(alertTitle: NSLocalizedString("空空如也\n现在就留下记忆么？", comment: ""), leftButtonTitle: NSLocalizedString("取消", comment: ""), rightButtonTitle: NSLocalizedString("好的", comment: ""), rightClosure: { 
                self?.enterReleaseModel()
            }, presentComplition: nil)
        }
        
        //MARK: 还未获得定位权限
        viewModel.needLocationAuthorization = {[weak self] _ in
            //引导用户去开启摄像头权限
            self?.alert(alertTitle: NSLocalizedString("地图功能需要取得定位权限才能正常工作", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("就不给你权限", comment: ""), rightButtonTitle: NSLocalizedString("这就去设置", comment: ""), leftClosure: nil, rightClosure: {
                let settingURL: URL = URL(string: UIApplicationOpenSettingsURLString)!
                if UIApplication.shared.canOpenURL(settingURL){
                    UIApplication.shared.openURL(settingURL)
                }
            }, presentComplition: nil)
        }
        
        //MARK: 获取用户信息成功
        userViewModel.getOtherUserInfoSuccess = {[weak self] (userInfoModel: UserInfoModel) in
            let vc: PUserController = PUserController()
            vc.userInfoModel = userInfoModel
            vc.umengViewName = "其他用户信息"
            _ = self?.navigationController?.pushViewController(vc, animated: true)
        }
        
        //MARK: 网络出错
        userViewModel.errorClosure = {[weak self] (error: Error?, result: [String: Any]?) in
            self?.handelNetWorkError(error, withResult: result)
        }
    }
    
    //MARK: 设置详细视图，iOS9以上会调用该方法
    fileprivate func setDetailViews(){
        annotationDetailView = ZTAnnotationDetailView(frame: CGRect(x: 0, y: 0, width: defaultMapDetailViewSize.width, height: defaultMapDetailViewSize.height))
        tooFarDetialView = ZTAnnotaionTooFarView(frame: CGRect(x: 0, y: 0, width: defaultTooIntroViewSize.width, height: defaultTooIntroViewSize.height))
    }
    
    //MARK: 设置详细视图， iOS9以下会调用该方法
    fileprivate func showAnnotationDetailView(annotations: [ZTAnnotation]){
        annotationDetailView?.annotations = annotations
        view.addSubview(annotationDetailView!)
        annotationDetailView?.frame = view.bounds
        annotationDetailView?.showAniamtion()
    }
    
    //MARK: 设置太远的视图，iOS9以下会调用该方法
    fileprivate func showTooFarDetailView(annotation: ZTAnnotation){
        if tooFarDetialView == nil {
            tooFarDetialView = ZTAnnotaionTooFarView()
        }
        tooFarDetialView?.annotation = annotation
        view.addSubview(tooFarDetialView!)
        tooFarDetialView?.frame = view.bounds
        tooFarDetialView?.showAniamtion()
    }
    
    //MARK: 刷新视图
    fileprivate func setRefreshView(){
        mapRefreshImageView = MapRefreshImageView(frame: CGRect(x: 0, y: 0, width: 36/baseWidth, height: 36/baseWidth))
        view.addSubview(mapRefreshImageView!)
        mapRefreshImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(view).offset(16/baseWidth)
            make.bottom.equalTo(whiteBottomView!.snp.top).offset(-34/baseWidth)
            make.size.equalTo(CGSize(width: 36/baseWidth, height: 36/baseWidth))
        })
        
        setLoadingAnimationView()
    }
    
    //MARK: 设置加载动画视图
    fileprivate func setLoadingAnimationView(){
        //加载动画的视图
        loadingAnimationView = MapLoadingView()
        view.addSubview(loadingAnimationView!)
        loadingAnimationView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(view)
        })
    }
    
    //MARK: 定位请求，并请求数据
    func requestLocation(shouldHideUI: Bool = true){
        if loadingAnimationView == nil {
            setLoadingAnimationView()
            loadingAnimationView?.loadingAnimation(shouldHideUI: shouldHideUI)
        }
        mapRefreshImageView?.beginRefresh()
        viewModel.requestLocation()
    }
    
    //MARK: 进入AR界面
    fileprivate func enterARVC(){
        // 请求摄像头权限
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { [weak self](result: Bool) in
            DispatchQueue.main.async {
                if result == true {
                    let arVC: LocalNewARController = LocalNewARController()
                    arVC.umengViewName = "AR"
                    let nav: UINavigationController = UINavigationController(rootViewController: arVC)
                    self?.present(nav, animated: true, completion: nil)
//                    let arVC: EasyARVC = EasyARVC()
//                    arVC.umengViewName = "AR"
//                    self?.present(arVC, animated: true, completion: nil)
                }else{
                    //引导用户去开启摄像头权限
                    self?.alert(alertTitle: NSLocalizedString("AR功能取得摄像头权限才能正常工作", comment: ""), messageString: nil, leftButtonTitle: NSLocalizedString("就不给你权限", comment: ""), rightButtonTitle: NSLocalizedString("这就去设置", comment: ""), leftClosure: nil, rightClosure: {
                        let settingURL: URL = URL(string: UIApplicationOpenSettingsURLString)!
                        if UIApplication.shared.canOpenURL(settingURL){
                            UIApplication.shared.openURL(settingURL)
                        }
                    }, presentComplition: nil)
                }
            }
        }
    }
    
    //MARK: 进入教程网页
    fileprivate func enterStrategyWebview(){
        let webView: BaseWebView = BaseWebView()
        webView.canUsePopToDismiss = true
        webView.urlString = "http://ucardstorevideo.b0.upaiyun.com/iOSImages/strategy.html"
        navigationController?.pushViewController(webView, animated: true)
    }
}

//MARK:MAMapViewDelegate, AMapLocationManagerDelegate, AMapSearchDelegate
extension MapController: MKMapViewDelegate, CLLocationManagerDelegate, CCHMapClusterControllerDelegate{
    
    //MARK: 添加MAAnnotationView
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return viewModel.getAnnotationView(mapView: mapView, viewFor: annotation)
    }
    
    //MARK: 监控手机朝向
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        //获得用户的annotaionView和CLHeading信息
        guard let userLocation: MKAnnotation = mapView?.userLocation, let userView: ZTAnnotationView = mapView?.view(for: userLocation) as? ZTAnnotationView else { return }
        // 旋转annotaionView
        userView.rotateWithHeader(heading: newHeading)
    }
    
    //MARK: 点击图片时的事件
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        MobClick.event("mapSelect")
        if view.annotation?.isKind(of: MKUserLocation.self) == true{return}
        
        //判断要呈现什么视图
        viewModel.displaceDetailCalloutView(mapView: mapView, didSelect: view, loadTooFarView: { [weak self] (annotation: ZTAnnotation) in
            //太远的视图
            if #available(iOS 9.0, *) {
                view.detailCalloutAccessoryView = self?.tooFarDetialView
                self?.tooFarDetialView?.annotation = annotation
            }
            }, loadDetailCalloutView: { [weak self] (annotations: [ZTAnnotation], clusterAnnotaiton: CCHMapClusterAnnotation) in
                //iOS 9的详细视图
                if #available(iOS 9.0, *) {
                    view.detailCalloutAccessoryView = self?.annotationDetailView
                    self?.annotationDetailView?.annotations = annotations
                    self?.showOrHideDetailCollectionViewController(show: true, andAnnotations: annotations, andClusterAnnotaton: clusterAnnotaiton)//出现
                }
            }, loadIOS8TooFarView: { [weak self] (annotation: ZTAnnotation) in
                //展示iOS 8的太远视图
                self?.showTooFarDetailView(annotation: annotation)
            }, loadIOS8DetailView: { [weak self] (annotations: [ZTAnnotation], clusterAnnotaiton: CCHMapClusterAnnotation) in
                //iOS 8的详细视图
                self?.showAnnotationDetailView(annotations: annotations)
                self?.showOrHideDetailCollectionViewController(show: true, andAnnotations: annotations, andClusterAnnotaton: clusterAnnotaiton)//出现
        })
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        showOrHideDetailCollectionViewController(show: false, andAnnotations: nil, andClusterAnnotaton: nil)//隐藏
    }
    
    //MARK: 添加了淡蓝色图层
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        return viewModel.addOverlay(mapView: mapView, rendererFor: overlay)
    }
    
    //MARK: ZTAnnotationView复用时
    func mapClusterController(_ mapClusterController: CCHMapClusterController!, willReuse mapClusterAnnotation: CCHMapClusterAnnotation!) {
        if let clusterAnnotationView: ZTAnnotationView = mapView?.view(for: mapClusterAnnotation) as? ZTAnnotationView{
            if let annotation: ZTAnnotation = mapClusterAnnotation.annotations.first as? ZTAnnotation{
                if annotation.postcardModel?.userType == UserType.business{
                    clusterAnnotationView.iconURL = annotation.postcardModel?.annotationIconURL
                }else{
                    clusterAnnotationView.count = mapClusterAnnotation.annotations.count
                }
            }
        }
    }
    func mapClusterController(_ mapClusterController: CCHMapClusterController!, titleFor mapClusterAnnotation: CCHMapClusterAnnotation!) -> String! {
        return NSLocalizedString(" 附近的时光记忆", comment: "")
    }
}
