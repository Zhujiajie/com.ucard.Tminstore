//
//  ZTAnnotationDetailView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import CCHMapClusterController

/// 默认的mapDetailView大小
let defaultMapDetailViewSize: CGSize = CGSize(width: screenWidth, height: 62/baseWidth)

class ZTAnnotationDetailView: UIView {
    
    /// 选中了cell
    var didSelecteCell: ((_ indexPath: IndexPath)->())?
    
    /// 数据源
    var annotations: [ZTAnnotation] = [ZTAnnotation](){
        didSet{
            selectedIndexPath = IndexPath(item: 0, section: 0)
            collectionView?.reloadData()
            
            if annotations.isEmpty == false{
                collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
            }
            
            switch annotations.count {
            case 1:
                widthConstraint?.constant = defaultMapDetailViewSize.width/6
            case 2:
                widthConstraint?.constant = defaultMapDetailViewSize.width/6 * 2
            case 3:
                widthConstraint?.constant = defaultMapDetailViewSize.width/6 * 3
            case 4:
                widthConstraint?.constant = defaultMapDetailViewSize.width/6 * 4
            case 5:
                widthConstraint?.constant = defaultMapDetailViewSize.width/6 * 5
            default:
                widthConstraint?.constant = defaultMapDetailViewSize.width
            }
            updateConstraints()
        }
    }
    
    /// 宽的约束
    fileprivate var widthConstraint: LayoutConstraint?
    /// 用户展示用户头像的collectionView
    fileprivate var collectionView: MapAnnotationCollectionView?
    /// 被选中的cell的indexPath
    fileprivate var selectedIndexPath: IndexPath?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        
        collectionView = MapAnnotationCollectionView()
        addSubview(collectionView!)
        
        
        if #available(iOS 9.0, *) {
            
            collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.edges.equalTo(self)
            })
            
            //添加约束
            widthConstraint = LayoutConstraint(item: self, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: defaultMapDetailViewSize.width)
            widthConstraint?.isActive = true

            let heightConstraint: LayoutConstraint = LayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: defaultMapDetailViewSize.height)
            heightConstraint.isActive = true
            
        }else{
            self.alpha = 0
            // 点击退出
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapSelfToDismiss(tap:)))
            self.addGestureRecognizer(tap)
            tap.cancelsTouchesInView = false
            
            collectionView?.snp.makeConstraints({ (make: ConstraintMaker) in
                make.center.equalTo(self)
                make.size.equalTo(defaultMapDetailViewSize)
            })//加载iOS8 的UI
        }

        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    func tapSelfToDismiss(tap: UITapGestureRecognizer){
        dismissSelfAnimation()
    }
    
    func touchDismissButton(button: UIButton){
        dismissSelfAnimation()
    }
    
    //MARK: 选择cell，出现边框
    /// 选择cell，出现边框
    ///
    /// - Parameter indexPath: 选中的indexPath
    func selecteCell(indexPath: IndexPath){
        selectedIndexPath = indexPath
        collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.left, animated: true)
        if let cell: MapAnnotationCollectionViewCell = collectionView?.cellForItem(at: indexPath) as? MapAnnotationCollectionViewCell{
            NotificationCenter.default.post(name: NSNotification.Name(MapAnnotationCollectionViewCellSetBorder), object: nil)
            cell.setBorder()
        }
    }
    
    //MARK: 退出视图
    func dismissSelfAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 0
            }, completion: {[weak self] (_) in
                self?.removeFromSuperview()
        })
    }
    
    //MARK: 出现动画
    func showAniamtion(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 1
        }, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ZTAnnotationDetailView: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return annotations.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MapAnnotationCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: appReuseIdentifier.MapAnnotationCollectionViewCellReuseIdentifier, for: indexPath) as! MapAnnotationCollectionViewCell
        cell.postcardModel = annotations[indexPath.item].postcardModel
        //判断是否设置cell的选中边框
        if selectedIndexPath != nil{
            if indexPath == selectedIndexPath!{
                cell.setBorder()
            }else{
                cell.cancelBorderAction()
            }
        }
        return cell
    }
    
    //MARK: 点击cell，设置边框
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        didSelecteCell?(indexPath)
        if let cell: MapAnnotationCollectionViewCell = collectionView.cellForItem(at: indexPath) as? MapAnnotationCollectionViewCell{
            NotificationCenter.default.post(name: NSNotification.Name(MapAnnotationCollectionViewCellSetBorder), object: nil)
            cell.setBorder()
        }
    }
}
