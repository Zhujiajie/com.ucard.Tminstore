//
//  ZTAnnotaionTooFarView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/22.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

/// 默认的提示距离太远的视图大小
let defaultTooIntroViewSize: CGSize = CGSize(width: 231/baseWidth, height: 89/baseWidth)

class ZTAnnotaionTooFarView: UIView {

    /// 位置信息
    var annotation: ZTAnnotation?{
        didSet{
            if annotation != nil{
                setData()
            }
        }
    }
    
    /// 预览
    fileprivate var previewImageView: UIImageView?
    /// 昵称
    fileprivate var nickNameLabel: UILabel?
    /// 地点和距离
    fileprivate var locationLabel: UILabel?
    /// 将经纬度转换为详细地址
    fileprivate var geocoder: CLGeocoder?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        if #available(iOS 9.0, *) {
        }else{
            self.alpha = 0
            // 点击退出
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapSelfToDismiss(tap:)))
            self.addGestureRecognizer(tap)
        }
        
        backgroundColor = UIColor.white
//        layer.cornerRadius = 5
//        layer.masksToBounds = true
        
        let containerView: UIView = UIView()
        containerView.backgroundColor = UIColor(hexString: "4CDFED")
        addSubview(containerView)
        containerView.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.left.right.equalTo(self)
            make.height.equalTo(57/baseWidth)
        }
        
        // 头像
        previewImageView = UIImageView()
        previewImageView?.contentMode = UIViewContentMode.scaleAspectFill
        containerView.addSubview(previewImageView!)
        previewImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(containerView)
            make.left.equalTo(containerView)
            make.size.equalTo(CGSize(width: 70/baseWidth, height: 57/baseWidth))
        })
        
        //昵称
        nickNameLabel = UILabel()
        nickNameLabel?.textColor = UIColor.white
        nickNameLabel?.font = UIFont.systemFont(ofSize: 16)
        containerView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(containerView).offset(5/baseWidth)
            make.left.equalTo(previewImageView!.snp.right).offset(10/baseWidth)
            make.right.equalTo(containerView).offset(-10/baseWidth)
            make.height.equalTo(18/baseWidth)
        })
        
        // 位置信息
        locationLabel = UILabel()
        locationLabel?.textColor = UIColor.white
        locationLabel?.font = UIFont.systemFont(ofSize: 10)
        locationLabel?.numberOfLines = 2
        containerView.addSubview(locationLabel!)
        locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom).offset(7/baseWidth)
            make.leftMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
            make.bottom.equalTo(containerView)
        })
        
        //提示太远
        let introLabel: UILabel = UILabel()
        introLabel.textColor = UIColor(hexString: "44E1EA")
        introLabel.font = UIFont.systemFont(ofSize: 10)
        addSubview(introLabel)
        introLabel.snp.makeConstraints { (make: ConstraintMaker) in
            make.top.equalTo(containerView.snp.bottom).offset(10/baseWidth)
            make.centerX.equalTo(self)
        }
        introLabel.text = NSLocalizedString("距离太远，走近些发现他（她）的记忆", comment: "")
        
        //添加约束
        if #available(iOS 9.0, *) {
            let widthConstraint: LayoutConstraint = LayoutConstraint(item: self, attribute: NSLayoutAttribute.width, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: defaultTooIntroViewSize.width)
            addConstraint(widthConstraint)
            let heightConstraint: LayoutConstraint = LayoutConstraint(item: self, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: defaultTooIntroViewSize.height)
            addConstraint(heightConstraint)
        }
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
    
        if let url: URL = annotation?.postcardModel?.frontImageURL{
            //先判断是否已换成图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                
                if result == true{
                    self?.previewImageView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.annotation?.postcardModel?.frontPlaceImageURL{
                        self?.previewImageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.previewImageView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.previewImageView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.previewImageView?.sd_setImage(with: url)
                    }
                }
            })
        }
        
        nickNameLabel?.text = annotation?.postcardModel?.nickName
        
        locationLabel?.text = annotation!.locationString
        if annotation?.locationString != " "{ return }

        // 将经纬度转换为详细的地址
        if geocoder == nil{
            geocoder = CLGeocoder()
        }
        if let coordinate: CLLocationCoordinate2D = annotation?.coordinate{
            let location: CLLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            //解析出地址信息
            geocoder?.reverseGeocodeLocation(location, completionHandler: { [weak self] ( results:[CLPlacemark]?, error: Error?) in
                if let result: CLPlacemark = results?.first, error == nil {
                    if let addressArray: [String] = result.addressDictionary?["FormattedAddressLines"] as? [String]{
                        if let address: String = addressArray.first{
                            self?.annotation?.locationString = address
                        }
                    }
                }
                
                //获取距离
                if let userLocation: CLLocation = self?.annotation?.userLocation{
                    let distance: CLLocationDistance = userLocation.distance(from: location)
                    self?.annotation?.locationString += " (\(Int(distance)) m)"
                }
                DispatchQueue.main.async {
                    self?.locationLabel?.text = (self?.annotation?.locationString)!
                }
            })
        }
    }
    
    func tapSelfToDismiss(tap: UITapGestureRecognizer){
        dismissSelfAnimation()
    }
    
    //MARK: 退出视图
    fileprivate func dismissSelfAnimation(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 0
            }, completion: {[weak self] (_) in
                self?.removeFromSuperview()
        })
    }
    
    //MARK: 出现动画
    func showAniamtion(){
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: { [weak self] _ in
            self?.alpha = 1
            }, completion: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
