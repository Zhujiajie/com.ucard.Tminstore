//
//  MapImageView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/23.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MapImageView: UIImageView {

    /// 点击imageView的事件
    var tapImageViewClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.white
        contentMode = UIViewContentMode.scaleAspectFill
        layer.masksToBounds = true
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapImageView(tap:)))
        addGestureRecognizer(tap)
        
        isUserInteractionEnabled = true
        
        sd_setImage(with: appAPIHelp.sampleImageURL())//设置样本图片
    }
    
    //MARK: 点击imageView的事件
    func tapImageView(tap: UITapGestureRecognizer){
        tapImageViewClosure?()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
