//
//  MapWeatherView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MapWeatherView: UIView {

    /// 天气icon
    fileprivate var weatherImageView: UIImageView?
    /// 气温
    fileprivate var temperatureLabel: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor(white: 1.0, alpha: 0.97)
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        
        weatherImageView = UIImageView()
        weatherImageView?.contentMode = UIViewContentMode.scaleAspectFit
        self.addSubview(weatherImageView!)
        weatherImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(self).offset(12)
            make.centerY.equalTo(self)
            make.size.equalTo(CGSize(width: 25, height: 25))
        })
        
        temperatureLabel = UILabel()
        temperatureLabel?.textColor = UIColor.black
        temperatureLabel?.font = UIFont.systemFont(ofSize: 18)
        self.addSubview(temperatureLabel!)
        temperatureLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(weatherImageView!.snp.right).offset(6)
            make.centerY.equalTo(weatherImageView!)
            make.height.equalTo(weatherImageView!)
        })
    }
    
    
//    override init(effect: UIVisualEffect?) {
//        let blur: UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
//        super.init(effect: blur)
//        
//        self.layer.cornerRadius = 8
//        self.layer.masksToBounds = true
//        
//        weatherImageView = UIImageView()
//        weatherImageView?.contentMode = UIViewContentMode.scaleAspectFit
//        self.contentView.addSubview(weatherImageView!)
//        weatherImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
//            make.left.equalTo(self.contentView).offset(12)
//            make.centerY.equalTo(self.contentView)
//            make.size.equalTo(CGSize(width: 25, height: 25))
//        })
//        
//        temperatureLabel = UILabel()
//        temperatureLabel?.textColor = UIColor.black
//        temperatureLabel?.font = UIFont.systemFont(ofSize: 18)
//        self.contentView.addSubview(temperatureLabel!)
//        temperatureLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
//            make.left.equalTo(weatherImageView!.snp.right).offset(6)
//            make.centerY.equalTo(weatherImageView!)
//            make.height.equalTo(weatherImageView!)
//        })
//    }
    
    //MARK: 绘制展示天气的视图
    class func weatherView(weatherCode: String, andTemperature temperature: String)->MapWeatherView{
        let weatherView: MapWeatherView = MapWeatherView()
        let urlStr: String = "http://ucardstorevideo.b0.upaiyun.com/iOSImages/weatherIcon/\(weatherCode).png"
        weatherView.weatherImageView?.sd_setImage(with: URL(string: urlStr))
        weatherView.temperatureLabel?.text = "\(temperature)°"
        return weatherView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
