//
//  MapViewButton.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import ChameleonFramework

class MapViewButton: UIButton {

    /// 点击按钮的事件
    var touchButtonClosure: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        addTarget(self, action: #selector(touchButton(button:)), for: UIControlEvents.touchUpInside)
    }
    
    //MARK: 退出的按钮
    /// 退出的按钮
    ///
    /// - Returns: MapViewButton
    class func dismissButton()->MapViewButton{
        //退出按钮
        let dismissButton: MapViewButton = MapViewButton()
        dismissButton.setImage(UIImage(named: "mapDismiss"), for: UIControlState.normal)
        
        return dismissButton
    }

    //MARK: 教程按钮
    /// 教程按钮
    ///
    /// - Returns: MapViewButton
    class func introButton()->MapViewButton{
        let button: MapViewButton = MapViewButton()
        button.setImage(UIImage(named: "mapIntro"), for: UIControlState.normal)
        
        return button
    }
    
    //MARK: 添加的按钮
    /// 添加的按钮
    ///
    /// - Returns: MapViewButton
    class func addButton()->MapViewButton{
        let button: MapViewButton = MapViewButton()
        button.setImage(UIImage(named: "mapAdd"), for: UIControlState.normal)
        return button
    }
    
    //MARK: 预览界面，继续的按钮
    /// 预览界面，继续的按钮
    ///
    /// - Returns: MapViewButton
    class func continueButton()->MapViewButton{
        
        let button: MapViewButton = MapViewButton()
        //按钮的颜色为渐变
        button.backgroundColor = UIColor(gradientStyle: UIGradientStyle.leftToRight, withFrame: CGRect.init(x: 0, y: 0, width: 335/baseWidth, height: 56/baseWidth), andColors: [UIColor(hexString: "#82F3ED"), UIColor(hexString: "#75EDEE")])
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor.white, NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("继续", comment: ""), attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        
        return button
    }
    
    //MARK: 预览界面，更换AR视频的按钮
    /// 预览界面，更换AR视频的按钮
    ///
    /// - Returns: MapViewButton
    class func changeARViewButton()->MapViewButton{
        
        let button: MapViewButton = MapViewButton()
        button.backgroundColor = UIColor.white
        button.layer.borderColor = UIColor(hexString: "9BFFEB").cgColor
        button.layer.borderWidth = 2
        button.layer.masksToBounds = true
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4CDFED"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("更换AR视频", comment: ""), attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        
        return button
    }
    
    //MARK: 发布界面发布的按钮
    /// 发布界面发布的按钮
    ///
    /// - Returns: MapViewButton
    class func releaseButton()->MapViewButton{
        let button: MapViewButton = MapViewButton()
        button.backgroundColor = UIColor.white
        button.layer.borderColor = UIColor(hexString: "9BFFEB").cgColor
        button.layer.borderWidth = 2
        button.layer.masksToBounds = true
        
        let attDic: [String: Any] = [NSForegroundColorAttributeName: UIColor(hexString: "4CDFED"), NSFontAttributeName: UIFont.systemFont(ofSize: 18)]
        let attStr: NSAttributedString = NSAttributedString(string: NSLocalizedString("发布到地图", comment: ""), attributes: attDic)
        button.setAttributedTitle(attStr, for: UIControlState.normal)
        
        return button
    }
    
    //MARK: 点击退出按钮
    func touchButton(button: UIButton){
        touchButtonClosure?()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
