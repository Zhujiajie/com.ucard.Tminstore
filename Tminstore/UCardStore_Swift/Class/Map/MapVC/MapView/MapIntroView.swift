//
//  MapIntroView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/4/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 地图上的介绍视图

import UIKit
import SnapKit

class MapIntroView: BaseIntroView {

    fileprivate var imageView: BaseImageView?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.clear
        imageView = BaseImageView.normalImageView(contentMode: UIViewContentMode.scaleAspectFill)
        addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(self)
        })
        imageView?.sd_setImage(with: appAPIHelp.mapIntroImageURL)
        
        tap?.isEnabled = false
        
        delay(seconds: 3, completion: { [weak self] _ in
            self?.tap?.isEnabled = true
        }, completionBackground: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
