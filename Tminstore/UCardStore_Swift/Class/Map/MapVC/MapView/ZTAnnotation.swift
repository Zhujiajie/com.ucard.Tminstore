//
//  ZTAnnotation.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MapKit

class ZTAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    /// 存放数据的模型
    var postcardModel: PostcardModel?{
        didSet{
            if let urlString: String = postcardModel?.videoURL?.absoluteString{
                defalutVideoURLPath = urlString
            }
        }
    }
    
    /// 默认的视频地址
    var defalutVideoURLPath: String = ""
    
    /// 根据经纬度转换来的地址信息
    var locationString: String = " "
    
    /// 用户的地理位置
    var userLocation: CLLocation?{
        get{
            return currentUserLocation
        }
    }
    
    init(coordinate: CLLocationCoordinate2D, title: String? = nil, subTitle: String? = nil) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subTitle
    }
}
