//
//  ZTAnnotationView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import MapKit
import SnapKit
import CCHMapClusterController

/// 默认的AnnotationView大小
let defaultAnnotationViewSize: CGSize = CGSize(width: 30, height: 40)

class ZTAnnotationView: MKAnnotationView {

    /// 用来决定是否展示详细视图。弱引用
    weak var mapView: MKMapView?
    
    /// 包含的Annotation数量
    var count: Int = 0{
        didSet{
            if count != 0{
//                canShowCallout = true
                if count < 10{
                    canShowCallout = true
                    imageView?.image = UIImage(named: "annotationImageView")
                    countLabel?.text = "\(count)"
                }else{
                    //根据mapView的缩放程度，决定是否可以展示详细视图
                    if mapView!.region.span.latitudeDelta > 0.001{
                        canShowCallout = false
                    }else{
                        canShowCallout = true
                    }
                    
                    imageView?.image = UIImage(named: "manyAnnotationImageView")
                    countLabel?.text = ""
                }
            }else{
                countLabel?.text = ""
//                canShowCallout = false
            }
        }
    }
    /// icon的路径
    var iconURL: URL?{
        didSet{
            if iconURL != nil{
                canShowCallout = true
            }
            imageView?.sd_setImage(with: iconURL)
        }
    }
    
    /// 信封图片
    fileprivate var imageView: UIImageView?
    /// 包含多少识别图片
    fileprivate var countLabel: BaseLabel?
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        self.frame.size = defaultAnnotationViewSize
        
        imageView = UIImageView()
        imageView?.contentMode = UIViewContentMode.scaleAspectFit
        imageView?.isUserInteractionEnabled = true
        self.addSubview(imageView!)
        imageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(self)
            make.size.equalTo(defaultAnnotationViewSize)
        })
        
        var font: UIFont
        if #available(iOS 8.2, *) {
            font = UIFont.systemFont(ofSize: 14, weight: UIFontWeightMedium)
        } else {
            font = UIFont.systemFont(ofSize: 14)
        }
        countLabel = BaseLabel.normalLabel(font: font, andTextColorHex: "7AEFFF")
        imageView?.addSubview(countLabel!)
        countLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(imageView!).offset(5)
            make.centerX.equalTo(imageView!)
        })
        
        centerOffset = CGPoint(x: -9, y: 0)
        
//        if let annotation: ZTAnnotation = annotation as? ZTAnnotation{
//            
//        }
    }
    
    /// 根据heading信息而选择用户的AnnotationView
    ///
    /// - Parameter heading: CLHeading
    func rotateWithHeader(heading: CLHeading){
        
        //将设备的方向角度换算成弧度
        let headings: Double = Double.pi * heading.magneticHeading / 180.0
        //创建可以旋转的CALayer动画
        let rotateAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform")
        
        //动画的起始值
        let fromValue: CATransform3D = self.layer.transform
        rotateAnimation.fromValue = fromValue
        
        //绕z轴旋转heading弧度的变换矩阵
        let toValue: CATransform3D = CATransform3DMakeRotation(CGFloat(headings), 0, 0, 1)
        //设置动画结束值
        rotateAnimation.toValue = NSValue(caTransform3D: toValue)
        rotateAnimation.duration = 0.35
        rotateAnimation.isRemovedOnCompletion = true
        
        //保存旋转后的transform
        self.layer.transform = toValue
        
        self.layer.add(rotateAnimation, forKey: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        countLabel?.text = ""
        imageView?.image = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
