//
//  ZTAnnotationTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit
import AddressBookUI
import SDWebImage

class ZTAnnotationTableViewCell: UITableViewCell {

    /// 定位信息
    var annotation: ZTAnnotation?{
        didSet{
            if annotation != nil{
                setDate()
            }
        }
    }
    
    /// 头像
    fileprivate var previewImageView: UIImageView?
    /// 昵称
    fileprivate var nickNameLabel: UILabel?
    /// 地点和距离
    fileprivate var locationLabel: UILabel?
    /// 将经纬度转换为详细地址
    fileprivate var geocoder: CLGeocoder?
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = UITableViewCellSelectionStyle.none
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor(hexString: "4CDFED")
        
        let whiteLine: UIView = UIView()
        whiteLine.backgroundColor = UIColor.white
        contentView.addSubview(whiteLine)
        whiteLine.snp.makeConstraints { (make: ConstraintMaker) in
            make.left.bottom.right.equalTo(contentView)
            make.height.equalTo(2)
        }
        
        //预览
        previewImageView = UIImageView()
        previewImageView?.contentMode = UIViewContentMode.scaleAspectFill
        contentView.addSubview(previewImageView!)
        previewImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(contentView)
            make.left.equalTo(contentView)
            make.size.equalTo(CGSize(width: 67/baseWidth, height: 56/baseWidth))
            make.bottom.equalTo(whiteLine.snp.top)
        })
        
        //昵称
        nickNameLabel = UILabel()
        nickNameLabel?.textColor = UIColor.white
        nickNameLabel?.font = UIFont.systemFont(ofSize: 16)
        contentView.addSubview(nickNameLabel!)
        nickNameLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.left.equalTo(previewImageView!.snp.right).offset(10/baseWidth)
            make.top.equalTo(contentView)
            make.right.equalTo(contentView).offset(-5/baseWidth)
            make.height.equalTo(18/baseWidth)
        })
        
        //地理位置
        locationLabel = UILabel()
        locationLabel?.textColor = UIColor.white
        locationLabel?.font = UIFont.systemFont(ofSize: 10)
        locationLabel?.numberOfLines = 3
        contentView.addSubview(locationLabel!)
        locationLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(nickNameLabel!.snp.bottom)
            make.leftMargin.equalTo(nickNameLabel!)
            make.rightMargin.equalTo(nickNameLabel!)
            make.bottom.equalTo(whiteLine.snp.top)
        })
    }
    
    //MARK: 设置数据
    fileprivate func setDate(){
        
        if let url: URL = annotation?.postcardModel?.frontImageURL{
            //先判断是否已换成图片
            SDWebImageManager.shared().cachedImageExists(for: url, completion: { [weak self] (result: Bool) in
                
                if result == true{
                    self?.previewImageView?.sd_setImage(with: url)
                }else{
                    if let placeHolderURL: URL = self?.annotation?.postcardModel?.frontPlaceImageURL{
                        self?.previewImageView?.sd_setImage(with: placeHolderURL, completed: { [weak self](image: UIImage?, _, _, _) in
                            DispatchQueue.main.async {
                                if image != nil {
                                    self?.previewImageView?.sd_setImage(with: url, placeholderImage: image!)
                                }else{
                                    self?.previewImageView?.sd_setImage(with: url)
                                }
                            }
                        })
                    }else{
                        self?.previewImageView?.sd_setImage(with: url)
                    }
                }
            })
        }
        
        nickNameLabel?.text = annotation?.postcardModel?.nickName
        
        locationLabel?.text = annotation!.postcardModel!.detailedAddress
        
        //获取距离
        if let userLocation: CLLocation = annotation?.userLocation{
            if let coordinate: CLLocationCoordinate2D = annotation?.coordinate{
                let location: CLLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
                let distance: CLLocationDistance = userLocation.distance(from: location)
                if locationLabel?.text != nil{
                    locationLabel!.text! += " (\(Int(distance)) m)"
                }
            }
        }
//        DispatchQueue.main.async {
//            self?.locationLabel?.text = (self?.annotation?.locationString)!
//        }
//        
//        if annotation?.locationString != " "{ return }
//        
//        // 将经纬度转换为详细的地址
//        if geocoder == nil{
//            geocoder = CLGeocoder()
//        }
//        if let coordinate: CLLocationCoordinate2D = annotation?.coordinate{
//            let location: CLLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
//            //解析出地址信息
//            geocoder?.reverseGeocodeLocation(location, completionHandler: { [weak self] ( results:[CLPlacemark]?, error: Error?) in
//                if let result: CLPlacemark = results?.first, error == nil {
//                    if let addressArray: [String] = result.addressDictionary?["FormattedAddressLines"] as? [String]{
//                        if let address: String = addressArray.first{
//                            self?.annotation?.locationString = address
//                        }
//                    }
//                }
//                
//                //获取距离
//                if let userLocation: CLLocation = self?.annotation?.userLocation{
//                    let distance: CLLocationDistance = userLocation.distance(from: location)
//                    //                        print(distance)
//                    self?.annotation?.locationString += " (\(Int(distance)) m)"
//                }
//                DispatchQueue.main.async {
//                    self?.locationLabel?.text = (self?.annotation?.locationString)!
//                }
//            })
//        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        previewImageView?.image = nil
        nickNameLabel?.text     = nil
        locationLabel?.text     = nil
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
