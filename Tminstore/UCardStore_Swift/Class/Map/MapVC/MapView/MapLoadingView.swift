//
//  MapLoadingView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/21.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class MapLoadingView: UIView {
    
    fileprivate var loadingIconImageView: UIImageView?
    fileprivate var loadingLabel: UILabel?
//    fileprivate var addImageView: UIImageView?
//    fileprivate var addIntroLineImageView: UIImageView?
//    fileprivate var introLabel: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(white: 0.0, alpha: 0.67)
        
        loadingIconImageView = UIImageView(image: UIImage(named: "mapLoadingIcon"))
        loadingIconImageView?.contentMode = UIViewContentMode.scaleAspectFit
        addSubview(loadingIconImageView!)
        loadingIconImageView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.center.equalTo(self)
            make.size.equalTo(CGSize(width: 51/baseWidth, height: 51/baseWidth))
        })
        
        loadingLabel = UILabel()
        loadingLabel?.textColor = UIColor.white
        loadingLabel?.font = UIFont.systemFont(ofSize: 12)
        loadingLabel?.textAlignment = NSTextAlignment.center
        loadingLabel?.text = NSLocalizedString("记忆定位中...", comment: "")
        addSubview(loadingLabel!)
        loadingLabel?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.top.equalTo(loadingIconImageView!.snp.bottom).offset(20/baseWidth)
            make.centerX.equalTo(loadingIconImageView!)
        })
        
//        addImageView = UIImageView(image: UIImage(named: "mapAdd"))
//        addImageView?.contentMode = UIViewContentMode.scaleAspectFit
//        addSubview(addImageView!)
//        addImageView?.snp.makeConstraints { (make: ConstraintMaker) in
//            make.right.equalTo(self).offset(-28/baseWidth)
//            make.bottom.equalTo(self).offset(-39/baseWidth)
//            make.size.equalTo(CGSize(width: 47/baseWidth, height: 47/baseWidth))
//        }
//        addImageView?.isHidden = true
//        
//        addIntroLineImageView = UIImageView(image: UIImage(named: "mapIntroLine"))
//        addIntroLineImageView?.contentMode = UIViewContentMode.scaleAspectFit
//        addSubview(addIntroLineImageView!)
//        addIntroLineImageView?.snp.makeConstraints { (make: ConstraintMaker) in
//            make.right.equalTo(self).offset(-6/baseWidth)
//            make.bottom.equalTo(addImageView!.snp.top)
//            make.size.equalTo(CGSize(width: 96/baseWidth, height: 96/baseWidth))
//        }
//        
//        introLabel = UILabel()
//        introLabel?.textColor = UIColor.white
//        introLabel?.font = UIFont.systemFont(ofSize: 14)
//        introLabel?.text = NSLocalizedString("点击按钮在地图上留下自己的时光记忆", comment: "")
//        introLabel?.numberOfLines = 0
//        introLabel?.textAlignment = NSTextAlignment.center
//        addSubview(introLabel!)
//        introLabel?.snp.makeConstraints { (make: ConstraintMaker) in
//            make.right.equalTo(addIntroLineImageView!.snp.left)
//            make.topMargin.equalTo(addIntroLineImageView!)
//            make.width.equalTo(100/baseWidth)
//        }
    }
    
    //MARK: 加载动画
    /// 加载动画
    ///
    /// - Parameter shouldHideUI: 是否需要隐藏其他UI
    func loadingAnimation(shouldHideUI: Bool = false){
        let rotateAnimation: CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation")
        
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = -Double.pi * 2
        rotateAnimation.duration = 3.0
        rotateAnimation.repeatCount = MAXFLOAT
        
        loadingIconImageView?.layer.add(rotateAnimation, forKey: nil)
        
        if shouldHideUI == true{
            backgroundColor = UIColor(white: 1.0, alpha: 0.0)
            loadingLabel?.isHidden = true
//            addImageView?.isHidden = true
//            addIntroLineImageView?.isHidden = true
//            introLabel?.isHidden = true
        }
    }
    
    //MARK: 停止动画
    /// 停止动画
    func stopAnimation(){
        self.layer.removeAllAnimations()
        self.removeFromSuperview()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
