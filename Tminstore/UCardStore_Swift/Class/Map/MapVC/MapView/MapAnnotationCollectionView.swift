//
//  MapAnnotationCollectionView.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/5/26.
//  Copyright © 2017年 ucard. All rights reserved.
//

//MARK: 点击地图上的点之后，弹出用户头像
import UIKit
import SnapKit

class MapAnnotationCollectionView: UICollectionView {

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 35/baseWidth, height: 35/baseWidth)
        layout.scrollDirection = UICollectionViewScrollDirection.horizontal
        layout.minimumLineSpacing = 17/baseWidth
        layout.minimumInteritemSpacing = 22/baseWidth
        
        super.init(frame: CGRect.zero, collectionViewLayout: layout)
        
        backgroundColor = UIColor.white
        register(MapAnnotationCollectionViewCell.self, forCellWithReuseIdentifier: appReuseIdentifier.MapAnnotationCollectionViewCellReuseIdentifier)
        
        contentInset = UIEdgeInsets(top: 0, left: 17/baseWidth, bottom: 0, right: 17/baseWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

let MapAnnotationCollectionViewCellSetBorder: String = "MapAnnotationCollectionViewCellSetBorder"

class MapAnnotationCollectionViewCell: UICollectionViewCell{
    
    /// 数据源
    var postcardModel: PostcardModel?{
        didSet{
            setData()
        }
    }
    
    /// 用户头像
    fileprivate var portraitView: BaseImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        portraitView = BaseImageView.userPortraitView(cornerRadius: 17.5/baseWidth)
        contentView.addSubview(portraitView!)
        portraitView?.snp.makeConstraints({ (make: ConstraintMaker) in
            make.edges.equalTo(contentView)
        })
        portraitView?.layer.borderColor = UIColor.clear.cgColor
        portraitView?.layer.borderWidth = 2
        
        NotificationCenter.default.addObserver(self, selector: #selector(cancelBorder(info:)), name: NSNotification.Name(MapAnnotationCollectionViewCellSetBorder), object: nil)
    }
    
    //MARK: 设置边框
    func setBorder(){
        portraitView?.layer.borderColor = UIColor(hexString: "86F6FF").cgColor
    }
    //MARK: 取消边框
    func cancelBorder(info: Notification){
        portraitView?.layer.borderColor = UIColor.clear.cgColor
    }
    //MARK: 取消边框
    func cancelBorderAction(){
        portraitView?.layer.borderColor = UIColor.clear.cgColor
    }
    
    //MARK: 设置数据
    fileprivate func setData(){
        portraitView?.sd_setImage(with: postcardModel?.userLogoURL, placeholderImage: UIImage(named: "defaultLogo"))
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        portraitView?.image = nil
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
