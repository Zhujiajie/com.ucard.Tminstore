//
//  MapLocationTableViewCell.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MapLocationTableViewCell: UITableViewCell {

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
