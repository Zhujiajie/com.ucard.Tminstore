//
//  MapLocationViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/3/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MapLocationViewModel: BaseViewModel, AMapSearchDelegate {

    fileprivate let searchManager: AMapSearchAPI = AMapSearchAPI()
    
    /// 成功获得搜索结果
    var searchSuccess: (()->())?
    /// 周围地理位置信息
    var locationInfos: [AMapPOI] = [AMapPOI]()
    /// 成功获得地址信息
    var getAddressInfoSuccess: ((_ addressComponent: AMapAddressComponent)->())?
    
    override init() {
        super.init()
        
        AMapServices.shared().apiKey = "ada0c40c91d1e70e47918e3b8635b47c"
        AMapServices.shared().crashReportEnabled = false
        
        searchManager.delegate = self
        
        //设置语言
        if languageCode() == "CN"{
            searchManager.language = AMapSearchLanguage.zhCN
        }else{
           searchManager.language = AMapSearchLanguage.en
        }
    }
    
    //MARK: 获取周围地里位置信息
    func requestAroundPOI(keyWord: String?) {
        
        let request: AMapPOIAroundSearchRequest = AMapPOIAroundSearchRequest()
        request.location = AMapGeoPoint.location(withLatitude: CGFloat(currentUserLocation!.coordinate.latitude), longitude: CGFloat(currentUserLocation!.coordinate.longitude))
        
        if keyWord != nil{
            request.keywords = keyWord!
        }
        request.sortrule = 0
        request.offset = 50
        request.radius = 500
        request.types = "汽车服务|汽车销售|汽车维修|摩托车服务|餐饮服务|购物服务|生活服务|体育休闲服务|医疗保健服务|住宿服务|风景名胜|商务住宅|政府机构及社会团体|科教文化服务|交通设施服务|金融保险服务|公司企业|道路附属设施|地名地址信息|公共设施"
        searchManager.aMapPOIAroundSearch(request)
    }
    
    //MARK: 获取城市、省份、国家信息
    /// 获取城市、省份、国家信息
    func requestLocationInfo(){
        let regeo: AMapReGeocodeSearchRequest = AMapReGeocodeSearchRequest()
        regeo.location = AMapGeoPoint.location(withLatitude: CGFloat(currentUserLocation!.coordinate.latitude), longitude: CGFloat(currentUserLocation!.coordinate.longitude))
        searchManager.aMapReGoecodeSearch(regeo)
    }
    
    func onReGeocodeSearchDone(_ request: AMapReGeocodeSearchRequest!, response: AMapReGeocodeSearchResponse!) {
        if response.regeocode != nil{
            getAddressInfoSuccess?(response.regeocode.addressComponent)
        }
    }
    
    func onPOISearchDone(_ request: AMapPOISearchBaseRequest!, response: AMapPOISearchResponse!) {
        locationInfos = response.pois
        searchSuccess?()
    }
}
