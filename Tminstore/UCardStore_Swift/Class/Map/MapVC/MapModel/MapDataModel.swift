//
//  MapDataModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import Alamofire

class MapDataModel: BaseDataModel {

    /// 请求天气数据的apiKey
    let apiKey: String = "ipwjhbsgkin9jjtk"
    
    //MARK: 请求实时天气
    /// 请求实时天气
    ///
    /// - Parameters:
    ///   - locationString: 经纬度字符串
    ///   - success: 成功的回调
    func getWeatherInfo(locationString: String, successClosure success: ((_ result: [String: Any])->())?){
        let urlStr: String = "https://api.thinkpage.cn/v3/weather/now.json"
        let parameters: [String: Any] = [
            "key": apiKey,
            "location": locationString
        ]
        sendGETRequestWithURLString(URLStr: urlStr, ParametersDictionary: parameters, urlEncoding: URLEncoding.default, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: nil, completionClosure: nil)
    }
    
    //MARK: 请求定位后，周围的图片数据
    /// 请求定位后，周围的图片数据
    ///
    /// - Parameters:
    ///   - coordinate: 定位信息
    ///   - success: 成功的闭包
    func requestAnnotationData(coordinate: CLLocationCoordinate2D, successClosure success: ((_ result: [String: Any])->())?) {
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "longitude": coordinate.longitude,
            "latitude": coordinate.latitude
        ]
        
        sendPOSTRequestWithURLString(URLStr: appAPIHelp.mapDataAPI, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
        }, completionClosure: {[weak self] _ in
            self?.completionClosure?()
        })
    }
    
    //MARK: 请求单张图片的信息
    /// 请求单张图片的信息
    ///
    /// - Parameters:
    ///   - originalId: 原创ID
    ///   - success: 成功的闭包
    func requestSingleAnnotationInfo(originalId: String, successClosure success: ((_ result: [String: Any])->())?){
        
        let urlString: String = appAPIHelp.checkARVideosAPI + "/1"
        
        let parameters: [String: Any] = [
            "token": getUserToken(),
            "originalId": originalId
        ] as [String: Any]
        
        sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameters, successClosure: { (result: [String : Any]) in
            success?(result)
        }, failureClourse: { [weak self] (error: Error?) in
            self?.errorClosure?(error)
            }, completionClosure: {[weak self] _ in
                self?.completionClosure?()
        })
    }
}
