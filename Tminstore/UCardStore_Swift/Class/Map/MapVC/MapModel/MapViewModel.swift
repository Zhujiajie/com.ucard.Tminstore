//
//  MapViewModel.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2017/2/15.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import CCHMapClusterController

class MapViewModel: BaseViewModel {

    fileprivate let dataModel: MapDataModel = MapDataModel()
    
    /// 定位
    var locationManager: AMapLocationManager?
    
    /// 需要先开启摄像头权限
    var shouldGetCameraAuthorication: (()->())?
    
    /// 数据
    var annotations: [ZTAnnotation] = [ZTAnnotation]()
    /// 合作用户的点
    var anotherAnnotations: [ZTAnnotation] = [ZTAnnotation]()
    
    /// 获取数据成功
    var getMapDataSuccess: (()->())?
    
    /// 加载动画需要停止
    var loadingAnimationShouldStop: (()->())?
    
    /// 需要地图权限
    var needLocationAuthorization: (()->())?
    
    /// 网络请求错误
    var netWorkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 成功获取天气信息
    var getWeatherSuccess: ((_ weatherCode: String, _ temperature: String)->())?
    
    /// 下载AR识别图成功
    var downLoadARImageSusscess: ((_ imagePath: String, _ annotation: ZTAnnotation)->())?
    
    /// 获取定位信息成功，缩放地图
    var setMapViewRegion: ((_ region: MKCoordinateRegion, _ coordinate: CLLocationCoordinate2D)->())?
    
    /// 地图上没有数据
    var emptyData: (()->())?
    
    override init() {
        super.init()
        
        //高德定位
        locationManager = AMapLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters//定位最精度为100米
        
        //MARK: 出错的闭包
        dataModel.errorClosure = {[weak self] (error: Error?) in
            self?.netWorkError?(error, nil)
            self?.dismissSVProgress()
        }
        
        //MARK: 请求完成的闭包
        dataModel.completionClosure = {[weak self] _ in
            self?.loadingAnimationShouldStop?()
        }
    }
    
    //MARK: 缩放地图到上一次位置
    /// 缩放地图到上一次位置
    func locateToLastLocation(){
        //定位到上一次的位置
        let lastLoation: String = getLastLocation()
        if lastLoation != ""{
            let infos: [String] = lastLoation.components(separatedBy: ",")
            let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: Double(infos[0])!, longitude: Double(infos[1])!)
            //显示精度
            let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
            let region: MKCoordinateRegion = MKCoordinateRegionMake(coordinate, span)
            setMapViewRegion?(region, coordinate)//地图缩放到当前位置
        }
    }
    
    //MARK: 获取定位信息的请求
    /// 获取定位信息的请求
    func requestLocation(){
        
        //如果用户未授权定位，则提醒用户去
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse{
            loadingAnimationShouldStop?()//停止加载动画
            needLocationAuthorization?()
        }
        //已经授权定位权限，开始定位
        else{
            //单次请求定位
            locationManager?.requestLocation(withReGeocode: false, completionBlock: { [weak self] (location: CLLocation?, reGeoCode: AMapLocationReGeocode?, error: Error?) in
                DispatchQueue.main.async {
                    if error == nil && location != nil{
                        currentUserLocation = location!
                        //显示精度
                        let span: MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02)
                        let region: MKCoordinateRegion = MKCoordinateRegionMake(location!.coordinate, span)
                        self?.setMapViewRegion?(region, location!.coordinate)//地图缩放到当前位置
                        doInBackground(inBackground: {
                            saveLastLocation(location: location!)
                            self?.requestMapData(coordinate: location!.coordinate)//请求地图数据
                        })
                    }
                }
                self?.locationManager?.stopUpdatingLocation()
            })
        }
    }
    
//    //MARK: 请求天气数据
//    /// 请求天气数据
//    ///
//    /// - Parameter coordinate: 经纬度数据
//    func requestWeather(coordinate: CLLocationCoordinate2D){
//        let locationString: String = "\(coordinate.latitude):\(coordinate.longitude)"
//        dataModel.getWeatherInfo(locationString: locationString) { [weak self] (result: [String : Any]) in
//            if let results: [[String: Any]] = result["results"] as? [[String: Any]]{
//                if let weather: [String: Any] = results.first{
//                    if let now: [String: Any] = weather["now"] as? [String: Any]{
//                        if let code: String = now["code"] as? String{
//                            if let temperature: String = now["temperature"] as? String{
//                                self?.getWeatherSuccess?(code, temperature)
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
    
    //MARK: 获取周围1000米的数据
    /// 获取周围1000米的数据
    ///
    /// - Parameter coordinate: 定位数据
    func requestMapData(coordinate: CLLocationCoordinate2D){
        dataModel.requestAnnotationData(coordinate: coordinate, successClosure: { [weak self] (result: [String : Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                
                DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
                    
                    self?.annotations.removeAll()
                    self?.anotherAnnotations.removeAll()
                    
                    if let data: [String: Any] = result["data"] as? [String: Any]{
                        
                        if let list: [[String: Any]] = data["exhibitionList"] as? [[String: Any]]{
                            for dic: [String: Any] in list{
                                let postcardModel: PostcardModel = PostcardModel.initFromSpaceTimeCircle(dic: dic)
                                if  postcardModel.photoLocation != nil && postcardModel.frontImageURL != nil {
                                    let annotation: ZTAnnotation = ZTAnnotation(coordinate: postcardModel.photoLocation!.coordinate, title: postcardModel.nickName, subTitle: nil)
                                    annotation.postcardModel = postcardModel
                                    if annotation.postcardModel?.userType == UserType.business{
                                        self?.anotherAnnotations.append(annotation)
                                    }else{
                                        self?.annotations.append(annotation)
                                    }
                                }
                            }
//                            for i in 0..<list.count{
//                                let postcardModel: PostcardModel = PostcardModel.initFromDic(dic: list[i])
//                                if  postcardModel.photoLocation != nil && postcardModel.frontImageURL != nil {
//                                    let annotation: ZTAnnotation = ZTAnnotation(coordinate: postcardModel.photoLocation!.coordinate, title: postcardModel.nickName, subTitle: nil)
//                                    annotation.postcardModel = postcardModel
//                                    if i % 2 == 0{
//                                        annotation.postcardModel?.annotationIconURL = URL(string: "http://ucardstorevideo.b0.upaiyun.com/iOSImages/testIcon.png!/format/webp")
//                                        annotation.postcardModel?.userType = UserType.business
//                                        self?.anotherAnnotations.append(annotation)
//                                    }else{
//                                        self?.annotations.append(annotation)
//                                    }
//                                }
//                            }
                            if list.isEmpty == true{
                                self?.emptyData?()
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self?.getMapDataSuccess?()//成功获得地图数据
                    }
                }
            }else{
                self?.netWorkError?(nil, result)
            }
            })
    }
    
    //MARK: 获得视频地址，并下载AR图片
    /// 获得视频地址，并下载AR图片
    ///
    /// - Parameter annotation: ZTAnnotation
    func requestVideoURL(annotation: ZTAnnotation){
        
        showSVProgress(title: NSLocalizedString("正在下载AR识别图", comment: ""))
        
        dataModel.requestSingleAnnotationInfo(originalId: annotation.postcardModel!.originalId) { [weak self] (result: [String : Any]) in
            
            if let code: Int = result["code"] as? Int, code == 1000{
                if let data: [String: Any] = result["data"] as? [String: Any]{
                    if let list: [[String: Any]] = data["list"] as? [[String: Any]]{
                        if let dic: [String: Any] = list.first{
                            if let videoURLString: String = dic["videoUrl"] as? String{
                                annotation.postcardModel?.videoURL = URL(string: videoURLString)
                                annotation.defalutVideoURLPath = videoURLString
                                self?.downLoadARImage(annotation: annotation)
                                return
                            }
                        }
                    }
                }
            }
            self?.netWorkError?(nil, result)
        }
    }
    
    //MARK: 下载识别图片
    /// 下载识别图片
    ///
    /// - Parameter annotation: ZTAnnotation
    fileprivate func downLoadARImage(annotation: ZTAnnotation){
        
        checkCameraAuthoriation(checkSuccess: {[weak self] _ in
            
            let prefetcher: SDWebImagePrefetcher = SDWebImagePrefetcher.shared()
            prefetcher.options = SDWebImageOptions.highPriority
            prefetcher.prefetchURLs([annotation.postcardModel!.arImageURL!, annotation.postcardModel!.frontImageURL!], progress: nil, completed: { [weak self] (count: UInt, skipCount: UInt) in
                
                if skipCount == 0{
                    if let imageCache: SDImageCache = SDWebImageManager.shared().imageCache, let imagePath: String = imageCache.defaultCachePath(forKey: annotation.postcardModel!.arImageURL!.absoluteString){
                        //成功下载识别图片
                        DispatchQueue.main.async{
                            self?.downLoadARImageSusscess?(imagePath, annotation)
                        }
                    }
                }
                DispatchQueue.main.async {
                    self?.dismissSVProgress()
                }
            })
        })
    }
    
    //MARK: 设置AnnotationView
    /// 设置AnnotationView
    ///
    /// - Parameters:
    ///   - mapView: MKMapView
    ///   - annotation: MKAnnotation
    /// - Returns: MKAnnotationView
    func getAnnotationView(mapView: MKMapView, viewFor annotation: MKAnnotation)->MKAnnotationView?{
        
        var annotationView: ZTAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: appReuseIdentifier.annotationReuseIndetifier) as? ZTAnnotationView
        
        if annotationView == nil {
            annotationView = ZTAnnotationView(annotation: annotation, reuseIdentifier: appReuseIdentifier.annotationReuseIndetifier)
            annotationView?.leftCalloutAccessoryView = nil
            annotationView?.rightCalloutAccessoryView = nil
            annotationView?.mapView = mapView
        }
        
        //初始化用户的MAAnnotationView
        if annotation.isKind(of: MKUserLocation.self){
            annotationView?.image = UIImage(named: "userLocation")
            annotationView?.count = 0
        }
        else{
            if let clusterAnnotation: CCHMapClusterAnnotation = annotation as? CCHMapClusterAnnotation{
                if let annotation: ZTAnnotation = clusterAnnotation.annotations.first as? ZTAnnotation{
                    if annotation.postcardModel?.userType != UserType.business{
                        annotationView?.count = clusterAnnotation.annotations.count
                    }
                }
            }
        }
        
        return annotationView
    }
    
    //MARK: 展示详细视图
    /// 展示详细视图
    ///
    /// - Parameters:
    ///   - mapView: MKMapView
    ///   - view: MKAnnotationView
    ///   - tooFarView: 太远视图
    ///   - calloutView: iOS9的详细视图
    ///   - iOS8TooFarView: iOS8的太远视图
    ///   - iOS8DetailView: iOS8的详细视图
    func displaceDetailCalloutView(mapView: MKMapView, didSelect view: MKAnnotationView, loadTooFarView tooFarView: ((_ annotation: ZTAnnotation)->())?, loadDetailCalloutView calloutView: ((_ annotationArray: [ZTAnnotation], _ clusterAnnotaiton: CCHMapClusterAnnotation)->())?, loadIOS8TooFarView iOS8TooFarView: ((_ annotation: ZTAnnotation)->())?, loadIOS8DetailView iOS8DetailView: ((_ annotationArray: [ZTAnnotation], _ clusterAnnotaiton: CCHMapClusterAnnotation)->())?){
        
        if let clusterAnnotaiton: CCHMapClusterAnnotation = view.annotation as? CCHMapClusterAnnotation{
            
            //如果大于9个，且地图还能缩放。则缩放地图
            if clusterAnnotaiton.annotations.count > 9 && mapView.region.span.latitudeDelta > 0.0012{
                var region: MKCoordinateRegion = mapView.region
                region.center = clusterAnnotaiton.coordinate
                region.span = MKCoordinateSpan(latitudeDelta: region.span.latitudeDelta * 0.5, longitudeDelta: region.span.longitudeDelta * 0.5)
                mapView.setRegion(region, animated: true)
            }
                //展示详细视图
            else{
                //先判断距离
                let location: CLLocation = CLLocation(latitude: clusterAnnotaiton.coordinate.latitude, longitude: clusterAnnotaiton.coordinate.longitude)
                let distance: CLLocationDistance = currentUserLocation!.distance(from: location)
                
                if #available(iOS 9.0, *) {
                    
                    //根据距离加载不同的detailView
                    if distance > maxDistance{
                        if let annotation: ZTAnnotation = clusterAnnotaiton.annotations.first as? ZTAnnotation{
                            tooFarView?(annotation)//太远的视图
                        }
                    }else{
                        if let annotationSet: Set<ZTAnnotation> = clusterAnnotaiton.annotations as? Set<ZTAnnotation>{
                            calloutView?(Array(annotationSet), clusterAnnotaiton)//详细视图
                        }
                    }
                } else {
                    mapView.deselectAnnotation(view.annotation, animated: true)
                    
                    if let annotationSet: Set<ZTAnnotation> = clusterAnnotaiton.annotations as? Set<ZTAnnotation>{
                        //根据不同的距离，显示不同视图
                        if distance < maxDistance{
                            iOS8DetailView?(Array(annotationSet), clusterAnnotaiton)//iOS8的详细视图
                        }else{
                            if let annotation: ZTAnnotation = annotationSet.first{
                                iOS8TooFarView?(annotation)//iOS8的太远视图
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK: 添加淡蓝色图层
    /// 添加淡蓝色图层
    ///
    /// - Parameters:
    ///   - mapView: MKMapView
    ///   - overlay: MKOverlay
    /// - Returns: MKOverlayRenderer
    func addOverlay(mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer{
        let render: MKCircleRenderer = MKCircleRenderer(overlay: overlay)
        render.lineWidth = 2
        render.strokeColor = UIColor(hexString: "#AAC8E1")
        render.fillColor = UIColor(hexString: "99EAFF")
        render.alpha = 0.43
        return render
    }
    
    //MARK: 检查摄像头权限
    fileprivate func checkCameraAuthoriation(checkSuccess: (()->())?){
        // 请求摄像头权限
        AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo) { [weak self](result: Bool) in
            //若还没有取得摄像头权限，则提醒用户去授权
            if result != true{
                DispatchQueue.main.async {
                    self?.shouldGetCameraAuthorication?()
                }
            }else{
                checkSuccess?()
            }
        }
    }
}
