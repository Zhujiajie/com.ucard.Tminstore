//
//  MainContentVC.swift
//  UCardStore_Swift
//
//  Created by 朱佳杰 on 2017/10/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MainContentVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        //测试数据
        var vcArray: [UITableViewController] = [UITableViewController]()
        for i in 0 ..< 7 {
            
            if i == 0 {
                let vc:TimeTableViewController = TimeTableViewController()
                vcArray.append(vc)
            } else {
                let vc = UITableViewController()
                vc.view.backgroundColor = UIColor.red
                vcArray.append(vc)
            }
        }
        
        let pageView = ZJJScrollPageView(frame: CGRect(x: 0, y: 64, width: screenWidth, height: screenHeight - 64 - 48))
        view.addSubview(pageView)
        //设置标题
        pageView.titleArray =  ["热门", "旅行", "人物", "动漫","萌宠", "美食"]
        //设置view
        pageView.pageArray = vcArray
        
        self.automaticallyAdjustsScrollViewInsets = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
