//
//  TimeTableViewController.swift
//  UCardStore_Swift
//
//  Created by 朱佳杰 on 2017/10/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

fileprivate let mainInfoCellIdentify: String = "mainInfoCellIdentify"

class TimeTableViewController: UITableViewController {
    
    fileprivate var timeModel: TimeTableViewModel = TimeTableViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(MainInfoCell.self, forCellReuseIdentifier: mainInfoCellIdentify)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tableView.backgroundColor = UIColor(hexString: "F2F2F2")
        
        timeModel.getData(classification: "1", index: "1")
    }



    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10 + 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (indexPath.row == 10) {
            return 48
        }
        return 330
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 10) {
            let cell: UITableViewCell = UITableViewCell.init()
            cell.contentView.backgroundColor = UIColor(hexString: "F2F2F2")
            return cell
        } else {
            let cell: MainInfoCell = tableView.dequeueReusableCell(withIdentifier: mainInfoCellIdentify, for: indexPath) as! MainInfoCell
            return cell
        }
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
