//
//  TimeTableViewModel.swift
//  UCardStore_Swift
//
//  Created by 朱佳杰 on 2017/10/28.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class TimeDataModel: BaseDataModel {
    
    /**
     从服务器返回的字典，初始化时光首页
     
     - parameter dic: [String: AnyObject]
     
     - returns: TimeDataModel
     */
    class func initFromDic(dic: [String: Any])-> TimeDataModel{
        
        let timeModel: TimeDataModel = TimeDataModel()
    
        return timeModel
    }
        
    //MARK:获取时光首页数据
    /**
     获取时光首页数据
     
     - parameter pageNum:    要获取第几页数据
     - parameter success:    成功的闭包
     - parameter failure:    失败的闭包
     - parameter completion: 请求完成的闭包
     */
    func getTimeData(classification: String?, index: String?, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?) {
        
        let param: [String: Any] = ["classification": classification!,
                                    "pageIndex": index!]
        sendGETRequestWithURLString(URLStr: appAPIHelp.timeDataAPI + "/" + classification! + "/" + index!, ParametersDictionary: nil, successClosure: { (result: [String: Any]) in
            print("time result is:\(result)")
        }, failureClourse: { (error: Error?) in
            print("failure is :\(String(describing: error))")
        })
    }
//    func getTimeData(classification: String?, index: String?) {
//
//    }
    
        //MARK:获取社区主页数据
        /**
         获取社区主页数据
         
         - parameter pageNum:    要获取第几页数据
         - parameter success:    成功的闭包
         - parameter failure:    失败的闭包
         - parameter completion: 请求完成的闭包
         */
//        func getMainPageData(pageIndex: Int, successClosure success: ((_ result: [String: Any])->())?, failureClosure failure: ((_ error: Error?)->())?,  completionClosure completion: (()->())?) {
//
//            let urlString: String = appAPIHelp.getCommunityDataAPI + "/\(pageIndex)"
//
//            let parameter: [String: Any] = [
//                "token": getUserToken()
//                ] as [String: Any]
//
//            sendPOSTRequestWithURLString(URLStr: urlString, ParametersDictionary: parameter, successClosure: { (result: [String: Any]) in
//                success?(result)
//            }, failureClourse: { (error: Error?) in
//                failure?(error)
//            }) {
//                completion?()
//            }
        
            //        let parameter: [String: Any] = [
            //            "pageIndex": pageIndex
            //            ] as [String: Any]
            //
            //        sendGETRequestWithURLString(URLStr: getCommunityDataAPI, ParametersDictionary: parameter, urlEncoding: URLEncoding.default, successClosure: { (result: [String : Any]) in
            //            success?(result)
            //        }, failureClourse: { (error: Error?) in
            //            failure?(error)
            //        }, completionClosure: {
            //            completion?()
            //        })
//        }
        
//        if let originalId: String = dic["originalId"] as? String{
//            postcard.originalId = originalId
//        }
//        if let videoId = dic["videoId"] as? String{
//            postcard.videoId = videoId
//            postcard.videoURL = URL(string: "http://ucardstorevideo.b0.upaiyun.com" + videoUpun + videoId + ".mp4")
//        }
//        if let memberCode: String = dic["memberCode"] as? String{
//            postcard.memberCode = memberCode
//        }
//        if let nickName: String = dic["nickName"] as? String{
//            postcard.nickName = nickName
//        }
//        if let remark: String = dic["remark"] as? String{
//            postcard.remark = remark
//        }
//        if let frontImageRemark: String = dic["frontImageRemark"] as? String{
//            postcard.remark = frontImageRemark
//        }
//        if let reviewStatus: String = dic["reviewStatus"] as? String{
//            switch reviewStatus {
//            case "00":
//                postcard.reviewStatus = ReviewStatus.waitingForReview
//            case "01":
//                postcard.reviewStatus = ReviewStatus.reviewed
//            default:
//                postcard.reviewStatus = ReviewStatus.reviewFailed
//            }
//        }
//        if let isOnMarket: String = dic["isOnMarket"] as? String{
//            if isOnMarket == "00"{
//                postcard.isOnMarket = false
//            }else{
//                postcard.isOnMarket = true
//            }
//        }
//        if let isPurchased: String = dic["isPurchased"] as? String{
//            if isPurchased == "00"{
//                postcard.isPurchased = false
//            }else{
//                postcard.isPurchased = true
//            }
//        }
//        if let pointPrice: Int = dic["pointPrice"] as? Int{
//            postcard.pointPrice = pointPrice
//        }
//        if let commentTotal: Int = dic["commentTotal"] as? Int{
//            postcard.commentTotal = commentTotal
//        }
//        if let orderTotla: Int = dic["orderTotal"] as? Int{
//            postcard.orderTotal = orderTotla
//        }
//        if let createDateValue: Double = dic["createDate"] as? Double{
//            let creatDate: Date = Date(timeIntervalSince1970: createDateValue/1000)
//            postcard.createDate = creatDate
//        }
//        if let updateDateValue: Double = dic["updateDate"] as? Double{
//            let updateDate: Date = Date(timeIntervalSince1970: updateDateValue/1000)
//            postcard.updateDate = updateDate
//        }
//        if let isSale: String = dic["isSale"] as? String{
//            if isSale == "00"{
//                postcard.isSale = false
//            }else{
//                postcard.isSale = true
//            }
//        }
//        if let frontImageURLStr: String = dic["frontImageUrl"] as? String{
//            // 向又拍云请求压缩过的图片
//            let url: URL? = URL(string: frontImageURLStr + "!/format/webp")
//            postcard.frontImageURL = url
//            let placeHolderURL: URL? = URL(string: frontImageURLStr + "!placeHolder")
//            postcard.frontPlaceImageURL = placeHolderURL
//            let arImageURL: URL? = URL(string: frontImageURLStr + "!thumbnail")
//            postcard.arImageURL = arImageURL
//        }
//        if let backImageURLStr: String = dic["backImageUrl"] as? String{
//            // 向又拍云请求压缩过的图片
//            let url: URL? = URL(string: backImageURLStr + "!/format/webp")
//            postcard.backImageURL = url
//        }
//        if let targetId: String = dic["targetId"] as? String{
//            postcard.targetId = targetId
//        }
//        if let longitude: Double = dic["longitude"] as? Double{
//            if let latitude: Double = dic["latitude"] as? Double{
//                let photoLocation = CLLocation(latitude: latitude, longitude: longitude)
//                postcard.photoLocation = photoLocation
//            }
//        }
//        if let country: String = dic["country"] as? String{
//            postcard.country = country
//        }
//        if let province: String = dic["province"] as? String{
//            postcard.province = province
//        }
//        if let city: String = dic["city"] as? String, city != "?"{
//            postcard.city = city
//        }
//        if let district: String = dic["district"] as? String, district != "?"{
//            postcard.district = district
//        }
//        if let detailedAddress: String = dic["detailedAddress"] as? String{
//            postcard.detailedAddress = detailedAddress
//        }
//        if let isARStr: String = dic["isAr"] as? String{
//            if isARStr == "00"{
//                postcard.isAR = false
//            }else{
//                postcard.isAR = true
//            }
//        }
//        if let customerTypeStr: String = dic["customerType"] as? String{
//            switch customerTypeStr {
//            case "B":
//                postcard.customerType = CustomerType.bussines
//            case "C":
//                postcard.customerType = CustomerType.customer
//            default:
//                postcard.customerType = CustomerType.undefined
//            }
//        }
//        if let userLogoStr: String = dic["userLogo"] as? String{
//            // 向又拍云请求压缩过的图片
//            let url: URL? = URL(string: userLogoStr + "!/format/webp")
//            postcard.userLogoURL = url
//        }
//        if let commentDics: [[String: Any]] = dic["commentList"] as? [[String: Any]]{
//            for commentDic: [String: Any] in commentDics{
//                let comment: CommentModel = CommentModel.initFromDic(dic: commentDic)
//                postcard.commentList.append(comment)
//            }
//        }
//        if let isLikedString: String = dic["isLike"] as? String, isLikedString == "01"{
//            postcard.isLiked = true
//        }
//        if let likeCount: Int = dic["likeTotal"] as? Int{
//            postcard.likeCount = likeCount
//        }
//        if let videoThumbnailURLString: String = dic["videoThumbnail"] as? String{
//            let url: URL? = URL(string: videoThumbnailURLString + "!/format/webp")
//            postcard.videoThumbnailURL = url
//        }
//        if let code: String = dic["code"] as? String{
//            postcard.arCode = code
//        }

}

class TimeTableViewModel: BaseViewModel {

    fileprivate let dataModel: TimeDataModel = TimeDataModel()
    
    /// footer停止刷新
    var footerStopRefresh: (()->())?
    
    /// 没有更多数据了
    var stopRefreshWithNoMoreData: (()->())?
    
    /// tableView需要重新加载数据
//    var tableShouldReloadData: ((_ postcardArray: [PostcardModel])->())?
    
    /// 需要更新数据
//    var updateData: ((_ indexPath: IndexPath, _ postcardArray: [PostcardModel])->())?
    
    /// tableView需要插入数据
//    var tableViewShouleInsetData: ((_ beginIndex: Int, _ totalIndex: Int, _ postcardArray: [PostcardModel])->())?
    
    /// 网络请求错误
    var netWorkError: ((_ error: Error?, _ result: [String: Any]?)->())?
    
    /// 向服务器端请求第几页数据
    var pageNum: Int = 1
    
    /// 存放数据的数组
    fileprivate var timeArray: [TimeDataModel] = [TimeDataModel]()
    
    /// 是否需要停止上拉刷新
    fileprivate var stopGetMoreData = false
    
    /// 正在加载
    fileprivate var isRequestingData = false
    
    //MARK: 上拉刷新
    /// 上拉刷新
    func pullToRefresh(){
        if stopGetMoreData == false{
//            getDate()
        }
    }
    
    //MARK: 获取数据
    /**
     获取时光主页数据
     
     - parameter success: 请求成功的闭包
     - parameter failure: 请求失败的闭包
     */
    
   func getData(classification: String?, index: String?) {
        dataModel.getTimeData(classification: classification!, index: index!, successClosure: { (result: [String: Any]) in
            print("time result is:\(result)")
        }, failureClosure: { [weak self] (error: Error?) in
            print("failure is :\(String(describing: error))")
        }, completionClosure: { [weak self] _ in
            self?.dismissSVProgress()
            self?.isRequestingData = false
        })
    }
//    fileprivate func getDate(){
//
//        if isRequestingData == true{ return }
//        isRequestingData = true
//
//        dataModel.getMainPageData(pageIndex: pageNum, successClosure: { [weak self](result: [String: Any]) in
//
//            if let code: Int = result["code"] as? Int, code == 1000{
//
//                let data: [String: Any] = result["data"] as! [String: Any]
//                let communityList: [[String: Any]] = data["communityList"] as! [[String: Any]]
//                // 没有更多数据了
//                if communityList.isEmpty == true{
//                    self?.stopGetMoreData = true
//                    self?.stopRefreshWithNoMoreData?()
//                }else{
//
//                    // 获得新数据
//                    var newPostcardArray: [PostcardModel] = [PostcardModel]()
//                    var placeHolderURLArray: [URL] = [URL]()
//
//                    for dic: [String: Any] in communityList{
//                        let postcard: PostcardModel = PostcardModel.initFromDic(dic: dic)
//                        newPostcardArray.append(postcard)
//                        if let url: URL = postcard.frontPlaceImageURL{
//                            placeHolderURLArray.append(url)
//                        }//预先下载占位图片
//                    }
//                    SDWebImagePrefetcher.shared().prefetchURLs(placeHolderURLArray)
//
//                    // 下拉刷新
//                    if self?.pageNum == 1{
//
//                        self?.postcardArray.removeAll()
//
//                        self?.postcardArray.append(contentsOf: newPostcardArray)
//                        self?.dataModel.saveNewPostCard(postcardArray: (self?.postcardArray)!)//存储新数据
//
//                        self?.headerStopRefresh?()//顶部停止刷新
//                    }
//                        // 上拉刷新
//                    else{
//                        //传递新数据，并存储新数据
//                        self?.postcardArray.append(contentsOf: newPostcardArray)
//                        self?.footerStopRefresh?()//底部停止刷新
//                    }
//                    self?.tableShouldReloadData?((self?.postcardArray)!)
//                    self?.pageNum += 1
//                }
//            }else{
//                self?.netWorkError?(nil, result)
//            }
//            }, failureClosure: { [weak self] (error: Error?) in
//                self?.netWorkError?(error, nil)
//                self?.footerStopRefresh?()
//                self?.headerStopRefresh?()
//        }) { [weak self] _ in
//            self?.dismissSVProgress()
//            self?.isRequestingData = false
//        }
//    }
//
//    //MARK: 更新评论数据
//    /// 更新评论的数据
//    func updateCommentData(indexPath: IndexPath){
//
//        let pageIndex: Int = indexPath.row / 20 + 1
//
//        dataModel.getMainPageData(pageIndex: pageIndex, successClosure: { [weak self] (result: [String: Any]) in
//
//            if let code: Int = result["code"] as? Int, code == 1000{
//                let arrayIndex: Int = indexPath.row % 20
//                let data: [String: Any] = result["data"] as! [String: Any]
//                let newArray: [[String: Any]] = data["communityList"] as! [[String: Any]]//解析出数据
//
//                let postcard: PostcardModel = PostcardModel.initFromDic(dic: newArray[arrayIndex])
//
//                self?.postcardArray[indexPath.row] = postcard
//                self?.updateData?(indexPath, (self?.postcardArray)!)
//            }
//            }, failureClosure: nil, completionClosure: nil)
//    }
//
//    //MARK: 从CoreData中获取已存储社区数据
//    /**
//     从CoreData中获取已存储社区数据
//
//     - returns: [PostCardModel]?
//     */
//    func getDataFromCoreData(){
//
//        var postcards: [PostcardModel] = [PostcardModel]()
//
//        doInBackground(inBackground: { [weak self] _ in
//            postcards = (self?.dataModel.getPostcardInfoFromCoreData())!
//            }, backToMainQueue: {[weak self] _ in
//                self?.postcardArray = postcards
//                self?.tableShouldReloadData?(postcards)
//                self?.headerStopRefresh?()
//                self?.dismissSVProgress()
//        })
//
//        //        doInBackground(inBackground: { [weak self] _ in
//        //            postcards = (self?.dataModel.getPostcardInfoFromCoreData())!
//        //        }) { [weak self] _ in
//        //            if postcards.isEmpty == true{
//        //                self?.pushToRefresh()
//        //            }else{
//        //                self?.postcardArray = postcards
//        //                self?.tableShouldReloadData?(postcards)
//        //                self?.headerStopRefresh?()
//        //                self?.dismissSVProgress()
//        //            }
//        //        }
//    }
//
//    //MARK: 发送点赞请求
//    /// 发送点赞请求
//    ///
//    /// - Parameter originalId: originalId
//    func sendLikeRequest(originalId: String, andIndexPath indexPath: IndexPath){
//        dataModel.sendLikeRequest(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
//            if let code: Int = result["code"] as? Int, code == 1000{
//                self?.sendLikeOrCancelLikeSuccess?(true, indexPath)
//            }else{
//                self?.netWorkError?(nil, result)
//            }
//            }, failureClosure: { [weak self] (error: Error?) in
//                self?.netWorkError?(error, nil)
//            }, completionClosure: nil)
//    }
//
//    //MARK: 发送取消点赞的请求
//    /// 发送取消点赞的请求
//    ///
//    /// - Parameter originalId: originalId
//    func sendCancelLikeRequest(originalId: String, andIndexPath indexPath: IndexPath){
//        dataModel.sendCancelLikeRequest(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
//            if let code: Int = result["code"] as? Int, code == 1000{
//                self?.sendLikeOrCancelLikeSuccess?(false, indexPath)
//            }else{
//                self?.netWorkError?(nil, result)
//            }
//            }, failureClosure: { [weak self] (error: Error?) in
//                self?.netWorkError?(error, nil)
//            }, completionClosure: nil)
//    }
//
//    //MARK: 检测是否有权限买图
//    /// 检测是否有权限买图
//    ///
//    /// - Parameters:
//    ///   - originalid: 原创ID
//    ///   - indexPath: 数据来源
//    func checkPurchaseAvaliable(originalId: String, andIndexPath indexPath: IndexPath){
//        showSVProgress(title: nil)
//        dataModel.checkPurchaseAvaliable(originalId: originalId, successClosure: { [weak self] (result: [String : Any]) in
//            if let code: Int = result["code"] as? Int, code == 1000{
//                self?.checkPurchaseAvailableSuccess?(indexPath)
//            }else{
//                self?.netWorkError?(nil, result)
//            }
//            }, failureClosure: { [weak self] (error: Error?) in
//                self?.netWorkError?(error, nil)
//            }, completionClosure: {[weak self] _ in
//                self?.dismissSVProgress()
//        })
//    }
//
//    //MARK: 检查是否有未读消息
//    /// 检查是否有未读消息
//    func checkMessageStatus(){
//        unReadMessage = 0
//        //先检查推送
//        dataModel.checkUnReadMessage(success: { [weak self] (result: [String : Any]) in
//            if let code: Int = result["code"] as? Int, code == 1000{
//                let data: [String: Any] = result["data"] as! [String: Any]
//                if let count: Int = data["count"] as? Int {
//                    self?.unReadMessage += count
//                    self?.checkSystemMessageStatus()//再检查系统消息
//                }
//            }
//            }, failureClosure: nil)
//    }
//
//    //MARK: 检查是否有未读的系统消息
//    fileprivate func checkSystemMessageStatus(){
//        dataModel.checkUnreadSystemMessage(success: { [weak self] (result: [String : Any]) in
//            if let code: Int = result["code"] as? Int, code == 1000{
//                let data: [String: Any] = result["data"] as! [String: Any]
//                if let count: Int = data["count"] as? Int {
//                    self?.unReadMessage += count
//                    self?.checkMessageStatusClosure?((self?.unReadMessage)!)
//                }
//            }
//            }, failureClosure: nil)
//    }
}
