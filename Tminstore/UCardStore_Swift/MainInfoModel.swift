//
//  MainInfoModel.swift
//  UCardStore_Swift
//
//  Created by 朱佳杰 on 2017/10/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit

class MainInfoModel: NSObject {
    
    var userImage: String?
    
    var titleInfo: String?
    
    var timeInfo: String?
    
    var priceInfo: String?
    
    var contentImagen: String?
    
    var iconImage: String?
    
    var countInfo: String?
    
    
}
