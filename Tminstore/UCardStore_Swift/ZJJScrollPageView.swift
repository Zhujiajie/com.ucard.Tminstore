//
//  ZJJScrollPageView.swift
//  UCardStore_Swift
//
//  Created by 朱佳杰 on 2017/10/24.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UIKit
import SnapKit

class ZJJScrollPageView: UIView {
    
    // 存放标题的数组
    var titleArray: [String]?
    
    // 存放页面的数组
    var pageArray: [UITableViewController]? {
        didSet {
            // UI设置
            setupUI()
        }
    }

    // 当前选择的按钮
    fileprivate var selectButton: UIButton = UIButton()
    
    // 按钮下方的View
    fileprivate var lineView: UIView?
    
    //MARK: 构造方法
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: 懒加载
    fileprivate lazy var titleScrollView: UIScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width:screenWidth , height: 44))
    
    fileprivate lazy var pageScrollView: UIScrollView = UIScrollView(frame: CGRect.zero)
    
}

extension ZJJScrollPageView {
    
    // 初始化UI
    fileprivate func setupUI() {
        backgroundColor = UIColor.white
        
        //titleScrollView
        titleScrollView.backgroundColor = UIColor(hexString: "F2F2F2")
        titleScrollView.showsHorizontalScrollIndicator = false
        titleScrollView.bounces = false
        addSubview(titleScrollView)
        
        //pageScrollView
        let maxY = titleScrollView.frame.maxY
        pageScrollView.frame = CGRect(x: 0, y: maxY, width: screenWidth, height: screenHeight - maxY - 64 - 48)
        pageScrollView.showsHorizontalScrollIndicator = true
        pageScrollView.isPagingEnabled = true
        pageScrollView.delegate = self
        pageScrollView.bounces = false
        addSubview(pageScrollView)
        
        guard let titleArray = titleArray else {
            return
        }
        guard let pageArray = pageArray else {
            return
        }
        
        //MARK: 设置数据
        let margin: CGFloat = 10
        let count: Int = titleArray.count
        let width: CGFloat = (screenWidth - margin * CGFloat.init(count + 1)) / CGFloat.init(count)
        
        // 设置下方的View
        lineView = UIView.init(frame: CGRect.init(x: margin + 5.0, y: 42, width: width - 10.0, height: 5))
        lineView?.backgroundColor = UIColor(hexString: "F2BA74")
        titleScrollView.addSubview(lineView!)
        for i in (0 ..< titleArray.count){
            //title
            let titleButton: UIButton = UIButton.init()
            titleButton.setTitle(titleArray[i], for: UIControlState.normal)
            titleButton.setTitleColor(UIColor(hexString: "424242"), for: UIControlState.normal)
            titleScrollView.addSubview(titleButton)
    
            print(titleButton.bounds.size.width)
            titleButton.addTarget(self, action: #selector(self.titleButtonClick(button:)), for: UIControlEvents.touchUpInside)
            titleButton.frame = CGRect(x: margin * (CGFloat.init(i + 1)) + width * CGFloat.init(i) , y: 4.0, width: width, height: 40)
            titleButton.tag = i + 1000
            //默认选中第一个
            if i == 0 {
                titleButtonClick(button: titleButton)
            }
            
            //页面
            let vc: UITableViewController = pageArray[i]
            pageScrollView.addSubview(vc.view)
            vc.view.frame = bounds.offsetBy(dx: CGFloat(i) * screenWidth, dy: 0)
        }
        
        titleScrollView.contentSize = CGSize(width: Int(width), height: 0)
        pageScrollView.contentSize = CGSize(width: pageArray.count * Int(screenWidth), height: 0)
        
    }
    
    //MARK: 点击按钮的方法
    @objc private func titleButtonClick(button:UIButton) {
        
        let margin: CGFloat = 10
        let count: Int = titleArray!.count
        let width: CGFloat = (screenWidth - margin * CGFloat.init(count + 1)) / CGFloat.init(count)
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.selectButton.setTitleColor(UIColor(hexString: "424242"), for: UIControlState.normal)
            button.setTitleColor(UIColor(hexString: "F2BA74"), for: UIControlState.normal)
            self?.selectButton = button
            //滚动到指定的位置
            self?.pageScrollView.setContentOffset(CGPoint(x: (button.tag - 1000) * Int(screenWidth), y: 0), animated: true)
            self?.lineView?.frame = CGRect.init(x: margin * (CGFloat.init((button.tag - 1000) + 1)) + width * CGFloat.init((button.tag - 1000)), y: 40, width: width - 10.0, height: 5)
        }
        
//        setUptitleButtonCenter(button: button)
        
    }
    
    //设置标题按钮居中
    fileprivate func setUptitleButtonCenter(button:UIButton){
        
        var offsetX:CGFloat = button.center.x - screenWidth * 0.5
        
        if offsetX < 0 {
            offsetX = 0
        }
        
        //最大的偏移量
        let maxX:CGFloat = titleScrollView.contentSize.width - screenWidth
        if offsetX > maxX {
            
            offsetX = maxX
        }
        
        titleScrollView.setContentOffset(CGPoint(x: offsetX, y: 0), animated: true)
    }
    
}

//MARK: scrollView代理方法
extension ZJJScrollPageView:UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let margin: CGFloat = 10
        let count: Int = titleArray!.count
        let width: CGFloat = (screenWidth - margin * CGFloat.init(count + 1)) / CGFloat.init(count)
        
        let page = scrollView.contentOffset.x / scrollView.bounds.width
        
        print("page is : \(page)")
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            let titleButton = self?.viewWithTag(Int(page) + 1000) as! UIButton
            self?.selectButton.setTitleColor(UIColor(hexString: "424242"), for: UIControlState.normal)
            titleButton.setTitleColor(UIColor(hexString: "F2BA74"), for: UIControlState.normal)
            self?.selectButton = titleButton
            self?.lineView?.frame = CGRect.init(x: margin * (CGFloat.init(page + 1)) + width * CGFloat.init(page), y: 40, width: width - 10.0, height: 5)
        }
    }
    
}

