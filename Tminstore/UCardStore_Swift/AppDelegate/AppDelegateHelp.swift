//
//  AppDelegateHelp.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 2016/10/26.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SDWebImage
import Instabug

class AppDelegateHelp: NSObject, JPUSHRegisterDelegate {
    
    /// 定位
    fileprivate var locationManager: AMapLocationManager?
    
    lazy var dataModel: BaseDataModel = BaseDataModel()
    lazy var replyModel: MainDetialViewModel = MainDetialViewModel()
    
    convenience init(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?){
        self.init()
        
        SVProgressHUD.setMinimumSize(CGSize(width: 120, height: 120))//设置加载指示框的大小
        // 初始化高德地图
        AMapServices.shared().apiKey = "ada0c40c91d1e70e47918e3b8635b47c"
        AMapServices.shared().crashReportEnabled = false//关闭crash上传
        
        Timer.scheduledTimer(timeInterval: 180, target: self, selector: #selector(startInstabugIntroMessage(_:)), userInfo: nil, repeats: false)//一分钟之后，再弹出Instabug的介绍窗口
        setLocationManager()//定位
        //设置第三方SDK
        setThreePartSDK(application, options: launchOptions)
        
        delay(seconds: 10, completion: {[weak self] _ in
            self?.setupJPush(launchOptions)//极光
//            self?.set3DTouch(application: application)//3D Touch
            //设置iOS 10通知
            if #available(iOS 10.0, *) {
                self?.configureNotifications()
            }
        }, completionBackground: {[weak self] _ in
            
                self?.checkUpdata()//强制更新
            //MARK: 暂时取消下载字体
//                self?.downLoadFont()//下载字体
                self?.requestLaunchScreen()//更新动态启动页
                //设置ping++
                Pingpp.setAppId("app_q9yjTSSmXTq1SKSq")
                Pingpp.setDebugMode(false)
        })
    }
    
    //MARK: 注册推送
    func registRemoteNotification(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        JPUSHService.registerDeviceToken(deviceToken)//注册deviceToken
        //注册极光成功的回调
        JPUSHService.registrationIDCompletionHandler { [weak self](resCode: Int32, pushID: String?) in
            if resCode == 0{
                saveJPushRegistrationID(registrationID: pushID)//保存极光ID
                self?.dataModel.postJPushID()//向后台发送pushID
            }
        }
    }
    
    //MARK: 设置3D_Touch
    fileprivate func set3DTouch(application: UIApplication){
        if #available(iOS 9.1, *) {
            let icon1: UIApplicationShortcutIcon = UIApplicationShortcutIcon(templateImageName: "scan")
            let item1: UIApplicationShortcutItem = UIApplicationShortcutItem(type: "scan", localizedTitle: NSLocalizedString("扫描二维码", comment: ""), localizedSubtitle: nil, icon: icon1, userInfo: nil)
            application.shortcutItems = [item1]
        }
    }
    
    //MARK: 设置极光推送
    /**
     设置极光推送
     */
    fileprivate func setupJPush(_ launchOptions: [AnyHashable: Any]?) {
        
        JPUSHService.setLogOFF()//关闭调试日志输出
        
        let entity: JPUSHRegisterEntity = JPUSHRegisterEntity()
        entity.types = Int(JPAuthorizationOptions.alert.rawValue|JPAuthorizationOptions.badge.rawValue|JPAuthorizationOptions.sound.rawValue)
        JPUSHService.register(forRemoteNotificationConfig: entity, delegate: self)
        
        JPUSHService.setup(withOption: launchOptions, appKey: appAPIHelp.JPushAppKey, channel: appAPIHelp.JPushChannel, apsForProduction: appAPIHelp.isProduction, advertisingIdentifier: nil)//配置极光推送
    }
    
    //MARK: 设置第三方SDK
    /**
     设置第三方SDK
     
     - parameter application: UIApplication
     - parameter options:     launchOptions
     */
    fileprivate func setThreePartSDK(_ application: UIApplication, options: [AnyHashable: Any]?) {
        
        doInBackground(inBackground: {
            //友盟统计
            UMAnalyticsConfig.sharedInstance().appKey = appAPIHelp.UMengAPPKey//app key
            MobClick.start(withConfigure: UMAnalyticsConfig.sharedInstance())
            MobClick.setLogEnabled(false)//关闭输出日志
            MobClick.setCrashReportEnabled(false)//关闭崩溃日志收集
            
            //友盟第三方分享和登录
            UMSocialManager.default().openLog(false)//打开日志
            UMSocialManager.default().umSocialAppkey = appAPIHelp.UMengAPPKey//app key
            
            //设置微博
            UMSocialManager.default().setPlaform(UMSocialPlatformType.sina, appKey: appAPIHelp.WeiboAppKey, appSecret: appAPIHelp.WeiboAppSecret, redirectURL: appAPIHelp.WeiboRedirectURL)
            
            //设置微信
            UMSocialManager.default().setPlaform(UMSocialPlatformType.wechatSession, appKey: appAPIHelp.WeiChatAppKey, appSecret: appAPIHelp.WeiChatAppSecret, redirectURL: "@http://mobile.umeng.com/social")
        })
        
        delay(seconds: 30, completion: {
            //MARK: 设置Instabug反馈
            Instabug.start(withToken: "da866d1c80843168f26bf26c782a6b3a", invocationEvent: IBGInvocationEvent.shake)
            Instabug.setIntroMessageEnabled(false)//先关闭提示窗口
            Instabug.setValue("Tministore", forStringWithKey: kIBGTeamStringName)
            Instabug.setPromptOptionsEnabledWithBug(true, feedback: true, chat: false)
            Instabug.setUserStepsEnabled(false)
            Instabug.setCrashReportingEnabled(false)
            Instabug.setPushNotificationsEnabled(false)
            Instabug.setShakingThresholdForiPhone(2.5, foriPad: 2.0)//唤出Instabug的摇动幅度
            Instabug.setIBGLogPrintsToConsole(false)//关闭打印
        }, completionBackground: {
            //自动提醒用户去评价app
            iRate.sharedInstance().useUIAlertControllerIfAvailable = true//使用UIAlertController
            iRate.sharedInstance().promptForNewVersionIfUserRated = true//每个新版本都提醒用户去评价
        })
    }
    
    //MARK: 开启instabug的介绍窗口
    func startInstabugIntroMessage(_ timer: Timer){
        Instabug.setIntroMessageEnabled(true)
    }
    
    //MARK: 处理推送
    func handleRemoteNotification(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        
        JPUSHService.handleRemoteNotification(userInfo)
        // 打开一个webView
        if let urlString: String = userInfo["openWebView"] as? String, urlString != ""{
            enterWebView(urlString: urlString)
        }else if let category: String = userInfo["category"] as? String{
            //进入评论界面
            if category == "comment"{
                if let originalId: String = userInfo["originalId"] as? String{
                    enterCommentVC(originalId: originalId, andIsPurchase: false)
                }
            }
                //进入回复详情界面
            else if category == "reply"{
                if let originalId: String = userInfo["originalId"] as? String, let commentId: String = userInfo["commentId"] as? String{
                    enterReplyVC(originalId: originalId, andCommentId: commentId)
                }
            }
                //买图的推送，进入发布界面
            else if category == "purchase"{
                if let originalId: String = userInfo["originalId"] as? String{
                    enterCommentVC(originalId: originalId, andIsPurchase: true)
                }
            }
                //订单发货的推送，进入订单界面
            else if category == "deliver"{
                if let mailOrderNo: String = userInfo["mailOrderNo"] as? String{
                    enterOrderInfoVC(mailOrderNo: mailOrderNo)
                }
            }
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    //MARK: 设置通知的按钮
    @available(iOS 10.0, *)
    fileprivate func configureNotifications(){
        //快捷回复
        let replyAction: UNTextInputNotificationAction = UNTextInputNotificationAction(identifier: "reply", title: NSLocalizedString("快捷回复", comment: ""), options: [], textInputButtonTitle: NSLocalizedString("发送", comment: ""), textInputPlaceholder: NSLocalizedString("快捷回复", comment: ""))
        let commentCategory: UNNotificationCategory = UNNotificationCategory(identifier: "comment", actions: [replyAction], intentIdentifiers: [], options: [])
        let replyCategory: UNNotificationCategory = UNNotificationCategory(identifier: "reply", actions: [replyAction], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([commentCategory, replyCategory])
    }
    
    @available(iOS 10.0, *)
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, didReceive response: UNNotificationResponse!, withCompletionHandler completionHandler: (() -> Void)!) {
        
        let userInfo: [AnyHashable: Any] = response.notification.request.content.userInfo
        if response.notification.request.trigger?.isKind(of: UNPushNotificationTrigger.self) == true{
            JPUSHService.handleRemoteNotification(userInfo)
            // 打开一个webView
            if let urlString: String = userInfo["openWebView"] as? String, urlString != ""{
                enterWebView(urlString: urlString)
            }
            //点击了回复按钮
            else if response.actionIdentifier == "reply"{
                if let textResponse: UNTextInputNotificationResponse = response as? UNTextInputNotificationResponse{
                    quickReply(userInfo: userInfo, andReplyContent: textResponse.userText)//快捷回复
                    JPUSHService.resetBadge()//重置服务器角标为0
                    UIApplication.shared.applicationIconBadgeNumber = 0//设置本地app角标为0
                }
            }else if let category: String = userInfo["category"] as? String{
                //进入评论界面
                if category == "comment"{
                    if let originalId: String = userInfo["originalId"] as? String{
                        enterCommentVC(originalId: originalId, andIsPurchase: false)
                    }
                }
                //进入回复详情界面
                else if category == "reply"{
                    if let originalId: String = userInfo["originalId"] as? String, let commentId: String = userInfo["commentId"] as? String{
                        enterReplyVC(originalId: originalId, andCommentId: commentId)
                    }
                }
                //买图的推送，进入发布界面
                else if category == "purchase"{
                    if let originalId: String = userInfo["originalId"] as? String{
                        enterCommentVC(originalId: originalId, andIsPurchase: true)
                    }
                }
                //订单发货的推送，进入订单界面
                else if category == "deliver"{
                    if let mailOrderNo: String = userInfo["mailOrderNo"] as? String{
                        enterOrderInfoVC(mailOrderNo: mailOrderNo)
                    }
                }
            }
        }
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, willPresent notification: UNNotification!, withCompletionHandler completionHandler: ((Int) -> Void)!) {
        let userInfo: [AnyHashable: Any] = notification.request.content.userInfo
        if notification.request.trigger?.isKind(of: UNPushNotificationTrigger.self) == true{
            JPUSHService.handleRemoteNotification(userInfo)
        }
        completionHandler(Int(UNNotificationPresentationOptions.alert.rawValue))
    }
    
    //MARK: 点击评论回复的消息，进入评论详情界面
    fileprivate func enterCommentVC(originalId: String, andIsPurchase isPurchase: Bool){
        let detaillVC: MainDetialController = MainDetialController()
        detaillVC.originalId = originalId
        detaillVC.shouldUseDismiss = true//需要使用dismiss方法
        
        if isPurchase == true{
            detaillVC.isReleaseVC = true
            detaillVC.shouldWaitData = true
        }
        let nav: UINavigationController = UINavigationController(rootViewController: detaillVC)
        topViewController().present(nav, animated: true, completion: nil)
    }
    
    //MARK: 进入webView
    fileprivate func enterWebView(urlString: String){
        let webView: BaseWebView = BaseWebView()
        webView.urlString = urlString
        let nav: UINavigationController = UINavigationController(rootViewController: webView)
        topViewController().present(nav, animated: true, completion: nil)
    }
    
    //MARK: 进入回复详情界面
    fileprivate func enterReplyVC(originalId: String, andCommentId commentId: String){
        let replyVC: MainReplyController = MainReplyController()
        replyVC.originalId = originalId
        replyVC.commentId = commentId
        replyVC.shouldUseDismiss = true//需要使用dismiss方法
        let nav: UINavigationController = UINavigationController(rootViewController: replyVC)
        topViewController().present(nav, animated: true, completion: nil)
    }
    
    //MARK: 进入订单详情界面
    fileprivate func enterOrderInfoVC(mailOrderNo: String){
        let orderVC: PDetialVC = PDetialVC()
        orderVC.mailOrderNo = mailOrderNo
        orderVC.shouldUseDismiss = true
        let nav: UINavigationController = UINavigationController(rootViewController: orderVC)
        topViewController().present(nav, animated: true, completion: nil)
    }
    
    //MARK: 快捷回复
    fileprivate func quickReply(userInfo: [AnyHashable: Any], andReplyContent content: String){
        guard let originalId: String = userInfo["originalId"] as? String, let commentId: String = userInfo["commentId"] as? String, let toMemberCode: String = userInfo["memberCode"] as? String else { return }
        //先生成评论模型
        let comment: CommentModel = CommentModel()
        comment.originalId = originalId
        comment.commentId = commentId
        //发表回复
        replyModel.postReply(replyContent: content, commentModel: comment, andToMemberCode: toMemberCode)
    }
    
    //MARK: 清理一周之前的缓存
    /// 清理一周之前的缓存
    func checkCacheCreateTime(){
        checkeFileCreateTime(folderName: "/default/com.hackemist.SDWebImageCache.default")//图片缓存
        checkeFileCreateTime(folderName: "/videoCache")// 视频缓存
    }
    //MARK: 分别计算不同缓存文件夹中文件的大小
    fileprivate func checkeFileCreateTime(folderName: String){
        
        // 取出cache文件夹路径
        let cachePath: String? = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first
        
        if cachePath != nil{
            let folderPath: String = cachePath! + folderName
            if let files: [String] = FileManager.default.subpaths(atPath: folderPath){
                // 快速枚举取出所有文件名
                for p: String in files{
                    // 把文件名拼接到路径中
                    let path: String = folderPath.appendingFormat("/\(p)")
                    // 取出文件属性
                    let floder: [FileAttributeKey: Any] = try! FileManager.default.attributesOfItem(atPath: path)
                    // 用元组取出文件大小属性
                    for (key, value) in floder {
                        // 获得文件的建立时间
                        if key == FileAttributeKey.creationDate{
                            // 删除一周之前的缓存
                            if let date: Date = value as? Date, date.timeIntervalSinceNow < -604800{
                                do{
                                    try FileManager.default.removeItem(at: URL(fileURLWithPath: path))
                                }catch{print(error)}
                            }
                        }
                    }
                }
            }
        }
    }
}

//MARK: 定位和其他一些弹窗相关
extension AppDelegateHelp{
    
    //MARK: 设置定位
    fileprivate func setLocationManager(){
        //高德定位
        locationManager = AMapLocationManager()
        locationManager?.desiredAccuracy = kCLLocationAccuracyHundredMeters//快速定位
        //单次请求定位
        locationManager?.requestLocation(withReGeocode: false, completionBlock: {(location: CLLocation?, reGeoCode: AMapLocationReGeocode?, error: Error?) in
            if error == nil && location != nil{
                doInBackground(inBackground: {
                    //解析出经纬度
                    let latlng: String = "\(location!.coordinate.latitude),\(location!.coordinate.longitude)"
                    saveLocationString(str: latlng)//将定位信息保存
                    currentUserLocation = location!//保存用户当前的地理位置
                }, backToMainQueue: nil)
            }
        })
    }
    
//    //MARK: 访问后端接口，判断是否弹出推广信息
//    /// 访问后端接口，判断是否弹出推广信息
//    func alertCheck(){
//        let parameters: [String: Any] = [
//            "memberCode": getUserID()
//        ]
//        dataModel.sendPOSTRequestWithURLString(URLStr: appAPIHelp.alertCheckAPI, ParametersDictionary: parameters, successClosure: { [weak self] (result: [String : Any]) in
//            //            print(result)
//            if let code: Int = result["code"] as? Int, code == 1000{
//                if let data: [String: Any] = result["data"] as? [String: Any]{
//                    if let flag: Bool = data["flag"] as? Bool, flag == true{
//                        if let content: String = data["content"] as? String{
//                            self?.showActivityAlert(content: content)
//                        }
//                    }
//                }
//            }
//            }, failureClourse: nil)
//    }

    
    //MARK: 检查是否需要获取更新信息
    fileprivate func checkUpdata(){
//        if checkAppUpdateOrNot() == false {
            iTunesAppUpdate()
//        }
        forceUpdate()//检查是否需要强制更新
    }
    
    //MARK: 检查版本更新
    fileprivate func iTunesAppUpdate(){
        
        let systemVersion: String? = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        
        Alamofire.request(appAPIHelp.GetITunesAPI(), method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON { [weak self](response: DataResponse<Any>) in
                switch response.result{
                case .success(_):
                    
                    if let json: [String: Any] = response.result.value as? [String: Any]{
                        if let code: Int = json["resultCount"] as? Int, code == 1{
                            if let results: [[String: Any]] = json["results"] as? [[String: Any]]{
                                if let version: String = results[0]["version"] as? String {
                                    //本地版本比App Store里的版本低，才会提示更新
                                    if version.compare(systemVersion!, options: NSString.CompareOptions.numeric, range: nil, locale: nil) == ComparisonResult.orderedDescending{
                                        let releaseNotes: String = results[0]["releaseNotes"] as! String
                                        self?.showUpdateAlert(releaseNotes: releaseNotes)//弹出更新的提示框
                                    }
                                }
                            }
                        }
                    } 
                case .failure( _):
                    break
                }
        }
    }
    
//    //MARK: 周庄推广活动的弹窗
//    fileprivate func showActivityAlert(content: String){
//        let activityAlertView: BaseActivityAlertView = BaseActivityAlertView(frame: UIScreen.main.bounds)
//        activityAlertView.alertText = content
//        topViewController().view.addSubview(activityAlertView)
//    }
    
    //MARK: 更新的提示框
    fileprivate func showUpdateAlert(releaseNotes: String){
        
//        let ac: UIAlertController = UIAlertController(title: NSLocalizedString("有新版本，是否要更新？", comment: ""), message: releaseNotes, preferredStyle: .alert)
//        
//        let confirmAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("更新", comment: ""), style: .default) { (_) in
//            let url: URL = URL(string: "https://itunes.apple.com/us/app/ucardstore-send-share-real/id966372400?mt=8")!
//            if UIApplication.shared.canOpenURL(url){
//                UIApplication.shared.openURL(url)
//            }
//        }
//        ac.addAction(confirmAction)
//        
//        let ignore: UIAlertAction = UIAlertAction(title: NSLocalizedString("忽略", comment: ""), style: .cancel, handler: nil)
//        ac.addAction(ignore)
        
        let updateAlertView: BaseUpdateAlertView = BaseUpdateAlertView(frame: UIScreen.main.bounds)
        updateAlertView.updateText = releaseNotes
        topViewController().view.addSubview(updateAlertView)
    }
    
    //MARK: 下载字体到缓存文件夹
    fileprivate func downLoadFont(){
        if FileManager.default.fileExists(atPath: appAPIHelp.fontPath().path) == false{
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                return (appAPIHelp.fontPath(), [.removePreviousFile, .createIntermediateDirectories])
            }
            Alamofire.download(FZYBXSJW_GB1_0_URL, to: destination).response(completionHandler: { (response: DefaultDownloadResponse) in
                if response.error != nil{
                    do{
                        try FileManager.default.removeItem(at: appAPIHelp.fontPath())
                    }catch{ print(error) }
                }
            })
        }
    }
    
    //MARK: 检查是否需要强制更新
    fileprivate func forceUpdate(){
        let url: String = "http://ucardstorevideo.b0.upaiyun.com/AndroidFiles/java4_0.json"
        Alamofire.request(url, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON { [weak self](response: DataResponse<Any>) in
                switch response.result{
                case .success(_):
                    if let json: [String: Any] = response.result.value as? [String: Any]{
                        if let result: Bool = json["shouldUpdate"] as? Bool, result == true{
                            var key: String
                            if languageCode() == "CN"{
                                key = "cnMessage"
                            }else{
                                key = "enMessage"
                            }
                            let message = json[key] as! String
                            self?.forceUpdateAlert(title: message)
                        }
                    }
                case .failure( _):
                    break
                }
        }
    }
    
    //MARK: 强制更新的提示框
    fileprivate func forceUpdateAlert(title: String){
        let ac: UIAlertController = UIAlertController(title: title, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        let confirmAction: UIAlertAction = UIAlertAction(title: NSLocalizedString("更新", comment: ""), style: UIAlertActionStyle.default) { [weak self] (_) in
            let url = URL(string: "https://itunes.apple.com/us/app/ucardstore-send-share-real/id966372400?mt=8")!
            if UIApplication.shared.canOpenURL(url){
                UIApplication.shared.openURL(url)
            }
            self?.forceUpdateAlert(title: title)
        }
        ac.addAction(confirmAction)
        app.window?.rootViewController?.present(ac, animated: true, completion: nil)
    }
    
    //MARK: 访问动态启动页，并保存相关数据
    fileprivate func requestLaunchScreen(){
        Alamofire.request(launchScreenURLStr, method: HTTPMethod.get, parameters: nil, encoding: URLEncoding.default, headers: nil)
            .responseJSON { (response: DataResponse<Any>) in
                if let json: [String: Any] = response.result.value as? [String: Any]{
                    if let imageURlStr: String = json["imageURL"] as? String, let imageURL: URL = URL(string: imageURlStr + "!/format/webp"){
                        saveLaunchScreenImageURL(imageURLString: imageURlStr)
                        SDWebImagePrefetcher.shared().prefetchURLs([imageURL])//预先下载图片
                    }else{
                        saveLaunchScreenImageURL(imageURLString: nil)
                    }
                    if let webViewURLStr: String = json["webViewURL"] as? String{
                        saveLaunchScreenWebView(urlString: webViewURLStr)
                    }else{
                        saveLaunchScreenWebView(urlString: nil)
                    }
                    if let timeCount: Double = json["timeCount"] as? Double{
                        saveLaucnScreenCountTime(count: timeCount)
                    }else{
                        saveLaucnScreenCountTime(count: 2)
                    }
                    if let beginTime: Double = json["beginTime"] as? Double{
                        saveLaunchScreenBeginTime(beginTime: beginTime)
                    }
                    if let endTime: Double = json["endTime"] as? Double{
                        saveLaunchScreenEndTime(endTime: endTime)
                    }
                }
        }
    }
    
    //MARK: 清空登录信息
    /// 清空登录信息
    func clearLoginInfo(){
        MobClick.profileSignOff()//用户退出登录，清空友盟的账号信息
        saveUserToken(token: "")//清空Token
        saveUserID(userId: "")//清空用户的id
        saveUserName(name: nil)//清空用户昵称
        setLoginStatus(loginStatus: false)//设置app为未登陆状态
        setUserHasLoadAPP(false)//设置用户未登陆过app
    }
}
