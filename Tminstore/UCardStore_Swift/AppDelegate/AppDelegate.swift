//
//  AppDelegate.swift
//  UCardStore_Swift
//
//  Created by Wenslow on 16/6/2.
//  Copyright © 2016年 ucard. All rights reserved.
//

import UIKit
import CoreData
import MobileCoreServices

//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate{

    var window: UIWindow?
    /// app进入前台的闭包
    var appWillEnterForeground: (()->())?
    /// app 进入后台的闭包
    var appDidEnterBackground: (()->())?
    /// app处于不活动状态
    var appWillResignActive: (()->())?
    /// app处于活动状态
    var appDidBecomeActive: (()->())?
    /// app即将终结
    var appWillTerminate: (()->())?
    
    /// 主控制器
    var containerVC: ContainerViewController?
    
    fileprivate var appDelegateHelp: AppDelegateHelp?
    
    /// 活动页面
    fileprivate var launchView: LaunchScreenView?
    
    /// Unity控制器
    var currentUnityController: ZTARController!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        appDelegateHelp = AppDelegateHelp(application: application, didFinishLaunchingWithOptions: launchOptions)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.white


        //判断用户是否在使用TminstoreToken
        if isUsingTminstoreToken() == false {
            //没有在使用TminstoreToken, 清空登录信息
            appDelegateHelp?.clearLoginInfo()
            setUsingTminstoreToken()
        }
        
        // 进入引导页
        if getGuideVCStatus() == false{
            let guideVC: GuideViewController = GuideViewController()
            guideVC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            window?.rootViewController = guideVC
        }else{
            //判断用户是否已登录
            if getUserLoginStatus() == false{
                configLoginVC()
            }else{
                setMainVC()//创建主页
            }
        }
        
        //活动页面
        launchView = LaunchScreenView()
        window?.rootViewController?.view.addSubview(launchView!)
        launchView?.frame = UIScreen.main.bounds
        window?.rootViewController?.view.bringSubview(toFront: launchView!)
        
        currentUnityController = ZTARController()
        currentUnityController.application(application,didFinishLaunchingWithOptions: launchOptions)
        
        delay(seconds: 1, completion: { [weak self] _ in
            self?.currentUnityController.pauseUnity()
        }, completionBackground: nil)
        
        window?.makeKeyAndVisible()
        
        return true
    }
    
    //MARK: 未登录。设置登录界面
    /**
     未登录。设置登录界面
     */
    func configLoginVC(shouldUseDismiss: Bool = false) {
        let loginVC: PLoginViewController = PLoginViewController()
        loginVC.shouldUseDismiss = shouldUseDismiss
        window?.rootViewController = loginVC
    }
    
    //MARK: 设置主界面
    /// 设置主界面
    func setMainVC(){
        let mainTabVC = BaseTabBarViewController()
//        containerVC = ContainerViewController()
        window?.rootViewController = mainTabVC
        setGuideVCStatus()
    }
    
//MARK: -----------------------------------------
    //MARK:注册deviceToken
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        appDelegateHelp?.registRemoteNotification(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    //MARK:处理接收到的推送消息
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if #available(iOS 10.0, *){
            completionHandler(UIBackgroundFetchResult.newData)
        }else{
            appDelegateHelp?.handleRemoteNotification(application, didReceiveRemoteNotification: userInfo, fetchCompletionHandler: completionHandler)
        }
    }

    //MARK: 应用进入后台时，清空显示消息数量的角标
    func applicationWillResignActive(_ application: UIApplication) {
        JPUSHService.resetBadge()//重置服务器角标为0
        UIApplication.shared.applicationIconBadgeNumber = 0//设置本地app角标为0
        appWillResignActive?()
        currentUnityController.applicationWillResignActive(application)
    }
    
    //MARK: 应用处于active状态
    func applicationDidBecomeActive(_ application: UIApplication) {
        appDidBecomeActive?()
        currentUnityController.applicationDidBecomeActive(application)
    }
    
    //MARK: 应用进入后台
    func applicationDidEnterBackground(_ application: UIApplication) {
        appDidEnterBackground?()
        currentUnityController.applicationDidEnterBackground(application)
    }
    //MARK: 应用进入前台
    func applicationWillEnterForeground(_ application: UIApplication) {
        appWillEnterForeground?()
        currentUnityController.applicationWillEnterForeground(application)
    }
    //MARK: 应用即将终结
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
        appDelegateHelp?.checkCacheCreateTime()//清理一周之前的缓存
        currentUnityController.applicationWillTerminate(application)
    }
    
    //iOS 9
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        
        if Pingpp.handleOpen(url, withCompletion: nil) == true{
            return true
        }else{
            return UMSocialManager.default().handleOpen(url, options: options)
        }
    }
    
    // iOS 8及以下
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if Pingpp.handleOpen(url, withCompletion: nil) == true{
            return true
        }else{
            return UMSocialManager.default().handleOpen(url, sourceApplication: sourceApplication, annotation: annotation)
        }
    }
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        if Pingpp.handleOpen(url, withCompletion: nil) == true{
            return true
        }else{
            return UMSocialManager.default().handleOpen(url)
        }
    }
    
//    //MARK: -------3D_Touch的回调-----------
//    @available(iOS 9.0, *)
//    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
//        if shortcutItem.type == "scan" && getUserLoginStatus() == true{
//            // 若相机不可用，则跳过
//            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) == false {
//                return
//            }
//            //进入优惠券扫描界面
//            let qrScanVC: QRViewController = QRViewController()
//            qrScanVC.shouldDisplayCodeInputView = true
//            topViewController().present(qrScanVC, animated: true, completion: nil)
//        }
//    }
    
    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.ucard.app.UCardStore_Swift" in the application's documents Application Support directory.
        let urls: [URL] = FileManager.default.urls(for: FileManager.SearchPathDirectory.documentDirectory, in: FileManager.SearchPathDomainMask.userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL: URL = Bundle.main.url(forResource: "UCardStore_Swift", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator: NSPersistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url: URL = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason: String = "There was an error creating or loading the application's saved data."
        
        let options: [String: Bool] = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch let error as NSError{
            // Report any error we got.
            var dict: [String: AnyObject] = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error
            let wrappedError: NSError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }catch{ }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator: NSPersistentStoreCoordinator = self.persistentStoreCoordinator
        var managedObjectContext: NSManagedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror: NSError = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
//                abort()
            }
        }
    }
}
