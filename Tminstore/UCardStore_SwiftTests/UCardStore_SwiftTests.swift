//
//  UCardStore_SwiftTests.swift
//  UCardStore_SwiftTests
//
//  Created by Wenslow on 16/6/2.
//  Copyright © 2016年 ucard. All rights reserved.
//

import XCTest
@testable import UCardStore_Swift

class UCardStore_SwiftTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
//        testDownARImage()
//        checkCache()
//        requestOrderInfo()
//        requestMapData()
//        testLocationSearch()
//        testGiftAPI()
//        alertCheck()
        checkUserPhoto()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    //MARK: 判断是否弹出推广弹窗
    fileprivate func alertCheck(){
//        let exp: XCTestExpectation = self.expectation(description: "推广信息弹窗")
//        app.appDelegateHelp?.alertCheck(success: {(result: [String: Any]) in
//            exp.fulfill()
//            print(result)
//        })
//        self.waitForExpectations(timeout: 10) { (error: Error?) in
//            if error != nil{
//                print(error!)
//            }
//        }
    }
    
    //MARK: 查看用户的图片
    fileprivate func checkUserPhoto(){
//        let exp: XCTestExpectation = self.expectation(description: "查看礼品卷接口")
//        let viewModel: PPhotoViewModel = PPhotoViewModel()
//        viewModel.requestUserPhoto()
//        viewModel.getResut = {(result: [String: Any]) in
//            print(result)
//            exp.fulfill()
//        }
//        
//        self.waitForExpectations(timeout: 5) { (error) in
//            if error != nil {
//                print(error!.localizedDescription)
//            }
//        }
    }
    
    //MARK: 测试查看礼品券接口
    fileprivate func testGiftAPI(){
        
//        let exp: XCTestExpectation = self.expectation(description: "查看礼品卷接口")
        let viewModel: MUCouponViewModel = MUCouponViewModel()
        viewModel.getGiftCoupon()
//        viewModel.giftResult = {(result: [String: Any]) in
//            exp.fulfill()
//            print("==================")
//            print(result)
//        }
        self.waitForExpectations(timeout: 15) { (error: Error?) in
            if error != nil{
                print(error!)
            }
        }
    }
    
    fileprivate func testDownARImage(){
        
        let exp: XCTestExpectation = self.expectation(description: "下载AR识别图")
        
        let viewModel: QRViewModel = QRViewModel()
        viewModel.requestARImageInfo(code: "lfik34")
        viewModel.requestARImageSuccess = {(imagePath: String, videoURLString: String) in
            print(imagePath)
            print(videoURLString)
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { (error: Error?) in
            if error != nil{
                print(error!.localizedDescription)
            }
        }
    }
    
    fileprivate func testBase64(){
        
        let base64Sring: String = "aHR0cDovL3VjYXJkc3RvcmV2aWRlby5iMC51cGFpeXVuLmNvbS92aWRlby8xYTNlMjM0NC0wOTQ2LTExZTctYWZkYy0wYTAwMjcwMDAwMDIubXA0"
        
        if let data: Data = Data(base64Encoded: base64Sring, options: []){
            if let string: NSString = NSString(data: data, encoding: String.Encoding.utf8.rawValue){
                print(string)
            }
        }
    }
    
    fileprivate func checkCache(){
        let appHelper: AppDelegateHelp = AppDelegateHelp()
        appHelper.checkCacheCreateTime()
    }
    
    //MARK: 请求单个订单信息的请求
    fileprivate func requestOrderInfo(){
        
        let exp: XCTestExpectation = self.expectation(description: "单个订单信息")
        
        let viewModel: PDetialViewModel = PDetialViewModel()
        viewModel.requestOrderInfo(mailOrderNo: "1489544288471l79h3nnap2l54j5g")
        viewModel.requestOrderInfoSuccess = {(order: OrderPostcardModel) in
            XCTAssertNotNil(order)
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { (error: Error?) in
            if error != nil{
                print(error!.localizedDescription)
            }
        }
    }
    
    //MARK: 请求周围1000米地图数据
    fileprivate func requestMapData(){
        let exp: XCTestExpectation = self.expectation(description: "周围1000米地图数据")
        
        let viewModel: MapViewModel = MapViewModel()
        viewModel.requestMapData(coordinate: currentUserLocation!.coordinate)
        viewModel.getMapDataSuccess = {
            
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { (error: Error?) in
            if error != nil{
                print(error!.localizedDescription)
            }
        }
    }
    
    //MARK: 测试高德搜索
    fileprivate func testLocationSearch(){
        
        let exp: XCTestExpectation = self.expectation(description: "高德搜索")
        
        currentUserLocation = CLLocation(latitude: 31.207491590711804, longitude: 121.60275661892361)
        
        let viewModel: MapLocationViewModel = MapLocationViewModel()
        viewModel.requestAroundPOI(keyWord: nil)
        
        viewModel.searchSuccess = {
            exp.fulfill()
        }
        
        self.waitForExpectations(timeout: 10) { (error: Error?) in
            if error != nil{
                print(error!)
            }
        }
    }
}
