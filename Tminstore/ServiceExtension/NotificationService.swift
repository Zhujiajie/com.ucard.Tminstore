//
//  NotificationService.swift
//  ServiceExtension
//
//  Created by Wenslow on 2017/2/4.
//  Copyright © 2017年 ucard. All rights reserved.
//

import UserNotifications
import MobileCoreServices

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent: UNMutableNotificationContent = bestAttemptContent{
            //1 获取图片地址
            guard let attachmentString: String = bestAttemptContent.userInfo["attachment-url"] as? String, let attachmentURL: URL = URL(string: attachmentString) else { return }
            
            //2 下载图片
            let session: URLSession = URLSession(configuration: URLSessionConfiguration.default)
            let attachmentDownloadTask = session.downloadTask(with: attachmentURL) { [weak self] (url: URL?, _, error: Error?) in
                if error == nil {
                    let attachment = try! UNNotificationAttachment(identifier: attachmentString, url: url!, options: [UNNotificationAttachmentOptionsTypeHintKey: kUTTypePNG])
                    self?.bestAttemptContent?.attachments = [attachment]
                }
                contentHandler(bestAttemptContent)
            }
            attachmentDownloadTask.resume()
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        // Called just before the extension will be terminated by the system.
        // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }

}
